<?php

use SmartRooms\Http\Controllers\API\ComfortOS;

Route::get('/', function () {
    return response()->json ([

    ]);
});

Route::post('authenticate', [ComfortOS\JWTController::class, 'authenticate'])->name('jwt.authenticate');
Route::get('refresh', [ComfortOS\JWTController::class, 'refresh'])->name('jwt.refresh');

//Route::post ('crossbar-subscription/{tag?}/{identifier?}/{extra?}', [\Crossbar\Subscribers\CrossbarSubscriber::class, 'receive'])->name('crossbar.receive');

Route::middleware(['auth:api'])->group(function () {
    Route::apiResource('buildings', ComfortOS\BuildingController::class)->only(['index', 'show']);
    Route::apiResource('bulbs', ComfortOS\BulbController::class)->only(['index', 'show', 'update']); // UPDATEABLE
    Route::apiResource('cameras', ComfortOS\CameraController::class)->only(['index', 'show', 'update']);
    Route::apiResource('chromecasts', ComfortOS\ChromecastController::class)->only(['index', 'show', 'update']);
    Route::apiResource('dimmers', ComfortOS\DimmerController::class)->only(['index', 'show', 'update']); // UPDATEABLE
    Route::apiResource('extensions', ComfortOS\ExtensionController::class)->only(['index', 'show']);
    Route::apiResource('handsets', ComfortOS\HandsetController::class)->only(['index', 'show']);
    Route::apiResource('leds', ComfortOS\LEDController::class)->only(['index', 'show', 'update']);
    Route::apiResource('locks', ComfortOS\LockController::class)->only(['index', 'show', 'update']); // UPDATEABLE
    Route::apiResource('mirrors', ComfortOS\MirrorController::class)->only(['index', 'show', 'update']);
    Route::apiResource('motors', ComfortOS\MotorController::class)->only(['index', 'show', 'update']); // UPDATEABLE
    Route::apiResource('plugs', ComfortOS\PlugController::class)->only(['index', 'show', 'update']); // UPDATEABLE
    Route::apiResource('properties', ComfortOS\PropertyController::class)->only(['index', 'show']);
    Route::apiResource('rooms', ComfortOS\RoomController::class)->only(['index', 'show']);
    Route::apiResource('sensors', ComfortOS\SensorController::class)->only(['index', 'show']);
    Route::apiResource('speakers', ComfortOS\SpeakerController::class)->only(['index', 'show', 'update']); // UPDATEABLE
    Route::apiResource('stbs', ComfortOS\STBController::class)->only(['index', 'show', 'update']);
    Route::apiResource('switches', ComfortOS\PhysicalSwitchController::class)->only(['index', 'show', 'update']);
    Route::apiResource('thermostats', ComfortOS\ThermostatController::class)->only(['index', 'show', 'update']); // UPDATEABLE
    Route::apiResource('tvs', ComfortOS\TVController::class)->only(['index', 'show', 'update']); // UPDATEABLE
    Route::apiResource('users', ComfortOS\UserController::class); // UPDATEABLE
    Route::apiResource('wifi', ComfortOS\WifiController::class); // UPDATEABLE
});
