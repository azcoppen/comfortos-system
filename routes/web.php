<?php

use SmartRooms\Http\Controllers;
use SmartRooms\Http\Controllers\Dashboard\Explore;
use SmartRooms\Http\Controllers\Dashboard\Houston;
use SmartRooms\Http\Controllers\Dashboard\IDAM;
use SmartRooms\Http\Controllers\Dashboard\Mirror;
use SmartRooms\Http\Controllers\Dashboard\OS;
use SmartRooms\Http\Controllers\Dashboard\Shared;
use SmartRooms\Http\Controllers\Dashboard\Telecoms;
use SmartRooms\Http\Controllers\Dashboard\TV;
use SmartRooms\Http\Controllers\Dashboard\Wifi;

use ConsoleTVs\Charts;

Route::get('login', [Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [Controllers\Auth\LoginController::class, 'login'])->name('login.authenticate');
Route::any('logout', [Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/', [Houston\DashboardController::class, 'room_code_generator'])->name('home');
    Route::get('home', [Houston\DashboardController::class, 'generator'])->name('dashboard.home');

    Route::get('chartdata/commands_chart/room/{room}', [Shared\GraphDataController::class, 'room_commands_tput'])->name('charts.commands_chart.room');
    Route::get('chartdata/signals_chart/room/{room}', [Shared\GraphDataController::class, 'room_signals_tput'])->name('charts.signals_chart.room');
    Route::get('chartdata/daily_commands_chart/room/{room}', [Shared\GraphDataController::class, 'room_commands_tput_hourly'])->name('charts.commands_chart.room.hourly');
    Route::get('chartdata/daily_signals_chart/room/{room}', [Shared\GraphDataController::class, 'room_signals_tput_hourly'])->name('charts.signals_chart.room.hourly');

    Route::prefix('dashboards')->name('dashboards.')->group(function () {
        Route::get('/', [Houston\DashboardController::class, 'room_code_generator'])->name('index');
        Route::get('room-generator', [Houston\DashboardController::class, 'room_code_generator'])->name('generator.room');
        Route::get('cpt-generator', [Houston\DashboardController::class, 'cpt_code_generator'])->name('generator.cpt');
        Route::get('signals', [Houston\DashboardController::class, 'signals'])->name('signals');
        Route::get('commands', [Houston\DashboardController::class, 'commands'])->name('commands');
        Route::get('operations', [Houston\DashboardController::class, 'operations'])->name('operations');

        Route::get('roomq', [Houston\DashboardController::class, 'room_search'])->name('roomq');
        Route::get('cptq/{group?}', [Houston\DashboardController::class, 'component_search'])->name('cptq');

        Route::get('tools/mqtt-publisher', [Houston\ToolController::class, 'mqtt_publisher'])->name('tools.mqtt_publisher');
        Route::get('tools/http-poster', [Houston\ToolController::class, 'http_poster'])->name('tools.http_poster');

        Route::get('checklists/type/{type}', [Houston\ChecklistController::class, 'index'])->name('checklists.index.type');
        Route::resource ('checklists', Houston\ChecklistController::class)->only (['index', 'show']);

        Route::get('mia-alerts', [Houston\MIAController::class, 'index'])->name('mia');
    });

    Route::prefix('explore')->name('explorer.')->group(function () {
        Route::get('/', [Explore\ExplorerController::class, 'index'])->name('index');
        Route::resource('operators', Explore\OperatorController::class);
        Route::resource('operators.brands', Explore\BrandController::class);
        Route::resource('operators.brands.properties', Explore\PropertyController::class);
        Route::resource('operators.brands.properties.buildings', Explore\BuildingController::class);
        Route::get('operators/{operator}/brands/{brand}/properties/{property}/buildings/{building}/{floor}', [Explore\RoomController::class, 'index'])->name ('operators.brands.properties.buildings.floor')->where('floor', '[0-9]+');
        Route::get('operators/{operator}/brands/{brand}/properties/{property}/buildings/{building}/{floor}/stickers', [Explore\BuildingController::class, 'stickers'])->name ('operators.brands.properties.buildings.floor.stickers')->where('floor', '[0-9]+');

        Route::prefix('operators/{operator}/brands/{brand}/properties/{property}/buildings/{building}/rooms/{room}')->name('operators.brands.properties.buildings.rooms.')->group(function () {
          Route::get('feed', [Explore\RoomController::class, 'feed'])->name ('feed');
          Route::get('hab', [Explore\RoomController::class, 'hab_availability'])->name ('hab_availability');
          Route::put('hab/provision', [Explore\RoomController::class, 'hab_provisioning'])->name ('hab_provisioning');
          Route::put('hab/deprovision', [Explore\RoomController::class, 'hab_deprovisioning'])->name ('hab_deprovisioning');
          Route::get('hab-config', [Explore\RoomController::class, 'openhab'])->name ('hab_config');
        });

        Route::resource('operators.brands.properties.buildings.rooms', Explore\RoomController::class);

        foreach ([
          'bulbs', 'cameras', 'chromecasts', 'dects', 'dimmers', 'extensions',
          'habs', 'handsets', 'leds', 'locks', 'mirrors', 'motors', 'plugs',
          'routers', 'sensors', 'speakers', 'stbs', 'switches', 'thermostats',
          'tvs', 'wifi-networks'] AS $cpt)
        {

          Route::prefix('operators/{operator}/brands/{brand}/properties/{property}/buildings/{building}/rooms/{room}')->name('operators.brands.properties.buildings.rooms.')->group(function () use ($cpt) {
            Route::get('/'.$cpt.'/{'.Str::singular ($cpt).'}/feed/commands', [Explore\ComponentController::class, 'commands'])->name ($cpt.'.feed.commands');
            Route::get('/'.$cpt.'/{'.Str::singular ($cpt).'}/feed/signals', [Explore\ComponentController::class, 'signals'])->name ($cpt.'.feed.signals');
            Route::put('/'.$cpt.'/provision', [Explore\ComponentController::class, 'provision'])->name ($cpt.'.provision');
            Route::put('/'.$cpt.'/deprovision', [Explore\ComponentController::class, 'deprovision'])->name ($cpt.'.deprovision');
          });

          Route::get('chartdata/daily_commands_chart/'.$cpt.'/{'.Str::singular ($cpt).'}', [Shared\GraphDataController::class, 'component_commands_tput_hourly'])->name('charts.commands_chart.'.Str::singular ($cpt).'.hourly');
          Route::get('chartdata/daily_signals_chart/'.$cpt.'/{'.Str::singular ($cpt).'}', [Shared\GraphDataController::class, 'component_signals_tput_hourly'])->name('charts.signals_chart.'.Str::singular ($cpt).'.hourly');

          Route::get('chartdata/commands_chart/'.$cpt.'/{'.Str::singular ($cpt).'}', [Shared\GraphDataController::class, 'component_commands_tput'])->name('charts.commands_chart.'.Str::singular ($cpt));
          Route::get('chartdata/signals_chart/'.$cpt.'/{'.Str::singular ($cpt).'}', [Shared\GraphDataController::class, 'component_signals_tput'])->name('charts.signals_chart.'.Str::singular ($cpt));

          Route::get('chartdata/attribute_daily_signals_chart/'.$cpt.'/{'.Str::singular ($cpt).'}/{attribute}', [Shared\GraphDataController::class, 'component_signals_attribute_hourly'])->name('charts.attribute_signals_chart.'.Str::singular ($cpt).'.hourly');
          Route::get('chartdata/attribute_daily_commands_chart/'.$cpt.'/{'.Str::singular ($cpt).'}/{attribute}', [Shared\GraphDataController::class, 'component_commands_attribute_hourly'])->name('charts.attribute_commands_chart.'.Str::singular ($cpt).'.hourly');

          Route::get('chartdata/attribute_signals_chart/'.$cpt.'/{'.Str::singular ($cpt).'}/{attribute}', [Shared\GraphDataController::class, 'component_signals_attribute'])->name('charts.attribute_signals_chart.'.Str::singular ($cpt));
          Route::get('chartdata/attribute_commands_chart/'.$cpt.'/{'.Str::singular ($cpt).'}/{attribute}', [Shared\GraphDataController::class, 'component_commands_attribute'])->name('charts.attribute_commands_chart.'.Str::singular ($cpt));

          Route::resource('operators.brands.properties.buildings.rooms.'.$cpt, Explore\ComponentController::class)->except(['index']);

        }
    });

    Route::prefix('idam')->name('idam.')->group(function () {
        Route::get('/', [IDAM\UserController::class, 'index'])->name('index');
        Route::resource('activity', IDAM\ActivityController::class)->only(['index', 'show', 'destroy']);
        Route::resource('permissions', IDAM\PermissionController::class);
        Route::resource('roles', IDAM\RoleController::class);

        Route::get('/users/tagged/{role_tag}', [IDAM\UserController::class, 'index'])->name('users.index.tag');
        Route::resource('users', IDAM\UserController::class);

        Route::resource('users.notes', Shared\NoteController::class);
        Route::resource('users.operators', Shared\OperatorController::class);
        Route::resource('users.permissions', Shared\PermissionController::class);
        Route::resource('users.roles', Shared\RoleController::class);

        Route::get('users/{user}/accesses/{access}/halt', [Shared\AccessController::class, 'halt'])->name('users.accesses.halt');
        Route::resource('users.accesses', Shared\AccessController::class);
    });

    Route::prefix('mirrors')->name('mirrors.')->group(function () {
        Route::get('/', [Mirror\DisplayController::class, 'index'])->name('index');
        Route::resource('displays', Mirror\DisplayController::class);
        Route::get('modules/tagged/{tag?}', [Mirror\ModuleController::class, 'index'])->name('modules.index.tag')->where('tag', '[A-Za-z]+');
        Route::resource('modules', Mirror\ModuleController::class);
    });

    Route::prefix('os')->name('os.')->group(function () {
        Route::get('/', [OS\HABController::class, 'index'])->name('index');
        Route::resource('habs', OS\HABController::class);
        Route::get('mqtt-brokers/{mqtt_broker}/mosquitto', [OS\MQTTBrokerController::class, 'mosquitto_template'])->name('mqtt-brokers.mosquitto');
        Route::get('mqtt-brokers/{mqtt_broker}/env', [OS\MQTTBrokerController::class, 'env_template'])->name('mqtt-brokers.env');
        Route::get('mqtt-brokers/{mqtt_broker}/systemd', [OS\MQTTBrokerController::class, 'systemd_template'])->name('mqtt-brokers.systemd');
        Route::resource('mqtt-brokers', OS\MQTTBrokerController::class);
        Route::get('ws-brokers/{ws_broker}/config', [OS\WSBrokerController::class, 'config_template'])->name('ws-brokers.config');
        Route::get('ws-brokers/{ws_broker}/nginx', [OS\WSBrokerController::class, 'nginx_template'])->name('ws-brokers.nginx');
        Route::resource('ws-brokers', OS\WSBrokerController::class);
        Route::get('vpn-clients/{vpn_client}/revoke', [OS\VPNClientController::class, 'revoke'])->name('vpn-clients.revoke');
        Route::get('vpn-clients/{vpn_client}/unrevoke', [OS\VPNClientController::class, 'unrevoke'])->name('vpn-clients.unrevoke');
        Route::resource('vpn-clients', OS\VPNClientController::class);
        Route::resource('vpn-servers', OS\VPNServerController::class);
    });

    Route::prefix('telecoms')->name('telecoms.')->group(function () {
        Route::get('/', [Telecoms\PBXController::class, 'index'])->name('index');
        Route::resource('pbxs', Telecoms\PBXController::class);
        Route::resource('dect', Telecoms\DECTController::class);
        Route::resource('handsets', Telecoms\HandsetController::class);
        Route::resource('extensions', Telecoms\ExtensionController::class);
    });

    Route::prefix('tvs')->name('tvs.')->group(function () {
        Route::get('/', [TV\STBController::class, 'index'])->name('index');
        Route::resource('stbs', TV\STBController::class);
        Route::get('apps/{tag?}', [TV\AppController::class, 'index'])->name('apps.index');
    });

    Route::prefix('wifi')->name('wifi.')->group(function () {
        Route::get('/', [Wifi\RouterController::class, 'index'])->name('index');
        Route::resource('ssids', Wifi\NetworkController::class);
        Route::resource('routers', Wifi\RouterController::class);
    });
});
