<?php

use SmartRooms\Http\Controllers\Guest;

  Route::view ('/', 'guest.index');

  Route::get ('locator/{coords?}', [Guest\LocatorController::class, 'options'])->name ('locator');

    Route::get ('lang/{iso}', function ($iso) {
      session (['lang' => $iso]);
      return redirect()->back();
    })->name ('lang');

    foreach ([
      'bulbs', 'cameras', 'chromecasts', 'dects', 'dimmers', 'extensions',
      'habs', 'handsets', 'leds', 'locks', 'mirrors', 'motors', 'plugs',
      'routers', 'sensors', 'speakers', 'stbs', 'switches', 'thermostats',
      'tvs', 'wifi-networks'] AS $cpt)
    {
      Route::prefix ('component/'.$cpt.'/{'.Str::singular ($cpt).'}')->name ('component.'.Str::singular ($cpt).'.')->group (function () {
        Route::get ('entrance/{prefill?}', [Guest\ComponentController::class, 'login'])->name ('login');
        Route::post ('verify', [Guest\ComponentController::class, 'verify'])->name ('verify');

        Route::middleware (['guest.otp'])->group (function () {
          Route::get ('/', [Guest\ComponentController::class, 'index'])->name ('display');
          Route::get ('logout', [Guest\ComponentController::class, 'logout'])->name ('logout');
        });
      });
    };


    Route::prefix ('room/{room}')->name ('room.')->group (function () {

      Route::get ('entrance/{prefill?}', [Guest\RoomController::class, 'login'])->name ('login');
      Route::post ('verify', [Guest\RoomController::class, 'verify'])->name ('verify');

      Route::middleware (['guest.otp'])->group (function () {
        Route::get ('/', [Guest\RoomController::class, 'index'])->name ('display');
        Route::get ('logout', [Guest\RoomController::class, 'logout'])->name ('logout');

        Route::get ('locks', [Guest\LockController::class, 'index'])->name ('locks');
        Route::get ('inbox', [Guest\InboxController::class, 'index'])->name ('inbox');
        Route::get ('lighting', [Guest\LightingController::class, 'index'])->name ('lighting');
        Route::get ('phone', [Guest\PhoneController::class, 'index'])->name ('phone');
        Route::get ('tv', [Guest\TVController::class, 'index'])->name ('tv');
        Route::get ('speakers', [Guest\SpeakerController::class, 'index'])->name ('speakers');
        Route::get ('motors', [Guest\MotorController::class, 'index'])->name ('motors');
        Route::get ('temp', [Guest\TempController::class, 'index'])->name ('temp');
        Route::get ('power', [Guest\PowerController::class, 'index'])->name ('power');

        Route::resource ('wifi', Guest\WifiController::class)->only (['index', 'store', 'destroy']);
      });



    });
