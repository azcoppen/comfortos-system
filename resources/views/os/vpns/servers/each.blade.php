@include ('partials.components.panels.sexybox', [
    'option' => 2,
    'icon' => 'cloud',
    'title' => $record->label,
    'subtitle' => $record->timezone,
    'figure' => '<i class="text-success" data-feather="command"></i> &nbsp;'.$record->ip_range,
    'link' => route ('os.vpn-servers.show', $record->_id),
    'edit' => route ('os.vpn-servers.edit', $record->_id),
    'footer_text' => $record->type . ' ' . $record->region . ': ' . $record->subnet,
])
