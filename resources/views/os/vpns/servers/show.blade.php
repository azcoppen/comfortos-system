@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: VPNs</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">VPN Cloud Servers</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">VPC</a>
            <a href="{{ route('os.vpn-servers.index') }}" class="text-primary">Servers</a>
            <a href="{{ route('os.vpn-servers.show', $vpn_server->_id) }}" class="text-primary">{{ $vpn_server->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.vpn-servers.edit', $vpn_server->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $vpn_server->label ?? $vpn_server->_id }}</h1>
  </div>
  <hr />

    <div class="row">

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $vpn_server->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Label</td>
                                <td class="border-0">{{ $vpn_server->label }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Type</td>
                                <td class="border-0">{{ $vpn_server->type }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Region</td>
                                <td class="border-0">{{ $vpn_server->region }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Timezone</td>
                                <td class="border-0">{{ $vpn_server->timezone }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Networking</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Host</td>
                                <td class="border-0">{{ $vpn_server->host }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Ports</td>
                                <td class="border-0">@json($vpn_server->ports)</td>
                            </tr>
                            <tr>
                                <td class="border-0">IP Range</td>
                                <td class="border-0">{{ $vpn_server->ip_range }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Subnet</td>
                                <td class="border-0">{{ $vpn_server->subnet }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Cipher</td>
                                <td class="border-0">{{ $vpn_server->cipher }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    @include ('os.vpns.servers.clients_table', [
      'title' => 'Clients',
      'clients' => $vpn_server->vpn_clients,
    ])

    @include ('os.vpns.servers.components_table', [
      'title' => 'DECT',
      'components' => $vpn_server->dects,
      'route' => 'telecoms.dect.show',
    ])

    @include ('os.vpns.servers.components_table', [
      'title' => 'HABs',
      'components' => $vpn_server->habs,
      'route' => 'os.habs.show',
    ])

    @include ('os.vpns.servers.components_table', [
      'title' => 'Mirrors',
      'components' => $vpn_server->mirrors,
      'route' => 'mirrors.displays.show',
    ])

    @include ('os.vpns.servers.components_table', [
      'title' => 'PBXs',
      'components' => $vpn_server->pbxs,
      'route' => 'telecoms.pbxs.show',
    ])

    @include ('os.vpns.servers.components_table', [
      'title' => 'Routers',
      'components' => $vpn_server->routers,
      'route' => 'wifi.routers.show',
    ])

    @include ('os.vpns.servers.components_table', [
      'title' => 'STBs',
      'components' => $vpn_server->stbs,
      'route' => 'tvs.stbs.show',
    ])

    @include ('partials.components.panels.device_business_flow', ['component' => $vpn_server])

@stop
