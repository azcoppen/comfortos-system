<div class="row">
  <div class="col-lg-12 card-margin">
      <div class="card">
          <div class="card-header">
              <h5 class="card-title">{{ $title ?? 'Clients' }}</h5>
          </div>
          <div class="card-body">
            @if ( isset ($clients) && count ($clients) )
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Identifier</th>
                      <th>Type</th>
                      <th>Label</th>
                      <th>Client</th>
                      <th>Created</th>
                    </tr>
                  </thead>
                    <tbody>

                        @foreach ($clients AS $record)
                          <tr>
                              <td><i class="fa fa-key text-muted"></i></td>
                              <td><a href="{{ route('os.vpn-clients.show', $record->_id) }}">{{ $record->_id }}</a></td>
                              <td>{{ $record->type }}</td>
                              <td>{{ $record->label }}</td>
                              <td class="text-{{ $record->revoked_at ? 'danger' : 'success' }}">{{ $record->client_id }}</td>
                              <td>{{ $record->created_at->diffForHumans() }}</td>
                          </tr>
                        @endforeach


                    </tbody>
                </table>
              </div>
              @else
                @include ('partials.components.alerts.empty')
              @endif
          </div>
      </div>
  </div>
</div>
