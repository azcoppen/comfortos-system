@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: VPNs</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">OS</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">VPC</a>
            <a href="{{ route('os.vpn-servers.index') }}" class="text-primary">VPN Servers</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.vpn-servers.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New VPN Server</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">VPN Servers</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'VPN servers'])
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
    <div class="row mt-5">
        @isset($records)
          @each('os.vpns.servers.each', $records, 'record')

          <hr />

          <div class="row">
            <div class="col-md-6 offset-3">
              {{ $records->links() }}
            </div>
          </div>
        @endisset

        @empty($records)
            @include ('partials.components.alerts.empty')
        @endempty
    </div>
@endsection
