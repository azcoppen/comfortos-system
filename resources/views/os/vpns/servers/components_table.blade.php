<div class="row">
  <div class="col-lg-12 card-margin">
      <div class="card">
          <div class="card-header">
              <h5 class="card-title">{{ $title ?? 'Components' }}</h5>
          </div>
          <div class="card-body">
            @if ( isset ($components) && count ($components) )
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Identifier</th>
                      <th>Label</th>
                      <th>VPN IP</th>
                      <th>Room</th>
                      <th>Created</th>
                    </tr>
                  </thead>
                    <tbody>

                        @foreach ($components AS $component)
                          <tr>
                            <td><a href="{{ route ($route, $component->_id) }}">{{ $component->_id }}</a></td>
                            <td>{{ $component->label ?? ''}}</td>
                            <td class="text-success">{{ $component->vpn_ip ?? '' }}</td>
                            <td>{{ $component->room->label ?? ''}}</td>
                            <td>{{ $component->created_at->timezone (auth()->user()->timezone)->format ('M d Y H:i:s A') }}</td>
                          </tr>
                        @endforeach


                    </tbody>
                </table>
              </div>
              @else
                @include ('partials.components.alerts.empty')
              @endif
          </div>
      </div>
  </div>
</div>
