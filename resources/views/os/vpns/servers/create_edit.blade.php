@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: VPNs</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">VPNs</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">VPC</a>
            <a href="{{ route('os.vpn-servers.index') }}" class="{{ isset($vpn_server) ? '' : 'text-primary' }}">Servers</a>

            @if ( isset($vpn_server) )
            <a href="{{ route('os.vpn-servers.show', $vpn_server->_id) }}" class="text-primary">{{ $vpn_server->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.vpn-servers.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <h1 class="display-4">{{ isset ($vpn_server) ? 'Update '.$vpn_server->label : 'Add A New VPN Server' }}</h1>
  <hr />
  @if ( isset ($vpn_server) )
    {!! Form::open (['method' => 'PUT', 'route' => ['os.vpn-servers.update', $vpn_server->_id]]) !!}
  @else
    {!! Form::open (['route' => 'os.vpn-servers.store']) !!}
  @endif

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $vpn_server->label ?? null,
                  'default'     => '',
                  'placeholder' => "VPN Server",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which type of VPN is it?",
                  'name'        => 'type',
                  'options'     => ['openvpn' => 'openvpn', 'wireguard' => 'wireguard'],
                  'value'       => $vpn_server->type ?? null,
                  'default'     => 'openvpn',
                  'placeholder' => "Choose its type",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which region is it in?",
                  'name'        => 'region',
                  'value'       => $vpn_server->region ?? null,
                  'default'     => '',
                  'placeholder' => "us-west",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Timezone",
                  'name'        => 'timezone',
                  'options'     => array_combine (array_values (config ('dropdowns.timezones')), array_values (config ('dropdowns.timezones'))),
                  'value'       => $vpn_server->timezone ?? null,
                  'default'     => 'America/Los_Angeles',
                  'placeholder' => "Choose a timezone",
                  'required'    => true,
                ])
              </div>
            </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="settings"></i> Configuration</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the hostname?",
                      'name'        => 'host',
                      'value'       => $vpn_server->host ?? null,
                      'default'     => '',
                      'placeholder' => "us-west.openvpn.com",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.number', [
                              'label'       => "What is the TLS port?",
                              'name'        => 'ports[0]',
                              'value'       => $vpn_server->ports[0] ?? null,
                              'default'     => '443',
                              'placeholder' => "443",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.number', [
                              'label'       => "What is the UDP port?",
                              'name'        => 'ports[1]',
                              'value'       => $vpn_server->ports[1] ?? null,
                              'default'     => '1194',
                              'placeholder' => "1194",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the subnet IP range?",
                      'name'        => 'subnet',
                      'value'       => $vpn_server->subnet ?? null,
                      'default'     => '',
                      'placeholder' => "10.0.0.0/24",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the client IP range?",
                      'name'        => 'ip_range',
                      'value'       => $vpn_server->ip_range ?? null,
                      'default'     => '',
                      'placeholder' => "100.96.1.32/28",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the netmask?",
                      'name'        => 'netmask',
                      'value'       => $vpn_server->netmask ?? null,
                      'default'     => '',
                      'placeholder' => "255.255.255.0",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.select', [
                      'label'       => "Which cipher does it use?",
                      'name'        => 'cipher',
                      'options'     => array_combine (openssl_get_cipher_methods(), openssl_get_cipher_methods()),
                      'value'       => $vpn_server->cipher ?? null,
                      'default'     => 'Choose a cipher:',
                      'placeholder' => "AES-256-CBC",
                      'required'    => true,
                    ])
                  </div>

                </div>
              </div>
            </div> <!-- end col -->



      </div> <!-- end row -->


  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $vpn_server ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $vpn_server ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $vpn_server ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->


    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}


    @if ( isset ($vpn_server) )
      @include ('partials.components.panels.delete_bar', [
        'entity'  => $vpn_server,
        'text'    => 'VPN',
        'destroy' => [
          'route' => 'os.vpn-servers.destroy',
          'params'=> [$vpn_server->_id],
        ],
        'return'  => [
          'route' => 'os.vpn-servers.index',
          'params'=> [$vpn_server->_id],
        ],
      ])
    @endif


@endsection
