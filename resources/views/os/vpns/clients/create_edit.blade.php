@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: VPN Clients</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">VPNs</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">VPC</a>
            <a href="{{ route('os.vpn-clients.index') }}" class="{{ isset($vpn_client) ? '' : 'text-primary' }}">Clients</a>

            @if ( isset($vpn_client) )
            <a href="{{ route('os.vpn-clients.show', $vpn_client->_id) }}" class="text-primary">{{ $vpn_client->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.vpn-clients.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <h1 class="display-4">{{ isset ($vpn_client) ? 'Update '.$vpn_client->label : 'Add A New VPN Client' }}</h1>
  <hr />
  @if ( isset ($vpn_client) )
    {!! Form::open (['method' => 'PUT', 'route' => ['os.vpn-clients.update', $vpn_client->_id]]) !!}
  @else
    {!! Form::open (['route' => 'os.vpn-clients.store']) !!}
  @endif

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $vpn_client->label ?? null,
                  'default'     => '',
                  'placeholder' => "VPN Server",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which VPN does it provide access to?",
                  'name'        => 'vpn_server_id',
                  'options'     => $vpn_servers->sortBy('label')->pluck('label', '_id')->all(),
                  'value'       => $vpn_client->vpn_server_id ?? null,
                  'default'     => '',
                  'placeholder' => "Choose the VPN server",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which type of VPN is it?",
                  'name'        => 'type',
                  'options'     => ['openvpn' => 'openvpn', 'wireguard' => 'wireguard'],
                  'value'       => $vpn_client->type ?? null,
                  'default'     => 'openvpn',
                  'placeholder' => "Choose its type",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What is the client ID on the VPN server?",
                  'name'        => 'client_id',
                  'value'       => $vpn_client->client_id ?? null,
                  'default'     => '',
                  'placeholder' => "some-client",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which cipher does it use?",
                  'name'        => 'cipher',
                  'options'     => array_combine (openssl_get_cipher_methods(), openssl_get_cipher_methods()),
                  'value'       => $vpn_client->cipher ?? null,
                  'default'     => 'Choose a cipher:',
                  'placeholder' => "AES-256-CBC",
                  'required'    => true,
                ])
              </div>

            </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="settings"></i> Certificate</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.textarea', [
                      'label'       => "What certificate was generated for the client?",
                      'name'        => 'certificate',
                      'value'       => $vpn_client->certificate ?? null,
                      'default'     => '',
                      'placeholder' => "",
                      "rows"        => 16,
                      'required'    => true,
                    ])
<pre class="mt-3">
cd /etc/openvpn/easy-rsa/
/easyrsa build-client-full my-new-client-name nopass
cat my-new-client-name.ovpn # <--- paste this
</pre>
                  </div>

                </div>
              </div>
            </div> <!-- end col -->



      </div> <!-- end row -->


  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $vpn_client ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $vpn_client ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $vpn_client ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->


    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}


    @if ( isset ($vpn_client) )
      @include ('partials.components.panels.delete_bar', [
        'entity'  => $vpn_client,
        'text'    => 'VPN',
        'destroy' => [
          'route' => 'os.vpn-clients.destroy',
          'params'=> [$vpn_client->_id],
        ],
        'return'  => [
          'route' => 'os.vpn-clients.index',
          'params'=> [$vpn_client->_id],
        ],
      ])
    @endif


@endsection
