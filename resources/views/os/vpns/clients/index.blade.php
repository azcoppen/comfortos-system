@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: VPNs</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">OS</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">VPC</a>
            <a href="{{ route('os.vpn-clients.index') }}" class="text-primary">VPN Clients</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.vpn-clients.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New VPN Client</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">VPN Clients</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'VPN Clients'])
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <div class="row mt-3">
      <div class="col-lg-12 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">Profiles By Date</h5>
              </div>
              <div class="card-body">

                @isset($records)
                  <div class="table-responsive">
                      <table id="" class="table table-hover" style="width:100%">
                          <thead>
                          <tr>
                              <th></th>
                              <th>Identifier</th>
                              <th>Type</th>
                              <th>Server</th>
                              <th>Label</th>
                              <th>Client</th>
                              <th>Created</th>
                          </tr>
                          </thead>
                          <tbody>
                            @each('os.vpns.clients.each', $records, 'record')
                          </tbody>
                      </table>
                  </div>

                  <hr />

                  <div class="row">
                    <div class="col-md-6 offset-3">
                      {{ $records->links() }}
                    </div>
                  </div>
                @endisset

                @empty($records)
                    @include ('partials.components.alerts.empty')
                @endempty

              </div>
          </div>
      </div>
  </div>
@endsection
