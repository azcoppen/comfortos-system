@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: VPN Clients</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">VPN Clients</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">VPC</a>
            <a href="{{ route('os.vpn-clients.index') }}" class="text-primary">Clients</a>
            <a href="{{ route('os.vpn-clients.show', $vpn_client->_id) }}" class="text-primary">{{ $vpn_client->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
        <a href="{{ route ('os.vpn-clients.edit', $vpn_client->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
        @if ( $vpn_client->revoked_at )
          <a href="{{ route ('os.vpn-clients.unrevoke', $vpn_client->_id) }}" class="btn btn-success"><i data-feather="check-circle"></i> Reactivate</a>
        @else
          <a href="{{ route ('os.vpn-clients.revoke', $vpn_client->_id) }}" class="btn btn-warning"><i data-feather="x-circle"></i> Mark Revoked</a>
        @endif

      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-6">{{ $vpn_client->label ?? $vpn_client->_id }}</h1>
  </div>
  <hr />

  @if ( $vpn_client->revoked_at )
        <div class="row">
          <div class="col-sm-12">
            <div class="alert alert-warning" role="alert">
              <div class="alert-text">
                  <strong>This client was revoked {{ $vpn_client->revoked_at }}.</strong>
              </div>
            </div>
          </div>
        </div>
  @endif

    <div class="row">

        <div class="col-lg-6 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $vpn_client->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Server</td>
                                <td class="border-0">{{ $vpn_client->vpn_server->label ?? '' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Host</td>
                                <td class="border-0">{{ $vpn_client->vpn_server->host ?? '' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Label</td>
                                <td class="border-0">{{ $vpn_client->label }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Type</td>
                                <td class="border-0">{{ $vpn_client->type }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Client ID</td>
                                <td class="border-0 text-primary">{{ $vpn_client->client_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Cipher</td>
                                <td class="border-0">{{ $vpn_client->cipher }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-6 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Certificate</h5>
                </div>
                <div class="card-body">
<pre class="mb-0">
{{ $vpn_client->certificate }}
</pre>
                </div>
            </div>
        </div>

    </div>

    @include ('os.vpns.clients.components_table', [
      'title' => 'DECT',
      'components' => $vpn_client->dects,
      'route' => 'telecoms.dect.show',
    ])

    @include ('os.vpns.clients.components_table', [
      'title' => 'HABs',
      'components' => $vpn_client->habs,
      'route' => 'os.habs.show',
    ])

    @include ('os.vpns.clients.components_table', [
      'title' => 'Mirrors',
      'components' => $vpn_client->mirrors,
      'route' => 'mirrors.displays.show',
    ])

    @include ('os.vpns.clients.components_table', [
      'title' => 'PBXs',
      'components' => $vpn_client->pbxs,
      'route' => 'telecoms.pbxs.show',
    ])

    @include ('os.vpns.clients.components_table', [
      'title' => 'Routers',
      'components' => $vpn_client->routers,
      'route' => 'wifi.routers.show',
    ])

    @include ('os.vpns.clients.components_table', [
      'title' => 'STBs',
      'components' => $vpn_client->stbs,
      'route' => 'tvs.stbs.show',
    ])

    @include ('partials.components.panels.device_business_flow', ['component' => $vpn_client])

@stop
