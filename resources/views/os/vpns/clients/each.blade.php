<tr>
    <td><i class="fa fa-key text-muted"></i></td>
    <td><a href="{{ route('os.vpn-clients.show', $record->_id) }}">{{ $record->_id }}</a></td>
    <td>{{ $record->type }}</td>
    <td>{{ $record->vpn_server->label ?? '' }}</td>
    <td>{{ $record->label }}</td>
    <td class="text-{{ $record->revoked_at ? 'danger' : 'success' }}">{{ $record->client_id }}</td>
    <td>{{ $record->created_at->diffForHumans() }}</td>

</tr>
