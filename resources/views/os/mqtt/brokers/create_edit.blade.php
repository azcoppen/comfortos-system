@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: MQTT Brokers</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">OS</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">Brokers</a>
            <a href="{{ route('os.mqtt-brokers.index') }}" class="{{ isset ($mqtt_broker) ? '' : 'text-primary' }}">MQTT Brokers</a>

            @if ( isset($mqtt_broker) )
            <a href="{{ route('os.mqtt-brokers.show', $mqtt_broker->_id) }}" class="text-primary">{{ $mqtt_broker->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.mqtt-brokers.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <h1 class="display-4">{{ isset ($mqtt_broker) ? 'Update '.$mqtt_broker->label : 'Add A New MQTT Broker' }}</h1>
  <hr />
  @if ( isset ($mqtt_broker) )
    {!! Form::open (['method' => 'PUT', 'route' => ['os.mqtt-brokers.update', $mqtt_broker->_id]]) !!}
  @else
    {!! Form::open (['route' => 'os.mqtt-brokers.store']) !!}
  @endif

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $mqtt_broker->label ?? null,
                  'default'     => '',
                  'placeholder' => "MQTT broker",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which type of MQTT broker is it?",
                  'name'        => 'type',
                  'options'     => ['mosquitto' => 'mosquitto', 'hivemq' => 'hivemq', ],
                  'value'       => $mqtt_broker->type ?? null,
                  'default'     => 'mosquitto',
                  'placeholder' => "Choose its type",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which version is it?",
                  'name'        => 'v',
                  'value'       => $mqtt_broker->v ?? null,
                  'default'     => '',
                  'placeholder' => "2.0",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which region is it in?",
                  'name'        => 'region',
                  'value'       => $mqtt_broker->region ?? null,
                  'default'     => '',
                  'placeholder' => "us-west",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "What server context is it?",
                  'name'        => 'context',
                  'options'     => ['local' => 'local', 'remote' => 'remote', ],
                  'value'       => $mqtt_broker->context ?? null,
                  'default'     => 'local',
                  'placeholder' => "Choose its context",
                  'required'    => true,
                ])
              </div>

            </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="settings"></i> Configuration</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the hostname?",
                      'name'        => 'host',
                      'value'       => $mqtt_broker->host ?? null,
                      'default'     => '',
                      'placeholder' => "localhost",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-row">
                      <div class="col-md-4">
                          <div class="position-relative form-group">
                            @include ('components.fields.number', [
                              'label'       => "What is the insecure port?",
                              'name'        => 'insecure_port',
                              'value'       => $mqtt_broker->insecure_port ?? null,
                              'default'     => '1883',
                              'placeholder' => "1883",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="position-relative form-group">
                            @include ('components.fields.number', [
                              'label'       => "What is the secure port?",
                              'name'        => 'secure_port',
                              'value'       => $mqtt_broker->secure_port ?? null,
                              'default'     => '8883',
                              'placeholder' => "8883",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="position-relative form-group">
                            @include ('components.fields.number', [
                              'label'       => "What is the admin ui port?",
                              'name'        => 'admin_port',
                              'value'       => $mqtt_broker->admin_port ?? null,
                              'default'     => '',
                              'placeholder' => "8080",
                              'required'    => false,
                            ])
                          </div>
                      </div>
                  </div>


                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is the admin username?",
                              'name'        => 'admin_user',
                              'value'       => $mqtt_broker->admin_user ?? null,
                              'default'     => '',
                              'placeholder' => "admin",
                              'required'    => false,
                              'help'        => 'Not used by Mosquitto',
                            ])
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is the admin pass?",
                              'name'        => 'admin_pass',
                              'value'       => $mqtt_broker->admin_pass ?? null,
                              'default'     => '',
                              'placeholder' => "",
                              'required'    => false,
                              'help'        => 'Not used by Mosquitto',
                            ])
                          </div>
                      </div>
                  </div>

                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is the app username?",
                              'name'        => 'app_user',
                              'value'       => $mqtt_broker->app_user ?? null,
                              'default'     => '',
                              'placeholder' => "smartrooms",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is the app pass?",
                              'name'        => 'app_pass',
                              'value'       => $mqtt_broker->app_pass ?? null,
                              'default'     => '',
                              'placeholder' => "",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                  </div>

                </div>
              </div>
            </div> <!-- end col -->

            @include ('partials.components.forms.vpn', [
              'entity'         => $mqtt_broker ?? null,
              'vpn_servers'    => $vpn_servers,
            ])

      </div> <!-- end row -->


  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $mqtt_broker ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $mqtt_broker ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $mqtt_broker ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->


    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}


    @if ( isset ($mqtt_broker) )
      @include ('partials.components.panels.delete_bar', [
        'entity'  => $mqtt_broker,
        'text'    => 'MQTT Broker',
        'destroy' => [
          'route' => 'os.mqtt-brokers.destroy',
          'params'=> [$mqtt_broker->_id],
        ],
        'return'  => [
          'route' => 'os.mqtt-brokers.index',
          'params'=> [$mqtt_broker->_id],
        ],
      ])
    @endif


@endsection
