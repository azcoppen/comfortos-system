@include ('partials.components.panels.sexybox', [
    'option' => 1,
    'icon' => 'life-buoy',
    'title' => $record->label,
    'subtitle' => $record->host,
    'figure' => '<i class="text-success" data-feather="command"></i> &nbsp;'.$record->ip_range,
    'link' => route ('os.mqtt-brokers.show', $record->_id),
    'edit' => route ('os.mqtt-brokers.edit', $record->_id),
    'footer_text' => $record->type . ' ' . $record->region . ': ' . $record->subnet,
])
