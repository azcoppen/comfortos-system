@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: MQTT Brokers</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">MQTT Brokers</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">Brokers</a>
            <a href="{{ route('os.mqtt-brokers.index') }}" class="text-primary">MQTT Brokers</a>
            <a href="{{ route('os.mqtt-brokers.show', $mqtt_broker->_id) }}" class="text-primary">{{ $mqtt_broker->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.mqtt-brokers.edit', $mqtt_broker->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $mqtt_broker->label ?? $mqtt_broker->_id }}</h1>
  </div>
  <hr />


  <div class="row">
    <div class="col-sm-9">
      <div class="alert alert-bordered-secondary" role="alert">
    			<span class="alert-text">
<pre class="mb-0">
mosquitto_sub -h {{ $mqtt_broker->host }} -p {{ $mqtt_broker->secure_port ?? '' }} -u {{ $mqtt_broker->app_user }} -P {{ $mqtt_broker->app_pass }} --capath ($CA_FILE) -t #
mosquitto_pub -h {{ $mqtt_broker->host }} -p {{ $mqtt_broker->secure_port ?? '' }} -u {{ $mqtt_broker->app_user }} -P {{ $mqtt_broker->app_pass }} --capath ($CA_FILE) -t some/topic -m "Some Data"
</pre>
          </span>
    	</div>
    </div>
    <div class="col-sm-3 text-right">
      <a class="btn btn-lg btn-secondary" href="{{ route('os.mqtt-brokers.mosquitto', $mqtt_broker->_id) }}"><i data-feather="download"></i> MQTT conf</a>
      <a class="btn btn-lg btn-secondary" href="{{ route('os.mqtt-brokers.env', $mqtt_broker->_id) }}"><i data-feather="download"></i> CLI .env</a>
      <a class="btn btn-lg btn-secondary" href="{{ route('os.mqtt-brokers.systemd', $mqtt_broker->_id) }}"><i data-feather="download"></i> systemd</a>
    </div>
  </div>

    <div class="row">

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $mqtt_broker->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Label</td>
                                <td class="border-0">{{ $mqtt_broker->label }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Type</td>
                                <td class="border-0">{{ $mqtt_broker->type }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Version</td>
                                <td class="border-0">{{ $mqtt_broker->v }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Region</td>
                                <td class="border-0">{{ $mqtt_broker->region }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Server Context</td>
                                <td class="border-0">{{ $mqtt_broker->context }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Connection</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Host</td>
                                <td class="border-0">{{ $mqtt_broker->host }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Insecure Port</td>
                                <td class="border-0">{{ $mqtt_broker->insecure_port }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Secure (TLS) Port</td>
                                <td class="border-0">{{ $mqtt_broker->secure_port ?? '' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Admin Port</td>
                                <td class="border-0">{{ $mqtt_broker->admin_port ?? 'None' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Authentication</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Admin User</td>
                                <td class="border-0">{{ $mqtt_broker->admin_user ?? 'None' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Admin Pass</td>
                                <td class="border-0">{{ $mqtt_broker->admin_pass ?? 'None' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Application User</td>
                                <td class="border-0">{{ $mqtt_broker->app_user }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Application Pass</td>
                                <td class="border-0">{{ $mqtt_broker->app_pass }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    @include ('os.mqtt.brokers.components_table', [
      'title' => 'Properties',
      'components' => $mqtt_broker->properties,
      'route' => 'explorer.operators.brands.index',
    ])


    @include ('partials.components.panels.device_business_flow', ['component' => $mqtt_broker])


@stop
