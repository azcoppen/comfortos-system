<div class="row">
  <div class="col-lg-12 card-margin">
      <div class="card">
          <div class="card-header">
              <h5 class="card-title">{{ $title ?? 'Components' }}</h5>
          </div>
          <div class="card-body">
            @if ( isset ($components) && count ($components) )
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Identifier</th>
                      <th>Operator</th>
                      <th>Brand</th>
                      <th>Property</th>
                      <th>Created</th>
                    </tr>
                  </thead>
                    <tbody>

                        @foreach ($components AS $component)
                          <tr>
                            <td><a href="{{ route ($route, [$component->brand->operator->slug, $component->brand->slug,]) }}">{{ $component->_id }}</a></td>
                            <td>{{ $component->brand->operator->label ?? ''}}</td>
                            <td>{{ $component->brand->label ?? ''}}</td>
                            <td>{{ $component->label ?? ''}}</td>
                            <td>{{ $component->created_at->timezone (auth()->user()->timezone)->format ('M d Y H:i:s A') }}</td>
                          </tr>
                        @endforeach


                    </tbody>
                </table>
              </div>
              @else
                @include ('partials.components.alerts.empty')
              @endif
          </div>
      </div>
  </div>
</div>
