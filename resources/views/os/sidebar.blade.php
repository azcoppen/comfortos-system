
<ul class="nav">
    <li class="nav-header">Controls</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#controls" aria-expanded="false" aria-controls="controls">
            <i data-feather="server" class="menu-icon"></i>
            <span class="menu-title">Bus Automation</span>
        </a>
        <div class="collapse show" id="controls">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'habs' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('os.habs.index') }}"><span class="menu-title">HABs</span></a></li>
            </ul>
        </div>
    </li>

    <li class="nav-header">Brokers</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#brokers" aria-expanded="false" aria-controls="brokers">
            <i data-feather="life-buoy" class="menu-icon"></i>
            <span class="menu-title">Servers</span>
        </a>
        <div class="collapse show" id="brokers">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'mqtt-brokers' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('os.mqtt-brokers.index') }}"><span class="menu-title">MQTT Brokers</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'ws-brokers' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('os.ws-brokers.index') }}"><span class="menu-title">WS Brokers</span></a></li>
            </ul>
        </div>
    </li>

    <li class="nav-header">VPC</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#vpn" aria-expanded="false" aria-controls="vpn">
            <i data-feather="cloud" class="menu-icon"></i>
            <span class="menu-title">Networks</span>
        </a>
        <div class="collapse show" id="vpn">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'vpn-servers' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('os.vpn-servers.index') }}"><span class="menu-title">Servers</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'vpn-clients' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('os.vpn-clients.index') }}"><span class="menu-title">Clients</span></a></li>
            </ul>
        </div>
    </li>


</ul>
