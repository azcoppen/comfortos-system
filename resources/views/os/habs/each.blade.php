@include ('partials.components.panels.sexybox', [
    'option' => 4,
    'icon' => 'server',
    'title' => $record->label ?? $record->serial,
    'subtitle' => $record->room->label ?? 'Not provisioned to a room',
    'figure' => '<i class="text-success" data-feather="box"></i> &nbsp;'.$record->serial,
    'link' => route ('os.habs.show', $record->_id),
    'edit' => route ('os.habs.edit', $record->_id),
    'footer_text' => $record->os . ' v'.$record->dist. ' ('.$record->vpn_ip.')',
])
