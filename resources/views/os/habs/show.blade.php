@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: OS</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Bus Automation</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">Hardware</a>
            <a href="{{ route('os.habs.index') }}">Bus Automation</a>
            <a href="{{ route('os.habs.index') }}">HABs</a>
            <a href="{{ route('os.habs.show', $hab->_id) }}" class="text-primary">{{ $hab->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.habs.edit', $hab->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')

  <div class="row">
    <h1 class="display-3 col-md-12">{{ $hab->label ?? $hab->_id }}</h1>
  </div>
  <hr />

    <div class="row">
        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ $hab->label }}</h5>
                </div>
                <div class="card-body text-center pt-5 pb-5">
                    <img src="https://res.cloudinary.com/smartrooms/devices/pi.jpg" style="max-width: 100%; max-height: 12rem; margin: auto;" align="center" />
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $hab->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">OS</td>
                                <td class="border-0">{{ $hab->os }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Release</td>
                                <td class="border-0">{{ $hab->release }} ({{ $hab->kernel }})</td>
                            </tr>
                            <tr>
                                <td class="border-0">Model</td>
                                <td class="border-0">{{ $hab->model }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Serial</td>
                                <td class="border-0">{{ $hab->serial }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Hardware</td>
                                <td class="border-0">{{ $hab->hardware }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        @include ('partials.provisioning', ['room' => $hab->room, 'item' => $hab])


        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Network</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Hostname</td>
                                <td class="border-0">{{ $hab->host }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">LAN IP</td>
                                <td class="border-0">{{ $hab->int_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">VPN IP</td>
                                <td class="border-0">{{ $hab->vpn_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Mac</td>
                                <td class="border-0">{{ $hab->mac }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">SSH User</td>
                                <td class="border-0">{{ $hab->ssh_user ?? 'openhabian' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">SSH Pass</td>
                                <td class="border-0">{{ isset ($hab->ssh_pass) ? Crypt::decrypt($hab->ssh_pass) : 'openhabian' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Engine</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Software</td>
                                <td class="border-0">{{ $hab->engine }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Version</td>
                                <td class="border-0">{{ $hab->dist }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">HTTP Auth</td>
                                <td class="border-0">{{ $hab->http_user ?? 'none' }}/{{ $hab->http_pass ?? 'none' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Admin</td>
                                <td class="border-0">
                                  <ul class="list-unstyled">
                                    <li>http://{{ $hab->vpn_ip }}:{{ $hab->http_port ?? 8080 }}</li>
                                    <li>https://{{ $hab->vpn_ip }}:{{ $hab->https_port ?? 8443 }}</li>
                                  </ul>
                                </td>
                            </tr>
                            <tr>
                                <td class="border-0">API</td>
                                <td class="border-0">
                                  <ul class="list-unstyled">
                                    <li>http://{{ $hab->vpn_ip }}:{{ $hab->http_port ?? 8080 }}/rest</li>
                                    <li>https://{{ $hab->vpn_ip }}:{{ $hab->https_port ?? 8443 }}/rest</li>
                                  </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>

    @if ( isset ($hab->room) && is_object ($hab->room) )

      <div class="row">
        @include ('partials.components.panels.commands', ['commands' => $hab->room->commands])
      </div>

      <div class="row">
        @livewire('room-signals-table', ['room' => $hab->room->_id])
      </div>

      <div class="row">
        @include ('partials.components.panels.mqtt_topics', ['room' => $hab->room, 'group' => 'habs', 'component' => $hab])
      </div>

    @endif

    @include ('partials.components.panels.device_business_flow', ['component' => $hab])

@stop
