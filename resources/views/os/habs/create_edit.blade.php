@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: OS</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Bus Automation</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">Hardware</a>
            <a href="{{ route('os.habs.index') }}">Bus Automation</a>
            <a href="{{ route('os.habs.index') }}" class="{{ isset($hab) ? '' : 'text-primary' }}">HABs</a>
            @if ( isset($hab) )
              <a href="{{ route('os.habs.show', $hab->_id) }}" class="text-primary">{{ $hab->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.habs.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')

  <h1 class="display-4">{{ isset ($hab) ? 'Update '.$hab->label : 'Add A New HAB' }}</h1>
  <hr />
  @if ( isset ($hab) )
    {!! Form::open (['method' => 'PUT', 'route' => ['os.habs.update', $hab->_id]]) !!}
  @else
    {!! Form::open (['route' => 'os.habs.store']) !!}
  @endif

  <div class="row">
    <div class="col-sm-12">
      <div class="alert alert-dark" role="alert">
        <div class="alert-text">
            Before you add or update a HAB, run <code>cat /proc/cpuinfo</code>, then <code>cat /etc/*-release</code>, and finally <code>openhab-cli info</code> to get the HAB's internal details.
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Operating System</h5>
            </div>
            <div class="card-body">

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What should the HAB be called?",
                        'name'        => 'label',
                        'value'       => $hab->label ?? null,
                        'default'     => 'OpenHABian 2.5.1 HAB server',
                        'placeholder' => "Name of the HAB",
                        'required'    => true,
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What is the operating system (image)?",
                        'name'        => 'os',
                        'value'       => $hab->os ?? null,
                        'default'     => 'OpenHABian',
                        'placeholder' => "Name of the operating system",
                        'required'    => true,
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "Which operating system release is it?",
                        'name'        => 'release',
                        'value'       => $hab->os ?? null,
                        'default'     => 'Raspbian GNU/Linux 10 (buster)',
                        'placeholder' => "Name of the operating system release",
                        'required'    => true,
                        'help'        => "cat /etc/*-release | grep PRETTY_NAME | cut -d '=' -f 2",
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What's the release's codename?",
                        'name'        => 'codename',
                        'value'       => $hab->release ?? null,
                        'default'     => 'Buster',
                        'placeholder' => "Codename of the operating system release",
                        'required'    => true,
                        'help'        => "cat /etc/*-release | grep VERSION_CODENAME | cut -d '=' -f 2",
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What kernel version is it using?",
                        'name'        => 'kernel',
                        'value'       => $hab->kernel ?? null,
                        'default'     => 'Linux 5.4.72-v7l+',
                        'placeholder' => "Kernel installed inside the operating system",
                        'required'    => true,
                        'help'        => 'uname -r',
                      ])
                    </div>

            </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="box"></i> Hardware</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What model is it?",
                    'name'        => 'model',
                    'value'       => $hab->model ?? null,
                    'default'     => 'Raspberry Pi 4 Model B Rev 1.2',
                    'placeholder' => "Model name of the hardware",
                    'required'    => true,
                    'help'        => "cat /proc/cpuinfo | grep Model | cut -d ' ' -f 2",
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's its serial number?",
                    'name'        => 'serial',
                    'value'       => $hab->serial ?? null,
                    'default'     => '',
                    'placeholder' => "Serial number of the CPU (e.g. 1000000000000000)",
                    'required'    => true,
                    'help'        => "cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2",
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's its hardware ID?",
                    'name'        => 'hardware',
                    'value'       => $hab->hardware ?? null,
                    'default'     => '',
                    'placeholder' => "Hardware number of the board (e.g. BCM0000)",
                    'required'    => true,
                    'help'        => "cat /proc/cpuinfo | grep Hardware | cut -d ' ' -f 2",
                  ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What's its network MAC address?",
                      'name'        => 'mac',
                      'value'       => $hab->mac ?? null,
                      'default'     => '',
                      'placeholder' => "MAC address of the network adaptor (e.g. dc:a6:32:96:f7:26)",
                      'required'    => false,
                      'help'        => "cat /sys/class/net/eth0/address",
                    ])
                  </div>

              </div>
        </div>
      </div> <!-- end col -->

    </div> <!-- end row -->

    <div class="row">

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="package"></i> Engine</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.select', [
                    'label'       => "Which software engine is it running?",
                    'name'        => 'engine',
                    'options'     => ['openHAB' => 'OpenHAB'],
                    'value'       => $hab->engine ?? null,
                    'default'     => 'openHAB',
                    'placeholder' => "Choose a software engine",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "Which version/distribution of the software engine is it?",
                    'name'        => 'dist',
                    'value'       => $hab->dist ?? null,
                    'default'     => '2.5.10-1',
                    'placeholder' => "Internal version of the software",
                    'required'    => true,
                    'help'        => "openhab-cli info",
                  ])
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.number', [
                            'label'       => "What port is the HTTP service on?",
                            'name'        => 'http_port',
                            'value'       => $hab->http_port ?? null,
                            'default'     => '8080',
                            'placeholder' => "Insecure HTTP port number of the software's web admin",
                            'required'    => true,
                            'help'        => "openhab-cli info",
                          ])
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.number', [
                            'label'       => "Which port is the HTTPS service on?",
                            'name'        => 'https_port',
                            'value'       => $hab->https_port ?? null,
                            'default'     => '8443',
                            'placeholder' => "Secure HTTPS port number of the software's web admin",
                            'required'    => true,
                            'help'        => "openhab-cli info",
                          ])
                        </div>
                    </div>
                  </div>

                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is the HTTP basic auth user?",
                              'name'        => 'http_user',
                              'value'       => $hab->http_user ?? null,
                              'default'     => '',
                              'placeholder' => "HTTP basic auth username for the web admin",
                              'required'    => false,
                            ])
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is the HTTP basic auth password?",
                              'name'        => 'http_pass',
                              'value'       => isset ($hab) && isset ($hab->http_pass) ? Crypt::decrypt($hab->http_pass) : null,
                              'default'     => '',
                              'placeholder' => "HTTP basic auth password for the web admin",
                              'required'    => false,
                            ])
                          </div>
                      </div>
                    </div>

              </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="wifi"></i> Network</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.select', [
                    'label'       => "Which router is it plugged into?",
                    'name'        => 'router_id',
                    'options'     => $routers->pluck ('label', '_id')->all(),
                    'value'       => $hab->router_id ?? null,
                    'default'     => '',
                    'placeholder' => "Choose a router to attach the HAB to",
                    'required'    => false,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's it's network hostname?",
                    'name'        => 'host',
                    'value'       => $hab->host ?? null,
                    'default'     => 'smartrooms-hab',
                    'placeholder' => "Network hostname of the hab",
                    'required'    => true,
                    'help'        => "hostname",
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's it's internal DHCP address from the router?",
                    'name'        => 'int_ip',
                    'value'       => $hab->int_ip ?? null,
                    'default'     => '192.168.88.248',
                    'placeholder' => "IP address of the hab",
                    'required'    => true,
                    'help'        => "hostname -I | awk '{print $1}'",
                  ])
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "What's the main SSH login username?",
                            'name'        => 'ssh_user',
                            'value'       => $hab->ssh_user ?? null,
                            'default'     => 'openhabian',
                            'placeholder' => "Username to log into the HAB with",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "What's the SSH password?",
                            'name'        => 'ssh_pass',
                            'value'       => isset ($hab) && isset ($hab->ssh_pass) ? Crypt::decrypt($hab->ssh_pass) : null,
                            'default'     => 'openhabian',
                            'placeholder' => "Password to log into the HAB with",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                  </div>


              </div>
        </div>
      </div> <!-- end col -->


      @include ('partials.components.forms.vpn', [
        'entity'  => $hab ?? null,
        'vpns'    => $vpns,
      ])


    </div> <!-- end row -->

    <div class="row">

      @include ('partials.components.forms.ordering', [
        'entity'  => $hab ?? null,
        'users'   => $users,
      ])

      @include ('partials.components.forms.installation', [
        'entity'  => $hab ?? null,
        'users'   => $users,
      ])

      @include ('partials.components.forms.provisioning', [
        'entity'  => $hab ?? null,
        'users'   => $users,
      ])


    </div> <!-- end row -->


  <hr />
  <div class="row mb-4">
    @if ( !isset ($hab) )
      @include ('partials.components.forms.clone')
    @endif
    <div class="col-lg-12 col-md-12 text-right pr-4">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}


  @if ( isset ($hab) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $hab,
      'text'    => 'HAB',
      'destroy' => [
        'route' => 'os.habs.destroy',
        'params'=> [$hab->_id],
      ],
      'return'  => [
        'route' => 'os.habs.index',
        'params'=> [$hab->_id],
      ],
    ])
  @endif


@endsection
