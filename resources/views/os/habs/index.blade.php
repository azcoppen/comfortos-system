@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: OS</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Bus Automation</div>
          <div class="header-breadcrumb">
              <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
              <a href="{{ route('os.index') }}">Hardware</a>
              <a href="{{ route('os.habs.index') }}">Bus Automation</a>
              <a href="{{ route('os.habs.index') }}" class="text-primary">HABs</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.habs.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New HAB</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">Automation Buses</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'HABs'])
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
    <div class="row mt-5">
        @isset($records)
          @each('os.habs.each', $records, 'record')

          <hr />

          <div class="row">
            <div class="col-md-6 offset-3">
              {{ $records->links() }}
            </div>
          </div>
        @endisset

        @empty($records)
            @include ('partials.components.alerts.empty')
        @endempty
    </div>
@endsection
