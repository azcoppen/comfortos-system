@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Websocket Brokers</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Websocket Brokers</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">Brokers</a>
            <a href="{{ route('os.ws-brokers.index') }}" class="text-primary">Websocket Brokers</a>
            <a href="{{ route('os.ws-brokers.show', $ws_broker->_id) }}" class="text-primary">{{ $ws_broker->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.ws-brokers.edit', $ws_broker->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $ws_broker->label ?? $ws_broker->_id }}</h1>
  </div>
  <hr />

  <div class="row">
    <div class="col-sm-9">
      <div class="alert alert-bordered-secondary" role="alert">
    			<span class="alert-text">
<pre class="mb-0">
const centrifuge = new Centrifuge ("{{ $ws_broker->ws_port == 443 ? 'wss' : 'ws' }}://{{ $ws_broker->host }}{{ $ws_broker->ws_port == 443 ? '' : ':'.$ws_broker->ws_port  }}/{{ $ws_broker->conn_endpoint ?? 'None' }}");
centrifuge.setToken("$JWT_TOKEN").subscribe("chat", function(ctx) { console.info (ctx.data); }).connect();
</pre>
          </span>
    	</div>
    </div>
    <div class="col-sm-3 text-right">
      <a class="btn btn-lg btn-secondary" href="{{ route('os.ws-brokers.config', $ws_broker->_id) }}"><i data-feather="download"></i> Centrifugo conf</a>
      <a class="btn btn-lg btn-secondary" href="{{ route('os.ws-brokers.nginx', $ws_broker->_id) }}"><i data-feather="download"></i> Nginx conf</a>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="alert alert-bordered-secondary" role="alert">
    			<span class="alert-text">
<pre class="mb-0">
curl -X POST {{ $ws_broker->ws_port == 443 ? 'https' : 'http' }}://{{ $ws_broker->host }}{{ $ws_broker->admin_port == 443 ? '' : ':'.$ws_broker->admin_port  }}/{{ $ws_broker->api_endpoint ?? 'None' }}
-H "Authorization: Bearer {{ $ws_broker->api_key ?? 'None' }}" -H "Content-Type: application/json" -H "Accept: application/json"
-d {"method":"publish","params":{"channel":"chat","data":{"text":"hello"}}}
</pre>

          </span>
    	</div>
    </div>
  </div>

    <div class="row">

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $ws_broker->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Label</td>
                                <td class="border-0">{{ $ws_broker->label }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Type</td>
                                <td class="border-0">{{ $ws_broker->type }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Version</td>
                                <td class="border-0">{{ $ws_broker->v }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Region</td>
                                <td class="border-0">{{ $ws_broker->region }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Server Context</td>
                                <td class="border-0">{{ $ws_broker->context }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Connection</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Host</td>
                                <td class="border-0">{{ $ws_broker->host }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Websocket Port</td>
                                <td class="border-0">{{ $ws_broker->ws_port }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Admin Port</td>
                                <td class="border-0">{{ $ws_broker->admin_port ?? 'None' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Connection URI</td>
                                <td class="border-0">{{ $ws_broker->conn_endpoint ?? 'None' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">API URI</td>
                                <td class="border-0">{{ $ws_broker->api_endpoint ?? 'None' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Auth Type</td>
                                <td class="border-0">{{ $ws_broker->auth_type ?? 'None' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Authentication</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Admin Pass</td>
                                <td class="border-0">{{ $ws_broker->admin_pass ?? 'None' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Admin Secret</td>
                                <td class="border-0">{{ $ws_broker->admin_secret ?? 'None' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">HMAC Secret</td>
                                <td class="border-0">{{ $ws_broker->hmac_secret ?? 'None' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">API Key</td>
                                <td class="border-0">
                                  {{ $ws_broker->api_key ?? 'None' }}
                                  <br />
                                  <small class="text-muted">This is NOT the websocket JWT token.</small>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    @include ('os.mqtt.brokers.components_table', [
      'title' => 'Properties',
      'components' => $ws_broker->properties,
      'route' => 'explorer.operators.brands.index',
    ])


    @include ('partials.components.panels.device_business_flow', ['component' => $ws_broker])


@stop
