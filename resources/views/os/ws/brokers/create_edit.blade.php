@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Websocket Brokers</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">OS</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">Brokers</a>
            <a href="{{ route('os.ws-brokers.index') }}" class="{{ isset ($ws_broker) ? '' : 'text-primary' }}">Websocket Brokers</a>

            @if ( isset($ws_broker) )
            <a href="{{ route('os.ws-brokers.show', $ws_broker->_id) }}" class="text-primary">{{ $ws_broker->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.ws-brokers.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <h1 class="display-4">{{ isset ($ws_broker) ? 'Update '.$ws_broker->label : 'Add A New Websocket Broker' }}</h1>
  <hr />
  @if ( isset ($ws_broker) )
    {!! Form::open (['method' => 'PUT', 'route' => ['os.ws-brokers.update', $ws_broker->_id]]) !!}
  @else
    {!! Form::open (['route' => 'os.ws-brokers.store']) !!}
  @endif

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $ws_broker->label ?? null,
                  'default'     => '',
                  'placeholder' => "Websocket broker",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which type of MQTT broker is it?",
                  'name'        => 'type',
                  'options'     => ['centrifugo' => 'centrifugo',],
                  'value'       => $ws_broker->type ?? null,
                  'default'     => 'centrifugo',
                  'placeholder' => "Choose its type",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which version is it?",
                  'name'        => 'v',
                  'value'       => $ws_broker->v ?? null,
                  'default'     => '',
                  'placeholder' => "2.0",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which region is it in?",
                  'name'        => 'region',
                  'value'       => $ws_broker->region ?? null,
                  'default'     => '',
                  'placeholder' => "us-west",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "What server context is it?",
                  'name'        => 'context',
                  'options'     => ['local' => 'local', 'remote' => 'remote', ],
                  'value'       => $ws_broker->context ?? null,
                  'default'     => 'local',
                  'placeholder' => "Choose its context",
                  'required'    => true,
                ])
              </div>

            </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="settings"></i> Configuration</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the hostname?",
                      'name'        => 'host',
                      'value'       => $ws_broker->host ?? null,
                      'default'     => '',
                      'placeholder' => "localhost",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.select', [
                      'label'       => "What authentication type is it?",
                      'name'        => 'auth_type',
                      'options'     => ['jwt' => 'jwt',],
                      'value'       => $ws_broker->auth_type ?? null,
                      'default'     => 'jwt',
                      'placeholder' => "Choose its auth type",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.number', [
                              'label'       => "What is the Websocket port?",
                              'name'        => 'ws_port',
                              'value'       => $ws_broker->ws_port ?? null,
                              'default'     => '8000',
                              'placeholder' => "8000",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.number', [
                              'label'       => "What is the admin ui port?",
                              'name'        => 'admin_port',
                              'value'       => $ws_broker->admin_port ?? null,
                              'default'     => '8000',
                              'placeholder' => "8000",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the HMAC secret?",
                      'name'        => 'hmac_secret',
                      'value'       => $ws_broker->hmac_secret ?? null,
                      'default'     => '',
                      'placeholder' => "secret",
                      'required'    => false,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the admin password?",
                      'name'        => 'admin_pass',
                      'value'       => $ws_broker->admin_pass ?? null,
                      'default'     => '',
                      'placeholder' => "secret",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the admin secret?",
                      'name'        => 'admin_secret',
                      'value'       => $ws_broker->admin_secret ?? null,
                      'default'     => '',
                      'placeholder' => "secret",
                      'required'    => false,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the server API key?",
                      'name'        => 'api_key',
                      'value'       => $ws_broker->api_key ?? null,
                      'default'     => '',
                      'placeholder' => "centrifugo_api_key",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the API endpoint?",
                      'name'        => 'api_endpoint',
                      'value'       => $ws_broker->api_endpoint ?? null,
                      'default'     => 'api',
                      'placeholder' => "api",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the connection endpoint?",
                      'name'        => 'conn_endpoint',
                      'value'       => $ws_broker->conn_endpoint ?? null,
                      'default'     => 'connection/websocket',
                      'placeholder' => "connection/websocket",
                      'required'    => true,
                    ])
                  </div>

                </div>
              </div>
            </div> <!-- end col -->

            @include ('partials.components.forms.vpn', [
              'entity'         => $ws_broker ?? null,
              'vpn_servers'    => $vpn_servers,
            ])

      </div> <!-- end row -->


  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $ws_broker ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $ws_broker ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $ws_broker ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->


    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}


    @if ( isset ($ws_broker) )
      @include ('partials.components.panels.delete_bar', [
        'entity'  => $ws_broker,
        'text'    => 'MQTT Broker',
        'destroy' => [
          'route' => 'os.mqtt-brokers.destroy',
          'params'=> [$ws_broker->_id],
        ],
        'return'  => [
          'route' => 'os.mqtt-brokers.index',
          'params'=> [$ws_broker->_id],
        ],
      ])
    @endif


@endsection
