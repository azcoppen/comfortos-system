@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Websocket Brokers</title>
@stop

@section('app_title')
  <h4>OS</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">OS</div>
          <div class="header-breadcrumb">
            <a href="{{ route('os.index') }}"><i data-feather="server"></i> OS</a>
            <a href="{{ route('os.index') }}">Brokers</a>
            <a href="{{ route('os.ws-brokers.index') }}" class="text-primary">Websocket Brokers</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('os.ws-brokers.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Websocket Broker</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">Websocket Brokers</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'Websocket Brokers'])
@stop

@section('sidebar')
  @include ('os.sidebar')
@endsection

@section('content')
  <div class="row mt-5">
      @isset($records)
        @each('os.ws.brokers.each', $records, 'record')

        <hr />

        <div class="row">
          <div class="col-md-6 offset-3">
            {{ $records->links() }}
          </div>
        </div>
      @endisset

      @empty($records)
          @include ('partials.components.alerts.empty')
      @endempty
  </div>
@endsection
