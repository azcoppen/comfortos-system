
<ul class="nav">
    <li class="nav-header">Flight Control</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#dashboards" aria-expanded="false" aria-controls="dashboards">
            <i data-feather="briefcase" class="menu-icon"></i>
            <span class="menu-title">Dashboards</span>
        </a>
        <div class="collapse show" id="dashboards">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'room-generator' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.generator.room') }}"><span class="menu-title">Room Code Generator</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'cpt-generator' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.generator.cpt') }}"><span class="menu-title">CPT Code Generator</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'signals' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.signals') }}"><span class="menu-title">Live Signals</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'commands' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.commands') }}"><span class="menu-title">Live Commands</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'mia' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.mia') }}"><span class="menu-title">MIA Alerts</span></a></li>
            </ul>
        </div>
    </li>

    <li class="nav-header">Speed Dial</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#shortcuts" aria-expanded="false" aria-controls="shortcuts">
            <i data-feather="target" class="menu-icon"></i>
            <span class="menu-title">Shortcuts</span>
        </a>
        <div class="collapse show" id="shortcuts">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href="{{ route ('explorer.operators.brands.properties.buildings.index', ['example-hotels-group', 'example-hotels', '223-south-hamilton', 'main-building']) }}"><span class="menu-title">223 South Hamilton Rooms</span></a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.show', ['example-hotels-group', 'example-hotels', '223-south-hamilton', 'main-building', '5fb2250a9ba3874e9767b656']) }}"><span class="menu-title">Room 100: Devices</span></a></li>
            </ul>
        </div>
    </li>

    <li class="nav-header">Tools</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#tools" aria-expanded="false" aria-controls="tools">
            <i data-feather="target" class="menu-icon"></i>
            <span class="menu-title">Commands</span>
        </a>
        <div class="collapse show" id="tools">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'mqtt' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.tools.mqtt_publisher') }}"><span class="menu-title">MQTT Publisher</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'http' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.tools.http_poster') }}"><span class="menu-title">HTTP Poster</span></a></li>
            </ul>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#checklists" aria-expanded="false" aria-controls="checklists">
            <i data-feather="align-left" class="menu-icon"></i>
            <span class="menu-title">Checklists</span>
        </a>
        <div class="collapse show" id="checklists">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'router' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.checklists.index.type', 'router') }}"><span class="menu-title">Router Setup Checklist</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'hab' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.checklists.index.type', 'hab') }}"><span class="menu-title">HAB Setup Checklist</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'pbx' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.checklists.index.type', 'pbx') }}"><span class="menu-title">PBX Setup Checklist</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'dect' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.checklists.index.type', 'dect') }}"><span class="menu-title">DECT Setup Checklist</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'handset' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('dashboards.checklists.index.type', 'handset') }}"><span class="menu-title">Handset Setup Checklist</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'mirror' ? 'mirror' : '' }}"><a class="nav-link" href="{{ route ('dashboards.checklists.index.type', 'mirror') }}"><span class="menu-title">Mirror Setup Checklist</span></a></li>
            </ul>
        </div>
    </li>

</ul>
