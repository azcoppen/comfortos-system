@extends ('guest.component.themes.blixy.dark_layout')

@push ('head')
  <style>
  .switch {width: 240px; height: 136px; }
  .slider { background-color: #0C1734; }
  .slider:before { background-color: #1B2944; height:104px; width: 104px; left: 16px; bottom: 16px; }
  input:checked + .slider:before {
    -webkit-transform: translateX(104px);
    -ms-transform: translateX(104px);
    transform: translateX(104px);
  }
  </style>
@endpush

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
        <a href="{{ route ('guest.component.'.Str::lower(Str::singular($component->plural)).'.logout', $component->_id) }}" class="btn btn-lg btn-primary d-block mr-2 float-right col-2"><i data-feather="log-out"></i></a>
        <div class="page-title col-10"><i data-feather="unlock" class="text-muted"></i> @lang('guest/locks.title')</div>

      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$component])
      </div>
  </div>

  @include ('partials.alerts')

  <div class="row">
    @if ($component && is_object ($component) && $component->active)

      <div class="col-12 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">{{ Str::limit ( $component->label ?? __('guest/locks.lock_placeholder'), 40) }}</h5>
              </div>
              <div class="card-body">
                <div class="row mt-4">
                  <div class="col-sm-4 col-xs-12 pt-4">
                    <div class="col-3 text-center pt-1">
                      @livewire ('guest.lock-switch', ['component' => $component])
                    </div>
                  </div>
                </div>
              </div>
          </div>
      </div>

    @else
      <div class="col-12">
        @include ('partials.components.alerts.error', ['msg' => __('guest/auth.unavailable')])
      </div>
    @endif

  </div>

@stop
