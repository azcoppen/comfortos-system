@extends ('guest.component.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
        <a href="{{ route ('guest.component.'.Str::lower(Str::singular($component->plural)).'.logout', $component->_id) }}" class="btn btn-lg btn-primary d-block mr-2 float-right col-2"><i data-feather="log-out"></i></a>
        <div class="page-title col-10"><i data-feather="zap" class="text-muted"></i> @lang('guest/power.title')</div>

      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$component])
      </div>
  </div>

  @include ('partials.alerts')

  <div class="row">
    @if ($component && is_object ($component) && $component->active)

      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 card-margin">
          <div class="card">
              <div class="card-header">
                <h5 class="card-title col-9">{{ Str::limit ($component->label ?? __('guest/power.plug_placeholder'), 22) }}</h5>
                <div class="col-3 text-right pt-1">
                  @livewire ('guest.power-switch', ['component' => $component])
                </div>
              </div>
              <div class="card-body text-center">
                @livewire ('guest.level-slider', ['component' => $component])
              </div>
          </div>
      </div>

    @else
      <div class="col-12">
        @include ('partials.components.alerts.error', ['msg' => __('guest/auth.unavailable')])
      </div>
    @endif

  </div>

@stop
