@extends ('guest.component.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
        <a href="{{ route ('guest.component.'.Str::lower(Str::singular($component->plural)).'.logout', $component->_id) }}" class="btn btn-lg btn-primary d-block mr-2 float-right col-2"><i data-feather="log-out"></i></a>
        <div class="page-title col-10"><i data-feather="monitor" class="text-muted"></i> @lang('guest/tv.title')</div>

      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$component])
      </div>
  </div>

  @include ('partials.alerts')

  <div class="row">
    @if ($component && is_object ($component) && $component->active)

      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card-margin">
          <div class="card">
              <div class="card-header">
                <h5 class="card-title">{{ Str::limit ($component->label ?? __('guest/tv.chromecast_placeholder'), 22) }}</h5>
              </div>
              <div class="card-body text-center">
                <div class="row mb-4">
                  <div class="col-12 p-0 pr-1 pt-1 text-center m-1">
                    @livewire ('guest.player-buttons', ['component' => $component])
                    @livewire ('guest.stop-button', ['component' => $component])
                    @livewire ('guest.mute-switch', ['component' => $component])
                  </div>
                </div>
                @livewire ('guest.volume-slider', ['component' => $component])
              </div>
          </div>
      </div>

    @else
      <div class="col-12">
        @include ('partials.components.alerts.error', ['msg' => __('guest/auth.unavailable')])
      </div>
    @endif

  </div>

@stop
