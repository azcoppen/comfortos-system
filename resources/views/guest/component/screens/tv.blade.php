@extends ('guest.component.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
        <a href="{{ route ('guest.component.'.Str::lower(Str::singular($component->plural)).'.logout', $component->_id) }}" class="btn btn-lg btn-primary d-block mr-2 float-right col-2"><i data-feather="log-out"></i></a>
        <div class="page-title col-10"><i data-feather="monitor" class="text-muted"></i> @lang('guest/tv.title')</div>

      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$component])
      </div>
  </div>

  @include ('partials.alerts')

  <div class="row">
    @if ($component && is_object ($component) && $component->active)

      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card-margin">
          <div class="card">
              <div class="card-header">
                <h5 class="card-title col-9">{{ Str::limit ($component->label ?? __('guest/tv.tv_placeholder'), 22) }}</h5>
                <div class="col-3 text-right pt-1">
                  @livewire ('guest.power-switch', ['component' => $component])
                </div>
              </div>
              <div class="card-body text-center">
                @livewire ('guest.t-v-channel-entry', ['component' => $component])
                <div class="row">
                  <div class="col-5">
                    @foreach (range (1,9) AS $kc)
                      @livewire ('guest.t-v-button', ['component' => $component, 'keycode' => $kc])
                    @endforeach
                    @livewire ('guest.t-v-button', ['component' => $component, 'keycode' => 0])
                  </div>
                  <div class="col-7">
                    @livewire ('guest.t-v-dir-buttons', ['component' => $component])
                  </div>
                </div>

                @livewire ('guest.volume-slider', ['component' => $component])
              </div>
          </div>
      </div>

    @else
      <div class="col-12">
        @include ('partials.components.alerts.error', ['msg' => __('guest/auth.unavailable')])
      </div>
    @endif

  </div>

@stop
