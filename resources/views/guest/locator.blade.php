<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>SmartRooms: Guest Access</title>
    <link rel="stylesheet" href="@cdn('dist/css/vendor.styles.css')" />
    <link rel="stylesheet" href="@cdn('dist/css/demo/dark-template.css')" />
    <link rel="shortcut icon" href="@cdn('favicon.ico')" />
		<link rel="apple-touch-icon" sizes="120x120" href="@cdn('apple-touch-icon.png')" />
		<link rel="icon" type="image/png" sizes="32x32" href="@cdn('favicon-32x32.png')" />
		<link rel="icon" type="image/png" sizes="16x16" href="@cdn('favicon-16x16.png')" />
		<link rel="manifest" href="@cdn('site.webmanifest')" />
		<link rel="mask-icon" href="@cdn('safari-pinned-tab.svg')" color="#5bbad5" />
		<meta name="msapplication-TileColor" content="#0C1734" />
		<meta name="theme-color" content="#0C1734" />
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
</head>

<body bgcolor="#0C1734">
  <div class="main-container">
      <div class="container-fluid page-body-wrapper full-page-wrapper">

        <div class="user-auth-v2">
            <div class="row no-gutters">
                <div class="col-12 auth-header" style="padding: 1rem !important;">
                    <div class="logo-container text-center">
                            <h1 class="display-2"><img src="@cdn('favicon-32x32.png')" style="max-height: 3rem; max-width: 3rem;" class="m-0 p-0" /> comfort<span class="text-primary">os</span></h1>
                    </div>

                </div>
            </div>
            <div class="row p-1">

              <div id='map' style='width: 100%; height: 200px;'></div>

              @if (isset($rooms) && count ($rooms))


              <h1 class="mt-3 mb-3 col-12 text-center">{{ __('guest/locator.title') }}</h1>

              @foreach ($rooms AS $room)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 m-3 card-margin">
                    <div class="card">
                        <div class="card-header text-center">
                          {{ __('guest/locator.room') }}{{ $room->number }}<span class="text-muted">, {{ $room->building->property->label }}</span>
                        </div>
                        <div class="card-body text-center">
                          <h2>
                            <a style="color: #FFF; text-decoration: none;" href="{{ route ('guest.room.login', $room->_id) }}">
                            {{ $room->label }}
                            </a>
                          </h2>

                          @if ( Cache::get ($room->_id.'-session-end') )
                            @if ( time () > Cache::get ($room->_id.'-session-end')->timestamp )
                              @if (! session ('override') )
                                <p class="text-danger"><i data-feather="times-circle"></i> {{ __('guest/locator.locked') }}</p>
                              @endif
                            @else
                              <p class="text-success"><i data-feather="check-circle"></i> {{ __('guest/locator.available') }}</p>
                            @endif
                          @else
                            <p class="text-success"><i data-feather="check-circle"></i> {{ __('guest/locator.available') }}</p>
                          @endif

                        </div> <!-- end card body -->
                    </div>
                </div>
              @endforeach
              @else
                <h2 class="col-12 m-4 text-center">{{ __('guest/locator.empty') }}</h2>
                <p class="col-11 p-3 ml-4 mr-2 text-center">{{ __('guest/locator.empty_extra') }}</p>
              @endif

            </div>
        </div>




      </div>
  </div>

  <!-- inject:js -->
  <script src="@cdn('dist/js/vendor.base.js')"></script>
  <script src="@cdn('dist/js/vendor.bundle.js')"></script>
  <script> feather.replace() </script>
  <script>
    mapboxgl.accessToken = '{{ env('MAPBOX_KEY') }}';
    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [{{ $longitude ?? 0 }}, {{ $latitude ?? 0 }}], // starting position
      zoom: 14 // starting zoom
    });
    map.addControl(new mapboxgl.NavigationControl());
    new mapboxgl.Marker({
      color: "red",
      draggable: false
    }).setLngLat([{{ $longitude ?? 0 }}, {{ $latitude ?? 0 }}]).addTo(map);
  </script>
</body>

</html>
