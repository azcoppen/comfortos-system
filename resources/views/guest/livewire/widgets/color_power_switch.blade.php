<div wire:poll>
  <label class="switch mr-1">
    <input wire:click="command($event.target.checked ? 'on' : 'off')" id="power-{{ $component->_id }}" type="checkbox" class="success" {{ isset ($last_value) && is_array ($last_value) && isset ($last_value[2]) && $last_value[2] > 0 ? 'checked': '' }} value="on" />
    <span class="slider"></span>
  </label>
</div>
