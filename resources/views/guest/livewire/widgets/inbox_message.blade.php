<div class="email-header">
    <div class="email-date">{{ now()->format('m d Y H:iA') }}</div>
    <div class="email-subject">@lang ('guest/inbox.placeholder.subject')</div>
    <p class="recipient"><span>@lang('guest/inbox.from'):</span> @lang ('guest/inbox.placeholder.sender') </p>
</div>
<div class="email-body">
  @lang ('guest/inbox.placeholder.content')
</div>
