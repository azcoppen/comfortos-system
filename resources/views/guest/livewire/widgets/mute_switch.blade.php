<div wire:poll class="d-inline">
  <button type="button" wire:click="command ('{!! isset ($last_value) && $last_value == 'ON' ? 'off' : 'on' !!}')" id="player-{{ $component->_id }}-next" class="btn btn-lg {!! isset ($last_value) && $last_value == 'ON' ? 'btn-warning': 'btn-flash-border-dark text-light' !!}">
    <i class="fa fa-volume-mute {!! isset ($last_value) && $last_value == 'ON' ? 'text-light': 'text-warning' !!}"></i>
  </button>
</div>
