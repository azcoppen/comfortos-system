<ul class="contact-list">
  @foreach (Lang::get ('guest/inbox.contacts') AS $name => $desc)
    <li class="contact-list-item">
        <a href="#">
            <div class="user">
                <p class="user-name">{{ $name }}</p>
                <p class="user-designation">{{ $desc }}</p>
            </div>
        </a>
    </li>
  @endforeach
</ul>
