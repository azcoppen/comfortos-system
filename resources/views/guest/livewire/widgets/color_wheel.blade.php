<div wire:ignore class="row mb-4">
  <div class="col-12 text-center pt-3">
    <div id="hsb-picker-{{ $component->_id }}" class="iro-picker m-auto"></div>
  </div>
</div>

@push ('js')
  <script>

  document.addEventListener("DOMContentLoaded", () => {
    window.picker_{{ $component->_id }} = new iro.ColorPicker('#hsb-picker-{{ $component->_id }}', {
      width: 180,
      layoutDirection: "horizontal",
      margin: 10,
      color: {
        h: {{ $last_value[0] ?? 0 }},
        s: {{ $last_value[1] ?? 0 }},
        v: {{ $last_value[2] ?? 100 }},
      }
    });

    /*
    @json ($last_value)
    */

    window.picker_{{ $component->_id }}.on('input:change', function(color) {
      window.livewire.emit('{{ $component->_id }}_command', color.hsv);
    })

    window.addEventListener('{{ $component->_id }}_command.sent', event => {
      if ( event.detail.attribute == 'power' )
      {
        switch (event.detail.value)
        {
          case 'on':
            window.picker_{{ $component->_id }}.color.setChannel ('hsv', 'v', 100);
          break;

          case 'off':
            window.picker_{{ $component->_id }}.color.setChannel ('hsv', 'v', 0);
          break;
        }
      }
    })

  });


  </script>
@endpush
