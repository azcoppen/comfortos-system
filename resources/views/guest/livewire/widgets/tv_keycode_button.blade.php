<div class="d-inline">
  <button type="button" wire:click="command ('{{ $keycode }}')" id="player-{{ $component->_id }}-stop" class="mr-1 btn btn-lg {!! isset ($last_value) && $last_value == 'ON' ? 'btn-danger': 'btn-flash-border-dark text-light' !!}">
  {{ $keycode }}
  </button>
</div>
