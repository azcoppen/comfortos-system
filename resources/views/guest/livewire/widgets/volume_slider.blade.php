<div wire:poll class="row mt-3 p-3">
  <div class="col-1 p-0 pt-2 pl-1 m-0 text-right">
    <i class="fa fa-volume-mute"></i>
  </div>
  <div class="col-10 m-0 p-0 pl-3 pr-3 text-center">
    <input wire:change="command ($event.target.value)" type="range" id="volume-{{ $component->_id }}" name="volume-{{ $component->_id }}" min="0" max="100" value="{{ $last_value ?? 50 }}" class="form-control" oninput="document.getElementById('{{ $component->_id }}_volume_pc_val_text').innerHTML = this.value">
  </div>
  <div class="col-1 p-0 pt-2 pr-1 m-0 text-left">
    <i class="fa fa-volume-up"></i>
  </div>
  <div class="col-12 text-center mt-2">
    <span class="text-muted">@lang('guest/widgets.volume') @ </span> <span class="text-primary"><span id="{{ $component->_id }}_volume_pc_val_text">{{ $last_value ?? 50 }}</span>%</span>
  </div>
</div>
