<div {{ isset ($code) && $code->readonly ? 'wire:ignore' : ''}} class="d-inline" code-index="{{ $code->index ?? 0 }}">
  <div class="form-row mb-1">
    <div class="col-4">

      <div class="input-group">
          <input id="code-{{ $component->_id }}-date-{{ $index }}" type="text" class="form-control lock-dt-picker {{ isset ($code) && is_object ($code) && isset ($code->readonly) ? 'text-muted' : ''  }}" placeholder="m/d/Y" {{ isset ($code) && is_object ($code) && isset ($code->readonly) ? 'disabled readonly' : ''  }} value="{{ isset ($code) && is_object ($code) ? $code->expire_at->timezone($component->room->building->property->timezone)->format('m/d/Y') : now()->addHours(24)->timezone($component->room->building->property->timezone)->format('m/d/Y') }}">
      </div>
    </div>
    <div class="col-2">
      <div class="input-group">
          <input id="code-{{ $component->_id }}-time-{{ $index }}" type="text" class="form-control lock-time-picker {{ isset ($code) && is_object ($code) && isset ($code->readonly) ? 'text-muted' : ''  }}" placeholder="H:i" {{ isset ($code) && is_object ($code) && isset ($code->readonly) ? 'disabled readonly' : ''  }} value="{{ isset ($code) && is_object ($code) ? $code->expire_at->timezone($component->room->building->property->timezone)->format('H:i') : now()->addHours(24)->timezone($component->room->building->property->timezone)->format('H:i') }}">
      </div>
    </div>
    <div class="col-4">
      <div class="input-group">
        <span class="pt-2 mr-1 text-muted">{{ $code->index ?? 0 }}</span>
          <input id="code-{{ $component->_id }}-digits-{{ $index }}" type="number" class="form-control {{ isset ($code) && is_object ($code) ? 'text-muted' : ''  }}" placeholder="{{ isset ($code) && is_object ($code) ? $code->hint : '' }}" min="10000", max="100000000" pattern="[0-9]+" {{ isset ($code) && is_object ($code) ? 'disabled readonly' : ''  }}  value="">
          <span wire:loading class="spinner-grow spinner-grow-sm text-muted mt-2 ml-2" role="status">
              <span class="sr-only">Loading...</span>
          </span>
      </div>
    </div>
    <div class="col-1 pt-1">
      @if ( isset ($code) && is_object ($code)  )
        @if (! isset ($code->readonly) )
          <button wire:click="update ({{ $index }}, '{{ $code->_id }}', document.getElementById('code-{{ $component->_id }}-date-{{ $index }}').value, document.getElementById('code-{{ $component->_id }}-time-{{ $index }}').value)" class="btn btn-sm text-success" type="button"><i class="fa fa-save"></i></button>
        @endif
      @else
        <button wire:click="insert ({{ $index }}, document.getElementById('code-{{ $component->_id }}-date-{{ $index }}').value, document.getElementById('code-{{ $component->_id }}-time-{{ $index }}').value, document.getElementById('code-{{ $component->_id }}-digits-{{ $index }}').value)" class="btn btn-sm text-success" type="button"><i class="fa fa-save"></i></button>
      @endif

    </div>
    <div class="col-1 pt-1">
      @if ( isset ($code) && is_object ($code) )
        @if ( isset ($code->readonly) )
        @else
          <button wire:click="remove ({{ $index }}, '{{ $code->_id }}')" class="btn btn-sm text-danger" type="button"><i class="fa fa-times"></i></button>
        @endif
      @else

      @endif
    </div>
  </div>
</div>

@push ('js')
  <script>

  document.addEventListener("DOMContentLoaded", () => {

    $('#code-{{ $component->_id }}-date-{{ $index }}').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '{{ now()->timezone($component->room->building->property->timezone)->format('m/d/Y') }}',
        todayHighlight: true,
        autoclose: true,
        weekStart: 1,
        clearBtn: true,
        language: '{{ App::getLocale() }}',
    });

    $('#code-{{ $component->_id }}-time-{{ $index }}').timepicker();

  });
  </script>
@endpush
