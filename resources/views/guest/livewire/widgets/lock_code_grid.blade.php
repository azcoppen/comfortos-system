<div>
  <div class="form-row mb-1">
    <div class="col-4 pl-3 text-muted">
      @lang ('guest/locks.expiry')
    </div>
    <div class="col-2">

    </div>
    <div class="col-4 pl-3 text-muted">
      @lang ('guest/locks.code')
    </div>
  </div>

  @livewire ('guest.manage-lock-code', ['component' => $lock, 'index' => 0, 'code' => $codes->get(0) ?? null], Str::random (12))
  @livewire ('guest.manage-lock-code', ['component' => $lock, 'index' => 1, 'code' => $codes->get(1) ?? null], Str::random (12))
  @livewire ('guest.manage-lock-code', ['component' => $lock, 'index' => 2, 'code' => $codes->get(2) ?? null], Str::random (12))
  @livewire ('guest.manage-lock-code', ['component' => $lock, 'index' => 3, 'code' => $codes->get(3) ?? null], Str::random (12))
  @livewire ('guest.manage-lock-code', ['component' => $lock, 'index' => 4, 'code' => $codes->get(4) ?? null], Str::random (12))
  @livewire ('guest.manage-lock-code', ['component' => $lock, 'index' => 5, 'code' => $codes->get(5) ?? null], Str::random (12))

</div>
