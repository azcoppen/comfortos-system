<ul class="email-list">
    <li class="email-list-item">
        <div class="recipient">
            <a href="#" class="recipient-name">@lang ('guest/inbox.placeholder.sender')</a>
        </div>
        <a href="#" class="email-subject">@lang ('guest/inbox.placeholder.subject') <i class="unread">&nbsp;</i></a>
    </li>

</ul>
