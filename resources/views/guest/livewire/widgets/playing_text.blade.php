<div wire:poll class="row mb-3">
  <div class="col-12 text-center">
    <span class="text-muted">@lang('guest/speakers.playing'):</span> {{ $last_value ?? __('guest/speakers.nothing') }}
  </div>
</div>
