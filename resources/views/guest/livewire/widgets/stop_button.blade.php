<div wire:poll class="d-inline">
  <button type="button" wire:click="command ('{!! isset ($last_value) && $last_value == 'ON' ? 'off' : 'on' !!}')" id="player-{{ $component->_id }}-stop" class="mr-1 btn btn-lg {!! isset ($last_value) && $last_value == 'ON' ? 'btn-danger': 'btn-flash-border-dark text-light' !!}">
    <i class="fa fa-times {!! isset ($last_value) && $last_value == 'ON' ? 'text-light': 'text-danger' !!}"></i>
  </button>
</div>
