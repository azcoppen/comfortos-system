<div wire:poll.30s class="col-sm-6 col-xs-12">

  <div class="card card-margin">
      <div class="card-body">

        <div class="row">

          <div class="col-8">

            <div class="row">
              <div class="col-2 p-0 pt-2 text-center">
                <i data-feather="calendar" class="text-muted"></i>
                <div wire:loading class="spinner-grow spinner-grow-sm" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
              </div>
              <div class="col-5 p-0 text-center">
                <span class="wt-day">{{ now()->timezone ($room->building->property->timezone)->day }}</span>
              </div>
              <div class="col-5 p-0 text-left">

                  <div class="col-12 live-day d-block wt-dt-text">
                  {{ strtoupper(now()->timezone ($room->building->property->timezone)->locale(App::getLocale())->shortMonthName) }}
                  </div>

                  <div id="live-time" class="col-12 live-time d-block wt-dt-text">
                    {{ now()->timezone ($room->building->property->timezone)->format ('H:i') }}
                  </div>

              </div>
            </div>

          </div>

          <div class="col-4">
            @if ( isset ($weather) && $weather )

              <div class="row">
                <div class="col-12">
                  <span class="">{{ $weather['location']['name'] ?? 'Unknown' }}</span>
                </div>
              </div>
              <div class="row">
                <div class="col-2 p-0">
                  <img src="{{ $weather['condition']['icon'] ?? 'Unknown' }}">
                </div>
                <div class="col-10 pt-2 pl-4">
                  <span class="wt-dt-text">{{ $weather['forecast']['temp'] ?? 'Unknown' }}&deg;</span>
                  <span class="text-muted d-block mb-4">{{ $weather['condition']['name'] ?? 'Unknown' }}</span>
                </div>
              </div>

              <div class="row">
                <div class="col-6 p-0 pr-2 text-right text-muted">
                  {{ $weather['forecast']['pressure'] ?? 'Unknown' }}
                </div>
                <div class="col-6 p-0 pl-2 text-muted">
                  {{ $weather['forecast']['humidity'] ?? 'Unknown' }}%
                </div>
              </div>

            @endif
          </div>

        </div>

      </div>
    </div>
</div>


@push ('js')
  <script>
  document.addEventListener("DOMContentLoaded", () => {
    window.time_update = setInterval (function () {
      if (document.getElementById ('live-time'))
      {
        document.getElementById ('live-time').innerHTML = moment().format('HH:mm');
      }

    }, 1000);
  });
  </script>
@endpush
