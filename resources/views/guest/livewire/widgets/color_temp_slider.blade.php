<div wire:poll class="row p-3">
  <div class="col-1 p-0 pt-2 pl-1 m-0 text-right">
    <small class="badge badge-light p-1">&nbsp;</small>
  </div>
  <div class="col-10 m-0 p-0 pl-3 pr-3 text-center">
    <input wire:change="command ($event.target.value)" type="range" id="temperature-{{ $component->_id }}" name="temperature-{{ $component->_id }}" min="0" max="100" value="{{ $last_value ?? 50 }}" class="form-control">
  </div>
  <div class="col-1 p-0 pt-2 pr-1 m-0 text-left">
    <small class="badge badge-warning p-1">&nbsp;</small>
  </div>
</div>
