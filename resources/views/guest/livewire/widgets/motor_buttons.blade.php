<div wire:poll class="row mb-4">
  <div class="col-12 pt-3 text-center">
    <button type="button" wire:click="command ('up')" id="dir-{{ $component->_id }}-up" class="mr-1 btn btn-lg btn-flash-border-dark text-{{ isset ($last_value) && $last_value == 'Forward' ? 'text-success' : 'text-light' }}">
      <i class="fa fa-arrow-up"></i>
    </button>
  </div>

  <div class="col-12 text-center">

    <button type="button" wire:click="command ('left')" id="dir-{{ $component->_id }}-left" class="mr-1 btn btn-lg btn-flash-border-dark text-{{ isset ($last_value) && $last_value == 'Left' ? 'text-success' : 'text-light' }}">
      <i class="fa fa-arrow-left"></i>
    </button>

    <button type="button" wire:click="command ('stop')" id="dir-{{ $component->_id }}-stop" class="mr-1 btn btn-lg btn-flash-border-dark text-{{ isset ($last_value) && $last_value == 'Stop' ? 'text-success' : 'text-danger' }}">
      <i class="fa fa-stop-circle text-danger"></i>
    </button>

    <button type="button" wire:click="command ('right')" id="dir-{{ $component->_id }}-right" class="mr-1 btn btn-lg btn-flash-border-dark text-{{ isset ($last_value) && $last_value == 'Right' ? 'text-success' : 'text-light' }}">
      <i class="fa fa-arrow-right"></i>
    </button>

  </div>

  <div class="col-12 text-center">
    <button type="button" wire:click="command ('down')" id="dir-{{ $component->_id }}-down" class="mr-1 btn btn-lg btn-flash-border-dark text-{{ isset ($last_value) && $last_value == 'Reverse' ? 'text-success' : 'text-light' }}">
      <i class="fa fa-arrow-down"></i>
    </button>
  </div>

</div>
