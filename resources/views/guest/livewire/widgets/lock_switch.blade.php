<div wire:poll>
  <label class="switch mr-1 text-center">
    <input wire:click="command($event.target.checked ? 'lock' : 'unlock')" id="lock-{{ $component->_id }}" {{ isset ($last_value) && $last_value == 'ON' ? 'checked': '' }} type="checkbox" class="success" value="on" />
    <span class="slider"></span>
  </label>
</div>
