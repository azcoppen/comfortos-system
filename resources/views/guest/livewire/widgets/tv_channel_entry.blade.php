<div class="row mb-4">
  <div class="col-12 text-center">
    <div class="input-group">
        <input id="channel-{{ $component->_id }}" name="channel-{{ $component->_id }}" type="text" class="form-control" placeholder="@lang ('guest/tv.channel_placeholder')" value="{{ $last_value ?? '' }}">
        <div class="input-group-append">
            <button wire:click="command (document.getElementById('channel-{{ $component->_id }}').value)" class="btn btn-sm btn-success" type="button">@lang ('guest/tv.channel_enter')</button>
        </div>
    </div>
  </div>
</div>
