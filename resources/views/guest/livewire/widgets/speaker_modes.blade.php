<div wire:poll class="row mb-4">
  <div class="col-12 pt-3 text-center">
    <select wire:change="command ($event.target.value)" id="mode-{{ $component->_id }}" name="mode-{{ $component->_id }}" class="form-control">
      @if ( isset ($component->options['modes']) && is_array ($component->options['modes']) && count ($component->options['modes']) )
        <option value="" selected>@lang ('guest/speakers.mode_placeholder')</option>
        @foreach ( $component->options['modes'] AS $label => $val )
          <option value="{{ $val ?? 0 }}" {{ isset ($last_value) && $last_value == $val ? 'selected' : '' }}>{{ $label ?? 'unknown' }}</option>
        @endforeach
      @endif
    </select>
  </div>
</div>

@push('js')
  <script>
  document.addEventListener("DOMContentLoaded", () => {

    window.addEventListener('mode.rendering', event => {
      if ( $('#mode-'+event.detail.component) )
      {
        $('#mode-'+event.detail.component).val( event.detail.value);
      }
    })

    window.addEventListener('mode.updated', event => {
      if ( $('#mode-'+event.detail.component) )
      {
        $('#mode-'+event.detail.component).val(event.detail.value);
      }
    })

  });
  </script>
@endpush
