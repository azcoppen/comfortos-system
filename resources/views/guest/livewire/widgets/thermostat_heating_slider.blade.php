<div wire:poll class="row pl-3 pr-3 pt-3">
  <div class="col-1 p-0 pt-2 pl-1 m-0 text-right">
    <i class="fa fa-thermometer-quarter text-base"></i>
  </div>
  <div class="col-10 m-0 p-0 pl-3 pr-3 text-center">
    <input wire:change="command ($event.target.value)" type="range" id="heating_point-{{ $component->_id }}" name="heating_point-{{ $component->_id }}" min="50" max="110" value="{{ $last_value ?? 60 }}" class="form-control" oninput="document.getElementById('{{ $component->_id }}_heating_point_val_text').innerHTML = this.value">
  </div>
  <div class="col-1 p-0 pt-2 pr-1 m-0 text-left">
    <i class="fa fa-thermometer-full text-danger"></i>
  </div>
  <div class="col-12 text-center mt-2">
    <span class="text-muted">@lang('guest/widgets.heating') @ </span> <span class="text-primary"><span id="{{ $component->_id }}_heating_point_val_text">{{ $last_value ?? 60 }}</span>&deg; F</span>
  </div>
</div>
