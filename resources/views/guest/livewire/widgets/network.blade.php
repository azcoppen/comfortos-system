  <div class="col-sm-6 col-xs-12 card-margin">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title pl-0 col-10"><span class="text-muted"><i class="fa fa-lock"></i></span>&nbsp;
                  <span class="{{ $network->type == 'virtual' && !$network->confirmed_at ? 'text-warning' : '' }}">
                  {{ Str::limit($network->label ?? __('guest/wifi.wifi_placeholder'), 40) }}

                  @if ( $network->type == 'virtual' && !$network->confirmed_at )
                  <span class="spinner-grow spinner-grow-sm ml-2" role="status">
                      <span class="sr-only">Loading...</span>
                  </span>
                  @endif

                  </span>
                </h5>
                <div class="col-2 text-right pr-0">
                  @if ( $network->type != 'hardware' )
                    <a class="btn btn-sm btn-danger" href="javascript:;" wire:click="remove ('{{ $network->_id }}')">
                      <i class="fa fa-times"></i>
                    </a>
                  @endif
                </div>
            </div>
            <div class="card-body">

                <h1 class="display-3 mt-2">{{ $network->ssid }}</h1>
                <p class="lead"><span class="text-success">{{ $network->password }}</span></p>
                <p class="lead"><span class="text-muted">Expires {{ $network->expire_at->diffForHumans() }}</span></p>

            </div>
        </div>
    </div>
