<div wire:poll.30s class="pl-2 {{ isset ($inline) && $inline != FALSE ? 'd-inline mr-3' : '' }}">
@if (cache ($model->_id.'-session-end') && is_object (cache ($model->_id.'-session-end')) )
  @php ( $diff = cache ($model->_id.'-session-end')->diff (now()) )
  <small class="text-{{ $diff->h < 1 ? 'danger' : 'muted' }}">
  @if ($diff->d > 0)
    {{ $diff->format (__('guest/start.session_days')) }}
  @else
    @if ($diff->h > 0)
      {{ $diff->format (__('guest/start.session_hours')) }}
    @else
      {{ $diff->format (__('guest/start.session_mins')) }}
    @endif
  @endif
  </small>
@endif
</div>
