<div wire:poll class="row p-3">
  <div class="col-1 p-0 pt-2 pl-1 m-0 text-right">
    <i class="fa fa-times text-danger"></i>
  </div>
  <div class="col-10 m-0 p-0 pl-3 pr-3 text-center">
    <input wire:change="command ($event.target.value)" type="range" id="level-{{ $component->_id }}" name="level-{{ $component->_id }}" min="0" max="100" value="{{ $last_value ?? 50 }}" class="form-control" oninput="document.getElementById('{{ $component->_id }}_level_val_text').innerHTML = this.value">
  </div>
  <div class="col-1 p-0 pt-2 pr-1 m-0 text-left">
    <i class="fa fa-signal text-success"></i>
  </div>
  <div class="col-12 text-center mt-2">
    <span class="text-muted">@lang('guest/widgets.level') @ </span> <span class="text-primary"><span id="{{ $component->_id }}_level_val_text">{{ $last_value ?? 50 }}</span>%</span>
  </div>
</div>
