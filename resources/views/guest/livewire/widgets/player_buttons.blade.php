<div class="d-inline">
  <button type="button" wire:click="command ('previous')" id="player-{{ $component->_id }}-previous" class="mr-1 btn btn-lg btn-flash-border-dark text-light">
    <i class="fa fa-step-backward"></i>
  </button>

  <button type="button" wire:click="command ('play')" id="player-{{ $component->_id }}-play" class="mr-1 btn btn-lg btn-flash-border-dark text-light">
    <i class="fa fa-play text-success"></i>
  </button>

  <button type="button" wire:click="command ('pause')" id="player-{{ $component->_id }}-pause" class="mr-1 btn btn-lg btn-flash-border-dark text-light">
    <i class="fa fa-pause text-success"></i>
  </button>

  <button type="button" wire:click="command ('next')" id="player-{{ $component->_id }}-next" class="mr-1 btn btn-lg btn-flash-border-dark text-light">
    <i class="fa fa-step-forward"></i>
  </button>
</div>
