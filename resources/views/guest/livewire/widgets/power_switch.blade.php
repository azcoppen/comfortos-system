<div wire:poll>
  <label class="switch mr-1">
    <input wire:click="command($event.target.checked ? 'on' : 'off')" id="power-{{ $component->_id }}" type="checkbox" class="success" {{ isset ($last_value) && $last_value == 'ON' ? 'checked': '' }} value="on" />
    <span class="slider"></span>
  </label>
</div>
