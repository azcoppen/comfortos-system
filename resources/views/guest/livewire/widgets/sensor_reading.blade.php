  <div wire:poll.{{ $poll ?? 10 }}s wire:ignore class="col-md-3 col-sm-4 col-xs-6">
    <div class="card card-margin">
        <div class="card-body">
          <span wire:loading class="spinner-grow spinner-grow-sm float-right text-muted" role="status">
              <span class="sr-only">Loading...</span>
          </span>
            <div class="widget-31">
                <div class="widget-31-icon">
                    <i data-feather="{{ config ('comfortos.capability_icons.'.$type) }}"></i>
                </div>
                <div class="widget-31-data">
                    <span class="title">@lang ('guest/sensors.'.$type)</span>
                    <h5 class="figure">
                      <span id="{{ $type }}-value" class="reading-value">{!! $last_value !!}</span>
                      <small id="{{ $type }}-suffix" class="text-muted">{!! $suffix !!}</small>
                    </h5>
                </div>
            </div>
        </div>
    </div>
  </div>
