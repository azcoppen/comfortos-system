@extends ('guest.room.themes.blixy.dark_layout')

@section ('content')

  <div class="row mb-4">
    <div class="col-10">
      <h1 class="display-3">@lang ('guest/start.title')</h1>
      @livewire ('guest.session-remaining', [$room])
    </div>
    <div class="col-2 text-right pr-1">
      <a href="{{ route ('guest.room.logout', $room->_id) }}" class="btn btn-lg btn-primary d-block mr-2 pr-2 "><i data-feather="log-out"></i></a>

    </div>
  </div>

  @include ('partials.alerts')

  <div class="row mb-4">
    <div class="col-12">
      <h1 class="display-4 text-muted">{{ Str::limit ($room->label ?? $room->number, 20) }} @ {{ Str::limit ($room->building->property->label, 20) }}</h1>
    </div>
  </div>

  <div class="row">

    @livewire ('guest.time-weather', [$room])
    @livewire ('guest.sensor-reading', [$room, 'contact'])
    @livewire ('guest.sensor-reading', [$room, 'motion'])
    @livewire ('guest.sensor-reading', [$room, 'temperature'])
    @livewire ('guest.sensor-reading', [$room, 'humidity'])
    @livewire ('guest.sensor-reading', [$room, 'luminance'])
    @livewire ('guest.sensor-reading', [$room, 'uv'])
    @livewire ('guest.sensor-reading', [$room, 'flood'])
    @livewire ('guest.sensor-reading', [$room, 'smoke'])
    @livewire ('guest.sensor-reading', [$room, 'co'])

  </div>
@stop

@push ('js')
  <script>
  document.addEventListener("DOMContentLoaded", () => {

    window.addEventListener('sensor.rendering', event => {
      var reading_val_el = document.getElementById (event.detail.type+'-value');
      var reading_suffix_el = document.getElementById (event.detail.type+'-suffix');

      if ( reading_val_el )
      {
        reading_val_el.innerHTML = event.detail.last_value;
      }

      if ( reading_suffix_el )
      {
        reading_suffix_el.innerHTML = event.detail.suffix;
      }

      feather.replace();
    });

  });
  </script>
