@extends ('guest.room.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title"><i data-feather="zap" class="text-muted"></i> @lang('guest/power.title')</div>
          <div class="header-breadcrumb">
              <a href="#"> @lang('guest/power.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$room])
      </div>
  </div>

  @include ('partials.alerts')

  @if ( isset ($room->plugs) && count ($room->plugs->where ('active', true)) )
    <div class="row">
      @foreach ($room->plugs->where ('active', true) AS $plug)
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 card-margin">
            <div class="card">
                <div class="card-header">
                  <h5 class="card-title col-9">{{ Str::limit ($plug->label ?? __('guest/power.plug_placeholder'), 22) }}</h5>
                  <div class="col-3 text-right pt-1">
                    @livewire ('guest.power-switch', ['component' => $plug])
                  </div>
                </div>
                <div class="card-body text-center">
                  @livewire ('guest.level-slider', ['component' => $plug])
                </div>
            </div>
        </div>
      @endforeach
    </div>
  @else
    @include ('guest.room.themes.blixy.dark_layout', ['msg' => __('guest/plugs.empty')])
  @endif

@stop
