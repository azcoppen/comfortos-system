@extends ('guest.room.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container mb-3">
      <div class="page-header-main">
          <div class="page-title"><i data-feather="phone" class="text-muted"></i> @lang('guest/phone.title')</div>
          <div class="header-breadcrumb">
              <a href="#"> @lang('guest/phone.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$room])
      </div>
  </div>

  @include ('partials.alerts')

  @if (is_object ($room->extensions) && count ($room->extensions->where ('active', true)))
    @php ($extension = $room->extensions->where ('active', true)->first())
    <div class="row mt-0 ">
      <div class="col-4"><h1 class="display-4"><span class="text-muted d-none d-none d-md-inline-block d-lg-inline-block">ext. </span>{{ $extension->sip_id }}</h1></div>
      <div class="col-8"><h1 class="display-4 text-muted text-right">{{ $extension->did }}</h1></div>
    </div>
  @endif

  <div class="row mt-1">
    <div class="col-sm-4 col-xs-12">

      <div class="card">
          <div class="card-header">
            <h5 class="card-title">Directory</h5>
          </div>
          <div class="card-body">
            <ul class="list-unstyled">
              @foreach (Lang::get('guest/phone.contacts') AS $ext => $label)
                <li class="row mb-2">
                    <span class="col-8"><a onclick="clear_dial_entry (); append_dial_entry ('{{ $ext }}')" href="javascript:;"><i class="fa fa-phone-square mr-3 text-muted"></i><span class="text-light">{{ $label }}</span></a></span>
                    <span class="col-4 text-right"><a href=""><span class="text-light">{{ $ext }}</span></a></span>
                </li>
              @endforeach

            </ul>
          </div>
      </div>
    </div>

    <div class="col-sm-8 col-xs-12">

      <div class="row mt-2">

        <div class="col-10">

          <div class="input-group offset-1">
              <input id="dial-entry" type="tel" class="form-control form-control-lg text-center ml-2 p-0" placeholder="000-000-0000" style="font-size: 1.5rem;" pattern="[0-9-+\s()]*$" >
              <div class="input-group-append">
                  <span class="input-group-text" id="backspace" onclick="document.getElementById ('dial-entry').setAttribute ('value', document.getElementById ('dial-entry').getAttribute ('value').slice(0, -1))"><i data-feather="arrow-left"></i></span>
              </div>
          </div>

        </div>


        <div class="col-12 mt-3">

          <div class="row">
                <a onclick="append_dial_entry ('1')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-3 pr-3 offset-2"><h1 class="display-4 text-center mb-0">1</h1><small class="text-primary">VM</small></a>
                <a onclick="append_dial_entry ('2')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-3 pr-3"><h1 class="display-4 text-center mb-0">2</h1><small class="text-muted">ABC</small></a>
                <a onclick="append_dial_entry ('3')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-3 pr-3"><h1 class="display-4 text-center mb-0">3</h1><small class="text-muted">DEF</small></a>
          </div>

          <div class="row mt-4">
                <a onclick="append_dial_entry ('4')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-3 pr-3 offset-2"><h1 class="display-4 text-center mb-0">4</h1><small class="text-muted">GHI</small></a>
                <a onclick="append_dial_entry ('5') "href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-3 pr-3"><h1 class="display-4 text-center mb-0">5</h1><small class="text-muted">JKL</small></a>
                <a onclick="append_dial_entry ('6')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-3 pr-3"><h1 class="display-4 text-center mb-0">6</h1><small class="text-muted">MNO</small></a>
          </div>

          <div class="row mt-4">
                <a onclick="append_dial_entry ('7')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-3 pr-3 offset-2"><h1 class="display-4 text-center mb-0">7</h1><small class="text-muted">PQRS</small></a>
                <a onclick="append_dial_entry ('8')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-3 pr-3"><h1 class="display-4 text-center mb-0">8</h1><small class="text-muted">TUV</small></a>
                <a onclick="append_dial_entry ('9')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-2 pr-3"><h1 class="display-4 text-center mb-0 pl-2">9</h1><small class="text-muted pl-1">WXYZ</small></a>
          </div>

          <div class="row mt-4">
              <a onclick="append_dial_entry ('*')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-3 pl-3 pr-3 offset-2"><h1 class="display-4 text-center mb-0">*</h1></a>
              <a onclick="append_dial_entry ('0')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-2 pl-3 pr-3"><h1 class="display-4 text-center mb-0">0</h1><small class="text-muted">+</small></a>
              <a onclick="append_dial_entry ('#')" href="javascript:;" class="col-2 btn btn-lg btn-flash-border-dark text-light text-center mr-4 pt-3 pl-3 pr-3"><h1 class="display-4 text-center mb-0">#</h1></a>
          </div>

          <div class="row mt-4">
            <button class="col-7 offset-2 btn btn-success text-center mr-4 pt-3 pl-0 pr-3"><h1 class="mt-0 btn-rounded text-center text-light"><i class="fa fa-phone" style="font-size: 2rem;"></i></h1></button>
          </div>

        </div>
      </div>



    </div>

  </div>

@stop

@push ('js')
  <script>

  (function($) {
      'use strict';

  })(jQuery);

  document.addEventListener('keyup', (e) => {
    var allowed = new Array ('Digit1','Digit2','Digit3','Digit4','Digit5','Digit6','Digit7','Digit8','Digit9','Digit0');
    if ( allowed.indexOf (e.code) != -1 )
    {
      append_dial_entry (e.code.slice(-1));
    }

    if (e.code == 'Backspace')
    {
      document.getElementById ('dial-entry').setAttribute ('value', document.getElementById ('dial-entry').getAttribute ('value').slice(0, -1))
    }
  });

  function clear_dial_entry ()
  {
    document.getElementById ('dial-entry').setAttribute ('value', '');
    return;
  }

  function append_dial_entry ( char )
  {
    var last = document.getElementById ('dial-entry').getAttribute('value');
    if (! last )
    {
      document.getElementById ('dial-entry').setAttribute ('value', char);
      return;
    }
    document.getElementById ('dial-entry').setAttribute ('value', last+char);
  }
  </script>
@endpush
