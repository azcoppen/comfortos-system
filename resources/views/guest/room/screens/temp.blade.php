@extends ('guest.room.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title"><i data-feather="thermometer" class="text-muted"></i> @lang('guest/temp.title')</div>
          <div class="header-breadcrumb">
              <a href="#"> @lang('guest/temp.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$room])
      </div>
  </div>

  @include ('partials.alerts')

  @if ( isset ($room->thermostats) && count ($room->thermostats->where ('active', true)) )
    <div class="row">
      @foreach ($room->thermostats->where ('active', true) AS $thermostat)
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card-margin">
            <div class="card">
                <div class="card-header">
                  <h5 class="card-title">{{ Str::limit ($thermostat->label ?? __('guest/temp.thermostat_placeholder'), 40) }}</h5>
                </div>
                <div class="card-body text-center">
                  @livewire ('guest.thermostat-mode', ['component' => $thermostat])
                  @livewire ('guest.thermostat-heating-slider', ['component' => $thermostat])
                  @livewire ('guest.thermostat-cooling-slider', ['component' => $thermostat])
                </div>
            </div>
        </div>
      @endforeach
    </div>
  @else
    @include ('guest.room.themes.blixy.dark_layout', ['msg' => __('guest/temp.empty')])
  @endif

@stop
