@extends ('guest.room.themes.blixy.dark_layout')

@push ('head')
  <style>
    .datepicker {
      z-index: 1600 !important; /* has to be larger than 1050 */
    }
</style>
@endpush

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title"><i data-feather="wifi" class="text-muted"></i> @lang('guest/wifi.title')</div>
          <div class="header-breadcrumb">
              <a href="#">@lang('guest/wifi.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action text-right">
        @livewire ('guest.session-remaining', [$room, true])
        <a href="#" data-toggle="modal" data-target="#create" class="btn btn-success"><i data-feather="plus-square"></i></a>
      </div>

  </div>

  @include ('partials.alerts')

    @livewire ('guest.wifi-networks', [$room])

    <div class="modal fade" id="create" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
          {!! Form::open (['route' => ['guest.room.wifi.store', $room->_id]]) !!}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-1" id="create-title"><span class="text-muted"><i class="fa fa-plus-square"></i></span> &nbsp;&nbsp; @lang ('guest/wifi.create_title')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <div class="card">
                      <div class="card-body">


                          <div class="form-group">
                            <label class="ml-1">@lang('guest/wifi.store.label_title') <span class="text-danger">*</span></label>
                              <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                      <span class="input-group-text" id="label-text"><i class="fa fa-user"></i></span>
                                  </div>
                                  <input type="text" name="label" class="form-control" minlength="8" maxlength="20" placeholder="@lang('guest/wifi.store.label_placeholder')" required value="{{ old('label') }}" />
                                  @if ($errors->has('label'))
                                    <small class="invalid-feedback">{{ $errors->first('label') }}</small>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group">
                            <label class="ml-1">@lang('guest/wifi.store.ssid_title') <span class="text-danger">*</span></label>
                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="ssid-label"><i class="fa fa-wifi text-success"></i></span>
                                </div>
                                <input type="text" name="ssid" class="form-control" placeholder="@lang('guest/wifi.store.ssid_placeholder')" minlength="5" maxlength="20" pattern="[a-zA-Z0-9_-]+" required value="{{ old('ssid') }}" />
                              </div>
                              <span class="form-text text-muted">Only letters, numbers, hyphens, and underscores. No spaces.</span>
                              @if ($errors->has('ssid'))
                                <small class="invalid-feedback">{{ $errors->first('ssid') }}</small>
                              @endif
                          </div>

                          <div class="form-group">
                            <label class="ml-1">@lang('guest/wifi.store.password_title') <span class="text-danger">*</span></label>
                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="password-label"><i class="fa fa-lock text-warning"></i></span>
                                </div>
                                <input type="text" name="password" class="form-control" minlength="8" maxlength="20" placeholder="@lang('guest/wifi.store.password_placeholder')" required value="{{ old('password') }}" />
                              </div>
                              @if ($errors->has('password'))
                                <small class="invalid-feedback">{{ $errors->first('password') }}</small>
                              @endif
                              <span class="form-text text-muted">Must be at least 8 characters or easy to guess.</span>
                          </div>

                          <div class="form-row">
                              <div class="col-md-8">
                                  <div class="position-relative form-group">
                                    <label for="expire-date" class="">@lang('guest/wifi.store.expire_date_title') <span class="text-danger">*</span></label>
                                    <div class="input-group mb-3">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="expire-date-label"><i class="fa fa-calendar"></i></span>
                                      </div>
                                      <input name="expire_at_date" id="expire-date" type="text" class="form-control" placeholder="@lang('guest/wifi.store.expire_date_placeholder')" required value="{{ now()->timezone ($room->building->property->timezone)->format('m/d/Y') }}" value="{{ old('expire_at_date') }}">
                                      @if ($errors->has('expire_at_date'))
                                        <small class="invalid-feedback">{{ $errors->first('expire_at_date') }}</small>
                                      @endif
                                    </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="position-relative form-group">
                                    <label for="expire-time" class="pl-3">@lang('guest/wifi.store.expire_time_title') <span class="text-danger">*</span></label>
                                    <div class="input-group mb-3">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="expire-time-label"><i class="fa fa-clock"></i></span>
                                      </div>
                                      <input name="expire_at_time" id="expire-time" type="text" class="form-control" placeholder="@lang('guest/wifi.store.expire_time_placeholder')" required value="{{ now()->timezone ($room->building->property->timezone)->format('H:i') }}" value="{{ old('expire_at_time') }}" />
                                      @if ($errors->has('expire_at_time'))
                                        <small class="invalid-feedback">{{ $errors->first('expire_at_time') }}</small>
                                      @endif
                                    </div>
                                  </div>
                              </div>

                          </div>


                          </form>
                      </div>
                  </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('guest/wifi.store.cancel_btn')</button>
                    <button type="submit" class="btn btn-primary">@lang('guest/wifi.store.submit_btn')</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@stop

@push ('js')
  <script>

  $('#create').on('shown.bs.modal', function() {

    $('#expire-date').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '{{ now()->timezone($room->building->property->timezone)->format('m/d/Y') }}',
        todayHighlight: true,
        autoclose: true,
        weekStart: 1,
        clearBtn: true,
        language: '{{ App::getLocale() }}'
    });

    $('#expire-time').timepicker();
  });

  @if (count($errors) > 0)
      $('#create').modal('show');
  @endif

  </script>
@endpush
