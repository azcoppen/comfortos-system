@extends ('guest.room.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title"><i data-feather="monitor" class="text-muted"></i> @lang('guest/tv.title')</div>
          <div class="header-breadcrumb">
              <a href="#"> @lang('guest/tv.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$room])
      </div>
  </div>

  @include ('partials.alerts')

  <div class="row">
  @if ( (isset ($room->component_map->tvs) && count ($room->component_map->tvs)) || (isset ($room->component_map->chromecasts) && count ($room->component_map->chromecasts)) )
    @if ( isset ($room->tvs) && count ($room->tvs->where ('active', true)) )

        @foreach ($room->tvs->where ('active', true) AS $tv)
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card-margin">
              <div class="card">
                  <div class="card-header">
                    <h5 class="card-title col-9">{{ Str::limit ($tv->label ?? __('guest/tv.tv_placeholder'), 22) }}</h5>
                    <div class="col-3 text-right pt-1">
                      @livewire ('guest.power-switch', ['component' => $tv])
                    </div>
                  </div>
                  <div class="card-body text-center">
                    @livewire ('guest.t-v-channel-entry', ['component' => $tv])
                    <div class="row">
                      <div class="col-5">
                        @foreach (range (1,9) AS $kc)
                          @livewire ('guest.t-v-button', ['component' => $tv, 'keycode' => $kc])
                        @endforeach
                        @livewire ('guest.t-v-button', ['component' => $tv, 'keycode' => 0])
                      </div>
                      <div class="col-7">
                        @livewire ('guest.t-v-dir-buttons', ['component' => $tv])
                      </div>
                    </div>

                    @livewire ('guest.volume-slider', ['component' => $tv])
                  </div>
              </div>
          </div>
        @endforeach

    @endif

    @if ( isset ($room->chromecasts) && count ($room->chromecasts->where ('active', true)) )

        @foreach ($room->chromecasts->where ('active', true) AS $chromecast)
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card-margin">
              <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">{{ Str::limit ($chromecast->label ?? __('guest/tv.chromecast_placeholder'), 22) }}</h5>
                  </div>
                  <div class="card-body text-center">
                    <div class="row mb-4">
                      <div class="col-12 p-0 pr-1 pt-1 text-center m-1">
                        @livewire ('guest.player-buttons', ['component' => $chromecast])
                        @livewire ('guest.stop-button', ['component' => $chromecast])
                        @livewire ('guest.mute-switch', ['component' => $chromecast])
                      </div>
                    </div>
                    @livewire ('guest.volume-slider', ['component' => $chromecast])
                  </div>
              </div>
          </div>
        @endforeach

    @endif

  @else
    @include ('guest.room.themes.blixy.dark_layout', ['msg' => __('guest/tv.empty')])
  @endif
  </div>
@stop
