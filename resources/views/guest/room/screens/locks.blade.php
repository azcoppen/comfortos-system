@extends ('guest.room.themes.blixy.dark_layout')

@push ('head')
  <style>
  .switch {width: 240px; height: 136px; }
  .slider { background-color: #0C1734; }
  .slider:before { background-color: #1B2944; height:104px; width: 104px; left: 16px; bottom: 16px; }
  input:checked + .slider:before {
    -webkit-transform: translateX(104px);
    -ms-transform: translateX(104px);
    transform: translateX(104px);
  }
  </style>
@endpush

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title"><i data-feather="unlock" class="text-muted"></i> @lang('guest/locks.title')</div>
          <div class="header-breadcrumb">
              <a href="#"> @lang('guest/locks.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$room])
      </div>
  </div>

  @include ('partials.alerts')

  @if ( isset ($room->locks) && count ($room->locks->where ('active', true)) )
    <div class="row">
      @foreach ($room->locks->where ('active', true)->sortBy('label') AS $lock)
          <div class="col-12 card-margin">
              <div class="card">
                  <div class="card-header">
                      <h5 class="card-title">{{ Str::limit ( $lock->label ?? __('guest/locks.lock_placeholder'), 40) }}</h5>
                  </div>
                  <div class="card-body">
                    <div class="row mt-4">
                      <div class="col-sm-5 col-xs-12 pt-4">
                        <div class="col-3 text-center pt-1">
                          @livewire ('guest.lock-switch', ['component' => $lock])
                        </div>
                      </div>
                      <div class="col-sm-7 col-xs-12 pt-4">
                        @livewire ('guest.lock-code-grid', ['component' => $lock])
                      </div>
                    </div>
                  </div>
              </div>
          </div>
      @endforeach
    </div>
  @else
    @include ('guest.room.themes.blixy.dark_layout', ['msg' => __('guest/locks.empty')])
  @endif

@stop
