@extends ('guest.room.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title"><i data-feather="sun" class="text-muted"></i> @lang('guest/lighting.title')</div>
          <div class="header-breadcrumb">
              <a href="#"> @lang('guest/lighting.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$room])
      </div>
  </div>

  @include ('partials.alerts')
  <div class="row">

  @if ( (isset ($room->component_map->bulbs) && count ($room->component_map->bulbs)) || (isset ($room->component_map->leds) && count ($room->component_map->leds)) )
    @if ( isset ($room->bulbs) && count ($room->bulbs->where ('active', true)) )

        @foreach ($room->bulbs->where ('active', true)->sortBy('label') AS $bulb)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 card-margin">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title col-9">{{ Str::limit ($bulb->label ?? __('guest/lighting.bulb_placeholder'), 20) }}</h5>
                        <div class="col-3 text-right pt-1">
                          @livewire ('guest.color-power-switch', ['component' => $bulb])
                        </div>
                    </div>
                    <div class="card-body text-center">
                      @livewire ('guest.color-wheel', ['component' => $bulb])
                      @livewire ('guest.color-temp-slider', ['component' => $bulb])
                    </div> <!-- end card body -->
                </div>
            </div>
        @endforeach

    @endif



    @if ( isset ($room->leds) && count ($room->leds->where ('active', true)) )

        @foreach ($room->leds->where ('active', true)->sortBy('label') AS $led)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 card-margin">
                <div class="card">
                    <div class="card-header">
                      <h5 class="card-title col-9">{{ Str::limit ($led->label ?? __('guest/lighting.led_placeholder'), 20) }}</h5>
                      <div class="col-3 text-right pt-1">
                        @livewire ('guest.color-power-switch', ['component' => $led])
                      </div>
                    </div>
                    <div class="card-body text-center">
                        @livewire ('guest.color-wheel', ['component' => $led])
                    </div> <!-- end card body -->
                </div>
            </div>
        @endforeach

    @endif

  @else
    @include ('guest.room.themes.blixy.dark_layout', ['msg' => __('guest/lighting.empty')])
  @endif
  </div>

@stop
