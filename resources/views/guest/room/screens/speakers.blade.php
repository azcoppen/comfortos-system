@extends ('guest.room.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title"><i data-feather="speaker" class="text-muted"></i> @lang('guest/speakers.title')</div>
          <div class="header-breadcrumb">
              <a href="#"> @lang('guest/speakers.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$room])
      </div>
  </div>

  @include ('partials.alerts')

  @if ( isset ($room->speakers) && count ($room->speakers->where ('active', true)) )
    <div class="row">
      @foreach ($room->speakers->where ('active', true) AS $speaker)
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card-margin">
              <div class="card">
                  <div class="card-header">
                    <h5 class="card-title col-9">{{ Str::limit ($speaker->label ?? __('guest/speakers.speaker_placeholder'), 22) }}</h5>
                    <div class="col-3 text-right pt-1">
                      @livewire ('guest.power-switch', ['component' => $speaker])
                    </div>
                  </div>
                  <div class="card-body text-center">
                    @livewire ('guest.speaker-mode', ['component' => $speaker])
                    @livewire ('guest.playing-text', ['component' => $speaker])
                    <div class="row mb-4">
                      <div class="col-12 pt-3 text-center">
                        @livewire ('guest.player-buttons', ['component' => $speaker])
                        @livewire ('guest.mute-switch', ['component' => $speaker])
                      </div>
                    </div>
                    @livewire ('guest.volume-slider', ['component' => $speaker])
                  </div>
              </div>
          </div>
      @endforeach
    </div>
  @else
    @include ('guest.room.themes.blixy.dark_layout', ['msg' => __('guest/speakers.empty')])
  @endif

@stop
