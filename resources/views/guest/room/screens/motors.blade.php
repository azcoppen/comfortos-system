@extends ('guest.room.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title"><i data-feather="move" class="text-muted"></i> @lang('guest/motors.title')</div>
          <div class="header-breadcrumb">
              <a href="#"> @lang('guest/motors.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$room])
      </div>
  </div>

  @include ('partials.alerts')

  @if ( isset ($room->motors) && count ($room->motors->where ('active', true)) )
    <div class="row">
      @foreach ($room->motors->where ('active', true) AS $motor)
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 card-margin">
              <div class="card">
                  <div class="card-header">
                    <h5 class="card-title col-9">{{ Str::limit ($motor->label ?? __('guest/motors.motor_placeholder'), 22) }}</h5>
                    <div class="col-3 text-right pt-1">
                      @livewire ('guest.power-switch', ['component' => $motor])
                    </div>
                  </div>
                  <div class="card-body text-center">
                    @livewire ('guest.motor-buttons', ['component' => $motor])
                    @livewire ('guest.speed-slider', ['component' => $motor])
                  </div>
              </div>
          </div>
      @endforeach
    </div>
  @else
    @include ('guest.room.themes.blixy.dark_layout', ['msg' => __('guest/motors.empty')])
  @endif

@stop
