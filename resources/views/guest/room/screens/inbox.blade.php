@extends ('guest.room.themes.blixy.dark_layout')

@section ('content')

  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">@lang ('guest/inbox.title')</div>
          <div class="header-breadcrumb">
              <a href="#"><i data-feather="mail"></i> @lang ('guest/inbox.breadcrumb')</a>
          </div>
      </div>
      <div class="page-header-action">
        @livewire ('guest.session-remaining', [$room])
      </div>
  </div>

  @include ('partials.alerts')

  <div class="email-app">
      <div class="email-toolbars-wrapper card-margin">
          <div class="toolbar-header">
              <button type="button" class="btn btn-lg btn-block btn-compose-mail"> <i data-feather="plus"></i> @lang('guest/inbox.sidebar.compose_btn')</button>
          </div>
          <div class="toolbar-body">
              <div class="toolbar-title">@lang('guest/inbox.sidebar.folders')</div>
              <ul class="toolbar-menu">
                  <li class="active">
                      <i data-feather="mail"></i>
                      <a href="#">@lang('guest/inbox.sidebar.messages')</a>
                      <span class="badge badge-sb-base">8</span>
                  </li>
                  <li>
                      <i data-feather="voicemail"></i>
                      <a href="#">@lang('guest/inbox.sidebar.voicemails')</a>
                  </li>
                  <li>
                      <i data-feather="phone-incoming"></i>
                      <a href="#">@lang('guest/inbox.sidebar.faxes')</a>
                  </li>
              </ul>
              <div class="contact-header">
                  <div class="contact-left">
                      <h5 class="title">@lang('guest/inbox.sidebar.contacts')</h5>
                      <span class="badge badge-sb-success">10</span>
                  </div>
              </div>
              @livewire ('guest.inbox-contacts', [$room])
          </div>
      </div>
      <div class="email-list-wrapper card-margin">
          <div id="email-app-body" class="email-list-scroll-container">
            @livewire ('guest.inbox-list', [$room])
          </div>
      </div>
      <div class="email-desc-wrapper card-margin">
        @livewire ('guest.inbox-message', [$room])
      </div>
  </div>



@stop


@push ('js')
  <script>

  (function($) {
      'use strict';
      $(function () {
          //Scrollbar for Email app
          var ps = new PerfectScrollbar('#email-app-body', {
              wheelPropagation: false
          });
      });
  })(jQuery);

  </script>
@endpush
