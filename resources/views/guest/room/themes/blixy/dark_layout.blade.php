<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">

<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<title>SmartRooms ComfortOS </title>
    @yield('head')
    <link rel="stylesheet" href="@cdn('dist/css/vendor.styles.css')" />
		<link rel="stylesheet" href="@cdn('dist/css/demo/dark-template.css')" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" />
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-range-input@1.0.0/dist/css/bootstrap-range-input.min.css" />
		<link rel="stylesheet" href="@cdn('dist/css/toggle.css')" />
		<link rel="stylesheet" href="@cdn('dist/css/dark_tweaks.css')" />
		<link rel="shortcut icon" href="@cdn('favicon.ico')" />
		<link rel="apple-touch-icon" sizes="120x120" href="@cdn('apple-touch-icon.png')" />
		<link rel="icon" type="image/png" sizes="32x32" href="@cdn('favicon-32x32.png')" />
		<link rel="icon" type="image/png" sizes="16x16" href="@cdn('favicon-16x16.png')" />
		<link rel="manifest" href="@cdn('site.webmanifest')" />
		<link rel="mask-icon" href="@cdn('safari-pinned-tab.svg')" color="#5bbad5" />
		<meta name="msapplication-TileColor" content="#0C1734" />
		<meta name="theme-color" content="#0C1734" />
		<script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
		<link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
		<meta name="jwt" content="{{ auth()->check() ? auth('api')->setTTL(10080)->fromUser(auth()->user()) : ''}}">
		@livewireStyles
		@stack('head')

</head>

<body class="primary-only" bgcolor="#0C1734">

	<div class="main-container">
			<div class="container-fluid">

				@include ('guest.room.partials.languages_panel')

				@include ('guest.room.partials.settings_panel')

				<div class="sidebar-overlay"></div>

				@include ('guest.room.partials.top_menu')

				@include ('guest.room.partials.nav_menu')

				<div class="main-panel">
						<div class="content-wrapper">

							<div wire:offline>
							    @include ('partials.components.alerts.error', ['msg' => __('guest/auth.offline')])
							</div>

	            @yield ('content')
						</div>
				</div>
			</div>
	</div>

	@livewireScripts

	<script src="@cdn('dist/js/vendor.base.js')"></script>
	<script src="@cdn('dist/js/vendor.bundle.js')"></script>
	<script src="@cdn('dist/js/vendor-override/tooltip.js')"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment-with-locales.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/centrifugal/centrifuge-js@2.6.4/dist/centrifuge.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/@jaames/iro@5"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jssip/3.1.2/jssip.min.js"></script>
	<script src="@cdn('dist/js/components/dark-double-sidebar/common-msb.js')"></script>

	<script>
	window.chartColors = {
	    red: 'rgb(255, 99, 132)',
	    orange: 'rgb(255, 159, 64)',
	    yellow: 'rgb(255, 205, 86)',
	    green: 'rgb(75, 192, 192)',
	    blue: 'rgb(54, 162, 235)',
	    purple: 'rgb(153, 102, 255)',
	    grey: 'rgb(201, 203, 207)'
	};
	$.fn.timepicker.defaults = $.extend(true, {}, $.fn.timepicker.defaults, {
			icons: {
					up: 'menu-up',
					down: 'menu-down'
			}
	});
	</script>
	@stack('js')
</body>
</html>
