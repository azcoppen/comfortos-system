<div class="row">
  <div class="col-sm-12">

    <div class="alert alert-outline-warning" role="alert">
      <i data-feather="alert-triangle" class="alert-icon"></i>
      <span class="alert-text">{{ $msg ?? '' }}</span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i data-feather="x" class="alert-close"></i>
      </button>
    </div>
  </div>
</div>
