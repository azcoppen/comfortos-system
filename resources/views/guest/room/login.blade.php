<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>SmartRooms: Guest Access</title>
    <link rel="stylesheet" href="@cdn('dist/css/vendor.styles.css')" />
    <link rel="stylesheet" href="@cdn('dist/css/demo/dark-template.css')" />
    <link rel="shortcut icon" href="@cdn('favicon.ico')" />
		<link rel="apple-touch-icon" sizes="120x120" href="@cdn('apple-touch-icon.png')" />
		<link rel="icon" type="image/png" sizes="32x32" href="@cdn('favicon-32x32.png')" />
		<link rel="icon" type="image/png" sizes="16x16" href="@cdn('favicon-16x16.png')" />
		<link rel="manifest" href="@cdn('site.webmanifest')" />
		<link rel="mask-icon" href="@cdn('safari-pinned-tab.svg')" color="#5bbad5" />
		<meta name="msapplication-TileColor" content="#0C1734" />
		<meta name="theme-color" content="#0C1734" />
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
</head>

<body bgcolor="#0C1734">
    <div class="main-container">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="user-auth-v2">
                <div class="row no-gutters">
                    <div class="col-12 auth-header">
                        <div class="logo-container text-center">
                                <h1 class="display-2"><img src="@cdn('favicon-32x32.png')" style="max-height: 3rem; max-width: 3rem;" class="m-0 p-0" /> comfort<span class="text-primary">os</span></h1>
                        </div>

                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-11 col-sm-11 col-lg-10 col-xl-8 mx-auto">
                        <div class="card card-margin">
                            <div class="card-body p-0">
                                <div class="row no-gutters">
                                    <div class="col-lg-6">

                                        <div class="user-auth-content">

                                            {!! Form::open (['route' => ['guest.room.verify', $room->_id], 'class'=>'needs-validation', 'onSubmit' => "if ( document.getElementById('otp').value.length >= 10) { return true; } return false;"]) !!}
                                                <h1 class="display-4 text-left mb-4">{{ $room->label }}</h1>

                                                @if (! session()->has('warning') )
                                                  @if ( Cache::get ($room->_id.'-session-end') )
                                                    @include ('partials.components.alerts.warning', ['msg' => __('guest/auth.login.existing')])
                                                  @endif
                                                @endif

                                                @if (session()->has('error'))
                                                  @include ('partials.components.alerts.error')
                                                @endif

                                                @if (session()->has('warning'))
                                                  @include ('partials.components.alerts.warning')
                                                @endif

                                                @if ($errors && $errors->count() > 0)
                                                  @include ('partials.components.alerts.validation-error')
                                                @endif

                                                <div class="input-group input-group-lg">
                                                    {!! Form::text ('otp', old ('otp', $prefill ?? ''), ['id' => 'otp', 'class'=>'form-control', 'pattern' => '[A-Z0-9]+', 'minlength' => '10', 'maxlength' => '14', 'placeholder' => 'Enter your one-time passcode', 'required', 'autofocus',]) !!}
                                                    <i data-feather="unlock"></i>
                                                </div>

                                                <div class="mb-3 d-block">
                                                    <button type="submit" class="btn btn-base btn-lg btn-sbt">@lang ('guest/auth.login.labels.login_btn')</button>

                                                </div>
                                                <div class="form-register-link">
                                                    @lang ('guest/auth.login.labels.need_code')
                                                </div>
                                            {!! Form::close () !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-6 d-none d-md-block">
                                        <div class="auth-left-section">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End main-container -->

    <!-- inject:js -->
    <script src="@cdn('dist/js/vendor.base.js')"></script>
    <script src="@cdn('dist/js/vendor.bundle.js')"></script>
    <script src="@cdn('dist/js/components/dark-double-sidebar/common-msb.js')"></script>
    <script src="@cdn('dist/js/vendor-override/tooltip.js')"></script>
</body>

</html>
