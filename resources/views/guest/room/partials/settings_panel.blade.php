<div class="settings-overlay">
    <div class="settings-header">
        <h4>Settings</h4>
    </div>
    <i data-feather="arrow-left-circle" class="setting-close"></i>
    <div class="settings-body">
      <div class="config-container form">
        <div class="config-file">

          <h5 class="mb-3">{{ $room->label }} (Floor {{ $room->floor }}, #{{ $room->number }}), {{ $room->building->label }}</h5>
          <ul class="config-list">

            <li class="config-item form-group-xs row mb-2">
              <div class="col-md-12">
                {{ $room->_id ?? 'No room ID?' }}
              </div>
            </li>

            <li class="config-item form-group-xs row mb-2">
              <div class="col-md-12 text-muted">
                {{ $room->description ?? 'No description' }}
              </div>
            </li>

              <li class="config-item form-group-xs row mb-2">
                <div class="col-md-12">
                  {{ $room->building->property->label }} ({{ $room->building->property->brand->label }})<br />
                  {{ $room->building->property->location['street'] ?? 'No Street Address' }},
                  {{ $room->building->property->location['city'] ?? 'None' }}, {{ $room->building->property->location['region'] ?? 'None' }}, {{ $room->building->property->location['postal'] ?? 'None' }}, {{ $room->building->property->location['country'] ?? 'None' }}
                  &nbsp; <span class="text-muted">
                  [{{ collect($room->geo['coordinates'] ?? [0, 0])->implode (', ') }}]
                  </span>
                </div>
              </li>

              <li class="config-item form-group-xs row mb-2">
                <div class="col-md-12">
                  {{ $room->building->property->locale ?? 'none' }} <span class="text-muted">/</span> {{ $room->building->property->timezone ?? 'UTC' }} <span class="text-muted">/</span> {{ $room->building->property->currency ?? 'USD' }}
                </div>
              </li>

          </ul>
        </div> <!-- end config file -->

        <div class="separator"></div>

        <div class="config-file">

          <h5 class="mb-3">Users</h5>
          <ul class="config-list">
            @foreach ($room->users AS $room_user)

              <li class="config-item form-group-xs row mb-2">
                <div class="col-md-12">
                  {{ $room_user->display ?? 'Unknown' }}<br />
                  {{ $room_user->email ?? '' }}<br />
                  <span class="text-muted">{{ $room_user->_id ?? 'Unknown' }}</span>
                </div>
              </li>

            @endforeach
          </ul>

        </div>

        <div class="separator"></div>

        <div class="config-file">

          @php ($mqtt_broker = $room->building->property->mqtt_brokers->first()) @endphp

          <h5 class="mb-3">MQTT</h5>
          <ul class="config-list">
            @if ($mqtt_broker)
              <li class="config-item form-group-xs row mb-2">
                <div class="col-md-12">
                  {{ $mqtt_broker->label ?? 'Unknown' }} &nbsp;<span class="text-muted">{{ $mqtt_broker->type }}/{{ $mqtt_broker->context }}</span><br />
                  mqtt://{{ $mqtt_broker->host ?? '' }}:{{ $mqtt_broker->secure_port }}<br />
                </div>
              </li>
            @endif
          </ul>

        </div>

        <div class="separator"></div>

        <div class="config-file">

          @php ($ws_broker = $room->building->property->ws_brokers->first()) @endphp

          <h5 class="mb-3">API/WS</h5>
          <ul class="config-list">
            @if ($ws_broker)
              <li class="config-item form-group-xs row mb-2">
                <div class="col-md-12">
                  {{ $ws_broker->label ?? 'Unknown' }} &nbsp;<span class="text-muted">{{ $ws_broker->type }}/{{ $ws_broker->context }}</span><br />
                  wss://{{ $ws_broker->host ?? '' }}:{{ $ws_broker->ws_port }}
                </div>
              </li>
            @endif

          </ul>

        </div>

      </div> <!-- end config container -->
    </div> <!-- end settings body -->
</div> <!-- end settings overloy -->
