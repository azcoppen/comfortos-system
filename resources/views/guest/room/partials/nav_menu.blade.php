<nav class="navbar-container flex-row" id="navbar">
    <div class="primary">
        <div class="nav-top">
            <a class="lead text-light" href="{{ route ('guest.room.display', $room->_id) }}">
                <img src="@cdn('favicon-32x32.png')" style="max-height: 3rem; max-width: 3rem;" class="m-0 p-0" />
            </a>
        </div>
        <ul class="nav nav-middle">
          @if ( isset ($room->component_map) )
            @if ( isset ($room->component_map->locks) && count ($room->component_map->locks) )
              <li class="nav-item mb-3"><a class="nav-link {{ $app_section == 'locks' ? 'text-success' : '' }}" href="{{ route ('guest.room.locks', $room->_id) }}"><i data-feather="unlock"></i></a></li>
            @endif
              <li class="nav-item mb-3"><a class="nav-link {{ $app_section == 'inbox' ? 'text-success' : '' }}" href="{{ route ('guest.room.inbox', $room->_id) }}"><i data-feather="mail"></i></a></li>
            @if ( (isset ($room->component_map->bulbs) && count ($room->component_map->bulbs)) || (isset ($room->component_map->leds) && count ($room->component_map->leds)) )
              <li class="nav-item mb-3"><a class="nav-link {{ $app_section == 'lighting' ? 'text-success' : '' }}" href="{{ route ('guest.room.lighting', $room->_id) }}"><i data-feather="sun"></i></a></li>
            @endif
            @if ( isset ($room->component_map->extensions) && count ($room->component_map->extensions) )
              <li class="nav-item mb-3"><a class="nav-link {{ $app_section == 'phone' ? 'text-success' : '' }}" href="{{ route ('guest.room.phone', $room->_id) }}"><i data-feather="phone-call"></i></a></li>
            @endif
            @if ( (isset ($room->component_map->tvs) && count ($room->component_map->tvs)) || (isset ($room->component_map->chromecasts) && count ($room->component_map->chromecasts)) )
              <li class="nav-item mb-3"><a class="nav-link {{ $app_section == 'tv' ? 'text-success' : '' }}" href="{{ route ('guest.room.tv', $room->_id) }}"><i data-feather="monitor"></i></a></li>
            @endif
            @if ( isset ($room->component_map->speakers) && count ($room->component_map->speakers) )
              <li class="nav-item mb-3"><a class="nav-link {{ $app_section == 'speakers' ? 'text-success' : '' }}" href="{{ route ('guest.room.speakers', $room->_id) }}"><i data-feather="speaker"></i></a></li>
            @endif
            @if ( isset ($room->component_map->motors) && count ($room->component_map->motors) )
            <li class="nav-item mb-3"><a class="nav-link {{ $app_section == 'motors' ? 'text-success' : '' }}" href="{{ route ('guest.room.motors', $room->_id) }}"><i data-feather="move"></i></a></li>
            @endif
            @if ( isset ($room->component_map->routers) && count ($room->component_map->routers) )
            <li class="nav-item mb-3"><a class="nav-link {{ $app_section == 'wifi' ? 'text-success' : '' }}" href="{{ route ('guest.room.wifi.index', $room->_id) }}"><i data-feather="wifi"></i></a></li>
            @endif
            @if ( isset ($room->component_map->thermostats) && count ($room->component_map->thermostats) )
              <li class="nav-item mb-3"><a class="nav-link {{ $app_section == 'temp' ? 'text-success' : '' }}" href="{{ route ('guest.room.temp', $room->_id) }}"><i data-feather="thermometer"></i></a></li>
            @endif
            @if ( isset ($room->component_map->plugs) && count ($room->component_map->plugs) )
              <li class="nav-item"><a class="nav-link {{ $app_section == 'power' ? 'text-success' : '' }}" href="{{ route ('guest.room.power', $room->_id) }}"><i data-feather="zap"></i></a></li>
            @endif
          @endif
        </ul>
        <ul class="nav nav-bottom">
            <li class="nav-item">
              <a class="nav-link dropdown" href="javascript:;" id="lang"><i class="flag-icon flag-icon-us" title="us" id="us"></i></a></li>
            <li class="nav-item"><a class="nav-link" href="javascript:;" id="settings"><i data-feather="settings"></i></a></li>
        </ul>
    </div>
  </nav>
