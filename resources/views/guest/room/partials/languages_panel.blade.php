<div class="profile-overlay">
    <div class="profile-header p-3">
      <h4><i data-feather="arrow-left-circle" class="setting-close mr-5"></i> <span class="ml-5">@lang ('guest/languages.title')</span></h4>
    </div>

    <div class="profile-body">
      <div class="config-container form">
        <div class="config-file">

          <h5>@lang ('guest/languages.subtitle')</h5>
          <ul class="config-list">
            @if ( App::getLocale() == 'en' )
              <li class="config-item form-group-xs row"><label class="col-8 col-form-label"><a href="javascript:;" class="text-muted">@lang ('guest/languages.en')</a></label><div class="col-4 text-right"><a href="javascript:;" class=""><i class="config-icon flag-icon flag-icon-us"></i></a></div></li>
            @else
              <li class="config-item form-group-xs row"><label class="col-8 col-form-label"><a href="{{ route ('guest.lang', 'en') }}" class="text-light">@lang ('guest/languages.en')</a></label><div class="col-4 text-right"><a href="{{ route ('guest.lang', 'en') }}" class=""><i class="config-icon flag-icon flag-icon-us"></i></a></div></li>
            @endif

            @foreach (['fr', 'es', 'de', 'it', 'pt'] AS $lang_opt)
              <li class="config-item form-group-xs row"><label class="col-8 col-form-label"><a href="{{ App::getLocale() == $lang_opt ? 'javascript:;' : route ('guest.lang', $lang_opt) }}" class="text-{{ App::getLocale() == $lang_opt ? 'muted' : 'light' }}">@lang ('guest/languages.'.$lang_opt)</a></label><div class="col-4 text-right"><a href="{{ App::getLocale() == $lang_opt ? 'javascript:;' : route ('guest.lang', $lang_opt) }}" class=""><i class="config-icon flag-icon flag-icon-{{ $lang_opt }}"></i></a></div></li>
            @endforeach
          </ul>
        </div> <!-- end config file -->
      </div> <!-- end config container -->
    </div>
</div>
