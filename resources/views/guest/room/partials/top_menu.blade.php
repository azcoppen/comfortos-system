<nav class="navbar fixed-top">
    <div class="navbar-menu-container d-flex align-items-center justify-content-center">
        <div class="text-center navbar-brand-container align-items-center justify-content-center">
            <a class="lead text-light" href="{{ route ('guest.room.display', $room->_id) }}">
                <img src="@cdn('favicon-32x32.png')" style="max-height: 3rem; max-width: 3rem;" class="m-0 p-0" />
            </a>
        </div>
        <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item mobile-sidebar text-light">
                <button class="nav-link navbar-toggler navbar-toggler-right align-self-center mr-2" type="button" data-toggle="msb-sidebar"><i data-feather="menu"></i></button>
            </li>
        </ul>
    </div>
</nav>
