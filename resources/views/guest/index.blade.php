<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>SmartRooms: Guest Access</title>
    <link rel="stylesheet" href="@cdn('dist/css/vendor.styles.css')" />
    <link rel="stylesheet" href="@cdn('dist/css/demo/dark-template.css')" />
    <link rel="shortcut icon" href="@cdn('favicon.ico')" />
		<link rel="apple-touch-icon" sizes="120x120" href="@cdn('apple-touch-icon.png')" />
		<link rel="icon" type="image/png" sizes="32x32" href="@cdn('favicon-32x32.png')" />
		<link rel="icon" type="image/png" sizes="16x16" href="@cdn('favicon-16x16.png')" />
		<link rel="manifest" href="@cdn('site.webmanifest')" />
		<link rel="mask-icon" href="@cdn('safari-pinned-tab.svg')" color="#5bbad5" />
		<meta name="msapplication-TileColor" content="#0C1734" />
		<meta name="theme-color" content="#0C1734" />
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
</head>

<body bgcolor="#0C1734">
  <div class="main-container">
      <div class="container-fluid page-body-wrapper full-page-wrapper">

        <div class="user-auth-v2">
            <div class="row no-gutters">
                <div class="col-12 auth-header">
                    <div class="logo-container text-center">
                            <h1 class="display-2"><img src="@cdn('favicon-32x32.png')" style="max-height: 3rem; max-width: 3rem;" class="m-0 p-0" /> comfort<span class="text-primary">os</span></h1>
                    </div>

                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-11 col-sm-11 col-lg-10 col-xl-8 mx-auto">
                    <div class="card card-margin">
                        <div class="card-body p-0">
                            <div class="row no-gutters">
                                <div class="col-12">

                                  <h1 class="lead m-4 text-center">{!! __('guest/locator.guide') !!}</h1>

                                  <div class="alert alert-danger alert-dismissible fade show m-4" id="error-panel" style="display: none" role="alert">
                                      <i data-feather="alert-triangle" class="alert-icon"></i>
                                      <span class="alert-text"><strong id ="error-msg">&nbsp;</strong></span>
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <i data-feather="x" class="alert-close"></i>
                                      </button>
                                  </div>



                                  <div class="m-3 d-block text-center">
                                      <button type="button" class="btn btn-lg btn-success" onClick="locate()"><i data-feather="map-pin"></i> {{ __('guest/locator.geo_btn') }}</button>
                                  </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




      </div>
  </div>

  <script>

  function locate ()
  {
    if ('geolocation' in navigator)
    {
      navigator.geolocation.getCurrentPosition((position) =>
      {
        window.location = '{{ route ('guest.locator') }}/' + position.coords.latitude + ',' + position.coords.longitude;
      });
    }
    else
    {
      document.getElementById('error-msg').innerHTML = "{{ __('guest/locator.browser_err') }}";
    }
  }

  locate();

  </script>

  <!-- inject:js -->
  <script src="@cdn('dist/js/vendor.base.js')"></script>
  <script src="@cdn('dist/js/vendor.bundle.js')"></script>
  <script> feather.replace() </script>
</body>

</html>
