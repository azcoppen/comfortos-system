@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Telecoms</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">PBX Cloud Servers</div>
          <div class="header-breadcrumb">

            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.index') }}">Network</a>
            <a href="{{ route('telecoms.pbxs.index') }}">PBX Cloud</a>
            <a href="{{ route('telecoms.pbxs.index') }}" class="text-primary">Servers</a>

          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.pbxs.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New PBX Server</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">PBX Servers</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'PBXs'])
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('content')
    <div class="row mt-5">
        @isset($records)
          @each('telecoms.pbxs.each', $records, 'record')

          <hr />

          <div class="row">
            <div class="col-md-6 offset-3">
              {{ $records->links() }}
            </div>
          </div>
        @endisset

        @empty($records)
            @include ('partials.components.alerts.empty')
        @endempty
    </div>
@endsection
