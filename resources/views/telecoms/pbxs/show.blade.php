@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Telecoms</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">PBX Cloud Servers</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.index') }}">Network</a>
            <a href="{{ route('telecoms.pbxs.index') }}">PBX Cloud</a>
            <a href="{{ route('telecoms.pbxs.index') }}">Servers</a>
            <a href="{{ route('telecoms.pbxs.show', $pbx->_id) }}" class="text-primary">{{ $pbx->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.pbxs.edit', $pbx->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $pbx->label ?? $pbx->_id }}</h1>
  </div>
  <hr />

    <div class="row">

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $pbx->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Country</td>
                                <td class="border-0">{{ $pbx->country }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">License</td>
                                <td class="border-0">{{ $pbx->license }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Host</td>
                                <td class="border-0">{{ $pbx->host }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">IP</td>
                                <td class="border-0">{{ $pbx->ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Version</td>
                                <td class="border-0">{{ $pbx->v }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Web/VPN Administration</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">URL</td>
                                <td class="border-0">{{ $pbx->admin_url }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Port</td>
                                <td class="border-0">{{ $pbx->admin_port }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Username</td>
                                <td class="border-0">{{ $pbx->admin_user }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Password</td>
                                <td class="border-0">{{ $pbx->admin_pwd }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Connectivity</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Trunk Provider</td>
                                <td class="border-0">{{ $pbx->trunk_vendor }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Trunk Number</td>
                                <td class="border-0">{{ $pbx->trunk_number }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">SIP Port</td>
                                <td class="border-0">{{ $pbx->sip_port }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Secure SIP Port</td>
                                <td class="border-0">{{ $pbx->secure_sip_port }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">RTP Port (From)</td>
                                <td class="border-0">{{ $pbx->rtp_port_range[0] ?? '' }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">RTP Port (To)</td>
                                <td class="border-0">{{ $pbx->rtp_port_range[1] ?? '' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Extensions</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($pbx->extensions) && $pbx->extensions->count() )
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>SIP ID</th>
                                    <th>Remote</th>
                                    <th>Location</th>
                                    <th>DID</th>
                                    <th>Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.extensions.each', $pbx->extensions, 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">DECT Base Stations</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($pbx->base_stations) && $pbx->base_stations->count() )
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Serial</th>
                                    <th>Model</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Handsets</th>
                                    <th>Created</th>
                                    <th><i class="fa fa-globe" title="WPN Admin"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.base-stations.each', $pbx->base_stations, 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>
    </div>

    @include ('partials.components.panels.device_business_flow', ['component' => $pbx])


@stop
