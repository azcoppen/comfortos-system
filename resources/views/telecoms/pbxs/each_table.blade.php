<tr>
    <td><i class="fa fa-server text-muted"></i></td>
    <td>{{ $record->country }}</td>
    <td><a href="{{ route('telecoms.pbxs.show', $record->_id) }}">{{ Str::limit($record->host, 30) }}</a></td>
    <td>{{ $record->ip }}</td>
    <td>{{ $record->type }} {{ $record->v }}</td>
    <td>{{ $record->created_at->diffForHumans() }}</td>
    <td>
        @if ( $record->admin_url )
        <a title="Launch admin panel" target="_blank" href="https://{{ $record->web_ui }}"><i class="fa fa-shield-alt" title="VPN Admin"></i></a>
        @endif
    </td>
</tr>
