@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Telecoms</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">PBX Cloud Servers</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.index') }}">Network</a>
            <a href="{{ route('telecoms.pbxs.index') }}">PBX Cloud</a>
            <a href="{{ route('telecoms.pbxs.index') }}" class="{{ isset($pbx) ? '' : 'text-primary' }}">Servers</a>
            @if (isset($pbx))
              <a href="{{ route('telecoms.pbxs.show', $pbx->_id) }}" class="text-primary">{{ $pbx->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.pbxs.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('content')
  <h1 class="display-4">{{ isset ($pbx) ? 'Update '.$pbx->label : 'Add A New PBX' }}</h1>
  <hr />
  @if ( isset ($pbx) )
    {!! Form::open (['method' => 'PUT', 'route' => ['telecoms.pbxs.update', $pbx->_id]]) !!}
  @else
    {!! Form::open (['route' => 'telecoms.pbxs.store']) !!}
  @endif

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $pbx->label ?? null,
                  'default'     => '',
                  'placeholder' => "PBX Server",
                  'required'    => true,
                ])
              </div>

              <div class="form-row">
                  <div class="col-md-10">
                      <div class="position-relative form-group">
                        @include ('components.fields.select', [
                          'label'       => "Which type of PBX is it?",
                          'name'        => 'type',
                          'options'     => ['3cx' => '3cx', 'elastix' => 'elastix', 'asterisk' => 'asterisk'],
                          'value'       => $pbx->type ?? null,
                          'default'     => '3cx',
                          'placeholder' => "Choose its type",
                          'required'    => true,
                        ])
                      </div>
                  </div>
                  <div class="col-md-2">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "Version?",
                          'name'        => 'v',
                          'value'       => $pbx->v ?? null,
                          'default'     => '16',
                          'placeholder' => "16",
                          'required'    => true,
                        ])
                      </div>
                  </div>
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which country is it in?",
                  'name'        => 'country',
                  'options'     => config ('dropdowns.countries'),
                  'value'       => $pbx->country ?? null,
                  'default'     => 'US',
                  'placeholder' => "Choose its country",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What is the license/serial number?",
                  'name'        => 'license',
                  'value'       => $pbx->license ?? null,
                  'default'     => '',
                  'placeholder' => "0000-0000-0000-0000",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What is the hostname?",
                  'name'        => 'host',
                  'value'       => $pbx->host ?? null,
                  'default'     => '',
                  'placeholder' => "something.3cx.us",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What is the IP address?",
                  'name'        => 'ip',
                  'value'       => $pbx->ip ?? null,
                  'default'     => '',
                  'placeholder' => "0.0.0.0",
                  'required'    => true,
                ])
              </div>
            </div>
        </div>

    </div> <!-- end col -->

    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> VoIP Settings</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which trunk provider is it?",
                  'name'        => 'trunk_vendor',
                  'value'       => $pbx->trunk_vendor ?? null,
                  'default'     => '',
                  'placeholder' => "vonage",
                  'required'    => false,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What is the trunk DID number?",
                  'name'        => 'trunk_number',
                  'value'       => $pbx->trunk_number ?? null,
                  'default'     => '',
                  'placeholder' => "+100012301234",
                  'required'    => false,
                ])
              </div>

              <div class="form-row">
                  <div class="col-md-10">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "What is the administrative URL?",
                          'name'        => 'admin_url',
                          'value'       => $pbx->admin_url ?? null,
                          'default'     => '',
                          'placeholder' => "https://something.3cx.us",
                          'required'    => true,
                        ])
                      </div>
                  </div>
                  <div class="col-md-2">
                      <div class="position-relative form-group">
                        @include ('components.fields.number', [
                          'label'       => "Port",
                          'name'        => 'admin_port',
                          'value'       => $pbx->admin_port ?? null,
                          'default'     => '443',
                          'placeholder' => "443",
                          'required'    => true,
                        ])
                      </div>
                  </div>
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "What is the admin username?",
                          'name'        => 'admin_user',
                          'value'       => $pbx->admin_user ?? null,
                          'default'     => 'admin',
                          'placeholder' => "admin",
                          'required'    => true,
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "What is the admin password?",
                          'name'        => 'admin_pwd',
                          'value'       => $pbx->admin_pwd ?? null,
                          'default'     => '',
                          'placeholder' => "admin",
                          'required'    => true,
                        ])
                      </div>
                  </div>
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.number', [
                          'label'       => "What is the SIP port?",
                          'name'        => 'sip_port',
                          'value'       => $pbx->sip_port ?? null,
                          'default'     => '5060',
                          'placeholder' => "5060",
                          'required'    => true,
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.number', [
                          'label'       => "What is the secure SIP port?",
                          'name'        => 'secure_sip_port',
                          'value'       => $pbx->secure_sip_port ?? null,
                          'default'     => '5061',
                          'placeholder' => "5061",
                          'required'    => true,
                        ])
                      </div>
                  </div>
              </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.number', [
                            'label'       => "What is the starting RTP port?",
                            'name'        => 'rtp_port_range[0]',
                            'value'       => $pbx->rtp_port_range[0] ?? null,
                            'default'     => '9000',
                            'placeholder' => "9000",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.number', [
                            'label'       => "What is the ending RTP port?",
                            'name'        => 'rtp_port_range[1]',
                            'value'       => $pbx->rtp_port_range[1] ?? null,
                            'default'     => '10999',
                            'placeholder' => "10999",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                  </div>


            </div>
        </div>

    </div> <!-- end col -->

    @include ('partials.components.forms.vpn', [
      'entity'  => $pbx ?? null,
      'vpns'    => $vpns,
    ])


  </div>

  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $pbx ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $pbx ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $pbx ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->


    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}

    @if ( isset ($pbx) )
      @include ('partials.components.panels.delete_bar', [
        'entity'  => $pbx,
        'text'    => 'PBX',
        'destroy' => [
          'route' => 'telecoms.pbxs.destroy',
          'params'=> [$pbx->_id],
        ],
        'return'  => [
          'route' => 'telecoms.pbxs.index',
          'params'=> [$pbx->_id],
        ],
      ])
    @endif


@endsection
