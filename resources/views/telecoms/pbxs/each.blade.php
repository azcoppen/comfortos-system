@include ('partials.components.panels.sexybox', [
    'option' => 2,
    'icon' => 'server',
    'title' => $record->host,
    'subtitle' => $record->license,
    'figure' => '<i class="text-success" data-feather="phone-forwarded"></i> &nbsp;'.$record->ip,
    'link' => route ('telecoms.pbxs.show', $record->_id),
    'edit' => route ('telecoms.pbxs.edit', $record->_id),
    'footer_text' => $record->type . ' ' . $record->v . ': ' . $record->trunk_number . ' ('.$record->trunk_vendor.')',
])
