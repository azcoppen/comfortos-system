
<ul class="nav">
    <li class="nav-header">Network</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#pbx" aria-expanded="false" aria-controls="pbx">
            <i data-feather="server" class="menu-icon"></i>
            <span class="menu-title">PBX Cloud</span>
        </a>
        <div class="collapse show" id="pbx">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'pbxs' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('telecoms.pbxs.index') }}"><span class="menu-title">Servers</span></a></li>
            </ul>
        </div>
    </li>

    <li class="nav-header">Hardware</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#hardware" aria-expanded="false" aria-controls="hardware">
            <i data-feather="mic" class="menu-icon"></i>
            <span class="menu-title">Clients</span>
        </a>
        <div class="collapse show" id="hardware">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'dect' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('telecoms.dect.index') }}"><span class="menu-title">DECT Base Stations</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'handsets' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('telecoms.handsets.index') }}"><span class="menu-title">VoIP Handsets</span></a></li>
            </ul>
        </div>
    </li>

    <li class="nav-header">Connectivity</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#connectivity" aria-expanded="false" aria-controls="connectivity">
            <i data-feather="phone-incoming" class="menu-icon"></i>
            <span class="menu-title">Provisioning</span>
        </a>
        <div class="collapse show" id="connectivity">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'extensions' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('telecoms.extensions.index') }}"><span class="menu-title">Extensions</span></a></li>
            </ul>
        </div>
    </li>
</ul>
