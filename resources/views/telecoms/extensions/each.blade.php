<tr>
    <td><i class="fa fa-phone text-muted"></i></td>
    <td><a href="{{ route('telecoms.extensions.show', $record->_id) }}">{{ $record->_id }}</a></td>
    <td>{{ $record->label }}</td>
    <td>{{ $record->sip_id }}</td>
    <td>{{ $record->remote_id }}</td>
    <td>
        @if (is_object ($record->room))
            {{ $record->room->label ?? '' }}
        @else
            <span class="text-warning"><i class="fa fa-question-circle"></i> Not provisioned</span>
        @endif
    </td>
    <td>{{ $record->did }}</td>
    <td>{{ $record->created_at->diffForHumans() }}</td>
</tr>
