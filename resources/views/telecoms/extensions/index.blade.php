@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Extensions</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Extensions</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.extensions.index') }}">Connectivity</a>
            <a href="{{ route('telecoms.extensions.index') }}">Provisioning</a>
            <a href="{{ route('telecoms.extensions.index') }}" class="text-primary">PBX Extensions</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.extensions.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Extension</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">PBX Extensions</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'Extensions'])
@stop

@section('content')
    <div class="row mt-3">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Extensions By Date</h5>
                </div>
                <div class="card-body">

                  @isset($records)
                    <div class="table-responsive">
                        <table id="" class="table table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Identifier</th>
                                <th>Label</th>
                                <th>SIP ID</th>
                                <th>Remote</th>
                                <th>Location</th>
                                <th>DID</th>
                                <th>Created</th>
                            </tr>
                            </thead>
                            <tbody>
                              @each('telecoms.extensions.each', $records, 'record')
                            </tbody>
                        </table>
                    </div>

                    <hr />

                    <div class="row">
                      <div class="col-md-6 offset-3">
                        {{ $records->links() }}
                      </div>
                    </div>
                  @endisset

                  @empty($records)
                      @include ('partials.components.alerts.empty')
                  @endempty

                </div>
            </div>
        </div>
    </div>
@endsection
