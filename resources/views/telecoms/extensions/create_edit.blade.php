@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Extensions</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Extensions</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.extensions.index') }}">Connectivity</a>
            <a href="{{ route('telecoms.extensions.index') }}">Provisioning</a>
            <a href="{{ route('telecoms.extensions.index') }}" class="{{ isset($extension) ? '' : 'text-primary' }}">PBX Extensions</a>
            @if ( isset($extension) )
              <a href="{{ route('telecoms.extensions.show', $extension->_id) }}" class="text-primary">{{ $extension->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.extensions.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('content')
  <h1 class="display-4">{{ isset ($extension) ? 'Update '.$extension->label : 'Add A New Extension' }}</h1>
  <hr />
  @if ( isset ($extension) )
    {!! Form::open (['method' => 'PUT', 'route' => ['telecoms.extensions.update', $extension->_id]]) !!}
  @else
    {!! Form::open (['route' => 'telecoms.extensions.store']) !!}
  @endif


  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $extension->label ?? null,
                  'default'     => '',
                  'placeholder' => "Room Extension",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which PBX does it exist on?",
                  'name'        => 'pbx_id',
                  'options'     => $pbxs->pluck ('label', '_id')->all(),
                  'value'       => $extension->pbx_id ?? null,
                  'default'     => '',
                  'placeholder' => "Choose its parent PBX",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "How does its caller ID appear?",
                  'name'        => 'caller_id',
                  'value'       => $extension->caller_id ?? null,
                  'default'     => '',
                  'placeholder' => "The extension's caller ID",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What is its DID number?",
                  'name'        => 'did',
                  'value'       => $extension->did ?? null,
                  'default'     => '',
                  'placeholder' => "+100012304321",
                  'required'    => true,
                  'help'        => 'Direct International Dialling number'
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.number', [
                  'label'       => "What is its voicemail PIN?",
                  'name'        => 'vpin',
                  'value'       => $extension->vpin ?? null,
                  'default'     => '4443',
                  'placeholder' => "4443",
                  'required'    => true,
                ])
              </div>

            </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="phone-forwarded"></i> VoIP Setup</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What is its remote ID on the PBX?",
                    'name'        => 'remote_id',
                    'value'       => $extension->remote_id ?? null,
                    'default'     => '',
                    'placeholder' => "Internal ID of the PBX extension",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What is its SIP ID on the PBX?",
                    'name'        => 'sip_id',
                    'value'       => $extension->sip_id ?? null,
                    'default'     => '',
                    'placeholder' => "Internal SIP ID of the PBX extension",
                    'required'    => true,
                    'help'        => 'SIP username, e.g. something@host'
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What is the Auth ID to use when connecting?",
                    'name'        => 'auth_id',
                    'value'       => $extension->auth_id ?? null,
                    'default'     => '',
                    'placeholder' => "Auth ID of the PBX extension",
                    'required'    => true,
                    'help'        => 'Issued by the PBX server',
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What is the Auth Password to use when connecting?",
                    'name'        => 'auth_pwd',
                    'value'       => $extension->auth_pwd ?? null,
                    'default'     => '',
                    'placeholder' => "Auth Password of the PBX extension",
                    'required'    => true,
                    'help'        => 'Issued by the PBX server',
                  ])
                </div>
              </div>
          </div>
        </div> <!-- end col -->

    </div> <!-- end row -->


  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $extension ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $extension ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $extension ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->

  <hr />
  <div class="row mb-4">
    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}


  @if ( isset ($extension) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $extension,
      'text'    => 'Extension',
      'destroy' => [
        'route' => 'telecoms.extensions.destroy',
        'params'=> [$extension->_id],
      ],
      'return'  => [
        'route' => 'telecoms.extensions.index',
        'params'=> [$extension->_id],
      ],
    ])
  @endif

@endsection
