@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Extensions</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Extensions</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.extensions.index') }}">Connectivity</a>
            <a href="{{ route('telecoms.extensions.index') }}">Provisioning</a>
            <a href="{{ route('telecoms.extensions.index') }}" class="">PBX Extensions</a>
            <a href="{{ route('telecoms.extensions.show', $extension->_id) }}" class="text-primary">{{ $extension->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.extensions.edit', $extension->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $extension->label ?? $extension->_id }}</h1>
  </div>
  <hr />

    <div class="row">
        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Details</h5>
                </div>
                <div class="card-body text-center">
                    <h2 class="lead"><i class="fa fa-phone"></i> {{ $extension->sip_id }}</h2>
                    <h3 class"text-muted">{{ $extension->caller_id }}</h3>

                    <p class="text-muted mt-4">
                        Created: {{ $extension->created_at->diffForHumans() }}<br />
                    </p>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Remote PBX ID</td>
                                <td class="border-0">{{ $extension->remote_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">SIP Server</td>
                                <td class="border-0">{{ $extension->pbx->host }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">SIP ID</td>
                                <td class="border-0">{{ $extension->sip_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Auth ID</td>
                                <td class="border-0">{{ $extension->auth_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Auth Password</td>
                                <td class="border-0">{{ $extension->auth_pwd }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Direct Dial (DID)</td>
                                <td class="border-0">{{ $extension->did }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Voicemail PIN</td>
                                <td class="border-0">{{ $extension->vpin }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include ('partials.provisioning', ['room' => $extension->room, 'item' => $extension])
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">PBX Server</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($extension->pbx) )
                        <div class="table-responsive">
                            <table id="" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Country</th>
                                    <th>Host</th>
                                    <th>IP</th>
                                    <th>Type</th>
                                    <th>Created</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.pbxs.each_table', collect([$extension->pbx]), 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">DECT Base Stations</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($extension->dects) )
                        <div class="table-responsive">
                            <table id="" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Serial</th>
                                    <th>Model</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Handsets</th>
                                    <th>Created</th>
                                    <th><i class="fa fa-globe" title="WPN Admin"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.dect.each', $extension->dects, 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Handsets</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($extension->handsets) && $extension->handsets->count() )
                        <div class="table-responsive">
                            <table id="" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Serial</th>
                                    <th>Name</th>
                                    <th>Model</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.handsets.each', $extension->handsets, 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop
