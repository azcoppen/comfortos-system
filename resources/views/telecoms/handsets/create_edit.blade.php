@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: VoIP Handsets</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">VoIP Handsets</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.dect.index') }}">Hardware</a>
            <a href="{{ route('telecoms.dect.index') }}">Clients</a>
            <a href="{{ route('telecoms.handsets.index') }}" class="{{ isset($handset) ? '' : 'text-primary' }}">VoIP Handsets</a>
            @if (isset($handset))
              <a href="{{ route('telecoms.handsets.show', $handset->_id) }}" class="text-primary">{{ $handset->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.handsets.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('content')

  <h1 class="display-4">{{ isset ($handset) ? 'Update '.$handset->label : 'Add A New VoIP Handset' }}</h1>
  <hr />
  @if ( isset ($handset) )
    {!! Form::open (['method' => 'PUT', 'route' => ['telecoms.handsets.update', $handset->_id]]) !!}
  @else
    {!! Form::open (['route' => 'telecoms.handsets.store']) !!}
  @endif

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $handset->label ?? null,
                  'default'     => '',
                  'placeholder' => "VoIP Handset",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which type of handset is it?",
                  'name'        => 'type',
                  'options'     => ['grandstream-dp720' => 'grandstream-dp720',],
                  'value'       => $handset->type ?? null,
                  'default'     => 'grandstream-dp720',
                  'placeholder' => "Choose its type",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which model of handset is it?",
                  'name'        => 'model',
                  'value'       => $handset->model ?? null,
                  'default'     => 'DP720',
                  'placeholder' => "The model of the handset",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which DECT base station does it connect to?",
                  'name'        => 'dect_id',
                  'options'     => $dects->pluck ('label', '_id')->all(),
                  'value'       => $handset->dect_ids[0] ?? null,
                  'default'     => '',
                  'placeholder' => "Choose its parent DECT",
                  'required'    => true,
                ])
              </div>

            </div>
        </div>
    </div> <!-- end col -->


    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="box"></i> Hardware</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which firmware is it running?",
                  'name'        => 'v',
                  'value'       => $handset->v ?? null,
                  'default'     => '',
                  'placeholder' => "The handset's firmware version",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What's its serial number?",
                  'name'        => 'serial',
                  'value'       => $handset->serial ?? null,
                  'default'     => '',
                  'placeholder' => "Serial number of the handset",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What's its IPEI number?",
                  'name'        => 'ipei',
                  'value'       => $handset->ipei ?? null,
                  'default'     => '',
                  'placeholder' => "IEPI number of the device",
                  'required'    => true,
                ])
              </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's its network MAC address?",
                    'name'        => 'mac',
                    'value'       => $handset->mac ?? null,
                    'default'     => '',
                    'placeholder' => "MAC address of the network adaptor (e.g. dc:a6:32:96:f7:26)",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's its name in the DECT station?",
                    'name'        => 'name',
                    'value'       => $handset->name ?? null,
                    'default'     => '',
                    'placeholder' => "DECT name of the handset",
                    'required'    => true,
                    'help'        => "This is the name of the handset registered in the DECT admin panel",
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's it's internal DHCP address from the router?",
                    'name'        => 'int_ip',
                    'value'       => $handset->int_ip ?? null,
                    'default'     => '',
                    'placeholder' => "IP address of the handset",
                    'required'    => true,
                  ])
                </div>

            </div>
      </div>
    </div> <!-- end col -->


  </div>



  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $handset ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $handset ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $handset ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->

  <hr />
  <div class="row mb-4">
    @if ( !isset ($handset) )
      @include ('partials.components.forms.clone')
    @endif

    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}

  @if ( isset ($handset) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $handset,
      'text'    => 'Handset',
      'destroy' => [
        'route' => 'telecoms.handsets.destroy',
        'params'=> [$handset->_id],
      ],
      'return'  => [
        'route' => 'telecoms.handsets.index',
        'params'=> [$handset->_id],
      ],
    ])
  @endif


@endsection
