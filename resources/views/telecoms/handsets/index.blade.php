@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: VoIP Handsets</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">VoIP Handsets</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.dect.index') }}">Hardware</a>
            <a href="{{ route('telecoms.dect.index') }}">Clients</a>
            <a href="{{ route('telecoms.handsets.index') }}" class="text-primary">VoIP Handsets</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.handsets.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Handset</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">VoIP Handsets</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'Handsets'])
@stop

@section('content')
    <div class="row mt-3">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Devices By Date</h5>
                </div>
                <div class="card-body">

                  @isset($records)
                    <div class="table-responsive">
                        <table id="" class="table table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Identifier</th>
                                <th>Label</th>
                                <th>Serial</th>
                                <th>Name</th>
                                <th>Model</th>
                                <th>Location</th>
                                <th>Created</th>
                            </tr>
                            </thead>
                            <tbody>
                              @each('telecoms.handsets.each', $records, 'record')
                            </tbody>
                        </table>
                    </div>

                    <hr />

                    <div class="row">
                      <div class="col-md-6 offset-3">
                        {{ $records->links() }}
                      </div>
                    </div>
                  @endisset

                  @empty($records)
                      @include ('partials.components.alerts.empty')
                  @endempty

                </div>
            </div>
        </div>
    </div>
@endsection
