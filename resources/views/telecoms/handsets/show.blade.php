@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: VoIP Handsets</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">VoIP Handsets</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.dect.index') }}">Hardware</a>
            <a href="{{ route('telecoms.dect.index') }}">Clients</a>
            <a href="{{ route('telecoms.handsets.index') }}" class="text-primary">VoIP Handsets</a>
            <a href="{{ route('telecoms.handsets.show', $handset->_id) }}" class="text-primary">{{ $handset->label }}</a>

          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.handsets.edit', $handset->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $handset->label ?? $handset->_id }}</h1>
  </div>
  <hr />

    <div class="row">
        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ $handset->label }}</h5>
                </div>
                <div class="card-body text-center">
                    <img src="https://res.cloudinary.com/smartrooms/devices/handset.jpg" style="max-height: 16rem; margin: auto;" align="center" />
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $handset->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Type</td>
                                <td class="border-0">{{ $handset->type }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Name</td>
                                <td class="border-0">{{ $handset->name }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Model</td>
                                <td class="border-0">{{ $handset->model }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Serial</td>
                                <td class="border-0">{{ $handset->serial }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Firmware</td>
                                <td class="border-0">{{ $handset->v }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include ('partials.provisioning', ['room' => $handset->room, 'item' => $handset])

        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Network</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Connection</td>
                                <td class="border-0">DECT</td>
                            </tr>
                            <tr>
                                <td class="border-0">Tunnelled</td>
                                <td class="border-0">No</td>
                            </tr>
                            <tr>
                                <td class="border-0">LAN IP</td>
                                <td class="border-0">{{ $handset->ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Gateway IP</td>
                                <td class="border-0">192.168.88.1</td>
                            </tr>
                            <tr>
                                <td class="border-0">IPEI</td>
                                <td class="border-0">{{ $handset->ipei }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Mac</td>
                                <td class="border-0">{{ $handset->mac }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <hr />

    <div class="row">
        @php $dect = $handset->dects->first() @endphp

        @if ( $dect )
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">PBX Server</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($dect->pbx) )
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Country</th>
                                    <th>Host</th>
                                    <th>IP</th>
                                    <th>Type</th>
                                    <th>Created</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.pbxs.each_table', collect([$dect->pbx]), 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>
      @endif

    </div>

    <div class="row">

        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">DECT Base Stations</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($handset->dects) && count ($handset->dects) )
                        <div class="table-responsive">
                            <table id="" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Serial</th>
                                    <th>Model</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Handsets</th>
                                    <th>Created</th>
                                    <th><i class="fa fa-globe" title="WPN Admin"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.dect.each', $handset->dects, 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Extensions</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($handset->extensions) && $handset->extensions->count() )
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>SIP ID</th>
                                    <th>Remote</th>
                                    <th>Location</th>
                                    <th>DID</th>
                                    <th>Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.extensions.each', $handset->extensions, 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
      @livewire('signals-table', ['component' => $handset->_id])
    </div>

    @include ('partials.components.panels.device_business_flow', ['component' => $handset])

@stop
