<tr>
    <td><i class="fa fa-mobile-alt text-muted"></i></td>
    <td><a href="{{ route('telecoms.handsets.show', $record->_id) }}">{{ $record->_id }}</a></td>
    <td>{{ $record->label }}</td>
    <td>{{ $record->serial }}</td>
    <td>{{ $record->name }}</td>
    <td>{{ $record->model }}</td>

    <td>
        @if (is_object ($record->room))
            {{ $record->room->label ?? '' }}
        @else
            <span class="text-warning"><i class="fa fa-question-circle"></i> Not provisioned</span>
        @endif
    </td>
    <td>{{ $record->created_at->diffForHumans() }}</td>
</tr>
