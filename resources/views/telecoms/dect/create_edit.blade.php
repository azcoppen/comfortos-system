@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: DECT Base Stations</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">DECT Base Stations</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.dect.index') }}">Hardware</a>
            <a href="{{ route('telecoms.dect.index') }}">Clients</a>
            <a href="{{ route('telecoms.dect.index') }}" class="{{ isset($dect) ? '' : 'text-primary' }}">DECT Base Stations</a>
            @if (isset($dect))
              <a href="{{ route('telecoms.dect.show', $dect->_id) }}" class="text-primary">{{ $dect->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.dect.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('content')

  <h1 class="display-4">{{ isset ($dect) ? 'Update '.$dect->label : 'Add A New DECT Base Station' }}</h1>
  <hr />
  @if ( isset ($dect) )
    {!! Form::open (['method' => 'PUT', 'route' => ['telecoms.dect.update', $dect->_id]]) !!}
  @else
    {!! Form::open (['route' => 'telecoms.dect.store']) !!}
  @endif

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $dect->label ?? null,
                  'default'     => '',
                  'placeholder' => "DECT Base Station",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which type of base station is it?",
                  'name'        => 'type',
                  'options'     => ['grandstream-dp750' => 'grandstream-dp750',],
                  'value'       => $dect->type ?? null,
                  'default'     => 'grandstream-dp750',
                  'placeholder' => "Choose its type",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which model of base station is it?",
                  'name'        => 'model',
                  'value'       => $dect->model ?? null,
                  'default'     => 'DP750',
                  'placeholder' => "The model of the base station",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which PBX does it connect to?",
                  'name'        => 'pbx_id',
                  'options'     => $pbxs->pluck ('label', '_id')->all(),
                  'value'       => $dect->pbx_id ?? null,
                  'default'     => '',
                  'placeholder' => "Choose its parent PBX",
                  'required'    => true,
                ])
              </div>

            </div>
        </div>
    </div> <!-- end col -->


    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="box"></i> Hardware</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which firmware is it running?",
                  'name'        => 'v',
                  'value'       => $dect->v ?? null,
                  'default'     => '',
                  'placeholder' => "The base station's firmware version",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What's its serial number?",
                  'name'        => 'serial',
                  'value'       => $dect->serial ?? null,
                  'default'     => '',
                  'placeholder' => "Serial number of the base station",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What's its RFPI number?",
                  'name'        => 'rfpi',
                  'value'       => $dect->rfpi ?? null,
                  'default'     => '',
                  'placeholder' => "RFPI number of the device",
                  'required'    => true,
                  'help'        => "Radio Fixed Part Identity Number",
                ])
              </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's its network MAC address?",
                    'name'        => 'mac',
                    'value'       => $dect->mac ?? null,
                    'default'     => '',
                    'placeholder' => "MAC address of the network adaptor (e.g. dc:a6:32:96:f7:26)",
                    'required'    => true,
                  ])
                </div>

            </div>
      </div>
    </div> <!-- end col -->
  </div> <!-- end row -->

  <div class="row">

    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="wifi"></i> Network</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which router is it plugged into?",
                  'name'        => 'router_id',
                  'options'     => $routers->pluck ('label', '_id')->all(),
                  'value'       => $dect->router_id ?? null,
                  'default'     => '',
                  'placeholder' => "Choose a router to attach the DECT to",
                  'required'    => false,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What's it's network hostname?",
                  'name'        => 'host',
                  'value'       => $dect->host ?? null,
                  'default'     => 'smartrooms-dect',
                  'placeholder' => "Network hostname of the DECT",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What's it's internal DHCP address from the router?",
                  'name'        => 'int_ip',
                  'value'       => $dect->int_ip ?? null,
                  'default'     => '',
                  'placeholder' => "IP address of the DECT",
                  'required'    => true,
                ])
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "What's the administrative username?",
                          'name'        => 'admin_user',
                          'value'       => $dect->admin_user ?? null,
                          'default'     => 'admin',
                          'placeholder' => "Username to log into the DECT with",
                          'required'    => true,
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "What's the administrative password?",
                          'name'        => 'admin_pwd',
                          'value'       => isset ($dect) && isset ($dect->admin_pwd) ? Crypt::decrypt($dect->admin_pwd) : null,
                          'default'     => 'grandstream',
                          'placeholder' => "Password to log into the DECT with",
                          'required'    => true,
                        ])
                      </div>
                  </div>
                </div>


            </div>
      </div>
    </div> <!-- end col -->


    @include ('partials.components.forms.vpn', [
      'entity'  => $hab ?? null,
      'vpns'    => $vpns,
    ])

  </div> <!-- end row -->

  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $dect ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $dect ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $dect ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->

  <hr />
  <div class="row mb-4">
    @if ( !isset ($dect) )
      @include ('partials.components.forms.clone')
    @endif

    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}

  @if ( isset ($dect) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $dect,
      'text'    => 'DECT',
      'destroy' => [
        'route' => 'telecoms.dect.destroy',
        'params'=> [$dect->_id],
      ],
      'return'  => [
        'route' => 'telecoms.dect.index',
        'params'=> [$dect->_id],
      ],
    ])
  @endif

@endsection
