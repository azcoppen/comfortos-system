@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: DECT Base Stations</title>
@stop

@section('app_title')
  <h4>Telecoms</h4>
@stop

@section('sidebar')
  @include ('telecoms.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">DECT Base Stations</div>
          <div class="header-breadcrumb">
            <a href="{{ route('telecoms.index') }}"><i data-feather="phone-call"></i> Telecoms</a>
            <a href="{{ route('telecoms.dect.index') }}">Hardware</a>
            <a href="{{ route('telecoms.dect.index') }}">Clients</a>
            <a href="{{ route('telecoms.dect.index') }}">DECT Base Stations</a>
            <a href="{{ route('telecoms.dect.show', $dect->_id) }}" class="text-primary">{{ $dect->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('telecoms.dect.edit', $dect->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $dect->label ?? $dect->_id }}</h1>
  </div>
  <hr />

    <div class="row">
        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ $dect->label }}</h5>
                </div>
                <div class="card-body text-center">
                    <img src="https://res.cloudinary.com/smartrooms/devices/dect.jpg" style="max-height: 14rem; max-width: 100%; margin: auto;" align="center" />
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $dect->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Type</td>
                                <td class="border-0">{{ $dect->type }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Model</td>
                                <td class="border-0">{{ $dect->model }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Serial</td>
                                <td class="border-0">{{ $dect->serial }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Firmware</td>
                                <td class="border-0">{{ $dect->v }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include ('partials.provisioning', ['room' => $dect->room, 'item' => $dect])

        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Network</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Connection</td>
                                <td class="border-0">Ethernet</td>
                            </tr>
                            <tr>
                                <td class="border-0">Tunnelled</td>
                                <td class="border-0">Yes</td>
                            </tr>
                            <tr>
                                <td class="border-0">LAN IP</td>
                                <td class="border-0">{{ $dect->int_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">VPN IP</td>
                                <td class="border-0">{{ $dect->vpn_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Mac</td>
                                <td class="border-0">{{ $dect->mac }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Web/VPN Administration</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">URL</td>
                                <td class="border-0">http://{{ $dect->vpn_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Port</td>
                                <td class="border-0">{{ $dect->admin_port ?? 443 }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Username</td>
                                <td class="border-0">{{ $dect->admin_user }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Password</td>
                                <td class="border-0">{{ $dect->admin_pwd ? Crypt::decrypt($dect->admin_pwd) : '' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <hr />

    <div class="row">

        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">PBX Server</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($dect->pbx) )
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Country</th>
                                    <th>Host</th>
                                    <th>IP</th>
                                    <th>Type</th>
                                    <th>Created</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.pbxs.each_table', collect([$dect->pbx]), 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Handsets</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($dect->handsets) && $dect->handsets->count() )
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Serial</th>
                                    <th>Name</th>
                                    <th>Model</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.handsets.each', $dect->handsets, 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Extensions</h5>
                </div>
                <div class="card-body">
                    @if ( is_object($dect->extensions) && $dect->extensions->count() )
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>SIP ID</th>
                                    <th>Remote</th>
                                    <th>Location</th>
                                    <th>DID</th>
                                    <th>Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('telecoms.extensions.each', $dect->extensions, 'record')
                                </tbody>
                            </table>
                        </div>
                    @else
                        @include ('partials.components.alerts.empty')
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
      @livewire('signals-table', ['component' => $dect->_id])
    </div>

    @include ('partials.components.panels.device_business_flow', ['component' => $dect])

@stop
