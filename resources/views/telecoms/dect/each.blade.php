<tr>
    <td><i class="fa fa-network-wired text-muted"></i></td>
    <td><a href="{{ route('telecoms.dect.show', $record->_id) }}">{{ $record->_id }}</a></td>
    <td>{{ $record->label }}</td>
    <td>{{ $record->serial }}</td>
    <td>{{ $record->model }}</td>

    <td>
        @if (is_object ($record->room))
            {{ $record->room->label ?? '' }}
        @else
            <span class="text-warning"><i class="fa fa-question-circle"></i> Not provisioned</span>
        @endif
    </td>
    <td class="text-center">
        @if ( count($record->handsets) > 0 )
        <span class="badge badge-soft-success">{{ count($record->handsets) }}</span>
        @endif
    </td>
    <td>{{ $record->created_at->diffForHumans() }}</td>
    <td>
        @if ( $record->web_ui )
        <a title="Launch admin panel" target="_blank" href="https://{{ $record->web_ui }}"><i class="fa fa-shield-alt" title="VPN Admin"></i></a>
        @endif
    </td>
</tr>
