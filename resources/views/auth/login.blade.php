@extends('layouts.themes.blixy.auth')

@section('content')
  <div class="row no-gutters pl-4">
      <div class="col-lg-4">
          <div class="user-auth-content login">
              <h3 class="auth-title mb-2">Bring your rooms to life. Take home with you.</h3>
              <span class="text-muted">ComfortOS talks securely to any device which uses Bluetooth&trade;, Ethernet, Wifi, GPS, Z-Wave&trade;, or Zigbee&trade;.</span>

              <div class="row no-gutters mt-5">
                  <div class="col-sm-10 col-lg-10">
                    @include ('partials.alerts')
                      {!! Form::open (['route' => 'login.authenticate']) !!}
                          <div class="form-group inner-addon">
                              <label class="mb-2" for="email">Email</label>
                              {!! Form::email ('email', old('email'), ['id'=> 'email', 'class'=>'form-control', 'placeholder'=>'Your email@domain.com', 'required', 'autofocus']) !!}
                          </div>
                          <div class="form-group inner-addon mb-5">
                              <label class="mb-2" for="password">Password</label>
                              {!! Form::password ('password', ['id'=>'password', 'class'=>'form-control', 'placeholder'=>'* * * * * * * *', 'required']) !!}
                          </div>
                          <div class="mb-4">
                            {!! Form::button ('<i class="fa fa-lock"></i> &nbsp;&nbsp; Authenticate', ['type'=>'submit', 'class'=>'btn btn-lg btn-base btn-rounded']) !!}
                          </div>
                      {!! Form::close () !!}
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-8 d-none d-md-block">
          <div class="auth-right-section" style="background: url(@cdn('dist/images/login-v2.png')) no-repeat;background-position: center;background-size: contain;"></div>
      </div>

  </div>
@stop
