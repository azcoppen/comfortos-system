
<ul class="nav">
    <li class="nav-header">Hardware</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#routers" aria-expanded="false" aria-controls="routers">
            <i data-feather="cpu" class="menu-icon"></i>
            <span class="menu-title">Routers</span>
        </a>
        <div class="collapse show" id="routers">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'routers' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('wifi.routers.index') }}"><span class="menu-title">Inventory</span></a></li>

            </ul>
        </div>
    </li>

    <li class="nav-header">Connectivity</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#connectivity" aria-expanded="false" aria-controls="connectivity">
            <i data-feather="wifi" class="menu-icon"></i>
            <span class="menu-title">Networks</span>
        </a>
        <div class="collapse show" id="connectivity">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'ssids' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('wifi.ssids.index') }}"><span class="menu-title">SSIDs</span></a></li>
            </ul>
        </div>
    </li>


</ul>
