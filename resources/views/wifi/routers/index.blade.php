@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Wifi Routers</title>
@stop

@section('app_title')
  <h4>Wifi</h4>
@stop

@section('sidebar')
  @include ('wifi.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Routers</div>
          <div class="header-breadcrumb">
            <a href="{{ route('wifi.index') }}"><i data-feather="wifi"></i> Wifi</a>
            <a href="{{ route('wifi.index') }}">Hardware</a>
            <a href="{{ route('wifi.routers.index') }}">Routers</a>
            <a href="{{ route('wifi.routers.index') }}" class="text-primary">Inventory</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('wifi.routers.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Router</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">Wifi Routers</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'Routers'])
@stop

@section('content')
    <div class="row mt-3">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Devices By Date</h5>
                </div>
                <div class="card-body">

                  @isset($records)
                    <div class="table-responsive">
                        <table id="" class="table table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Label</th>
                                <th>Serial</th>
                                <th>Model</th>
                                <th>Location</th>
                                <th class="text-center">SSIDs</th>
                                <th>Created</th>
                                <th><i class="fa fa-globe" title="WPN Admin"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                              @each('wifi.routers.each', $records, 'record')
                            </tbody>
                        </table>
                    </div>

                    <hr />

                    <div class="row">
                      <div class="col-md-6 offset-3">
                        {{ $records->links() }}
                      </div>
                    </div>
                  @endisset

                  @empty($records)
                      @include ('partials.components.alerts.empty')
                  @endempty

                </div>
            </div>
        </div>
    </div>
@endsection
