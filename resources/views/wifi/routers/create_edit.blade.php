@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Wifi Routers</title>
@stop

@section('app_title')
  <h4>Wifi</h4>
@stop

@section('sidebar')
  @include ('wifi.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Routers</div>
          <div class="header-breadcrumb">
            <a href="{{ route('wifi.index') }}"><i data-feather="wifi"></i> Wifi</a>
            <a href="{{ route('wifi.index') }}">Hardware</a>
            <a href="{{ route('wifi.routers.index') }}">Routers</a>
            <a href="{{ route('wifi.routers.index') }}" class="{{ isset($router) ? '' : 'text-primary' }}">Inventory</a>
            @if (isset($router))
              <a href="{{ route('wifi.routers.show', $router->_id) }}" class="text-primary">{{ $router->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('wifi.routers.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('content')
  <h1 class="display-4">{{ isset ($router) ? 'Update '.$router->label : 'Add A New Router' }}</h1>
  <hr />
  @if ( isset ($router) )
    {!! Form::open (['method' => 'PUT', 'route' => ['wifi.routers.update', $router->_id]]) !!}
  @else
    {!! Form::open (['route' => 'wifi.routers.store']) !!}
  @endif

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $router->label ?? null,
                  'default'     => '',
                  'placeholder' => "Wifi Router",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which type of router is it?",
                  'name'        => 'type',
                  'options'     => ['router-os-6' => 'router-os-6',],
                  'value'       => $router->type ?? null,
                  'default'     => 'router-os-6',
                  'placeholder' => "Choose its type",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which model of router is it?",
                  'name'        => 'model',
                  'value'       => $router->model ?? null,
                  'default'     => 'RB931-2nD',
                  'placeholder' => "RB931-2nD",
                  'required'    => true,
                ])
              </div>
          </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="box"></i> Hardware</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "Which firmware is it running?",
                    'name'        => 'v',
                    'value'       => $router->v ?? null,
                    'default'     => '6.46.4',
                    'placeholder' => "The router's firmware version",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's its serial number?",
                    'name'        => 'serial',
                    'value'       => $router->serial ?? null,
                    'default'     => '',
                    'placeholder' => "Serial number of the router",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's its network MAC address?",
                    'name'        => 'mac',
                    'value'       => $router->mac ?? null,
                    'default'     => '',
                    'placeholder' => "MAC address of the network adaptor (e.g. dc:a6:32:96:f7:26)",
                    'required'    => true,
                  ])
                </div>

              </div>
        </div>
      </div> <!-- end col -->

    </div> <!-- end row -->


    <div class="row">

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="wifi"></i> Network</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's it's network hostname?",
                    'name'        => 'host',
                    'value'       => $router->hostname ?? null,
                    'default'     => 'smartrooms',
                    'placeholder' => "Network hostname of the router",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's it's internal DHCP address?",
                    'name'        => 'int_ip',
                    'value'       => $router->int_ip ?? null,
                    'default'     => '',
                    'placeholder' => "Gateway IP address of the router",
                    'required'    => true,
                  ])
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "What's the administrative username?",
                            'name'        => 'web_user',
                            'value'       => $router->web_user ?? null,
                            'default'     => 'admin',
                            'placeholder' => "Username to log into the router with",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "What's the administrative password?",
                            'name'        => 'web_pwd',
                            'value'       => $router->web_pwd ?? null,
                            'default'     => 'mikrotik',
                            'placeholder' => "Password to log into the router with",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                  </div>

                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What's the API username?",
                              'name'        => 'api_user',
                              'value'       => $router->api_user ?? null,
                              'default'     => 'admin',
                              'placeholder' => "API username to log into the router with",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What's the API password?",
                              'name'        => 'api_pwd',
                              'value'       => $router->api_pwd ?? null,
                              'default'     => 'mikrotik',
                              'placeholder' => "API Password to log into the router with",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                    </div>

              </div>
        </div>
      </div> <!-- end col -->


      @include ('partials.components.forms.vpn', [
        'entity'  => $router ?? null,
        'vpns'    => $vpns,
      ])

    </div> <!-- end row -->


  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $router ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $router ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $router ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->


    <hr />
    <div class="row mb-4">
      @if ( !isset ($router) )
        @include ('partials.components.forms.clone')
      @endif

      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}

    @if ( isset ($router) )
      @include ('partials.components.panels.delete_bar', [
        'entity'  => $router,
        'text'    => 'Router',
        'destroy' => [
          'route' => 'wifi.routers.destroy',
          'params'=> [$router->_id],
        ],
        'return'  => [
          'route' => 'wifi.routers.index',
          'params'=> [$router->_id],
        ],
      ])
    @endif


@endsection
