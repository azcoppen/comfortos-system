<tr>
    <td><i class="fa fa-network-wired text-muted"></i></td>
    <td><a href="{{ route('wifi.routers.show', $record->_id) }}">{{ $record->_id }}</a></td>
    <td>{{ $record->label }}</td>
    <td>{{ $record->serial }}</td>
    <td>{{ $record->model }}</td>

    <td>
        @if (is_object ($record->room))
            {{ $record->room->label }}
        @else
            <span class="text-warning"><i class="fa fa-question-circle"></i> Not provisioned</span>
        @endif
    </td>
    <td class="text-center">
        @if ( count($record->networks) > 0 )
        <span class="badge badge-soft-success">{{ count($record->networks) }}</span>
        @endif
    </td>
    <td>{{ $record->created_at->diffForHumans() }}</td>
    <td>
        @if ( $record->vpn_ip )
        <a title="Launch admin panel" target="_blank" href="http://{{ $record->vpn_ip }}"><i class="fa fa-shield-alt" title="VPN Admin"></i></a>
        @endif
    </td>
</tr>
