@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Routers</title>
@stop

@section('app_title')
  <h4>Wifi</h4>
@stop

@section('sidebar')
  @include ('wifi.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Routers</div>
          <div class="header-breadcrumb">
            <a href="{{ route('wifi.index') }}"><i data-feather="wifi"></i> Wifi</a>
            <a href="{{ route('wifi.index') }}">Hardware</a>
            <a href="{{ route('wifi.routers.index') }}">Routers</a>
            <a href="{{ route('wifi.routers.index') }}" class="">Inventory</a>
            <a href="{{ route('wifi.routers.show', $router->_id) }}" class="text-primary">{{ $router->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('wifi.routers.edit', $router->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $router->label ?? $router->_id }}</h1>
  </div>
  <hr />

    <div class="row">
        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ $router->label ?? $router->_id}}</h5>
                </div>
                <div class="card-body text-center">
                    <img src="https://res.cloudinary.com/smartrooms/devices/mikrotik.jpg" style="max-height: 16rem; margin: auto;" align="center" />
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $router->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Type</td>
                                <td class="border-0">{{ $router->type }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Label</td>
                                <td class="border-0">{{ $router->label }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Model</td>
                                <td class="border-0">{{ $router->model }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Serial</td>
                                <td class="border-0">{{ $router->serial }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Firmware</td>
                                <td class="border-0">{{ $router->v }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include ('partials.provisioning', ['room' => $router->room, 'item' => $router])

        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Network</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Connection</td>
                                <td class="border-0">Ethernet</td>
                            </tr>
                            <tr>
                                <td class="border-0">Tunnelled</td>
                                <td class="border-0">Yes</td>
                            </tr>
                            <tr>
                                <td class="border-0">LAN IP</td>
                                <td class="border-0">{{ $router->int_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">VPN IP</td>
                                <td class="border-0">{{ $router->vpn_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Gateway IP</td>
                                <td class="border-0">192.168.88.1</td>
                            </tr>
                            <tr>
                                <td class="border-0">Mac</td>
                                <td class="border-0">{{ $router->mac }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Web/VPN Administration</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">URL</td>
                                <td class="border-0">http://{{ $router->vpn_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Port</td>
                                <td class="border-0">80/443</td>
                            </tr>
                            <tr>
                                <td class="border-0">Username</td>
                                <td class="border-0">{{ $router->web_user }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Password</td>
                                <td class="border-0">{{ $router->web_pwd ?? null }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">API Connection</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">URL</td>
                                <td class="border-0">{{ $router->vpn_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Port</td>
                                <td class="border-0">{{ $router->api_port ?? 8728 }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Username</td>
                                <td class="border-0">{{ $router->api_user }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Password</td>
                                <td class="border-0">{{ $router->api_pwd ?? null }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <hr />

    @if ( $router->networks->count() )
    <div class="row">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Network SSIDs</h5>
                </div>
                <div class="card-body">

                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Router</th>
                                    <th>Location</th>
                                    <th>Type</th>
                                    <th>SSID</th>
                                    <th>Index</th>
                                    <th>Created</th>
                                    <th>Expiry</th>
                                    <th>Lifetime</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('wifi.ssids.each', $router->networks, 'record')
                                </tbody>
                            </table>
                        </div>

                </div>
            </div>
        </div>

    </div>
    @endif

    @if ( is_object ($router->latest_status) && is_object ($router->latest_status->first()))
    <hr />

        <div class="row">

            <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                  <span class="alert-text">Data below received: {{ $router->latest_status->first()->created_at->format('D M d Y H:iA') }} GMT ({{ $router->latest_status->first()->created_at->diffForHumans() }})</span>
                </div>
            </div>
        </div>


        @foreach ($router->latest_status->first()->data AS $group => $fields)

        <div class="row">
            <h3 class="col-md-12 mb-4">{{ $group }}</h3>

            @foreach ($fields AS $field => $data)
            <div class="col-lg-12 card-margin">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">{{ $field }} </h5>
                    </div>
                    <div class="card-body">

                                @include ('partials.array_table', compact('data'))

                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
    @endif


    <div class="row">
      @livewire('signals-table', ['component' => $router->_id])
    </div>

    @include ('partials.components.panels.device_business_flow', ['component' => $router])

@endsection
