@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Wifi Networks</title>
@stop

@section('app_title')
  <h4>Wifi</h4>
@stop

@section('sidebar')
  @include ('wifi.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Networks</div>
          <div class="header-breadcrumb">
            <a href="{{ route('wifi.index') }}"><i data-feather="wifi"></i> Wifi</a>
            <a href="{{ route('wifi.ssids.index') }}">Connectivity</a>
            <a href="{{ route('wifi.ssids.index') }}">Networks</a>
            <a href="{{ route('wifi.ssids.index') }}" class="text-primary">SSIDs</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('wifi.ssids.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Network</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">Wifi Networks</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'Networks'])
@stop

@section('content')
    <div class="row mt-3">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Networks By Date</h5>
                </div>
                <div class="card-body">

                  @isset($records)
                    <div class="table-responsive">
                        <table id="" class="table table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Identifier</th>
                                <th>Label</th>
                                <th>Location</th>
                                <th>Type</th>
                                <th>SSID</th>
                                <th>Created</th>
                                <th>Expiry</th>
                            </tr>
                            </thead>
                            <tbody>
                              @each('wifi.ssids.each', $records, 'record')
                            </tbody>
                        </table>
                    </div>

                    <hr />

                    <div class="row">
                      <div class="col-md-6 offset-3 text-center">
                        {{ $records->links() }}
                      </div>
                    </div>
                  @endisset

                  @empty($records)
                      @include ('partials.components.alerts.empty')
                  @endempty

                </div>
            </div>
        </div>
    </div>
@endsection
