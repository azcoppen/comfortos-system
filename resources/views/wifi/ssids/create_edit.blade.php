@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Wifi Networks</title>
@stop

@section('app_title')
  <h4>Wifi</h4>
@stop

@section('sidebar')
  @include ('wifi.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Networks</div>
          <div class="header-breadcrumb">
            <a href="{{ route('wifi.index') }}"><i data-feather="wifi"></i> Wifi</a>
            <a href="{{ route('wifi.ssids.index') }}">Connectivity</a>
            <a href="{{ route('wifi.ssids.index') }}">Networks</a>
            <a href="{{ route('wifi.ssids.index') }}" class="{{ isset($network) ? '' : 'text-primary' }}">SSIDs</a>
            @if ( isset($network) )
            <a href="{{ route('wifi.ssids.show', $network->_id) }}" class="text-primary">{{ $network->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('wifi.ssids.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('content')
  <h1 class="display-4">{{ isset ($network) ? 'Update '.$network->label : 'Add A New Network' }}</h1>
  <hr />
  @if ( isset ($network) )
    {!! Form::open (['method' => 'PUT', 'route' => ['wifi.ssids.update', $network->_id]]) !!}
  @else
    {!! Form::open (['route' => 'wifi.ssids.store']) !!}
  @endif

  <div class="row">
    <div class="col-sm-12">
      <div class="alert alert-dark" role="alert">
        <div class="alert-text">
            This tool is for registering <strong class="text-warning">permanent hardware SSIDs</strong> on WLAN1. The network registered here will <strong class="text-warning">not</strong> be created dynamically on the device.
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $network->label ?? null,
                  'default'     => '',
                  'placeholder' => "Wifi Network",
                  'required'    => true,
                  'help'        => "This is the sytem name, NOT the wifi network name (SSID).",
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which type of network is it?",
                  'name'        => 'type',
                  'options'     => ['hardware' => 'hardware', 'virtual' => 'virtual',],
                  'value'       => $network->type ?? null,
                  'default'     => 'hardware',
                  'placeholder' => "Choose its type",
                  'required'    => true,
                  'help'        => "Should be set to [hardware] only.",
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which router is it on?",
                  'name'        => 'router_id',
                  'options'     => $routers->pluck ('label', '_id')->all(),
                  'value'       => $network->router_id ?? null,
                  'default'     => '',
                  'placeholder' => "Choose its parent router",
                  'required'    => true,
                ])
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.datepicker', [
                          'label'       => "What day should it launch?",
                          'name'        => 'launch_at_date',
                          'value'       => isset ($network->launch_at) && is_object ($network->launch_at) ? $network->launch_at->timezone(auth()->user()->timezone)->format ('m/d/Y') : null,
                          'default'     => now()->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'placeholder' => now()->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'required'    => true,
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.timepicker', [
                          'label'       => "What time should it launch?",
                          'name'        => 'launch_at_time',
                          'value'       => isset ($network->launch_at) && is_object ($network->launch_at) ? $network->launch_at->timezone(auth()->user()->timezone)->format ('H:i:s') : null,
                          'default'     => now()->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'placeholder' => now()->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'required'    => true,
                        ])
                      </div>
                  </div>
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.datepicker', [
                          'label'       => "What day should it expire?",
                          'name'        => 'expire_at_date',
                          'value'       => isset ($network->expire_at) && is_object ($network->expire_at) ? $network->expire_at->timezone(auth()->user()->timezone)->format ('m/d/Y') : null,
                          'default'     => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'placeholder' => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'required'    => true,
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.timepicker', [
                          'label'       => "What time should it expire?",
                          'name'        => 'expire_at_time',
                          'value'       => isset ($network->expire_at) && is_object ($network->expire_at) ? $network->expire_at->timezone(auth()->user()->timezone)->format ('H:i:s') : null,
                          'default'     => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'placeholder' => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'required'    => true,
                        ])
                      </div>
                  </div>
              </div>


            </div>
          </div>
        </div> <!-- end col -->


        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="box"></i> Configuration</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "Which interface is it on?",
                      'name'        => 'interface',
                      'value'       => $network->interface ?? null,
                      'default'     => 'wlan1',
                      'placeholder' => 'wlan1',
                      'required'    => true,
                      'help'        => 'The network interface on the router (eth0 or wlan1)',
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "Which security profile does it use?",
                      'name'        => 'security_profile',
                      'value'       => $network->security_profile ?? null,
                      'default'     => 'default',
                      'placeholder' => 'default',
                      'required'    => true,
                      'help'        => 'Internal profile on the router',
                    ])
                  </div>

                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What index is the security profile?",
                              'name'        => 'profile_index',
                              'value'       => $network->profile_index ?? null,
                              'default'     => '',
                              'placeholder' => "*A",
                              'required'    => true,
                              'help'        => 'Internal ID allocated by the router. Must include an asterisk (*).',
                            ])
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What index is the network profile?",
                              'name'        => 'network_index',
                              'value'       => $network->network_index ?? null,
                              'default'     => '',
                              'placeholder' => "*A",
                              'required'    => true,
                              'help'        => 'Internal ID allocated by the router. Must include an asterisk (*).',
                            ])
                          </div>
                      </div>
                    </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the visible wifi network name (SSID)?",
                      'name'        => 'ssid',
                      'value'       => $network->ssid ?? null,
                      'default'     => 'SmartRooms-Room-N',
                      'placeholder' => "SmartRooms-Room-N",
                      'required'    => true,
                      'help'        => "Do not include spaces or special characters."
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the network password?",
                      'name'        => 'password',
                      'value'       => $network->password ?? null,
                      'default'     => '',
                      'placeholder' => "room101",
                      'required'    => true,
                      'help'        => 'Set in the security profile',
                    ])
                  </div>

                </div>
              </div>
            </div> <!-- end col -->

      </div> <!-- end row -->

  <div class="row">

    @include ('partials.components.forms.installation', [
      'entity'  => $network ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $network ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->


    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}


    @if ( isset ($network) )
      @include ('partials.components.panels.delete_bar', [
        'entity'  => $network,
        'text'    => 'Network',
        'destroy' => [
          'route' => 'wifi.ssids.destroy',
          'params'=> [$network->_id],
        ],
        'return'  => [
          'route' => 'wifi.ssids.index',
          'params'=> [$network->_id],
        ],
      ])
    @endif

@endsection
