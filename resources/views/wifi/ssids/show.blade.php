@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Wifi Networks</title>
@stop

@section('app_title')
  <h4>Wifi</h4>
@stop

@section('sidebar')
  @include ('wifi.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Networks</div>
          <div class="header-breadcrumb">
            <a href="{{ route('wifi.index') }}"><i data-feather="wifi"></i> Wifi</a>
            <a href="{{ route('wifi.ssids.index') }}">Connectivity</a>
            <a href="{{ route('wifi.ssids.index') }}">Networks</a>
            <a href="{{ route('wifi.ssids.index') }}" class="">SSIDs</a>
            <a href="{{ route('wifi.ssids.show', $network->_id) }}" class="text-primary">{{ $network->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('wifi.ssids.edit', $network->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop


@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $network->label ?? $network->_id }}</h1>
  </div>
  <hr />

    <div class="row">
        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Details</h5>
                </div>
                <div class="card-body text-center">
                    <h2 class="lead"><i class="fa fa-wifi"></i> {{ $network->ssid }}</h2>
                    <p><span class="text-muted">Password: </span> {{ $network->password }}</p>
                    <br />
                    <p class="text-muted">
                        Created: {{ $network->created_at->diffForHumans() }}<br />
                        Expiry: {{ $network->expire_at ? $network->expire_at->diffForHumans() : '' }}</br>
                        Lifetime: {{ $network->expire_at ? $network->expire_at->diffForHumans($network->created_at) : '' }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Internals</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $network->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Type</td>
                                <td class="border-0">{{ $network->type }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Interface</td>
                                <td class="border-0">{{ $network->interface }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">SP Index</td>
                                <td class="border-0">{{ $network->profile_index }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Net Index</td>
                                <td class="border-0">{{ $network->network_index }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Profile</td>
                                <td class="border-0">{{ $network->security_profile }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include ('partials.provisioning', ['room' => $network->room, 'item' => $network])

    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Router</h5>
                </div>
                <div class="card-body">
                    @if ( $network->router )
                        <div class="table-responsive">
                            <table id="" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Router</th>
                                    <th>Location</th>
                                    <th>Type</th>
                                    <th>SSID</th>
                                    <th>Index</th>
                                    <th>Created</th>
                                    <th>Expiry</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                  @each('wifi.routers.each', collect([$network->router]), 'record')
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>

@stop
