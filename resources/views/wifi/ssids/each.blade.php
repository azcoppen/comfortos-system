<tr>
    <td><i class="fa fa-wifi text-muted"></i></td>
    <td><a href="{{ route('wifi.ssids.show', $record->_id) }}">{{ $record->_id }}</a></td>
    <td>{{ $record->label }}</td>
    <td>
        @if (isset($record->room) && is_object ($record->room))
            {{ $record->room->label }}
        @else
            <span class="text-warning"><i class="fa fa-question-circle"></i> Not provisioned</span>
        @endif
    </td>
    <td>{{ $record->type }}</td>
    <td><a href="{{ route('wifi.ssids.show', $record->_id) }}">{{ $record->ssid }}</a></td>
    <td>{{ $record->created_at->diffForHumans() }}</td>
    <td>{{ is_object($record->expire_at) ? $record->expire_at->diffForHumans() : '' }}</td>
</tr>
