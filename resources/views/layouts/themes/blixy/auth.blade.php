<!DOCTYPE html>
<html lang="en">

<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>SmartRooms: Authentication </title>
		<link rel="stylesheet" href="@cdn('dist/css/vendor.styles.css')">
		<link rel="stylesheet" href="@cdn('dist/css/demo/light-template.css')">
		<link rel="shortcut icon" href="@cdn('favicon.ico')" />
		<link rel="apple-touch-icon" sizes="120x120" href="@cdn('apple-touch-icon.png')" />
		<link rel="icon" type="image/png" sizes="32x32" href="@cdn('favicon-32x32.png')" />
		<link rel="icon" type="image/png" sizes="16x16" href="@cdn('favicon-16x16.png')" />
		<link rel="manifest" href="@cdn('site.webmanifest')" />
		<link rel="mask-icon" href="@cdn('safari-pinned-tab.svg')" color="#5bbad5" />
		<meta name="msapplication-TileColor" content="#da532c" />
		<meta name="theme-color" content="#ffffff" />
</head>
<body>
<div class="main-container">
		<div class="container-fluid page-body-wrapper full-page-wrapper">
				<div class="user-auth-v3 h-100">
						<div class="row no-gutters mb-5 pl-3">
								<div class="col-lg-12 auth-header">
										<div class="logo-container">
												<a style="text-decoration:none;" class="brand-logo text-dark pl-0 ml-0 display-3" href="{{ route('home') }}">
													<img src="@cdn('favicon-32x32.png')" style="max-height: 3rem; max-width: 3rem;" class="m-0 p-0" /> comfort<strong class="text-primary">os</strong>
												</a>
										</div>
										<div class="link-container d-none d-lg-block">
												<a target="_blank" href="https://www.openhab.org/"><i data-feather="cpu" class="menu-icon text-muted"></i> &nbsp;Smart Home</a>
												<a target="_blank" href="https://developers.meethue.com/"><i data-feather="sun" class="menu-icon text-muted"></i> &nbsp;Lights</a>
												<a target="_blank" href="https://developer.bose.com/"><i data-feather="speaker" class="menu-icon text-muted"></i> &nbsp;Audio</a>
                        <a target="_blank" href="https://mikrotik.com/software"><i data-feather="wifi" class="menu-icon text-muted"></i> &nbsp;Wifi</a>
                        <a target="_blank" href="https://developer.android.com/tv/"><i data-feather="monitor" class="menu-icon text-muted"></i> &nbsp;TV</a>
                        <a target="_blank" href="https://www.elastix.org/"><i data-feather="phone-call" class="menu-icon text-muted"></i> &nbsp;Phone</a>
												<a target="_blank" href="https://magicmirror.builders/"><i data-feather="cast" class="menu-icon text-muted"></i> &nbsp;Mirrors</a>
										</div>
								</div>
						</div>
            @yield('content')
				</div>
		</div>
</div>
<!-- End main-container -->

<!-- inject:js -->
<script src="@cdn('dist/js/vendor.base.js')"></script>
<script src="@cdn('dist/js/vendor.bundle.js')"></script>
<!-- endinject -->
<script src="@cdn('dist/js/components/light-double-sidebar/common-msb.js')"></script>
<script src="@cdn('dist/js/vendor-override/tooltip.js')"></script>
</body>

</html>
