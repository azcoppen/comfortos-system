
<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">

<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		@yield('head')
    <link rel="stylesheet" href="@cdn('dist/css/vendor.styles.css')" />
		<link rel="stylesheet" href="@cdn('dist/css/demo/light-template.css')" />
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" />
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-range-input@1.0.0/dist/css/bootstrap-range-input.min.css" />
		<link rel="stylesheet" href="@cdn('dist/css/toggle.css')" />
		<link rel="shortcut icon" href="@cdn('favicon.ico')" />
		<link rel="apple-touch-icon" sizes="120x120" href="@cdn('apple-touch-icon.png')" />
		<link rel="icon" type="image/png" sizes="32x32" href="@cdn('favicon-32x32.png')" />
		<link rel="icon" type="image/png" sizes="16x16" href="@cdn('favicon-16x16.png')" />
		<link rel="manifest" href="@cdn('site.webmanifest')" />
		<link rel="mask-icon" href="@cdn('safari-pinned-tab.svg')" color="#5bbad5" />
		<meta name="msapplication-TileColor" content="#da532c" />
		<meta name="theme-color" content="#ffffff" />
		<script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
		<link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
		<meta name="jwt" content="{{ auth('api')->setTTL(10080)->fromUser(auth()->user()) }}" />
		@stack('ws')
		@livewireStyles
		@cloudinaryJS
		@stack('head')
</head>
<body>
<div class="main-container">
		<div class="container-fluid page-body-wrapper">

			<nav class="navbar fixed-top">
					<div class="navbar-menu-container d-flex align-items-center justify-content-center">
							<div class="text-center navbar-brand-container align-items-center justify-content-center">
									<a style="text-decoration:none;" class="brand-logo text-dark display-4" href="{{ route('home') }}">
											<img src="@cdn('favicon-32x32.png')" style="max-height: 3rem; max-width: 3rem;" class="m-0 p-0" />
									</a>
							</div>
							<ul class="navbar-nav navbar-nav-right">
									<li class="nav-item mobile-sidebar">
											<button class="nav-link navbar-toggler navbar-toggler-right align-self-center" type="button"
															data-toggle="msb-sidebar">
													<i data-feather="align-right"></i>
											</button>
									</li>
							</ul>
					</div>
			</nav>

			<!-- partial:../../partials/_sidebar.html -->
			<nav class="navbar-container flex-row" id="navbar">
			    <div class="primary">
			        <div class="nav-top">
			            <a style="text-decoration:none;" class="brand-logo text-dark display-4" href="{{ route('home') }}">
			               <img src="@cdn('favicon-32x32.png')" style="max-height: 3rem; max-width: 3rem;" class="m-0 p-0" />
			            </a>
			        </div>
			        <ul class="nav nav-middle">
									@if ( $user_roles->whereIn('name', ['developer', 'superuser', 'administrator'])->count() > 0 )
			            <li class="nav-item">
			                <a class="nav-link {{ !isset($app_section) || (isset($app_section) && $app_section == 'dashboards') ? 'active' : '' }}" data-toggle="tooltip" title="Monitoring and Dashboards" href="{{ route('dashboards.index') }}">
			                    <i data-feather="activity"></i>
			                </a>
			            </li>
									@endif
			            <li class="nav-item">
			                <a class="nav-link {{ isset($app_section) && $app_section == 'explorer' ? 'active' : '' }}" data-toggle="tooltip" title="Explore Operators and Properties" href="{{ route('explorer.index') }}">
			                    <i data-feather="layers"></i>
			                </a>
			            </li>

									@if ( $user_roles->whereIn('name', ['developer', 'superuser', 'administrator'])->count() > 0 )
									<li class="nav-item">
						                <a class="nav-link {{ isset($app_section) && $app_section == 'os' ? 'active' : '' }}" data-toggle="tooltip" title="Property HABs" href="{{ route('os.index') }}">
						                    <i data-feather="server"></i>
						                </a>
						            </li>
									<li class="nav-item">
						                <a class="nav-link {{ isset($app_section) && $app_section == 'telecoms' ? 'active' : '' }}" data-toggle="tooltip" title="Telecoms (PBX, Base Stations, Handsets)" href="{{ route('telecoms.index') }}">
						                    <i data-feather="phone-call"></i>
						                </a>
						            </li>
									<li class="nav-item">
						                <a class="nav-link {{ isset($app_section) && $app_section == 'mirrors' ? 'active' : '' }}" data-toggle="tooltip" title="Smart Mirrors (Pi Boards)" href="{{ route('mirrors.index') }}">
						                    <i data-feather="sun"></i>
						                </a>
						            </li>
									<li class="nav-item">
						                <a class="nav-link {{ isset($app_section) && $app_section == 'wifi' ? 'active' : '' }}" data-toggle="tooltip" title="Routers" href="{{ route('wifi.index') }}">
						                    <i data-feather="wifi"></i>
						                </a>
						            </li>
									@endif

									<li class="nav-item">
						                <a class="nav-link {{ isset($app_section) && $app_section == 'idam' ? 'active' : '' }}" data-toggle="tooltip" title="Identity &amp; Access Management" href="{{ route('idam.index') }}">
						                    <i data-feather="user-plus"></i>
						                </a>
						            </li>
			        </ul>
			        <ul class="nav nav-bottom">
			          <li class="nav-item">
			              <a class="nav-link text-warning" title="Log out" href="{{ route('logout') }}">
			                  <i data-feather="log-out"></i>
			              </a>
			          </li>
			        </ul>
			    </div>
			    <div class="secondary">
			        <div class="sub-header">
			            @yield('app_title')
			        </div>
			        <div class="nav-wrapper">
								@yield ('sidebar')
			        </div>
			    </div>
			  </nav>

				<div class="main-panel">
						<div class="content-wrapper">
								@yield('header')
								@include ('partials.alerts')
								@yield('content')
						</div>
						@include ('partials.footer')
				</div>
		</div>
</div>

	@livewireScripts

	<script src="@cdn('dist/js/vendor.base.js')"></script>
	<script src="@cdn('dist/js/vendor.bundle.js')"></script>
	<script src="@cdn('dist/js/components/light-double-sidebar/common-msb.js')"></script>
	<script src="@cdn('dist/js/vendor-override/tooltip.js')"></script>
	<script src="@cdn('dist/js/components/data-table.js')"></script>
	<script src="@cdn('dist/js/components/dropify.js')"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment-with-locales.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/centrifugal/centrifuge-js@2.6.4/dist/centrifuge.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/xcash/bootstrap-autocomplete@v2.3.7/dist/latest/bootstrap-autocomplete.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/@jaames/iro@5"></script>
	<script src="https://unpkg.com/chart.js@2.9.3/dist/Chart.min.js"></script>
	<script src="https://unpkg.com/@chartisan/chartjs@^2.1.0/dist/chartisan_chartjs.umd.js"></script>

	<script>
	window.chartColors = {
			red: 'rgb(255, 99, 132)',
			orange: 'rgb(255, 159, 64)',
			yellow: 'rgb(255, 205, 86)',
			green: 'rgb(75, 192, 192)',
			blue: 'rgb(54, 162, 235)',
			purple: 'rgb(153, 102, 255)',
			grey: 'rgb(201, 203, 207)'
	};
	</script>
	@stack('js')
</body>

</html>
