@if ( isset($field) && is_object ($field) )
{{ $field->timezone(auth()->user()->timezone)->format ('D M d H:iA') }}
@endif
