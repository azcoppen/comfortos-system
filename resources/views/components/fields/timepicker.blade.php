@if ( isset ($columns) && $columns != FALSE )
  <label for="{{ $name }}" class="col-sm-2 col-form-label">{{ $label ?? '' }} {!! isset($required) && $required != FALSE ? '<span class="text-danger">*</span>' : ''!!}</label>
  <div class="col-sm-10">
@else
  <label for="{{ $name }}" class="">{{ $label ?? '' }} {!! isset($required) && $required != FALSE ? '<span class="text-danger">*</span>' : ''!!}</label>
@endif

<div class="input-group">
  {!! Form::text ($name, old ($name, $value ?? $default), [
    'id' => $name, 'class' => 'form-control'.(isset($large) && $large != FALSE ? '-lg' : '').($errors->has($name) ? ' is-invalid' : ''), 'placeholder' => $placeholder ?? '',
    (isset ($required) && $required != FALSE ? 'required' : 'not-required') => (isset ($required) && $required != FALSE ? 'required' : 'true'),
    (isset ($readonly) && $readonly != FALSE ? 'readonly' : 'write') => (isset ($readonly) && $readonly != FALSE ? 'readonly' : 'true'),
    (isset ($disabled) && $disabled != FALSE ? 'disabled' : 'active') => (isset ($disabled) && $disabled != FALSE ? 'disabled' : 'true'),
    (isset ($autofocus) && $autofocus != FALSE ? 'autofocus' : 'focus') => (isset ($autofocus) && $autofocus != FALSE ? 'autofocus' : 'false'),
  ]) !!}
  <div class="input-group-append">
      <span class="input-group-text"><svg data-feather="clock" width="16px" height="16px"></svg></span>
  </div>
  @if ( isset ($help) )
    <span class="form-text text-muted">{{ $help }}</span>
  @endif
  @if ($errors->has($name))
    <small class="invalid-feedback">{{ $errors->first($name) }}</small>
  @endif
</div>

@if ( isset ($columns) && $columns != FALSE )
  </div>
@endif

@push ('js')
<script>

(function($) {
    'use strict';
    $(function () {
        $.fn.timepicker.defaults = $.extend(true, {}, $.fn.timepicker.defaults, {
            icons: {
                up: 'menu-up',
                down: 'menu-down'
            }
        });
        $('#{{ $name }}').timepicker({showMeridian: false});
    });
})(jQuery);

</script>
@endpush
