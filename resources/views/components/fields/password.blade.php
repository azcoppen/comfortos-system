@if ( isset ($columns) && $columns != FALSE )
  <label for="{{ $name }}" class="col-sm-2 col-form-label">{{ $label ?? '' }} {!! isset($required) && $required != FALSE ? '<span class="text-danger">*</span>' : ''!!}</label>
  <div class="col-sm-10">
@else
  <label for="{{ $name }}" class="">{{ $label ?? '' }} {!! isset($required) && $required != FALSE ? '<span class="text-danger">*</span>' : ''!!}</label>
@endif

{!! Form::password ($name, [
  'id' => $name, 'class' => 'form-control'.(isset($large) && $large != FALSE ? '-lg' : '').($errors->has($name) ? ' is-invalid' : ''), 'placeholder' => $placeholder ?? '',
  (isset ($required) && $required != FALSE ? 'required' : 'not-required') => (isset ($required) && $required != FALSE ? 'required' : 'true'),
  (isset ($readonly) && $readonly != FALSE ? 'readonly' : 'write') => (isset ($readonly) && $readonly != FALSE ? 'readonly' : 'true'),
  (isset ($disabled) && $disabled != FALSE ? 'disabled' : 'active') => (isset ($disabled) && $disabled != FALSE ? 'disabled' : 'true'),
  (isset ($autofocus) && $autofocus != FALSE ? 'autofocus' : 'focus') => (isset ($autofocus) && $autofocus != FALSE ? 'autofocus' : 'false'),
]) !!}
@if ( isset ($help) )
  <span class="form-text text-muted">{{ $help }}</span>
@endif
@if ($errors->has($name))
  <small class="invalid-feedback">{{ $errors->first($name) }}</small>
@endif

@if ( isset ($columns) && $columns != FALSE )
  </div>
@endif
