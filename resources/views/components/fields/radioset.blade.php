@if ( isset ($columns) && $columns != FALSE )
  <label class="col-sm-2 col-form-label">{{ $label ?? '' }} {!! isset($required) && $required != FALSE ? '<span class="text-danger">*</span>' : ''!!}</label>
  <div class="col-sm-10">
@else
  <label>{{ $label ?? '' }} {!! isset($required) && $required != FALSE ? '<span class="text-danger">*</span>' : ''!!}</label>
@endif

  <div class="string-check-inline">
    @foreach ($options AS $text => $val)

      <div class="string-check string-check-bordered-{{ $color ?? 'primary' }} string-check-inline">
        {!! Form::radio ($name, $val, ((old ($name) && old ($name) == $val) || $val == $value ? 1 : 0), ['id' => $name.'-'.$val, 'class' => 'form-check-input']) !!}
          <label class="string-check-label" for="{{ $name.'-'.$val }}">
              <span class="ml-2">{{ $text ?? 'unknown' }}</span>
          </label>
      </div>

    @endforeach
  </div>

  @if ( isset ($help) )
    <span class="form-text text-muted">{{ $help }}</span>
  @endif
  @if ($errors->has($name))
    <small class="invalid-feedback">{{ $errors->first($name) }}</small>
  @endif

@if ( isset ($columns) && $columns != FALSE )
  </div>
@endif
