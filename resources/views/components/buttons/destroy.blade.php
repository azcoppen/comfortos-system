@if ( isset($record) && is_object ($record) )
  @if ( isset ($parent) )
    {!! Form::open (['method' => 'DELETE', 'route' => [$route, $parent->{$parent->getKeyName()}, $record->{$key}]]) !!}
  @else
    {!! Form::open (['method' => 'DELETE', 'route' => [$route, $record->{$key}]]) !!}
  @endif

  <button type="submit" title="Destroy this object" class="mb-2 mr-2 btn-icon btn-icon-only btn btn-danger"><i class="pe-7s-trash btn-icon-wrapper"> </i></button>
{!! Form::close () !!}
@endif
