<div id="hsb-picker-{{ $component->_id }}-panel" class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-sm-8"><i class="fa fa-paint-brush text-muted"></i> Color</h5>
            <div class="col-sm-4 text-right">
              <span class="{{ $component->_id }}-hsb badge badge-{{ isset ($last_value) ? 'info' : 'secondary' }}">{{ isset ($last_value) ? json_encode ($last_value) : '?' }}</span>
            </div>
        </div>
        <div wire:ignore class="card-body text-center">
          <div wire:change="command ()" id="hsb-picker-{{ $component->_id }}" data-last="{{ isset ($last_value) ? json_encode (['h' => $last_value[0], 's' => $last_value[1], 'v' => $last_value[2]]) : '{h:0,s:0,v:0}' }}">
          </div>
        </div>
        @include ('partials.components.widgets.control.footer_refresh')
    </div>
</div>
