<div wire:poll.10s class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-sm-8"><i class="fa fa-wind text-muted"></i> fan mode</h5>
            <div class="col-sm-4 text-right">
              <span class="badge badge-{{ isset ($last_value) ? 'info' : 'secondary' }}">{{ $last_value ?? '?' }}</span>
            </div>
        </div>
        <div wire:ignore class="card-body text-center">

          <select disabled wire:change="command ($event.target.value)" id="fan_mode-{{ $component->_id }}" name="fan_mode-{{ $component->_id }}" class="form-control selectpicker">
            @if ( isset ($additional) && is_array ($additional) && count ($additional) )
              @foreach ( $additional AS $label => $val )
                <option value="{{ $val ?? 0 }}" {{ isset ($last_value) && $last_value == $val ? 'selected' : '' }}>{{ $label ?? 'unknown' }}</option>
              @endforeach
            @endif
          </select>

        </div>
        @include ('partials.components.widgets.control.footer_refresh')
    </div>
</div>
