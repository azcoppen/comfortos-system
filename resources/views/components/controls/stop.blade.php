<div wire:poll.10s class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-sm-8"><i class="fa fa-stop-circle text-muted"></i> stop</h5>
            <div class="col-sm-4 text-right">
              <span class="badge badge-{{ isset ($last_value) ? 'info' : 'secondary' }}">{{ $last_value ?? '?' }}</span>
            </div>
        </div>
        <div class="card-body text-center">

          <div class="text-center">
            <label class="switch">
              <input wire:click="command($event.target.checked ? 'on' : 'off')" id="power-{{ $component->_id }}" type="checkbox" class="warning" {{ isset ($last_signal) && is_object ($last_signal) && $last_signal->value == 'ON' ? 'checked': '' }} value="on" />
              <span class="slider"></span>
            </label>
          </div>

        </div>
        @include ('partials.components.widgets.control.footer_refresh')
    </div>
</div>
