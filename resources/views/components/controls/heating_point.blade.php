<div wire:poll.10s class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-sm-8 ml-0"><i class="fa fa-thermometer-half text-muted"></i> heating point</h5>
            <div class="col-sm-4 text-right">
              <span class="badge badge-{{ isset ($last_value) ? 'info' : 'secondary' }}">{{ $last_value ?? '?' }}</span>
            </div>

        </div>
        <div class="card-body text-center">
          <input wire:change="command ($event.target.value)" type="range" id="heating_point-{{ $component->_id }}" name="heating_point-{{ $component->_id }}" min="36" max="112" value="{{ $last_signal->value ?? 0}}" class="form-control">
        </div>
        @include ('partials.components.widgets.control.footer_refresh')
    </div>
</div>
