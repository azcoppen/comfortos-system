<div wire:poll.10s class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
          <h5 class="card-title col-sm-8"><i class="fa fa-keyboard text-muted"></i> keycode</h5>
          <div class="col-sm-4 text-right">
            <span class="badge badge-{{ isset ($last_value) ? 'info' : 'secondary' }}">{{ $last_value ?? '?' }}</span>
          </div>
        </div>
        <div class="card-body">

          <div class="input-group">
              <input id="keycode-{{ $component->_id }}" name="keycode-{{ $component->_id }}" type="text" class="form-control" placeholder="Remote control keypress" value="{{ $last_signal->value ?? ''}}">
              <div class="input-group-append">
                  <button wire:click="command ($event.target.value)" class="btn btn-sm btn-success" type="button">Press</button>
              </div>
          </div>

        </div>
        @include ('partials.components.widgets.control.footer_refresh')
    </div>
</div>
