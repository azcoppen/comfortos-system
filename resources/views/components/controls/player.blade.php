<div class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-sm-8"><i class="fa fa-play-circle text-muted"></i> player</h5>
            <div class="col-sm-4 text-right">
              <span class="badge badge-{{ isset ($last_value) ? 'info' : 'secondary' }}">{{ $last_value ?? '?' }}</span>
            </div>
        </div>
        <div class="card-body text-center">

          <button type="button" wire:click="command ('previous')" id="player-{{ $component->_id }}-previous" class="mr-1 btn btn-lg btn-success">
            <i class="fa fa-step-backward"></i>
          </button>

          <button type="button" wire:click="command ('play')" id="player-{{ $component->_id }}-play" class="mr-1 btn btn-lg btn-success">
            <i class="fa fa-play"></i>
          </button>

          <button type="button" wire:click="command ('pause')" id="player-{{ $component->_id }}-pause" class="mr-1 btn btn-lg btn-success">
            <i class="fa fa-pause"></i>
          </button>

          <button type="button" wire:click="command ('next')" id="player-{{ $component->_id }}-next" class="mr-1 btn btn-lg btn-success">
            <i class="fa fa-step-forward"></i>
          </button>
        </div>
    </div>
</div>
