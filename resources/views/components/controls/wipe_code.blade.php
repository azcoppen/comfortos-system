<div wire:ignore class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
          <h5 class="card-title col-sm-8"><i class="fa fa-eraser text-muted"></i> wipe code</h5>
        </div>
        <div class="card-body">

          <div class="input-group">
              <input id="codes-{{ $component->_id }}" name="codes-{{ $component->_id }}" type="number" class="form-control" placeholder="Code Index" value="{{ $last_signal->value ?? ''}}">
              <div class="input-group-append">
                  <button wire:click="command ($event.target.value)" class="btn btn-sm btn-danger" type="button">Wipe</button>
              </div>
          </div>

        </div>
    </div>
</div>
