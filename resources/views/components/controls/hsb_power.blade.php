<div wire:poll.10s class="col-sm-4 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-sm-8"><i class="fa fa-power-off text-muted"></i>  power</h5>
            <div class="col-sm-4 text-right">
              <span class="badge badge-{{ isset ($last_value) ? 'info' : 'secondary' }}">{{ isset ($last_value[2]) && $last_value[2] == 0 ? 'OFF': 'ON' }}</span>
            </div>
        </div>
        <div class="card-body">
          <div class="text-center">
            <label class="switch">
              <input wire:click="command($event.target.checked ? 'on' : 'off')" id="power-{{ $component->_id }}" type="checkbox" class="success" {{ isset ($last_value[2]) && $last_value[2] > 0 ? 'checked': '' }} value="on" />
              <span class="slider"></span>
            </label>
          </div>

        </div>
        @include ('partials.components.widgets.control.footer_refresh')
    </div>
</div>
