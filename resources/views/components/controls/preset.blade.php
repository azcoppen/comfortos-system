<div wire:poll.10s class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-sm-6"><i class="fa fa-save text-muted"></i> preset</h5>
            <div class="col-sm-6 text-right">
              <span class="badge badge-{{ isset ($last_value) ? 'info' : 'secondary' }}">{{ $last_value ?? '?' }}</span>
            </div>
        </div>
        <div wire:ignore class="card-body text-center">

          <select wire:change="command ($event.target.value)" id="preset-{{ $component->_id }}" name="preset-{{ $component->_id }}" class="form-control selectpicker">
              @foreach ( range (1, 25) AS $val )
                <option value="{{ $val }}" {{ isset ($last_value) && $last_value == $val ? 'selected' : '' }}>Preset {{ $val }}</option>
              @endforeach
          </select>

        </div>
        @include ('partials.components.widgets.control.footer_refresh')
    </div>
</div>
