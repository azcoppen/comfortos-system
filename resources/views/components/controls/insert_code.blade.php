<div wire:ignore class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
          <h5 class="card-title col-sm-8"><i class="fa fa-plus-circle text-muted"></i> insert code</h5>
        </div>
        <div class="card-body">

          <div class="input-group mb-1">
              <input id="codes-{{ $component->_id }}-label" name="codes-{{ $component->_id }}-label" type="text" class="form-control" placeholder="Name or label" value="{{ $last_signal->value ?? ''}}">
          </div>

          <div class="input-group mb-1">
              <input id="codes-{{ $component->_id }}-hours" name="codes-{{ $component->_id }}-hours" type="number" pattern="[0-9]+" class="form-control" placeholder="Number of Hours (12, 24, 36 etc)" value="{{ $last_signal->value ?? ''}}">
          </div>

          <div class="input-group mb-1">
              <input id="codes-{{ $component->_id }}-index" name="codes-{{ $component->_id }}-index" type="number" pattern="[0-9]+" class="form-control" placeholder="Code Index" value="{{ $last_signal->value ?? ''}}">
          </div>

          <div class="input-group">
              <input id="codes-{{ $component->_id }}-pin" name="codes-{{ $component->_id }}-pin" type="number" pattern="[0-9]+" class="form-control" placeholder="Numeric PIN Code" value="{{ $last_signal->value ?? ''}}">
              <div class="input-group-append">
                  <button wire:click="command ($event.target.value)" class="btn btn-sm btn-success" type="button">Add</button>
              </div>
          </div>

        </div>
    </div>
</div>
