<div wire:poll.10s class="col-sm-6 col-md-3 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-sm-8"><i class="fa fa-arrows-alt text-muted"></i>  direction</h5>
            <div class="col-sm-4 text-right">
              <span class="badge badge-{{ isset ($last_value) ? 'info' : 'secondary' }}">{{ $last_value ?? '?' }}</span>
            </div>
        </div>
        <div class="card-body text-center">

          <button type="button" wire:click="command ('down')" id="dir-{{ $component->_id }}-down" class="mr-1 btn btn-lg btn-success">
            <i class="fa fa-arrow-down"></i>
          </button>

          <button type="button" wire:click="command ('up')" id="dir-{{ $component->_id }}-up" class="mr-1 btn btn-lg btn-success">
            <i class="fa fa-arrow-up"></i>
          </button>

        </div>
        @include ('partials.components.widgets.control.footer_refresh')
    </div>
</div>
