<div class="row">
  <div class="col-md-12">

    <div class="card card-margin">
        <div class="card-header">
            <h5 class="card-title"><i data-feather="zap"></i> Send MQTT Command</h5>
        </div>
        <div class="card-body">

          <div class="form-group">
            <label for="topic" class="">Which broker would you like to connect to? <span class="text-danger">*</span></label>
            <select id="broker" class="form-control" required="required" name="broker">
              @foreach ($mqtt_brokers AS $broker)
                <option value="{{ $broker->_id }}">{{ $broker->label }} --> {{ $broker->host }}</option>
              @endforeach

            </select>
            @error('topic') <span class="text-danger">{{ $message }}</span> @enderror
          </div>

          <div class="form-group">
            <label for="topic" class="">Which topic would you like to send to? <span class="text-danger">*</span></label>
            <input id="topic" class="form-control" placeholder="/property/building/room/group/component/attribute/command" required="required" name="topic" type="text" value="" wire:model.lazy="topic">
            @error('topic') <span class="text-danger">{{ $message }}</span> @enderror
          </div>

          <div class="form-group">
            <label for="payload" class="">What should the payload be? <span class="text-danger">*</span></label>
            <input id="payload" class="form-control" placeholder="ON" required="required" name="payload" type="text" value="" wire:model.lazy="payload">
            @error('payload') <span class="text-danger">{{ $message }}</span> @enderror
          </div>

          <hr />
          <div class="row mb-4">
            <div class="col-lg-12 col-md-12">
              <button type="button" wire:click="send()" class="mt-1 btn btn-warning"><i class="fa fa-bell"> </i>&nbsp; Send</button>
            </div>
          </div> <!-- end row -->


          </div>
      </div>

  </div>
</div>
