<div class="row">
  <div class="col-md-12">

    <div class="card card-margin">
        <div class="card-header">
            <h5 class="card-title"><i data-feather="zap"></i> Send HTTP POST Command</h5>
        </div>
        <div class="card-body">

          <div class="row">
            <div class="col-md-12">

              <div class="form-group">
                <label for="url" class="">Which URL would you like to send to? <span class="text-danger">*</span></label>
                <input id="url" class="form-control" placeholder="https://192.168.88.248/rest/items/something" required="required" name="url" type="url" value="" wire:model.lazy="url">
                @error('url') <span class="text-danger">{{ $message }}</span> @enderror
              </div>

            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="auth_user">Auth User</label>
                    <input type="text" class="form-control" id="auth_user" name="auth_user" wire:model.lazy="auth_user">
                    @error('auth_user') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="form-group">
                  <label for="auth_pass">Auth Pass</label>
                  <input type="text" class="form-control" id="auth_pass" name="auth_pass" wire:model.lazy="auth_pass">
                  @error('auth_pass') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
          </div>

          <div class="form-group">
            <label for="payload" class="">What should the payload be? <span class="text-danger">*</span></label>
            <input id="payload" class="form-control" placeholder="ON" required="required" name="payload" type="text" value="" wire:model.lazy="payload">
            @error('payload') <span class="text-danger">{{ $message }}</span> @enderror
          </div>

          <hr />
          <div class="row mb-4">
            <div class="col-lg-12 col-md-12">
              <button type="button" wire:click="send()" class="mt-1 btn btn-warning"><i class="fa fa-bell"> </i>&nbsp; Send</button>
            </div>
          </div> <!-- end row -->


          </div>
      </div>

  </div>
</div>

<!--
<div class="row">
  <div class="col-md-12">

    <div class="card card-margin">
        <div class="card-header">
            <h5 class="card-title"><i data-feather="corner-down-right"></i> Last Response</h5>
        </div>
        <div class="card-body">

          <div class="row">
            <div class="col-md-3">Status</div>
            <div class="col-md-9">{{ $response['status'] ?? 'Unknown' }}</div>
          </div>

          <div class="row">
            <div class="col-md-3">Successful</div>
            <div class="col-md-9">{{ $response['successful'] ? 'Yes' : 'No' }}</div>
          </div>

          <div class="row">
            <div class="col-md-3">Failed</div>
            <div class="col-md-9">{{ $response['failed'] ? 'Yes' : 'No' }}</div>
          </div>

          <div class="row">
            <div class="col-md-3">Server Error</div>
            <div class="col-md-9">{{ $response['serverError'] ? 'Yes' : 'No' }}</div>
          </div>

          <div class="row">
            <div class="col-md-3">Client Error</div>
            <div class="col-md-9">{{ $response['clientError'] ? 'Yes' : 'No' }}</div>
          </div>

          <div class="row">
            <div class="col-md-3">Headers</div>
            <div class="col-md-9">{{ $response['headers'] ? json_encode ($response['headers']) : 'None' }}</div>
          </div>

          <div class="row">
            <div class="col-md-3">Body</div>
            <div class="col-md-9">{{ $response['body'] ?? 'None' }}</div>
          </div>

        </div>
    </div>
  </div>
</div>

-->
