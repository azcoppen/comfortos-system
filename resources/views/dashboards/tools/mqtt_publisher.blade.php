@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Dashboard</title>
@stop

@section('app_title')
  <h4>Flight Control</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Flight Control</div>
          <div class="header-breadcrumb">
            <a href="{{ route('dashboards.index') }}"><i data-feather="briefcase"></i> Dashboards</a>
            <a href="{{ route('dashboards.tools.mqtt_publisher') }}" class="text-primary">Tools</a>
          </div>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')
  <h1 class="display-4">MQTT Publisher</h1>

  <hr />

  @livewire('tools.m-q-t-t-publisher')

@endsection
