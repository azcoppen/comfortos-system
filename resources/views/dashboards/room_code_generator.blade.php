@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Dashboard</title>
@stop

@section('app_title')
  <h4>Flight Control</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Flight Control</div>
          <div class="header-breadcrumb">
            <a href="{{ route('dashboards.index') }}"><i data-feather="briefcase"></i> Dashboards</a>
            <a href="{{ route('dashboards.operations') }}" class="text-primary">Operations</a>
          </div>
      </div>
  </div>

@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')
  <h1 class="display-4">Generate A Guest Code For A Room</h1>

  <hr />

  <div class="row">

    <div class="col-12 ">
        <div class="card card-margin">
            <div class="card-body">
              @livewire ('o-t-p.room-code')
            </div>
        </div>
    </div> <!-- end col -->
  </div>



@endsection

@push('js')
<script>
  $('.roomfinder'). selectpicker('destroy');
  $('.roomfinder').autoComplete({
    minLength: 1
  });

  $('.roomfinder').on('autocomplete.select', function(evt, item) {
    if (item && item.value)
    {
      window.livewire.emit('room', item.value);
    }
  });

</script>
@endpush
