@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Dashboard</title>
@stop

@section('app_title')
  <h4>Flight Control</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Flight Control</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="briefcase"></i> Dashboards</a>
              <a href="{{ route('dashboards.commands') }}" class="text-primary">Commands</a>
          </div>
      </div>
  </div>
    @include ('partials.components.forms.search_bar', ['text' => 'Commands'])
@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')
  <h1 class="display-4">Live Command Feed</h1>

  @if (is_object ($q_commands) && count ($q_commands) )
    <div class="row mt-3">
      @include('partials.components.panels.commands', ['commands' => $q_commands, 'search' => true])
    </div>
  @endif

    <div class="row mt-3">
      @livewire('graphs.commands-throughput-hourly-graph')
    </div>

    <div class="row mt-3">
      @livewire('graphs.commands-throughput-graph')
    </div>

    <div class="row mt-3">
      @livewire('commands-table', ['component' => 'all'])
    </div>
@endsection
