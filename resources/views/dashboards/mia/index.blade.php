@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: MIA Alerts</title>
@stop

@section('app_title')
  <h4>Flight Control</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Flight Control</div>
          <div class="header-breadcrumb">
            <a href="{{ route('dashboards.index') }}"><i data-feather="briefcase"></i> Dashboards</a>
            <a href="{{ route('dashboards.mia') }}" class="text-primary">MIA Alerts</a>
          </div>
      </div>
  </div>
  @include ('partials.components.forms.search_bar', ['text' => 'MIA Alerts'])
@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')

  <div class="row mt-3">
    @livewire('graphs.m-i-a-throughput-hourly-graph')
  </div>

  <div class="row mt-3">
    @livewire('graphs.m-i-a-throughput-graph')
  </div>

  <div class="row mt-3">
    @livewire('m-i-a-table')
  </div>
@endsection
