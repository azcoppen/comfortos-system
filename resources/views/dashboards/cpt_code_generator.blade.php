@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Dashboard</title>
@stop

@section('app_title')
  <h4>Flight Control</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Flight Control</div>
          <div class="header-breadcrumb">
            <a href="{{ route('dashboards.index') }}"><i data-feather="briefcase"></i> Dashboards</a>
            <a href="{{ route('dashboards.operations') }}" class="text-primary">Operations</a>
          </div>
      </div>
  </div>

@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')
  <h1 class="display-4">Generate A Guest Code For A Component</h1>

  <hr />

  <div class="row">

    <div class="col-12 ">
        <div class="card card-margin">
            <div class="card-body">
              @livewire ('o-t-p.component-code')
            </div>
        </div>
    </div> <!-- end col -->
  </div>



@endsection

@push('js')
<script>
  $('.cptfinder'). selectpicker('destroy');
  $('.cptfinder').autoComplete({
    minLength: 1,
    resolver: 'custom',
    events: {
        search: function (qry, callback) {
            // let's do a custom ajax call
            $.ajax(
                '{{ route ('dashboards.cptq') }}/'+$('input[name="group"]:checked').val(),
                {
                    data: { 'q': qry }
                }
            ).done(function (res) {
                callback(res)
            });
        }
    }
  });

  $('.form-check-input').on('change', function() {
    window.livewire.emit('group', $("input[name='group']:checked").val());
    $("input[type='search']").val('');
  });

  $('.cptfinder').on('autocomplete.select', function(evt, item) {
    if (item && item.value)
    {
      window.livewire.emit('cpt', item.value);
    }
  });

</script>
@endpush
