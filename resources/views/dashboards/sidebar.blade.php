
<ul class="nav">
    <li class="nav-header">Monitoring</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#dashboards" aria-expanded="false" aria-controls="dashboards">
            <i data-feather="briefcase" class="menu-icon"></i>
            <span class="menu-title">Dashboards</span>
        </a>
        <div class="collapse show" id="dashboards">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href=""><span class="menu-title">Live Signals</span></a></li>
            </ul>
        </div>
    </li>

    <li class="nav-header">Speed Dial</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#shortcuts" aria-expanded="false" aria-controls="shortcuts">
            <i data-feather="target" class="menu-icon"></i>
            <span class="menu-title">Shortcuts</span>
        </a>
        <div class="collapse show" id="shortcuts">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href=""><span class="menu-title">Atmo Motel</span></a></li>
                <li class="nav-item"><a class="nav-link" href=""><span class="menu-title">Room 150: Status</span></a></li>
                <li class="nav-item"><a class="nav-link" href=""><span class="menu-title">Room 150: Lighting</span></a></li>
            </ul>
        </div>
    </li>



</ul>
