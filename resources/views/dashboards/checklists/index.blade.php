@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Checklists</title>
@stop

@section('app_title')
  <h4>Flight Control</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Flight Control</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="briefcase"></i> Home</a>
              <a href="{{ route('dashboards.checklists.index') }}" class="">Checklists</a>
              <a href="{{ route('dashboards.checklists.index.type', $type ?? '') }}" class="text-primary">{{ Str::title($type) ?? '' }}</a>
          </div>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')
  <h1 class="display-4">{{ Str::title ($type) }} Checklist</h1>
  <hr />

  @if (! isset ($records) || ! count ($records) )
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-bordered-info" role="alert">
          <span class="alert-text">This checklist hasn't been entered yet.</span>
        </div>
      </div>
    </div>
  @else

    <div class="row">
      <div class="col-md-12">
        <ul class="list-group list-unstyled">
          @foreach ($records->sortBy('index') AS $record)
          <li class="list-group-item mb-2 p-3">

                  <span class="string-check string-check-soft-{{ $record->priority == 1 ? 'danger' : 'success' }} string-check-inline">
                      <input type="checkbox" class="form-check-input" id="{{ $record->_id }}">
                      <label class="string-check-label pl-2 {{ $record->priority == 1 ? 'text-danger' : '' }}" for="{{ $record->_id }}">
                        {{ $record->item }}
                        <br /><small class="text-muted">{{ $record->help }}</small>
                      </label>
                  </span>
          </li>
          @endforeach
        </ul>
      </div>
    </div>

  @endif
@endsection
