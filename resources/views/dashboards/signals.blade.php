@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Dashboard</title>
@stop

@section('app_title')
  <h4>Flight Control</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Flight Control</div>
          <div class="header-breadcrumb">
            <a href="{{ route('dashboards.index') }}"><i data-feather="briefcase"></i> Dashboards</a>
            <a href="{{ route('dashboards.signals') }}" class="text-primary">Signals</a>
          </div>
      </div>
  </div>
    @include ('partials.components.forms.search_bar', ['text' => 'Signals'])
@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')
  <h1 class="display-4">Live Signal Feed</h1>

    @if (is_object ($q_signals) && count ($q_signals) )
      <div class="row mt-3">
        @include('partials.components.panels.signals', ['signals' => $q_signals, 'search' => true])
      </div>
    @endif

    <div class="row mt-3">
      @livewire('graphs.signals-throughput-hourly-graph')
    </div>

    <div class="row mt-3">
      @livewire('graphs.signals-throughput-graph')
    </div>

    <div class="row mt-3">
      @livewire('signals-table', ['component' => 'all'])
    </div>
@endsection
