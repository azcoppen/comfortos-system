@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Dashboard</title>
@stop

@section('app_title')
  <h4>Flight Control</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Flight Control</div>
          <div class="header-breadcrumb">
            <a href="{{ route('dashboards.index') }}"><i data-feather="briefcase"></i> Dashboards</a>
            <a href="{{ route('dashboards.operations') }}" class="text-primary">Operations</a>
          </div>
      </div>
  </div>
  @include ('partials.components.forms.search_bar', ['text' => 'Operations'])

@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')
  <h1 class="display-4">Live Operation Queue</h1>

  <hr />

    <div class="row mt-3">
      @livewire('graphs.operations-throughput-graph')
    </div>

    <div class="row mt-3">
      @livewire('operations-table')
    </div>
@endsection
