@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Dashboard</title>
@stop

@section('app_title')
  <h4>Flight Control</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Flight Control</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="briefcase"></i> Home</a>
          </div>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')
    @include ('op-queue')
@endsection
