
<ul class="nav">
    <li class="nav-header">Identity</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#users" aria-expanded="false" aria-controls="users">
            <i data-feather="user" class="menu-icon"></i>
            <span class="menu-title">User Management</span>
        </a>
        <div class="collapse show" id="users">
            <ul class="nav flex-column sub-menu">
              @if ( $user_roles->whereIn('name', ['developer', 'superuser', 'administrator'])->count() > 0 )
                @foreach ( collect ([
                    'developer',
                    'superuser',
                    'administrator',
                    'executive',
                    'auditor',
                    'staff',
                    'sales',
                    'installer',
                    'engineer',
                    'provider',
                    'partner',
                    'analyst',
                    'editor',
                    'reviewer',
                    'agent',
                    'support',
                    'operator',
                    'property',
                    'facility',
                    'traveler',
                    'anon',
                    'guest',
                    ]) AS $role )
                <li class="nav-item {{ isset($nav_section) && $nav_section == Str::plural ($role) ? 'active' : '' }}"><a class="nav-link" href="{{ route ('idam.users.index.tag', Str::plural ($role)) }}"><span class="menu-title">{{ Str::title(Str::plural ($role)) }}</span></a></li>
                @endforeach
              @else
                <li class="nav-item active"><a class="nav-link" href="{{ route ('idam.users.index') }}"><span class="menu-title">Everyone</span></a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route ('idam.users.edit', auth()->user()->_id) }}"><span class="menu-title">Me</span></a></li>
              @endif
            </ul>
        </div>
    </li>

    @if ( $user_roles->whereIn('name', ['developer', 'superuser', 'administrator'])->count() > 0 )
    <li class="nav-header">Configuration</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#config" aria-expanded="false" aria-controls="config">
            <i data-feather="settings" class="menu-icon"></i>
            <span class="menu-title">System</span>
        </a>
        <div class="collapse show" id="config">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'roles' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('idam.roles.index') }}"><span class="menu-title">Roles</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'permissions' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('idam.permissions.index') }}"><span class="menu-title">Permissions</span></a></li>
            </ul>
        </div>
    </li>
    @endif

</ul>
