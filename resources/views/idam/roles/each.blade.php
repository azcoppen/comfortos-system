
  <tr>
      <td class="text-center">{{ $record->_id }}</td>
      <td class="text-center">{{ $record->guard_name }}</td>
      <td>{{ $record->name }}</td>
      <td class="text-center">{{ $record->permissions_count ?? 0 }}</td>
      <td class="text-center">{{ $record->users_count ?? 0 }}</td>
      <td>@include ('components.fields.date', ['field' => $record->created_at])</td>
      <td>@include ('components.fields.date', ['field' => $record->updated_at])</td>
  </tr>
