@extends('themes.architect.layouts.main')

@section ('title')
  <title>@lang ('shared.brand') - {{ isset($record) ? 'Edit Role: '.$record->name : 'Create New Role' }}</title>
@stop

@section ('header')
  @include ('themes.architect.idam.roles.header', ['button' => false])
@stop

@section ('content')
  <div class="main-card mb-3 card">
    <div class="card-body">
      <h5 class="card-title">{{ isset($record) ? 'Edit Role: '.$record->name : 'Create New Role' }}</h5>
      <hr />

      @if ( isset($record) )
        {!! Form::open(['method' => 'PUT', 'route' => ['idam.roles.update', $record->id]]) !!}
      @else
        {!! Form::open(['route' => 'idam.roles.store']) !!}
      @endif

      <div class="position-relative row form-group">
        <label for="name" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
          {!! Form::text ('name', old('name', $record->name ?? ''), ['id' => 'name', 'class' => 'form-control'.($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => 'developer', 'required']) !!}
          @if ($errors->has('name'))
            <small class="invalid-feedback">{{ $errors->first('name') }}</small>
          @endif
        </div>
      </div>

      <div class="position-relative row form-group">
        <label for="name" class="col-sm-2 col-form-label">Guard</label>
        <div class="col-sm-10">
          {!! Form::text ('guard_name', old('guard_name', $record->guard_name ?? 'api'), ['id' => 'guard_name', 'class' => 'form-control'.($errors->has('guard_name') ? ' is-invalid' : ''), 'placeholder' => 'web', 'required']) !!}
          @if ($errors->has('guard_name'))
            <small class="invalid-feedback">{{ $errors->first('guard_name') }}</small>
          @endif
        </div>
      </div>

      <div id="accordion" class="accordion-wrapper mb-3">
        <div class="card">

          @foreach ($permissions AS $group => $perms)

            <div id="heading-{{ $group }}" class="card-header">
                <button type="button" data-toggle="collapse" data-target="#{{ $group }}" aria-expanded="false" aria-controls="{{ $group }}" class="text-left m-0 p-0 btn btn-link btn-block collapsed">
                    <h5 class="m-0 p-0">{{ $group }}</h5>
                </button>
                <div class="pull-right">
                  <div class="form-check form-check-inline">
                    {!! Form::checkbox ($group.'-all', 1, false, ['id' => $group.'-all', 'class' => 'form-check-input select-all',
                      'data-style' => 'mr-3', 'data-toggle' => 'toggle',
                      'data-size' => 'mini', 'data-on' => 'All', 'data-off' => 'Default', 'data-onstyle' => 'success', 'data-offstyle' => 'info',
                      'onChange' => "toggle_all(this)", 'data-group' => $group,
                      ]) !!}
                  </div>
                </div>
            </div>
            <div data-parent="#accordion" id="{{ $group }}" aria-labelledby="heading-{{ $group }}" class="collapse show" style="">
                <div class="card-body">
                  @foreach ($perms->sort() AS $perm)
                    <div class="custom-checkbox custom-control custom-control-inline col-sm-2">
                      {!! Form::checkbox ('permissions[]', $perm->name,
                        in_array ($perm->name, old('permissions', isset($record) ? $record->permissions->pluck('name')->all() : [])) ? true : false,
                        ['id' => $perm->name, 'class' => 'custom-control-input'.($errors->has('permissions.'.$perm->name) ? ' is-invalid' : '')]) !!}
                      <label class="custom-control-label" for="{{ $perm->name }}">{{ $perm->name }}</label>
                    </div>
                  @endforeach
                </div>
            </div>
          @endforeach

        </div>

      </div>


      <div class="position-relative row form-group">
        <div class="col-sm-12 text-right">
          @include ('themes.architect.components.buttons.cancel')
          @include ('themes.architect.components.buttons.submit')
        </div>
      </div>

      {!! Form::close() !!}

    </div>
  </div>
@stop

@push('js')
  <script type="text/javascript">
    function toggle_all (obj) {
      checkboxes = document.querySelectorAll('#' +obj.getAttribute('data-group')+ ' input')
      for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = obj.checked;
      }
    }

  </script>
@endpush
