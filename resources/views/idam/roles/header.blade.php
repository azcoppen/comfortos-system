<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-diamond icon-gradient bg-grow-early">
                </i>
            </div>
            <div>Roles
                <div class="page-title-subheading">Browse, view, create, update, and delete system roles
                </div>
            </div>
        </div>
        <div class="page-title-actions">
          @if ( isset($button) && $button !== FALSE )
            @include ('themes.architect.components.buttons.create', ['route' => 'idam.roles.create', 'object' => 'Role'])
          @endif
        </div>
      </div>
</div>
