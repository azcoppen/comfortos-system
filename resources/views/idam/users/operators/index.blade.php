@extends('layouts.themes.blixy.dashboard')


@section('head')
  <title>SmartRooms:: User Operator Associations</title>
@stop

@section('app_title')
  <h4>IDAM</h4>
@stop

@section('sidebar')
  @include ('idam.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Users</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="user-plus"></i> Home</a>
              <a href="{{ route('idam.index') }}">IDAM</a>
              <a href="{{ route('idam.users.index') }}" class="text-primary">Users</a>
          </div>
      </div>
  </div>
@stop

@section ('content')
  @include ('idam.users.tabs')

  @include ('partials.alerts')

  <div class="main-card mb-3 card">
    <div class="card-body">
      <h5 class="card-title">Manage Operator Associations</h5>
      <hr />

      {!! Form::open(['route' => ['idam.users.operators.store', $record->_id]]) !!}

        <ul class="list-group">
          @foreach ($operators AS $operator)
            <li class="list-group-item">

                <div class="string-check string-check-bordered-primary mb-2">
                    {!! Form::checkbox ('operators[]', $operator->_id, $record->operators->contains ('_id', $operator->_id), ['id' => $operator->slug, 'class' => 'form-check-input']) !!}

                        <label class="string-check-label" for="{{ $operator->slug }}">
                                <span class="ml-2 text-primary">{{ $operator->label }}
                                    @if ( $operator->brands->count () > 0 )
                                        <span class="text-">({{ $operator->brands->pluck ('label')->implode (', ') }})</span>
                                    @endif
                                </span><br />
                                <small class="pl-2 text-muted">{{ collect($operator->location)->implode (', ') }}</small>
                        </label>

                </div>

            </li>
          @endforeach
        </ul>

        <div class="position-relative row form-group mt-3">
          <div class="col-sm-12 text-right">
            @include ('components.buttons.cancel')
            @include ('components.buttons.submit')
          </div>
        </div>

      {!! Form::close() !!}

    </div>
  </div>


@stop
