@extends('layouts.themes.blixy.dashboard')


@section('head')
  <title>SmartRooms:: Users</title>
@stop

@section('app_title')
  <h4>IDAM</h4>
@stop

@section('sidebar')
  @include ('idam.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Users</div>
          <div class="header-breadcrumb">
            <a href="{{ route('idam.index') }}"><i data-feather="user-plus"></i> IDAM</a>
            <a href="{{ route('idam.index') }}">Identity</a>
            <a href="{{ route('idam.users.index') }}">User Management</a>
            <a href="{{ route('idam.users.index.tag', $nav_section) }}" class="text-primary">{{ Str::title ($nav_section ?? 'All') }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('idam.users.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New User</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">ComfortOS Users</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'Users'])
@stop


@section ('content')

    <div class="row mt-3 mb-2">
        <div class="col-md-12">
            <div class="alert alert-bordered-info" role="alert">
              <i data-feather="alert-octagon" class="alert-icon"></i>
              <span class="alert-text"><strong>Important:</strong> A user must have a role assigned to it to be able to interact with ComfortOS.</span>
            </div>
        </div>
    </div>

  <div class="main-card mb-3 card">
    <div class="card-body">

      @if ( isset($records) && count ($records) )
        <div class="table-responsive">
          <table style="width: 100%;" id="" class="table table-hover">
             <thead>
             <tr>
                 <th>@sortablelink('_id', 'ID')</th>
                 <th>@sortablelink('first', 'Name')</th>
                 <th>Email</th>
                 <th class="text-center">Roles</th>
                 <th class="text-center">Perms</th>
                 <th class="text-center">Rooms</th>
                 <th>@sortablelink('created_at', 'Created')</th>
                 <th>@sortablelink('updated_at', 'Updated')</th>
             </tr>
             </thead>
             <tbody>
                 @each ('idam.users.each', $records, 'record')
             </tbody>
           </table>
       </div>

       <hr />

       <div class="row">
         <div class="col-md-6 offset-3">
           {{ $records->links() }}
         </div>
       </div>

    @else
        @include ('partials.components.alerts.empty')
    @endif

    </div>
  </div>
@stop
