<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-user icon-gradient bg-grow-early">
                </i>
            </div>
            <div>{{ isset ($record) ? $record->first . ' ' . $record->last : 'Users' }}
                <div class="page-title-subheading">Browse, view, create, update, and delete system users
                </div>
            </div>
        </div>
        <div class="page-title-actions">
          @if ( isset($button) && $button !== FALSE )
            @include ('themes.architect.components.buttons.create', ['route' => 'idam.users.create', 'object' => 'User'])
          @endif
        </div>
      </div>
</div>
