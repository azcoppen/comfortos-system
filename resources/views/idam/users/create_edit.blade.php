@extends('layouts.themes.blixy.dashboard')

@section('head')
  <title>SmartRooms:: - {{ isset($record) ? 'Edit User: '.$record->first. ' '.$record->last : 'Create New User' }}</title>
@stop

@section('app_title')
  <h4>IDAM</h4>
@stop

@section('sidebar')
  @include ('idam.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Users</div>
          <div class="header-breadcrumb">
            <a href="{{ route('idam.index') }}"><i data-feather="user-plus"></i> IDAM</a>
            <a href="{{ route('idam.index') }}">Identity</a>
            <a href="{{ route('idam.users.index') }}" class="{{ isset($record) ? '' : 'text-primary' }}">User Management</a>
            @if ( isset ($record) )
              <a href="{{ route('idam.users.show', $record->_id) }}" class="text-primary">{{ $record->full_name }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
  </div>
@stop


@section ('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $record->full_name ?? $record->_id }}</h1>
  </div>
  <hr />


  @if ( isset($record) )
    @include ('idam.users.tabs')
  @endif

   @include ('partials.alerts')

   @if ( isset($record) )
   {!! Form::open(['method' => 'PUT', 'files' => true, 'route' => ['idam.users.update', $record->_id]]) !!}
   @else
     {!! Form::open(['files' => true, 'route' => ['idam.users.store']]) !!}
   @endif

    <div class="row">



      <div class="col-sm-9">


            <div class="main-card mb-3 card">
              <div class="card-body">
                <h5 class="card-title">Personal</h5>
                <hr />

                <div class="form-row">
                    <div class="col-md-2">
                        <div class="position-relative form-group">
                          @include ('components.fields.select', [
                            'label'       => "Prefix",
                            'name'        => 'prefix',
                            'options'     => ['Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Dr' => 'Dr', 'Ms' => 'Ms', 'Prof' => 'Prof', 'Rev' => 'Rev'],
                            'value'       => $record->prefix ?? null,
                            'default'     => '',
                            'placeholder' => "Mr",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "First Name",
                            'name'        => 'first',
                            'value'       => $record->first ?? null,
                            'default'     => '',
                            'placeholder' => "John",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "Last Name",
                            'name'        => 'last',
                            'value'       => $record->last ?? null,
                            'default'     => '',
                            'placeholder' => "Doe",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "Suffix",
                            'name'        => 'suffix',
                            'value'       => $record->suffix ?? null,
                            'default'     => '',
                            'placeholder' => "",
                            'required'    => false,
                          ])
                        </div>
                    </div>
                </div>


                <div class="position-relative row form-group">
                  @include ('components.fields.text', [
                    'columns'     => true,
                    'label'       => "Display As",
                    'name'        => 'display',
                    'value'       => $record->display ?? ($record->first ?? '').' '.($record->last ?? ''),
                    'default'     => '',
                    'placeholder' => "<first_name> <last_name>",
                    'required'    => true,
                  ])
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.text', [
                    'columns'     => true,
                    'label'       => "Slug",
                    'name'        => 'slug',
                    'value'       => $record->slug ?? null,
                    'default'     => '',
                    'placeholder' => "<first_name>-<last_name>",
                    'required'    => true,
                  ])
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.email', [
                    'columns'     => true,
                    'label'       => "Email",
                    'name'        => 'email',
                    'value'       => $record->email ?? null,
                    'default'     => '',
                    'placeholder' => "someone@website.com",
                    'required'    => true,
                  ])
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.textarea', [
                    'columns'     => true,
                    'label'       => "Phone Numbers",
                    'name'        => 'telephones',
                    'value'       => collect ($record->telephones ?? [])->implode ("\n") ?? null,
                    'default'     => '',
                    'placeholder' => "Add a phone number on each line",
                    'rows'        => 3,
                    'required'    => true,
                    'help'        => 'Add one phone number per line',
                  ])
                </div>

                <div class="position-relative form-group row">
                  @include ('components.fields.radioset', [
                    'columns'     => true,
                    'label'       => "Sex",
                    'color'       => 'primary',
                    'name'        => 'sex',
                    'options'     => ['Male' => 'M', 'Female' => 'F', 'Unstated' => 'X'],
                    'value'       => $record->sex ?? null,
                    'default'     => 'M',
                    'required'    => true,
                  ])
                </div>

                <div class="position-relative form-group row">
                  @include ('components.fields.datepicker', [
                    'columns'     => true,
                    'label'       => "Date of Birth",
                    'name'        => 'dob',
                    'value'       => isset ($record->dob) && is_object ($record->dob) ? $record->dob->format ('m/d/Y') : null,
                    'default'     => '',
                    'placeholder' => "mm/dd/yyyy",
                    'required'    => true,
                    'help'        => "Date format: mm/dd/yyyy"
                  ])
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.text', [
                    'columns'     => true,
                    'label'       => "Job Title",
                    'name'        => 'job_title',
                    'value'       => $record->job_title ?? null,
                    'default'     => '',
                    'placeholder' => "Manager",
                    'required'    => true,
                  ])
                </div>

              </div>
            </div>

            <div class="main-card mb-3 card">
              <div class="card-body">
                <h5 class="card-title">Geographic</h5>
                <hr />

                <div class="form-row">
                    <div class="col-md-12">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "Street Address",
                            'name'        => 'street',
                            'value'       => $record->location['street'] ?? null,
                            'default'     => '',
                            'placeholder' => "123 Example Street",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "City",
                            'name'        => 'city',
                            'value'       => $record->location['city'] ?? null,
                            'default'     => '',
                            'placeholder' => "Example City",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "Region",
                            'name'        => 'region',
                            'value'       => $record->location['region'] ?? null,
                            'default'     => '',
                            'placeholder' => "VA",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "Postal/Zip",
                            'name'        => 'postal',
                            'value'       => $record->location['postal'] ?? null,
                            'default'     => '',
                            'placeholder' => "01234",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.select', [
                    'columns'     => true,
                    'label'       => "Country",
                    'name'        => 'country',
                    'options'     => config ('dropdowns.countries'),
                    'value'       => $record->location['country'] ?? null,
                    'default'     => 'US',
                    'placeholder' => "Choose a country",
                    'required'    => true,
                  ])
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "Longitude",
                          'name'        => 'longitude',
                          'value'       => $record->geo['coordinates'][0] ?? null,
                          'default'     => '',
                          'placeholder' => "0.00",
                          'required'    => true,
                        ])
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "Latitude",
                          'name'        => 'latitude',
                          'value'       => $record->geo['latitude'][0] ?? null,
                          'default'     => '',
                          'placeholder' => "0.00",
                          'required'    => true,
                        ])
                      </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-12 text-right">
                      <button type="button" title="Click to request browser location" class="mb-2 mr-2 btn-icon btn-icon-only btn btn-info" onClick="navigator.geolocation.getCurrentPosition(function (position) { document.getElementById('latitude').value = position.coords.latitude; document.getElementById('longitude').value = position.coords.longitude; })"><i class="pe-7s-map btn-icon-wrapper"> </i> Ask Browser (SSL-only)</button>
                    </div>
                  </div>

              </div>
            </div>

            <div class="main-card mb-3 card">
              <div class="card-body">
                <h5 class="card-title">Account</h5>
                <hr />

                <div class="position-relative row form-group">
                  @include ('components.fields.select', [
                    'columns'     => true,
                    'label'       => "Language",
                    'name'        => 'lang',
                    'options'     => config ('dropdowns.languages'),
                    'value'       => $record->lang ?? null,
                    'default'     => 'en',
                    'placeholder' => "Choose a language",
                    'required'    => true,
                  ])
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.select', [
                    'columns'     => true,
                    'label'       => "Locale",
                    'name'        => 'locale',
                    'options'     => config ('dropdowns.locales'),
                    'value'       => $record->locale ?? null,
                    'default'     => 'en_US',
                    'placeholder' => "Choose a locale",
                    'required'    => true,
                  ])
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.select', [
                    'columns'     => true,
                    'label'       => "Currency",
                    'name'        => 'currency',
                    'options'     => config ('dropdowns.currencies'),
                    'value'       => $record->currency ?? null,
                    'default'     => 'USD',
                    'placeholder' => "Choose a currency",
                    'required'    => true,
                  ])
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.select', [
                    'columns'     => true,
                    'label'       => "Timezone",
                    'name'        => 'timezone',
                    'options'     => array_combine (array_values (config ('dropdowns.timezones')), array_values (config ('dropdowns.timezones'))),
                    'value'       => $record->timezone ?? null,
                    'default'     => 'America/Los_Angeles',
                    'placeholder' => "Choose a timezone",
                    'required'    => true,
                  ])
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.password', [
                    'columns'     => true,
                    'label'       => "Password",
                    'name'        => 'password',
                    'value'       => '',
                    'default'     => '',
                    'placeholder' => "01234567890",
                    'required'    => isset ($record) ? false : true,
                  ])
                </div>

                <div class="position-relative row form-group">
                  @include ('components.fields.textarea', [
                    'columns'     => true,
                    'label'       => "Tags",
                    'name'        => 'tags',
                    'value'       => isset ($record) ? collect($record->tags)->implode ("\n") : null,
                    'default'     => '',
                    'placeholder' => "What tags should people use to find this person?",
                    'rows'        => 5,
                    'help'        => 'Add one tag per line',
                  ])
                </div>

              </div>
            </div>

            <div class="position-relative row form-group mt-3">
              <div class="col-sm-12 text-right">
                @include ('components.buttons.cancel')
                @include ('components.buttons.submit')
              </div>
            </div>



      </div> <!-- end left col -->

      <div class="col-sm-3">

        <div class="main-card mb-3 card">
          <div class="card-body">
            <h5 class="card-title">Image</h5>

            <input name="image" type="file" class="dropify" data-default-file="{{ isset ($record) && $record->image ? 'https://res.cloudinary.com/smartrooms/users/' . $record->image .'.jpg' : '' }}" data-max-file-size="5M" data-allowed-formats="portrait" data-allowed-file-extensions="jpg jpeg png" />

          </div>
        </div>


        @if ( isset($record) )
        <div class="main-card mb-3 card">
          <div class="card-body">
            <h5 class="card-title">Timestamps</h5>
            <hr />

            <ul class="list-group">
              <li class="list-group-item mb-1"><h6 class="text-muted">Created</h6> <span class="">@include ('components.fields.date', ['field' => $record->created_at])</span></li>
              <li class="list-group-item mb-1"><h6 class="text-muted">Updated</h6> <span class="">@include ('components.fields.date', ['field' => $record->updated_at])</span></li>
              <li class="list-group-item mb-1"><h6 class="text-muted">Last Login</h6> <span class="">@include ('components.fields.date', ['field' => $record->last_login_at])</span></li>
              <li class="list-group-item mb-1"><h6 class="text-muted">Last Activity</h6> <span class="">@include ('components.fields.date', ['field' => $record->last_activity_at])</span></li>
              <li class="list-group-item mb-1"><h6 class="text-muted">Password Expires</h6> <span class="">@include ('components.fields.date', ['field' => $record->pwd_expires_at])</span></li>
              <li class="list-group-item mb-1"><h6 class="text-muted">Verified</h6> <span class="">@include ('components.fields.date', ['field' => $record->email_verified_at])</span></li>
            </ul>

          </div>
        </div>
        @else
        @endif

      </div> <!-- end right col -->


    </div> <!-- end row -->

{!! Form::close() !!}

@stop
