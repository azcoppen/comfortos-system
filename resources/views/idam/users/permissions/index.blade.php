@extends('layouts.themes.blixy.dashboard')


@section('head')
  <title>SmartRooms:: User Permissions</title>
@stop

@section('app_title')
  <h4>IDAM</h4>
@stop

@section('sidebar')
  @include ('idam.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Users</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="user-plus"></i> Home</a>
              <a href="{{ route('idam.index') }}">IDAM</a>
              <a href="{{ route('idam.users.index') }}" class="text-primary">Users</a>
          </div>
      </div>
  </div>
@stop

@section ('content')
  @include ('idam.users.tabs')

  @include ('partials.alerts')

  <div class="main-card mb-3 card">
    <div class="card-body">
      <h5 class="card-title">Manage Individual Permissions</h5>
      <hr />

      <div class="alert alert-bordered-warning mt-3" role="alert">
        These permissions are in addition to those already inherited by the user from their role(s).
      </div>

      {!! Form::open(['route' => ['idam.users.permissions.store', $record->_id]]) !!}

      <div id="accordion" class="accordion mb-3">
        <div class="card">

          @foreach ($permissions->groupBy(function ($item, $key) {
              return \Str::beforeLast ($item->name, '.');
          })->sortKeys() AS $group => $perms)

            <div id="heading-{{ $group }}" class="card-header">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#{{ $group }}" aria-expanded="false" aria-controls="{{ $group }}" class="text-left m-0 p-0 btn btn-link btn-block collapsed">
                    <h5 class="m-0 p-0 text-dark">{{ $group }}</h5>
                </a>

                  <div class="string-check string-check-bordered-primary string-check-inline pull-right">
                    {!! Form::checkbox ($group.'-all', 1, false, ['id' => $group.'-all', 'class' => 'form-check-input select-all',
                      'onChange' => "toggle_all(this)", 'data-group' => $group,
                      ]) !!}
                  </div>

            </div>
            <div data-parent="#accordion" id="{{ $group }}" aria-labelledby="heading-{{ $group }}" class="collapse show" style="">
                <div class="card-body">
                    <div class="string-check-inline">
                          @foreach ($perms AS $perm)
                            <div class="string-check string-check-bordered-{{ $perm->guard_name == 'web' ? 'primary' : 'warning' }} string-check-inline col-sm-3 col-md-3 mb-2">
                              {!! Form::checkbox ('permissions[]', $perm->_id,
                                in_array ($perm->name, old('permissions', isset($record) ? $record->permissions->pluck('name')->all() : [])) ? true : false,
                                ['id' => $perm->_id, 'class' => 'form-check-input'.($errors->has('permissions.'.$perm->name) ? ' is-invalid' : '')]) !!}
                              <label class="string-check-label text-dark" for="{{ $perm->_id }}"><span class="ml-2">{{ $perm->name }}</span></label>
                            </div>
                          @endforeach
                    </div>
                </div>
            </div>
          @endforeach

        </div>

      </div>


      <hr />

      <div class="row">
        <div class="col-md-6 offset-3">
          {{ $permissions->links() }}
        </div>
      </div>

      <div class="position-relative row form-group mt-3">
        <div class="col-sm-12 text-right">
          @include ('components.buttons.cancel')
          @include ('components.buttons.submit')
        </div>
      </div>

    {!! Form::close() !!}

  </div>
</div>
@stop

@push('js')
  <script type="text/javascript">
    function toggle_all (obj) {
      checkboxes = document.querySelectorAll('#' +obj.getAttribute('data-group')+ ' input')
      for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = obj.checked;
      }
    }

  </script>
@endpush
