<nav class="mb-3">
    <div class="nav nav-tabs nav-pills nav-pills-primary nav-fill" id="nav-tab" role="tablist">

          <a role="tab" class="nav-item nav-link {{ isset($sub_section) && $sub_section == 'basics' || !isset ($sub_section) || isset($sub_section) && empty ($sub_section) ? 'active' : '' }}" id="tab-basics" href="{{ route ('idam.users.edit', [$record->_id]) }}">
              <span>Basics</span>
          </a>

          <a role="tab" class="nav-item nav-link {{ isset($sub_section) && $sub_section == 'operators' ? 'active' : '' }}" id="tab-operators" href="{{ route ('idam.users.operators.index', [$record->_id]) }}">
              <span>Operators</span>
          </a>

          <a role="tab" class="nav-item nav-link {{ isset($sub_section) && $sub_section == 'roles' ? 'active' : '' }}" id="tab-roles" href="{{ route ('idam.users.roles.index', [$record->_id]) }}">
              <span>Roles</span>
          </a>

          <a role="tab" class="nav-item nav-link {{ isset($sub_section) && $sub_section == 'permissions' ? 'active' : '' }}" id="tab-permissions" href="{{ route ('idam.users.permissions.index', [$record->_id]) }}">
              <span>Permissions</span>
          </a>

          <a role="tab" class="nav-item nav-link {{ isset($sub_section) && $sub_section == 'rooms' ? 'active' : '' }}" id="tab-rooms" href="{{ route ('idam.users.accesses.index', [$record->_id]) }}">
              <span>Rooms</span>
          </a>

          <a role="tab" class="nav-item nav-link text-muted {{ isset($sub_section) && $sub_section == 'media' ? 'active' : '' }}" id="tab-media" href="">
              <span>Media</span>
          </a>

          <a role="tab" class="nav-item nav-link text-muted {{ isset($sub_section) && $sub_section == 'notifications' ? 'active' : '' }}" id="tab-notifications" href="">
              <span>Notifications</span>
          </a>

          <a role="tab" class="nav-item nav-link {{ isset($sub_section) && $sub_section == 'notes' ? 'active' : '' }}" id="tab-notes" href="{{ route ('idam.users.notes.index', [$record->_id]) }}">
              <span>Notes</span>
          </a>


    </div>
</nav>
