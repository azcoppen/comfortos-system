@extends('layouts.themes.blixy.dashboard')


@section('head')
  <title>SmartRooms:: User Notes</title>
@stop

@section('app_title')
  <h4>IDAM</h4>
@stop

@section('sidebar')
  @include ('idam.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Users</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="user-plus"></i> Home</a>
              <a href="{{ route('idam.index') }}">IDAM</a>
              <a href="{{ route('idam.users.index') }}" class="text-primary">Users</a>
          </div>
      </div>
  </div>
@stop

@section ('content')
  @include ('idam.users.tabs')
  <div class="main-card mb-3 card">
    <div class="card-body">

      @isset($records)
        <div class="table-responsive">
          <table style="width: 100%;" id="" class="table table-hover">
             <thead>
             <tr>
                 <th class="text-left">@sortablelink('_id', 'ID')</th>
                 <th>Author</th>
                 <th>Content</th>
                 <th>@sortablelink('created_at', 'Created')</th>
             </tr>
             </thead>
             <tbody>
               @each('idam.users.notes.each', $records, 'record')
             </tbody>
           </table>
       </div>

       <hr />

       <div class="row">
         <div class="col-md-6 offset-3">
           {{ $records->links() }}
         </div>
       </div>

      @endisset

    @empty($records)
        @component ('components.alerts.info')No records available @endcomponent
    @endempty

    </div>
  </div>
@stop
