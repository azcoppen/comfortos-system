  <tr>
      <td class="text-left">{{ $record->_id }}</td>
      <td class="text-left">{{ $record->author->first ?? '' }} {{ $record->author->last ?? '' }}</td>
      <td class="text-left">{{ Str::limit ($record->content ?? '') }}</td>
      <td>@include ('components.fields.date', ['field' => $record->created_at])</td>
  </tr>
