  <tr>
      <td class="text-center">{{ $record->id}}</td>
      <td class="text-left">{{ $record->disk ?? '' }}</td>
      <td class="text-left">{{ $record->collection_name ?? '' }}</td>
      <td class="text-left">{{ Str::limit ($record->name ?? '', 20) }}</td>
      <td class="text-left">{{ Str::limit ($record->file_name ?? '', 20) }}</td>
      <td class="text-left">{{ Str::limit ($record->mime_type ?? '', 20) }}</td>
      <td>@include ('themes.architect.components.fields.date', ['field' => $record->created_at])</td>
      <td class="text-center">
        @include ('themes.architect.components.buttons.destroy', ['route' => 'idam.users.media.destroy', 'key' => 'id', 'record' => $record, 'parent' => auth()->user() ])
      </td>
  </tr>
