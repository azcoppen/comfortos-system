  <tr>
      <td>{{ $record->_id }}</td>
      <td><a href="{{ route ('idam.users.edit', $record->_id) }}">{{ $record->first }} {{ $record->last }}</a></td>
      <td>{{ Str::limit($record->email, 20) }}</td>
      <td class="text-center">{{ count ($record->roles) ?? 0 }}</td>
      <td class="text-center">{{ count ($record->permissions) ?? 0 }}</td>
      <td class="text-center">{{ count ($record->rooms) ?? 0 }}</td>
      <td>{{ $record->created_at->diffForHumans() }}</td>
      <td>{{ $record->updated_at->diffForHumans() }}</td>
  </tr>
