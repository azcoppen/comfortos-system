<li class="timeline-item">
        <div class="timeline-header-container">
                <div class="timeline-icon bg-soft-{{ $access->end_at->timestamp > time() ? 'success' : 'dark' }}">
                        <i class="text-{{ $access->end_at->timestamp > time() ? 'success' : 'dark' }}" data-feather="calendar"></i>
                </div>
                <div class="timeline-header">
                        <a class="text-{{ $access->end_at->timestamp > time() ? 'success' : 'dark' }}" href="{{ route ('idam.users.accesses.edit', [$access->grantee_id, $access->_id]) }}">Room #{{ $access->accessible->number }}, {{ $access->accessible->building->property->title ?? '' }} ({{ $access->start_at->format ('D M d Y') }})</a>
                        <span>{{ $access->end_at->longAbsoluteDiffForHumans ($access->start_at) }}</span>
                </div>
        </div>
        <div class="timeline-content">
                {{ $access->accessible->building->label ?? '' }}, {{ $access->accessible->building->property->label ?? '' }},
                {!! collect ($access->accessible->building->property->location)->implode (', ') !!} <br />
        </div>
        <div class="timeline-sub-content">
            {{ $access->start_at->format ('D M d Y H:i') }} UTC ({{ $access->start_at->diffForHumans() }}) --> {{ $access->end_at->format ('D M d Y H:i') }} UTC ({{ $access->end_at->diffForHumans() }}).
            @if ($access->end_at->timestamp > time())
            <a class="btn btn-danger float-right" href="{{ route ('idam.users.accesses.halt', [$access->grantee_id, $access->_id]) }}">End Access (Now)</a>
            @endif
        </div>
</li>
