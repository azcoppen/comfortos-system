@extends('layouts.themes.blixy.dashboard')


@section('head')
  <title>SmartRooms:: User Room Access</title>
@stop

@section('app_title')
  <h4>IDAM</h4>
@stop

@section('sidebar')
  @include ('idam.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Users</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="user-plus"></i> Home</a>
              <a href="{{ route('idam.index') }}">IDAM</a>
              <a href="{{ route('idam.users.index') }}" class="text-primary">Users</a>
          </div>
      </div>
  </div>
@stop

@section ('content')
  @include ('idam.users.tabs')

  @include ('partials.alerts')

  <div class="main-card mb-3 card">
    <div class="card-body">
      <h5 class="card-title">{{ isset ($access) ? 'Modify' : 'Create New' }} Room Access</h5>
      <hr />

      <div class="alert alert-bordered-info" role="alert">
        <i data-feather="alert-octagon" class="alert-icon"></i>
        <span class="alert-text"><strong>Important:</strong> A user can only have <strong>one access to a room at a time</strong> (although they nay be able to access multiple rooms simultaneously). A new access record will void any others. You can revoke access by setting the end time to a date/time in the past.</span>
      </div>

      @if ( isset ($access) )
          {!! Form::open(['method' => 'PATCH', 'route' => ['idam.users.accesses.update', $record->_id, $access->_id]]) !!}
      @else
      {!! Form::open(['route' => ['idam.users.accesses.store', $record->_id]]) !!}
      @endif

      <div class="position-relative row form-group">
        <label for="slug" class="col-sm-2 col-form-label">Property/Room</label>
        <div class="col-sm-10">
            {!! Form::select ('room_id', $properties, old ('room_id', $access->accessible_id ?? NULL), ['id'=>'room_id', 'class' => 'selectpicker'.($errors->has('room_id') ? ' is-invalid' : '')]) !!}
            @if ($errors->has('room_id'))
              <small class="invalid-feedback">{{ $errors->first('room_id') }}</small>
            @endif
        </div>
      </div>

          <div class="form-row">
              <div class="col-md-6">
                  <div class="position-relative form-group">
                    <label for="start_at" class="">When should the access start?</label>
                    <div class="">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                    <span class="input-group-text" id=""><i class="fa fa-calendar text-success"></i></span>
                            </div>
                            {!! Form::text ('start_at_date', old('start_at_date', isset($access) && is_object($access) ? $access->start_at->copy()->timezone($access->accessible->building->property->timezone)->format ('m/d/Y') : now()->timezone(auth()->user()->timezone)->format('m/d/Y')), ['id' => 'start_at_date', 'class' => 'form-control'.($errors->has('start_at_date') ? ' is-invalid' : ''), 'placeholder' => '', 'required']) !!}
                            @if ($errors->has('start_at_date'))
                              <small class="invalid-feedback">{{ $errors->first('start_at_date') }}</small>
                            @endif
                        </div>
                    </div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="position-relative form-group">
                    <label for="start_at" class="text-muted float-right">Operator's local timezone</label>
                    <div class="">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                    <span class="input-group-text" id=""><i class="fa fa-clock text-success"></i></span>
                            </div>
                            {!! Form::text ('start_at_time', old('start_at_time', isset($access) && is_object($access) ? $access->start_at->copy()->timezone($access->accessible->building->property->timezone)->format ('H:i:s') : now()->timezone(auth()->user()->timezone)->format('H:i:s')), ['id' => 'start_at_time', 'class' => 'form-control'.($errors->has('start_at_time') ? ' is-invalid' : ''), 'placeholder' => '', 'required']) !!}
                            @if ($errors->has('start_at_time'))
                              <small class="invalid-feedback">{{ $errors->first('start_at_time') }}</small>
                            @endif
                        </div>
                    </div>
                  </div>
              </div>
          </div>

        <hr />
        <br />

        <div class="form-row">
            <div class="col-md-6">
              <div class="position-relative form-group">
                <label for="end_at" class="">When should the access end? </label>
                <div class="">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                <span class="input-group-text" id=""><i class="fa fa-calendar text-danger"></i></span>
                        </div>
                        {!! Form::text ('end_at_date', old('end_at_date', isset($access) && is_object($access) ? $access->end_at->copy()->timezone($access->accessible->building->property->timezone)->format ('m/d/Y') : now()->timezone(auth()->user()->timezone)->addDays(1)->format ('m/d/Y')), ['id' => 'end_at_date', 'class' => 'form-control'.($errors->has('end_at_date') ? ' is-invalid' : ''), 'placeholder' => '', 'required']) !!}
                        @if ($errors->has('end_at_date'))
                          <small class="invalid-feedback">{{ $errors->first('end_at_date') }}</small>
                        @endif
                    </div>
                </div>
              </div>
          </div>
          <div class="col-md-6">
            <div class="position-relative form-group">
              <label for="end_at" class="text-muted float-right">Operator's local timezone</label>
              <div class="">
                  <div class="input-group">
                      <div class="input-group-prepend">
                              <span class="input-group-text" id=""><i class="fa fa-clock text-danger"></i></span>
                      </div>
                      {!! Form::text ('end_at_time', old('end_at_time', isset($access) && is_object($access) ? $access->end_at->copy()->timezone($access->accessible->building->property->timezone)->format ('H:i:s') : now()->addDays(1)->timezone(auth()->user()->timezone)->format ('H:i:s')), ['id' => 'end_at_time', 'class' => 'form-control'.($errors->has('end_at_time') ? ' is-invalid' : ''), 'placeholder' => '', 'required']) !!}
                      @if ($errors->has('end_at_time'))
                        <small class="invalid-feedback">{{ $errors->first('end_at_time') }}</small>
                      @endif
                  </div>
              </div>
            </div>
        </div>
        </div>


      <div class="position-relative row form-group mt-3">
        <div class="col-sm-12 text-right">
          @include ('components.buttons.cancel')
          @include ('components.buttons.submit')
        </div>
      </div>

    {!! Form::close() !!}

  </div>
</div>

@stop

@push ('js')
<script>
(function($) {
    'use strict';
    $(function () {
        $.fn.timepicker.defaults = $.extend(true, {}, $.fn.timepicker.defaults, {
            icons: {
                up: 'menu-up',
                down: 'menu-down'
            }
        });

        $('#start_at_date').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-100y',
            autoclose: true
        });

        $('#end_at_date').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-100y',
            autoclose: true
        });

        $('#start_at_time').timepicker({
            minuteStep: 1,
            showSeconds: true,
            showMeridian: false,
            defaultTime: false
        });

        $('#end_at_time').timepicker({
            minuteStep: 1,
            showSeconds: true,
            showMeridian: false,
            defaultTime: false
        });
    });
})(jQuery);

</script>
@endpush
