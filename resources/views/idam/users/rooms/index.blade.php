@extends('layouts.themes.blixy.dashboard')


@section('head')
  <title>SmartRooms:: User Room Access</title>
@stop

@section('app_title')
  <h4>IDAM</h4>
@stop

@section('sidebar')
  @include ('idam.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Users</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="user-plus"></i> Home</a>
              <a href="{{ route('idam.index') }}">IDAM</a>
              <a href="{{ route('idam.users.index') }}" class="text-primary">Users</a>
          </div>
      </div>
  </div>
@stop

@section ('content')
  @include ('idam.users.tabs')

  @include ('partials.alerts')

  <div class="row mb-2">
      <div class="col-md-9">
          <div class="alert alert-bordered-warning" role="alert">
            <i data-feather="alert-triangle" class="alert-icon"></i>
            <span class="alert-text"><strong>Careful:</strong> Giving a user access to a room will allow them to control <strong>ALL/ANY of its installed devices</strong>.</span>

          </div>
      </div>
      <div class="col-md-3 align-right">
          <a class="btn btn-lg btn-primary float-right mt-2" href="{{ route ('idam.users.accesses.create', [$record->_id]) }}"><i data-feather="plus-circle" class="alert-icon"></i> Add New Room Access</a>
      </div>
  </div>

  <div class="main-card mb-3 card">
    <div class="card-body">

      <h5 class="card-title">Accesses</h5>
      <hr />
      @if ( isset ($records) && count ($records) )

      <ul class="timeline">
          @each ('idam.users.rooms.each', $records, 'access')
      </ul>


      <hr />

      <div class="row">
        <div class="col-md-6 offset-3">
          {{ $records->links() }}
        </div>
      </div>


      @else
          @include ('partials.components.alerts.empty')
      @endif

    </div>
  </div>


@stop
