@extends('layouts.themes.blixy.dashboard')


@section('head')
  <title>SmartRooms:: User Roles</title>
@stop

@section('app_title')
  <h4>IDAM</h4>
@stop

@section('sidebar')
  @include ('idam.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Users</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="user-plus"></i> Home</a>
              <a href="{{ route('idam.index') }}">IDAM</a>
              <a href="{{ route('idam.users.index') }}" class="text-primary">Users</a>
          </div>
      </div>
  </div>
@stop

@section ('content')
  @include ('idam.users.tabs')

  @include ('partials.alerts')

  <div class="main-card mb-3 card">
    <div class="card-body">

      <h5 class="card-title">Manage Roles</h5>
      <hr />

      {!! Form::open(['route' => ['idam.users.roles.store', $record->_id]]) !!}

        <ul class="list-group">
          @foreach ($roles AS $role)
            <li class="list-group-item">

                <div class="string-check string-check-bordered-{{ $role->guard_name == 'web' ? 'primary' : 'warning' }} mb-2">
                    {!! Form::checkbox ('roles[]', $role->_id, $record->roles->contains ('name', $role->name), ['id' => $role->_id, 'class' => 'form-check-input']) !!}

                        <label class="string-check-label" for="{{ $role->_id }}">
                                <span class="ml-2 text-dark">{{ $role->name }} <span class="float-right badge badge-soft-{{ $role->guard_name == 'web' ? 'primary' : 'warning' }}">{{ $role->guard_name }}</span></span>
                        </label>

                </div>

            </li>
          @endforeach
        </ul>

        <div class="position-relative row form-group mt-3">
          <div class="col-sm-12 text-right">
            @include ('components.buttons.cancel')
            @include ('components.buttons.submit')
          </div>
        </div>

      {!! Form::close() !!}

    </div>
  </div>


@stop
