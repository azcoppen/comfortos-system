@extends('themes.architect.layouts.main')

@section ('title')
  <title>@lang ('shared.brand') - Users</title>
@stop

@section ('header')
  @include ('themes.architect.idam.users.header')
@stop

@section ('content')
  @include ('themes.architect.idam.users.tabs')
  <div class="main-card mb-3 card">
    <div class="card-body">

      @isset($records)
        <div class="table-responsive">
          <table style="width: 100%;" id="" class="table table-hover table-striped table-bordered">
             <thead>
             <tr>
                 <th class="text-center">@sortablelink('id', 'ID')</th>
                 <th>@sortablelink('log_name', 'Log')</th>
                 <th>@sortablelink('subject_type', 'Subject')</th>
                 <th>@sortablelink('description', 'Desc')</th>
                 <th>CH</th>
                 <th>@sortablelink('created_at', 'Created')</th>
                 <th></th>
             </tr>
             </thead>
             <tbody>
               @each('themes.architect.idam.users.activity.each', $records, 'record')
             </tbody>
           </table>
       </div>

       <hr />

       <div class="row">
         <div class="col-md-6 offset-3">
           {{ $records->links() }}
         </div>
       </div>

      @endisset

    @empty($records)
        @component ('themes.architect.components.alerts.info')No records available @endcomponent
    @endempty

    </div>
  </div>
@stop
