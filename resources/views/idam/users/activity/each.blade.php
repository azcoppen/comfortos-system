  <tr>
      <td class="text-center"><a href="{{ route('idam.activity.show', $record->id) }}">{{ $record->id }}</a></td>
      <td class="text-left">{{ $record->log_name }}</td>
      <td>
        @if ( $record->subject_type == get_class(auth()->user()) && $record->subject_id == auth()->user()->user_id )
          <span class="mb-2 mr-2 badge badge-info">SELF</span>
        @else
          {{ str_replace('WGTS\\Models\\', '', $record->subject_type) }} : <strong>{{ $record->subject_id }}</strong>
        @endif
      </td>
      <td class="text-left">{{ $record->description ?? '' }}</td>
      <td class="text-center">{{ count($record->changes) ?? 0 }}</td>
      <td>@include ('themes.architect.components.fields.date', ['field' => $record->created_at])</td>
      <td class="text-center">
        @include ('themes.architect.components.buttons.destroy', ['route' => 'idam.users.activity.destroy', 'key' => 'id', 'record' => $record, 'parent' => auth()->user() ])
      </td>
  </tr>
