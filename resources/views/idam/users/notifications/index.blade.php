@extends('themes.architect.layouts.main')

@section ('title')
  <title>@lang ('shared.brand') - Users</title>
@stop

@section ('header')
  @include ('themes.architect.idam.users.header')
@stop

@section ('content')
  @include ('themes.architect.idam.users.tabs')
@stop
