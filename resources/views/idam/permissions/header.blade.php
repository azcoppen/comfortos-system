<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-shield icon-gradient bg-grow-early">
                </i>
            </div>
            <div>Permissions
                <div class="page-title-subheading">Browse, view, create, update, and delete system permissions
                </div>
            </div>
        </div>
        <div class="page-title-actions">
          @if ( isset($button) && $button !== FALSE )
            @include ('themes.architect.components.buttons.create', ['route' => 'idam.permissions.create', 'object' => 'Permission'])
          @endif
        </div>
      </div>
</div>
