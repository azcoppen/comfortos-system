@extends('themes.architect.layouts.main')

@section ('title')
  <title>@lang ('shared.brand') - {{ isset($record) ? 'Edit Permission '.$record->name : 'Create New Permission' }} </title>
@stop

@section ('header')
  @include ('themes.architect.idam.permissions.header', ['button' => false])
@stop

@section ('content')
  <div class="main-card mb-3 card">
    <div class="card-body">
      <h5 class="card-title">{{ isset($record) ? 'Edit Permission '.$record->name : 'Create New Permission' }}</h5>
      <hr />

      @if ( isset($record) )
        {!! Form::open(['method' => 'PUT', 'route' => ['idam.permissions.update', $record->id]]) !!}
      @else
        {!! Form::open(['route' => 'idam.permissions.store']) !!}
      @endif

      @if ( isset($record) )
        <div class="position-relative row">
          <label for="name" class="col-sm-2 col-form-label">ID</label>
          <div class="col-sm-10 pl-4">
            <strong>{{ $record->id }}</strong>
          </div>
        </div>
      @endif

      <div class="position-relative row form-group">
        <label for="name" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
          {!! Form::text ('name', old('name', $record->name ?? NULL), ['id' => 'name', 'class' => 'form-control'.($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => 'object.ability', 'required']) !!}
          @if ($errors->has('guard_name'))
            <small class="col-sm-12 invalid-feedback">{{ $errors->first('name') }}</small>
          @endif
        </div>
      </div>

      <div class="position-relative row form-group">
        <label for="name" class="col-sm-2 col-form-label">Guard</label>
        <div class="col-sm-10">
          {!! Form::text ('guard_name', old('guard_name', $record->guard_name ?? 'api'), ['id' => 'guard_name', 'class' => 'form-control'.($errors->has('guard_name') ? ' is-invalid' : ''), 'placeholder' => 'web', 'required']) !!}
          @if ($errors->has('guard_name'))
            <small class="invalid-feedback">{{ $errors->first('guard_name') }}</small>
          @endif
        </div>
      </div>

      <div class="position-relative row form-group">
        <div class="col-sm-12 text-right">
          @include ('themes.architect.components.buttons.cancel')
          @include ('themes.architect.components.buttons.submit')
        </div>
      </div>

      {!! Form::close() !!}

    </div>
  </div>
@stop
