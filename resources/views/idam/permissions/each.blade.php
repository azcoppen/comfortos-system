  <tr>
      <td class="text-center">{{ $record->id }}</td>
      <td class="text-center">{{ $record->guard_name }}</td>
      <td>{{ $record->name }}</td>
      <td class="text-center">{{ $record->roles_count ?? 0 }}</td>
      <td class="text-center">{{ $record->users_count ?? 0 }}</td>
      <td>@include ('components.fields.date', ['field' => $record->created_at])</td>
      <td>@include ('components.fields.date', ['field' => $record->updated_at])</td>
  </tr>
