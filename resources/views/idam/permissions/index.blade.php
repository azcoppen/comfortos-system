@extends('layouts.themes.blixy.dashboard')

@section ('head')
  <title>SmartRooms:: - Permissions</title>
@stop

@section('app_title')
  <h4>IDAM</h4>
@stop

@section('sidebar')
  @include ('idam.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Permissions</div>
          <div class="header-breadcrumb">
            <a href="{{ route('idam.index') }}"><i data-feather="repeat"></i> IDAM</a>
            <a href="{{ route('idam.roles.index') }}">Configuration</a>
            <a href="{{ route('idam.permissions.index') }}" class="text-primary">Permissions</a>
          </div>
      </div>
  </div>
@stop

@section ('content')
  <div class="main-card mb-3 card">
    <div class="card-body">

      @isset($records)
        <div class="table-responsive">
          <table style="width: 100%;" id="" class="table table-hover">
             <thead>
             <tr>
                 <th class="text-left">@sortablelink('_id', 'ID')</th>
                 <th class="text-center">@sortablelink('guard_name', 'Guard')</th>
                 <th>@sortablelink('name', 'Name')</th>
                 <th class="text-center">Roles</th>
                 <th class="text-center">Users</th>
                 <th>@sortablelink('created_at', 'Created')</th>
                 <th>@sortablelink('updated_at', 'Updated')</th>
             </tr>
             </thead>
             <tbody>
               @each('idam.permissions.each', $records, 'record')
             </tbody>
           </table>
       </div>

       <hr />

       <div class="row">
         <div class="col-md-6 offset-3">
           {{ $records->links() }}
         </div>
       </div>

      @endisset

    @empty($records)
        @component ('components.alerts.info')No records available @endcomponent
    @endempty

    </div>
  </div>
@stop
