  <tr>
      <td class="text-center"><a href="{{ route('idam.activity.show', $record->id) }}">{{ $record->id }}</a></td>
      <td class="text-left">{{ $record->log_name }}</td>
      <td>{{ $record->causer->first ??'' }} {{ $record->causer->last ??'' }}</td>
      <td>{{ str_replace('WGTS\\Models\\', '', $record->subject_type) }} : <strong>{{ $record->subject_id }}</strong></td>
      <td class="text-left">{{ $record->description ?? '' }}</td>
      <td class="text-center">{{ count($record->changes) ?? 0 }}</td>
      <td>@include ('themes.architect.components.fields.date', ['field' => $record->created_at])</td>
      <td class="text-center">
        @include ('themes.architect.components.buttons.destroy', ['route' => 'idam.activity.destroy', 'key' => 'id', 'record' => $record])
      </td>
  </tr>
