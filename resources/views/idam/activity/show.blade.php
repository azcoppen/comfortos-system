@extends('themes.architect.layouts.main')

@section ('title')
  <title>@lang ('shared.brand') - Activity</title>
@stop

@section ('header')
  @include ('themes.architect.idam.activity.header')
@stop

@section ('content')
  <div class="main-card mb-3 card">
    <div class="card-body">
      @isset($record)
        @include ('themes.architect.components.object', ['record' => $record])
      @endisset
    </div>
  </div>
@stop
