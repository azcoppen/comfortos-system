<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-graph2 icon-gradient bg-grow-early">
                </i>
            </div>
            <div>Activity
                <div class="page-title-subheading">View activity performed by system users
                </div>
            </div>
        </div>
      </div>
</div>
