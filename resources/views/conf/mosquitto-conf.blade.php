user root
allow_anonymous false
password_file /etc/mosquitto/passwd

listener 1883 localhost

listener 1883 10.8.0.2

listener 8883 0.0.0.0

cafile /etc/letsencrypt/live/{{ $property->_id }}-broker.smartrooms.cloud/chain.pem
certfile /etc/letsencrypt/live/{{ $property->_id }}-broker.smartrooms.cloud/cert.pem
keyfile  /etc/letsencrypt/live/{{ $property->_id }}-broker.smartrooms.cloud/privkey.pem
