[Unit]
Description=SmartRooms MQTT State Listener (Property {{ $property->_id }})

[Service]
Restart=always
ExecStart=/root/smartrooms-mqtt-cli/builds/smartrooms-cli listen

[Install]
WantedBy=default.target
