{
  "v3_use_offset": true,
  "admin" : true,
  "token_hmac_secret_key": "{{ $ws_broker->hmac_secret }}",
  "admin_password": "{{ $ws_broker->admin_pass }}",
  "admin_secret": "{{ $ws_broker->admin_secret }}",
  "api_key": "{{ $ws_broker->api_key }}",
  "allowed_origins": ["*"]
}
