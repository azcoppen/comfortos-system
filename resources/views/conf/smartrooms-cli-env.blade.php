MQTT_HOST=localhost
MQTT_PORT=1883
MQTT_USERNAME=={{ $property->_id }}
MQTT_PASSWORD={{ strrev ($property->_id) }}
MQTT_TOPIC="{{ $property->_id }}/#"
MQTT_CLIENT=cli-{{ $property->_id }}

QUEUE_CONNECTION=sync
REDIS_HOST=127.0.0.1

DB_HOST=10.8.10.1
DB_DATABASE=smartrooms_cloud_prod
DB_USERNAME={{ $property->_id }}
DB_PASSWORD={{ strrev ($property->_id) }}

CENTRIFUGE_URL=http://127.0.0.1:8000
CENTRIFUGE_API_KEY=ADDTHISKEYFORWSBROKER
