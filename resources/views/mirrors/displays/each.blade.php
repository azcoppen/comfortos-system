<tr>
    <td><i class="fa fa-sun text-muted"></i></td>
    <td><a href="{{ route('mirrors.displays.show', $record->_id) }}">{{ $record->_id }}</a></td>
    <td>{{ $record->label }}</td>
    <td>{{ $record->engine }}</td>
    <td>
        @if (is_object ($record->room))
            {{ $record->room->label ?? '' }}
        @else
            <span class="text-warning"><i class="fa fa-question-circle"></i> Not provisioned</span>
        @endif
    </td>
    <td class="text-center">
        @if ( isset($record->modules) && count($record->modules) > 0 )
        <span class="badge badge-soft-success">{{ count($record->modules) }}</span>
        @endif
    </td>
    <td>{{ $record->created_at->diffForHumans() }}</td>
    <td>
        @if ( $record->vpn_ip )
        <a title="Launch admin panel" target="_blank" href="https://{{ $record->vpn_ip }}"><i class="fa fa-shield-alt" title="VPN Admin"></i></a>
        @endif
    </td>
</tr>
