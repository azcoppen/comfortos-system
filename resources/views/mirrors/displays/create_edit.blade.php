@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Smart Mirrors</title>
@stop

@section('app_title')
  <h4>Smart Mirrors</h4>
@stop

@section('sidebar')
  @include ('mirrors.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Displays</div>
          <div class="header-breadcrumb">
            <a href="{{ route('mirrors.index') }}"><i data-feather="sun"></i> Smart Mirrors</a>
            <a href="{{ route('mirrors.index') }}">Hardware</a>
            <a href="{{ route('mirrors.displays.index') }}">Boards</a>
            <a href="{{ route('mirrors.displays.index') }}" class="{{ isset($display) ? '' : 'text-primary' }}">Displays</a>
            @if ( isset($display) )
              <a href="{{ route('mirrors.displays.show', $display->_id) }}" class="text-primary">{{ $display->label }}</a>
            @else
              <a href="">Create</a>
            @endif
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('mirrors.displays.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('content')
  <h1 class="display-4">{{ isset ($display) ? 'Update '.$display->label : 'Add A New Mirror Display' }}</h1>
  <hr />
  @if ( isset ($display) )
    {!! Form::open (['method' => 'PUT', 'route' => ['mirrors.displays.update', $display->_id]]) !!}
  @else
    {!! Form::open (['route' => 'mirrors.displays.store']) !!}
  @endif

  <div class="row">
    <div class="col-sm-12">
      <div class="alert alert-dark" role="alert">
        <div class="alert-text">
            Before you add or update a mirror, run <code>cat /proc/cpuinfo</code>, then <code>cat /etc/*-release</code>, and finally <code>openhab-cli info</code> to get the board's internal details.
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Operating System</h5>
            </div>
            <div class="card-body">

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What should the mirror be called?",
                        'name'        => 'label',
                        'value'       => $display->label ?? null,
                        'default'     => 'Raspbian MagicMirror-2 server',
                        'placeholder' => "Name of the mirror",
                        'required'    => true,
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What is the operating system (image)?",
                        'name'        => 'os',
                        'value'       => $display->os ?? null,
                        'default'     => 'MagicMirrorOS',
                        'placeholder' => "Name of the operating system",
                        'required'    => true,
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "Which operating system release is it?",
                        'name'        => 'release',
                        'value'       => $display->os ?? null,
                        'default'     => 'Raspbian GNU/Linux 10 (buster)',
                        'placeholder' => "Name of the operating system release",
                        'required'    => true,
                        'help'        => "cat /etc/*-release | grep PRETTY_NAME | cut -d '=' -f 2",
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What's the release's codename?",
                        'name'        => 'codename',
                        'value'       => $display->release ?? null,
                        'default'     => 'Buster',
                        'placeholder' => "Codename of the operating system release",
                        'required'    => true,
                        'help'        => "cat /etc/*-release | grep VERSION_CODENAME | cut -d '=' -f 2",
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What kernel version is it using?",
                        'name'        => 'kernel',
                        'value'       => $display->kernel ?? null,
                        'default'     => 'Linux 5.4.72-v7l+',
                        'placeholder' => "Kernel installed inside the operating system",
                        'required'    => true,
                        'help'        => 'uname -r',
                      ])
                    </div>

            </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="box"></i> Hardware</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What model is it?",
                    'name'        => 'model',
                    'value'       => $display->model ?? null,
                    'default'     => 'Raspberry Pi 4 Model B Rev 1.2',
                    'placeholder' => "Model name of the hardware",
                    'required'    => true,
                    'help'        => "cat /proc/cpuinfo | grep Model | cut -d ' ' -f 2",
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's its serial number?",
                    'name'        => 'serial',
                    'value'       => $display->serial ?? null,
                    'default'     => '',
                    'placeholder' => "Serial number of the CPU (e.g. 1000000000000000)",
                    'required'    => true,
                    'help'        => "cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2",
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's its hardware ID?",
                    'name'        => 'hardware',
                    'value'       => $display->hardware ?? null,
                    'default'     => '',
                    'placeholder' => "Hardware number of the board (e.g. BCM0000)",
                    'required'    => true,
                    'help'        => "cat /proc/cpuinfo | grep Hardware | cut -d ' ' -f 2",
                  ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What's its network MAC address?",
                      'name'        => 'mac',
                      'value'       => $display->mac ?? null,
                      'default'     => '',
                      'placeholder' => "MAC address of the network adaptor (e.g. dc:a6:32:96:f7:26)",
                      'required'    => true,
                      'help'        => "cat /sys/class/net/eth0/address",
                    ])
                  </div>

              </div>
        </div>
      </div> <!-- end col -->

    </div> <!-- end row -->

    <div class="row">

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="wifi"></i> Network</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.select', [
                    'label'       => "Which router is it plugged into?",
                    'name'        => 'router_id',
                    'options'     => $routers->pluck ('label', '_id')->all(),
                    'value'       => $display->router_id ?? null,
                    'default'     => '',
                    'placeholder' => "Choose a router to attach the mirror to",
                    'required'    => false,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's it's network hostname?",
                    'name'        => 'host',
                    'value'       => $display->host ?? null,
                    'default'     => 'smartrooms-mirror',
                    'placeholder' => "Network hostname of the mirror",
                    'required'    => true,
                    'help'        => "hostname",
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What's it's internal DHCP address from the router?",
                    'name'        => 'int_ip',
                    'value'       => $display->int_ip ?? null,
                    'default'     => '',
                    'placeholder' => "IP address of the mirror",
                    'required'    => true,
                    'help'        => "hostname -I | awk '{print $1}'",
                  ])
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "What's the main SSH login username?",
                            'name'        => 'ssh_user',
                            'value'       => $display->ssh_user ?? null,
                            'default'     => 'magicmirror',
                            'placeholder' => "Username to log into the mirror with",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "What's the SSH password?",
                            'name'        => 'ssh_pass',
                            'value'       => isset ($display) && isset ($display->ssh_pass) ? Crypt::decrypt($display->ssh_pass) : null,
                            'default'     => 'magicmirror',
                            'placeholder' => "Password to log into the mirror with",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                  </div>


              </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="package"></i> Engine</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.select', [
                    'label'       => "Which software engine is it running?",
                    'name'        => 'engine',
                    'options'     => ['MagicMirror-2' => 'MagicMirror-2'],
                    'value'       => $display->engine ?? null,
                    'default'     => 'MagicMirror-2',
                    'placeholder' => "Choose a software engine",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "Which version/distribution of the software engine is it?",
                    'name'        => 'dist',
                    'value'       => $display->dist ?? null,
                    'default'     => '2.12.0',
                    'placeholder' => "Internal version of the software",
                    'required'    => true,
                    'help'        => "openhab-cli info",
                  ])
                </div>

              </div>
        </div>
      </div> <!-- end col -->


      @include ('partials.components.forms.vpn', [
        'entity'  => $display ?? null,
        'vpns'    => $vpns,
      ])


    </div> <!-- end row -->

    <div class="row">

      <div class="col-lg-12 col-md-12 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="layers"></i> Modules</h5>
              </div>
              <div class="card-body">
                <div class="string-check-inline">

                  @foreach ($modules AS $module)
                  <div class="string-check string-check-bordered-primary string-check-inline col-sm-3 mb-2">
                    {!! Form::checkbox ('modules[]', $module->_id, true, ['id' => 'modules-'.$module->_id, 'class' => 'form-check-input']) !!}
                      <label class="string-check-label" for="modules-{{ $module->_id }}" title="{{ $module->description }}">
                          <span class="ml-2">{{ $module->module }}</span>
                      </label>
                  </div>
                  @endforeach

                </div>
              </div>
          </div>
      </div>

    </div>


    <div class="row">

      @include ('partials.components.forms.ordering', [
        'entity'  => $display ?? null,
        'users'   => $users,
      ])

      @include ('partials.components.forms.installation', [
        'entity'  => $display ?? null,
        'users'   => $users,
      ])

      @include ('partials.components.forms.provisioning', [
        'entity'  => $display ?? null,
        'users'   => $users,
      ])


    </div> <!-- end row -->


  <hr />
  <div class="row mb-4">
    @if ( !isset ($display) )
      @include ('partials.components.forms.clone')
    @endif

    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}


  @if ( isset ($display) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $display,
      'text'    => 'Mirror',
      'destroy' => [
        'route' => 'mirrors.displays.destroy',
        'params'=> [$display->_id],
      ],
      'return'  => [
        'route' => 'mirrors.displays.index',
        'params'=> [$display->_id],
      ],
    ])
  @endif

@endsection
