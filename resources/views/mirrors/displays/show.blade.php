@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Smart Mirrors</title>
@stop

@section('app_title')
  <h4>Smart Mirrors</h4>
@stop

@section('sidebar')
  @include ('mirrors.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Boards</div>
          <div class="header-breadcrumb">
            <a href="{{ route('mirrors.index') }}"><i data-feather="sun"></i> Smart Mirrors</a>
            <a href="{{ route('mirrors.index') }}">Hardware</a>
            <a href="{{ route('mirrors.displays.index') }}">Boards</a>
            <a href="{{ route('mirrors.displays.index') }}" class="">Displays</a>
            <a href="{{ route('mirrors.displays.show', $display->_id) }}" class="text-primary">{{ $display->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('mirrors.displays.edit', $display->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $display->label ?? $display->_id }}</h1>
  </div>
  <hr />

    <div class="row">
        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">{{ $display->label ?? $display->_id }}</h5>
                </div>
                <div class="card-body text-center pt-3 pb-3">
                    <img src="https://res.cloudinary.com/smartrooms/devices/pi.jpg" style="max-height: 9rem; margin: auto;" align="center" />
                    <br /><br />
                    <small class="text-muted mt-3">{{ $display->_id }}</small>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $display->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Release</td>
                                <td class="border-0">{{ $display->release }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Model</td>
                                <td class="border-0">{{ $display->model }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Hardware</td>
                                <td class="border-0">{{ $display->hardware }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Serial</td>
                                <td class="border-0">{{ $display->serial }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include ('partials.provisioning', ['room' => $display->room, 'item' => $display])

        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Network</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">Connection</td>
                                <td class="border-0">Wifi</td>
                            </tr>
                            <tr>
                                <td class="border-0">Tunnelled</td>
                                <td class="border-0">Yes</td>
                            </tr>
                            <tr>
                                <td class="border-0">LAN IP</td>
                                <td class="border-0">{{ $display->int_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">VPN IP</td>
                                <td class="border-0">{{ $display->vpn_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Mac</td>
                                <td class="border-0">{{ $display->mac }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-4 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Internals</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">OS</td>
                                <td class="border-0">{{ $display->os }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Kernel</td>
                                <td class="border-0">{{ $display->kernel }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Engine</td>
                                <td class="border-0">{{ $display->engine }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Release</td>
                                <td class="border-0">{{ $display->dist }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Container</td>
                                <td class="border-0">ElectronJS</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="col-lg-3 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Web/VPN Administration</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">SSH Host</td>
                                <td class="border-0">{{ $display->vpn_ip }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">SSH Port</td>
                                <td class="border-0">{{ $display->ssh_port ?? 22 }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Username</td>
                                <td class="border-0">smartrooms</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
      @livewire('signals-table', ['component' => $display->_id])
    </div>

    <div class="row">

        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Installed Modules</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            @foreach ($display->modules AS $module)
                            <tr>
                                <td class="border-0">{{ $module->module }}</td>
                                <td class="border-0">{{ $module->position ?? '' }}</td>
                                <td class="border-0">{{ $module->description ?? '' }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    @include ('partials.components.panels.device_business_flow', ['component' => $display])



@stop
