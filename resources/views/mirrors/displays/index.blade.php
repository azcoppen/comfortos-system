@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Smart Mirrors</title>
@stop

@section('app_title')
  <h4>Smart Mirrors</h4>
@stop

@section('sidebar')
  @include ('mirrors.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Boards</div>
          <div class="header-breadcrumb">
            <a href="{{ route('mirrors.index') }}"><i data-feather="sun"></i> Smart Mirrors</a>
            <a href="{{ route('mirrors.index') }}">Hardware</a>
            <a href="{{ route('mirrors.displays.index') }}">Boards</a>
            <a href="{{ route('mirrors.displays.index') }}" class="text-primary">Displays</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('mirrors.displays.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Display</a>
      </div>
  </div>

  <div class="row mb-2">
    <h1 class="display-3 col-md-12 ml-1">Smart Mirrors</h1>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'Mirrors'])
@stop

@section('content')
    <div class="row mt-3">
        <div class="col-lg-12 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Devices By Date</h5>
                </div>
                <div class="card-body">

                  @isset($records)
                      <div class="table-responsive">
                          <table id="" class="table table-hover" style="width:100%">
                              <thead>
                              <tr>
                                  <th></th>
                                  <th>Identifier</th>
                                  <th>Label</th>
                                  <th>Engine</th>
                                  <th>Location</th>
                                  <th class="text-center">Modules</th>
                                  <th>Created</th>
                                  <th><i class="fa fa-globe" title="VPN Admin"></i></th>
                              </tr>
                              </thead>
                              <tbody>
                                @each('mirrors.displays.each', $records, 'record')
                              </tbody>
                          </table>
                      </div>

                    <hr />

                    <div class="row">
                      <div class="col-md-6 offset-3">
                        {{ $records->links() }}
                      </div>
                    </div>
                  @endisset

                  @empty($records)
                      @include ('partials.components.alerts.empty')
                  @endempty

                </div>
            </div>
        </div>
    </div>
@endsection
