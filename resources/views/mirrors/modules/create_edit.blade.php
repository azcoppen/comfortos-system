@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Smart Mirror Modules</title>
@stop

@section('app_title')
  <h4>Smart Mirrors</h4>
@stop

@section('sidebar')
  @include ('mirrors.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Modules</div>
          <div class="header-breadcrumb">
            <a href="{{ route('mirrors.index') }}"><i data-feather="sun"></i> Smart Mirrors</a>
            <a href="{{ route('mirrors.modules.index') }}">Software</a>
            <a href="{{ route('mirrors.modules.index') }}" class="{{ isset($module) ? '' : 'text-primary' }}">Modules</a>
            @if ( isset($module) )
              <a href="{{ route('mirrors.modules.show', $module->_id) }}" class="text-primary">{{ $module->module }}</a>
            @else
              <a href="">Create</a>
            @endif

          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('mirrors.modules.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('content')
  <h1 class="display-4">{{ isset ($module) ? 'Update '.$module->module : 'Add A New Mirror Module' }}</h1>
  <hr />
  @if ( isset ($module) )
    {!! Form::open (['method' => 'PUT', 'route' => ['mirrors.modules.update', $module->_id]]) !!}
  @else
    {!! Form::open (['route' => 'mirrors.modules.store']) !!}
  @endif

  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What is the name of the module?",
                    'name'        => 'module',
                    'value'       => $module->module ?? null,
                    'default'     => '',
                    'placeholder' => "Name of the software module",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What is the description for the module?",
                    'name'        => 'description',
                    'value'       => $module->description ?? null,
                    'default'     => '',
                    'placeholder' => "Description of the software module",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "What is URL to its repository?",
                    'name'        => 'repository',
                    'value'       => $module->repository ?? null,
                    'default'     => '',
                    'placeholder' => "Repository of the software module",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.select', [
                    'label'       => "What position should it have on the screen?",
                    'name'        => 'position',
                    'options'     => array_combine (config ('comfortos.module_positions'), config ('comfortos.module_positions')),
                    'value'       => $module->position ?? null,
                    'default'     => '',
                    'placeholder' => "Screen position of the software module",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.textarea', [
                    'label'       => "Specify a list of tags for this module",
                    'name'        => 'tags',
                    'value'       => isset ($module) ? collect($module->tags)->implode ("\n") : (isset ($tag) ? $tag : null),
                    'default'     => '',
                    'placeholder' => "What tags should people use to find this?",
                    'rows'        => 4,
                    'required'    => true,
                    'help'        => 'Add one tag per line',
                  ])
                </div>

          </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="settings"></i> Configuration</h5>
              </div>
              <div class="card-body">
                <div class="form-group">
                  @include ('components.fields.textarea', [
                    'pre'         => true,
                    'label'       => "Example config",
                    'name'        => 'config',
                    'value'       => isset ($module->config) ? json_encode ($module->config, JSON_PRETTY_PRINT) : null,
                    'default'     => '',
                    'placeholder' => "What configuration should this module use?",
                    'rows'        => 30,
                    'help'        => 'Paste the example JSON config.',
                  ])
                </div>

              </div>
            </div>
          </div> <!-- end col -->

    </div> <!-- end row -->

  <hr />
  <div class="row mb-4">
    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}

  @if ( isset ($module) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $module,
      'text'    => 'Module',
      'destroy' => [
        'route' => 'mirrors.modules.destroy',
        'params'=> [$module->_id],
      ],
      'return'  => [
        'route' => 'mirrors.modules.index',
        'params'=> [$module->_id],
      ],
    ])
  @endif

@endsection
