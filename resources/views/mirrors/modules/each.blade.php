<tr>
    <td><i class="fa fa-window-restore text-muted"></i></td>
    <td>{{ Arr::first($record->tags) }}</td>
    <td><a href="{{ route('mirrors.modules.show', $record->_id) }}">{{ $record->module }}</a></td>
    <td>{{ $record->position ?? '' }}</td>
    <td>{{ Str::limit($record->description ?? '', 30) }}</td>
    <td>{{ $record->created_at->diffForHumans() }}</td>
    <td class="text-">
        @if ( $record->repository )
        <a title="Go to repository" target="_blank" href="{{ $record->repository }}"><i class="fa fa-share-square" title="Repository"></i></a>
        @endif
    </td>
</tr>
