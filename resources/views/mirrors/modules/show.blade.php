@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: MM Software</title>
@stop

@section('app_title')
  <h4>Smart Mirrors</h4>
@stop

@section('sidebar')
  @include ('mirrors.sidebar')
@endsection

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Modules</div>
          <div class="header-breadcrumb">
            <a href="{{ route('mirrors.index') }}"><i data-feather="sun"></i> Smart Mirrors</a>
            <a href="{{ route('mirrors.modules.index') }}">Software</a>
            <a href="{{ route('mirrors.modules.index') }}">Modules</a>
            <a href="{{ route('mirrors.modules.show', $module->_id) }}" class="text-primary">{{ $module->module }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('mirrors.modules.edit', $module->_id) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('content')
  <div class="row">
    <h1 class="display-3 col-md-12">{{ $module->module ?? $module->_id }}</h1>
  </div>
  <hr />


    <div class="row">


        <div class="col-lg-6 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">General</h5>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td class="border-0">ID</td>
                                <td class="border-0">{{ $module->_id }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Tags</td>
                                <td class="border-0">{{ collect($module->tags)->implode (',') }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Repository</td>
                                <td class="border-0"><a target="_blank" href="{{ $module->repository }}">{{ $module->repository }}</a></td>
                            </tr>
                            <tr>
                                <td class="border-0">Description</td>
                                <td class="border-0">{{ $module->description }}</td>
                            </tr>
                            <tr>
                                <td class="border-0">Position</td>
                                <td class="border-0">{{ $module->position }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-6 card-margin">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Configuration</h5>
                </div>
                <div class="card-body">
<pre>
<code>
{!! json_encode ($module->config, JSON_PRETTY_PRINT) !!}
</code>
</pre>
                </div>
            </div>
        </div>


    </div>



@stop
