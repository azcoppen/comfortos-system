
<ul class="nav">
    <li class="nav-header">Hardware</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#hardware" aria-expanded="false" aria-controls="hardware">
            <i data-feather="sun" class="menu-icon"></i>
            <span class="menu-title">Boards</span>
        </a>
        <div class="collapse show" id="hardware">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'displays' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.displays.index') }}"><span class="menu-title">Displays</span></a></li>
            </ul>
        </div>
    </li>

    <li class="nav-header">Software</li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#software" aria-expanded="false" aria-controls="software">
            <i data-feather="grid" class="menu-icon"></i>
            <span class="menu-title">Modules</span>
        </a>
        <div class="collapse show" id="software">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'default' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'default') }}"><span class="menu-title">Default</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'entertainment' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'entertainment') }}"><span class="menu-title">Entertainment</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'finance' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'finance') }}"><span class="menu-title">Finance</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'health' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'health') }}"><span class="menu-title">Health</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'iot' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'iot') }}"><span class="menu-title">IoT</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'office' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'office') }}"><span class="menu-title">Office</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'religion' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'religion') }}"><span class="menu-title">Religion</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'sport' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'sport') }}"><span class="menu-title">Sport</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'travel' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'travel') }}"><span class="menu-title">Travel</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'voice' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'voice') }}"><span class="menu-title">Voice</span></a></li>
                <li class="nav-item {{ isset($nav_section) && $nav_section == 'weather' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('mirrors.modules.index.tag', 'weather') }}"><span class="menu-title">Weather</span></a></li>
            </ul>
        </div>
    </li>

</ul>
