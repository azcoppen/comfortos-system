
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="AC">
    <title>SmartRooms: Lab Authentication</title>
    <link rel="apple-touch-icon" href="https://smartrooms.b-cdn.net/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://smartrooms.b-cdn.net/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/pages/login-register.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/assets/css/style.css">

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu 1-column  bg-full-screen-image blank-page blank-page" data-open="hover" data-menu="horizontal-menu" data-color="bg-gradient-x-purple-blue" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-6 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <div class="font-large-1  text-center">
                                        SmartRooms Lab Demo
                                    </div>
                                </div>
                                <div class="card-content">

                                    <div class="card-body">

                                      @if ($errors->count() > 0)
                                      <div class="alert round bg-danger alert-icon-left alert-dismissible mb-2" role="alert">
                                          <span class="alert-icon">
                                              <i class="ft-thumbs-down"></i>
                                          </span>
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">×</span>
                                          </button>
                                          <strong>Nope.</strong> Wrong.
                                      </div>
                                      @endif

                                      @if ( session('logout') )
                                      <div class="alert round bg-success alert-icon-left alert-dismissible mb-2" role="alert">
                                          <span class="alert-icon">
                                              <i class="ft-thumbs-up"></i>
                                          </span>
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">×</span>
                                          </button>
                                          <strong>Done.</strong> Session Killed.
                                      </div>
                                      @endif

                                        <form class="form-horizontal" method="POST" action="{{ route ('login') }}">
                                          @csrf
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="email" name="email" class="form-control round" id="user-name" placeholder="first.last@cstraight.com" value ="{{ old ('email') }}" required>
                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" name="password" class="form-control round" id="user-password" placeholder="NOT password1, admin, passw0rd" required>
                                                <div class="form-control-position">
                                                    <i class="ft-lock"></i>
                                                </div>
                                            </fieldset>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12 text-center text-sm-left">

                                                </div>
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">Authenticate</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>

    <script src="https://smartrooms.b-cdn.net/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://smartrooms.b-cdn.net/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="https://smartrooms.b-cdn.net/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
    <script src="https://smartrooms.b-cdn.net/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="https://smartrooms.b-cdn.net/app-assets/js/core/app.js" type="text/javascript"></script>
    <script src="https://smartrooms.b-cdn.net/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>

</body>
<!-- END: Body-->

</html>
