<div class="col-sm-12 d-none d-md-block d-lg-block">
  <div class="alert alert-outline-light alert-dismissible fade show" role="alert">
      <i data-feather="help-circle" class="alert-icon"></i>
      <span class="alert-text">Each of these controls are available in the SmartRooms API for your application to use, and are available here for testing. While on auto-refresh, there may be some overlap and/or a delay between when you issue a command to the queue and when the widget updates itself with the component's new state.
        For real-time information, your application should connect to the room websocket, which provides a live data stream of commands and signals.</span>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i data-feather="x" class="alert-close"></i>
      </button>
  </div>
</div>
