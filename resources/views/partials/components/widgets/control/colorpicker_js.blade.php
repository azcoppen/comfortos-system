<script>

window.is_manual_update = false;

function set_widget_color ( hsv )
{
  if ( window.colorPicker )
  {
    try
    {
      if ( hsv )
      {
        window.colorPicker.color.set ({
          h: hsv[0],
          s: hsv[1],
          v: hsv[2]
        });
      }
    }
    catch (e)
    {
      console.log (e);
    }
  }
}


/*
This is a dynamic JS component we can't reload every Xs because it recreates the DOM.
*/
document.addEventListener("DOMContentLoaded", () => {

  window.colorPicker = new iro.ColorPicker('#hsb-picker-{{ $component->_id }}', {
    width: 140,
    color: {
      h: {{ $component->last_hsb_signal->value[0] ?? 0 }},
      s: {{ $component->last_hsb_signal->value[1] ?? 0 }},
      v: {{ $component->last_hsb_signal->value[2] ?? 100 }},
    }
  });

  // Flag this is a manual change with a mouse
  window.colorPicker.on('input:change', function(color) {
    window.livewire.emit('command', color.hsv);
  })


  window.addEventListener('command.sent', event => {
    if ( event.detail.attribute == 'power' )
    {
      switch (event.detail.value)
      {
        case 'on':
          window.colorPicker.color.setChannel ('hsv', 'v', 100);
        break;

        case 'off':
          window.colorPicker.color.setChannel ('hsv', 'v', 0);
        break;
      }
    }
  })

});
</script>
