<div class="card-footer bg-white" style="line-height: 0.9 !important;">
    <small class="text-muted">Control is for monitoring only and will refresh every {{ $num ?? 10 }} seconds.</small>
</div>
