<div class="form-row">
    <div class="col-md-2">
      <div class="position-relative form-group pt-4">
        New guest code:
      </div>
    </div>
    <div class="col-md-2">
        <div class="position-relative form-group">
          <label for="activation" class="">Activate within (mins)</label>
          <input wire:model.lazy="activation" type="number" class="form-control" id="activation" placeholder="30" min="5" max="200" required pattern="[0-9]+" value="30">
        </div>
    </div>
    <div class="col-md-2">
        <div class="position-relative form-group">
          <label for="hours" class="">Last for (hours)</label>
          <input wire:model.lazy="hours" type="number" class="form-control" id="hours" placeholder="24" min="1" max="200" required pattern="[0-9]+" value="4">
        </div>
    </div>
    <div class="col-md-2">
        <div class="position-relative form-group pt-4">
          <button wire:click="generate()" id="generate-btn" type="button" class="btn btn-success"><i data-feather="refresh-cw"></i> create</button>
          <span wire:loading class="spinner-grow spinner-grow-sm mb-2 text-warning" role="status">
              <span class="sr-only">Generating...</span>
          </span>
        </div>
    </div>
    <div class="col-md-4">
        <div class="position-relative form-group pt-2 ">
          <input wire:model.lazy="code" type="text" class="form-control form-control-lg pt-0 pb-0" style="font-size: 1.5rem;" id="code" placeholder="" min="1" max="200" required pattern="[0-9]+" value="" readonly>
        </div>
    </div>
</div>
