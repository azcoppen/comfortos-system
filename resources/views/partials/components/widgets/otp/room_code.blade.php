<div>
  <div wire:ignore class="row">
    <div class="col-12">
      <div class="form-group">
        <select class="form-control form-control-lg roomfinder" id="room" name="room"
          placeholder="Type the name or number of a room, then click it from the results which appear."
          data-noresults-text="No rooms found. Are you sure you typed correctly?"
          data-url="{{ route ('dashboards.roomq') }}" autocomplete="off"></select>
      </div>
    </div>
  </div>
  <div class="row mt-4">
      <div class="col-md-4">
          <div class="position-relative form-group">
            <label for="activation" class="">How many minutes should they have to enter the code?</label>
            <input wire:model.lazy="activation" type="number" class="form-control form-control-lg" id="activation" placeholder="60" min="5" max="200" required pattern="[0-9]+" value="60">
          </div>
      </div>
      <div class="col-md-4">
          <div class="position-relative form-group">
            <label for="hours" class="">How many hours should their access last?</label>
            <input wire:model.lazy="hours" type="number" class="form-control form-control-lg" id="hours" placeholder="24" min="1" max="200" required pattern="[0-9]+" value="24">
          </div>
      </div>
      <div class="col-md-4">
          <div class="position-relative form-group pt-4">
            <button wire:click="generate()" id="generate-btn" type="button" class="btn btn-lg btn-success"><i data-feather="refresh-cw"></i> generate code</button>
            <span wire:loading class="spinner-grow spinner-grow-sm mb-2 text-warning" role="status">
                <span class="sr-only">Generating...</span>
            </span>
          </div>
      </div>
  </div>

  <div class="row mt-5">
    <div class="col-12">
        <input wire:model.lazy="code" type="text" class="form-control form-control-lg pt-0 pb-0" style="font-size: 4rem; text-align: center" id="code" placeholder="" min="1" max="200" required pattern="[0-9]+" value="" readonly>
    </div>
  </div>
</div>
