<div class="row">
  <div class="col-12">
    <div class="form-group">
      <select class="form-control form-control-lg roomfinder" id="room_id" name="room_id" wire:model.lazy="room"
        placeholder="Type the name or number of a room"
        data-noresults-text="No rooms found. Are you sure you typed correctly?"
        data-url="{{ route ('dashboards.roomq') }}" autocomplete="off"></select>
    </div>
  </div>
</div>
