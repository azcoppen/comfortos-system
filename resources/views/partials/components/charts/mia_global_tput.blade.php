<div wire:ignore class="col-lg-12 col-md-6 card-margin">
    <div class="card">
        <div class="card-header">
            <h6 class="card-title m-0 text-danger">Last Hour / Global Alert Throughput (per min)</h6>
        </div>
        <div class="card-body">
            <div id="mia-chart" style="height: 200px;"></div>
        </div>
    </div>
</div>


@push ('js')

  <script>
  var color = Chart.helpers.color;

  const mia_chart = new Chartisan ({
    el: '#mia-chart',
    url: "@chart('m_i_a_chart')",
    hooks: new ChartisanHooks()
      .colors([color(window.chartColors.red).alpha(0.2).rgbString()])
      .borderColors([window.chartColors.red]).responsive().beginAtZero().legend(false)
      .datasets([
        {
          type: 'line',
          fill: true,
          borderWidth: 1,
          pointRadius: 1
        }
      ]),
  });

  setInterval (function () {
    mia_chart.update({ background: true });
  }, 10000);

</script>
@endpush
