<div wire:ignore class="col-lg-12 col-md-6 card-margin">
    <div class="card">
        <div class="card-header">
            <h6 class="card-title m-0">Last 24hrs / Component Signal Throughput (per hr)</h6>
        </div>
        <div class="card-body">
            <div id="signals-hourly-chart" style="height: 200px;"></div>
        </div>
    </div>
</div>


@push ('js')

  <script>
  var color = Chart.helpers.color;

  const signals_hourly_chart = new Chartisan ({
    el: '#signals-hourly-chart',
    url: "{{ route ('explorer.charts.signals_chart.'.Str::singular($group).'.hourly', [$component->_id]) }}",
    hooks: new ChartisanHooks()
      .colors([color(window.chartColors.grey).alpha(0.2).rgbString()])
      .borderColors([window.chartColors.grey]).responsive().beginAtZero().legend(false)
      .datasets([
        {
          type: 'line',
          fill: true,
          borderWidth: 1,
          pointRadius: 1
        }
      ]),
  });

  setInterval (function () {
    signals_hourly_chart.update({ background: true });
  }, 1800000);

</script>
@endpush
