<div wire:ignore class="col-lg-12 col-md-6 card-margin">
    <div class="card">
        <div class="card-header">
            <h6 class="card-title m-0">Last 200 records / Component Attribute Commands</h6>
        </div>
        <div class="card-body">
            <div id="commands-chart-{{ $attribute }}" style="height: 200px;"></div>
        </div>
    </div>
</div>


@push ('js')

  <script>
  var color = Chart.helpers.color;

  const commands_chart_{{ $attribute }} = new Chartisan ({
    el: '#commands-chart-{{ $attribute }}',
    url: "{{ route ('explorer.charts.attribute_commands_chart.'.Str::singular($group), [$component->_id, $attribute]) }}",
    hooks: new ChartisanHooks()
      .colors([color(window.chartColors.red).alpha(0.2).rgbString()])
      .borderColors([window.chartColors.red]).responsive().beginAtZero().legend(false)
      .datasets([
        {
          type: 'bar',
          fill: true,
          borderWidth: 1,
          pointRadius: 1
        }
      ]),
  });

  setInterval (function () {
    commands_chart_{{ $attribute }}.update({ background: true });
  }, 10000);

</script>
@endpush
