<div wire:ignore class="col-lg-12 col-md-6 card-margin">
    <div class="card">
        <div class="card-header">
            <h6 class="card-title m-0">Last Hour / Component Command Throughput (per min)</h6>
        </div>
        <div class="card-body">
            <div id="commands-chart" style="height: 200px;"></div>
        </div>
    </div>
</div>


@push ('js')

  <script>
  var color = Chart.helpers.color;

  const commands_chart = new Chartisan ({
    el: '#commands-chart',
    url: "{{ route ('explorer.charts.commands_chart.'.Str::singular($group), [$component->_id]) }}",
    hooks: new ChartisanHooks()
      .colors([color(window.chartColors.red).alpha(0.2).rgbString()])
      .borderColors([window.chartColors.red]).responsive().beginAtZero().legend(false)
      .datasets([
        {
          type: 'line',
          fill: true,
          borderWidth: 1,
          pointRadius: 1
        }
      ]),
  });

  setInterval (function () {
    commands_chart.update({ background: true });
  }, 10000);

</script>
@endpush
