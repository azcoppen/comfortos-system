<div wire:ignore class="col-lg-12 col-md-6 card-margin">
    <div class="card">
        <div class="card-header">
            <h6 class="card-title m-0">Last Hour / Global Signal Throughput (per min)</h6>
        </div>
        <div class="card-body">
            <div id="signals-chart" style="height: 200px;"></div>
        </div>
    </div>
</div>


@push ('js')

  <script>
  var color = Chart.helpers.color;

  const signals_chart = new Chartisan ({
    el: '#signals-chart',
    url: "@chart('signals_chart', ['foo' => 'bar'])",
    hooks: new ChartisanHooks()
      .colors([color(window.chartColors.green).alpha(0.2).rgbString()])
      .borderColors([window.chartColors.green]).responsive().beginAtZero().legend(false)
      .datasets([
        {
          type: 'line',
          fill: true,
          borderWidth: 1,
          pointRadius: 1
        }
      ]),
  });

  setInterval (function () {
    signals_chart.update({ background: true });
  }, 10000);

</script>
@endpush
