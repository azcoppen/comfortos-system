<div class="col-lg-12 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-md-10">Values By Date</h5>
            <div class="text-muted float-right col-md-2 text-right">Updated {{ now()->timezone (auth()->user()->timezone)->format ('H:i:s A') }}</div>
        </div>
        <div class="card-body">
          @if ( isset ($signals) && count ($signals) && $signals->where ('attribute', $attribute)->count () > 0 )

            <div class="table-responsive">
              <table style="width: 100%;" id="" class="table table-hover">
                 <thead>
                 <tr>
                     <th></th>
                     <th>Prev</th>
                     <th>Value</th>
                     @if ( $attribute == 'hsb' )
                       <th></th>
                     @endif
                     <th>Sent</th>
                     <th>Recorded</th>
                     <th>Action</th>
                 </tr>
                 </thead>
                 <tbody>
                   @php ($previous = null) @endphp
                     @foreach ($signals->take ($limit ?? 200) AS $signal)
                       <tr>
                           <td><i class="fa fa-{{ config ('comfortos.fa_attribute_icons.'.$signal->attribute) }} text-muted"></i></td>
                           <td class="text-muted">{{ is_string ($previous) ? $previous : json_encode ($previous) }}</td>
                           <td class="text-primary">{{ is_string ($signal->value) ? $signal->value : json_encode ($signal->value) }}
                             @if ( $attribute == 'hsb' )
                               <td><div style="width: 10px; height: 10px; background-color: {{ hsb ($signal->value)->toHex() }}"></div></td>
                             @endif
                           </td>
                           <td>{{ $signal->received_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} <span class="text-muted">({{ $signal->received_at->diffForHumans() }})</span></td>
                           <td>{{ $signal->created_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }}</td>
                           <td>
                             @if ( $previous )
                               @if ( is_numeric ($signal->value) )
                                 @if ( $signal->value && $signal->value < $previous)
                                   <i class="fa fa-arrow-up text-success">/</i>
                                   <span class="badge badge-info">{{ round(abs ($previous - $signal->value), 1) }}</span>
                                 @endif
                                 @if ( $signal->value && $signal->value > $previous)
                                   <i class="fa fa-arrow-down text-danger">/</i>
                                   <span class="badge badge-info">{{ round(abs ($previous - $signal->value), 1) }}</span>
                                 @endif
                               @endif

                               @if ( $signal->value && !is_numeric ($signal->value) && $signal->value != $previous)
                                 <i class="fa fa-arrow-left text-warning">/</i>
                               @endif

                             @endif
                           </td>
                       </tr>
                       @php ($previous = $signal->value) @endphp
                     @endforeach
                     @php ($previous = null)
                 </tbody>
               </table>
           </div>

          @else
            @include ('partials.components.alerts.empty')
          @endif
        </div>
    </div>
</div>
