<div wire:poll.5s class="col-lg-12 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-md-2">Operations By Date</h5>
            <div class="text-muted float-right col-md-10 text-right">Updated {{ now()->timezone (auth()->user()->timezone)->format ('H:i:s A') }}</div>
        </div>
        <div class="card-body">

          @if (isset($operations) && count ($operations))
            <div class="table-responsive">
                <table id="" class="table table-sm table-hover" style="width:100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Domain</th>
                        <th>Src</th>
                        <th>User</th>
                        <th>Room</th>
                        <th>Operation</th>
                        <th>Args</th>
                        <th>Dispatcher</th>
                        <th>Executor</th>
                        <th>Execute</th>
                        <th>Process</th>
                        <th>Expire</th>
                        <th>Created</th>
                        <th>Updated</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($operations AS $op)
                            <tr>
                                <td><i class="fa fa-bolt text-muted"></i></td>
                                <td>{{ $op->domain ?? 'global' }}</td>
                                <td>{{ $op->src ?? ''}}</td>
                                <td>{{ is_object($op->user) ? $op->user->first : ''}}</td>
                                <td>{{ $op->room_id ?? '' }}</td>
                                <td>{{ $op->operation ?? '' }}</td>
                                <td>@json ($op->request)</td>
                                <td>{{ Str::afterLast($op->dispatcher, '\\') }}</td>
                                <td>{{ Str::afterLast($op->executor, '\\') }}</td>
                                <td>{{ is_object($op->execute_at) ? $op->execute_at->timezone(auth()->user()->timezone)->format('H:i:s') : '' }}</td>
                                <td>{{ is_object($op->processed_at) ? $op->processed_at->timezone(auth()->user()->timezone)->format('H:i:s'): '' }}</td>
                                <td>{{ is_object($op->expire_at) ? $op->expire_at->timezone(auth()->user()->timezone)->format('H:i:s') : '' }}</td>
                                <td>{{ is_object($op->created_at) ? $op->created_at->timezone(auth()->user()->timezone)->format('H:i:s') : '' }}</td>
                                <td>{{ is_object($op->updated_at) ? $op->updated_at->timezone(auth()->user()->timezone)->format('H:i:s') : '' }}</td>
                            </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>


          @else
              @include ('partials.components.alerts.empty')
          @endif

        </div>
    </div>
</div>
