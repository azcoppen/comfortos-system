<div class="card">
    <div class="card-header">
        <h5 class="card-title">Last Signal</h5>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <tbody>
                <tr>
                    <td class="border-0">Domain</td>
                    <td class="border-0">HAB</td>
                </tr>
                <tr>
                    <td class="border-0">Attribute</td>
                    <td class="border-0 text-primary" id="{{ $component->_id }}-attribute">
                      @if ( isset($component->last_signal) && is_object ($component->last_signal) )
                        {{ $component->last_signal->attribute }}
                      @else
                        <span class="text-muted">None</span>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td class="border-0">Value</td>
                    <td class="border-0" id="{{ $component->_id }}-value">
                      @if ( isset($component->last_signal) && is_object ($component->last_signal) )
                        @if ($component->last_signal->group == 'routers')
                          Data Object
                        @else
                          {{ is_string ($component->last_signal->value) ? $component->last_signal->value : json_encode($component->last_signal->value) }}
                        @endif
                      @else
                        <span class="text-muted">None</span>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td class="border-0">Detected</td>
                    <td class="border-0" id="{{ $component->_id }}-received">
                      @if ( isset($component->last_signal) && is_object ($component->last_signal) )
                      {{ $component->last_signal->received_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} ({{ $component->last_signal->received_at->diffForHumans() }})
                      @else
                        <span class="text-muted">None</span>
                      @endif
                    </td>
                </tr>
                <tr>
                    <td class="border-0">Recorded</td>
                    <td class="border-0" id="{{ $component->_id }}-created">
                      @if ( isset($component->last_signal) && is_object ($component->last_signal) )
                      {{ $component->last_signal->created_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} ({{ $component->last_signal->created_at->diffForHumans() }})
                      @else
                        <span class="text-muted">None</span>
                      @endif
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
