<div wire:poll class="col-lg-12 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-md-10">Codes By Date</h5>
            <div class="text-muted float-right col-md-2 text-right">Updated {{ now()->timezone (auth()->user()->timezone)->format ('H:i:s A') }}</div>
        </div>
        <div class="card-body">
          @if ( isset ($codes) && count ($codes) )

            <div class="table-responsive">
              <table style="width: 100%;" id="" class="table table-hover">
                 <thead>
                 <tr>
                     <th>Index</th>
                     <th>Label</th>
                     <th>Hint</th>
                     <th>Expiry</th>
                     <th>Used</th>
                     <th>Created</th>
                 </tr>
                 </thead>
                 <tbody>
                     @foreach ($codes->take ($limit ?? 20) AS $code)
                       <tr>
                           <td>{{ $code->index }}</td>
                           <td>{{ $code->label }}</td>
                           <td>{{ $code->hint }}</td>
                           <td class="text-{{ $code->expire_at->timestamp > now()->timestamp ? 'success' : 'danger'}}">{{ $code->expire_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} <span class="text-muted">({{ $code->expire_at->diffForHumans() }})</span></td>
                           <td>{{ $code->used }}</td>
                           <td>{{ $code->created_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }}</td>
                       </tr>
                     @endforeach
                 </tbody>
               </table>
           </div>

          @else
            @include ('partials.components.alerts.empty')
          @endif
        </div>
    </div>
</div>
