
<div class="alert alert-bordered-light alert-dismissible fade show" role="alert">
    <i data-feather="activity" class="alert-icon"></i>
  <span id="component-feed" data-component = "{{ $component->_id }}" class="alert-text">Waiting for stream connection...</span>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <i data-feather="x" class="alert-close"></i>
  </button>
</div>
