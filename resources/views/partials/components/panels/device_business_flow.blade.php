<div class="row mb-3">

  <div class="col-lg-4">
    <div class="table-responsive">
    <table class="table table-hover">
      <tbody>
          <tr>
              <td class="">Ordered At</td>
              <td class="">
                @if (isset ($component->ordered_at) && is_object ($component->ordered_at))
                 {{ $component->ordered_at->timezone (auth()->user()->timezone)->format ('M d Y') }}
                 <small class="text-muted">({{ $component->ordered_at->diffForHumans() }})</small>
                @endif
              </td>
          </tr>
          <tr>
              <td class="">Ordered By</td>
              <td class="">{{ $component->purchaser->full_name ?? '' }}</td>
          </tr>
          <tr>
              <td class="">Order ID</td>
              <td class="">{{ $component->order_id ?? '' }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="table-responsive">
        <table class="table table-hover">
          <tbody>
              <tr>
                  <td class="">Installed At</td>
                  <td class="">
                    @if (isset ($component->installed_at) && is_object ($component->installed_at))
                     {{ $component->installed_at->timezone (auth()->user()->timezone)->format ('M d Y') }}
                     <small class="text-muted">({{ $component->installed_at->diffForHumans() }})</small>
                    @endif
                  </td>
              </tr>
              <tr>
                  <td class="">Installed By</td>
                  <td class="">{{ $component->installer->full_name ?? ''}}</td>
              </tr>
            </tbody>
          </table>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="table-responsive">
        <table class="table table-hover">
          <tbody>
              <tr>
                  <td class="">Provisioned At</td>
                  <td class="">
                    @if (isset ($component->provisioned_at) && is_object ($component->provisioned_at))
                     {{ $component->provisioned_at->timezone (auth()->user()->timezone)->format ('M d Y') }}
                     <small class="text-muted">({{ $component->provisioned_at->diffForHumans() }})</small>
                    @endif
                  </td>
              </tr>
              <tr>
                  <td class="">Provisioned By</td>
                  <td class="">{{ $component->provisioner->full_name ?? ''}}</td>
              </tr>
            </tbody>
          </table>
    </div>
  </div>
</div>
