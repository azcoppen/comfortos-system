@if ( isset ($destroy['route']) && isset ($return['route']) )
  <hr />
  {!! Form::open (['method' => 'delete', 'route' => [$destroy['route'], $destroy['params']]]) !!}
  {!! Form::hidden ('return', route ($return['route'], $return['params'])) !!}
  <div class="row mb-4">
    <div class="col-md-12">
      <button type="submit" class="btn btn-danger"><i data-feather="trash-2"></i> Delete This {{ $text ?? 'Component' }}</button>
      &nbsp;<small class="text-muted"><i data-feather="alert-circle"></i> Careful: destroying this will remove all its dependent/child objects.</small>
    </div>
  </div>
  {!! Form::close () !!}
@endif
