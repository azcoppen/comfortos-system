  <div wire:poll.5s class="col-lg-12 card-margin">
      <div class="card">
          <div class="card-header">
            @if (isset($search) && $search !== FALSE)
              <h5 class="card-title text-success"><i data-feather="search"></i> MIA Alerts: search results for &quot;{{ request()->get('q') }}&quot;</h5>
            @else
              <h5 class="card-title">MIA Alerts</h5>
              <div class="text-muted float-right col-md-11 text-right">Updated {{ now()->timezone (auth()->user()->timezone)->format ('H:i:s A') }}</div>
            @endif
          </div>
          <div class="card-body">
            @if ( isset ($alerts) && count ($alerts) )

              <div class="table-responsive">
                <table style="width: 100%;" id="" class="table table-hover">
                   <thead>
                   <tr>
                       <th></th>
                       <th>Location</th>
                       <th>Component</th>
                       <th>Group</th>
                       <th>T</th>
                       <th>Last Seen</th>
                       <th>Generated</th>
                   </tr>
                   </thead>
                   <tbody>
                       @foreach ($alerts->take ($limit ?? 100) AS $alert)
                         <tr>
                             <td class="text-danger"><i class="fa fa-question-circle"></i></td>
                             <td>{{ $alert->room->label ?? '' }}</td>
                             <td class="text-danger"><strong>{{ $alert->component_id }}</strong></td>
                             <td>{{ $alert->group }}</td>
                             <td>{{ $alert->last_seen_mins ?? 0}} ({{ $alert->threshold }})</td>
                             @if ( is_object ($alert->last_seen_at) )
                               <td>{{ $alert->last_seen_at->timezone(auth()->user()->timezone)->format ('H:i:s A') }}
                                 <span class="text-muted">
                                   ({{ $alert->last_seen_at->diffForHumans() }})
                                 </span>
                               </td>
                             @else
                               <td>Never</td>
                             @endif
                             <td>{{ $alert->created_at->timezone(auth()->user()->timezone)->format ('H:i:s A') }}
                               <span class="text-muted">
                                 ({{ $alert->created_at->diffForHumans() }})
                               </span>
                             </td>
                         </tr>
                       @endforeach
                   </tbody>
                 </table>
             </div>

            @else
              @include ('partials.components.alerts.empty')
            @endif
          </div>
      </div>
  </div>
