  <div wire:poll.10s class="col-lg-12 card-margin">
      <div class="card">
          <div class="card-header">
            @if (isset($search) && $search !== FALSE)
              <h5 class="card-title text-success"><i data-feather="search"></i> Signals: search results for &quot;{{ request()->get('q') }}&quot;</h5>
            @else
              <h5 class="card-title">Signals</h5>
              <div class="text-muted float-right col-md-11 text-right">Updated {{ now()->timezone (auth()->user()->timezone)->format ('H:i:s A') }}</div>
            @endif
          </div>
          <div class="card-body">
            @if ( isset ($signals) && count ($signals) )

              <div class="table-responsive">
                <table style="width: 100%;" id="" class="table table-hover">
                   <thead>
                   <tr>
                      <th></th>
                       <th>Component</th>
                       <th>Group</th>
                       <th>Attribute</th>
                       <th>Value</th>
                       <th>Received</th>
                       <th>Recorded</th>
                   </tr>
                   </thead>
                   <tbody>
                       @foreach ($signals->take ($limit ?? 10) AS $signal)
                         <tr>
                             <td><i class="fa fa-{{ config ('comfortos.fa_attribute_icons.'.$signal->attribute) }} text-muted"></i></td>
                             <td>{{ $signal->component_id }}</td>
                             <td>{{ $signal->group }}</td>
                             <td>{{ $signal->attribute }}</td>
                             <td>
                               @if ( is_object ($signal->value) )
                                   Data Object
                               @endif
                               @if ( is_array ($signal->value) )
                                 @if ( $signal->group == 'routers' )
                                   Data Object
                                 @else
                                   @json ($signal->value)
                                 @endif
                               @endif
                               @if ( is_string ($signal->value) || is_numeric ($signal->value) )
                                 {{ $signal->value }}
                               @endif
                              </td>
                             <td>{{ $signal->received_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} <span class="text-muted">({{ $signal->received_at->diffForHumans() }})</span></td>
                             <td>{{ $signal->created_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }}</td>
                         </tr>
                       @endforeach
                   </tbody>
                 </table>
             </div>

            @else
              @include ('partials.components.alerts.empty')
            @endif
          </div>
      </div>
  </div>
