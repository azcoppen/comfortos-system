<div wire:poll.10s class="row mb-3 p-2">
  @foreach ($summary AS $signal)
    @if ($signal->group != 'routers' )
    <div class="col-sm-2 m-2 p-0">

      <div class="card m-0 p-0">
        <div class="card-body text-center">
          <h1 class="display-4" style="font-size: 1.7rem !important;">
            <i class="fa fa-{{ config ('comfortos.fa_attribute_icons.'.$signal->attribute) }} text-muted"></i>
            @if ( $signal->attribute == 'hsb' || $signal->attribute == 'rgb' )
              {{ str_replace (['[', ']'], ['', ''], json_encode ($signal->value)) }}
            @else
              {{ is_string ($signal->value) ? Str::limit ($signal->value, 10) : Str::limit (json_encode ($signal->value)) }}
            @endif
          </h1>
          <span class="text-">{{ $signal->attribute ?? '' }}</span>
        </div>
        <div class="card-footer text-center">
          <small>
            <span class="text-muted">{{ $signal->group ?? '' }}</span>
            <span class="text-success">
            {{ $signal->received_at->timezone(auth()->user()->timezone)->format ('H:i A') }}
            </span>
          </small>
        </div>
      </div>

    </div>
    @endif
  @endforeach
</div>
