<div class="col-lg-{{ $size ?? 12 }} col-md-6 col-sm-6">
        <div class="card card-margin" data-device="{{ $id ?? '' }}">
                <div class="card-body p-0">
                        <div class="widget-33">
                                <div class="widget-33-header">
                                        <img src="{{ $image ?? '' }}" alt="" style="width: auto !important;" />
                                        <div class="widget-33-appliance-info">
                                                <h4 class="title"><a href="{{ isset ($route_path) ? route ($route_path, array_merge ($route_vars, [$id])) : ''}}">{{ $title ?? '' }}</a></h4>
                                                <small class="additional-content">{{ $sub ?? '' }}</small>
                                        </div>
                                </div>
                                <div class="widget-33-body">
                                        <span class="widget-33-service-title"></span>
                                        <div class="widget-33-service-action">

                                            <h5 class="service">&nbsp;</h5>
                                            <div class="actions">
                                                    <button type="button" class="btn btn-base mr-1"><i data-feather="package"></i></button>
                                            </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
