@if ( config ('comfortos.mqtt_topics.'.$group) )
<div class="col-lg-12 card-margin">
    <div class="card">
        <div class="card-header" data-toggle="collapse" href="#mqtt-states">
          <h5 class="card-title">MQTT State Topics</h5>
        </div>
        <div class="card-body collapse" id ="mqtt-states">


            @foreach (config ('comfortos.mqtt_topics.'.$group) AS $item_key => $commandable)
              <div class="form-group row">
                  <label for="{{ $item_key }}-state" class="col-sm-2 col-form-label pl-4">{{ $item_key }}</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id=""><i class="fa fa-chart-bar"></i></span>
                        </div>
                        {!! Form::text ($item_key.'-state', $component->mqtt_topic .'/'.$item_key.'/state', ['id' => $item_key.'-state', 'class'=>'form-control', 'readonly']) !!}
                    </div>
                    <span class="form-text text-muted">
                      mosquitto_sub -h mqtt.{{ env ('APP_DOMAIN') }} -p 8883 -u ($USERNAME) -P ($PASSWDRD) --capath ($CA_FILE)<br />
                      -t {{ $component->mqtt_topic .'/'.$item_key.'/state' }}
                    </span>
                  </div>



              </div>
            @endforeach

        </div>
    </div>
</div>
@endif

@if ( config ('comfortos.mqtt_topics.'.$group) && in_array (true, config ('comfortos.mqtt_topics.'.$group)) )
<div class="col-lg-12 card-margin">
    <div class="card">
        <div class="card-header" data-toggle="collapse" href="#mqtt-commands">
          <h5 class="card-title">MQTT Command Topics</h5>
        </div>
        <div class="card-body collapse" id ="mqtt-commands">
            @foreach (config ('comfortos.mqtt_topics.'.$group) AS $item_key => $commandable)
              @if ( $commandable != FALSE )
                <div class="form-group row">
                    <label for="{{ $item_key }}-command" class="col-sm-2 col-form-label pl-4">{{ $item_key }}</label>
                    <div class="col-sm-10">
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <span class="input-group-text" id=""><i class="fa fa-compass text-success"></i></span>
                          </div>
                          {!! Form::text ($item_key.'-command', $component->mqtt_topic .'/'.$item_key.'/command', ['id' => $item_key.'-command', 'class'=>'form-control', 'readonly']) !!}
                      </div>

                      <span class="form-text text-muted">
                        mosquitto_pub -h mqtt.{{ env ('APP_DOMAIN') }} -p 8883 -m "($COMMAND)" -u ($USERNAME) -P ($PASSWDRD) --capath ($CA_FILE) <br />
                        -t {{ $component->mqtt_topic .'/'.$item_key.'/state' }}
                      </span>

                    </div>
                </div>
              @endif
            @endforeach

        </div>
    </div>
</div>
@endif

@if (! in_array ($group, ['habs', 'dect', 'routers', 'mirrors', 'stbs']) )
<div class="col-lg-12 card-margin">
    <div class="card">
        <div class="card-header">
          <h5 class="card-title">REST API Endpoint</h5>
        </div>
        <div class="card-body">

          <div class="form-row">
            <div class="col-md-12">
                <div class="position-relative form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                    <span class="input-group-text" id=""><i class="fa fa-gem text-warning"></i></span>
                            </div>
                            {!! Form::text ('component_api', route ('api.'.$group.'.show', $component->_id), ['id' => 'component_api', 'class'=>'form-control', 'readonly']) !!}
                        </div>
                </div> <!-- end form-group -->
                <span class="form-text text-muted">
                  curl -X GET -H &quot;Accept: application/json&quot; -H &quot;Authorization: Bearer ${TOKEN}&quot; {{ route ('api.'.$group.'.show', $component->_id) }}
                </span>
             </div> <!-- end col -->
            </div>

        </div>
    </div>
</div>
@endif
