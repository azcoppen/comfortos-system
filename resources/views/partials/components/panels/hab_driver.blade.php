        <div class="table-responsive">
            <table class="table table-hover">
              <tbody>
                  <tr>
                      <td class="">driver</td>
                      <td class="">{{ $component->driver ?? '' }}</td>
                      <td class=""></td>
                  </tr>
                  @if ( isset ($component->items) && count ($component->items) )
                    @foreach ($component->items AS $key => $item)
                      <tr>
                          <td class="">{{ $key ?? '' }}</td>
                          <td class="">{{ $item ?? '' }}</td>
                          <td class="">
                            @if ( $item && $component->hab_item_endpoint )
                              {{ $component->hab_item_endpoint }}{{ $item ?? '' }}
                            @else
                              <small class="text-muted">No HAB provisioned</small>
                            @endif
                          </td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
        </div>
