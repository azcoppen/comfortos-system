<div wire:poll.10s class="col-lg-12 card-margin">
    <div class="card">
        <div class="card-header">
          @if (isset($search) && $search !== FALSE)
            <h5 class="card-title text-success"><i data-feather="search"></i> Commands: search results for &quot;{{ request()->get('q') }}&quot;</h5>
          @else
            <h5 class="card-title">Commands</h5>
            <div class="text-muted float-right col-md-11 text-right">Updated {{ now()->timezone (auth()->user()->timezone)->format ('H:i:s A') }}</div>
          @endif
        </div>
        <div class="card-body">
          @if ( isset ($commands) && count ($commands) )

            <div class="table-responsive">
              <table style="width: 100%;" id="" class="table table-hover">
                 <thead>
                 <tr>
                     <th></th>
                     <th>Component</th>
                     <th>Group</th>
                     <th>Attribute</th>
                     <th>Value</th>
                     <th>Sent</th>
                     <th>Recorded</th>
                 </tr>
                 </thead>
                 <tbody>
                     @foreach ($commands->take ($limit ?? 10) AS $command)
                       <tr>
                           <td><i class="fa fa-{{ config ('comfortos.fa_attribute_icons.'.$command->attribute) }} text-muted"></i></td>
                           <td>{{ $command->component_id }}</td>
                           <td>{{ $command->group }}</td>
                           <td class="text-{{ $command->response == 200 ? 'success' : 'danger' }}">{{ $command->attribute }}</td>
                           <td class="text-{{ $command->response == 200 ? 'success' : 'danger' }}">{{ $command->value }}</td>
                           <td>{{ $command->sent_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} <span class="text-muted">({{ $command->sent_at->diffForHumans() }})</span></td>
                           <td>{{ $command->created_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }}</td>
                       </tr>
                     @endforeach
                 </tbody>
               </table>
           </div>

          @else
            @include ('partials.components.alerts.empty')
          @endif
        </div>
    </div>
</div>
