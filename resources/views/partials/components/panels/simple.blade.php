<div class="col-lg-4 col-md-6 col-sm-6 ">
        <div class="card card-margin">
                <div class="card-body">


                  <div class="widget-4">
                      <div class="widget-4-icon option-1">
                          <i data-feather="{{ $icon ?? 'box' }}"></i>
                      </div>
                      <div class="widget-4-title">
                          {{ $big ?? '' }}
                          <small>{{ $small ?? '' }}</small>
                      </div>
                      <div class="widget-4-body">
                          <div class="widget-4-stat">
                              <!--<div class="widget-4-figure"><i class="text-success" data-feather="trending-up"></i>$5,900.00</div>-->
                              <!--<small>55% higher</small>-->
                          </div>
                          <div class="widget-4-redirect">
                              <a class="btn btn-rounded btn-widget-4-view" href="{{ $edit ?? '' }}"> Edit <i data-feather="edit-2"></i></a>
                              <a class="btn btn-rounded btn-widget-4-view" href="{{ $link ?? '' }}"> View {{ $view ?? '' }} <i data-feather="arrow-right"></i></a>
                          </div>
                      </div>
                  </div>

                </div>
        </div>
</div>
