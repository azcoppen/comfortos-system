<div class="col-lg-4 card-margin">
    <div class="card card-rounded">
        <div class="card-body">
            <div class="widget-4">
                <div class="widget-4-icon option-{{ $option ?? 1 }}">
                    <i data-feather="{{ $icon ?? 'alert-circle' }}"></i>
                </div>
                <div class="widget-4-title">
                    <a class="text-dark" href="{{ $link }}">
                      {{ $title ?? 'No Title' }}
                    </a>
                    <small>{!! $subtitle ?? 'No Subtitle' !!}</small>
                </div>
                <div class="widget-4-body">
                    <div class="widget-4-stat">
                        <div class="widget-4-figure">
                            {!! $figure ?? '' !!}
                        </div>
                        <small>{{ $footer_text }}</small>
                    </div>
                    <div class="widget-4-redirect">
                        @if ( isset ($edit) )
                          <a class="btn btn-rounded btn-widget-4-view" href="{{ $edit ?? '' }}"> Edit <i data-feather="edit-2"></i></a>
                        @endif
                        <a class="btn btn-rounded btn-widget-4-view" href="{{ $link ?? '' }}"> View <i data-feather="arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
