<div class="col-lg-12 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title col-md-10">Values By Date</h5>
            <div class="text-muted float-right col-md-2 text-right">Updated {{ now()->timezone (auth()->user()->timezone)->format ('H:i:s A') }}</div>
        </div>
        <div class="card-body">
          @if ( isset ($commands) && count ($commands) && $commands->where ('attribute', $attribute)->count () > 0 )

            <div class="table-responsive">
              <table style="width: 100%;" id="" class="table table-hover">
                 <thead>
                 <tr>
                     <th></th>
                     <th>Prev</th>
                     <th>Value</th>
                     @if ( $attribute == 'hsb' || $attribute == 'color' )
                       <th></th>
                     @endif
                     <th>Sent</th>
                     <th>Sent</th>
                     <th>Action</th>
                 </tr>
                 </thead>
                 <tbody>
                   @php ($previous = null) @endphp
                     @foreach ($commands->take ($limit ?? 200) AS $command)
                       <tr>
                           <td><i class="fa fa-{{ config ('comfortos.fa_attribute_icons.'.$command->attribute) }} text-muted"></i></td>
                           <td class="text-muted">{{ is_string ($previous) ? $previous : json_encode ($previous) }}</td>
                           <td class="text-{{ $command->response == 200 ? 'success' : 'danger' }}">{{ is_string ($command->value) ? $command->value : json_encode ($command->value) }}
                             @if ( $attribute == 'hsb' || $attribute == 'color' )
                               <td><div style="width: 10px; height: 10px; background-color: {{ hsb (is_array ($command->value) ? $command->value : explode (',', $command->value ?? []))->toHex() }}"></div></td>
                             @endif
                           </td>
                           <td>{{ $command->sent_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} <span class="text-muted">({{ $command->sent_at->diffForHumans() }})</span></td>
                           <td>{{ $command->created_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }}</td>
                           <td>
                             @if ( $previous )
                               @if ( is_numeric ($command->value) )
                                 @if ( $command->value && $command->value < $previous)
                                   <i class="fa fa-arrow-up text-success">/</i>
                                   <span class="badge badge-info">{{ round(abs ($previous - $command->value), 1) }}</span>
                                 @endif
                                 @if ( $command->value && $command->value > $previous)
                                   <i class="fa fa-arrow-down text-danger">/</i>
                                   <span class="badge badge-info">{{ round(abs ($previous - $command->value), 1) }}</span>
                                 @endif
                               @endif

                               @if ( $command->value && !is_numeric ($command->value) && $command->value != $previous)
                                 <i class="fa fa-arrow-left text-warning">/</i>
                               @endif

                             @endif
                           </td>
                       </tr>
                       @php ($previous = $command->value) @endphp
                     @endforeach
                     @php ($previous = null)
                 </tbody>
               </table>
           </div>

          @else
            @include ('partials.components.alerts.empty')
          @endif
        </div>
    </div>
</div>
