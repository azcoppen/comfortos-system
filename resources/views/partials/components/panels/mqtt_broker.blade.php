@if ( isset ($broker) && is_object ($broker) )
<div class="card card-margin">
    <div class="card-header">
        <h5 class="card-title"><i data-feather="life-buoy"></i> MQTT Broker</h5>
    </div>
    <div class="card-body">
      <p class="lead ml-3 mb-3">{{ $broker->label }} <br /><small class="text-muted">{{ $broker->_id }}</small></p>


      <div class="table-responsive">
        <table class="table table-hover">
          <tbody>
            <tr>
              <td class="text-muted">Software</td>
              <td>{{ $broker->type }} v{{ $broker->v }} [{{ $broker->context }}]</td>
            </tr>

            <tr>
              <td class="text-muted">VPN IP</td>
              <td>{{ $broker->vpn_id ?? 'None' }}</td>
            </tr>

            <tr>
              <td class="text-muted">Insecure</td>
              <td>{{ $broker->host }}:{{ $broker->insecure_port }}</td>
            </tr>

            <tr>
              <td class="text-muted">Secure</td>
              <td>{{ $broker->host }}:{{ $broker->secure_port }}</td>
            </tr>

            <tr>
              <td class="text-muted">Authorisation</td>
              <td>{{ $broker->app_user }} / {{ $broker->app_pass }}</td>
            </tr>

          </tbody>
        </table>
      </div>

    </div>
</div>
@else
  <div class="alert alert-bordered-info" role="alert">
    <span class="alert-text">No MQTT broker has been assigned.</span>
  </div>
@endif
