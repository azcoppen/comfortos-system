<div class="col-lg-4 col-md-6 ">
    <div class="card card-margin">
        <div class="card-header">
            <h5 class="card-title"><i data-feather="briefcase"></i> Provisioning</h5>
        </div>
        <div class="card-body">

          <div class="form-group">
            @include ('components.fields.text', [
              'label'       => "When was it provisioned?",
              'name'        => 'provisioned_at',
              'value'       => isset ($entity->provisioned_at) && is_object($entity->provisioned_at) ? $entity->provisioned_at->format('m/d/Y') : null,
              'default'     => '',
              'placeholder' => "Date the item was provisioned into the room",
              'required'    => false,
            ])
          </div>

          <div class="form-group">
            @include ('components.fields.select', [
              'label'       => "Who provisioned it?",
              'name'        => 'provisioned_by',
              'options'     => $users->each->setAppends(['full_name'])->pluck ('full_name', '_id')->all(),
              'value'       => $entity->provisioned_by ?? null,
              'default'     => '',
              'placeholder' => "Choose the user who provisioned the item",
              'required'    => false,
            ])
          </div>

        </div>
  </div>
</div> <!-- end col -->

@push ('js')
  <script>

  (function($) {
      $(function () {
          $('#provisioned_at').datepicker({format: 'mm/dd/yyyy'});
      });
  })(jQuery);

  </script>
@endpush
