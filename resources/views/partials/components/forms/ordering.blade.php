<div class="col-lg-4 col-md-6 ">
    <div class="card card-margin">
        <div class="card-header">
            <h5 class="card-title"><i data-feather="map"></i> Ordering</h5>
        </div>
        <div class="card-body">

          <div class="form-group">
            @include ('components.fields.text', [
              'label'       => "When was it ordered?",
              'name'        => 'ordered_at',
              'value'       => isset ($entity->ordered_at) && is_object($entity->ordered_at) ? $entity->ordered_at->format('m/d/Y') : null,
              'default'     => '',
              'placeholder' => "Date the item was ordered/purchased",
              'required'    => true,
            ])
          </div>

          <div class="form-group">
            @include ('components.fields.select', [
              'label'       => "Who ordered it?",
              'name'        => 'ordered_by',
              'options'     => $users->each->setAppends(['full_name'])->pluck ('full_name', '_id')->all(),
              'value'       => $entity->ordered_by ?? null,
              'default'     => '',
              'placeholder' => "Choose the user who ordered the item",
              'required'    => true,
            ])
          </div>

          <div class="form-group">
            @include ('components.fields.text', [
              'label'       => "What was the order ID?",
              'name'        => 'order_id',
              'value'       => $entity->order_id ?? null,
              'default'     => '',
              'placeholder' => "Database ID or reference number of the order",
              'required'    => false,
            ])
          </div>

        </div>
  </div>
</div> <!-- end col -->

@push ('js')
  <script>

  (function($) {
      $(function () {
          $('#ordered_at').datepicker({format: 'mm/dd/yyyy'});
      });
  })(jQuery);

  </script>
@endpush
