
  {!! Form::open (['method' => 'GET', 'class'=>'mt-2']) !!}
  <div class="form-group">
  <div class="input-group">
    {!! Form::search ('q', old ('q', request()->get('q')), ['class' => 'form-control '.(request()->has('q') ? 'is-valid' : ''), 'placeholder' => 'Search for '.$text ?? 'objects']) !!}
      <div class="input-group-append">
          <button class="btn btn-sm btn-{{ (request()->has('q') ? 'success' : 'primary') }}" type="submit"><i data-feather="search"></i></button>
      </div>
  </div>
</div>
  {!! Form::close() !!}
