<div class="col-lg-4 col-md-6 ">
    <div class="card card-margin">
        <div class="card-header">
            <h5 class="card-title"><i data-feather="anchor"></i> Installation</h5>
        </div>
        <div class="card-body">

          <div class="form-group">
            @include ('components.fields.text', [
              'label'       => "When was it installed?",
              'name'        => 'installed_at',
              'value'       => isset ($entity->installed_at) && is_object($entity->installed_at) ? $entity->installed_at->format('m/d/Y') : null,
              'default'     => '',
              'placeholder' => "Date the item was installed into the room",
              'required'    => false,
            ])
          </div>

          <div class="form-group">
            @include ('components.fields.select', [
              'label'       => "Who installed it?",
              'name'        => 'installed_by',
              'options'     => $users->each->setAppends(['full_name'])->pluck ('full_name', '_id')->all(),
              'value'       => $entity->installed_by ?? null,
              'default'     => '',
              'placeholder' => "Choose the user who installed the item",
              'required'    => false,
            ])
          </div>

        </div>
  </div>
</div> <!-- end col -->

@push ('js')
  <script>

  (function($) {
      $(function () {
          $('#installed_at').datepicker({format: 'mm/dd/yyyy'});
      });
  })(jQuery);

  </script>
@endpush
