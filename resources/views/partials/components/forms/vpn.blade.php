<div class="col-lg-6 col-md-6 ">
    <div class="card card-margin">
        <div class="card-header">
            <h5 class="card-title"><i data-feather="lock"></i> VPN Mapping</h5>
        </div>
        <div class="card-body">

          @if ( isset ($vpn_servers) && is_object ($vpn_servers) && count ($vpn_servers) )
            <div class="form-group">
              @include ('components.fields.select', [
                'label'       => "Which VPN is it on?",
                'name'        => 'vpn_server_id',
                'options'     => $vpn_servers->pluck ('label', '_id')->all(),
                'value'       => isset ($entity) && is_object ($entity->vpn_servers->first()) ? $entity->vpn_servers->first()->_id : null,
                'default'     => '',
                'placeholder' => "Choose a VPN to attach the item to",
                'required'    => false,
                'help'        => "This is needed to know how to connect the object.",
              ])
            </div>
          @endif

          @if ( isset ($vpn_clients) && is_object ($vpn_clients) && count ($vpn_clients) )
            <div class="form-group">
              @include ('components.fields.select', [
                'label'       => "Which VPN client does it use?",
                'name'        => 'vpn_client_id',
                'options'     => $vpn_clients->pluck ('label', '_id')->all(),
                'value'       => isset ($entity) && is_object ($entity->vpn_clients->first()) ? $entity->vpn_clients->first()->_id : null,
                'default'     => '',
                'placeholder' => "Choose a VPN client to attach the item to",
                'required'    => false,
                'help'        => "This is needed to flag the object's VPN access has been revoked.",
              ])
            </div>
          @endif

          <div class="form-group">
            @include ('components.fields.text', [
              'label'       => "What's it's external VPN address from its VPN server?",
              'name'        => 'vpn_ip',
              'value'       => $entity->vpn_ip ?? null,
              'default'     => '',
              'placeholder' => "VPN IP address of the item",
              'required'    => false,
              'help'        => "hostname -I | awk '{print $2}'",
            ])
          </div>
        </div>
  </div>
</div> <!-- end col -->
