<div class="alert alert-info alert-dismissible fade show" role="alert">
    <i data-feather="alert-circle" class="alert-icon"></i>
    <span class="alert-text"><strong>No records available.</strong></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i data-feather="x" class="alert-close"></i>
    </button>
</div>
