<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <i data-feather="alert-triangle" class="alert-icon"></i>
    <span class="alert-text"><strong>{{ $msg ?? session('error') }}</strong></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i data-feather="x" class="alert-close"></i>
    </button>
</div>
