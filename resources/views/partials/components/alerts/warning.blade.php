<div class="alert alert-warning alert-dismissible fade show text-dark" role="alert">
    <i data-feather="alert-triangle" class="alert-icon"></i>
    <span class="alert-text"><strong>{{ $msg ?? session('warning') }}</strong></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i data-feather="x" class="alert-close"></i>
    </button>
</div>
