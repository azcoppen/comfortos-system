<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-center text-sm-left d-block d-sm-inline-block">
        </span>
        <div class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">
          &copy; {{ date('Y') }} <a href="mailto:acamerondev@protonmail.com" target="_blank">Alex Cameron</a>. All rights reserved.
        </div>
    </div>
  </footer>
