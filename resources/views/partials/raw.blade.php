<ul class="list-group">
    @foreach ($obj->toArray() AS $attr => $value)
        @if ( isset($except) && count ($except) && in_array ($attr, $except) )
        @else

            <li class="list-group-item">
                <div class="col-sm-4 text-muted">
                    {{ $attr }}
                </div>
                <div class="col-sm-8">
                    @switch ($attr)
                        @case ('created_at')
                        @case ('updated_at')
                          {{ $value }} <span class="text-muted">({{ Carbon\Carbon::parse($value)->timezone('EST')->format ('D dS M Y H:iA') }})</span>
                        @break
                        @default

                            @if ( is_string ($value) )
                                {{ $value }}
                            @else
                                <code><pre>{{ json_encode ($value, JSON_PRETTY_PRINT) }}</pre></code>
                            @endif

                        @break
                    @endswitch
                </div>
            </li>

        @endif

    @endforeach
</ul>
