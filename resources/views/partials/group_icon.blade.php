@if ( isset ($group) && $group )
    @switch ($group)
        @case ('contact-sensors')
        @case ('multi-sensors')
        <i class="text-warning" data-feather="target"></i>
        @break
        @case ('hubs-bridges')
        <i class="text-warning" data-feather="upload-cloud"></i>
        @break
        @case ('bulbs')
        <i class="text-warning" data-feather="sun"></i>
        @break
        @case ('locks')
        <i class="text-warning" data-feather="lock"></i>
        @break
        @case ('thermostats')
        <i class="text-warning" data-feather="thermometer"></i>
        @break
        @case ('speakers')
        <i class="text-warning" data-feather="speaker"></i>
        @break
        @case ('smoke-detectors')
        <i class="text-warning" data-feather="alert-triangle"></i>
        @break
    @endswitch
@endif
