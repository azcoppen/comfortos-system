const centrifuge = new Centrifuge("{{ $ws_broker_url }}");
centrifuge.setToken("{{ session('centrifugo_jwt_token') }}");

centrifuge.on('connect', function(ctx) {
  if (document.getElementById('live-feed'))
  {
    document.getElementById('live-feed').innerHTML = "Websocket connected: {{ $ws_broker_url }}";
  }
  if (document.getElementById('component-feed'))
  {
    document.getElementById('component-feed').innerHTML = "Websocket connected: {{ $ws_broker_url }}";
  }
});

centrifuge.on('disconnect', function(ctx) {
  if (document.getElementById('live-feed'))
  {
    document.getElementById('live-feed').innerHTML = "Websocket disconnected.";
  }
  if (document.getElementById('component-feed'))
  {
    document.getElementById('component-feed').innerHTML = "Websocket disconnected.";
  }
});

centrifuge.subscribe("opqueue", function(ctx) {
  console.info (ctx.data);

  if (document.getElementById('live-feed'))
  {
    document.getElementById('live-feed').innerHTML = '<span class="">[' + ctx.data.domain +"/"+ ctx.data.src+"/"+ctx.data.room_id+"]</span> " + ctx.data._id + ' <span class="">'+ctx.data.operation + '</span>: <strong class="">' + ctx.data.request + "</strong> ("+ moment(ctx.data.created_at).fromNow() +")";
  }

});

centrifuge.connect();
