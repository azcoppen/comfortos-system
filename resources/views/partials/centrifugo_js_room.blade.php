@if ( isset ($ws_broker) && is_object ($ws_broker) )

const centrifuge = new Centrifuge("{{ $ws_broker->wss_url  }}/{{ $ws_broker->conn_endpoint ?? '' }}", { debug: true });
centrifuge.setToken("{{ $centrifugo_room_token ?? 'token-not-set' }}");

centrifuge.on('error', function(err) {
  console.log(err);
});

centrifuge.on('connect', function(ctx) {
  console.info(ctx);
  if (document.getElementById('live-feed'))
  {
    document.getElementById('live-feed').innerHTML = "Websocket connected: {{ $ws_broker->host }}";
  }
  if (document.getElementById('component-feed'))
  {
    document.getElementById('component-feed').innerHTML = "Websocket connected: {{ $ws_broker->host }}";
  }
});

centrifuge.on('disconnect', function(ctx) {
  console.log(ctx);
  if (document.getElementById('live-feed'))
  {
    document.getElementById('live-feed').innerHTML = "Websocket disconnected.";
  }
  if (document.getElementById('component-feed'))
  {
    document.getElementById('component-feed').innerHTML = "Websocket disconnected.";
  }
});

centrifuge.subscribe("rooms.{{ $room->_id }}", function(ctx) {
  console.info (ctx.data);

  //trigger_event (document, ctx.data.component, ctx.data);

  if ( document.getElementById(ctx.data.component+'-attribute') )
  {
    document.getElementById(ctx.data.component+'-attribute').innerHTML = ctx.data.attribute;
    document.getElementById(ctx.data.component+'-value').innerHTML = ctx.data.value;
    document.getElementById(ctx.data.component+'-received').innerHTML = moment(ctx.data.received_at).format("MMM D h:mm:ss A");
    if ( document.getElementById(ctx.data.component+'-created') )
    {
      document.getElementById(ctx.data.component+'-created').innerHTML = moment(ctx.data.created_at).format("MMM D h:mm:ss A");
    }
  }
  if (document.getElementById('live-feed'))
  {
    if (ctx.data.group != 'routers')
    {
      document.getElementById('live-feed').innerHTML = '<span class="">[' + ctx.data.domain +"/"+ ctx.data.event+"]</span> " + ctx.data.component + ' <span class="">'+ctx.data.attribute + '</span>: <strong class="">' + ctx.data.value + "</strong> ("+ moment(ctx.data.created_at).fromNow() +")";
    }

  }
  if (document.getElementById('component-feed'))
  {
    if (document.getElementById('component-feed').getAttribute("data-component") == ctx.data.component )
    {
      if (ctx.data.group != 'routers')
      {
        document.getElementById('component-feed').innerHTML = '<span class="">[' + ctx.data.domain +"/"+ ctx.data.event+"]</span> " + ctx.data.component + ' <span class="">'+ctx.data.attribute + '</span>: <strong class="">' + ctx.data.value + "</strong> ("+ moment(ctx.data.created_at).fromNow() +")";
      }
    }
  }
});

try
{
  centrifuge.connect();
}
catch (e)
{
  console.log(e);
}

@endif
