<div class="row">
  <div class="col-md-12 text-right">
    <a class="btn btn-primary" href="{{ route ($route) }}">Add New {{ $object ?? 'Object' }}</a>
  </div>
</div>
<hr />
