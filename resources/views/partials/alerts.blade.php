@if (session()->has('info'))
  @include ('partials.components.alerts.info')
@endif

@if (session()->has('error'))
  @include ('partials.components.alerts.error')
@endif

@if ($errors && $errors->count() > 0)
  @include ('partials.components.alerts.validation-error')
@endif

@if (session()->has('warning'))
  @include ('partials.components.alerts.warning')
@endif

@if (session()->has('success'))
  @include ('partials.components.alerts.success')
@endif
