@if ( isset ($data) && is_array ($data) )

        @if (count($data) == 1)
        <div class="row">
            <div class="col-md-6">
                <table class="table table-sm">
                    <tbody>
                        @foreach (head($data) AS $key => $val)
                        <tr>
                            <td class="border-0 text-muted" width="50%">{{ $key }}</td>
                            <td class="border-0">{{ $val }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @else
            <div class="row">
                @foreach ($data AS $item)
                <div class="col-md-4">
                    <table class="table table-sm">
                        <tbody>
                            @foreach ($item AS $key => $val)
                            <tr>
                                <td class="border-0 text-muted" width="50%">{{ $key }}</td>
                                <td class="border-0">{{ $val }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endforeach
            </div>
        @endif
@endif
