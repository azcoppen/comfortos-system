<div class="col-lg-4 card-margin">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Provisioning</h5>
        </div>
        <div class="card-body">

          @if ( isset ($room) && is_object ($room) )
            <p class="lead">
            {{ $room->label }}
            </p>

            <br />

            <p class="text-muted">
                {!! collect ($room->building->property->location)->implode (', ') !!}
            </p>
            <p class="text-muted">
                {!! collect ($room->building->property->geo['coordinates'])->implode (', ') !!}
                <br />
                {{ $room->building->property->timezone ?? '' }} / {{ $room->building->property->currency ?? '' }}
            </p>

            <small class="text-muted">
                {{ $item->created_at->timezone(auth()->user()->timezone)->format('M d Y H:iA') }} ({{ $item->created_at->diffForHumans() }})<br />
                {{ $item->updated_at->timezone(auth()->user()->timezone)->format('M d Y H:iA') }} ({{ $item->updated_at->diffForHumans() }})
            </small>
          @else
            <div class="alert alert-bordered-warning" role="alert">
              <span class="alert-text">This object has not been provisioned to a room.</span>
            </div>
          @endif
        </div>
    </div>
</div>
