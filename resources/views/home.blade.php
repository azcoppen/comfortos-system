@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Dashboard</title>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Operation Queue</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="layers"></i> Home</a>
              <a href="{{ route('tvs.index') }}" class="text-primary">TV</a>
              <a href="{{ route('tvs.stbs.index') }}" class="text-primary">Set Top Boxes</a>
          </div>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('sidebar')
@endsection

@section('content')
    @include ('op-queue')
@endsection
