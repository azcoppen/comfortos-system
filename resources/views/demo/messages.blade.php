<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="AC">
    <title>SmartRooms Demo</title>
    <link rel="apple-touch-icon" href="https://smartrooms.b-cdn.net/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://smartrooms.b-cdn.net/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/pages/chat-application.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu content-left-sidebar chat-application " data-open="hover" data-menu="horizontal-menu" data-color="bg-gradient-x-purple-blue" data-col="content-left-sidebar">

    <!-- BEGIN: Header-->
    <!-- fixed-top-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-static-top navbar-light navbar-brand-center">
      <div class="navbar-header">
          <ul class="nav navbar-nav flex-row">
              <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
              <li class="nav-item"><a class="navbar-brand" href="{{ route ('demo.index') }}"><img class="brand-logo" alt="creaative admin logo" src="https://smartrooms.b-cdn.net/app-assets/images/logo/logo.png">
                      <h3 class="brand-text">Lab Demo</h3>
                  </a></li>
              <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
          </ul>
      </div>
      <div class="navbar-wrapper">
          <div class="navbar-container content">
              <div class="collapse navbar-collapse" id="navbar-mobile">
                  <ul class="nav navbar-nav mr-auto float-left">
                      <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                      <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>

                  </ul>
                  <ul class="nav navbar-nav float-right">
                      <li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-us"></i><span class="selected-language"></span></a>
                          <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                              <div class="arrow_box"><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-us"></i> English</a></div>
                          </div>
                      </li>


                      <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"> <span class="avatar avatar-online"><img src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-19.png" alt="avatar"></span></a>
                          <div class="dropdown-menu dropdown-menu-right">
                              <div class="arrow_box_right"><a class="dropdown-item" href="#"><span class="user-name text-bold-700 ml-1">{{ auth()->user()->first }} {{ auth()->user()->last }}</span></span></a>

                                  <div class="dropdown-divider"></div><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="ft-power"></i> Logout</a>
                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>
                              </div>
                          </div>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-static navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'checkin' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'checkin') }}"><i class="ft-map-pin"></i><span>Checkin</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'messages' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'messages') }}"><i class="ft-mail"></i><span>Messages (<strong>2</strong>)</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'lock' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'lock') }}"><i class="ft-lock"></i><span>Door Lock</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'phone' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'phone') }}"><i class="ft-phone-call"></i><span>Phone</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'wifi' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'wifi') }}"><i class="ft-wifi"></i><span>Wifi</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'tv' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'tv') }}"><i class="ft-airplay"></i><span>TV</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'thermostat' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'thermostat') }}"><i class="ft-thermometer"></i><span>Thermostat</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'lighting' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'lighting') }}"><i class="ft-sun"></i><span>Lighting</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'speaker' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'speaker') }}"><i class="ft-volume-2"></i><span>Speaker</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'camera' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'camera') }}"><i class="ft-camera"></i><span>Camera</span></a></li>
              <li class="nav-item {{ isset ($nav_active) && $nav_active == 'sensors' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'sensors') }}"><i class="ft-radio"></i><span>Sensors</span></a></li>

            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="sidebar-left sidebar-fixed">
            <div class="sidebar">
                <div class="sidebar-content card d-none d-lg-block">
                    <div class="card-body chat-fixed-search">
                        <fieldset class="form-group position-relative has-icon-left m-0  w-75 display-inline">
                            <input type="text" class="form-control round" id="searchUser" placeholder="Search user">
                            <div class="form-control-position">
                                <i class="ft-search"></i>
                            </div>
                        </fieldset>
                        <span class="float-right  primary font-large-1 cursor-pointer"> <i class="ft-edit"></i> </span>
                    </div>
                    <div id="users-list" class="list-group position-relative">
                        <div class="users-list-padding media-list">
                            <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                                <div class="media-left pr-1">
                                    <span class="avatar avatar-md"><img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-3.png" alt="Generic placeholder image">
                                    </span>
                                </div>
                                <div class="media-body w-100">
                                    <h6 class="list-group-item-heading font-medium-1 text-bold-700">John Elliott&nbsp; <i class="ft-circle font-small-2 success"></i><span class="float-right primary"><span class="badge badge-pill badge-danger lighten-3">1</span></span></h6>
                                    <p class="font-small-3 text-muted text-bold-500">3:55 PM</p>
                                    <p class="list-group-item-text text-muted mb-0 lighten-1">I was looking at your profile. The design looks better... </p>
                                </div>
                            </a>
                            <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5 border-right-primary  border-right-2">
                                <div class="media-left pr-1">
                                    <span class="avatar avatar-md"><img class="media-object rounded-circle box-shadow-3" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-7.png" alt="Generic placeholder image">
                                    </span>
                                </div>
                                <div class="media-body w-100">
                                    <h6 class="list-group-item-heading font-medium-1 text-bold-700">Chris Candy&nbsp; <i class="ft-circle font-small-2 success"></i><span class="float-right primary"><span class="badge badge-pill badge-danger lighten-3">3</span></span></h6>
                                    <p class="font-small-3 text-muted text-bold-500">9:04 PM</p>
                                    <p class="list-group-item-text text-muted mb-0 lighten-1">Thanks and have a great weekend. Hope to see you soon... </p>
                                </div>
                            </a>
                            <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                                <div class="media-left pr-1">
                                    <span class="avatar avatar-md"><img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-8.png" alt="Generic placeholder image">
                                    </span>
                                </div>
                                <div class="media-body w-100">
                                    <h6 class="list-group-item-heading font-medium-1 text-bold-700">Sara Cena&nbsp; <i class="ft-circle font-small-2 danger"></i></h6>
                                    <p class="font-small-3 text-muted text-bold-500">1:55 AM</p>
                                    <p class="list-group-item-text text-muted mb-0 lighten-1">Hello john! Can we meet and discuss few issues... </p>
                                </div>
                            </a>
                            <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                                <div class="media-left pr-1">
                                    <span class="avatar avatar-md"><img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-7.png" alt="Generic placeholder image">
                                    </span>
                                </div>
                                <div class="media-body w-100">
                                    <h6 class="list-group-item-heading font-medium-1 text-bold-700">Wayne Bruto&nbsp; <i class="ft-circle font-small-2 warning"></i></h6>
                                    <p class="font-small-3 text-muted text-bold-500">Today</p>
                                    <p class="list-group-item-text text-muted mb-0 lighten-1">Can we connect and explore few topics togather?... </p>
                                </div>
                            </a>
                            <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                                <div class="media-left pr-1">
                                    <span class="avatar avatar-md"><img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-5.png" alt="Generic placeholder image">
                                    </span>
                                </div>
                                <div class="media-body w-100">
                                    <h6 class="list-group-item-heading font-medium-1 text-bold-700">Sarah Mery&nbsp; <i class="ft-circle font-small-2 danger"></i></h6>
                                    <p class="font-small-3 text-muted text-bold-500">Yesterday</p>
                                    <p class="list-group-item-text text-muted mb-0 lighten-1">Will contact you soon and have a great weekend... </p>
                                </div>
                            </a>
                            <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                                <div class="media-left pr-1">
                                    <span class="avatar avatar-md"><img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-9.png" alt="Generic placeholder image">
                                    </span>
                                </div>
                                <div class="media-body w-100">
                                    <h6 class="list-group-item-heading font-medium-1 text-bold-700">Heather Bell&nbsp; <i class="ft-circle font-small-2 danger"></i></h6>
                                    <p class="font-small-3 text-muted text-bold-500">Friday</p>
                                    <p class="list-group-item-text text-muted mb-0 lighten-1">Thank you for your support and will see you soon... </p>
                                </div>
                            </a>
                            <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                                <div class="media-left pr-1">
                                    <span class="avatar avatar-md"><img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-7.png" alt="Generic placeholder image">
                                    </span>
                                </div>
                                <div class="media-body w-100">
                                    <h6 class="list-group-item-heading font-medium-1 text-bold-700">Kelly Reyes&nbsp; <i class="ft-circle font-small-2 success"></i></h6>
                                    <p class="font-small-3 text-muted text-bold-500">Thursday</p>
                                    <p class="list-group-item-text text-muted mb-0 lighten-1">The company will have meeting tonight. Please attend... </p>
                                </div>
                            </a>
                            <a href="#" class="media border-bottom-blue-grey border-bottom-lighten-5">
                                <div class="media-left pr-1">
                                    <span class="avatar avatar-md"><img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-14.png" alt="Generic placeholder image">
                                    </span>
                                </div>
                                <div class="media-body w-100">
                                    <h6 class="list-group-item-heading font-medium-1 text-bold-700">Vill Nelson&nbsp; <i class="ft-circle font-small-2 warning"></i></h6>
                                    <p class="font-small-3 text-muted text-bold-500">Monday</p>
                                    <p class="list-group-item-text text-muted mb-0 lighten-1">Who you are? Please provide more details about you... </p>
                                </div>
                            </a>


                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="content-right">
            <div class="content-wrapper">
                <div class="content-wrapper-before"></div>
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="chat-app-window">
                        <div class="mb-1 secondary text-bold-700">Chat History</div>
                        <div class="chats">
                            <div class="chats">
                                <div class="chat">
                                    <div class="chat-avatar">
                                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                                            <img src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-1.png" class="box-shadow-4" alt="avatar" />
                                        </a>
                                    </div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p>How are you Chris?</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat chat-left">
                                    <div class="chat-avatar">
                                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="" data-original-title="">
                                            <img src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-7.png" class="box-shadow-4" alt="avatar" />
                                        </a>
                                    </div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p>Hey John, I am good.</p>
                                            <p>Could you please send me a pic?</p>
                                        </div>
                                        <div class="chat-content">
                                            <p>It should be of smaller size.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat">
                                    <div class="chat-avatar">
                                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                                            <img src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-1.png" class="box-shadow-4" alt="avatar" />
                                        </a>
                                    </div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <img class="img-thumbnail img-fluid" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-1.png" alt="Image description">
                                        </div>
                                        <div class="chat-content">
                                            <p>Here you go! Let me know if you need anything else.</p>
                                        </div>
                                    </div>
                                </div>
                                <p class="time">25 minutes ago</p>
                                <div class="chat chat-left">
                                    <div class="chat-avatar">
                                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="" data-original-title="">
                                            <img src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-7.png" class="box-shadow-4" alt="avatar" />
                                        </a>
                                    </div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p>Looks cool!</p>
                                        </div>
                                        <div class="chat-content">
                                            <p>It's perfect for my next project.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat">
                                    <div class="chat-avatar">
                                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                                            <img src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-1.png" class="box-shadow-4" alt="avatar" />
                                        </a>
                                    </div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p>Thanks, glad to help you.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat chat-left">
                                    <div class="chat-avatar">
                                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="" data-original-title="">
                                            <img src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-7.png" class="box-shadow-4" alt="avatar" />
                                        </a>
                                    </div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p>I will talk you later.</p>
                                        </div>
                                        <div class="chat-content">
                                            <p>Bye.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat">
                                    <div class="chat-avatar">
                                        <a class="avatar" data-toggle="tooltip" href="#" data-placement="right" title="" data-original-title="">
                                            <img src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-1.png" class="box-shadow-4" alt="avatar" />
                                        </a>
                                    </div>
                                    <div class="chat-body">
                                        <div class="chat-content">
                                            <p>Sure, Feel free to get in touch.</p>
                                        </div>
                                        <div class="chat-content">
                                            <p>Bye</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="chat-app-form">
                        <form class="chat-app-input d-flex">
                            <fieldset class="col-10 m-0">
                                <div class="input-group position-relative has-icon-left">
                                    <div class="form-control-position">
                                        <span id="basic-addon3"><i class="ft-image"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Send message" aria-describedby="button-addon2">
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left col-2 m-0">
                                <button type="button" class="btn btn-danger">
                                    <i class="la la-paper-plane-o d-xl-none"></i>
                                    <span class="d-none d-lg-none d-xl-block">Send </span>
                                </button>
                            </fieldset>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->





    <!-- BEGIN: Vendor JS-->
    <script src="https://smartrooms.b-cdn.net/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script type="text/javascript" src="https://smartrooms.b-cdn.net/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="https://smartrooms.b-cdn.net/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="https://smartrooms.b-cdn.net/app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="https://smartrooms.b-cdn.net/app-assets/js/scripts/pages/chat-application.js" type="text/javascript"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
