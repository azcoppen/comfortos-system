@extends ('demo.index')
@section ('headline') Door Lock @stop
@section ('content')
  <div class="row">
      <div class="col-lg-3 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Smart Lock 1</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <div class="card-text">
                      <p>Locks can have up to 6 codes. This will set the first slot of lock 1.</p>
                  </div>
                  <form class="form">
                    <div class="form-body">
                      <div class="form-group">
                          <label for="lock-1">Set the keycode for smart lock 1.</label>
                          <div class="position-relative has-icon-left">
                              <input type="number" class="form-control input-xl" name="lock-1" value = "9927" required autofocus>
                              <div class="form-control-position">
                                  <i class="la la-lock"></i>
                              </div>
                          </div>
                      </div>
                      <div class="form-actions center">
                          <button type="submit" class="btn btn-primary">
                              <i class="la la-check-square-o"></i> Save To Device
                          </button>
                      </div>
                    </div>
                  </form>
                </div>

              </div>
          </div>
      </div>
      <div class="col-lg-3 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Smart Lock 2</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">

                  <div class="card-body">
                    <div class="card-text">
                        <p>Locks can have up to 6 codes. This will set the first slot of lock 2.</p>
                    </div>
                    <form class="form">
                      <div class="form-body">
                        <div class="form-group">
                            <label for="lock-2">Set the keycode for smart lock 2.</label>
                            <div class="position-relative has-icon-left">
                                <input type="number" class="form-control input-xl" name="lock-2" value = "1003" disabled>
                                <div class="form-control-position">
                                    <i class="la la-lock"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center">
                            <button type="submit" class="btn btn-primary" disabled>
                                <i class="la la-check-square-o"></i> Save To Device
                            </button>
                        </div>
                      </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Smart Lock 3</h4>
              </div>
              <div class="card-content collapse show">
                  <div class="card-body">
                    <div class="card-text">
                        <p>Locks can have up to 6 codes. This will set the first slot of lock 3.</p>
                    </div>
                    <form class="form">
                      <div class="form-body">
                        <div class="form-group">
                            <label for="lock-3">Set the keycode for smart lock 3.</label>
                            <div class="position-relative has-icon-left">
                                <input type="number" class="form-control input-xl" name="lock-3" value = "6576" disabled>
                                <div class="form-control-position">
                                    <i class="la la-lock"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center">
                            <button type="submit" class="btn btn-primary" disabled>
                                <i class="la la-check-square-o"></i> Save To Device
                            </button>
                        </div>
                      </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Smart Lock 4</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                  <div class="card-body">
                    <div class="card-text">
                        <p>Locks can have up to 6 codes. This will set the first slot of lock 4.</p>
                    </div>
                    <form class="form">
                      <div class="form-body">
                        <div class="form-group">
                            <label for="lock-4">Set the keycode for smart lock 4.</label>
                            <div class="position-relative has-icon-left">
                                <input type="number" class="form-control input-xl" name="lock-4" value = "5190" disabled>
                                <div class="form-control-position">
                                    <i class="la la-lock"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center">
                            <button type="submit" class="btn btn-primary" disabled>
                                <i class="la la-check-square-o"></i> Save To Device
                            </button>
                        </div>
                      </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
@stop
