@extends ('demo.index')
@section ('headline') Wifi @stop
@section ('content')
  <div class="row">
    <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Wifi Status</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                  <div class="card-text">
                      <p>The values below are summarised from a live polling interval supplied by the access point's monitoring API.</p>
                  </div>
                  <table class="table table-bordered table-responsive-lg">
                      <thead>
                          <tr>
                              <th scope="col">Meter</th>
                              <th scope="col">Value</th>
                          </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <th scope="row">Status</th>
                            <td>connected-to-ess</td>
                        </tr>
                          <tr>
                              <th scope="row">Band</th>
                              <td>2.4ghz-g</td>
                          </tr>
                          <tr>
                              <th scope="row">Frequency</th>
                              <td>2412MHz</td>
                          </tr>
                          <tr>
                              <th scope="row">TX Rate</th>
                              <td>54Mbps</td>
                          </tr>
                          <tr>
                              <th scope="row">RX Rate</th>
                              <td>54Mbps</td>
                          </tr>
                          <tr>
                              <th scope="row">SSID</th>
                              <td>MIKROTIK</td>
                          </tr>
                          <tr>
                              <th scope="row">Mac</th>
                              <td>00:0C:42:05:00:14</td>
                          </tr>
                          <tr>
                              <th scope="row">Radio</th>
                              <td>000C42050014</td>
                          </tr>
                          <tr>
                              <th scope="row">Signal</th>
                              <td>-23dBm</td>
                          </tr>
                          <tr>
                              <th scope="row">Noise</th>
                              <td>73dB</td>
                          </tr>
                          <tr>
                              <th scope="row">Clients</th>
                              <td>1</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
      <div class="col-lg-4 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Set Wifi Network Name(s)</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                  <div class="card-body">
                    <div class="card-text">
                        <p>The access point uses multiple security profiles. A user can set their own custom profile, but if a fault or reboot occurs, the device will restart with its default profile.</p>
                    </div>
                    <form class="form">
                      <div class="form-body">
                        <div class="form-group">
                            <label for="ssid-custom">Set your personal choice of SSID for the Wifi network.</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" class="form-control input-xl" name="ssid-custom" value = "SMARTROOMS-WIFI" placeholder="MyPersonalChoice" required>
                                <div class="form-control-position">
                                    <i class="la la-wifi"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center">
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-check-square-o"></i> Save To Device
                            </button>
                        </div>
                      </div>
                    </form>

                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Set Wifi Password</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="card-text">
                        <p>The default security profile always has the same default password (although that can be changed, if necessary).</p>
                    </div>

                  <form class="form">
                    <div class="form-body">
                      <div class="form-group">
                          <label for="wifi-password">Set your personal password for your custom SSID.</label>
                          <div class="position-relative has-icon-left">
                              <input type="text" class="form-control input-xl" name="wifi-password" value = "cstr8media" placeholder="somepassword" required>
                              <div class="form-control-position">
                                  <i class="la la-lock"></i>
                              </div>
                          </div>
                      </div>
                      <div class="form-actions center">
                          <button type="submit" class="btn btn-primary">
                              <i class="la la-check-square-o"></i> Save To Device
                          </button>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
      </div>
  </div>
@stop
