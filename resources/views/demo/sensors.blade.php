@extends ('demo.index')
@section ('headline') Sensors @stop
@section ('content')
  <div class="row">
      <div class="col-lg-3 col-md-6">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Motion Sensor</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                  <div class="card-body p-0 pb-0">

                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Smoke Detector</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

            </div>
            <div class="card-content collapse show">
                <div class="card-body p-0 pb-0">

                </div>
            </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Water Leaks</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

            </div>
            <div class="card-content collapse show">
                <div class="card-body p-0 pb-0">

                </div>
            </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Window Contact</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

            </div>
            <div class="card-content collapse show">
                <div class="card-body p-0 pb-0">

                </div>
            </div>
        </div>
      </div>
  </div>
@stop
