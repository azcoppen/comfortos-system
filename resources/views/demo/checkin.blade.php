@extends ('demo.index')
@section ('headline') Remote Check-In @stop
@section ('content')
  <div class="row">
      <div class="col-lg-12 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Choose A Saved Guest Registration Card Profile</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                  <div class="card-body">

                    <div id="accordion3" class="card-accordion">
                        <div class="card collapse-icon accordion-icon-rotate">
                            <div class="card">
                                <div class="card-header" id="headingGOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#accordionC1" aria-expanded="true" aria-controls="accordionC1">
                                            <i class="ft-map-pin"></i> Personal
                                        </button>
                                    </h5>
                                </div>

                                <div id="accordionC1" class="collapse show" aria-labelledby="headingGOne" data-parent="#accordion3" style="">
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-lg-6 col-md-12">

                                          <form class="form">
                                            <div class="form-body">
                                              <h4 class="form-section"><i class="ft-camera"></i> Upload ID Document</h4>
                                              <div class="card-text">
                                                  <p>This is the copy of your identification which will be sent to the front desk which you will need to carry with you when you arrive. It is stored encrypted.</p>
                                              </div>

                                              <img src="/demo/id.jpg" width="80%" />
                                            </div>
                                          </form>

                                          <form class="form mt-2">
                                            <div class="form-body">
                                              <h4 class="form-section"><i class="ft-edit"></i> Upload Signature</h4>

                                              <div class="card-text">
                                                  <p>This image will automatically be imprinted onto your registration as a legal mark of your contractual acceptance.</p>
                                              </div>

                                              <img src="/demo/signature.png" width="80%" />
                                            </div>
                                          </form>
                                        </div>
                                        <div class="col-lg-6 col-md-12">

                                          <form class="form">
                                            <div class="form-body">
                                              <h4 class="form-section"><i class="ft-disk"></i> Saved Details</h4>
                                              <div class="card-text">
                                                  <p>These are the details which will be automatically filled into your guest registration card and sent to the hospitality provider's system.</p>
                                              </div>

                                              <div class="form-body">

                                                  <div class="form-group">
                                                      <label for="eventRegInput1">Full Name</label>
                                                      <div class="position-relative has-icon-left">
                                                        <input type="text" id="eventRegInput1" class="form-control" placeholder="name" name="fullname" value="Jennifer Sample" required>
                                                          <div class="form-control-position">
                                                              <i class="ft-user"></i>
                                                          </div>
                                                      </div>
                                                  </div>

                                                  <div class="form-group">
                                                      <label for="eventRegInput2">Address</label>
                                                      <div class="position-relative has-icon-left">
                                                        <input type="text" id="eventRegInput2" class="form-control" placeholder="address" name="address" value="4802 Sheboygan Ave" required>
                                                        <div class="form-control-position">
                                                            <i class="ft-map-pin"></i>
                                                        </div>
                                                    </div>
                                                  </div>

                                                  <div class="form-group">
                                                      <label for="eventRegInput3">City</label>
                                                      <div class="position-relative has-icon-left">
                                                        <input type="text" id="eventRegInput3" class="form-control" placeholder="city" name="city" value="Madison" required>
                                                        <div class="form-control-position">
                                                            <i class="ft-map-pin"></i>
                                                        </div>
                                                      </div>
                                                  </div>

                                                  <div class="form-group">
                                                      <label for="eventRegInput4">State</label>
                                                      <select id="projectinput5" name="state" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="state" data-original-title="" title="">
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI" selected>Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                      </select>
                                                  </div>

                                                  <div class="form-group">
                                                      <label for="eventRegInput4">Zip</label>
                                                      <div class="position-relative has-icon-left">
                                                        <input type="number" id="eventRegInput9" class="form-control" placeholder="state" name="zip" value="53705" required>
                                                          <div class="form-control-position">
                                                              <i class="ft-map-pin"></i>
                                                          </div>
                                                      </div>
                                                  </div>

                                                  <div class="form-group">
                                                    <label for="companyinput5">Country</label>
                                                    <select id="companyinput5" name="country" class="form-control">
                                                      <option value="ISO 3166-2:AF">Afghanistan</option>
                                                    	<option value="ISO 3166-2:AX">Åland Islands</option>
                                                    	<option value="ISO 3166-2:AL">Albania</option>
                                                    	<option value="ISO 3166-2:DZ">Algeria</option>
                                                    	<option value="ISO 3166-2:AS">American Samoa</option>
                                                    	<option value="ISO 3166-2:AD">Andorra</option>
                                                    	<option value="ISO 3166-2:AO">Angola</option>
                                                    	<option value="ISO 3166-2:AI">Anguilla</option>
                                                    	<option value="ISO 3166-2:AQ">Antarctica</option>
                                                    	<option value="ISO 3166-2:AG">Antigua and Barbuda</option>
                                                    	<option value="ISO 3166-2:AR">Argentina</option>
                                                    	<option value="ISO 3166-2:AM">Armenia</option>
                                                    	<option value="ISO 3166-2:AW">Aruba</option>
                                                    	<option value="ISO 3166-2:AU">Australia</option>
                                                    	<option value="ISO 3166-2:AT">Austria</option>
                                                    	<option value="ISO 3166-2:AZ">Azerbaijan</option>
                                                    	<option value="ISO 3166-2:BS">Bahamas</option>
                                                    	<option value="ISO 3166-2:BH">Bahrain</option>
                                                    	<option value="ISO 3166-2:BD">Bangladesh</option>
                                                    	<option value="ISO 3166-2:BB">Barbados</option>
                                                    	<option value="ISO 3166-2:BY">Belarus</option>
                                                    	<option value="ISO 3166-2:BE">Belgium</option>
                                                    	<option value="ISO 3166-2:BZ">Belize</option>
                                                    	<option value="ISO 3166-2:BJ">Benin</option>
                                                    	<option value="ISO 3166-2:BM">Bermuda</option>
                                                    	<option value="ISO 3166-2:BT">Bhutan</option>
                                                    	<option value="ISO 3166-2:BO">Bolivia, Plurinational State of</option>
                                                    	<option value="ISO 3166-2:BQ">Bonaire, Sint Eustatius and Saba</option>
                                                    	<option value="ISO 3166-2:BA">Bosnia and Herzegovina</option>
                                                    	<option value="ISO 3166-2:BW">Botswana</option>
                                                    	<option value="ISO 3166-2:BV">Bouvet Island</option>
                                                    	<option value="ISO 3166-2:BR">Brazil</option>
                                                    	<option value="ISO 3166-2:IO">British Indian Ocean Territory</option>
                                                    	<option value="ISO 3166-2:BN">Brunei Darussalam</option>
                                                    	<option value="ISO 3166-2:BG">Bulgaria</option>
                                                    	<option value="ISO 3166-2:BF">Burkina Faso</option>
                                                    	<option value="ISO 3166-2:BI">Burundi</option>
                                                    	<option value="ISO 3166-2:KH">Cambodia</option>
                                                    	<option value="ISO 3166-2:CM">Cameroon</option>
                                                    	<option value="ISO 3166-2:CA">Canada</option>
                                                    	<option value="ISO 3166-2:CV">Cape Verde</option>
                                                    	<option value="ISO 3166-2:KY">Cayman Islands</option>
                                                    	<option value="ISO 3166-2:CF">Central African Republic</option>
                                                    	<option value="ISO 3166-2:TD">Chad</option>
                                                    	<option value="ISO 3166-2:CL">Chile</option>
                                                    	<option value="ISO 3166-2:CN">China</option>
                                                    	<option value="ISO 3166-2:CX">Christmas Island</option>
                                                    	<option value="ISO 3166-2:CC">Cocos (Keeling) Islands</option>
                                                    	<option value="ISO 3166-2:CO">Colombia</option>
                                                    	<option value="ISO 3166-2:KM">Comoros</option>
                                                    	<option value="ISO 3166-2:CG">Congo</option>
                                                    	<option value="ISO 3166-2:CD">Congo, the Democratic Republic of the</option>
                                                    	<option value="ISO 3166-2:CK">Cook Islands</option>
                                                    	<option value="ISO 3166-2:CR">Costa Rica</option>
                                                    	<option value="ISO 3166-2:CI">Côte d'Ivoire</option>
                                                    	<option value="ISO 3166-2:HR">Croatia</option>
                                                    	<option value="ISO 3166-2:CU">Cuba</option>
                                                    	<option value="ISO 3166-2:CW">Curaçao</option>
                                                    	<option value="ISO 3166-2:CY">Cyprus</option>
                                                    	<option value="ISO 3166-2:CZ">Czech Republic</option>
                                                    	<option value="ISO 3166-2:DK">Denmark</option>
                                                    	<option value="ISO 3166-2:DJ">Djibouti</option>
                                                    	<option value="ISO 3166-2:DM">Dominica</option>
                                                    	<option value="ISO 3166-2:DO">Dominican Republic</option>
                                                    	<option value="ISO 3166-2:EC">Ecuador</option>
                                                    	<option value="ISO 3166-2:EG">Egypt</option>
                                                    	<option value="ISO 3166-2:SV">El Salvador</option>
                                                    	<option value="ISO 3166-2:GQ">Equatorial Guinea</option>
                                                    	<option value="ISO 3166-2:ER">Eritrea</option>
                                                    	<option value="ISO 3166-2:EE">Estonia</option>
                                                    	<option value="ISO 3166-2:ET">Ethiopia</option>
                                                    	<option value="ISO 3166-2:FK">Falkland Islands (Malvinas)</option>
                                                    	<option value="ISO 3166-2:FO">Faroe Islands</option>
                                                    	<option value="ISO 3166-2:FJ">Fiji</option>
                                                    	<option value="ISO 3166-2:FI">Finland</option>
                                                    	<option value="ISO 3166-2:FR">France</option>
                                                    	<option value="ISO 3166-2:GF">French Guiana</option>
                                                    	<option value="ISO 3166-2:PF">French Polynesia</option>
                                                    	<option value="ISO 3166-2:TF">French Southern Territories</option>
                                                    	<option value="ISO 3166-2:GA">Gabon</option>
                                                    	<option value="ISO 3166-2:GM">Gambia</option>
                                                    	<option value="ISO 3166-2:GE">Georgia</option>
                                                    	<option value="ISO 3166-2:DE">Germany</option>
                                                    	<option value="ISO 3166-2:GH">Ghana</option>
                                                    	<option value="ISO 3166-2:GI">Gibraltar</option>
                                                    	<option value="ISO 3166-2:GR">Greece</option>
                                                    	<option value="ISO 3166-2:GL">Greenland</option>
                                                    	<option value="ISO 3166-2:GD">Grenada</option>
                                                    	<option value="ISO 3166-2:GP">Guadeloupe</option>
                                                    	<option value="ISO 3166-2:GU">Guam</option>
                                                    	<option value="ISO 3166-2:GT">Guatemala</option>
                                                    	<option value="ISO 3166-2:GG">Guernsey</option>
                                                    	<option value="ISO 3166-2:GN">Guinea</option>
                                                    	<option value="ISO 3166-2:GW">Guinea-Bissau</option>
                                                    	<option value="ISO 3166-2:GY">Guyana</option>
                                                    	<option value="ISO 3166-2:HT">Haiti</option>
                                                    	<option value="ISO 3166-2:HM">Heard Island and McDonald Islands</option>
                                                    	<option value="ISO 3166-2:VA">Holy See (Vatican City State)</option>
                                                    	<option value="ISO 3166-2:HN">Honduras</option>
                                                    	<option value="ISO 3166-2:HK">Hong Kong</option>
                                                    	<option value="ISO 3166-2:HU">Hungary</option>
                                                    	<option value="ISO 3166-2:IS">Iceland</option>
                                                    	<option value="ISO 3166-2:IN">India</option>
                                                    	<option value="ISO 3166-2:ID">Indonesia</option>
                                                    	<option value="ISO 3166-2:IR">Iran, Islamic Republic of</option>
                                                    	<option value="ISO 3166-2:IQ">Iraq</option>
                                                    	<option value="ISO 3166-2:IE">Ireland</option>
                                                    	<option value="ISO 3166-2:IM">Isle of Man</option>
                                                    	<option value="ISO 3166-2:IL">Israel</option>
                                                    	<option value="ISO 3166-2:IT">Italy</option>
                                                    	<option value="ISO 3166-2:JM">Jamaica</option>
                                                    	<option value="ISO 3166-2:JP">Japan</option>
                                                    	<option value="ISO 3166-2:JE">Jersey</option>
                                                    	<option value="ISO 3166-2:JO">Jordan</option>
                                                    	<option value="ISO 3166-2:KZ">Kazakhstan</option>
                                                    	<option value="ISO 3166-2:KE">Kenya</option>
                                                    	<option value="ISO 3166-2:KI">Kiribati</option>
                                                    	<option value="ISO 3166-2:KP">Korea, Democratic People's Republic of</option>
                                                    	<option value="ISO 3166-2:KR">Korea, Republic of</option>
                                                    	<option value="ISO 3166-2:KW">Kuwait</option>
                                                    	<option value="ISO 3166-2:KG">Kyrgyzstan</option>
                                                    	<option value="ISO 3166-2:LA">Lao People's Democratic Republic</option>
                                                    	<option value="ISO 3166-2:LV">Latvia</option>
                                                    	<option value="ISO 3166-2:LB">Lebanon</option>
                                                    	<option value="ISO 3166-2:LS">Lesotho</option>
                                                    	<option value="ISO 3166-2:LR">Liberia</option>
                                                    	<option value="ISO 3166-2:LY">Libya</option>
                                                    	<option value="ISO 3166-2:LI">Liechtenstein</option>
                                                    	<option value="ISO 3166-2:LT">Lithuania</option>
                                                    	<option value="ISO 3166-2:LU">Luxembourg</option>
                                                    	<option value="ISO 3166-2:MO">Macao</option>
                                                    	<option value="ISO 3166-2:MK">Macedonia, the former Yugoslav Republic of</option>
                                                    	<option value="ISO 3166-2:MG">Madagascar</option>
                                                    	<option value="ISO 3166-2:MW">Malawi</option>
                                                    	<option value="ISO 3166-2:MY">Malaysia</option>
                                                    	<option value="ISO 3166-2:MV">Maldives</option>
                                                    	<option value="ISO 3166-2:ML">Mali</option>
                                                    	<option value="ISO 3166-2:MT">Malta</option>
                                                    	<option value="ISO 3166-2:MH">Marshall Islands</option>
                                                    	<option value="ISO 3166-2:MQ">Martinique</option>
                                                    	<option value="ISO 3166-2:MR">Mauritania</option>
                                                    	<option value="ISO 3166-2:MU">Mauritius</option>
                                                    	<option value="ISO 3166-2:YT">Mayotte</option>
                                                    	<option value="ISO 3166-2:MX">Mexico</option>
                                                    	<option value="ISO 3166-2:FM">Micronesia, Federated States of</option>
                                                    	<option value="ISO 3166-2:MD">Moldova, Republic of</option>
                                                    	<option value="ISO 3166-2:MC">Monaco</option>
                                                    	<option value="ISO 3166-2:MN">Mongolia</option>
                                                    	<option value="ISO 3166-2:ME">Montenegro</option>
                                                    	<option value="ISO 3166-2:MS">Montserrat</option>
                                                    	<option value="ISO 3166-2:MA">Morocco</option>
                                                    	<option value="ISO 3166-2:MZ">Mozambique</option>
                                                    	<option value="ISO 3166-2:MM">Myanmar</option>
                                                    	<option value="ISO 3166-2:NA">Namibia</option>
                                                    	<option value="ISO 3166-2:NR">Nauru</option>
                                                    	<option value="ISO 3166-2:NP">Nepal</option>
                                                    	<option value="ISO 3166-2:NL">Netherlands</option>
                                                    	<option value="ISO 3166-2:NC">New Caledonia</option>
                                                    	<option value="ISO 3166-2:NZ">New Zealand</option>
                                                    	<option value="ISO 3166-2:NI">Nicaragua</option>
                                                    	<option value="ISO 3166-2:NE">Niger</option>
                                                    	<option value="ISO 3166-2:NG">Nigeria</option>
                                                    	<option value="ISO 3166-2:NU">Niue</option>
                                                    	<option value="ISO 3166-2:NF">Norfolk Island</option>
                                                    	<option value="ISO 3166-2:MP">Northern Mariana Islands</option>
                                                    	<option value="ISO 3166-2:NO">Norway</option>
                                                    	<option value="ISO 3166-2:OM">Oman</option>
                                                    	<option value="ISO 3166-2:PK">Pakistan</option>
                                                    	<option value="ISO 3166-2:PW">Palau</option>
                                                    	<option value="ISO 3166-2:PS">Palestinian Territory, Occupied</option>
                                                    	<option value="ISO 3166-2:PA">Panama</option>
                                                    	<option value="ISO 3166-2:PG">Papua New Guinea</option>
                                                    	<option value="ISO 3166-2:PY">Paraguay</option>
                                                    	<option value="ISO 3166-2:PE">Peru</option>
                                                    	<option value="ISO 3166-2:PH">Philippines</option>
                                                    	<option value="ISO 3166-2:PN">Pitcairn</option>
                                                    	<option value="ISO 3166-2:PL">Poland</option>
                                                    	<option value="ISO 3166-2:PT">Portugal</option>
                                                    	<option value="ISO 3166-2:PR">Puerto Rico</option>
                                                    	<option value="ISO 3166-2:QA">Qatar</option>
                                                    	<option value="ISO 3166-2:RE">Réunion</option>
                                                    	<option value="ISO 3166-2:RO">Romania</option>
                                                    	<option value="ISO 3166-2:RU">Russian Federation</option>
                                                    	<option value="ISO 3166-2:RW">Rwanda</option>
                                                    	<option value="ISO 3166-2:BL">Saint Barthélemy</option>
                                                    	<option value="ISO 3166-2:SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                                    	<option value="ISO 3166-2:KN">Saint Kitts and Nevis</option>
                                                    	<option value="ISO 3166-2:LC">Saint Lucia</option>
                                                    	<option value="ISO 3166-2:MF">Saint Martin (French part)</option>
                                                    	<option value="ISO 3166-2:PM">Saint Pierre and Miquelon</option>
                                                    	<option value="ISO 3166-2:VC">Saint Vincent and the Grenadines</option>
                                                    	<option value="ISO 3166-2:WS">Samoa</option>
                                                    	<option value="ISO 3166-2:SM">San Marino</option>
                                                    	<option value="ISO 3166-2:ST">Sao Tome and Principe</option>
                                                    	<option value="ISO 3166-2:SA">Saudi Arabia</option>
                                                    	<option value="ISO 3166-2:SN">Senegal</option>
                                                    	<option value="ISO 3166-2:RS">Serbia</option>
                                                    	<option value="ISO 3166-2:SC">Seychelles</option>
                                                    	<option value="ISO 3166-2:SL">Sierra Leone</option>
                                                    	<option value="ISO 3166-2:SG">Singapore</option>
                                                    	<option value="ISO 3166-2:SX">Sint Maarten (Dutch part)</option>
                                                    	<option value="ISO 3166-2:SK">Slovakia</option>
                                                    	<option value="ISO 3166-2:SI">Slovenia</option>
                                                    	<option value="ISO 3166-2:SB">Solomon Islands</option>
                                                    	<option value="ISO 3166-2:SO">Somalia</option>
                                                    	<option value="ISO 3166-2:ZA">South Africa</option>
                                                    	<option value="ISO 3166-2:GS">South Georgia and the South Sandwich Islands</option>
                                                    	<option value="ISO 3166-2:SS">South Sudan</option>
                                                    	<option value="ISO 3166-2:ES">Spain</option>
                                                    	<option value="ISO 3166-2:LK">Sri Lanka</option>
                                                    	<option value="ISO 3166-2:SD">Sudan</option>
                                                    	<option value="ISO 3166-2:SR">Suriname</option>
                                                    	<option value="ISO 3166-2:SJ">Svalbard and Jan Mayen</option>
                                                    	<option value="ISO 3166-2:SZ">Swaziland</option>
                                                    	<option value="ISO 3166-2:SE">Sweden</option>
                                                    	<option value="ISO 3166-2:CH">Switzerland</option>
                                                    	<option value="ISO 3166-2:SY">Syrian Arab Republic</option>
                                                    	<option value="ISO 3166-2:TW">Taiwan, Province of China</option>
                                                    	<option value="ISO 3166-2:TJ">Tajikistan</option>
                                                    	<option value="ISO 3166-2:TZ">Tanzania, United Republic of</option>
                                                    	<option value="ISO 3166-2:TH">Thailand</option>
                                                    	<option value="ISO 3166-2:TL">Timor-Leste</option>
                                                    	<option value="ISO 3166-2:TG">Togo</option>
                                                    	<option value="ISO 3166-2:TK">Tokelau</option>
                                                    	<option value="ISO 3166-2:TO">Tonga</option>
                                                    	<option value="ISO 3166-2:TT">Trinidad and Tobago</option>
                                                    	<option value="ISO 3166-2:TN">Tunisia</option>
                                                    	<option value="ISO 3166-2:TR">Turkey</option>
                                                    	<option value="ISO 3166-2:TM">Turkmenistan</option>
                                                    	<option value="ISO 3166-2:TC">Turks and Caicos Islands</option>
                                                    	<option value="ISO 3166-2:TV">Tuvalu</option>
                                                    	<option value="ISO 3166-2:UG">Uganda</option>
                                                    	<option value="ISO 3166-2:UA">Ukraine</option>
                                                    	<option value="ISO 3166-2:AE">United Arab Emirates</option>
                                                    	<option value="ISO 3166-2:GB">United Kingdom</option>
                                                    	<option value="ISO 3166-2:US" selected>United States</option>
                                                    	<option value="ISO 3166-2:UM">United States Minor Outlying Islands</option>
                                                    	<option value="ISO 3166-2:UY">Uruguay</option>
                                                    	<option value="ISO 3166-2:UZ">Uzbekistan</option>
                                                    	<option value="ISO 3166-2:VU">Vanuatu</option>
                                                    	<option value="ISO 3166-2:VE">Venezuela, Bolivarian Republic of</option>
                                                    	<option value="ISO 3166-2:VN">Viet Nam</option>
                                                    	<option value="ISO 3166-2:VG">Virgin Islands, British</option>
                                                    	<option value="ISO 3166-2:VI">Virgin Islands, U.S.</option>
                                                    	<option value="ISO 3166-2:WF">Wallis and Futuna</option>
                                                    	<option value="ISO 3166-2:EH">Western Sahara</option>
                                                    	<option value="ISO 3166-2:YE">Yemen</option>
                                                    	<option value="ISO 3166-2:ZM">Zambia</option>
                                                    	<option value="ISO 3166-2:ZW">Zimbabwe</option>
                                                    </select>
                                                  </div>

                                                  <div class="card-text">
                                                      <p>Phone &amp; Email</p>
                                                  </div>

                                                  <div class="form-group">
                                                      <label for="eventRegInput5">Contact Number</label>
                                                      <div class="position-relative has-icon-left">
                                                        <input type="tel" id="eventRegInput5" class="form-control" name="contact" placeholder="contact number" value="608-123-0123" required>
                                                        <div class="form-control-position">
                                                            <i class="ft-phone"></i>
                                                        </div>
                                                      </div>
                                                  </div>

                                                  <div class="form-group">
                                                      <label for="eventRegInput5">Contact Email</label>
                                                      <div class="position-relative has-icon-left">
                                                        <input type="email" id="eventRegInput6" class="form-control" name="email" placeholder="contact email" value="jennifer@example.com" required>
                                                          <div class="form-control-position">
                                                              <i class="ft-message-square"></i>
                                                          </div>
                                                      </div>
                                                  </div>

                                                  <div class="card-text">
                                                      <p>Demographic &amp; Security Information</p>
                                                  </div>

                                                  <div class="form-group">
                                                    <label for="both">Sex</label><br />
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                        <label class="form-check-label" for="inlineRadio1">Male</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" checked>
                                                        <label class="form-check-label" for="inlineRadio2">Female</label>
                                                    </div>
                                                  </div>

                                                  <div class="form-group">
                                                      <label for="eventRegInput4">Nationality</label>
                                                      <select id="projectinput9" name="nationality" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Nationality" data-original-title="" title="">
                                                        <option value="afghan">Afghan</option>
                                                        <option value="albanian">Albanian</option>
                                                        <option value="algerian">Algerian</option>
                                                        <option value="american" selected>American</option>
                                                        <option value="andorran">Andorran</option>
                                                        <option value="angolan">Angolan</option>
                                                        <option value="antiguans">Antiguans</option>
                                                        <option value="argentinean">Argentinean</option>
                                                        <option value="armenian">Armenian</option>
                                                        <option value="australian">Australian</option>
                                                        <option value="austrian">Austrian</option>
                                                        <option value="azerbaijani">Azerbaijani</option>
                                                        <option value="bahamian">Bahamian</option>
                                                        <option value="bahraini">Bahraini</option>
                                                        <option value="bangladeshi">Bangladeshi</option>
                                                        <option value="barbadian">Barbadian</option>
                                                        <option value="barbudans">Barbudans</option>
                                                        <option value="batswana">Batswana</option>
                                                        <option value="belarusian">Belarusian</option>
                                                        <option value="belgian">Belgian</option>
                                                        <option value="belizean">Belizean</option>
                                                        <option value="beninese">Beninese</option>
                                                        <option value="bhutanese">Bhutanese</option>
                                                        <option value="bolivian">Bolivian</option>
                                                        <option value="bosnian">Bosnian</option>
                                                        <option value="brazilian">Brazilian</option>
                                                        <option value="british">British</option>
                                                        <option value="bruneian">Bruneian</option>
                                                        <option value="bulgarian">Bulgarian</option>
                                                        <option value="burkinabe">Burkinabe</option>
                                                        <option value="burmese">Burmese</option>
                                                        <option value="burundian">Burundian</option>
                                                        <option value="cambodian">Cambodian</option>
                                                        <option value="cameroonian">Cameroonian</option>
                                                        <option value="canadian">Canadian</option>
                                                        <option value="cape verdean">Cape Verdean</option>
                                                        <option value="central african">Central African</option>
                                                        <option value="chadian">Chadian</option>
                                                        <option value="chilean">Chilean</option>
                                                        <option value="chinese">Chinese</option>
                                                        <option value="colombian">Colombian</option>
                                                        <option value="comoran">Comoran</option>
                                                        <option value="congolese">Congolese</option>
                                                        <option value="costa rican">Costa Rican</option>
                                                        <option value="croatian">Croatian</option>
                                                        <option value="cuban">Cuban</option>
                                                        <option value="cypriot">Cypriot</option>
                                                        <option value="czech">Czech</option>
                                                        <option value="danish">Danish</option>
                                                        <option value="djibouti">Djibouti</option>
                                                        <option value="dominican">Dominican</option>
                                                        <option value="dutch">Dutch</option>
                                                        <option value="east timorese">East Timorese</option>
                                                        <option value="ecuadorean">Ecuadorean</option>
                                                        <option value="egyptian">Egyptian</option>
                                                        <option value="emirian">Emirian</option>
                                                        <option value="equatorial guinean">Equatorial Guinean</option>
                                                        <option value="eritrean">Eritrean</option>
                                                        <option value="estonian">Estonian</option>
                                                        <option value="ethiopian">Ethiopian</option>
                                                        <option value="fijian">Fijian</option>
                                                        <option value="filipino">Filipino</option>
                                                        <option value="finnish">Finnish</option>
                                                        <option value="french">French</option>
                                                        <option value="gabonese">Gabonese</option>
                                                        <option value="gambian">Gambian</option>
                                                        <option value="georgian">Georgian</option>
                                                        <option value="german">German</option>
                                                        <option value="ghanaian">Ghanaian</option>
                                                        <option value="greek">Greek</option>
                                                        <option value="grenadian">Grenadian</option>
                                                        <option value="guatemalan">Guatemalan</option>
                                                        <option value="guinea-bissauan">Guinea-Bissauan</option>
                                                        <option value="guinean">Guinean</option>
                                                        <option value="guyanese">Guyanese</option>
                                                        <option value="haitian">Haitian</option>
                                                        <option value="herzegovinian">Herzegovinian</option>
                                                        <option value="honduran">Honduran</option>
                                                        <option value="hungarian">Hungarian</option>
                                                        <option value="icelander">Icelander</option>
                                                        <option value="indian">Indian</option>
                                                        <option value="indonesian">Indonesian</option>
                                                        <option value="iranian">Iranian</option>
                                                        <option value="iraqi">Iraqi</option>
                                                        <option value="irish">Irish</option>
                                                        <option value="israeli">Israeli</option>
                                                        <option value="italian">Italian</option>
                                                        <option value="ivorian">Ivorian</option>
                                                        <option value="jamaican">Jamaican</option>
                                                        <option value="japanese">Japanese</option>
                                                        <option value="jordanian">Jordanian</option>
                                                        <option value="kazakhstani">Kazakhstani</option>
                                                        <option value="kenyan">Kenyan</option>
                                                        <option value="kittian and nevisian">Kittian and Nevisian</option>
                                                        <option value="kuwaiti">Kuwaiti</option>
                                                        <option value="kyrgyz">Kyrgyz</option>
                                                        <option value="laotian">Laotian</option>
                                                        <option value="latvian">Latvian</option>
                                                        <option value="lebanese">Lebanese</option>
                                                        <option value="liberian">Liberian</option>
                                                        <option value="libyan">Libyan</option>
                                                        <option value="liechtensteiner">Liechtensteiner</option>
                                                        <option value="lithuanian">Lithuanian</option>
                                                        <option value="luxembourger">Luxembourger</option>
                                                        <option value="macedonian">Macedonian</option>
                                                        <option value="malagasy">Malagasy</option>
                                                        <option value="malawian">Malawian</option>
                                                        <option value="malaysian">Malaysian</option>
                                                        <option value="maldivan">Maldivan</option>
                                                        <option value="malian">Malian</option>
                                                        <option value="maltese">Maltese</option>
                                                        <option value="marshallese">Marshallese</option>
                                                        <option value="mauritanian">Mauritanian</option>
                                                        <option value="mauritian">Mauritian</option>
                                                        <option value="mexican">Mexican</option>
                                                        <option value="micronesian">Micronesian</option>
                                                        <option value="moldovan">Moldovan</option>
                                                        <option value="monacan">Monacan</option>
                                                        <option value="mongolian">Mongolian</option>
                                                        <option value="moroccan">Moroccan</option>
                                                        <option value="mosotho">Mosotho</option>
                                                        <option value="motswana">Motswana</option>
                                                        <option value="mozambican">Mozambican</option>
                                                        <option value="namibian">Namibian</option>
                                                        <option value="nauruan">Nauruan</option>
                                                        <option value="nepalese">Nepalese</option>
                                                        <option value="new zealander">New Zealander</option>
                                                        <option value="ni-vanuatu">Ni-Vanuatu</option>
                                                        <option value="nicaraguan">Nicaraguan</option>
                                                        <option value="nigerien">Nigerien</option>
                                                        <option value="north korean">North Korean</option>
                                                        <option value="northern irish">Northern Irish</option>
                                                        <option value="norwegian">Norwegian</option>
                                                        <option value="omani">Omani</option>
                                                        <option value="pakistani">Pakistani</option>
                                                        <option value="palauan">Palauan</option>
                                                        <option value="panamanian">Panamanian</option>
                                                        <option value="papua new guinean">Papua New Guinean</option>
                                                        <option value="paraguayan">Paraguayan</option>
                                                        <option value="peruvian">Peruvian</option>
                                                        <option value="polish">Polish</option>
                                                        <option value="portuguese">Portuguese</option>
                                                        <option value="qatari">Qatari</option>
                                                        <option value="romanian">Romanian</option>
                                                        <option value="russian">Russian</option>
                                                        <option value="rwandan">Rwandan</option>
                                                        <option value="saint lucian">Saint Lucian</option>
                                                        <option value="salvadoran">Salvadoran</option>
                                                        <option value="samoan">Samoan</option>
                                                        <option value="san marinese">San Marinese</option>
                                                        <option value="sao tomean">Sao Tomean</option>
                                                        <option value="saudi">Saudi</option>
                                                        <option value="scottish">Scottish</option>
                                                        <option value="senegalese">Senegalese</option>
                                                        <option value="serbian">Serbian</option>
                                                        <option value="seychellois">Seychellois</option>
                                                        <option value="sierra leonean">Sierra Leonean</option>
                                                        <option value="singaporean">Singaporean</option>
                                                        <option value="slovakian">Slovakian</option>
                                                        <option value="slovenian">Slovenian</option>
                                                        <option value="solomon islander">Solomon Islander</option>
                                                        <option value="somali">Somali</option>
                                                        <option value="south african">South African</option>
                                                        <option value="south korean">South Korean</option>
                                                        <option value="spanish">Spanish</option>
                                                        <option value="sri lankan">Sri Lankan</option>
                                                        <option value="sudanese">Sudanese</option>
                                                        <option value="surinamer">Surinamer</option>
                                                        <option value="swazi">Swazi</option>
                                                        <option value="swedish">Swedish</option>
                                                        <option value="swiss">Swiss</option>
                                                        <option value="syrian">Syrian</option>
                                                        <option value="taiwanese">Taiwanese</option>
                                                        <option value="tajik">Tajik</option>
                                                        <option value="tanzanian">Tanzanian</option>
                                                        <option value="thai">Thai</option>
                                                        <option value="togolese">Togolese</option>
                                                        <option value="tongan">Tongan</option>
                                                        <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                                                        <option value="tunisian">Tunisian</option>
                                                        <option value="turkish">Turkish</option>
                                                        <option value="tuvaluan">Tuvaluan</option>
                                                        <option value="ugandan">Ugandan</option>
                                                        <option value="ukrainian">Ukrainian</option>
                                                        <option value="uruguayan">Uruguayan</option>
                                                        <option value="uzbekistani">Uzbekistani</option>
                                                        <option value="venezuelan">Venezuelan</option>
                                                        <option value="vietnamese">Vietnamese</option>
                                                        <option value="welsh">Welsh</option>
                                                        <option value="yemenite">Yemenite</option>
                                                        <option value="zambian">Zambian</option>
                                                        <option value="zimbabwean">Zimbabwean</option>
                                                      </select>
                                                    </div>

                                                  <div class="form-group">
                                                      <label for="timesheetinput3">Date of Birth</label>
                                                      <div class="position-relative has-icon-left">
                                                          <input type="date" id="timesheetinput3" class="form-control" name="date" value="03/14/1976" required>
                                                          <div class="form-control-position">
                                                              <i class="ft-message-square"></i>
                                                          </div>
                                                      </div>
                                                  </div>

                                                  <div class="card-text">
                                                      <p>Vehicle &amp; Parking Information</p>
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="eventRegInput5">Car Registation #</label>
                                                      <input type="text" id="eventRegInput09" class="form-control" name="car" placeholder="car" value="777-LLL">
                                                  </div>

                                                  <div class="card-text">
                                                      <p>Terms &amp; Policies of the Provider</p>
                                                  </div>
                                                  <div class="form-group">

                                                    <div class="custom-control custom-checkbox">
                                                      <input type="checkbox" class="custom-control-input bg-primary" id="customCheck4">
                                                      <label class="custom-control-label" for="customCheck4">I accept the provider's policies</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                      <input type="checkbox" class="custom-control-input bg-primary" id="customCheck6" checked>
                                                      <label class="custom-control-label" for="customCheck6">I realise the room is non-smoking</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                      <input type="checkbox" class="custom-control-input bg-primary" id="customCheck5">
                                                      <label class="custom-control-label" for="customCheck5">You can debit my credit card</label>
                                                    </div>

                                                  </div>

                                                  <div class="card-text">
                                                      <p>Special and Extra Details (e.g. disability, diet etc)</p>
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="donationinput7">Additional/Extra Requirements</label>
                                                      <textarea id="donationinput7" rows="5" class="form-control square" name="comments" placeholder="comments"></textarea>
                                                  </div>



                                              </div>



                                            </div>
                                          </form>
                                        </div>

                                      </div> <!-- end row -->

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingGTwo">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC2" aria-expanded="false" aria-controls="accordionC2">
                                            <i class="ft-map-pin"></i> Business
                                        </button>
                                    </h5>
                                </div>
                                <div id="accordionC2" class="collapse" aria-labelledby="headingGTwo" data-parent="#accordion3">
                                    <div class="card-body">

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingGThree">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#accordionC3" aria-expanded="false" aria-controls="accordionC3">
                                            <i class="ft-map-pin"></i> Family
                                        </button>
                                    </h5>
                                </div>
                                <div id="accordionC3" class="collapse" aria-labelledby="headingGThree" data-parent="#accordion3">
                                    <div class="card-body">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                  </div> <!-- end card body -->
              </div>
          </div>
      </div>
      <div class="col-lg-4 col-md-12">
        <!-- EMPTY COL -->
      </div>
  </div>
@stop
