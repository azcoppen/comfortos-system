@extends ('demo.index')
@section ('headline') Phone @stop

@section ('content')
  <div class="row">
      <div class="col-lg-5 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">In-Room Mobile Guest SoftPhone</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                  <div class="card-body pb-2">
                    <div class="card-text">
                        <p>Dial <strong>9</strong> for an outside line. Outside callers should dial <strong>+1 555-555-0162</strong>. Extensions work wherever you are, as long as you have an internet connection.</p>
                    </div>
                    <div align="center">
                      <img src="/demo/dialpad.png" width="60%" align="center" />
                    </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Voicemail</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

            </div>
            <div class="card-content collapse show">

                <div class="card-body p-0 pb-0">

                  <div class="card-text p-1">
                      <p>Dial <strong>1</strong> to access your voice mailbox on your handset.</p>
                  </div>

                  <div id="recent-buyers" class="media-list">
                    <a href="#" class="media border-0">
                        <div class="media-left pr-1">
                            <span class="avatar avatar-md avatar-online">
                                <img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-7.png" alt="Generic placeholder image">
                                <i></i>
                            </span>
                        </div>
                        <div class="media-body w-100">
                            <span class="list-group-item-heading">Concierge <span class="blue-grey lighten-2 font-small-3"> (0.25) </span>

                            </span>
                            <ul class="list-unstyled users-list m-0 float-right">
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 1" class="avatar avatar-sm pull-up pr-3">
                                <h3><i class="ft-mail"></i></h3>
                              </li>
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 3" class="avatar avatar-sm pull-up">

                              </li>

                            </ul>

                            <p class="list-group-item-text mb-0">
                                <span class="blue-grey lighten-2 font-small-3"> #105 </span>
                            </p>

                        </div>
                    </a>

                    <a href="#" class="media border-0">
                        <div class="media-left pr-1">
                            <span class="avatar avatar-md avatar-away">
                                <img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-8.png" alt="Generic placeholder image">
                                <i></i>
                            </span>
                        </div>
                        <div class="media-body w-100">
                            <span class="list-group-item-heading">L. Fowler <span class="blue-grey lighten-2 font-small-3"> (1.37) </span>

                            </span>
                            <ul class="list-unstyled users-list m-0 float-right">
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 1" class="avatar avatar-sm pull-up pr-3">
                                <h3><i class="ft-mail"></i></h3>
                              </li>
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 3" class="avatar avatar-sm pull-up">
                              </li>

                            </ul>

                            <p class="list-group-item-text mb-0">
                                <span class="blue-grey lighten-2 font-small-3"> #103 </span>

                            </p>
                        </div>
                    </a>
                    <a href="#" class="media border-0">
                        <div class="media-left pr-1">
                            <span class="avatar avatar-md avatar-busy">
                                <img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-9.png" alt="Generic placeholder image">
                                <i></i>
                            </span>
                        </div>
                        <div class="media-body w-100">
                            <span class="list-group-item-heading">Linda Olson <span class="blue-grey lighten-2 font-small-3"> (0.46) </span>

                            </span>
                            <ul class="list-unstyled users-list m-0 float-right">
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 1" class="avatar avatar-sm pull-up pr-3">
                                <h3><i class="ft-mail"></i></h3>
                              </li>
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 3" class="avatar avatar-sm pull-up">
                              </li>

                            </ul>

                            <p class="list-group-item-text mb-0">
                                <span class="blue-grey lighten-2 font-small-3"> #230 </span>

                            </p>
                        </div>
                    </a>
                    <a href="#" class="media border-0">
                        <div class="media-left pr-1">
                            <span class="avatar avatar-md avatar-online">
                                <img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-10.png" alt="Generic placeholder image">
                                <i></i>
                            </span>
                        </div>
                        <div class="media-body w-100">
                            <span class="list-group-item-heading">Front Desk <span class="blue-grey lighten-2 font-small-3"> (0.34) </span>

                            </span>
                            <ul class="list-unstyled users-list m-0 float-right">
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 1" class="avatar avatar-sm pull-up pr-3">
                                <h3><i class="ft-mail"></i></h3>
                              </li>
                              <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 3" class="avatar avatar-sm pull-up">
                              </li>

                            </ul>

                            <p class="list-group-item-text mb-0">
                                <span class="blue-grey lighten-2 font-small-3"> #I00 </span>

                            </p>
                        </div>
                    </a>
                    <a href="#" class="media border-0">
                        <div class="media-left pr-1">
                            <span class="avatar avatar-md avatar-online">
                                <img class="media-object rounded-circle" src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-11.png" alt="Generic placeholder image">
                                <i></i>
                            </span>
                        </div>
                        <div class="media-body w-100">
                            <span class="list-group-item-heading">Parking/Valet <span class="blue-grey lighten-2 font-small-3"> (0.24) </span>

                            </span>
                            <ul class="list-unstyled users-list m-0 float-right">
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 1" class="avatar avatar-sm pull-up pr-3">
                                  <h3><i class="ft-mail"></i></h3>
                                </li>
                                <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Product 3" class="avatar avatar-sm pull-up">
                                </li>
                            </ul>

                            <p class="list-group-item-text mb-0">
                                <span class="blue-grey lighten-2 font-small-3"> #112 </span>

                            </p>
                        </div>
                    </a>
                </div>
                </div>
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Address Book</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

            </div>
            <div class="card-content collapse show">
                <div class="card-body pt-0">

                  <div class="card-text p-1">
                      <p>You can personalise your contact list and store your favourites, co-guests, and family members.</p>
                  </div>

                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary float-right">#100</span>
                        <span class="blue-grey lighten-2 font-small-3"> <i class="ft-phone"></i> </span>&nbsp; Front Desk
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-info float-right">#112</span>
                        <span class="blue-grey lighten-2 font-small-3"> <i class="ft-phone"></i> </span>&nbsp; Parking/Valet
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-warning float-right">#105</span>
                        <span class="blue-grey lighten-2 font-small-3"> <i class="ft-phone"></i> </span>&nbsp; Concierge
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-success float-right">#107</span>
                        <span class="blue-grey lighten-2 font-small-3"> <i class="ft-phone"></i> </span>&nbsp; Kitchen/Dining
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-danger float-right">#110</span>
                        <span class="blue-grey lighten-2 font-small-3"> <i class="ft-phone"></i> </span>&nbsp; Housekeeping
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-phone"></i></span>
                        <span class="primary font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #150
                      </a>
                    </li>
                    <li class="list-group-item danger">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right danger"><i class="ft-phone-call danger"></i></span>
                        <span class="danger font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #151 (Engaged)
                      </a>
                    </li>
                    <li class="list-group-item warning">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-mail warning"></i></span>
                        <span class="warning font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #152 (Voicemail)
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-phone"></i></span>
                        <span class="primary font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #153
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-phone"></i></span>
                        <span class="primary font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #154
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-phone"></i></span>
                        <span class="primary font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #155
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-phone"></i></span>
                        <span class="primary font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #156
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-phone"></i></span>
                        <span class="primary font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #157
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-phone"></i></span>
                        <span class="primary font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #158
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-phone"></i></span>
                        <span class="primary font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #159
                      </a>
                    </li>
                    <li class="list-group-item">
                      <a>
                        <span class="badge badge-pill bg-primary bg-accent-1 float-right primary"><i class="ft-phone"></i></span>
                        <span class="primary font-small-3"> <i class="ft-user"></i> </span>&nbsp; Room #150
                      </a>
                    </li>
                </ul>


                </div> <!-- end card body -->
            </div>
        </div>
      </div>
  </div>
@stop
