@extends ('demo.index')
@section ('headline') Lighting @stop
@section ('content')
  <div class="row">
      <div class="col-lg-4 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Bulb 1</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                  <div class="card-body p-0 pb-0">

                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Bulb 2</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

            </div>
            <div class="card-content collapse show">
                <div class="card-body p-0 pb-0">

                </div>
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Bulb 3</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

            </div>
            <div class="card-content collapse show">
                <div class="card-body p-0 pb-0">

                </div>
            </div>
        </div>
      </div>
  </div>
@stop
