@extends ('demo.index')
@section ('headline') Android TV @stop
@section ('content')
  <div class="row">
      <div class="col-lg-6 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">TV Remote Control</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                  <div class="card-body p-0 pb-0">
                    <div align="center">
                      <img src="/demo/tv_remote.png" width="80%" align="center" />
                    </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Netflix Login</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                  <div class="card-body pt-1">

                    <div align="center">
                      <img src="/demo/netflix.jpg" width="30%" align="center" />
                    </div>

                    <div class="card-text mt-1">
                        <p>You can be automatically logged into your own Netflix account on the room Android TV STB.</p>
                    </div>

                    <form class="form">
                      <div class="form-body">
                        <div class="form-group">
                            <label for="ssid-custom">Your Netflix username (email)</label>
                            <div class="position-relative has-icon-left">
                                <input type="email" class="form-control input-xl" name="netflix-email" value = "me@example.com" placeholder="me@example.com" required>
                                <div class="form-control-position">
                                    <i class="la la-envelope"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ssid-custom">Your Netflix password</label>
                            <div class="position-relative has-icon-left">
                                <input type="password" class="form-control input-xl" name="netflix-pwd" placeholder="12345" required>
                                <div class="form-control-position">
                                    <i class="la la-lock"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center">
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-check-square-o"></i> Save To Device
                            </button>
                        </div>
                      </div>
                    </form>


                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-3 col-md-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Spotify Login</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

              </div>
              <div class="card-content collapse show">
                  <div class="card-body pt-1">

                    <div align="center">
                      <img src="/demo/spotify.png" width="30%" align="center" />
                    </div>

                    <div class="card-text mt-1">
                        <p>You can be automatically logged into your own Spotify account on the room Android TV STB and audio speakers.</p>
                    </div>

                    <form class="form">
                      <div class="form-body">
                        <div class="form-group">
                            <label for="ssid-custom">Your Spotify username</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" class="form-control input-xl" name="spotify-user" value = "myusername" placeholder="myusername" required>
                                <div class="form-control-position">
                                    <i class="la la-user"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ssid-custom">Your Spotify password</label>
                            <div class="position-relative has-icon-left">
                                <input type="password" class="form-control input-xl" name="spotify-pwd" placeholder="12345" required>
                                <div class="form-control-position">
                                    <i class="la la-lock"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center">
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-check-square-o"></i> Save To Device
                            </button>
                        </div>
                      </div>
                    </form>

                  </div>
              </div>
          </div>
      </div>
  </div>
@stop
