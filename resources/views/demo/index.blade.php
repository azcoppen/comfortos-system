<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="AC">
    <title>SmartRooms Demo</title>
    <link rel="apple-touch-icon" href="https://smartrooms.b-cdn.net/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://smartrooms.b-cdn.net/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/core/colors/palette-gradient.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="https://smartrooms.b-cdn.net/app-assets/css/pages/chat-application.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu 2-columns  " data-open="hover" data-menu="horizontal-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    <!-- BEGIN: Header-->
    <!-- fixed-top-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-static-top navbar-light navbar-brand-center">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item"><a class="navbar-brand" href="{{ route ('demo.index') }}"><img class="brand-logo" alt="creaative admin logo" src="https://smartrooms.b-cdn.net/app-assets/images/logo/logo.png">
                        <h3 class="brand-text">Lab Demo</h3>
                    </a></li>
                <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
            </ul>
        </div>
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>

                    </ul>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-us"></i><span class="selected-language"></span></a>
                            <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                                <div class="arrow_box"><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-us"></i> English</a></div>
                            </div>
                        </li>


                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"> <span class="avatar avatar-online"><img src="https://smartrooms.b-cdn.net/app-assets/images/portrait/small/avatar-s-19.png" alt="avatar"></span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="arrow_box_right"><a class="dropdown-item" href="#"><span class="user-name text-bold-700 ml-1">{{ auth()->user()->first }} {{ auth()->user()->last }}</span></span></a>

                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="ft-power"></i> Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">

                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'checkin' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'checkin') }}"><i class="ft-map-pin"></i><span>Checkin</span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'messages' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'messages') }}"><i class="ft-mail"></i><span>Messages (<strong>2</strong>) </span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'lock' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'lock') }}"><i class="ft-lock"></i><span>Door Lock</span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'phone' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'phone') }}"><i class="ft-phone-call"></i><span>Phone</span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'wifi' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'wifi') }}"><i class="ft-wifi"></i><span>Wifi</span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'tv' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'tv') }}"><i class="ft-airplay"></i><span>TV</span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'thermostat' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'thermostat') }}"><i class="ft-thermometer"></i><span>Thermostat</span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'lighting' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'lighting') }}"><i class="ft-sun"></i><span>Lighting</span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'speaker' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'speaker') }}"><i class="ft-volume-2"></i><span>Speaker</span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'camera' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'camera') }}"><i class="ft-camera"></i><span>Camera</span></a></li>
                <li class="nav-item {{ isset ($nav_active) && $nav_active == 'sensors' ? 'active' : '' }}"><a class="nav-link" href="{{ route ('demo.index', 'sensors') }}"><i class="ft-radio"></i><span>Sensors</span></a></li>


            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
              <div class="content-header-left col-md-4 col-12 mb-2">
                  <h3 class="content-header-title">@yield ('headline')</h3>
              </div>
              <div class="content-header-right col-md-8 col-12">
              </div>
            </div>
            <div class="content-body">

              @yield ('content')

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-shadow">
        <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">2019 &copy; Copyright <a class="text-bold-800 grey darken-2" href="https://atmosphere.us" target="_blank">Atmosphere</a></span>
        </div>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="https://smartrooms.b-cdn.net/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script type="text/javascript" src="https://smartrooms.b-cdn.net/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="https://smartrooms.b-cdn.net/app-assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
    <script src="https://smartrooms.b-cdn.net/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="https://smartrooms.b-cdn.net/app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="https://smartrooms.b-cdn.net/app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="https://smartrooms.b-cdn.net/app-assets/js/scripts/pages/dashboard-analytics.js" type="text/javascript"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
