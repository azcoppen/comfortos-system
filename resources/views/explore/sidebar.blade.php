
<ul class="nav">
    <li class="nav-header">Operators</li>

    @if ( isset($menu_operators) && count ($menu_operators) )
        @foreach ($menu_operators->sortBy('label') AS $operator)
            <li class="nav-item">
        				<a class="nav-link" href="{{ route ('explorer.operators.brands.index', $operator->slug) }}">
        					  <i data-feather="codepen" class="menu-icon"></i>
        						<span class="menu-title">{{ $operator->label }}</span>
        				</a>
        	</li>
        @endforeach
    @endif

    @if ( isset($menu_operator) )
            <li class="nav-item active">
        				<a class="nav-link" href="{{ route ('explorer.operators.brands.index', $menu_operator->slug) }}">
        					  <i data-feather="codepen" class="menu-icon"></i>
        						<span class="menu-title">{{ $menu_operator->label }}</span>
        				</a>
        	</li>
    @endif



    @if ( isset($menu_brands) && count ($menu_brands) )
    <li class="nav-header">Brands</li>
        @foreach ($menu_brands->sortBy('label') AS $brand)
            <li class="nav-item">
        				<a class="nav-link" href="{{ route ('explorer.operators.brands.properties.index', [$menu_operator->slug, $brand->slug]) }}">
        					  <i data-feather="box" class="menu-icon"></i>
        						<span class="menu-title">{{ $brand->label }}</span>
        				</a>
        	</li>
        @endforeach
    @endif

    @if ( isset($menu_brand) )
    <li class="nav-header">Brands</li>
            <li class="nav-item active">
        				<a class="nav-link" href="{{ route ('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">
        					  <i data-feather="box" class="menu-icon"></i>
        						<span class="menu-title">{{ $menu_brand->label }}</span>
        				</a>
        	</li>
    @endif

    @if ( isset($menu_properties) && count ($menu_properties) )
    <li class="nav-header">Properties</li>
        @foreach ($menu_properties->sortBy('label') AS $property)
            <li class="nav-item">
        				<a class="nav-link" href="{{ route ('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $property->slug]) }}">
        					  <i data-feather="map-pin" class="menu-icon"></i>
        						<span class="menu-title">{{ $property->label }}</span>
        				</a>
        	</li>
        @endforeach
    @endif

    @if ( isset($menu_property) )
    <li class="nav-header">Properties</li>
    <li class="nav-item active">
                <a class="nav-link" data-toggle="collapse" href="#{{ $menu_property->slug }}" aria-expanded="true" aria-controls="{{ $menu_property->slug }}">
                        <i data-feather="map-pin" class="menu-icon"></i>
                        <span class="menu-title">{{ $menu_property->label }}</span>
                      <i data-feather="chevron-right" class="menu-arrow"></i>
                </a>
                <div class="collapse show" id="{{ $menu_property->slug }}" style="">
                        <ul class="nav flex-column sub-menu" style="background-color: #FFF !important;">
                            @if ( isset($menu_building) )
                                <li class="nav-item">
                                        <a class="nav-link" data-toggle="collapse" href="#{{ $menu_building->slug }}" aria-expanded="true" aria-controls="{{ $menu_building->slug }}">
                                                <span class="menu-title">{{ $menu_building->label }}</span>
                                                <i data-feather="chevron-right" class="menu-arrow"></i>
                                        </a>
                                        <div class="collapse show" id="{{ $menu_building->slug }}" style="">
                                                <ul class="nav flex-column sub-menu">
                                                    @foreach ($menu_building->rooms->pluck('floor')->unique()->sort() AS $floor)
                                                      @php ($rooms_on_floor = $menu_building->rooms->where('floor', $floor)->count()) @endphp
                                                        <li class="nav-item {{ (isset($menu_room)  && ($floor == $menu_room->floor)) || (isset($menu_floor) && $menu_floor == $floor) ? 'active' : '' }}">

                                                            <a style="{{ isset($menu_room)  && ($menu_room->floor == $floor) ? 'background-color: rgba(22, 39, 211, 0.1) !important;' : '' }}" class="nav-link" href="{{ route ('explorer.operators.brands.properties.buildings.floor', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $floor ?? 0]) }}">
                                                            <span class="menu-title">Floor {{ $floor }}</span>
                                                            @if ( $rooms_on_floor > 0 )
                                                            <span class="badge badge-pill badge-outline-info mt-2 mr-2">{{ $rooms_on_floor }}</span>
                                                            @endif

                                                        </a></li>
                                                    @endforeach
                                                </ul>
                                        </div>
                                </li>

                            @else
                                @foreach ($menu_property->buildings->sortBy('label') AS $building)
                                    <li class="nav-item">
                                				<a class="nav-link" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $building->slug]) }}">
                                						<span class="menu-title">{{ $building->label }}</span>
                                                        <i data-feather="chevron-right" class="menu-arrow"></i>
                                				</a>
                                	</li>
                                @endforeach
                            @endif
                        </ul>
                </div>
        </li>
    @endif



</ul>
