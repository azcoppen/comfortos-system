@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Explore Clients &amp; Properties</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Explore Comfort OS</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="layers"></i> Home</a>
              <a href="{{ route('explorer.index') }}" class="text-primary">OS Explorer</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('explorer.operators.create') }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Operator</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')

  <div class="row mb-4">
    <h1 class="display-4 col-md-6">Your Access Map</h1>
    <div class="col-md-2 offset-md-4">
      @if ( isset ($map) && $map->count() )
      {{ $map->links() }}
      @endif
    </div>
  </div>


  @if ( isset ($map) && $map->count() )
    @foreach ($map->sortBy('label') AS $op_index => $operator)

      <div class="row border-bottom border-secondary mb-3">
        <div class="col-md-1 col-sm-3 text-center text-muted border-right border-secondary">Operator</div>
        <div class="col-md-1 col-sm-3 text-center text-muted border-right border-secondary">Brand</div>
        <div class="col-md-1 col-sm-3 text-center text-muted border-right border-secondary">Property</div>
        <div class="col-md-1 col-sm-3 text-center text-muted border-right border-secondary">Building</div>
        <div class="col-md-1 col-sm-3 text-center text-muted border-right border-secondary">Floor</div>
        <div class="col-md-1 col-sm-3 text-left text-muted border-secondary">Rooms</div>
      </div>

      <div class="row pt-2 pb-2 border-bottom border-secondary">
        <div class="col-md-1 col-sm-2">
          <a class="btn btn-sm btn-base" href="{{ route ('explorer.operators.brands.index', $operator->slug) }}">
            {{ Str::limit($operator->label, 12) }}
          </a>
        </div>
        <div class="col-md-3 border-right border-secondary">
        </div>
      </div>

      @foreach ($operator->brands AS $brand)
        <div class="row pt-2 pb-2 border-bottom border-secondary">
          <div class="col-md-1 offset-md-1">
            <a class="btn btn-sm btn-primary" href="{{ route ('explorer.operators.brands.properties.index', [$operator->slug, $brand->slug]) }}">
              {{ Str::limit($brand->label, 12) }}
            </a>
          </div>
          <div class="col-md-2 border-right border-secondary">
          </div>
        </div>

        @foreach ($brand->properties AS $property)
          <div class="row pt-2 pb-2 border-bottom border-secondary">
            <div class="col-md-1 offset-md-2">
              <a class="btn btn-sm btn-warning" href="{{ route ('explorer.operators.brands.properties.buildings.index', [$operator->slug, $brand->slug, $property->slug]) }}">
                {{ Str::limit($property->label, 12) }}
              </a>
            </div>
            <div class="col-md-1 border-right border-secondary">
            </div>
          </div>

          @foreach ($property->buildings AS $building)
            <div class="row pt-2 pb-2 border-bottom border-secondary">
              <div class="col-md-1 offset-md-3 border-right border-secondary mr-5">
                <a class="btn btn-sm btn-secondary" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.index', [$operator->slug, $brand->slug, $property->slug, $building->slug]) }}">
                  {{ Str::limit($building->label, 12) }}
                </a>
              </div>

            </div>

              @foreach ($building->rooms->groupBy('floor')->sortKeys() AS $floor => $rooms)
                <div class="row pt-2 pb-2 border-bottom border-secondary">
                  <div class="col-md-6 offset-md-4 border-left">
                    <div class="row">
                      <div class="col-md-2">
                          <a title="Floor {{ $floor }}" href="{{ route ('explorer.operators.brands.properties.buildings.floor', [$operator->slug, $brand->slug, $property->slug, $building->slug, $floor ?? 0]) }}" class="btn btn-sm btn-info text-light">FL {{ $floor }}</a>
                      </div>
                      <div class="col-md-10">
                        @foreach ($rooms AS $room)
                          <a class="btn btn-sm btn-dark mt-3 mr-1" title="{{ $room->label }}" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.show', [$operator->slug, $brand->slug, $property->slug, $building->slug, $room->_id]) }}">{{ $room->number }}</a>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>

              @endforeach

          @endforeach

        @endforeach

      @endforeach

      @if ($op_index < $map->count())
        <div class="row mt-3 mb-5"></div>

        <div class="row mb-2 mt-3"></div>
      @endif

    @endforeach

    <div class="row mt-5"></div>

  @else
    <div class="row mt-1">
      <div class="col-md-12">
        <div class="alert alert-info" role="alert">
          <span class="alert-text"><i data-feather="alert-octagon"></i> <strong>You haven't added any operators yet. Once you add them, your map will appear here.</strong></span>
        </div>
      </div>
    </div>
  @endif

@endsection
