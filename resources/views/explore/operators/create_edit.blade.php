@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ isset ($operator) ? 'Manage '.$operator->label : 'Add A New Operator' }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Operators</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="layers"></i> Home</a>
              <a href="{{ route('explorer.index') }}">OS Explorer</a>
              <a href="{{ route('explorer.operators.index') }}" class="text-primary">Operators</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('explorer.index') }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4">{{ isset ($operator) ? 'Manage '.$operator->label : 'Add A New Operator' }}</h1>
  <hr />
  @if ( isset ($operator) )
    {!! Form::open (['method' => 'PUT', 'files' => true, 'route' => ['explorer.operators.update', $operator->slug]]) !!}
  @else
    {!! Form::open (['route' => 'explorer.operators.store', 'files' => true]) !!}
  @endif

    <div class="row">
      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="box"></i> Basics</h5>
              </div>
              <div class="card-body">

                      <div class="form-group">
                        @include ('components.fields.text', [
                          'label'       => "What is the company name?",
                          'name'        => 'label',
                          'value'       => $operator->label ?? null,
                          'default'     => '',
                          'placeholder' => "Name of the operator",
                          'required'    => true,
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.text', [
                          'label'       => "What slug should they use?",
                          'name'        => 'slug',
                          'value'       => $operator->slug ?? null,
                          'default'     => '',
                          'placeholder' => "(Leave blank to auto-generate)",
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.select', [
                          'label'       => "What is their primary language?",
                          'name'        => 'lang',
                          'options'     => config ('dropdowns.languages'),
                          'value'       => $operator->lang ?? null,
                          'default'     => 'en',
                          'placeholder' => "Choose their language",
                          'required'    => true,
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.select', [
                          'label'       => "What is their primary locale?",
                          'name'        => 'locale',
                          'options'     => config ('dropdowns.locales'),
                          'value'       => $operator->locale ?? null,
                          'default'     => 'en_US',
                          'placeholder' => "Choose their system locale",
                          'required'    => true,
                          'help'        => 'Many countries speak multiple languages.',
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.select', [
                          'label'       => "What is their timezone?",
                          'name'        => 'timezone',
                          'options'     => array_combine (array_values (config ('dropdowns.timezones')), array_values (config ('dropdowns.timezones'))),
                          'value'       => $operator->timezone ?? null,
                          'default'     => 'America/Los_Angeles',
                          'placeholder' => "Choose their timezone",
                          'required'    => true,
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.select', [
                          'label'       => "What is their primary currency?",
                          'name'        => 'currency',
                          'options'     => config ('dropdowns.currencies'),
                          'value'       => $operator->currency ?? null,
                          'default'     => 'USD',
                          'placeholder' => "Choose their currency",
                          'required'    => true,
                        ])
                      </div>

              </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="map-pin"></i> Location</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is their street address?",
                      'name'        => 'street',
                      'value'       => $operator->location['street'] ?? null,
                      'default'     => '',
                      'placeholder' => "123 Example Street, Suite 40",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is their city?",
                      'name'        => 'city',
                      'value'       => $operator->location['city'] ?? null,
                      'default'     => '',
                      'placeholder' => "Los Angeles",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is their state, province, or region?",
                      'name'        => 'region',
                      'value'       => $operator->location['region'] ?? null,
                      'default'     => '',
                      'placeholder' => "CA",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is their zip/postal code?",
                      'name'        => 'postal',
                      'value'       => $operator->location['postal'] ?? null,
                      'default'     => '',
                      'placeholder' => "90210",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.select', [
                      'label'       => "What is their country?",
                      'name'        => 'country',
                      'options'     => config ('dropdowns.countries'),
                      'value'       => $operator->location['country'] ?? null,
                      'default'     => 'US',
                      'placeholder' => "Choose their country",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is their GPS longitude?",
                              'name'        => 'longitude',
                              'value'       => $operator->geo['coordinates'][0] ?? null,
                              'default'     => '',
                              'placeholder' => "-118.375397",
                              'required'    => true,
                              'help'        => 'Help: https://www.latlong.net/',
                            ])
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is their GPS latitude?",
                              'name'        => 'latitude',
                              'value'       => $operator->geo['coordinates'][1] ?? null,
                              'default'     => '',
                              'placeholder' => "34.063251",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                    </div>

                </div>
          </div>
        </div> <!-- end col -->

      </div> <!-- end row -->


      <div class = "row">


        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="mail"></i> Contact Details</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.textarea', [
                      'label'       => "What are their contact phone numbers?",
                      'name'        => 'telephones',
                      'value'       => collect ($operator->telephones ?? [])->implode ("\n") ?? null,
                      'default'     => '',
                      'placeholder' => "Add a phone number on each line",
                      'rows'        => 3,
                      'required'    => true,
                      'help'        => 'Add one phone number per line',
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.textarea', [
                      'label'       => "What are their contact email addresses?",
                      'name'        => 'emails',
                      'value'       => collect ($operator->emails ?? [])->implode ("\n") ?? null,
                      'default'     => '',
                      'placeholder' => "Add an email address on each line",
                      'rows'        => 3,
                      'required'    => true,
                      'help'        => 'Add one email address per line',
                    ])
                  </div>

                </div>
          </div>
        </div> <!-- end col -->



        <div class="col-lg-6 col-md-6">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="edit"></i> Additional Notes</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.textarea', [
                      'label'       => "Add any additional information here",
                      'name'        => 'notes',
                      'value'       => isset ($operator) ? $operator->notes->pluck('content')->implode ("\n") : null,
                      'default'     => '',
                      'placeholder' => "What is important to know?",
                      'rows'        => 9,
                    ])
                  </div>

                </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="camera"></i> Cover image</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                      <input name="image" type="file" class="dropify" data-default-file="{{ isset ($operator) && $operator->image ? 'https://res.cloudinary.com/smartrooms/operators/' . $operator->image .'.jpg' : '' }}" data-max-file-size="5M" data-allowed-formats="landscape" data-allowed-file-extensions="jpg jpeg png" />
                  </div>

                </div>
          </div>
        </div> <!-- end col -->

    </div> <!-- end row -->

    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}

    @if ( isset ($operator) )
      @include ('partials.components.panels.delete_bar', [
        'entity'  => $operator,
        'text'    => 'Operator',
        'destroy' => [
          'route' => 'explorer.operators.destroy',
          'params'=> [$operator->slug],
        ],
        'return'  => [
          'route' => 'explorer.operators.index',
          'params'=> [$operator->slug],
        ],
      ])
    @endif


@stop
