@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: Explore Clients &amp; Properties</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Explore Comfort OS</div>
          <div class="header-breadcrumb">
              <a href="{{ route('explorer.index') }}">OS Explorer</a>
              <a href="{{ route('explorer.operators.index') }}" class="text-primary">Operators</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('explorer.operators.create') }}" class="btn btn-success btn-header"><i data-feather="plus-circle"></i> Add New Operator</a>
      </div>
  </div>
  @include ('partials.components.forms.search_bar', ['text' => 'Operators'])
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4">Operators You Have Access To</h1>
  <hr />

  @if ($menu_operators->count())
    <div class="row mt-5">
        @foreach ($menu_operators AS $operator)

          <div class="col-lg-4 col-md-6 col-sm-6 ">
              <div class="card card-margin">
                  <div class="card-body p-1" style="background-image: url({{ $operator->image ? 'https://res.cloudinary.com/smartrooms/operators/' . $operator->image .'.jpg' : config ('comfortos.placeholders.image.operator') }}); background-size: cover;">

                      <div class="widget-4">
                          <div class="widget-4-icon option-3">
                              <a title="Manage {{ $operator->label }}" href="{{ route ('explorer.operators.edit', $operator->slug) }}"><i data-feather="codepen"></i></a>
                          </div>
                          <a class="text-light" style="text-decoration: none;" href="{{ route ('explorer.operators.brands.index', $operator->slug) }}">
                            <div class="widget-4-body pt-5">
                              <h1 class="display-3 p-3 mt-5 mb-2">
                                  {{ $operator->label }}
                              </h1>

                            </div>
                          </a>
                      </div>

                  </div>

                  <div class="card-footer">
                    @if ($operator->brands->count())
                      @foreach ($operator->brands AS $brand)
                        <span class="badge badge-info mt-2 mr-2">{{ $brand->label ?? '' }}</span>
                      @endforeach
                    @else
                      <span class="text-muted">No brands added yet.</span>
                    @endif
                  </div>

              </div>
          </div>

        @endforeach
    </div>
  @else
    <div class="row mt-1">
      <div class="col-md-12">
        <div class="alert alert-info" role="alert">
          <span class="alert-text"><i data-feather="alert-octagon"></i> <strong>You haven't added any operators yet. Once you add them, they will appear here.</strong></span>
        </div>
      </div>
    </div>
  @endif
@endsection
