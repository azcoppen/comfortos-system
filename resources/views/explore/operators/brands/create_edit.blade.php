@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_operator->label }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">{{ $menu_operator->label }}</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="layers"></i> Home</a>
              <a href="{{ route('explorer.index') }}">OS Explorer</a>
              <a href="{{ route('explorer.operators.index') }}">Operators</a>
              <a href="{{ route('explorer.operators.brands.index', $menu_operator->slug) }}" class="text-primary">{{ $menu_operator->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('explorer.operators.brands.index', $menu_operator->slug) }}" class="btn btn-header"><i data-feather="plus-circle"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4">{{ $menu_operator->label }} <span class="text-muted"> / {{ isset ($brand) ? 'Manage '.$brand->label : 'Add A New Brand' }}</span></h1>
  <hr />

  @if ( isset ($brand) )
    {!! Form::open (['method' => 'PUT', 'files' => true, 'route' => ['explorer.operators.brands.update', [$brand->operator->slug, $brand->slug]]]) !!}
  @else
    {!! Form::open (['route' => ['explorer.operators.brands.store', $menu_operator->slug], 'files' => true,]) !!}
  @endif

  <div class="row">

    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="box"></i> Basics</h5>
            </div>
            <div class="card-body">

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What is the brand's name?",
                        'name'        => 'label',
                        'value'       => $brand->label ?? null,
                        'default'     => '',
                        'placeholder' => "Name of the brand",
                        'required'    => true,
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What slug should it use?",
                        'name'        => 'slug',
                        'value'       => $brand->slug ?? null,
                        'default'     => '',
                        'placeholder' => "(Leave blank to auto-generate)",
                      ])
                    </div>

            </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="tag"></i> Tags</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.textarea', [
                    'label'       => "Specify a list of tags for this brand",
                    'name'        => 'tags',
                    'value'       => isset ($brand) ? collect($brand->tags)->implode ("\n") : $menu_operator->slug,
                    'default'     => '',
                    'placeholder' => "What tags should people use to find this?",
                    'rows'        => 4,
                    'help'        => 'Add one tag per line',
                  ])
                </div>

              </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="edit"></i> Additional Notes</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.textarea', [
                    'label'       => "Add any additional information here",
                    'name'        => 'notes',
                    'value'       => isset ($brand) ? $brand->notes->pluck('content')->implode ("\n") : null,
                    'default'     => '',
                    'placeholder' => "What is important to know?",
                    'rows'        => 9,
                  ])
                </div>

              </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="camera"></i> Cover image</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                    <input name="image" type="file" class="dropify" data-default-file="{{ isset ($brand) && $brand->image ? 'https://res.cloudinary.com/smartrooms/brands/' . $brand->image .'.jpg' : '' }}" data-max-file-size="5M" data-allowed-formats="landscape" data-allowed-file-extensions="jpg jpeg png" />
                </div>

              </div>
        </div>
      </div> <!-- end col -->

  </div> <!-- end row -->

  <hr />
  <div class="row mb-4">
    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}


  @if ( isset ($brand) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $brand,
      'text'    => 'Brand',
      'destroy' => [
        'route' => 'explorer.operators.brands.destroy',
        'params'=> [$menu_operator->slug, $menu_brand->slug],
      ],
      'return'  => [
        'route' => 'explorer.operators.brands.index',
        'params'=> [$menu_operator->slug],
      ],
    ])
  @endif


@endsection
