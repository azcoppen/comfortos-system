@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_brand->label }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">{{ $menu_brand->label }}</div>
          <div class="header-breadcrumb">
              <a href="{{ route('dashboards.index') }}"><i data-feather="layers"></i> Home</a>
              <a href="{{ route('explorer.index') }}">OS Explorer</a>
              <a href="{{ route('explorer.operators.show', $menu_operator->slug) }}">{{ $menu_operator->label }}</a>
              <a href="{{ route('explorer.operators.brands.show', [$menu_operator->slug, $menu_brand->slug]) }}">{{ $menu_brand->label }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>

  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">{{ $menu_operator->label }} / {{ $menu_brand->label }} / </span> {{ isset ($property) ? 'Manage '.$property->label : 'Add A New Property' }}</h1>
  <hr />
  @if ( isset ($property) )
    {!! Form::open (['method' => 'PUT', 'files' => true, 'route' => ['explorer.operators.brands.properties.update', [$menu_operator->slug, $menu_brand->slug, $property->slug]]]) !!}
  @else
    {!! Form::open (['route' => ['explorer.operators.brands.properties.store', [$menu_operator->slug, $menu_brand->slug]], 'files' => true,]) !!}

    @if (! request()->has ('autofill') )
    <div class="row">
      <div class="col-sm-12">
        <div class="alert alert-outline-info" role="alert">
          <div class="alert-text col-sm-9">
              Are these details the same as the parent operator, {{ $menu_operator->label }}?
          </div>
          <div class="col-sm-3 text-right">
            <a class="btn btn-info" href="?autofill={{ $menu_operator->_id }}">Autofill from {{ $menu_operator->label }}</a>
          </div>
        </div>
      </div>
    </div>
    @endif

  @endif

    <div class="row">
      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="box"></i> Basics</h5>
              </div>
              <div class="card-body">

                      <div class="form-group">
                        @include ('components.fields.text', [
                          'label'       => "What is the property's name?",
                          'name'        => 'label',
                          'value'       => $property->label ?? null,
                          'default'     => '',
                          'placeholder' => "Name of the property",
                          'required'    => true,
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.text', [
                          'label'       => "What slug should it use?",
                          'name'        => 'slug',
                          'value'       => $property->slug ?? null,
                          'default'     => '',
                          'placeholder' => "(Leave blank to auto-generate)",
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.select', [
                          'label'       => "What is its primary language?",
                          'name'        => 'lang',
                          'options'     => config ('dropdowns.languages'),
                          'value'       => $property->lang ?? ($autofill->lang ?? null),
                          'default'     => 'en',
                          'placeholder' => "Choose its language",
                          'required'    => true,
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.select', [
                          'label'       => "What is its primary locale?",
                          'name'        => 'locale',
                          'options'     => config ('dropdowns.locales'),
                          'value'       => $property->locale ?? ($autofill->locale ?? null),
                          'default'     => 'en_US',
                          'placeholder' => "Choose its system locale",
                          'required'    => true,
                          'help'        => 'Many countries speak multiple languages.',
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.select', [
                          'label'       => "What is its timezone?",
                          'name'        => 'timezone',
                          'options'     => array_combine (array_values (config ('dropdowns.timezones')), array_values (config ('dropdowns.timezones'))),
                          'value'       => $property->timezone ?? ($autofill->timezone ?? null),
                          'default'     => 'America/Los_Angeles',
                          'placeholder' => "Choose its timezone",
                          'required'    => true,
                        ])
                      </div>

                      <div class="form-group">
                        @include ('components.fields.select', [
                          'label'       => "What is its primary currency?",
                          'name'        => 'currency',
                          'options'     => config ('dropdowns.currencies'),
                          'value'       => $property->currency ?? ($autofill->currency ?? null),
                          'default'     => 'USD',
                          'placeholder' => "Choose its currency",
                          'required'    => true,
                        ])
                      </div>

              </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="map-pin"></i> Location</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is its street address?",
                      'name'        => 'street',
                      'value'       => $property->location['street'] ?? ($autofill->location['street'] ?? null),
                      'default'     => '',
                      'placeholder' => "123 Example Street, Suite 40",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is its city?",
                      'name'        => 'city',
                      'value'       => $property->location['city'] ?? ($autofill->location['city'] ?? null),
                      'default'     => '',
                      'placeholder' => "Los Angeles",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is its state, province, or region?",
                      'name'        => 'region',
                      'value'       => $property->location['region'] ?? ($autofill->location['region'] ?? null),
                      'default'     => '',
                      'placeholder' => "CA",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is its zip/postal code?",
                      'name'        => 'postal',
                      'value'       => $property->location['postal'] ?? ($autofill->location['postal'] ?? null),
                      'default'     => '',
                      'placeholder' => "90210",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.select', [
                      'label'       => "What is its country?",
                      'name'        => 'country',
                      'options'     => config ('dropdowns.countries'),
                      'value'       => $property->location['country'] ?? ($autofill->location['country'] ?? null),
                      'default'     => 'US',
                      'placeholder' => "Choose its country",
                      'required'    => true,
                    ])
                  </div>

                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is its GPS longitude?",
                              'name'        => 'longitude',
                              'value'       => $property->geo['coordinates'][0] ?? ($autofill->geo['coordinates'][0] ?? null),
                              'default'     => '',
                              'placeholder' => "-118.375397",
                              'required'    => true,
                              'help'        => 'Help: https://www.latlong.net/',
                            ])
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="position-relative form-group">
                            @include ('components.fields.text', [
                              'label'       => "What is its GPS latitude?",
                              'name'        => 'latitude',
                              'value'       => $property->geo['coordinates'][1] ?? ($autofill->geo['coordinates'][1] ?? null),
                              'default'     => '',
                              'placeholder' => "34.063251",
                              'required'    => true,
                            ])
                          </div>
                      </div>
                    </div>

                </div>
          </div>
        </div> <!-- end col -->

      </div> <!-- end row -->


      <div class = "row">


        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="mail"></i> Contact Details</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.textarea', [
                      'label'       => "What are its contact phone numbers?",
                      'name'        => 'telephones',
                      'value'       => collect ($property->telephones ?? $autofill->telephones ?? [])->implode ("\n") ?? null,
                      'default'     => '',
                      'placeholder' => "Add a phone number on each line",
                      'rows'        => 3,
                      'required'    => true,
                      'help'        => 'Add one phone number per line',
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.textarea', [
                      'label'       => "What are its contact email addresses?",
                      'name'        => 'emails',
                      'value'       => collect ($property->emails ?? $autofill->emails ?? [])->implode ("\n") ?? null,
                      'default'     => '',
                      'placeholder' => "Add an email address on each line",
                      'rows'        => 3,
                      'required'    => true,
                      'help'        => 'Add one email address per line',
                    ])
                  </div>

                </div>
          </div>
        </div> <!-- end col -->


        <div class="col-lg-6 col-md-6">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="tag"></i> Tags</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.textarea', [
                      'label'       => "Specify a list of tags for this property",
                      'name'        => 'tags',
                      'value'       => isset ($property) ? collect($property->tags)->implode ("\n") : $menu_operator->slug ."\n" .$menu_brand->slug,
                      'default'     => '',
                      'placeholder' => "What tags should people use to find this?",
                      'rows'        => 6,
                      'help'        => 'Add one tag per line',
                    ])
                  </div>

                </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="edit"></i> Additional Notes</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.textarea', [
                      'label'       => "Add any additional information here",
                      'name'        => 'notes',
                      'value'       => isset ($property) ? $property->notes->pluck('content')->implode ("\n") : null,
                      'default'     => '',
                      'placeholder' => "What is important to know?",
                      'rows'        => 9,
                    ])
                  </div>

                </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="camera"></i> Cover image</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                      <input name="image" type="file" class="dropify" data-default-file="{{ isset ($property) && $property->image ? 'https://res.cloudinary.com/smartrooms/properties/' . $property->image .'.jpg' : '' }}" data-max-file-size="5M" data-allowed-formats="landscape" data-allowed-file-extensions="jpg jpeg png" />
                  </div>

                </div>
          </div>
        </div> <!-- end col -->

        <div class="col-lg-6 col-md-6">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="life-buoy"></i> Messaging Brokers</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.select', [
                      'label'       => "Which MQTT broker should it use?",
                      'name'        => 'mqtt_broker_id',
                      'options'     => $mqtt_brokers->sortBy('label')->pluck('label', '_id')->all(),
                      'value'       => isset ($property) && is_object ($property->mqtt_brokers->first()) ? $property->mqtt_brokers->first()->_id : null,
                      'default'     => '',
                      'placeholder' => "Choose an MQTT broker",
                      'required'    => false,
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.select', [
                      'label'       => "Which Websocket broker should it use?",
                      'name'        => 'ws_broker_id',
                      'options'     => $ws_brokers->sortBy('label')->pluck('label', '_id')->all(),
                      'value'       => isset ($property) && is_object ($property->ws_brokers->first()) ? $property->ws_brokers->first()->_id : null,
                      'default'     => '',
                      'placeholder' => "Choose a Websocket broker",
                      'required'    => false,
                    ])
                  </div>

                </div>
          </div>
        </div> <!-- end col -->

    </div> <!-- end row -->

    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}


    @if ( isset ($property) )
      @include ('partials.components.panels.delete_bar', [
        'entity'  => $property,
        'text'    => 'Property',
        'destroy' => [
          'route' => 'explorer.operators.brands.properties.destroy',
          'params'=> [$menu_operator->slug, $menu_brand->slug, $property->slug],
        ],
        'return'  => [
          'route' => 'explorer.operators.brands.properties.index',
          'params'=> [$menu_operator->slug, $menu_brand->slug],
        ],
      ])
    @endif

@endsection
