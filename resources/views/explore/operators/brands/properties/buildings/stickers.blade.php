<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <title>Printable QR Code Stickers for Floor {{ $floor }}, {{ $building->label }}, {{ $property->label }}</title>
    <style>* {font-family: 'Poppins', sans-serif;}</style>
  </head>
  <body>
    <div class="container-fluid">

      <div class="row align-items-center">
        <div class="col card-group">
          @if (isset($building))
            @foreach ($building->rooms->where('floor', $floor)->sortBy('number') AS $room)

              <div class="card border-0 m-2">
                <div class="card-header bg-transparent border-0 text-center">
                  <h2 class=" m-0">Room {{ $room->number }}</h2>
                </div>

                <div class="card-body bg-transparent border-0 text-center">
                  {!! QrCode::size(200)->generate(route ('guest.room.login', [$room->_id])); !!}
                </div>
                <div class="card-footer border-0 bg-transparent text-center text-muted m-0">
                  <small>https://guest.smartrooms.cloud</small>
                </div>
              </div>

            @endforeach
          @endif
        </div>
      </div>

    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script>window.print()</script>
  </body>
</html>
