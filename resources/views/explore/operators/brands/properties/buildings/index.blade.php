@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">{{ $menu_property->label }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}" class="text-primary">{{ Str::limit($menu_property->label, 10) }}</a>
          </div>
      </div>

      <div class="page-header-action">
              <a href="{{ route ('explorer.operators.brands.properties.buildings.create', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Building</a>
      </div>

  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'buildings within '.$menu_property->label])
@stop

@section ('header-actions')
@endsection

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted"> {{ $menu_brand->label }} /</span> {{ $menu_property->label }} <span class="text-muted">/ buildings</span></h1>
  <hr />


      @if ( $menu_buildings->count () )
        <div class="row mt-5">
        @foreach ($menu_buildings AS $building)

          <div class="col-lg-4 col-md-6 col-sm-6 ">
              <div class="card card-margin">
                  <div class="card-body p-1" style="background-image: url({{ $building->image ? 'https://res.cloudinary.com/smartrooms/buildings/' . $building->image .'.jpg' : config ('comfortos.placeholders.image.building') }}); background-size: cover;">

                      <div class="widget-4">
                          <div class="widget-4-icon option-3">
                              <a title="Manage {{ $building->label }}" href="{{ route ('explorer.operators.brands.properties.buildings.edit', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $building->slug]) }}"><i data-feather="map"></i></a>
                          </div>
                          <a class="text-light" style="text-decoration: none;" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $building->slug]) }}">
                            <div class="widget-4-body pt-5">
                              <h1 class="display-3 pt-5 p-3 mt-5 mb-2">
                                  {{ $building->label }}
                              </h1>

                            </div>
                          </a>
                      </div>

                  </div>

                  <div class="card-footer">
                    <span class="badge badge-secondary mt-2 mr-2">{{ $building->rooms->unique('floor')->count() ?? '' }} FLOORS</span>
                    <span class="badge badge-info mt-2 mr-2">{{ $building->rooms->count() ?? '' }} ROOMS</span>
                  </div>

              </div>
          </div>

        @endforeach
        </div>
      @else
        <div class="row mt-1">
          <div class="col-md-12">
            <div class="alert alert-info" role="alert">
              <span class="alert-text"><i data-feather="alert-octagon"></i> <strong>You haven't added any buildings yet. Once you add them, they will appear here.</strong></span>
            </div>
          </div>
        </div>
      @endif

@endsection
