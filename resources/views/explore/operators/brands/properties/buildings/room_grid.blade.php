<div wire:poll.10s>

  <div class="row mt-4">
    <h3 class="col-md-12">
      @foreach ($unique_floors AS $unique_floor)
        @if ( $unique_floor == $menu_floor )
            <span title="Floor {{ $unique_floor }}" class="badge badge-info mr-1 text-light">FL {{ $unique_floor }}</span>
        @else
            <span class="badge badge-light mr-1">
              <a title="Floor {{ $unique_floor }}" href="{{ route ('explorer.operators.brands.properties.buildings.floor', [$building->property->brand->operator->slug, $building->property->brand->slug, $building->property->slug, $building->slug, $unique_floor ?? 0]) }}" class="text-info">FL {{ $unique_floor }}</a>
            </span>
        @endif

        <a class="btn btn-secondary" target="_blank" href="{{ route ('explorer.operators.brands.properties.buildings.floor.stickers', [$building->property->brand->operator->slug, $building->property->brand->slug, $building->property->slug, $building->slug, $unique_floor ?? 0]) }}"><i class="fa fa-print"></i> Print Floor {{ $unique_floor }} QR Code Stickers</a>

      @endforeach
    </h3>
  </div>
  <hr />

  @foreach ($rooms->groupBy('floor')->sortKeys() AS $floor => $rooms)

    <div class="row">
    @foreach ($rooms AS $room)
      @php ( $room->setAppends (['summary'])) @endphp
      <div class="col-lg-3 col-md-3 col-sm-4">
              <div class="card card-margin">

                <a class="text-light" style="text-decoration: none;" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.show', [$room->building->property->brand->operator->slug, $room->building->property->brand->slug, $room->building->property->slug, $room->building->slug, $room->_id]) }}">
                      <div class="card-body p-1" style="background-image: url({{ $room->image ? 'https://res.cloudinary.com/smartrooms/rooms/' . $room->image .'.jpg' : config ('comfortos.placeholders.image.room') }}); background-size: cover;">
                        <h1 class="display-3 pl-3 pt-3 mt-5 mb-0">
                            {{ $room->number ?? '' }}
                        </h1>
                        <p class="pl-3 mt-0">{{ $room->label ?? '' }}</p>
                      </div>
                </a>
                <div class="card-footer text-center">
                  @if ($room->summary && $room->summary->count() > 0)
                      @foreach ($room->summary AS $attribute => $value)
                        @if (! is_object ($value) && ! (is_array ($value) && $attribute != 'hsb') )
                        <span title="{{ $attribute }}" class="badge badge-soft-dark mr-1 mb-1 col-ms-4 col-sm-3">
                          <span class="text-dark">{{ Str::limit(strtoupper($attribute), 5) }}</span><br />
                          <span class="text-primary">{{ is_string ($value) ? $value : Str::limit(json_encode ($value), 5) }}</span>
                        </span>
                        @endif
                      @endforeach
                  @else
                      <span class="text-muted">No room data.</span>
                  @endif
                </div>
              </div>
      </div>
    @endforeach
    </div>
  @endforeach

</div>
