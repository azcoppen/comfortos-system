@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">{{ $menu_property->label }}</div>
          <div class="header-breadcrumb">
              <a href="{{ route('explorer.operators.brands.show', [$menu_operator->slug, $menu_brand->slug]) }}"><i data-feather="map-pin"></i> {{ $menu_brand->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}" class="text-primary">{{ $menu_property->label }}</a>
          </div>
      </div>

      <div class="page-header-action">
              <a href="{{ route ('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>

  </div>
@stop

@section ('header-actions')
@endsection

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">{{ $menu_property->label }} / </span>{{ isset ($building) ? 'Manage '.$building->label : 'Add A New Building' }}</h1>
  <hr />

  @if ( isset ($building) )
    {!! Form::open (['method' => 'PUT', 'files' => true, 'route' => ['explorer.operators.brands.properties.buildings.update', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $building->slug]]]) !!}
  @else
    {!! Form::open (['route' => ['explorer.operators.brands.properties.buildings.store', $menu_operator->slug, $menu_brand->slug, $menu_property->slug], 'files' => true, ]) !!}

    @if (! request()->has ('autofill') )
    <div class="row">
      <div class="col-sm-12">
        <div class="alert alert-outline-info" role="alert">
          <div class="alert-text col-sm-6">
              Are these details the same as the parent property, {{ $menu_property->label }}?
          </div>
          <div class="col-sm-6 text-right">
            <a class="btn btn-info" href="?autofill={{ $menu_property->_id }}">Autofill from {{ $menu_property->label }}</a>
          </div>
        </div>
      </div>
    </div>
    @endif

  @endif

  <div class="row">

    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="box"></i> Basics</h5>
            </div>
            <div class="card-body">

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What is the building's name?",
                        'name'        => 'label',
                        'value'       => $building->label ?? 'Main Building',
                        'default'     => '',
                        'placeholder' => "Name of the building",
                        'required'    => true,
                        'help'        => 'If there is only one building, use a generic term like "main building"',
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What slug should it use?",
                        'name'        => 'slug',
                        'value'       => $building->slug ?? null,
                        'default'     => '',
                        'placeholder' => "(Leave blank to auto-generate)",
                      ])
                    </div>

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What identifier should it use?",
                        'name'        => 'identifier',
                        'value'       => $building->identifier ?? 'main',
                        'default'     => '',
                        'placeholder' => "main",
                        'required'    => true,
                        'help'        => 'If there is only one building, use a generic term like "main"',
                      ])
                    </div>
            </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="map-pin"></i> Location &amp; Tags</h5>
              </div>
              <div class="card-body">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "What is its GPS longitude?",
                            'name'        => 'longitude',
                            'value'       => $building->geo['coordinates'][0] ?? ($autofill->geo['coordinates'][0] ?? null),
                            'default'     => '',
                            'placeholder' => "-118.375397",
                            'required'    => true,
                            'help'        => 'Help: https://www.latlong.net/',
                          ])
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                          @include ('components.fields.text', [
                            'label'       => "What is its GPS latitude?",
                            'name'        => 'latitude',
                            'value'       => $building->geo['coordinates'][1] ?? ($autofill->geo['coordinates'][1] ?? null),
                            'default'     => '',
                            'placeholder' => "34.063251",
                            'required'    => true,
                          ])
                        </div>
                    </div>
                  </div>

                  <div class="form-group">
                    @include ('components.fields.textarea', [
                      'label'       => "Specify a list of tags for this building",
                      'name'        => 'tags',
                      'value'       => isset ($building) ? collect($building->tags)->implode ("\n") : $menu_operator->slug . "\n". $menu_brand->slug. "\n". $menu_property->slug,
                      'default'     => '',
                      'placeholder' => "What tags should people use to find this?",
                      'rows'        => 5,
                      'help'        => 'Add one tag per line',
                    ])
                  </div>

              </div>
          </div>
      </div> <!-- end col -->


      <div class="col-lg-6 col-md-6">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="edit"></i> Additional Notes</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                  @include ('components.fields.textarea', [
                    'label'       => "Add any additional information here",
                    'name'        => 'notes',
                    'value'       => isset ($building) ? $building->notes->pluck('content')->implode ("\n") : null,
                    'default'     => '',
                    'placeholder' => "What is important to know?",
                    'rows'        => 9,
                  ])
                </div>

              </div>
        </div>
      </div> <!-- end col -->

      <div class="col-lg-6 col-md-6">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="camera"></i> Cover image</h5>
              </div>
              <div class="card-body">

                <div class="form-group">
                    <input name="image" type="file" class="dropify" data-default-file="{{ isset ($building) && $building->image ? 'https://res.cloudinary.com/smartrooms/buildings/' . $building->image .'.jpg' : '' }}" data-max-file-size="5M" data-allowed-formats="landscape" data-allowed-file-extensions="jpg jpeg png" />
                </div>

              </div>
        </div>
      </div> <!-- end col -->

  </div> <!-- end row -->

  <hr />
  <div class="row mb-4">
    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}


  @if ( isset ($building) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $building,
      'text'    => 'Building',
      'destroy' => [
        'route' => 'explorer.operators.brands.properties.buildings.destroy',
        'params'=> [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $building->slug],
      ],
      'return'  => [
        'route' => 'explorer.operators.brands.properties.buildings.index',
        'params'=> [$menu_operator->slug, $menu_brand->slug, $menu_property->slug],
      ],
    ])
  @endif

@endsection
