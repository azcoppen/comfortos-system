@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ Str::limit($menu_building->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}">[{{ $menu_room->floor }}]-{{ $menu_room->number }}</a>
            <a href="" class="text-primary">Manage</a>
          </div>
      </div>
      <div class="page-header-action">
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>

  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')

  <h1 class="display-4"><span class="text-muted">manage / </span> {{ $room->label ?? 'Room #'.$room->number }}</h1>
  <hr />

  {!! Form::open (['method' => 'PUT', 'files' => true, 'route' => ['explorer.operators.brands.properties.buildings.rooms.update', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $room->_id]]]) !!}


  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="box"></i> General Data</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $room->label ?? null,
                  'default'     => '',
                  'placeholder' => "Room [X], Main Building, Property",
                  'required'    => true,
                ])
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.number', [
                          'label'       => "Which floor is it on?",
                          'name'        => 'floor',
                          'value'       => $room->floor ?? null,
                          'default'     => '',
                          'placeholder' => "0",
                          'required'    => false,
                          'disabled'    => true,
                          'help'        => "You can't edit this."
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.number', [
                          'label'       => "Which room number is it?",
                          'name'        => 'floor',
                          'value'       => $room->number ?? null,
                          'default'     => '',
                          'placeholder' => "100",
                          'required'    => false,
                          'disabled'    => true,
                          'help'        => "You can't edit this."
                        ])
                      </div>
                  </div>
                </div>



                <div class="form-group">
                  @include ('components.fields.select', [
                    'label'       => "What type of room is it?",
                    'name'        => 'type',
                    'options'     => array_combine (config ('comfortos.room_types'), config ('comfortos.room_types')),
                    'value'       => $room->type ?? null,
                    'default'     => 'double',
                    'placeholder' => "Choose a room type:",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.select', [
                    'label'       => "What grade of room is it?",
                    'name'        => 'grade',
                    'options'     => array_combine (config ('comfortos.room_grades'), config ('comfortos.room_grades')),
                    'value'       => $room->grade ?? null,
                    'default'     => 'queen',
                    'placeholder' => "Choose a room grade:",
                    'required'    => true,
                  ])
                </div>

                <div class="form-group">
                  @include ('components.fields.textarea', [
                    'label'       => "Provide a brief description of this room:",
                    'name'        => 'description',
                    'value'       => $room->description ?? null,
                    'default'     => '',
                    'placeholder' => "Guest room, 1 Double, Courtyard view",
                    'rows'        => 7,
                    'required'    => true,
                  ])
                </div>


            </div>
        </div>
    </div> <!-- end col -->

    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="grid"></i> Layout</h5>
            </div>
            <div class="card-body">

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "What is its GPS longitude?",
                          'name'        => 'longitude',
                          'value'       => $room->geo['coordinates'][0] ?? null,
                          'default'     => '',
                          'placeholder' => "-118.375397",
                          'required'    => false,
                          'help'        => 'Help: https://www.latlong.net/',
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.text', [
                          'label'       => "What is its GPS latitude?",
                          'name'        => 'latitude',
                          'value'       => $room->geo['coordinates'][1] ?? null,
                          'default'     => '',
                          'placeholder' => "34.063251",
                          'required'    => false,
                        ])
                      </div>
                  </div>
                </div>

              <div class="form-row">
                  <div class="col-md-4">
                      <div class="position-relative form-group">
                        @include ('components.fields.number', [
                          'label'       => "Size (Sq. Ft)",
                          'name'        => 'footage',
                          'value'       => $room->footage ?? null,
                          'default'     => '',
                          'placeholder' => "400",
                          'required'    => true,
                          'help'        => "Can be approximate.",
                        ])
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="position-relative form-group">
                        @include ('components.fields.number', [
                          'label'       => "How many beds?",
                          'name'        => 'beds',
                          'value'       => $room->beds ?? null,
                          'default'     => '',
                          'placeholder' => "1",
                          'required'    => true,
                          'help'        => "Can be approximate.",
                        ])
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="position-relative form-group">
                        @include ('components.fields.number', [
                          'label'       => "Max occupancy?",
                          'name'        => 'occupancy',
                          'value'       => $room->occupancy ?? null,
                          'default'     => '',
                          'placeholder' => "2",
                          'required'    => true,
                          'help'        => "Max number of guests.",
                        ])
                      </div>
                  </div>
                </div>


              <div class="form-group">
                  @include ('components.fields.radioset', [
                    'label'       => "Is this room inside or outside?",
                    'color'       => 'primary',
                    'name'        => 'ext',
                    'options'     => ['Inside' => 0, 'Outside' => 1],
                    'value'       => $room->ext ?? 0,
                    'default'     => 0,
                    'required'    => true,
                  ])
              </div>

              <div class="form-group">
                  @include ('components.fields.radioset', [
                    'label'       => "Is this room accessible to the public?",
                    'color'       => 'primary',
                    'name'        => 'public',
                    'options'     => ['Yes' => 1, 'No' => 0],
                    'value'       => $room->public ?? 0,
                    'default'     => 0,
                    'required'    => true,
                  ])
              </div>

              <div class="form-group">
                  @include ('components.fields.radioset', [
                    'label'       => "Is this a smoking room?",
                    'color'       => 'primary',
                    'name'        => 'smoking',
                    'options'     => ['Smoking' => 1, 'No Smoking' => 0],
                    'value'       => $room->smoking ?? 0,
                    'default'     => 0,
                  ])
              </div>

              <div class="form-group">
                  @include ('components.fields.radioset', [
                    'label'       => "Does this room have A/C?",
                    'color'       => 'primary',
                    'name'        => 'AC',
                    'options'     => ['Yes' => 1, 'No' => 0],
                    'value'       => $room->AC ?? 1,
                    'default'     => 1,
                  ])
              </div>

              <div class="form-group">
                  @include ('components.fields.radioset', [
                    'label'       => "Is Ethernet available?",
                    'color'       => 'warning',
                    'name'        => 'eth',
                    'options'     => ['yes' => 1, 'No' => 0],
                    'value'       => $room->eth ?? null,
                    'default'     => 0,
                    'required'    => true,
                  ])
              </div>

            </div>
        </div>
    </div> <!-- end col -->

  </div> <!-- end roo -->

  <div class="row">

    <div class="col-lg-4 col-md-4 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="tag"></i> Tags</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.textarea', [
                  'label'       => "Specify a list of tags for this room",
                  'name'        => 'tags',
                  'value'       => collect($room->tags ?? [])->implode ("\n"),
                  'default'     => '',
                  'placeholder' => "What tags should people use to find this?",
                  'rows'        => 10,
                  'help'        => 'Add one tag per line',
                ])
              </div>

            </div>
        </div>
    </div> <!-- end col -->

    <div class="col-lg-8 col-md-8 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="edit"></i> Notes</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.textarea', [
                  'label'       => "Add any additional information here",
                  'name'        => 'notes',
                  'value'       => $room->notes->pluck('content')->implode ("\n"),
                  'default'     => '',
                  'placeholder' => "What is important to know?",
                  'rows'        => 11,
                ])
              </div>

            </div>
        </div>
    </div> <!-- end col -->

    <div class="col-lg-6 col-md-6">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="camera"></i> Cover image</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                  <input name="image" type="file" class="dropify" data-default-file="{{ isset ($room) && $room->image ? 'https://res.cloudinary.com/smartrooms/rooms/' . $room->image .'.jpg' : '' }}" data-max-file-size="5M" data-allowed-formats="landscape" data-allowed-file-extensions="jpg jpeg png" />
              </div>

            </div>
      </div>
    </div> <!-- end col -->

    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="user"></i> Users</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @if (isset ($room->users) && $room->users->count ())
                  @foreach ($room->users->sortBy('display') AS $room_user)
                    <div class="string-check string-check-bordered-primary mb-2">
                      {!! Form::checkbox ('users[]', $room_user->_id, 1, ['id' => 'room-user-'.$room_user->_id, 'class' => 'form-check-input']) !!}
                        <label class="string-check-label" for="room-user-{{ $room_user->_id }}">
                            <span class="ml-2">{{ $room_user->display }} ({{ $room_user->full_name }})</span>
                        </label>

                    </div>
                  @endforeach
                @endif
              </div>

            </div>
        </div>
    </div> <!-- end col -->

  </div> <!-- end roo -->


  <hr />
  <div class="row mb-4">
    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}

  @if ( isset ($room) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $room,
      'text'    => 'Room',
      'destroy' => [
        'route' => 'explorer.operators.brands.properties.buildings.rooms.destroy',
        'params'=> [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $room->_id],
      ],
      'return'  => [
        'route' => 'explorer.operators.brands.properties.buildings.index',
        'params'=> [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug],
      ],
    ])
  @endif


@endsection
