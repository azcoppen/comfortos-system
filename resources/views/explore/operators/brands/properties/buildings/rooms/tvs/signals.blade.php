@extends ('explore.operators.brands.properties.buildings.rooms.signals')
@section ('signals')

  @include ('explore.operators.brands.properties.buildings.rooms.component_signals', ['component' => $component, 'attributes' => ['brightness', 'channel', 'contrast', 'keycode', 'power', 'sharpness', 'temperature', 'volume']])

@stop
