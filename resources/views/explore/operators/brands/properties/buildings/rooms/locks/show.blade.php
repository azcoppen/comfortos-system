@extends ('explore.operators.brands.properties.buildings.rooms.device')

@section ('codes')
  <div class="row">

    <h3 class="col-md-12 mb-4">codes</h3>

    @livewire('codes-table', ['component' => $component])

  </div>
@stop

@section ('controls')
  <div class="row">

    <h3 class="col-md-12 mb-4">controls</h3>

    @include ('partials.components.widgets.control.help')

    @livewire('controls.lock', ['component' => $component])

    @livewire('controls.insert-code', ['component' => $component])

    @livewire('controls.wipe-code', ['component' => $component])

  </div>

@stop
