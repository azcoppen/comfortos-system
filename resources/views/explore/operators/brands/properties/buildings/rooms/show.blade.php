@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ Str::limit($menu_building->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="text-primary">[{{ $menu_room->floor }}]-{{ $menu_room->number }}</a>
          </div>
      </div>
      <div class="page-header-action">
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.hab_config', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="settings"></i> HAB Config</a>
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.feed', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="activity"></i> Data Stream</a>
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.edit', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="edit"></i> Manage Room</a>
      </div>

  </div>

@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@push('ws')
  <meta name="ws_host" content="{{ $ws_broker->wss_url ?? null }}/{{ $ws_broker->conn_endpoint ?? '' }}" />
  <meta name="ws_token" content="{{ $centrifugo_room_token ?? null }}" />
  <meta name="ws_user" content="{{ $room->users->first()->_id ?? null }}" />
  <meta name="ws_channel" content="{{ $room->_id ?? null }}" />
@endpush

@section('content')

  <div class="row">


    <div class="col-md-10">
      <div class="card card-margin">
          <div class="card-body">

            <div class="row">

              <h1 class="display-3 col-sm-9">
                <span class="badge badge-info">{{ $room->floor }}</span> <small class="text-muted">{{ $room->number }}</small> {{ $room->label ?? 'Room #'.$room->number }}</span>
              </h1>


              <div class="col-sm-3 text-right">
                <div class="dropdown float-right">
                    <button class="btn btn-success shadow-none dropdown-toggle" type="button" id="add-component" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i data-feather="plus-circle"></i> New...
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="add-component">
                      @foreach ([
                        'bulbs' => 'Bulb',
                        'cameras' => 'Camera',
                        'chromecasts' => 'Chromecast',
                        'dimmers' => 'Dimmer',
                        'leds' => 'LED',
                        'locks' => 'Lock',
                        'motors' => 'Motor',
                        'plugs' => 'Plug',
                        'sensors' => 'Sensor',
                        'speakers' => 'Speaker',
                        'stbs' => 'Set-top Box',
                        'thermostats' => 'Thermostat',
                        'tvs' => 'TV',
                        'wifi-networks' => 'Wifi Network',
                        ] AS $cpt_add_group => $cpt_text)
                        <li class="dropdown-item">
                          <a class="dropdown-link" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.'.$cpt_add_group.'.create', [$room->building->property->brand->operator->slug, $room->building->property->brand->slug, $room->building->property->slug, $room->building->slug, $room->_id]) }}">
                              <i data-feather="{{ config('comfortos.component_icons.'.Str::singular($cpt_add_group == 'wifi-networks' ? 'wifi' : $cpt_add_group)) }}"></i>
                            New {{ $cpt_text }}
                          </a>
                        </li>
                      @endforeach
                    </ul>
                </div>


                <div class="dropdown mr-1 float-right">
                    <button class="btn btn-warning shadow-none dropdown-toggle" type="button" id="provision-component" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i data-feather="log-in"></i> Provision...
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="provision-component">
                      @foreach ([
                        'dects' => 'DECT Base Station',
                        'extensions' => 'VoIP Extension',
                        'handsets' => 'Phone Handset',
                        'mirrors' => 'Smart Mirror',
                        'routers' => 'Router',
                        ] AS $cpt_add_group => $cpt_text)
                        <li class="dropdown-item">
                          <a class="dropdown-link" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.'.$cpt_add_group.'.create', [$room->building->property->brand->operator->slug, $room->building->property->brand->slug, $room->building->property->slug, $room->building->slug, $room->_id]) }}">
                              <i data-feather="{{ config('comfortos.component_icons.'.Str::singular($cpt_add_group)) }}"></i>
                            Provision {{ $cpt_text }}
                          </a>
                        </li>
                      @endforeach
                    </ul>
                </div>
              </div>

            </div>


            <div class="row mt-3 d-none d-md-block d-lg-block">
              <div class="col-md-12">
                <div class="alert alert-bordered-light alert-dismissible fade show" role="alert">
            		    <i data-feather="activity" class="alert-icon text-dark"></i>
            			<span id="live-feed" class="alert-text text-dark">Waiting for stream connection...</span>
            			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            				<i data-feather="x" class="alert-close"></i>
            			</button>
            	</div>
              </div>
            </div>

            @livewire ('o-t-p.room-code', compact ('room'))

          </div>
      </div>
    </div>

    <div class="col-md-2">
      <div class="card card-margin">
          <div class="card-body text-center">

              <a class="mb-3" target="_blank" href="{{ route ('guest.room.login', [$room->_id, $otp]) }}">
              {!! QrCode::size(100)->generate(route ('guest.room.login', [$room->_id, $otp])); !!}
              </a>
              <br />
              <input type="text" class="form-control pt-0 pb-0 mt-4 text-center" style="font-size: 1rem;" placeholder="" min="1" max="999" required pattern="[0-9]+" value="{{ $otp }}" readonly>

          </div>
      </div>
    </div>


  </div>


  @if ( $room->habs->count() < 1 )
        <div class="row mt-1">
          <div class="col-sm-12">
            <div class="alert alert-dark" role="alert">
              <div class="alert-text text-warning col-sm-9">
                  <strong>This room does not have a HAB provisioned.</strong>
              </div>
              <div class="col-sm-3 text-right">
                <a class="btn btn-warning" href="{{ route('explorer.operators.brands.properties.buildings.rooms.hab_availability', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}"><i data-feather="plus-circle"></i> Provision HAB</a>
              </div>
            </div>
          </div>
        </div>
  @endif

  <div class="main-card mb-4 card">
    <div class="card-header" data-toggle="collapse" href="#hab-config">
        <h5 class="card-title"><i class="text-muted" data-feather="settings"></i> HAB Configuration Files</h5>
    </div>
    <div class="card-body collapse" id ="hab-config">
      @include ('explore.operators.brands.properties.buildings.rooms.openhab-config-table', ['room' => $room])
    </div>
  </div>


    <div class="main-card mb-4 card">
      <div class="card-header" data-toggle="collapse" href="#room-keys">
          <h5 class="card-title"><i class="text-muted" data-feather="zap"></i> Access Information: IDAM / MQTT / WS / API</h5>
      </div>
      <div class="card-body collapse" id ="room-keys">


        <form class="">
            <div class="form-row">
                <div class="col-md-6">
                    <label for="room_user_email" class="pl-2">Room User Login</label>
                    <div class="position-relative form-group">
                        <div class="">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                        <span class="input-group-text" id=""><i class="fa fa-envelope text-warning"></i></span>
                                </div>
                                {!! Form::email ('room_user_email', (is_object ($room->users->first()) ? $room->users->first()->email : ''), ['id' => 'room_user_email', 'class'=>'form-control', 'readonly']) !!}
                            </div>
                        </div>
                    </div> <!-- end form-group -->
                </div> <!-- end col -->
                <div class="col-md-6">
                    <label for="room_user_password" class="pl-2">Room User Password</label>
                    <div class="position-relative form-group">
                      <div class="">
                          <div class="input-group">
                              <div class="input-group-prepend">
                                      <span class="input-group-text" id=""><i class="fa fa-lock text-warning"></i></span>
                              </div>
                              {!! Form::text ('room_user_password', 'room-'.$menu_room->number, ['id' => 'room_user_password', 'class'=>'form-control', 'readonly']) !!}
                          </div>
                      </div>
                  </div> <!-- end form-group -->
                </div> <!-- end col -->
            </div> <!-- end form-row -->
            <div class="form-row">
                <div class="col-md-12">
                    <label for="room_user_ws_jwt" class="pl-2">Room User Websocket JWT Authentication</label>
                    <div class="position-relative form-group">

                        <div class="">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                        <span class="input-group-text" id=""><i class="fa fa-key text-warning"></i></span>
                                </div>
                                {!! Form::text ('room_user_ws_jwt', $centrifugo_room_token ?? null, ['id' => 'room_user_ws_jwt', 'class'=>'form-control', 'readonly']) !!}
                            </div>
                        </div>
                        <small class="text-muted">Connect to {{ $ws_broker->wss_url ?? null }}/{{ $ws_broker->conn_endpoint ?? '' }}</small>

                    </div> <!-- end form-group -->
                </div> <!-- end col -->
            </div> <!-- end form-row -->
            <div class="form-row">
                <div class="col-md-12">
                    <label for="room_mqtt" class="pl-2">Room MQTT Topic Hierarchy</label>
                    <div class="position-relative form-group">

                        <div class="">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                        <span class="input-group-text" id=""><i class="fa fa-chart-bar text-warning"></i></span>
                                </div>
                                {!! Form::text ('room_mqtt', $menu_property->_id .'/' .$menu_building->_id .'/'. $menu_room->_id.'/[group]/[object]/[state|command]', ['id' => 'room_mqtt', 'class'=>'form-control', 'readonly']) !!}
                            </div>
                        </div>

                    </div> <!-- end form-group -->
                </div> <!-- end col -->
            </div> <!-- end form-row -->

            <div class="form-row">
                <div class="col-md-12">
                    <label for="room_api" class="pl-2">ROOM REST API Endpoint</label>
                    <div class="position-relative form-group">

                        <div class="">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                        <span class="input-group-text" id=""><i class="fa fa-gem text-warning"></i></span>
                                </div>
                                {!! Form::text ('room_api', route ('api.rooms.show', $menu_room->_id), ['id' => 'room_api', 'class'=>'form-control', 'readonly']) !!}
                            </div>
                        </div>

                    </div> <!-- end form-group -->
                </div> <!-- end col -->
            </div> <!-- end form-row -->

        </form>
        </div>
    </div>


    @livewire('room-signals-summary-cards', ['room' => $room])

    @if ( isset ($room->component_map) && is_object ($room->component_map) )
      @foreach (['bulbs', 'cameras', 'chromecasts', 'dects', 'dimmers', 'extensions', 'handsets', 'leds', 'locks', 'mirrors', 'motors', 'plugs', 'routers', 'sensors', 'speakers', 'stbs', 'switches', 'thermostats', 'tvs', 'wifi'] AS $cpt_group)
        @if ( isset ($room->component_map->{$cpt_group}) && is_array ($room->component_map->{$cpt_group}) && count ($room->component_map->{$cpt_group}) )

          <h1 class="display-5 mt-4">
            <i class="text-warning" data-feather="{{ config('comfortos.component_icons.'.Str::singular($cpt_group)) }}"></i> {{ $cpt_group }}
            <small class="badge badge-info float-right">{{ count ($room->component_map->{$cpt_group}) }}</small>
          </h1>
          <hr />

          <div class="row">
            @foreach ($room->component_map->{$cpt_group} AS $component)
              <div class="col-md-4">
                <div class="card card-margin">
                  @livewire ('room-component-panel', ['room' => $room, 'group' => $cpt_group, 'object_id' => (string)$component])
                </div>
              </div>
            @endforeach
          </div>


        @endif
      @endforeach
    @endif

    <div class="row mb-4 mt-5">
      <div class="col-sm-6">
        @include ('partials.components.panels.mqtt_broker', ['broker' => $room->building->property->mqtt_brokers->first()])
      </div>

      <div class="col-sm-6">
        @include ('partials.components.panels.ws_broker', ['broker' => $room->building->property->ws_brokers->first()])
      </div>

    </div>



    @if ( $room->habs->count() > 0)

      <div class="row mb-4 mt-5">
        <div class="col-md-12">
          <a class="btn btn-danger" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.hab_availability', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}"><i data-feather="trash-2"></i> Unlink Provisioned HAB</a>
          &nbsp;<small class="text-muted"><i data-feather="alert-circle"></i> Careful: destroying this stop any signals or commands.</small>
        </div>
      </div>

    @endif

    <div class="row mt-3"></div>

@endsection

@push ('js')
    <script src="/js/ws.js"></script>
@endpush
