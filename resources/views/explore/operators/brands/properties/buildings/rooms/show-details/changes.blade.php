@if (isset($data) && is_object ($data))
  <div class="col-lg-12 card-margin">
      <div class="card">
          <div class="card-header">
              <h5 class="card-title">Event Changes By Date</h5>
          </div>
          <div class="card-body">

              <div class="table-responsive">
                <table id="" class="table table-hover" style="width:100%">
                    <thead>
                    <tr>
                      <th>Capability</th>
                      <th>Attribute</th>
                      <th class="text-center">Value</th>
                      <th>Recorded</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach ($data->take (10) AS $record)
                        <tr>
                          <td class="text-left">{{ Str::limit($record->capability, 16) }}</td>
                          <td class="text-left text-primary">{{ Str::limit($record->attribute, 12) }}</td>
                          <td class="text-center"><strong class="text-">{{ !is_string ($record->value) ? json_encode ($record->value) : $record->value ?? '' }}</strong></td>
                          <td>{{ $record->updated_at->timezone(auth()->user()->timezone)->format('M d Y H:i:s A') }} ({{ $record->updated_at->diffForHumans() }})</td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
              </div>

          </div>
      </div>
  </div>
@endif
