@extends ('explore.operators.brands.properties.buildings.rooms.device')

@section ('controls')
  <div class="row">

    <h3 class="col-md-12 mb-4">controls</h3>

    @include ('partials.components.widgets.control.help')

    @livewire('controls.power', ['component' => $component])

    @livewire('controls.change-mode', ['component' => $component, 'additional' => $component->options['modes']])

    @livewire('controls.change-volume', ['component' => $component])

    @livewire('controls.mute', ['component' => $component])

    @livewire('controls.player', ['component' => $component])

    @livewire('controls.change-preset', ['component' => $component])

  </div>


@stop

@push('js')
  <script>
  document.addEventListener("DOMContentLoaded", () => {

    window.addEventListener('mode.rendering', event => {
      $('#mode-{{ $component->_id }}').selectpicker('val', event.detail.last_value);
    })

    window.addEventListener('preset.rendering', event => {
      $('#preset-{{ $component->_id }}').selectpicker('val', event.detail.last_value);
    })

  });
  </script>
@endpush
