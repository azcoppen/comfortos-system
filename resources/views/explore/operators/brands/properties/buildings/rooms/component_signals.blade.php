@foreach ($attributes ?? [] AS $attribute)
<div class="row">

  <h3 class="col-md-12 mb-4"><i class="fa fa-{{ config ('comfortos.fa_attribute_icons.'.$attribute) }} text-warning"></i> {{ $attribute == 'bulb-power' ? 'power' : $attribute }}</h3>


  @if (! in_array ($attribute, ['mode']) )
    @livewire('graphs.component-signal-attribute-graph', ['component' => $component,  'group' => $group,  'attribute' => $attribute])
  @endif


  @livewire('signal-attribute-table', ['component' => $component, 'attribute' => $attribute])

</div>
@endforeach
