<div class="row">

  <div class="col-lg-12 card-margin">
      <div class="card">
          <div class="card-header">
              <h5 class="card-title col-md-9">{{ $title ?? 'Components' }} @if (is_object($records) && $records->count()) <span class="badge badge-soft-danger mt-0 ml-2">{{ $records->count() }}</span> @endif
                @if ( in_array ($group, ['dects', 'extensions', 'handsets', 'mirrors', 'routers', 'stbs',]) )
                <small class="text-muted">Must be provisioned from inventory database</small>
                @endif
              </h5>

                @if ( in_array ($group, ['dects', 'extensions', 'handsets', 'mirrors', 'routers', 'stbs',]) )

                  <div class="float-right col-md-3 text-right"><a class="btn btn-sm btn-warning" href="{{ route (str_replace('show', 'create', $route_path), $route_vars) }}">
                  Provision
                  {{ Str::singular ($title) }} <i data-feather="check-square"></i></a></div>
                @else
                  <div class="float-right col-md-3 text-right"><a class="btn btn-sm btn-success" href="{{ route (str_replace('show', 'create', $route_path), $route_vars) }}">
                  Add
                  {{ Str::singular ($title) }} <i data-feather="plus-circle"></i></a></div>
                @endif

          </div>
          <div class="card-body">

            @if (! is_object($records) || ! $records->count () )

                    <div class="alert alert-bordered-info" role="alert">
                      <i data-feather="alert-octagon" class="alert-icon"></i>
                      <span class="alert-text"><strong>Diagnostics:</strong> This room has no {{ $desc }} installed.</span>
                    </div>
            @else

                <div class="table-responsive">
                  <table style="width: 100%;" id="" class="table table-hover">
                     <thead>
                     <tr>
                         <th></th>
                         <th>Identifier</th>
                         <th>Label</th>
                         @if ( isset ($fields) && count ($fields) )
                           @foreach ($fields AS $field)
                             <th>{{ str_replace('Id', 'ID', Str::title (str_replace('_', ' ', $field))) }}</th>
                           @endforeach
                         @else
                           <th>{{ isset ($signals) && $signals !== TRUE ? '' : 'Attribute' }}</th>
                           <th>{{ isset ($signals) && $signals !== TRUE ? '' : 'Value' }}</th>
                           <th>{{ isset ($signals) && $signals !== TRUE ? '' : 'Last Signal' }}</th>
                           <th>{{ isset ($signals) && $signals !== TRUE ? '' : 'Last Command' }}</th>
                         @endif
                         <th>Created</th>
                     </tr>
                     </thead>
                     <tbody>


                       @foreach ($records AS $record)

                         @php ($last_signal = is_object ($record->last_signal) ? $record->last_signal : NULL)
                         @php ($last_command = is_object ($record->last_signal) ? $record->last_command : NULL)

                         <tr>
                             <td><i class="text-muted" data-feather="{{ $icon ?? '' }}"></i></td>
                             <td><a href="{{ route ($route_path,  array_merge ($route_vars, [$record->_id])) }}">{{ $record->_id }}</a></td>
                             <td>{!! $record->label ?? '<span class="text-muted">None</span>' !!}</td>
                             @if ( isset ($fields) && count ($fields) )
                               @foreach ($fields AS $field)
                                 @if ( Str::endsWith($field, '_at') && is_object ($record->{$field}) )
                                   <td>{{ $record->{$field}->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} <span class="text-muted">({{ $record->{$field}->diffForHumans() }})</span></td>
                                 @endif
                                 <td>{{ $record->{$field} }}</td>
                               @endforeach
                             @else
                               <td id="{{ $record->_id }}-attribute">{!! is_object ($last_signal) ? $last_signal->attribute : (isset ($signals) && $signals !== TRUE ? '' : '<span class="text-muted">None</span>') !!}</td>
                               <td id="{{ $record->_id }}-value">
                                 @if (is_object ($last_signal)  && $last_signal->group == 'routers')
                                   Data Object
                                 @else
                                   {!! is_object ($last_signal) ? (is_string ($last_signal->value) ? $last_signal->value : json_encode($last_signal->value)) : (isset ($signals) && $signals !== TRUE ? '' : '<span class="text-muted">None</span>') !!}
                                 @endif

                               </td>
                               <td id="{{ $record->_id }}-received">{!! is_object ($last_signal) ? $last_signal->received_at->timezone (auth()->user()->timezone)->format ('M d H:i:s A') : (isset ($signals) && $signals !== TRUE ? '' : '<span class="text-muted">None</span>')!!}</td>
                               <td id="{{ $record->_id }}-sent">{!! is_object ($last_command) ? $last_command->sent_at->timezone (auth()->user()->timezone)->format ('M d H:i:s A') : (isset ($signals) && $signals !== TRUE ? '' : '<span class="text-muted">None</span>') !!}</td>
                             @endif
                             <td>{{ $record->created_at->timezone (auth()->user()->timezone)->format ('M d H:i A') }}</td>
                         </tr>

                       @endforeach
                     </tbody>
                   </table>
                 </div> <!-- end table responsive -->


            @endif
      </div> <!-- end card body -->
    </div> <!-- end card -->
  </div> <!-- end card -->
</div> <!-- end row -->
