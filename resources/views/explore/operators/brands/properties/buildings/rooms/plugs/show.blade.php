@extends ('explore.operators.brands.properties.buildings.rooms.device')

@section ('controls')
  <div class="row">

    <h3 class="col-md-12 mb-4">controls</h3>

    @include ('partials.components.widgets.control.help')

    @livewire('controls.power', ['component' => $component])

    @livewire('controls.change-level', ['component' => $component])

  </div>


@stop
