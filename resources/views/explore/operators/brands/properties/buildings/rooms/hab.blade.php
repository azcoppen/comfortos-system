@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ Str::limit($menu_building->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}">[{{ $menu_room->floor }}]-{{ $menu_room->number }}</a>
            <a href="" class="text-primary">HAB</a>
          </div>
      </div>
      <div class="page-header-action">
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">room #{{ $menu_room->number }} / hab /</span> {{ $menu_room->habs->count() > 0 ? 'Deprovision Room HAB' : 'Provision New HAB' }}</h1>
  <hr />

  @if ( $menu_room->habs->count() > 0 )
    <!-- WARNING: ONLY ONE DEVICE PER ROOM -->
    {!! Form::open (['method' => 'PUT', 'route' => ['explorer.operators.brands.properties.buildings.rooms.hab_deprovisioning', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]]]) !!}

    <div class="row">
      <div class="col-sm-12">
        <div class="alert alert-warning" role="alert">
          <div class="alert-text">
            <strong>CAREFUL! When you deprovision a HAB, no more commands or signal communication can be processed.</strong>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">

        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="box"></i> Provisioned HAB</h5>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Identifier</th>
                      <th>Serial</th>
                      <th>Model</th>
                      <th>Label</th>
                      <th>Created</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($room->habs AS $entity)
                      <tr>
                        <td>
                          <div class="string-check string-check-soft-danger mb-2">
                              {!! Form::radio ('habs[]', $entity->_id, true, ['id' => $entity->_id, 'class' => 'form-check-input']) !!}
                              <label class="string-check-label" for="{{ $entity->_id }}">
                              </label>
                          </div>
                        </td>
                        <td>{{ $entity->_id }}</td>
                        <td>{{ $entity->serial }}</td>
                        <td>{{ $entity->model }}</td>
                        <td>{{ $entity->label }}</td>
                        <td>{{ $entity->created_at->timezone (auth()->user()->timezone)->format ('M d Y H:iA') }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </div>

      </div>
    </div>
  @else
      <!-- CREATE NEW -->
      {!! Form::open (['method' => 'PUT', 'route' => ['explorer.operators.brands.properties.buildings.rooms.hab_provisioning', $menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]]) !!}

        <div class="row">
          <div class="col-md-12">

            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="box"></i> Available <span class="text-muted"> / Unprovisioned</span> HABs</h5>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Identifier</th>
                          <th>Serial</th>
                          <th>Model</th>
                          <th>Label</th>
                          <th>Created</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($available AS $entity)
                          <tr>
                            <td>
                              <div class="string-check string-check-bordered-primary mb-2">
                                  {!! Form::radio ('habs[]', $entity->_id, false, ['id' => $entity->_id, 'class' => 'form-check-input']) !!}
                                  <label class="string-check-label" for="{{ $entity->_id }}">
                                  </label>
                              </div>
                            </td>
                            <td>{{ $entity->_id }}</td>
                            <td>{{ $entity->serial }}</td>
                            <td>{{ $entity->model }}</td>
                            <td>{{ $entity->label }}</td>
                            <td>{{ $entity->created_at->timezone (auth()->user()->timezone)->format ('M d Y H:iA') }}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

          </div>
        </div>
    @endif



    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}



@stop
