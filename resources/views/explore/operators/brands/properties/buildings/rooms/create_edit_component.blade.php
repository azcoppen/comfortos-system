@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ Str::limit($menu_building->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}">[{{ $menu_room->floor }}]-{{ $menu_room->number }}</a>
            @if ( isset ($component) )
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}">{{ Str::limit($component->label, 10) }}</a>
            @endif
            @if ( in_array ($group, ['wifi-networks']) )
            @else
              <a href="" class="text-primary">{{ isset ($component) ? 'Manage' : 'Create '.Str::title(Str::singular ($group)) }}</a>
            @endif

          </div>
      </div>
      <div class="page-header-action">
        @if ( isset ($component) )
          @if ( in_array ($group, ['bulbs', 'cameras', 'chromecasts', 'leds', 'locks', 'mirrors', 'motors', 'plugs', 'routers', 'speakers', 'switches', 'thermostats', 'tvs']) )
          <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.feed.commands', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="activity"></i> Command Stream</a>
          @endif
          <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.feed.signals', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="activity"></i> Signal Stream</a>
          <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
        @else
          <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
        @endif
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">room #{{ $menu_room->number }} / {{ $group }} /</span> {{ isset ($component) ? 'Manage '.$component->label : 'Add A New '.Str::title(Str::singular ($group)) }}</h1>
  <hr />

  @if ( !isset ($component) )
    @if ( in_array ($group, ['speakers', 'thermostats',]) )
    <div class="row">
      <div class="col-sm-12">
        <div class="alert alert-dark" role="alert">
          <div class="alert-text">
            @if (head(config ('comfortos.options.'.$group)))
              This kind of component requires {{ count (head(config ('comfortos.options.'.$group))) }} sets of options configured. You can do this after you create it and specify which type it is.
            @endif
          </div>
        </div>
      </div>
    </div>
    @endif
  @endif

  @if ( isset ($component) )
    @if ( in_array ($group, ['speakers', 'thermostats',]) )
      @if (head(config ('comfortos.options.'.$group)) && !isset($component->options))
        <div class="row">
          <div class="col-sm-12">
            <div class="alert alert-warning" role="alert">
              <div class="alert-text">
                @if (head(config ('comfortos.options.'.$group)))
                  <strong>This kind of component requires {{ count (head(config ('comfortos.options.'.$group))) }} sets of options configured. They are not set.</strong>
                @endif
              </div>
            </div>
          </div>
        </div>
      @endif
    @endif
  @endif

  @if ( isset ($component) )
    {!! Form::open (['method' => 'PUT', 'route' => ['explorer.operators.brands.properties.buildings.rooms.'.$group.'.update', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]]]) !!}
  @else
    {!! Form::open (['route' => ['explorer.operators.brands.properties.buildings.rooms.'.$group.'.store', $menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]]) !!}
  @endif


  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $component->label ?? null,
                  'default'     => '',
                  'placeholder' => Str::title(Str::singular ($group))." 01",
                  'required'    => true,
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "Which type of ".Str::singular ($group)." is it?",
                  'name'        => 'type',
                  'value'       => $component->type ?? null,
                  'default'     => '',
                  'placeholder' => "Provide its type",
                  'required'    => true,
                ])
              </div>

              @if ( $group == 'sensors' )
                <div class="form-group">
                  @include ('components.fields.text', [
                    'label'       => "Which variant of ".Str::singular ($group)." is it?",
                    'name'        => 'variant',
                    'value'       => $component->variant ?? null,
                    'default'     => '',
                    'placeholder' => "Provide its type",
                    'required'    => false,
                    'help'        => 'Specify the sub-category of the component here.'
                  ])
                </div>
              @endif

              <div class="form-group">
                @include ('components.fields.select', [
                  'label'       => "Which driver does this ".Str::singular ($group)." use?",
                  'name'        => 'driver',
                  'options'     => ['openhab' => 'openhab', 'hue' => 'hue (api)', 'routeros' => 'routeros', 'smartthings' => 'smartthings'],
                  'value'       => $component->driver ?? 'openhab',
                  'default'     => 'openhab',
                  'placeholder' => "Provide its driver",
                  'required'    => true,
                ])
              </div>

            </div>
          </div>
        </div> <!-- end col -->


        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="grid"></i> HAB Items</h5>
                </div>
                <div class="card-body">

                  @if ( isset ($commands) && count ($commands) )

                    @for ($i = 0; $i < count ($commands); $i++)

                      <div class="position-relative row form-group">
                        @include ('components.fields.text', [
                          'columns'     => true,
                          'colwidth'    => 3,
                          'label'       => $commands[$i],
                          'name'        => 'items['.$commands[$i].']',
                          'value'       => $component->items[$commands[$i]] ?? strtolower(Str::singular($group))."_n_".$commands[$i],
                          'default'     => strtolower(Str::singular($group))."_n_".$commands[$i],
                          'placeholder' => strtolower(Str::singular($group))."_n_".$commands[$i],
                          'required'    => false,
                          'help'        => 'The HAB item configured to receive '.$commands[$i]. ' commands.',
                        ])
                      </div>

                    @endfor

                  @else
                    <div class="alert alert-warning" role="alert">
                      <span class="alert-text">No commands available</span>
                    </div>
                  @endif

                </div>
              </div>
            </div> <!-- end col -->


      </div> <!-- end row -->

  @if ( isset ($component) && config ('comfortos.options.'.$group.'.'.$component->type) )
    <div class="row">

      @foreach (config ('comfortos.options.'.$group.'.'.$component->type) AS $cmd => $options)

      <div class="col-lg-4 col-md-4 ">
          <div class="card card-margin">
              <div class="card-header">
                  <h5 class="card-title"><i data-feather="settings"></i> <span class="text-muted">Options:</span> {{ str_replace ('_', ' ', $cmd) }}</h5>
              </div>
              <div class="card-body">
                @foreach ( $options AS $option => $default_value )

                  <div class="position-relative row form-group">
                    @include ('components.fields.text', [
                      'columns'     => true,
                      'colwidth'    => 5,
                      'label'       => $option,
                      'name'        => 'options['.$cmd.']['.$option.']',
                      'value'       => $component->options[$cmd][$option] ?? $default_value,
                      'default'     => $default_value,
                      'placeholder' => $default_value,
                      'required'    => true,
                      'help'        => 'Value to set '.Str::singular ($cmd).' = '.$option.'.',
                    ])
                  </div>

                @endforeach
              </div>
            </div>
       </div> <!-- end col -->

      @endforeach

    </div>
  @endif


  <div class="row">

    @include ('partials.components.forms.ordering', [
      'entity'  => $component ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.installation', [
      'entity'  => $component ?? null,
      'users'   => $users,
    ])

    @include ('partials.components.forms.provisioning', [
      'entity'  => $component ?? null,
      'users'   => $users,
    ])


  </div> <!-- end row -->

  <hr />
  <div class="row mb-4">
    @if ( !isset ($component) )
      @include ('partials.components.forms.clone')
    @endif

    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}


  @if ( isset ($component) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $component,
      'text'    => Str::title(Str::singular ($group)),
      'destroy' => [
        'route' => 'explorer.operators.brands.properties.buildings.rooms.'.$group.'.destroy',
        'params'=> [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id],
      ],
      'return'  => [
        'route' => 'explorer.operators.brands.properties.buildings.rooms.show',
        'params'=> [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id],
      ],
    ])
  @endif

@stop
