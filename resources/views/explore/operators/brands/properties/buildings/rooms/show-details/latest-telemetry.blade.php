@if (isset($data) && is_object ($data))
  @foreach ($data->unique('attribute')->sortBy('attribute') AS $record)
    <div class="col-lg-2 col-md-6 col-sm-6">
        <div class="card card-margin">
            <div class="card-body">
                <div class="widget-31">
                    <div class="widget-31-icon">
                        <i data-feather="{{ config ('smartthings.v1.capability_icons.'.$record->attribute) }}"></i>
                    </div>
                    <div class="widget-31-data">
                        <span class="title">{{ $record->attribute }}</span>
                        <h5 class="figure text-primary">
                          @switch(gettype($record->value))
                            @case ('NULL')
                            <span class="text-muted">Empty</span>
                            @break
                            @case ('string')
                            @case ('integer')
                            {{ $record->value }}
                            @break
                            @case ('array')
                            {{ json_encode ($record->value) }}
                            @break
                            @case ('object')
                            Object
                            @break
                            @default
                            {{ $record->value }}
                            @break
                          @endswitch
                          </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @endforeach
@endif
