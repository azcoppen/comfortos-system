@extends ('explore.operators.brands.properties.buildings.rooms.signals')

@section ('signals')

  @include ('explore.operators.brands.properties.buildings.rooms.component_signals', ['component' => $component, 'attributes' => ['mode', 'mute', 'power', 'preset', 'volume', 'http']])

@stop
