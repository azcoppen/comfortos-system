@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ Str::limit($menu_building->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}">[{{ $menu_room->floor }}]-{{ $menu_room->number }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="text-primary">{{ Str::limit($component->label, 10) }}</a>
          </div>
      </div>
      <div class="page-header-action">
        @if ( in_array ($group, ['bulbs', 'cameras', 'chromecasts', 'leds', 'locks', 'mirrors', 'motors', 'plugs', 'routers', 'speakers', 'switches', 'thermostats', 'tvs']) )
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.feed.commands', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="activity"></i> Commands</a>
        @endif
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.feed.signals', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="activity"></i> Signals</a>
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.edit', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="edit"></i> Manage</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@push('ws')
  <meta name="ws_host" content="{{ $ws_broker->wss_url ?? null }}/{{ $ws_broker->conn_endpoint ?? '' }}" />
  <meta name="ws_token" content="{{ $centrifugo_room_token ?? null }}" />
  <meta name="ws_user" content="{{ $room->users->first()->_id ?? null }}" />
  <meta name="ws_channel" content="{{ $room->_id ?? null }}" />
@endpush

@section('content')

  <div class="row mb-3">
    <div class="col-md-10">

      <h1 class="display-3"><span class="text-muted">{{ $group }} / </span> {{ $component->label ?? $component->_id }} <span class="text-muted">@ {{ $component->room->label }}</span></h1>

      <div class="row d-none d-md-block d-lg-block">
          <div class="col-md-12">
            @include ('partials.components.panels.component_feed', ['component' => $component]);
          </div>

          <div class="col-md-12">
            @livewire ('o-t-p.component-code', compact ('component'))
          </div>
      </div>

    </div>

    <div class="col-md-2 text-center pt-2">
      <a target="_blank" href="{{ route ('guest.component.'.Str::lower(Str::singular($component->plural)).'.login', [$component->_id, $otp]) }}">
        {!! QrCode::size(100)->generate(route ('guest.component.'.Str::lower(Str::singular($component->plural)).'.login', [$component->_id, $otp])); !!}
      </a>
      <input type="text" class="form-control pt-0 pb-0 mt-4 text-center" style="font-size: 1.4rem;" placeholder="" min="1" max="999" required pattern="[0-9]+" value="{{ $otp }}" readonly>
    </div>

  </div> <!-- end row -->



  @if ( $component->room->habs->count() < 1 )
        <div class="row">
          <div class="col-sm-12">
            <div class="alert alert-warning" role="alert">
              <div class="alert-text">
                  <strong>This kind of component must be connected to a HAB. The room does not have a HAB provisioned.</strong>
              </div>
            </div>
          </div>
        </div>
  @endif

  @if ( isset ($component) )
    @if ( in_array ($group, ['speakers', 'thermostats',]) )
      @if (head(config ('comfortos.options.'.$group)) && !isset($component->options))
        <div class="row">
          <div class="col-sm-12">
            <div class="alert alert-warning" role="alert">
              <div class="alert-text">
                @if (head(config ('comfortos.options.'.$group)))
                  <strong>This kind of component requires {{ count (head(config ('comfortos.options.'.$group))) }} sets of options configured. They are not set.</strong>
                @endif
              </div>
            </div>
          </div>
        </div>
      @endif
    @endif
  @endif

  <div class="row">
      <div class="col-lg-3 card-margin">
          <div class="card">
              <div class="card-body text-center">
                  <img src="https://res.cloudinary.com/smartrooms/devices/{{ ($group == 'sensors' ? $component->variant .'-' : '') . Str::singular ($group) }}.png" style="max-height: 13rem; max-width: 100%; margin: auto;" align="center" />
                  <br /><br /> <a href="">{{ $component->_id }}</a>
              </div>
          </div>
      </div>

      <div class="col-lg-5 card-margin">
        @include ('partials.components.panels.last_signal', ['component' => $component])
      </div>


    @include ('partials.provisioning', [
      'room' => $component->room,
      'item' => $component,
    ])

    </div>

    @yield ('codes')

    @yield ('controls')

    <div class="row mt-3">
      @livewire('graphs.component-signals-throughput-hourly-graph', ['component' => $component])
    </div>

    <div class="row mt-3">
      @livewire('graphs.component-signals-throughput-graph', ['component' => $component])
    </div>

    <div class="row">

      <h3 class="col-md-12 mb-4">latest signals</h3>

      @if ( isset($component->signals) && is_object ($component->signals) && $component->signals->count() )

          @livewire('signals-table', ['component' => $component->_id, 'limit' => 10])

      @else
        <div class="col-md-12">
          <div class="alert alert-bordered-info" role="alert">
            <i data-feather="alert-octagon" class="alert-icon"></i>
            <span class="alert-text"><strong>Diagnostics:</strong> No telemetry available.</span>
          </div>
        </div>
      @endif

    </div>

    @if ( in_array ($group, ['bulbs', 'cameras', 'chromecasts', 'leds', 'locks', 'mirrors', 'motors', 'plugs', 'routers', 'speakers', 'switches', 'thermostats', 'tvs']) )
      <div class="row mt-3">
        @livewire('graphs.component-commands-throughput-hourly-graph', ['component' => $component])
      </div>

      <div class="row mt-3">
        @livewire('graphs.component-commands-throughput-graph', ['component' => $component])
      </div>

      <div class="row">

        <h3 class="col-md-12 mb-4">latest commands</h3>

        @if ( isset($component->commands) && is_object ($component->commands) && $component->commands->count() )

            @livewire('commands-table', ['component' => $component->_id, 'limit' => 10])

        @else
          <div class="col-md-12">
            <div class="alert alert-bordered-info" role="alert">
              <i data-feather="alert-octagon" class="alert-icon"></i>
              <span class="alert-text"><strong>Diagnostics:</strong> No telemetry available.</span>
            </div>
          </div>
        @endif

      </div>
    @endif

    @if ( $component->driver && isset ($component->items) && count ($component->items) )
      <div class="row">
        <h3 class="col-md-12 mb-4">configuration</h3>
        <div class="col-lg-12 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">HAB Items</h5>
              </div>

              <div class="card-body">
                  @include ('partials.components.panels.hab_driver', ['component' => $component])
              </div>
          </div>
        </div>

        @if ( $component->driver && isset ($component->options) && count ($component->options) )

            @foreach (config ('comfortos.options.'.$group.'.'.$component->type) AS $cmd => $options)

            <div class="col-lg-4 col-md-4 ">
                <div class="card card-margin">
                    <div class="card-header">
                        <h5 class="card-title">{{ str_replace ('_', ' ', $cmd) }}</h5>
                    </div>
                    <div class="card-body">
                      <small class="text-muted">These values will be received by the HAB when the {{ Str::singular($cmd) }} option is changed.</small>
                      <table class="table table-sm table-hover mt-3">
                        <thead>
                          <tr>
                            <th>Option</th>
                            <th>Value</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ( $options AS $option => $default_value )
                            <tr>
                              <td>{{ $option ?? '' }}</td>
                              <td>{{ $component->options[$cmd][$option] ?? '' }}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
             </div> <!-- end col -->

            @endforeach

        @endif

        @include ('partials.components.panels.mqtt_topics', ['property' => $menu_property, 'building' => $menu_building, 'room' => $menu_room, 'group' => $group, 'component' => $component])
      </div>
    @endif

    <div class="row mb-4">
      <div class="col-sm-6">
        @include ('partials.components.panels.mqtt_broker', ['broker' => $component->room->building->property->mqtt_brokers->first()])
      </div>

      <div class="col-sm-6">
        @include ('partials.components.panels.ws_broker', ['broker' => $component->room->building->property->ws_brokers->first()])
      </div>
    </div>

    @include ('partials.components.panels.device_business_flow', ['component' => $component])


@stop

@push ('js')
    <script src="/js/ws.js"></script>

    <script>

      document.addEventListener('{{ $component->_id }}', function (e) {
        console.log (e);
      });

      Livewire.hook('component.initialized', (el, component) => {
        feather.replace();
      })
      Livewire.hook('element.updated', (el, component) => {
        feather.replace();
      })
    </script>
@endpush
