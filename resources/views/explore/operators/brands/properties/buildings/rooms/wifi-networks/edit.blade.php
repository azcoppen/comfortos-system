@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
              <a href="{{ route('explorer.operators.brands.show', [$menu_operator->slug, $menu_brand->slug]) }}"><i data-feather="map-pin"></i> {{ $menu_brand->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ $menu_property->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.buildings.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ $menu_building->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="text-primary">Room #{{ $menu_room->number }}</a>
          </div>
      </div>
      <div class="page-header-action">
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">room #{{ $menu_room->number }} / {{ $group }} /</span> Manage {{ $component->label ?? 'Network' }}</h1>
  <hr />


  <div class="row">
    <div class="col-sm-12">
      <div class="alert alert-warning" role="alert">
        <div class="alert-text">
            <strong class="text-light">Operations on a router send a remote command which may take up to 30 seconds to complete. The record will auto-update in the background when the command has completed successfully. After submitting your change, check the network status again in 30-60 seconds.</strong>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">
              {!! Form::open (['method' => 'PATCH', 'route' => ['explorer.operators.brands.properties.buildings.rooms.'.$group.'.update', $menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]]) !!}

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => $component->label,
                  'default'     => '',
                  'placeholder' => "Wifi Network",
                  'required'    => true,
                  'help'        => "This is the sytem name, NOT the wifi network name (SSID).",
                ])
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.datepicker', [
                          'label'       => "What day should it launch?",
                          'name'        => 'launch_at_date',
                          'value'       => isset ($component->launch_at) && is_object ($component->launch_at) ? $component->launch_at->timezone(auth()->user()->timezone)->format ('m/d/Y') : null,
                          'default'     => now()->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'placeholder' => now()->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'disabled'    => true,
                          'help'        => 'This cannot be changed after it the SSID has been launched.',
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.timepicker', [
                          'label'       => "What time should it launch?",
                          'name'        => 'launch_at_time',
                          'value'       => isset ($component->launch_at) && is_object ($component->launch_at) ? $component->launch_at->timezone(auth()->user()->timezone)->format ('H:i:s') : null,
                          'default'     => now()->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'placeholder' => now()->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'disabled'    => true,
                          'help'        => 'This cannot be changed after it the SSID has been launched.',
                        ])
                      </div>
                  </div>
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.datepicker', [
                          'label'       => "What day should it expire?",
                          'name'        => 'expire_at_date',
                          'value'       => isset ($component->expire_at) && is_object ($component->expire_at) ? $component->expire_at->timezone(auth()->user()->timezone)->format ('m/d/Y') : null,
                          'default'     => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'placeholder' => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'required'    => false,
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.timepicker', [
                          'label'       => "What time should it expire?",
                          'name'        => 'expire_at_time',
                          'value'       => isset ($component->expire_at) && is_object ($component->expire_at) ? $component->expire_at->timezone(auth()->user()->timezone)->format ('H:i:s') : null,
                          'default'     => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'placeholder' => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'required'    => false,
                        ])
                      </div>
                  </div>
              </div>

              <hr />
              <div class="row mb-4">
                <div class="col-lg-12 col-md-12 text-right">
                  @include ('components.buttons.submit')
                  @include ('components.buttons.cancel')
                </div>
              </div> <!-- end row -->
              {!! Form::close () !!}

            </div>
          </div>
        </div> <!-- end col -->


        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="wifi"></i> SSID</h5>
                </div>
                <div class="card-body">
                  {!! Form::open (['method' => 'PATCH', 'route' => ['explorer.operators.brands.properties.buildings.rooms.'.$group.'.update', $menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]]) !!}

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the visible wifi network name (SSID)?",
                      'name'        => 'ssid',
                      'value'       => $component->ssid,
                      'default'     => 'MyNewNetwork-'.uniqId(),
                      'placeholder' => "MyNewNetwork",
                      'required'    => false,
                      'help'        => "Do not include spaces or special characters."
                    ])
                  </div>

                  <hr />
                  <div class="row mb-4">
                    <div class="col-lg-12 col-md-12 text-right">
                      @include ('components.buttons.submit')
                      @include ('components.buttons.cancel')
                    </div>
                  </div> <!-- end row -->
                  {!! Form::close () !!}

                </div>
              </div>

              <div class="card card-margin">
                  <div class="card-header">
                      <h5 class="card-title"><i data-feather="lock"></i> Password</h5>
                  </div>
                  <div class="card-body">
                    {!! Form::open (['method' => 'PATCH', 'route' => ['explorer.operators.brands.properties.buildings.rooms.'.$group.'.update', $menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]]) !!}

                    <div class="form-group">
                      @include ('components.fields.text', [
                        'label'       => "What is the network password?",
                        'name'        => 'password',
                        'value'       => $component->password,
                        'default'     => Str::random (12),
                        'placeholder' => "mypassword",
                        'required'    => false,
                        'help'        => 'Minimum of 8 characters.',
                      ])
                    </div>

                    <hr />
                    <div class="row mb-4">
                      <div class="col-lg-12 col-md-12 text-right">
                        @include ('components.buttons.submit')
                        @include ('components.buttons.cancel')
                      </div>
                    </div> <!-- end row -->
                    {!! Form::close () !!}

                  </div>
                </div>


            </div> <!-- end col -->


      </div> <!-- end row -->




  @if ( isset ($component) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $component,
      'text'    => 'Network',
      'destroy' => [
        'route' => 'explorer.operators.brands.properties.buildings.rooms.'.$group.'.destroy',
        'params'=> [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id],
      ],
      'return'  => [
        'route' => 'explorer.operators.brands.properties.buildings.rooms.show',
        'params'=> [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id],
      ],
    ])
  @endif

@endsection
