@extends ('explore.operators.brands.properties.buildings.rooms.device')

@section ('controls')
  <div class="row">

    <h3 class="col-md-12 mb-4">controls</h3>

    @include ('partials.components.widgets.control.help')

    @livewire('controls.power', ['component' => $component])

    @livewire('controls.change-channel', ['component' => $component])

    @livewire('controls.change-keycode', ['component' => $component])

    @livewire('controls.change-volume', ['component' => $component])

    @livewire('controls.change-brightness', ['component' => $component])

    @livewire('controls.change-contrast', ['component' => $component])

    @livewire('controls.change-sharpness', ['component' => $component])

    @livewire('controls.change-color-temperature', ['component' => $component])

    @livewire('controls.mute', ['component' => $component])

  </div>


@stop
