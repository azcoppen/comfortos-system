@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
              <a href="{{ route('explorer.operators.brands.show', [$menu_operator->slug, $menu_brand->slug]) }}"><i data-feather="map-pin"></i> {{ $menu_brand->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ $menu_property->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.buildings.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ $menu_building->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="text-primary">Room #{{ $menu_room->number }}</a>
          </div>
      </div>
      <div class="page-header-action">
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">room #{{ $menu_room->number }} / {{ $group }} /</span> {{ isset ($component) ? 'Manage '.$component->label : 'Add A New Network' }}</h1>
  <hr />

{!! Form::open (['route' => ['explorer.operators.brands.properties.buildings.rooms.'.$group.'.store', $menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]]) !!}

  <div class="row">
    <div class="col-sm-12">
      <div class="alert alert-warning" role="alert">
        <div class="alert-text">
            <strong class="text-light">Operations on a router send a remote command which may take up to 30 seconds to complete. The record will auto-update in the background when the command has completed successfully. After submitting your change, check the network status again in 30-60 seconds.</strong>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-6 col-md-6 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="cpu"></i> Basics</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.text', [
                  'label'       => "What should it be called?",
                  'name'        => 'label',
                  'value'       => null,
                  'default'     => '',
                  'placeholder' => "Wifi Network",
                  'required'    => true,
                  'help'        => "This is the sytem name, NOT the wifi network name (SSID).",
                ])
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.datepicker', [
                          'label'       => "What day should it launch?",
                          'name'        => 'launch_at_date',
                          'value'       => null,
                          'default'     => now()->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'placeholder' => now()->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'required'    => true,
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.timepicker', [
                          'label'       => "What time should it launch?",
                          'name'        => 'launch_at_time',
                          'value'       => null,
                          'default'     => now()->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'placeholder' => now()->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'required'    => true,
                        ])
                      </div>
                  </div>
              </div>

              <div class="form-row">
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.datepicker', [
                          'label'       => "What day should it expire?",
                          'name'        => 'expire_at_date',
                          'value'       => null,
                          'default'     => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'placeholder' => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('m/d/Y'),
                          'required'    => true,
                        ])
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="position-relative form-group">
                        @include ('components.fields.timepicker', [
                          'label'       => "What time should it expire?",
                          'name'        => 'expire_at_time',
                          'value'       => null,
                          'default'     => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'placeholder' => now()->addYears(10)->timezone (auth()->user()->timezone)->format ('H:i:s'),
                          'required'    => true,
                        ])
                      </div>
                  </div>
              </div>


            </div>
          </div>
        </div> <!-- end col -->


        <div class="col-lg-6 col-md-6 ">
            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="box"></i> Configuration</h5>
                </div>
                <div class="card-body">

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the visible wifi network name (SSID)?",
                      'name'        => 'ssid',
                      'value'       => null,
                      'default'     => 'MyNewNetwork-'.uniqId(),
                      'placeholder' => "MyNewNetwork",
                      'required'    => true,
                      'help'        => "Do not include spaces or special characters."
                    ])
                  </div>

                  <div class="form-group">
                    @include ('components.fields.text', [
                      'label'       => "What is the network password?",
                      'name'        => 'password',
                      'value'       => null,
                      'default'     => Str::random (12),
                      'placeholder' => "mypassword",
                      'required'    => true,
                      'help'        => 'Minimum of 8 characters.',
                    ])
                  </div>

                </div>
              </div>
            </div> <!-- end col -->

      </div> <!-- end row -->



  <hr />
  <div class="row mb-4">
    <div class="col-lg-12 col-md-12 text-right">
      @include ('components.buttons.submit')
      @include ('components.buttons.cancel')
    </div>
  </div> <!-- end row -->
  {!! Form::close () !!}


  @if ( isset ($component) )
    @include ('partials.components.panels.delete_bar', [
      'entity'  => $component,
      'text'    => 'Network',
      'destroy' => [
        'route' => 'wifi.ssids.destroy',
        'params'=> [$component->_id],
      ],
      'return'  => [
        'route' => 'wifi.ssids.index',
        'params'=> [$component->_id],
      ],
    ])
  @endif

@endsection
