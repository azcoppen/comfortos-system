@extends ('explore.operators.brands.properties.buildings.rooms.signals')
@section ('signals')

  @include ('explore.operators.brands.properties.buildings.rooms.component_signals', ['component' => $component, 'attributes' => ['cooling_point', 'fan_mode', 'fan_state', 'heating_point', 'mode', 'opstate', 'temperature',]])

@stop
