@extends ('explore.operators.brands.properties.buildings.rooms.device')

@section ('controls')
  <div class="row">

    <h3 class="col-md-12 mb-4">controls</h3>

    @include ('partials.components.widgets.control.help')

    @livewire('controls.bulb-power', ['component' => $component])

    @livewire('controls.change-h-s-b', ['component' => $component])

    @livewire('controls.change-color-temperature', ['component' => $component])

  </div>

@stop

@push ('head')
  <style>
    .IroWheel { margin: auto !important; }
    .IroSlider { margin-left: auto !important; margin-right: auto !important; }
  </style>
@endpush

@push('js')
  @include ('partials.components.widgets.control.colorpicker_js', ['component' => $component])
@endpush
