@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">OpenHAB Config Files / </span> {{ $room->_id }}</h1>
  <hr />

  <div class="row">
    <div class="col-12">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title">Everything</h5>
            </div>
            <div class="card-body">

              <table class="table">
                <thead>
                  <tr>
                    <th>All</th>
                    <th>Things</th>
                    <th>Items</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <td width="50%"><a class="btn btn-primary" href=""><i class="fa fa-download"></i> Download ALL</a></td>
                    <td><a class="btn btn-primary" href=""><i class="fa fa-download"></i> Download .things files</a></td>
                    <td><a class="btn btn-primary" href=""><i class="fa fa-download"></i> Download .items files</a></td>
                  </tr>
                </tbody>
              </table>

            </div>
        </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title">Bridging</h5>
            </div>
            <div class="card-body">

              <table class="table">
                <thead>
                  <tr>
                    <th>Bridge</th>
                    <th>Thing</th>
                    <th>Item</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <td width="50%">MQTT Broker</td>
                    <td><a href=""><i class="fa fa-download"></i> Download .things</a></td>
                    <td><a href=""><i class="fa fa-download"></i> Download .items</a></td>
                  </tr>
                </tbody>
              </table>

            </div>
        </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title">Devices</h5>
            </div>
            <div class="card-body">

              @if ( isset ($room->component_map) && is_object ($room->component_map) )
                @foreach (['bulbs', 'cameras', 'chromecasts', 'dects', 'dimmers', 'extensions', 'handsets', 'leds', 'locks', 'mirrors', 'motors', 'plugs', 'routers', 'sensors', 'speakers', 'stbs', 'switches', 'thermostats', 'tvs', 'wifi'] AS $cpt_group)
                  @if ( isset ($room->component_map->{$cpt_group}) && is_array ($room->component_map->{$cpt_group}) && count ($room->component_map->{$cpt_group}) )
                    <table class="table mt-3">
                      <thead>
                        <tr>
                          <th>{{ $cpt_group }}</th>
                          <th>Thing</th>
                          <th>Item</th>
                        </tr>
                      </thead>

                      <tbody>
                        @foreach ($room->component_map->{$cpt_group} AS $component)
                              <tr>
                                <td width="50%">{{ (string)$component }}</td>
                                <td><a href=""><i class="fa fa-download"></i> Download .things</a></td>
                                <td><a href=""><i class="fa fa-download"></i> Download .items</a></td>
                              </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                @endforeach

              @endif

            </div>
        </div>
    </div>
  </div>








@stop
