@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ Str::limit($menu_building->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}">[{{ $menu_room->floor }}]-{{ $menu_room->number }}</a>
            <a href="" class="text-primary">Feed</a>
          </div>
      </div>
      <div class="page-header-action">
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>

  </div>
  @include ('partials.components.forms.search_bar', ['text' => 'commands and signals to/from '.$room->label . ' (Floor ' .$room->floor.', Room '.$room->number . ')'])
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@push('ws')
  <meta name="ws_host" content="{{ $ws_broker->wss_url ?? null }}/{{ $ws_broker->conn_endpoint ?? '' }}" />
  <meta name="ws_token" content="{{ $centrifugo_room_token ?? null }}" />
  <meta name="ws_user" content="{{ $room->users->first()->_id ?? null }}" />
  <meta name="ws_channel" content="{{ $room->_id ?? null }}" />
@endpush

@section('content')

  <h1 class="display-4"><span class="text-muted">feed / </span> {{ $room->label ?? 'Room #'.$room->number }}</h1>
  <hr />

    <div class="row mb-4">
        <div class="col-md-12">

            <div class="alert alert-bordered-success alert-dismissible fade show" role="alert">
        		    <i data-feather="activity" class="alert-icon"></i>
        			<span id="live-feed" class="alert-text">Waiting for stream connection...</span>
        			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        				<i data-feather="x" class="alert-close"></i>
        			</button>
        	</div>

        </div>

    </div>

    @if (is_object ($q_signals) && count ($q_signals) )
      <div class="row mt-3">
        @include('partials.components.panels.signals', ['signals' => $q_signals, 'search' => true])
      </div>
    @endif

    @if (is_object ($q_commands) && count ($q_commands) )
      <div class="row mt-3">
        @include('partials.components.panels.commands', ['commands' => $q_commands, 'search' => true])
      </div>
    @endif

    <div class="row mt-3">
      @livewire('graphs.room-signals-throughput-hourly-graph', ['room' => $menu_room])
    </div>

    <div class="row mt-3">
      @livewire('graphs.room-signals-throughput-graph', ['room' => $menu_room])
    </div>

    <div class="row">
      @livewire('room-signals-table', ['room' => $menu_room->_id])
    </div>

    <div class="row mt-3">
      @livewire('graphs.room-commands-throughput-hourly-graph', ['room' => $menu_room])
    </div>

    <div class="row mt-3">
      @livewire('graphs.room-commands-throughput-graph', ['room' => $menu_room])
    </div>

    <div class="row">
      @livewire('room-commands-table', ['room' => $menu_room->_id])
    </div>

    <div class="row mb-4">
      <div class="col-sm-6">
        @include ('partials.components.panels.mqtt_broker', ['broker' => $room->building->property->mqtt_brokers->first()])
      </div>

      <div class="col-sm-6">
        @include ('partials.components.panels.ws_broker', ['broker' => $room->building->property->ws_brokers->first()])
      </div>
    </div>

@endsection

@push ('js')
    <script src="/js/ws.js"></script>
@endpush
