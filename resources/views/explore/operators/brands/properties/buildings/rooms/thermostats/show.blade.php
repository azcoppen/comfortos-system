@extends ('explore.operators.brands.properties.buildings.rooms.device')

@section ('controls')
  <div class="row">

    <h3 class="col-md-12 mb-4">controls</h3>

    @include ('partials.components.widgets.control.help')

    @if ( $component->options )

      @livewire('controls.change-mode', ['component' => $component, 'additional' => $component->options['modes']])

      @livewire('controls.change-heating-point', ['component' => $component])

      @livewire('controls.change-cooling-point', ['component' => $component])

      @livewire('controls.change-fan-mode', ['component' => $component, 'additional' => $component->options['fan_modes']])

      @livewire('controls.change-op-state', ['component' => $component, 'additional' => $component->options['op_states']])

      @livewire('controls.change-fan-state', ['component' => $component, 'additional' => $component->options['fan_states']])

    @endif
  </div>


@stop

@push('js')
  <script>
  document.addEventListener("DOMContentLoaded", () => {

    window.addEventListener('mode.rendering', event => {
      $('#mode-{{ $component->_id }}').selectpicker('val', event.detail.last_value);
    })

    window.addEventListener('fan_mode.rendering', event => {
      $('#fan_mode-{{ $component->_id }}').selectpicker('val', event.detail.last_value);
    })

    window.addEventListener('fan_state.rendering', event => {
      $('#fan_state-{{ $component->_id }}').selectpicker('val', event.detail.last_value);
    })

    window.addEventListener('op_state.rendering', event => {
      $('#op_state-{{ $component->_id }}').selectpicker('val', event.detail.last_value);
    })

  });
  </script>
@endpush
