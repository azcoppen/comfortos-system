<div wire:init="load" wire:poll.30s>


    <div class="card-body">
      @if ( isset ($component) && is_object ($component) )
          <p class="display-5 mb-3 col-md-12">
            <a class="text-dark" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.'.($group == 'wifi' ? 'wifi-networks' : $group).'.show', [$room->building->property->brand->operator->slug, $room->building->property->brand->slug, $room->building->property->slug, $room->building->slug, $room->_id, $component->_id]) }}">
            {{ $component->label }}
            </a>
            <br />
            <small class="text-muted">{{ $component->_id }}</small>
          </p>

        <div class="row mt-4">
          <div class="col-sm-4 pl-5">
            <a class="text-dark" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.'.($group == 'wifi' ? 'wifi-networks' : $group).'.show', [$room->building->property->brand->operator->slug, $room->building->property->brand->slug, $room->building->property->slug, $room->building->slug, $room->_id, $component->_id]) }}">
              <img src="https://res.cloudinary.com/smartrooms/devices/{{ ($group == 'sensors' ? $component->variant.'-' : '') . Str::singular ($group) }}.jpg" style="max-width: 80%; max-height: 80%;" />
            </a>
          </div>

          <div class="col-sm-8">

            <ul class="list-unstyled ml-3">
              @if ($component->last_signal && is_object ($component->last_signal) )
                <li class="">
                  <h4>
                  {{ $component->last_signal->attribute }}
                  <span class="text-success">
                    @if (is_object ($component->last_signal->value))
                      Data
                    @endif

                    @if (is_array ($component->last_signal->value))
                      @if ( $group == 'routers' )
                        Data
                      @else
                        @if ( $component->last_signal->attribute == 'hsb')
                          {{ collect ($component->last_signal->value)->implode(',') }}

                        @else
                          @json($component->last_signal->value)
                        @endif
                      @endif

                    @endif

                    @if (is_string ($component->last_signal->value) || is_numeric ($component->last_signal->value))
                      {{ Str::limit($component->last_signal->value, 15) }}
                    @endif
                  </span>
                  </h4>
                </li>
                <li class="text-muted"><i class="text-muted fa fa-{{ config ('comfortos.fa_attribute_icons.'.$component->last_signal->attribute) }}"></i>  {{ $component->last_signal->received_at->timezone(auth()->user()->timezone)->format('M d H:i:sA') }}</li>
              @else
                <li class="text-muted">no last signal</li>
              @endif

              @if ($component->last_command && is_object ($component->last_command) )
                <li class="mt-3">
                  <h4>

                  {{ $component->last_command->attribute }}
                  <span class="text-warning">
                    @if (is_object ($component->last_command->value))
                      Data
                    @endif

                    @if (is_array ($component->last_command->value))
                      @if ( $group == 'routers' )
                        Data
                      @else
                        @if ( $component->last_command->attribute == 'hsb')
                          {{ collect ($component->last_command->value)->implode(',') }}

                        @else
                          @json($component->last_command->value)
                        @endif
                      @endif
                    @endif

                    @if (is_string ($component->last_command->value) || is_numeric ($component->last_command->value))
                      {{ Str::limit($component->last_command->value, 15) }}
                    @endif
                  </span>
                  </h4>
                </li>
                <li class="text-muted"><i class="text-muted fa fa-{{ config ('comfortos.fa_attribute_icons.'.$component->last_command->attribute) }}"></i> {{ $component->last_command->sent_at->timezone(auth()->user()->timezone)->format('M d H:i:sA') }}</li>
              @else
                <li class="text-muted">no last command</li>
              @endif

            </ul>



          </div>
        </div>

      @endif
      <small class="text-muted" wire:loading>
        Getting info...
      </small>
    </div>
    <div class="card-footer">
      @if ( isset ($component) && is_object ($component) )

        <small class="text-muted">
          Created: {{ $component->created_at->timezone (auth()->user()->timezone)->format ('M d H:i A') }}
        </small>

        <div class="float-right">

          @if ( $component->type == 'hardware' )
          @else
          <a class="btn btn-sm text-secondary" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.'.($group == 'wifi' ? 'wifi-networks' : $group).'.edit', [$room->building->property->brand->operator->slug, $room->building->property->brand->slug, $room->building->property->slug, $room->building->slug, $room->_id, $component->_id]) }}" title="Manage {{ $component->label }}"><i class="fa fa-edit"></i></a>
          @endif

          @if (! in_array ($group, ['extensions', 'wifi']) )
          <a class="btn btn-sm text-secondary" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.'.($group == 'wifi' ? 'wifi-networks' : $group).'.feed.signals', [$room->building->property->brand->operator->slug, $room->building->property->brand->slug, $room->building->property->slug, $room->building->slug, $room->_id, $component->_id]) }}" title="Signals from {{ $component->label }}"><i class="fa fa-compass"></i></a>
          @endif

          @if (! in_array ($group, ['cameras', 'dects', 'dimmers', 'extensions', 'handsets', 'mirrors', 'sensors', 'stbs']) )
            @if ( $component->type != 'hardware' )
              <a class="btn btn-sm text-secondary" href="{{ route ('explorer.operators.brands.properties.buildings.rooms.'.($group == 'wifi' ? 'wifi-networks' : $group).'.feed.commands', [$room->building->property->brand->operator->slug, $room->building->property->brand->slug, $room->building->property->slug, $room->building->slug, $room->_id, $component->_id]) }}" title="Commands to {{ $component->label }}"><i class="fa fa-gem"></i></a>
            @endif
          @endif
        </div>


      @endif
      <small class="text-muted" wire:loading>
        Getting info...
      </small>
    </div>

</div>
