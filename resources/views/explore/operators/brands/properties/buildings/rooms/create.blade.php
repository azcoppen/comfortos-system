@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">{{ $menu_building->label }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.index') }}">Operators</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ Str::limit($menu_building->label, 10) }}</a>
            <a href="" class="text-primary">Create Rooms</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route('explorer.operators.brands.properties.buildings.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}" class="btn btn-header"><i data-feather="plus-circle"></i> Back</a>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">{{ $menu_property->label }} / {{ $menu_building->label }} / </span> Create New Rooms</h1>
  <hr />

    {!! Form::open (['files' => true, 'route' => ['explorer.operators.brands.properties.buildings.rooms.store', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]]]) !!}

    {!! Form::hidden ('building_id', $menu_building->_id) !!}

    <div class="row">
      <div class="col-sm-12">
        <div class="alert alert-info" role="alert">
          <div class="alert-text">
              This process will create a batch of rooms for a <strong>single</strong> floor. You can only create rooms for one floor at a time. To individually customise each room, you must edit them manually.

          </div>
        </div>
      </div>
    </div>


  <div class="row">

    <div class="col-lg-4 col-md-4 ">
        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title">Spread</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                @include ('components.fields.number', [
                  'label'       => "What floor are these rooms for?",
                  'name'        => 'floor',
                  'value'       => 0,
                  'default'     => '0',
                  'placeholder' => "0",
                  'required'    => true,
                  'help'        => 'e.g. Parking lot (-3), Basement (-1), Ground (0), Fourth (4).',
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.number', [
                  'label'       => "How many rooms do you want to create?",
                  'name'        => 'num',
                  'value'       => 1,
                  'default'     => '1',
                  'placeholder' => "1",
                  'required'    => true,
                  'help'        => 'Each room will be given a incrementing number.'
                ])
              </div>

              <div class="form-group">
                @include ('components.fields.number', [
                  'label'       => "Which room number do you want to start from?",
                  'name'        => 'start',
                  'value'       => 100,
                  'default'     => 100,
                  'placeholder' => "1",
                  'required'    => true,
                  'help'        => 'e.g. 5 rooms starting from 100 = 100, 101, 102, 103, 104.'
                ])
              </div>

            </div>
        </div>

        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title">Image</h5>
            </div>
            <div class="card-body">

              <div class="form-group">
                  <input name="image" type="file" class="dropify" data-default-file="" data-max-file-size="5M" data-allowed-formats="landscape" data-allowed-file-extensions="jpg jpeg png" />
              </div>

            </div>
        </div>

     </div> <!-- end col -->

     <div class="col-lg-8 col-md-8 ">
         <div class="card card-margin">
             <div class="card-header">
                 <h5 class="card-title">Room Details</h5>
             </div>
             <div class="card-body">

               <div class="form-group">
                 @include ('components.fields.select', [
                   'label'       => "What type of room?",
                   'name'        => 'type',
                   'options'     => array_combine (config ('comfortos.room_types'), config ('comfortos.room_types')),
                   'value'       => null,
                   'default'     => 'double',
                   'placeholder' => "Choose a room type:",
                   'required'    => true,
                   'help'        => 'You can change this for each room later.',
                 ])
               </div>

               <div class="form-group">
                 @include ('components.fields.select', [
                   'label'       => "What grade of room?",
                   'name'        => 'grade',
                   'options'     => array_combine (config ('comfortos.room_grades'), config ('comfortos.room_grades')),
                   'value'       => null,
                   'default'     => 'queen',
                   'placeholder' => "Choose a room grade:",
                   'required'    => true,
                   'help'        => 'You can change this for each room later.',
                 ])
               </div>

               <div class="form-group">
                 @include ('components.fields.text', [
                   'label'       => "Provide a brief description of this type of room:",
                   'name'        => 'description',
                   'value'       => null,
                   'default'     => '',
                   'placeholder' => "Guest room, 1 Double, Courtyard view",
                   'required'    => true,
                   'help'        => 'You can change this for each room later.',
                 ])
               </div>

               <div class="form-group">
                 @include ('components.fields.number', [
                   'label'       => "What is the approximate sq footage of each room?",
                   'name'        => 'footage',
                   'value'       => null,
                   'default'     => '',
                   'placeholder' => "400",
                   'required'    => true,
                   'help'        => 'You can change this for each room later.',
                 ])
               </div>

               <div class="form-group">
                 @include ('components.fields.number', [
                   'label'       => "How many beds are in each room?",
                   'name'        => 'beds',
                   'value'       => null,
                   'default'     => '',
                   'placeholder' => "1",
                   'required'    => true,
                   'help'        => 'You can change this for each room later.',
                 ])
               </div>

               <div class="form-group">
                 @include ('components.fields.number', [
                   'label'       => "What is the max occupancy of each room?",
                   'name'        => 'occupancy',
                   'value'       => null,
                   'default'     => '',
                   'placeholder' => "2",
                   'required'    => true,
                   'help'        => 'You can change this for each room later.',
                 ])
               </div>

               <div class="form-group">
                   @include ('components.fields.radioset', [
                     'label'       => "Are they inside or outside?",
                     'color'       => 'primary',
                     'name'        => 'ext',
                     'options'     => ['Inside' => 0, 'Outside' => 1],
                     'value'       => 0,
                     'default'     => 0,
                     'required'    => true,
                     'help'        => 'You can change this for each room later.',
                   ])
               </div>

               <div class="form-group">
                   @include ('components.fields.radioset', [
                     'label'       => "Are they accessible to the public?",
                     'color'       => 'primary',
                     'name'        => 'public',
                     'options'     => ['Yes' => 1, 'No' => 0],
                     'value'       => 0,
                     'default'     => 0,
                     'required'    => true,
                     'help'        => 'You can change this for each room later.',
                   ])
               </div>

               <div class="form-group">
                   @include ('components.fields.radioset', [
                     'label'       => "Are they smoking rooms?",
                     'color'       => 'primary',
                     'name'        => 'smoking',
                     'options'     => ['Smoking' => 1, 'No Smoking' => 0],
                     'value'       => 0,
                     'default'     => 0,
                     'help'        => 'You can change this for each room later.',
                   ])
               </div>

               <div class="form-group">
                   @include ('components.fields.radioset', [
                     'label'       => "Do the rooms have A/C?",
                     'color'       => 'primary',
                     'name'        => 'AC',
                     'options'     => ['Yes' => 1, 'No' => 0],
                     'value'       => 1,
                     'default'     => 1,
                     'help'        => 'You can change this for each room later.',
                   ])
               </div>

               <div class="form-group">
                   @include ('components.fields.radioset', [
                     'label'       => "Is Ethernet available?",
                     'color'       => 'warning',
                     'name'        => 'eth',
                     'options'     => ['yes' => 1, 'No' => 0],
                     'value'       => 0,
                     'default'     => 0,
                     'required'    => true,
                     'help'        => 'You can change this for each room later.',
                   ])
               </div>

             </div>
         </div>
     </div>

   </div> <!-- end roo -->

    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit', ['label' => 'Create Rooms'])
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}
@endsection
