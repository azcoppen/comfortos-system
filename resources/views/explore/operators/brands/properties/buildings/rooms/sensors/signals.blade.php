@extends ('explore.operators.brands.properties.buildings.rooms.signals')
@section ('signals')

  @if ( $component->variant == 'contact' )
    @include ('explore.operators.brands.properties.buildings.rooms.component_signals', ['component' => $component, 'attributes' => ['battery', 'contact', 'temperature']])
  @endif

  @if ( $component->variant == 'flood' )
    @include ('explore.operators.brands.properties.buildings.rooms.component_signals', ['component' => $component, 'attributes' => ['battery', 'flood',]])
  @endif

  @if ( $component->variant == 'multi' )
    @include ('explore.operators.brands.properties.buildings.rooms.component_signals', ['component' => $component, 'attributes' => ['battery', 'humidity', 'luminance', 'motion', 'tamper', 'temperature', 'uv']])
  @endif

  @if ( $component->variant == 'smoke' )
    @include ('explore.operators.brands.properties.buildings.rooms.component_signals', ['component' => $component, 'attributes' => ['battery', 'co', 'smoke',]])
  @endif
@stop
