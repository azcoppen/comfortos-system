@extends ('explore.operators.brands.properties.buildings.rooms.commands')
@section ('commands')

  @include ('explore.operators.brands.properties.buildings.rooms.component_commands', ['component' => $component, 'attributes' => ['dir', 'position', 'power', 'speed']])

@stop
