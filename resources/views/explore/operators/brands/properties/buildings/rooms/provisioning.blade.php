@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ Str::limit($menu_building->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}">[{{ $menu_room->floor }}]-{{ $menu_room->number }}</a>
            @if ( isset ($component) )
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}">{{ Str::limit($component->label, 10) }}</a>
            @endif
            <a href="" class="text-primary">{{ Str::title ($group) }}</a>
            <a href="" class="text-primary">Provisioning</a>
          </div>
      </div>
      <div class="page-header-action">
        @if ( isset ($component) )
          @if ( in_array ($group, ['bulbs', 'cameras', 'chromecasts', 'leds', 'locks', 'mirrors', 'motors', 'plugs', 'routers', 'speakers', 'switches', 'thermostats', 'tvs']) )
          <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.feed.commands', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="activity"></i> Command Stream</a>
          @endif
          <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.feed.signals', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="activity"></i> Signal Stream</a>
          <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
        @else
          <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
        @endif
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">room #{{ $menu_room->number }} / {{ $group }} /</span> {{ isset ($component) ? 'Manage '.$component->label : 'Provision New '.Str::title($group) }}</h1>
  <hr />

  @if (! isset ($component) )
    <!-- WARNING: ONLY ONE DEVICE PER ROOM -->
    @if ( in_array ($group, ['dects', 'extensions', 'routers']) && $menu_room->{$group}->count () > 0 )
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-warning" role="alert">
            <i data-feather="alert-octagon" class="alert-icon"></i>
            <span class="alert-text"><strong>Important:</strong> You can only provision one {{ Str::singular ($group) }} per room.</span>
          </div>
        </div>
      </div>
    @else
      <!-- CREATE NEW -->
      {!! Form::open (['method' => 'PUT', 'route' => ['explorer.operators.brands.properties.buildings.rooms.'.$group.'.provision', $menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]]) !!}

      @if ( isset ($available) && count ($available) )
        <div class="row">
          <div class="col-md-12">

            <div class="card card-margin">
                <div class="card-header">
                    <h5 class="card-title"><i data-feather="box"></i> Available <span class="text-muted"> / Unprovisioned</span> {{ $group }}</h5>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Identifier</th>
                          @if ( $group == 'extensions' )
                            <th>SIP ID</th>
                            <th>Remote ID</th>
                          @else
                            <th>Type</th>
                            <th>Model</th>
                          @endif
                          <th>Label</th>
                          <th>Created</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($available AS $entity)
                          <tr>
                            <td>
                              <div class="string-check string-check-bordered-primary mb-2">
                                @if ( in_array ($group, ['dects', 'extensions', 'routers']) )
                                  {!! Form::radio ('provisionables['.$group.'][]', $entity->_id, false, ['id' => $entity->_id, 'class' => 'form-check-input']) !!}
                                @else
                                  {!! Form::checkbox ('provisionables['.$group.'][]', $entity->_id, false, ['id' => $entity->_id, 'class' => 'form-check-input']) !!}
                                @endif
                                  <label class="string-check-label" for="{{ $entity->_id }}">
                                  </label>
                              </div>
                            </td>
                            <td>{{ $entity->_id }}</td>
                            @if ( $group == 'extensions' )
                              <td>{{ $entity->sip_id }} @ {{ $entity->pbx->host }}</td>
                              <td>{{ $entity->remote_id }} ({{ $entity->caller_id }})</td>
                            @else
                              <td>{{ $entity->type }}</td>
                              <td>{{ $entity->model }}</td>
                            @endif
                            <td>{{ $entity->label }}</td>
                            <td>{{ $entity->created_at->timezone (auth()->user()->timezone)->format ('M d Y H:iA') }}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>

          </div>
        </div>

        <hr />
        <div class="row mb-4">
          <div class="col-lg-12 col-md-12 text-right">
            @include ('components.buttons.submit')
            @include ('components.buttons.cancel')
          </div>
        </div> <!-- end row -->
        {!! Form::close () !!}


      @else
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-bordered-info" role="alert">
              <i data-feather="alert-octagon" class="alert-icon"></i>
              <span class="alert-text"><strong>Diagnostics:</strong> No items available for provisioning.</span>
            </div>
          </div>
        </div>
      @endif

    @endif
  @else
    <!-- REMOVE EXISTING -->
    {!! Form::open (['method' => 'PUT', 'route' => ['explorer.operators.brands.properties.buildings.rooms.'.$group.'.deprovision', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]]]) !!}

    <div class="row">
      <div class="col-md-12">

        <div class="card card-margin">
            <div class="card-header">
                <h5 class="card-title"><i data-feather="box"></i> Provisioned {{ $group }}</h5>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Identifier</th>
                      @if ( $group == 'extensions' )
                        <th>SIP ID</th>
                        <th>Remote ID</th>
                      @else
                        <th>Type</th>
                        <th>Model</th>
                      @endif
                      <th>Label</th>
                      <th>Created</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($room->{$group} AS $entity)
                      <tr>
                        <td>
                          <div class="string-check string-check-soft-danger mb-2">
                            @if ( in_array ($group, ['dects', 'extensions', 'routers']) )
                              {!! Form::radio ('deprovisionables['.$group.'][]', $entity->_id, true, ['id' => $entity->_id, 'class' => 'form-check-input']) !!}
                            @else
                              {!! Form::checkbox ('deprovisionables['.$group.'][]', $entity->_id, true, ['id' => $entity->_id, 'class' => 'form-check-input']) !!}
                            @endif
                              <label class="string-check-label" for="{{ $entity->_id }}">
                              </label>
                          </div>
                        </td>
                        <td>{{ $entity->_id }}</td>
                        @if ( $group == 'extensions' )
                          <td>{{ $entity->sip_id }} @ {{ $entity->pbx->host }}</td>
                          <td>{{ $entity->remote_id }} ({{ $entity->caller_id }})</td>
                        @else
                          <td>{{ $entity->type }}</td>
                          <td>{{ $entity->model }}</td>
                        @endif
                        <td>{{ $entity->label }}</td>
                        <td>{{ $entity->created_at->timezone (auth()->user()->timezone)->format ('M d Y H:iA') }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </div>

      </div>
    </div>

    <hr />
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-right">
        @include ('components.buttons.submit')
        @include ('components.buttons.cancel')
      </div>
    </div> <!-- end row -->
    {!! Form::close () !!}

  @endif









@stop
