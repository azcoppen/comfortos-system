@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">{{ $menu_building->label }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}" class="text-primary">{{ Str::limit($menu_building->label, 10) }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.create', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Rooms</a>
      </div>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'rooms within '.$menu_property->label .' / '. $menu_building->label])
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">{{ $menu_property->label }} /</span> {{ $menu_building->label }} <span class="text-muted">/ rooms</span></h1>

    @livewire('room-grid', ['building' => $menu_building, 'menu_floor' => $menu_floor ?? 0])
@endsection
