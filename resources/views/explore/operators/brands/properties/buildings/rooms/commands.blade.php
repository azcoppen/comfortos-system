@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
            <a href="{{ route('explorer.index') }}">OS Explorer</a>
            <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}">{{ Str::limit($menu_brand->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ Str::limit($menu_property->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.index', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ Str::limit($menu_building->label, 10) }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}">[{{ $menu_room->floor }}]-{{ $menu_room->number }}</a>
            <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}">{{ Str::limit($component->label, 10) }}</a>
            <a href="" class="text-primary">Commands</a>
          </div>
      </div>
      <div class="page-header-action">
<a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.feed.signals', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="activity"></i> Signal Stream</a>
        <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.'.$group.'.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="arrow-left"></i> Back</a>
      </div>

  </div>
  @include ('partials.components.forms.search_bar', ['text' => 'commands to '.$component->label])
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')

  <h1 class="display-4"><span class="text-muted">command stream / </span> {{ $component->label }}</h1>
  <hr />

  @if (is_object ($q_commands) && count ($q_commands) )
    <div class="row mt-3">
      @include('partials.components.panels.commands', ['commands' => $q_commands, 'search' => true])
    </div>
  @endif

  @yield ('commands')

  <div class="row mb-4">
    <div class="col-sm-6">
      @include ('partials.components.panels.mqtt_broker', ['broker' => $component->room->building->property->mqtt_brokers->first()])
    </div>

    <div class="col-sm-6">
      @include ('partials.components.panels.ws_broker', ['broker' => $component->room->building->property->ws_brokers->first()])
    </div>
  </div>

@endsection
