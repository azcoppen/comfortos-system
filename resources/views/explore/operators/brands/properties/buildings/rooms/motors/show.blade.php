@extends ('explore.operators.brands.properties.buildings.rooms.device')

@section ('controls')
  <div class="row">

    <h3 class="col-md-12 mb-4">controls</h3>

    @include ('partials.components.widgets.control.help')

    @livewire('controls.power', ['component' => $component])

    @livewire('controls.change-mode', ['component' => $component, 'additional' => ['standby', 'on',]])

    @livewire('controls.change-direction', ['component' => $component])

    @livewire('controls.change-speed', ['component' => $component])

  </div>


@stop
