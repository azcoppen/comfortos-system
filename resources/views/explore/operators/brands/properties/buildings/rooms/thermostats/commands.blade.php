@extends ('explore.operators.brands.properties.buildings.rooms.commands')
@section ('commands')

  @include ('explore.operators.brands.properties.buildings.rooms.component_commands', ['component' => $component, 'attributes' => ['cooling_point', 'fan_mode', 'fan_state', 'heating_point', 'mode', 'opstate', 'temperature',]])

@stop
