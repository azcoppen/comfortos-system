@extends ('explore.operators.brands.properties.buildings.rooms.device')

@section ('controls')
  <div class="row">

    <h3 class="col-md-12 mb-4">controls</h3>

    @include ('partials.components.widgets.control.help')

    @livewire('controls.change-volume', ['component' => $component])

    @livewire('controls.mute', ['component' => $component])

    @livewire('controls.stop', ['component' => $component])

    @livewire('controls.player', ['component' => $component])

  </div>



@stop
