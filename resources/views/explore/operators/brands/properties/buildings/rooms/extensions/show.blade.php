@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
              <a href="{{ route('explorer.operators.brands.show', [$menu_operator->slug, $menu_brand->slug]) }}"><i data-feather="map-pin"></i> {{ $menu_brand->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ $menu_property->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.buildings.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ $menu_building->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="text-primary">Room #{{ $menu_room->number }}</a>
          </div>
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">{{ $group }} / </span> {{ $component->label ?? $component->_id }} <span class="text-muted">@ {{ $component->room->label }}</span></h1>
  <hr />

  <div class="row">
      <div class="col-lg-4 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">Details</h5>
              </div>
              <div class="card-body text-center">
                  <h2 class="lead"><i class="fa fa-phone"></i> {{ $component->sip_id }}{{ isset ($component->pbx->host) ? '@'.$component->pbx->host : '' }}</h2>
                  <h3 class"text-muted">{{ $component->caller_id }}</h3>

                  <p class="text-muted mt-4">
                      Created: {{ $component->created_at->diffForHumans() }}<br />
                  </p>
              </div>
          </div>
      </div>

      <div class="col-lg-4 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">General</h5>
              </div>
              <div class="card-body">
                  <table class="table table-hover">
                      <tbody>
                          <tr>
                              <td class="border-0">Remote PBX ID</td>
                              <td class="border-0">{{ $component->remote_id }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">SIP Server</td>
                              <td class="border-0">{{ $component->pbx->host ?? '' }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">SIP ID</td>
                              <td class="border-0">{{ $component->sip_id }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">Auth ID</td>
                              <td class="border-0">{{ $component->auth_id }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">Auth Password</td>
                              <td class="border-0">{{ $component->auth_pwd }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">Direct Dial (DID)</td>
                              <td class="border-0">{{ $component->did }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">Voicemail PIN</td>
                              <td class="border-0">{{ $component->vpin }}</td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

      @include ('partials.provisioning', [
        'room' => $component->room,
        'item' => $component,
      ])

  </div>

  <hr />

  <div class="row">
      <div class="col-lg-12 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">PBX Server</h5>
              </div>
              <div class="card-body">
                  @if ( is_object($component->pbx) )
                      <div class="table-responsive">
                          <table id="" class="table table-hover" style="width:100%">
                              <thead>
                              <tr>
                                  <th></th>
                                  <th>Country</th>
                                  <th>Host</th>
                                  <th>IP</th>
                                  <th>Type</th>
                                  <th>Created</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                @each('telecoms.pbxs.each_table', collect([$component->pbx]), 'record')
                              </tbody>
                          </table>
                      </div>
                  @else
                      @include ('partials.components.alerts.empty')
                  @endif
              </div>
          </div>
      </div>

  </div>

  <div class="row">

      <div class="col-lg-12 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">DECT Base Stations</h5>
              </div>
              <div class="card-body">
                  @if ( is_object($component->dects) )
                      <div class="table-responsive">
                          <table id="" class="table table-hover" style="width:100%">
                              <thead>
                              <tr>
                                  <th></th>
                                  <th>Identifier</th>
                                  <th>Model</th>
                                  <th>Label</th>
                                  <th>Created</th>
                                  <th><i class="fa fa-globe" title="WPN Admin"></i></th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach ($component->dects AS $record)
                                  <tr>
                                      <td><i class="fa fa-network-wired text-muted"></i></td>
                                      <td><a href="{{ route('explorer.operators.brands.properties.buildings.rooms.dects.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $record->_id]) }}">{{ $record->_id }}</a></td>
                                      <td>{{ $record->model }}</td>
                                      <td>{{ $record->label }}</td>
                                      <td>{{ $record->created_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} <span class="text-muted">({{ $record->created_at->diffForHumans() }})</span></td>
                                      <td>
                                          @if ( $record->web_ui )
                                          <a title="Launch admin panel" target="_blank" href="http://{{ $record->vpn_ip }}"><i class="fa fa-shield-alt" title="VPN Admin"></i></a>
                                          @endif
                                      </td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                  @else
                      @include ('partials.components.alerts.empty')
                  @endif
              </div>
          </div>
      </div>

  </div>

  <div class="row">
      <div class="col-lg-12 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">Handsets</h5>
              </div>
              <div class="card-body">
                  @if ( is_object($component->handsets) && $component->handsets->count() )
                      <div class="table-responsive">
                          <table id="" class="table table-hover" style="width:100%">
                              <thead>
                              <tr>
                                  <th></th>
                                  <th>Identifier</th>
                                  <th>Model</th>
                                  <th>Label</th>
                                  <th>Created</th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach ($component->handsets AS $record)
                                  <tr>
                                      <td><i class="fa fa-network-wired text-muted"></i></td>
                                      <td><a href="{{ route('explorer.operators.brands.properties.buildings.rooms.handsets.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $record->_id]) }}">{{ $record->_id }}</a></td>
                                      <td>{{ $record->model }}</td>
                                      <td>{{ $record->label }}</td>
                                      <td>{{ $record->created_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} <span class="text-muted">({{ $record->created_at->diffForHumans() }})</span></td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                  @else
                      @include ('partials.components.alerts.empty')
                  @endif
              </div>
          </div>
      </div>
  </div>


    @include ('partials.components.panels.device_business_flow', ['component' => $component])

@stop

@push ('js')
    <script>
      @include ('partials.centrifugo_js_room', ['room' => $menu_room, 'ws_broker' => $room->building->property->mqtt_brokers->first() ?? null])
    </script>
@endpush
