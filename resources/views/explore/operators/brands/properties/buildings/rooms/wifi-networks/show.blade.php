@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_property->label }} / {{ $menu_building->label }} / Room #{{ $menu_room->number }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">Room #{{ $menu_room->number }}</div>
          <div class="header-breadcrumb">
              <a href="{{ route('explorer.operators.brands.show', [$menu_operator->slug, $menu_brand->slug]) }}"><i data-feather="map-pin"></i> {{ $menu_brand->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug]) }}">{{ $menu_property->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.buildings.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug]) }}">{{ $menu_building->label }}</a>
              <a href="{{ route('explorer.operators.brands.properties.buildings.rooms.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id]) }}" class="text-primary">Room #{{ $menu_room->number }}</a>
          </div>
      </div>
      <div class="page-header-action">
        @if ( $component->type == 'virtual' )
              <a href="{{ route ('explorer.operators.brands.properties.buildings.rooms.'.$group.'.edit', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->_id]) }}" class="btn btn-header"><i data-feather="edit"></i> Manage Network</a>
        @endif
      </div>
  </div>
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">{{ $group }} / </span> {{ $component->label ?? $component->_id }} <span class="text-muted">@ {{ $component->room->label }}</span></h1>
  <hr />

  @if ( $component->type == 'hardware' )
    <div class="row">
      <div class="col-sm-12">
        <div class="alert alert-dark" role="alert">
          <div class="alert-text">
              This network is a <strong class="text-warning">permanent hardware SSID</strong> on WLAN1 and cannot be edited.
          </div>
        </div>
      </div>
    </div>
  @endif

  <div class="row">
      <div class="col-lg-4 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">Details</h5>
              </div>
              <div class="card-body text-center">
                  <h2 class="lead"><i class="fa fa-wifi"></i> {{ $component->ssid }}</h2>
                  <p><span class="text-muted">Password: </span> {{ $component->password }}</p>
                  <br />
                  <p class="text-muted">
                      Created: {{ $component->created_at->diffForHumans() }}<br />
                      Expiry: {{ $component->expire_at ? $component->created_at->diffForHumans() : '' }}</br>
                      Lifetime: {{ $component->expire_at ? $component->expire_at->diffForHumans($component->created_at) : '' }}
                  </p>
              </div>
          </div>
      </div>

      <div class="col-lg-4 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">Internals</h5>
              </div>
              <div class="card-body">
                  <table class="table table-hover">
                      <tbody>
                          <tr>
                              <td class="border-0">ID</td>
                              <td class="border-0">{{ $component->_id }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">Type</td>
                              <td class="border-0">{{ $component->type }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">Interface</td>
                              <td class="border-0">{{ $component->interface }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">SP Index</td>
                              <td class="border-0">{{ $component->profile_index }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">Net Index</td>
                              <td class="border-0">{{ $component->network_index }}</td>
                          </tr>
                          <tr>
                              <td class="border-0">Profile</td>
                              <td class="border-0">{{ $component->security_profile }}</td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>

      @include ('partials.provisioning', [
        'room' => $component->room,
        'item' => $component,
      ])

  </div>

  <hr />

  <div class="row">
      <div class="col-lg-12 card-margin">
          <div class="card">
              <div class="card-header">
                  <h5 class="card-title">Router</h5>
              </div>
              <div class="card-body">
                  @if ( $component->router )
                      <div class="table-responsive">
                          <table id="" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                              <tr>
                                  <th></th>
                                  <th>Identifier</th>
                                  <th>Label</th>
                                  <th>Type</th>
                                  <th>Model</th>
                                  <th>Created</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td><i class="fa fa-network-wired text-muted"></i></td>
                                    <td><a href="{{ route('explorer.operators.brands.properties.buildings.rooms.routers.show', [$menu_operator->slug, $menu_brand->slug, $menu_property->slug, $menu_building->slug, $menu_room->_id, $component->router->_id]) }}">{{ $component->router->_id }}</a></td>
                                    <td>{{ $component->router->label }}</td>
                                    <td>{{ $component->router->type }}</td>
                                    <td>{{ $component->router->model }}</td>
                                    <td>{{ $component->router->created_at->timezone(auth()->user()->timezone)->format ('M d Y H:i:s A') }} <span class="text-muted">({{ $component->router->created_at->diffForHumans() }})</span></td>
                                    <td>
                                        @if ( $component->router->vpn_ip )
                                        <a title="Launch admin panel" target="_blank" href="http://{{ $component->router->vpn_ip }}"><i class="fa fa-shield-alt" title="VPN Admin"></i></a>
                                        @endif
                                    </td>
                                </tr>
                              </tbody>
                          </table>
                      </div>
                  @endif
              </div>
          </div>
      </div>

  </div>

@stop

@push ('js')
    <script>
      @include ('partials.centrifugo_js_room', ['room' => $menu_room, 'ws_broker' => $room->building->property->mqtt_brokers->first() ?? null])
    </script>
@endpush
