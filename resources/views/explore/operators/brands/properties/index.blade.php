@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_brand->label }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">{{ $menu_brand->label }}</div>
          <div class="header-breadcrumb">
              <a href="{{ route('explorer.index') }}">OS Explorer</a>
              <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}">{{ Str::limit($menu_operator->label, 10) }}</a>
              <a href="{{ route('explorer.operators.brands.properties.index', [$menu_operator->slug, $menu_brand->slug]) }}" class="text-primary">{{ Str::limit($menu_brand->label, 10) }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('explorer.operators.brands.properties.create', [$menu_operator->slug, $menu_brand->slug]) }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Property</a>
      </div>

  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'properties within '.$menu_brand->label])
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4"><span class="text-muted">{{ $menu_operator->label }} /</span> {{ $menu_brand->label }} <span class="text-muted">/ properties</span></h1>
  <hr />

  @if ($menu_properties->count())
    <div class="row mt-5">
        @foreach ($menu_properties AS $property)

          <div class="col-lg-4 col-md-6 col-sm-6 ">
              <div class="card card-margin">
                  <div class="card-body p-1" style="background-image: url({{ $property->image ? 'https://res.cloudinary.com/smartrooms/properties/' . $property->image .'.jpg' : config ('comfortos.placeholders.image.property') }}); background-size: cover;">

                      <div class="widget-4">
                          <div class="widget-4-icon option-3">
                              <a title="Manage {{ $property->label }}" href="{{ route ('explorer.operators.brands.properties.edit', [$menu_operator->slug, $menu_brand->slug, $property->slug]) }}"><i data-feather="map-pin"></i></a>
                          </div>
                          <a class="text-light" style="text-decoration: none;" href="{{ route ('explorer.operators.brands.properties.buildings.index', [$menu_operator->slug, $menu_brand->slug, $property->slug]) }}">
                            <div class="widget-4-body pt-5">
                              <h1 class="display-3 pt-5 p-3 mt-5 mb-2">
                                  {{ $property->label }}
                              </h1>

                            </div>
                          </a>
                      </div>

                  </div>

                  <div class="card-footer">
                    @if ($property->buildings->count())
                      @foreach ($property->buildings AS $building)
                        <span class="badge badge-info mt-2 mr-2">{{ $building->label ?? '' }}</span>
                      @endforeach
                    @else
                      <span class="text-muted">No buildings added yet.</span>
                    @endif
                  </div>

              </div>
          </div>

        @endforeach
    </div>

  @else
    <div class="row mt-1">
      <div class="col-md-12">
        <div class="alert alert-info" role="alert">
          <span class="alert-text"><i data-feather="alert-octagon"></i> <strong>You haven't added any properties yet. Once you add them, they will appear here.</strong></span>
        </div>
      </div>
    </div>
  @endif

@endsection
