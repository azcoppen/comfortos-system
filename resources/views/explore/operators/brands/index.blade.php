@extends('layouts.themes.blixy.dashboard')
@section('head')
  <title>SmartRooms:: {{ $menu_operator->label }}</title>
@stop

@section('app_title')
  <h4>OS Explorer</h4>
@stop

@section('header')
  <div class="page-header-container">
      <div class="page-header-main">
          <div class="page-title">{{ $menu_operator->label }}</div>
          <div class="header-breadcrumb">
              <a href="{{ route('explorer.index') }}">OS Explorer</a>
              <a href="{{ route('explorer.operators.brands.index', [$menu_operator->slug]) }}" class="text-primary">{{ Str::limit($menu_operator->label, 10) }}</a>
          </div>
      </div>
      <div class="page-header-action">
              <a href="{{ route ('explorer.operators.brands.create', $menu_operator->slug) }}" class="btn btn-header"><i data-feather="plus-circle"></i> Add New Brand</a>
      </div>
  </div>

  @include ('partials.components.forms.search_bar', ['text' => 'brands within '.$menu_operator->label])
@stop

@section('sidebar')
  @include ('explore.sidebar')
@endsection

@section('content')
  <h1 class="display-4">{{ $menu_operator->label }} <span class="text-muted">/ brands</span></h1>
  <hr />

  @if ( $menu_brands->count () )
    <div class="row mt-5">
        @foreach ($menu_brands AS $brand)

          <div class="col-lg-4 col-md-6 col-sm-6 ">
              <div class="card card-margin">
                  <div class="card-body p-1" style="background-image: url({{ $brand->image ? 'https://res.cloudinary.com/smartrooms/brands/' . $brand->image .'.jpg' : config ('comfortos.placeholders.image.brand') }}); background-size: cover;">

                      <div class="widget-4">
                          <div class="widget-4-icon option-3">
                              <a title="Manage {{ $brand->label }}" href="{{ route ('explorer.operators.brands.edit', [$menu_operator->slug, $brand->slug]) }}"><i data-feather="box"></i></a>
                          </div>
                          <a class="text-light" style="text-decoration: none;" href="{{ route ('explorer.operators.brands.properties.index', [$menu_operator->slug, $brand->slug]) }}">
                            <div class="widget-4-body pt-5">
                              <h1 class="display-3 pt-5 p-3 mt-5 mb-2">
                                  {{ $brand->label }}
                              </h1>

                            </div>
                          </a>
                      </div>

                  </div>

                  <div class="card-footer">
                    @if ($brand->properties->count())
                      @foreach ($brand->properties AS $property)
                        <span class="badge badge-info mt-2 mr-2">{{ $property->label ?? '' }}</span>
                      @endforeach
                    @else
                      <span class="text-muted">No properties added yet.</span>
                    @endif
                  </div>

              </div>
          </div>


        @endforeach
    </div>
  @else
    <div class="row mt-1">
      <div class="col-md-12">
        <div class="alert alert-info" role="alert">
          <span class="alert-text"><i data-feather="alert-octagon"></i> <strong>You haven't added any brands yet. Once you add them, they will appear here.</strong></span>
        </div>
      </div>
    </div>
  @endif

@endsection
