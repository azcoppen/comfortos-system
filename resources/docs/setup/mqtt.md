# External MQTT Controls

## Listening to MQTT Commands

### Listen to a whole property
mosquitto_sub -h mqtt.smartrooms.dev -t 5fb225099ba3874e9767b64e/# -u smartrooms -P cstr8media -v -d

### Listen to a whole building
mosquitto_sub -h mqtt.smartrooms.dev -t 5fb225099ba3874e9767b64e/5fb2250a9ba3874e9767b652/# -u smartrooms -P cstr8media -v -d

### Listen to a group of components
mosquitto_sub -h mqtt.smartrooms.dev -t 5fb225099ba3874e9767b64e/5fb2250a9ba3874e9767b652/speakers/# -u smartrooms -P cstr8media -v -d

### Listen to a component
mosquitto_sub -h mqtt.smartrooms.dev -t 5fb225099ba3874e9767b64e/5fb2250a9ba3874e9767b652/speakers/5fb230d808d7ee320c1a18d2/# -u smartrooms -P cstr8media -v -d

## Sending MQTT Commands

mosquitto_pub -h mqtt.smartrooms.dev -m "50" -t 5fb225099ba3874e9767b64e/5fb2250a9ba3874e9767b652/5fb2250a9ba3874e9767b656/speakers/5fb230d808d7ee320c1a18d2/volume -u smartrooms -P cstr8media -r -d
