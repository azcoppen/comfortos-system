# Setting up an OpenHABian Pi

## Create the SD image

Follow the instructions here: https://www.openhab.org/docs/installation/openhabian.html#raspberry-pi-prepackaged-sd-card-image

1. Download the latest `.xz` file: https://github.com/openhab/openhabian/releases
2. Download Etcher: https://www.balena.io/etcher/
3. Insert the SD card into your computer.
4. Open Etcher and flash the SD card with the `.xz` image file.
5. Unmount and remove the SD card, insert into the Pi.
6. Switch on, wait 10 mins.

## First Run

### Finding the Pi

1. Log into the RouterOS control panel (`http://192.168.88.1`) using the user/pass of that unit.
2. In Webfig, go to `IP` > `DHCP Server` > `Leases` (tab). Look for `OpenHABian` or `openhab` in the "Active Host Name" column.
3. Note the IP address and MAC address.
4. Use "Add New" under "leases" to create a new lease with the IP address **192.168.88.2** with the MAC address, and client name "openhab".
5. Reboot the Pi.

### First login

1. Connect to the PI using the default user/pass: `ssh openhabian@192.168.88.x` (password `openhabian`).
2. Run `sudo apt update && sudo apt upgrade`.

### Configuring OpenHABian setup

Run `sudo openhabian-config`. Apply any updates or fixes (self-updates) using option `02`. Apply improvements using option `10`.

Under option `30`, or **System Settings**:

1. 31: Change hostname to "smartrooms-openhab".
2. 33: Change timezone to the correct locale.
3. Enable NTP.
4. 34: Change password for user "openhabian". Note the new one.

Under option `40`, or **OpenHAB related**,

1. 44: Install Nginx as a reverse proxy. Pick a username and password for HTTP basic auth and enable SSL/HTTPS.

**Important**: OpenHABian configures Nginx with `localhost` as its `server_name` directive as it assumes it's running on the same PC with a monitor. For external access you need to:

- Edit `/etc/nginx/sites-enabled/openhab`.
- Comment out the `server_name` directives.
- Restart nginx with `sudo systemctl restart nginx`.

Example conf:

```

#################################
# openHABian NGINX Confiuration #
#################################

## Redirection
 server {
   return 301                      https://$host$request_uri;
 }

## Reverse Proxy to openHAB
server {

   listen                          443 ssl http2 default_server;

    # Cross-Origin Resource Sharing.
    add_header 'Access-Control-Allow-Origin' '*' always; # make sure that also a 400 response works
    add_header 'Access-Control-Allow_Credentials' 'true' always;
    add_header 'Access-Control-Allow-Headers' 'Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range' always;
    add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE,PATCH' always;

## Secure Certificate Locations
   ssl_certificate                 /etc/ssl/certs/openhab.crt;
   ssl_certificate_key             /etc/ssl/certs/openhab.key;

    location / {
        proxy_pass                              http://localhost:8080/;
        proxy_set_header Host                   $http_host;
        proxy_set_header X-Real-IP              $remote_addr;
        proxy_set_header X-Forwarded-For        $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto      $scheme;
        proxy_read_timeout 3600;                                # to avoid "offline" error msgs in Basic UI

## Password Protection
       auth_basic                              "Username and Password Required";
       auth_basic_user_file                    /etc/nginx/.htpasswd;
    }

}

```

Note the inclusion of `http2 default_server` and removal of transport security.

By default, OpenHAB listens on `0.0.0.0:8080` and `0.0.0.0:8443`. We don't want that as anyone can log into the web admin unprotected.

Check which ports are open using:

Important: add the HTTP Basic Auth user and password. Note it down.

```
netstat -peanut | grep LISTEN
```

Block incoming ports `8080` and `8443` and `9001` (log).

### Getting configuration details

To get the operating system details:

```
cat /etc/*-release
```

To get serial/hardware numbers:

```
cat /proc/cpuinfo
```

To get IP and MAC addressing:

```
hostname -I
```

To get OpenHAB info:

```
openhab-cli info
```

### Installing PHP & the MikroTik CLI

PHP is not installed with Raspbian by default.

```
sudo apt update
sudo apt upgrade
sudo apt -y install lsb-release apt-transport-https ca-certificates
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
sudo apt -y install php7.4 php7.4-bcmath php7.4-cli php7.4-curl php7.4-dev php7.4-intl php7.4-json php7.4-mbstring php7.4-mongodb php7.4-redis php7.4-soap php7.4-sqlite3 php7.4-xml php7.4-xmlrpc php7.4-zip
sudo systemctl disable apache2
```

Copy the MikroTik CLI so all users can run it:

```
cd $HOME
wget http://cdn.smartrooms.dev/setup/mikrotik-cli
sudo mv ./mikrotik-cli /usr/local/bin/mikrotik-cli
sudo chmod +x /usr/local/bin/mikrotik-cli
```

Add the .env file and root CA in the same directory:

```
nano /home/openhabian/.env
```

Sample vars: the .pem file MUST be specified for executing in the HAB.

```
ROUTER_HOST=192.168.88.1
ROUTER_USERNAME=admin
ROUTER_PASSWORD=mypass

MQTT_HOST=mqtt.smartrooms.dev
MQTT_USERNAME=smartrooms
MQTT_PASSWORD=pass
MQTT_TOPIC_PREFIX=5fb225099ba3874e9767b64e/5fb2250a9ba3874e9767b652/5fb2250a9ba3874e9767b656/routers/5fb225169ba3874e9767b688
MQTT_CLIENT_ID=5fb2251b9ba3874e9767b6a8
MQTT_RETAIN=0
MQTT_CERT_FILE=/home/openhabian/Cloudflare_CA.pem
```

Add the CLI to the exec() whitelist:

```
nano /etc/openhab2/misc/exec.whitelist
```

Add these:

```
/home/openhabian/mikrotik-cli
/home/openhabian/mikrotik-cli list
/home/openhabian/mikrotik-cli create %2$s
/home/openhabian/mikrotik-cli rename %2$s
/home/openhabian/mikrotik-cli password %2$s
/home/openhabian/mikrotik-cli remove %2$s

```

You can now run commands like this and channel them into OpenHAB.

```
mikrotik-cli list
mikrotik-cli create MyNewWifiNetwork somewifipass --h=192.168.88.1 --u=admin --p=somepass
```

For a complete list of commands:

```
mikrotik-cli

  Mikrotik-cli  1.0

  USAGE: mikrotik-cli <command> [options] [arguments]

  clock         Get the router's clock settings.
  connected     Get the devices connected to the router.
  create        Creates a new wireless network.
  dhcp          Get the router's DHCP data.
  dns           Get the router's DNS settings.
  ethernet      Get the router's ethernet networks.
  flow          Get the router's traffic flow.
  get           Gets any specified router configuration data.
  health        Get the router's health.
  identity      Get the router's network hostname.
  interfaces    Get the router's network interfaces.
  ip-addresses  Get the router's IP addresses.
  ip-pools      Get the router's IP pools.
  license       Get the router's license number.
  ovpn-clients  Get the router's OpenVPN client connections.
  password      Changes a wireless network password.
  proxies       Get the router's proxy settings.
  remove        Removes a wireless network.
  rename        Renames a wireless network.
  resources     Get the router's resource data.
  routerboard   Get the router's routerboard info.
  routing       Get the router's routing settings.
  self-update   Allows to self-update a build application
  services      Get the router's active network services.
  users         Get the router's user accounts.
  wifi          Get the router's wireless networks.
  wifi-profiles Get the router's wireless security profiles.

```

To add to the hourly report:

```
crontab -e
```

Entries:

```
0 0 * * * /home/openhabian/mikrotik-cli clock # Every Day at midnight GMT
*/15 * * * * /home/openhabian/mikrotik-cli connected # Every 15mins
0 0 * * * /home/openhabian/mikrotik-cli dhcp
0 0 * * * /home/openhabian/mikrotik-cli dns
0 0 * * * /home/openhabian/mikrotik-cli ethernet
0 0 * * * /home/openhabian/mikrotik-cli identity
0 0 * * * /home/openhabian/mikrotik-cli interfaces
0 0 * * * /home/openhabian/mikrotik-cli ip-addresses
0 0 * * * /home/openhabian/mikrotik-cli ip-pools
0 0 * * * /home/openhabian/mikrotik-cli license
0 0 * * * /home/openhabian/mikrotik-cli ovpn-clients
*/15 * * * * /home/openhabian/mikrotik-cli resources # Every 15mins
0 0 * * * /home/openhabian/mikrotik-cli routerboard
0 0 * * * /home/openhabian/mikrotik-cli services
0 0 * * * /home/openhabian/mikrotik-cli users
*/15 * * * * /home/openhabian/mikrotik-cli wifi # Every 15mins
*/15 * * * * /home/openhabian/mikrotik-cli wifi-profiles # Every 15mins
```

### Installing Certs

Copy the Cloudflare Root CA (RSA) pem file into `/etc/ssl/certs`. This is needed to verify TLS connections.

### Installing VPN

Run:

```
sudo apt install openvpn
```

Download and place the device's `.ovpn` file into `/etc/openvpn` with a new renamed extension of `.conf`. For example, if you had a file called *smartrooms.ovpn*, you should copy it to */etc/openvpn/smartrooms.conf*.

Check the connection:

```
openvpn --config /etc/openvpn/yourfile.conf
```

Set it to run on startup:

```
sudo systemctl enable openvpn@yourfile
```

Start the service:
```
sudo systemctl start openvpn@yourfile
```

Check to see the network interface has `tun0` entry, and **note the VPN IP address**:

```
ifconfig -a
```

## Logging in to the Web Admin

In your browser, while connected to the same network (replace "x" with the IP of your box):

```
https://192.168.88.x
```

You will receive a privacy error, which is fine. Continue.

Enter your username and password.

## Important: MQTT Setup

Install OpenHAB's MQTT Binding. To connect successfully, "secure" must be checked, and OpenHAB needs the Cloudflare Root CA certificate stored in `/etc/ssl/certs` for the TLS handshake.

### Broker

The main broker cluster is `mqtt.smartrooms.dev`. TLS must be used on port `8883` and the Cloudflare root CA must be supplied by the client (OpenSSL, mosquitto_pub, MQTT Spy, MQTT Explorer etc). Individual keypairs are not required, but the CA is.

Connections over 1883 are only allowed on localhost.

### Change the client ID

The default MQTT client name is "openhab". Each HAB client ID MUST have a unique name - its database ID or serial number - or the MQTT broker is unable to tell the difference between HABs: meaning it will discard (not retain) messages it thinks the client has already seen.
