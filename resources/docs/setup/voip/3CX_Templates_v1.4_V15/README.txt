-------------------------------------------------------------------------------------------
                               		 READ ME                                      
-------------------------------------------------------------------------------------------
- Please remove any system default template for Grandstream (e.g. grandstreamexecutive.ph)
- Please make sure your device has the latest firmware version: http://www.grandstream.com/support/firmware
- For instructions on how to upload the Grandstream phone template see "3CX v15 Templates upload_guide"
- Gateway support has been discontinued. 
- CTI: Conference feature is currently not working, # and * keys are not working 
- 3CX phone for windows v15 may complain about 3CX phone system v15 not detected. To work 
around this issue, first provision the 3CX phone soft phone with an extension by assigning
an extension to 3CX phone in the "phones" section of 3CX system. Then the 3CX soft phone may 
prompt to update. Select yes. After provisioned once, the 3CX phone can change extension by
going to settings->configure accounts and modify the account settings. 
- Starting from v1.3, there exists both VPK and MPK templates. If you load both into 3CX template folder, 
then when you assign an extension to a phone it may default to the MPK template. When this happens, delete 
the chosen template and add the desired MPK/VPK template then re-enter the MAC address. 
- Please change the default parameter TIME_TIMEZONE_GRANDSTREAMEXEC under 
Settings->Parameters prior to using the GDS template to prevent reboot loop issue. (see changelog)