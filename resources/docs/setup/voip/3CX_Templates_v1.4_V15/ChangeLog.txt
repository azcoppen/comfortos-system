################################### v1.0 (10/05/2016) ####################################

1. Modified GXP2130/35/40/60/70 dial plan to use { x+ | \+x+ | *x+ | *xx*x+ | xxx**xxx }. 
   GXP11xx/14xx/21xx will use { x+ | *x+ | *xx*x+ | xxx**xxx }

2. GXP2135 BLF feature revised to use VPK starting from VPK 5 to not conflict with line keys
that may use VPK 1-4.

3. Turn off call features on GXP2135/2170, P191 = 0

4. Updated gateways templates and DP715/750 config template P values. 
(GXW400x/HT502/HT503 1.0.15.5, GXW410x 1.4.1.4, GXW42xx 1.0.5.16, HT70x 1.0.8.2, DP750 1.0.1.20,
DP715 1.0.0.33)

5. Added "FXS Port" section for HT503

6. Fixed config template whitespace issue for all gateway templates and DP715/750

7. Updated network settings section for all GXP devices to prevent overriding of existing settings

8. Commented syslog options on all GXP models

9. Removed incorrect P values P105, P106, P107 on GXP2130/40/60

10. GXV3240/3275 added: P26152, P29604, P29067, P1415, P22104, P8300, P22105, P22106, P22107,
P22108, P22109, P22110, P1464, P2906 

11. GXV3240/3275 updated: P2305, P1685, P8032, P4428, P1301

12. GXV3140/3175/3175v2 updated: P290 

13. Removed gateway folder.

################################### v1.1 (12/15/2016) ####################################

1. Added GAC2500 phone template which contains one model: GAC2500 conference phone

################################### v1.2 (1/11/2017) #####################################

1. Added GXP17xx phone template which contains models: GXP1760, GXP1780, GXP1782 phones.
Note: GXP1760 will use VPK 4-6 for BLF fields 1-3. GXP1780/1782 will use VPK 5-8 for BLF 
fields 1-4.

################################### v1.3 (3/27/2017) #####################################

1. Separated GXP21xx templates into VPK and MPK templates.
Note: You can load both VPK and MPK templates into the 3CX template folder. When you assign an extension to a phone it may default to the MPK template. When this happens, delete the chosen template and add the desired MPK/VPK template then re-enter the MAC address. 

2. Updated GXP17xx template to include rest of VPK P values (24 VPK for GXP1760, 32 VPK for GXP178x).

3. Fixed dial plan not match issue for call park lot SP1, SP2, etc. on GXP21xx, GXP16xx, GXP17xx, GXV32xx and GAC2500

4. Fixed GAC2500 MPK reboot loop issue caused by invalid value field when BLF is set to blank. 
Note: GAC2500 and GXV32xx models cannot reset or remove the MPK once it's set. It can only be overwritten by new MPK setting. 

5. Added Grandstream GDS template. 
Note: Please change the default parameter TIME_TIMEZONE_GRANDSTREAMEXEC under 
Settings->Parameters prior to using this template to prevent reboot loop issue. Alternatively, please set the timezone to any option other than "Use Global Timezone" to avoid this problem. Currently, GDS cannot handle
invalid values in the P64 timezone field. 

################################### v1.4 (5/22/2017) #####################################

1. Updated NTP P values to use 3cx parameter for GXP17xx. 


