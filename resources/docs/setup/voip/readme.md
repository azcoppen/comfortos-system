# 3CX v16 + Grandstream DP750 Base Station + Grandstream DP720 Handsets

## Base Station & Handsets

### Overview

DECT is a technology for cordless handsets. The base station connects to your ethernet connection and manages the non-IP radio connections of each handset. If can manage 5 of them at once - 5 phones with 5 simultaneous calls.

The base station can hold 10 SIP accounts (e.g. 2 per phone, or 10 for 1 phone). Each handset can host 10 lines. A line, SIP address, call etc are all separate. For now, keep it simple: 1 line and 1 SIP account per handset.

Product page: http://www.grandstream.com/products/ip-voice-telephony/dect-cordless/product/dp750

### Web Admin

It's not easy to find the IP of the base station. You'll have to do an IP scan of your network to find it. Once you do, just open http://BASE_STATION_IP to get the web GUI. Default username is **admin** and no password.

### First Steps

 - Make a note of the mac address (Status > Network Status)
 - Update the firmware (current is v1.0.9.9)
 - Set passwords for the web GUI and SSH (Maintenance > Web/SSH Access)
 - Set the base station name (DECT > General Settings)
 - Switch on STUN and uPnP (Settings > Network > Advanced Settings)
 - Enable VPN (Settings > Network > OpenVPN Settings)

### Connecting Handsets

Press the "wifi" icon button on the back of the box: this will put it into "registration mode" and broadcast itself as available. On the new phone, the screen will show "subscribe" in the window. Press, and select the first "base" option. It will auto-connect.

Within the phone itself, set the **date/time** correctly. Hit the middle "select" button to access the menu, go to "settings". The default pin is 0000. Give the handset a **name**.

### Assigning phone lines

Do the PBX setup first. See the next sections.

### API

The 750 does **not** have an API.

## 3CX PBX

3CX is a massive pain in the ass. Don't use a trial account: set it up within your own cloud account where you have SSH access.

There are two typical ways to use a PBX: **inside the LAN**, or **remotely**. 3CX is configured by default to work INSIDE one. It needs to be configured to recognise "remote" phones when it's a cloud install.

Everything that happens with base stations and phones (e.g. SIP registration etc) can be seen in the **event log** (dashboard).

### Server setup tips

- The initial HTTP setup screens are done on port 5015. This is only for the first run. You can choose ports 80 and 443 for normal service later.
- 3CX licensing registers a license key against a domain name (FQDN, e.g. myserver.3cx.us). Whenever you run a new install, it will ask you for that license key, and provison the DNS subdomain you have registered with them against the box you set it up on.
- You'll be prompted to create a default "operator" (000) extension.

### Whitelist important IPs

3CX will auto-ban failed login attempts for 24hrs. If you lose your password, you're in trouble. **Before anything else**, whitelist office and VPN IP addresses so this doesn't happen.

### Grandstream DP range isn't officially supported

3CX uses XML "templates" to provision phones and DECT base stations. The DP range aren't officially supported, but Grandstream do provide some. Download them here: https://www.tecfused.com/wp-content/uploads/2017/11/3CX_Templates_v1.4_V15.zip (look for the DP750 XML file). These treat the base station as the individual phone: so although the phone is the DP720, the template is for the DP750.

Install the template by going to _Settings > Templates > Add_. Upload the DP750 xml file, which should make it appear in the list. Uploads are disabled on the trial servers.

Each phone you provision will have this template show up in red to indicate it's a custom/unsupported one.

### Set up some extensions

This is straightforward: add the extension number to dial, first/last name, email etc. For every extension, go to the "Options" tab and **uncheck "Disallow use of extension outside the LAN"**.

## Map Handsets To PBX Lines

### Overview

This is where it gets complex. 2 things are needed: a) the base station needs to know how to talk to the PBX, and the PBX needs to know who is talking to it.

### Add a shared PBX profile to the DP750 base station

First, each of your handsets should have a **name** and be **subscribed** to the base station with **updated firmware**. Check this in _Status > DECT Base Status_.

Next, we need to set up a profile for the PBX (Profile 1). Go to _Profiles > Profile 1 > General Settings_:

- Active: Yes
- Name: Whatever (3cx or something recognisable)
- SIP Server: URL of your PBX box (myserver.3cx.us)
- Fallover: leave blank
- Prefer: Yes
- Outbound Proxy: URL of your PBX box (myserver.3cx.us)
- Voicemail Num: Go to _3CX Admin > Settings > Voicemail Settings_ and check (should be 999)

Save and Apply. Then, go to _Profile 1 > Network Settings_ and make sure NAT Traversal is set to **STUN**. STUN is a protocol which works through port 443 to help get through NAT.

### Add a phone to the PBX

In 3CX admin, go to your chosen extension, and select the "Phone Provisioning" tab. An extension can have 500 phones attached to it, if you want. But we'll just add a single handset. Click "Add". In this part, we're telling it the handset's preferences (audio codec etc).

Scroll down the list and look for the **custom DP750 xml template**. The mac address is the mac address of the **base station, NOT the phone**. Add this **WITHOUT colons** (copy/paste the mac address string and manual remove all : characters so it is a single string of letters and numbers). If you leave the colons in, you'll get a dumb message saying "that isn't a mac address" (when it actually is).

Below it, change the provisioning method to **STUN**.

Click OK at the top.

That has told the PBX that for _this extension, this particular type of handset will be connecting to it, this way_.

### Give the extension details to the phone

Back in the DP720 base station, we need to give each phone the details they need to provide to the PBX. Go to _DECT > SIP Account Settings_. This page is a "map" of handsets and phone lines.

For each account:

 - SIP User ID - the extension number
 - Authenticate ID - go to the extension's page in 3CX, scroll down to Authentication (_General > Authentication_), copy/paste the details.
 - Password - go to the extension's page in 3CX, scroll down to Authentication (_General > Authentication_), copy/paste the details.
 - Name - whatever you want
 - Profile - Profile 1 (the 3CX SIP server)
 - Active - Yes

 Click Save and Apply. This will download the SIP settings onto the handset and display them in its LCD menu under "settings".

 That has told the phone to look up Profile1 as the main PBX and SIP details, and stored the SIP user (which translates to extension-number@sip-server-in-profile1) and auth information for it to give to the PBX.

## Testing Phone Lines

 Check the 3CX event log to see it's all registered OK.

 - In _Status > Account Status_ of the base station, "SIP Registration" should be a GREEN bar saying "YES" with a green phone icon, for each handset.
 - Each handset should display a GREEN "user" icon with the account name (prob extension number).
 - In the 3CX extensions list, each handset which is registered should have a GREEN dot next to it (red = offline)

Lastly, download the 3CX softphone for iOS/Android: https://www.3cx.com/VOIP/softphone/ . Go to any extension setup page, and scan the QR code into it. It will automatically set itself up.

Dial the extension of one handset from another. After a second or two, it should dial, then ring. Like a normal phone.
