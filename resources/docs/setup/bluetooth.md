# Set up BROKEN OH Bluetooth

Bluetooth is completely fucked on OH because the Java libraries aren't installed properly.

## Fix

```
wget https://github.com/openhab/openhab-addons/raw/2.5.x/bundles/org.openhab.binding.bluetooth.bluez/src/main/resources/lib/armv6hf/libtinyb.so
wget https://github.com/openhab/openhab-addons/raw/2.5.x/bundles/org.openhab.binding.bluetooth.bluez/src/main/resources/lib/armv6hf/libjavatinyb.so
sudo cp lib* /opt/jdk/zulu8.48.3.246-ca-jdk8.0.265-linux_aarch32hf/jre/lib/aarch32/
sudo apt purge bluez
wget http://archive.raspberrypi.org/debian/pool/main/b/bluez/bluez_5.43-2+rpt2+deb9u2_armhf.deb
sudo dpkg -i bluez_5.43-2+rpt2+deb9u2_armhf.deb
sudo apt-get install pi-bluetooth
sudo usermod -a -G bluetooth openhab
sudo reboot
```

## Pi Setup

```
sudo apt-get install pi-bluetooth
sudo apt-get install bluetooth bluez blueman
```

Then


```
rfkill unblock bluetooth
bluetoothctl
system-alias 'SmartRooms-HAB'
power on
show
discoverable on
agent on
scan on
pair 6C:C4:D5:6C:C5:BC
trust 6C:C4:D5:6C:C5:BC
```
