type: object

properties:

  _id:
    description: Identifer of the record
    type: string

  prefix:
    description: The user's title or pronoun
    type: string
    default: Mr

  first:
    description: The user's first (given) name
    type: string
    default: John

  last:
    description: The user's last (family) name
    type: string
    default: Doe

  display:
    description: The preferred way to display the user's full name
    type: string
    default: John Doe

  slug:
    description: The user's slug/vanity URL
    type: string
    default: first-last-randomstr

  email:
    description: The user's login email address
    type: string
    format: email
    default: first.last@example.com

  password:
    description: The user's login password
    type: string
    minimum: 5

  dob:
    description:  The user's date of birth (Y-m-d)
    type: string
    format: date
    default: '1970-01-01'

  sex:
    description: The user's biological sex
    type: string
    enum: [M, F, X]
    default: 'X'

  job_title:
    description: The user's job title
    type: string
    default: empty

  location:
    description: Formatted dictionary of the user's location
    type: object
    properties:
      street:
        type: string
      city:
        type: string
      region:
        type: string
      postal:
        type: string
        maximum: 5
      country:
        type: string
        enum: [US,CA,UK]
        maximum: 2
        default: US

  geo:
    description: A GeoJSON object specifying the object's geographical location
    type: object
    properties:
      type:
        type: string
      coordinates:
        type: array
        items:
          type: integer

  telephones:
    description: A list of E164 internationally-formatted phone numbers
    type: array
    items:
      type: string

  lang:
    description: A 2-digit ISO language code
    type: string
    enum: [en,fr,es]
    default: en

  locale:
    description: An official locale code/string
    type: string
    default: en-US

  timezone:
    type: string
    description: The timezone the user is in
    enum: [UTC,EST,CST,PST,America/New_York]
    default: America/New_York

  currency:
    type: string
    description: The currency the user pays in
    enum: [USD, CAD, GBP, EUR]
    default: USD

  tags:
    description: A list of searchable tags applied to the object
    type: array
    items:
      type: string

  last_login_at:
    description:  A UTC datetime in ISO 8601 format specifying when the user last authenticated a JWT token
    type: string
    format: date-time
    default: '2004-02-12T15:19:21+00:00'

  last_activity_at:
    description:  A UTC datetime in ISO 8601 format specifying the last time the user made a request to the API
    type: string
    format: date-time
    default: '2004-02-12T15:19:21+00:00'

  email_verified_at:
    description:  A UTC datetime in ISO 8601 format specifying when the user's account was confirmed
    type: string
    format: date-time
    default: '2004-02-12T15:19:21+00:00'

  pwd_changed_at:
    description:  A UTC datetime in ISO 8601 format specifying when the user's password was last changed
    type: string
    format: date-time
    default: '2004-02-12T15:19:21+00:00'

  pwd_expires_at:
    description:  A UTC datetime in ISO 8601 format specifying when the user's password will expire
    type: string
    format: date-time
    default: '2004-02-12T15:19:21+00:00'

  created_at:
    description:  A UTC datetime in ISO 8601 format specifying when the record was created
    type: string
    format: date-time
    default: '2004-02-12T15:19:21+00:00'

  updated_at:
    description:  A UTC datetime in ISO 8601 format specifying when the record was updated
    type: string
    format: date-time
    default: '2004-02-12T15:19:21+00:00'

  creator:
    description: System user object representing the person who created the record (if manually entered)
    $ref: '#/components/schemas/User'


required:
  - first
  - last
  - email
  - location
