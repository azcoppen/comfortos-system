type: object

properties:

  _id:
    description: Identifer of the record
    type: string

  group:
    description: Which group of device types the record belongs to
    type: string
    enum: [bulbs, cameras, contact-sensors, dimmers, hubs-bridges, led-strips, locks, multi-sensors, plugs, smoke-detectors, speakers, switches, thermostats, tvs]

  installation_id:
    description: Internal identifier of the IoT installation
    type: string

  remote_id:
    description: External remote ID of the device registered on the network
    type: string

  api:
    description: Attached copy of the external remote data object for the device
    type: object
    properties:

      deviceId:
        description: Remote ID of the device callable to the external API
        type: string

      type:
        description: How the device is connected to the external network
        type: string
        default: DTH

      name:
        description: Identification of the device in the application setup
        type: string

      label:
        description: Friendly human name given to the device by the consumer user
        type: string

      deviceManufacturerCode:
        description: Serial number of SKU allocated to the device by the manufacturer
        type: string

      locationId:
        description: Location object where the device was originally installed
        type: string

      roomId:
        description: Room/group object where the device was originally installed
        type: string

      deviceTypeId:
        description: External/remote ID of the device type
        type: string

      deviceTypeName:
        description: Friendly human name given to the device type by the driver author
        type: string

      deviceNetworkType:
        description: Communication network or protocol used by the device
        type: string
        enum: [ZWAVE, ZIGBEE, LAN, UNKNOWN]

      dth:
        type: object
        properties:

          deviceTypeId:
            description: External/remote ID of the device type
            type: string

          deviceTypeName:
            description: Friendly human name given to the device type by the driver author
            type: string

          deviceNetworkType:
            description: Communication network or protocol used by the device
            type: string
            enum: [ZWAVE, ZIGBEE, LAN, UNKNOWN]

          completedSetup:
            description: Whether or not the device has finished setup
            type: boolean
            default: true

          networkSecurityLevel:
            description: The security access level protections employed by the device's environment
            type: string
            default: ZWAVE_LEGACY_NON_SECURE

          hubId:
            description: Hub object where the device was originally installed
            type: string

      components:
        type: array
        items:
          type: object
          properties:
              id:
                description: Internal ID of the device component
                type: string
                default: main

              label:
                description: Friendly name of the component
                type: string

              capabilities:
                description: List of the device's individual capabilities
                type: array
                items:
                  type: object
                  properties:
                      id:
                        description: Alphanumeric (snake/camel case) name of the capability
                        type: string
                      version:
                        description: Which API version the capability is
                        type: integer
                        default: 1
