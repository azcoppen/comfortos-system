    get:
      tags:
         - Wifi Networks
      summary: "Display a single wifi network record"
      description: "Access a detailed representation of a wifi network record, including its deeper relationships."
      security:
        - JWT: [wifi.view]
      parameters:
        - $ref: '#/components/parameters/JSONHeader'
        - $ref: '#/components/parameters/EagerLoad'
        - $ref: '#/components/parameters/EagerCount'
        - $ref: '#/components/parameters/Include'
        - name: wifi_network_id
          in: path
          description: ID of wifi network to return
          required: true
          schema:
            type: string
      responses:
        200:
          description: A single item record
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/WifiNetwork"
        401:
          $ref: '#/components/responses/UnauthorizedError'
        405:
          $ref: '#/components/responses/MethodNotAllowedError'
        406:
          $ref: '#/components/responses/NotAcceptableError'
        429:
          $ref: '#/components/responses/RateLimitingError'
        500:
          $ref: '#/components/responses/InternalServerError'
        502:
          $ref: '#/components/responses/BadGatewayError'
        503:
          $ref: '#/components/responses/ServiceUnavailableError'
        504:
          $ref: '#/components/responses/GatewayTimeoutError'

    patch:
      tags:
         - Wifi Networks
      summary: "Change the details of a wifi network"
      security:
        - JWT: [wifi.update]
      parameters:
        - $ref: '#/components/parameters/JSONHeader'
        - name: wifi_network_id
          in: path
          description: ID of wifi network to change
          required: true
          schema:
            type: string
      requestBody:
          description: "A list of key:value fields and values which should fill the object's properties"
          required: true
          content:
            application/x-www-form-urlencoded:
              schema:
                type: object
                properties:
                  name:
                    description: Name or label of the wifi network (SSID), letters/numbers/hyphens/underscores only
                    type: string
                    minimum: 5
                  password:
                    description: Password to join the wifi network
                    type: string
                    minimum: 8
                  expire_at:
                    description: A UTC datetime in ISO 8601 format specifying when the network should auto-delete
                    type: string
                    format: date-time
                    default: '2004-02-12T15:19:21+00:00'
      responses:
        200:
          description: A representation of the updated record
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/WifiNetwork"
        401:
          $ref: '#/components/responses/UnauthorizedError'
        405:
          $ref: '#/components/responses/MethodNotAllowedError'
        406:
          $ref: '#/components/responses/NotAcceptableError'
        429:
          $ref: '#/components/responses/RateLimitingError'
        500:
          $ref: '#/components/responses/InternalServerError'
        502:
          $ref: '#/components/responses/BadGatewayError'
        503:
          $ref: '#/components/responses/ServiceUnavailableError'
        504:
          $ref: '#/components/responses/GatewayTimeoutError'

    delete:
      tags:
         - Wifi Networks
      summary: "Delete a single wifi network record"
      security:
        - JWT: [wifi.destroy]
      parameters:
        - $ref: '#/components/parameters/JSONHeader'
        - name: wifi_network_id
          in: path
          description: ID of wifi network to remove
          required: true
          schema:
            type: integer
            format: int64
      responses:
        200:
          $ref: '#/components/responses/Deleted'
        401:
          $ref: '#/components/responses/UnauthorizedError'
        405:
          $ref: '#/components/responses/MethodNotAllowedError'
        406:
          $ref: '#/components/responses/NotAcceptableError'
        429:
          $ref: '#/components/responses/RateLimitingError'
        500:
          $ref: '#/components/responses/InternalServerError'
        502:
          $ref: '#/components/responses/BadGatewayError'
        503:
          $ref: '#/components/responses/ServiceUnavailableError'
        504:
          $ref: '#/components/responses/GatewayTimeoutError'
