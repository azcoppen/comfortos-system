    get:
      tags:
         - Thermostats
      summary: "Display a single thermostat record"
      description: "Access a detailed representation of a thermostat record, including its deeper relationships."
      security:
        - JWT: [thermostats.view]
      parameters:
        - $ref: '#/components/parameters/JSONHeader'
        - $ref: '#/components/parameters/EagerLoad'
        - $ref: '#/components/parameters/EagerCount'
        - $ref: '#/components/parameters/Include'
        - name: thermostat_id
          in: path
          description: ID of thermostat to return
          required: true
          schema:
            type: string
      responses:
        200:
          description: A single item record
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Thermostat"
        401:
          $ref: '#/components/responses/UnauthorizedError'
        405:
          $ref: '#/components/responses/MethodNotAllowedError'
        406:
          $ref: '#/components/responses/NotAcceptableError'
        429:
          $ref: '#/components/responses/RateLimitingError'
        500:
          $ref: '#/components/responses/InternalServerError'
        502:
          $ref: '#/components/responses/BadGatewayError'
        503:
          $ref: '#/components/responses/ServiceUnavailableError'
        504:
          $ref: '#/components/responses/GatewayTimeoutError'

    patch:
      tags:
         - Thermostats
      summary: "Change the state of a thermostat"
      security:
        - JWT: [thermostats.update]
      parameters:
        - $ref: '#/components/parameters/JSONHeader'
        - name: thermostat_id
          in: path
          description: ID of thermostat to change
          required: true
          schema:
            type: string
      requestBody:
          description: "A list of key:value fields and values which should fill the object's properties"
          required: true
          content:
            application/x-www-form-urlencoded:
              schema:
                type: object
                properties:
                  power:
                    type: string
                    enum: [on, off]
                  mode:
                    description: The mode the device should operate in
                    type: string
                    enum: [auto, eco, rush hour, cool, emergency heat, heat, off]
                  fan:
                    description: How the fan should operate
                    type: string
                    enum: [auto, circulate, followschedule, on]
                  unit:
                    type: string
                    enum: [C, F]
                  cooling_point:
                    description: The temperature threshold at which cooling should be triggered
                    type: integer
                    minimum: 50
                    maximum: 100
                  heating_point:
                    description: The temperature threshold at which heating should be triggered
                    type: integer
                    minimum: 50
                    maximum: 100
                  execute_at:
                    description: A UTC datetime in ISO 8601 format specifying when the command should take place
                    type: string
                    format: date-time
                    default: '2004-02-12T15:19:21+00:00'
      responses:
        200:
          description: A representation of the updated record
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Thermostat"
        401:
          $ref: '#/components/responses/UnauthorizedError'
        405:
          $ref: '#/components/responses/MethodNotAllowedError'
        406:
          $ref: '#/components/responses/NotAcceptableError'
        429:
          $ref: '#/components/responses/RateLimitingError'
        500:
          $ref: '#/components/responses/InternalServerError'
        502:
          $ref: '#/components/responses/BadGatewayError'
        503:
          $ref: '#/components/responses/ServiceUnavailableError'
        504:
          $ref: '#/components/responses/GatewayTimeoutError'
