<?php

return [
  'title' => "Language",
  'subtitle' => "Available Languages",

  'en' => "English",
  'fr' => "Francais",
  'es' => "Espanol",
  'de' => "Deutsche",
  'it' => "Italiano",
  'pt' => "Português",
];
