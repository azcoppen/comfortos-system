<?php

return [
  'title'       => "TV",
  'breadcrumb'  => "Televisions, media players, and movies",
  'empty'       => "No TV devices have been set up for this room yet.",
  'tv_placeholder' => "Unnamed TV",
  "chromecast_placeholder" => "Unnamed Chromecast",
  'channel_placeholder' => "Channel number or name",
  'keycode_placeholder' => "Remote control keycode",
  'channel_enter' => 'Enter',
  'keycode_enter' => 'Enter',
];
