<?php

return [
  'on' => 'on',
  'off' => 'off',
  'yes' => 'yes',
  'no' => 'no',
  'open' => 'open',
  'closed' => 'closed',
  'clear' => 'clear',
  'contact'     => 'Door',
  'motion'      => 'Motion',
  'temperature' => 'Temperature',
  'humidity'    => 'Humidity',
  'luminance'   => 'Luminance',
  'uv'          => 'Ultraviolet',
  'flood'       => 'Flood',
  'smoke'       => 'Smoke',
  'co'          => 'CO',
];
