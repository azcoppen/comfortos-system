<?php

return [
  'guide'       => "Select &quot;Allow&quot; when your device asks for permission to tell us where you are. We'll find the rooms nearest to you.",
  'geo_btn'     => "I Didn't See A Prompt! Try Again!",
  'browser_err' => 'Your browser does not support geolocation.',
  'empty'       => "We couldn't find any rooms nearby. :(",
  'empty_extra' => 'These are the GPS map coordinates your device gave us when we requested them. In some cases, your device or browser might try its best guess from your Internet address, which is often not very accurate and more related to where your Internet provider places the connection (such as their office which your device talks to). GPS chips in a smartphone provide the most accurate results.',
  'title'       => "Which room are you in?",
  'room'        => 'Room #',
  'locked'      => 'Another guest is logged in.',
  'available'   => 'Controls are available.',
];
