<?php

return [
  'menu'  => 'MENU',
  'level' => 'level',
  'speed' => 'speed',
  'cooling' => 'start cool',
  'heating' => 'start heat',
  'volume' => 'volume',
];
