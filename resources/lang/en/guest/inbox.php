<?php

return [
  'title' => 'Inbox',
  'breadcrumb' => 'Messages, Voicemails, Notifications',
  'empty' => "No lighting has been set up for this room yet.",
  "from" => "From",
  'sidebar' => [
    'compose_btn' => 'Compose',
    'folders' => 'Folders',
    'messages' => 'Messages',
    'voicemails' => 'Voicemails',
    'faxes' => 'Faxes',
    'contacts' => 'Contacts',
  ],
  "contacts" => [
    "Front Desk" => "Building Reception",
    "Concierge" => "Services &amp; Bookings",
    "Restaurant" => "Food &amp; Drink",
    "Laundry" => "Washing &amp; Dry Cleaning",
    "Valet" => "Parking &amp; Taxis",
  ],
  "placeholder" => [
    'subject' => "Ask your SmartRooms account manager how to set up unified messaging",
    "sender" => "SmartRooms Access Bot",
    "content" => "  <p>Hi there,</p>
      <p>You're seeing this because messaging isn't set up for this room yet. To enable it, the room needs to be configured by a technician once it has been requested by the sales department.</p>
      <p>Once it is installed, ComfortOS provides instant messaging, SMS, MMS, emails, voicemails, and faxes right here, all together in one place.</p>",
  ],
];
