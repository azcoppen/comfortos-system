<?php

return [
  'title'       => "Wifi",
  'breadcrumb'  => "Personal wifi networks",
  'empty'       => "No wifi networks have been set up for this room yet.",
  'wifi_placeholder' => "Unnamed Wifi Network",
  'create_title' => "Create A New Wifi Network",
  'store' => [
    'label_title' => "What do you want to call it?",
    'label_placeholder' => "My New Wifi Network",
    'ssid_title' => "What name (SSID) should it broadcast?",
    'ssid_placeholder' => "New-Room-Wifi",
    'password_title' => "What password do you want?",
    'password_placeholder' => "8characterpassword",
    "expire_date_title" => "Expire on",
    "expire_date_placeholder" => "m/d/Y",
    "expire_time_title" => "At ",
    "expire_time_placeholder" => "H:m",
    "submit_btn" => "Create",
    'cancel_btn' => "Cancel",
  ],
];
