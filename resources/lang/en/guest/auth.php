<?php

return [
  'offline' => 'You are currently offline. ComfortOS requires an internet connection.',
  "unavailable" => "This ComfortOS object is not currently available.",
  "violation" => "You do not have permission to view that.",
  'login' => [
    'existing' => "Someone else is already logged in.",
    'form' => [
      'otp_required'  => "You must provide a guest code.",
      'otp_present'   => "You must provide a guest code.",
      'otp_filled'    => "The guest code can't be empty.",
      'otp_string'    => "The guest code must be a string.",
      'otp_numeric'   => "The guest code must be numeric.",
      'otp_min'       => "The guest code must be at least 8 characters.",
      'otp_max'       => "The guest code can't be more than 20 characters.",
      'otp_format'    => "The guest code isn't in the right format."
    ],
    'errors' => [
      "forbidden" => "This type of component cannot be controlled by a guest.",
      "object" => "There was a problem identifying what you want to log into.",
      "expired" => "Your guest code has expired.",
      'session' => "You must sign in with a guest code.",
      'no_users' => "Unable to load user.",
      'invalid_code' => "Code is invalid or has expired.",
      "conflict" => "Your code worked, but someone else is already logged in. Only one person can be logged in at a time.",
    ],
    'labels' => [
      'login_btn' => "OPEN",
      'need_code' => "Contact the property manager if you don't have a guest code.",
    ]
  ],

];
