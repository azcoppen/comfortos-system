<?php

return [
  'title'       => "Motors",
  'breadcrumb'  => "Curtains, blinds, and rotary devices",
  'empty'       => "No motors have been set up for this room yet.",
  'motor_placeholder' => "Unnamed Motor",
];
