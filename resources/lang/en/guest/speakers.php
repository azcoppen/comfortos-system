<?php

return [
  'title'       => "Speakers",
  'breadcrumb'  => "Audio speakers and music",
  'empty'       => "No speakers have been set up for this room yet.",
  'speaker_placeholder' => "Unnamed Speaker",
  'mode_placeholder' => 'Choose an audio mode...',
  'playing' => 'playing',
  'nothing' => 'nothing',
];
