<?php

return [
  'title'       => "Power",
  'breadcrumb'  => "Plug outlets and electricity switches",
  'empty'       => "No power sources have been set up for this room yet.",
  'plug_placeholder' => "Unnamed Plug",
];
