<?php

return [
  'title'       => "Lock",
  'breadcrumb'  => "Door lock and access codes",
  'empty'       => "No locks have been set up for this room yet.",
  'lock_placeholder' => "Unnamed Lock",
  'expiry'      => 'Expire: ',
  'code'        => 'Digits (> 5)',
];
