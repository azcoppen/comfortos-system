<?php

return [
  'title'         => "Welcome to ComfortOS&trade;",
  'session_days'  => '%d days, %h hours, %i minutes left',
  'session_hours' => '%h hours, %i minutes left',
  'session_mins'  => '%i minutes left',
];
