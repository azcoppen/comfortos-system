<?php

return [
  'title'       => "Phone",
  'breadcrumb'  => "Internal telephone line extension",
  'empty'       => "No phones have been set up for this room yet.",
  'ext_placeholder' => "Unnamed Extension",
  'contacts' => [
    'Front Desk',
    'Concierge',
    'Restaurant',
    'Laundry',
    'Rm Service',
    'Bar',
    'Valet',
    'Biz Center',
    'Gym',
    "External",
  ],
];
