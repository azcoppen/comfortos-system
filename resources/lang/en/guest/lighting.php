<?php

return [
  'title' => 'Lighting',
  'breadcrumb' => 'Door lock and access codes',
  'empty' => "No lighting has been set up for this room yet.",
  'bulb_placeholder' => 'Unnamed Bulb',
  'led_placeholder' => 'Unnamed LED',
];
