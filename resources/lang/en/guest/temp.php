<?php

return [
  'title'       => "Temperature",
  'breadcrumb'  => "Thermostats, heating, and A/C",
  'empty'       => "No thermostats have been set up for this room yet.",
  'thermostat_placeholder' => "Unnamed Thermostat",
  'mode_placeholder' => "Choose a thermostat mode...",
];
