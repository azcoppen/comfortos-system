<?php

declare(strict_types = 1);

namespace SmartRooms\Charts;

use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

use SmartRooms\Models\Property\Room;

use Illuminate\Support\Collection;

use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABCommandThroughputRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABCommandThroughputRepository;


use SmartRooms\Criteria\WhereFieldEqualsCriteria;
use SmartRooms\Criteria\WhereFieldGTECriteria;
use SmartRooms\Criteria\WhereFieldLTECriteria;

abstract class DayTimelineChart extends BaseChart
{
  public $repository;
  public $component;
  public $room;

  public function component ($component) : self
  {
    $this->component = $component;
    $this->repository = $this->repositories['component'];

    return $this;
  }

  public function room (Room $room) : self
  {
    $this->room = $room;
    $this->repository = $this->repositories['room'];

    return $this;
  }

  public function structurize (Request $request) : Collection
  {
    $structure = collect ([]);

    $from = now()->subHours(23);

    if (! $this->repository )
    {
      $this->repository = $this->repositories['global'];
    }

    $series =
      app ($this->repository)
        ->pushCriteria (new WhereFieldGTECriteria('dt', $from))
        ->pushCriteria (new WhereFieldLTECriteria('dt', $from->copy()->addHours(24)));

    if ( $this->room && is_object ($this->room) )
    {
      $series = $series->pushCriteria (new WhereFieldEqualsCriteria ('room_id', $this->room->_id) );
    }

    if ( $this->component && is_object ($this->component) )
    {
      $series = $series->pushCriteria (new WhereFieldEqualsCriteria ('component_id', $this->component->_id) );
    }

    $series = $series->all()->transform(function ($item, $key) {
            $item->ts = $item->ts / 1000;
            return $item;
        });


    $data = collect ([])->push ([
      'ts'    => $from->copy()->timezone($request->user()->timezone)->hour,
      'total' => 0,
    ]);

    $current = $from->addHours(1);

    for ($i = 0; $i < 23; $i++)
    {
      $hit = $series->where('h', $current->hour)->first();

      $data->push ([
        'ts'    => $current->copy()->timezone($request->user()->timezone)->hour,
        'total' => $hit ? $hit->total : 0
      ]);

      $current = $current->addHours(1);
    }

    $structure
      ->put ('labels', $data->pluck ('ts')->all())
      ->put ('data', $data->pluck ('total')->all());

    return $structure;
  }

}
