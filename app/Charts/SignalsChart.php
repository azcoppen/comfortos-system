<?php

namespace SmartRooms\Charts;

use Chartisan\PHP\Chartisan;

use Illuminate\Http\Request;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABSignalThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABSignalThroughputRepository;

class SignalsChart extends HourTimelineChart
{
  public $repositories = [
    'global'    => HABSignalThroughputRepository::class,
    'room'      => RoomHABSignalThroughputRepository::class,
    'component' => ComponentHABSignalThroughputRepository::class,
  ];

  public function handler (Request $request): Chartisan
  {
      $structure = $this->structurize ($request);

      return Chartisan::build()
          ->labels ($structure->get('labels'))
          ->dataset ('Signals/Min', $structure->get('data'));
  }
}
