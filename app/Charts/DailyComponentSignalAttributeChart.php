<?php

namespace SmartRooms\Charts;

use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;

use SmartRooms\Criteria\WhereFieldEqualsCriteria;
use SmartRooms\Criteria\WhereFieldGTECriteria;
use SmartRooms\Criteria\WhereFieldLTECriteria;

class DailyComponentSignalAttributeChart extends BaseChart
{
    public $component;
    public $attribute;

    public function component ($component) : self
    {
      $this->component = $component;
      $this->repository = HABSignalRepository::class;

      return $this;
    }

    public function attribute (string $attribute) : self
    {
      $this->attribute = $attribute;

      return $this;
    }

    public function format_value ( $value )
    {
      if ( is_string ($value) || is_numeric ($value) && !empty ($value) )
      {
        if ( in_array ($value, ['ON', 'OFF']) )
        {
          return strtolower($value)  == 'on' ? 100 : 5;
        }

        if ( Str::contains ($value, ',') )
        {
          return intval(Str::afterLast ($value, ',')) > 0 ? 100 : 5;
        }

        return floatval($value);
      }

      if ( is_array ($value) && isset ($value[2]) )
      {
        return $value[2] > 0 ? 100 : 5;
      }

      return 0;
    }


    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {

        $series =
          app ($this->repository)->scopeQuery(function($query) {
              return $query->orderBy('received_at','ASC')->take (200);
          });

        if ( $this->component && is_object ($this->component) )
        {
          $series = $series->pushCriteria (new WhereFieldEqualsCriteria ('component_id', $this->component->_id) );
        }

        if ( $this->attribute )
        {
          $series = $series->pushCriteria (new WhereFieldEqualsCriteria ('attribute', $this->attribute) );
        }

        $series = $series->all();

        $labels = $series->map(function ($item, $key) use ($request) {
                return $item->received_at->timezone ($request->user()->timezone)->format ('M d H:i:s');
        })->all();

        $values = $series->map(function ($item, $key) use ($request) {
          return $this->format_value ($item->value);
        })->all();

        return Chartisan::build()
            ->labels($labels)
            ->dataset($this->attribute.' value', $values);
    }
}
