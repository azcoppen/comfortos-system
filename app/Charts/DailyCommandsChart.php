<?php

namespace SmartRooms\Charts;

use Chartisan\PHP\Chartisan;

use Illuminate\Http\Request;

use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABCommandThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABCommandThroughputHourlyRepository;

class DailyCommandsChart extends DayTimelineChart
{
  public $repositories = [
    'global'    => HABCommandThroughputHourlyRepository::class,
    'room'      => RoomHABCommandThroughputHourlyRepository::class,
    'component' => ComponentHABCommandThroughputHourlyRepository::class,
  ];

  public function handler (Request $request): Chartisan
  {
      $structure = $this->structurize ($request);

      return Chartisan::build()
          ->labels ($structure->get('labels'))
          ->dataset ('Commands/Hr', $structure->get('data'));
  }
}
