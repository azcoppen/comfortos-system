<?php

namespace SmartRooms\Charts;

use Chartisan\PHP\Chartisan;

use Illuminate\Http\Request;

use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentMIAAlertThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomMIAAlertThroughputHourlyRepository;

class DailyMIAChart extends DayTimelineChart
{
  public $repositories = [
    'global'    => MIAAlertThroughputHourlyRepository::class,
    'room'      => RoomMIAAlertThroughputHourlyRepository::class,
    'component' => ComponentMIAAlertThroughputHourlyRepository::class,
  ];

  public function handler (Request $request): Chartisan
  {
      $structure = $this->structurize ($request);

      return Chartisan::build()
          ->labels ($structure->get('labels'))
          ->dataset ('Alerts/Hr', $structure->get('data'));
  }
}
