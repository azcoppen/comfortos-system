<?php

namespace SmartRooms\Charts;

use Chartisan\PHP\Chartisan;

use Illuminate\Http\Request;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABSignalThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABSignalThroughputHourlyRepository;

class DailySignalsChart extends DayTimelineChart
{
  public $repositories = [
    'global'    => HABSignalThroughputHourlyRepository::class,
    'room'      => RoomHABSignalThroughputHourlyRepository::class,
    'component' => ComponentHABSignalThroughputHourlyRepository::class,
  ];

  public function handler (Request $request): Chartisan
  {
      $structure = $this->structurize ($request);

      return Chartisan::build()
          ->labels ($structure->get('labels'))
          ->dataset ('Signals/Hr', $structure->get('data'));
  }
}
