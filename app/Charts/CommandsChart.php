<?php

namespace SmartRooms\Charts;

use Chartisan\PHP\Chartisan;

use Illuminate\Http\Request;

use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABCommandThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABCommandThroughputRepository;

class CommandsChart extends HourTimelineChart
{
  public $repositories = [
    'global'    => HABCommandThroughputRepository::class,
    'room'      => RoomHABCommandThroughputRepository::class,
    'component' => ComponentHABCommandThroughputRepository::class,
  ];

  public function handler (Request $request): Chartisan
  {
      $structure = $this->structurize ($request);

      return Chartisan::build()
          ->labels ($structure->get('labels'))
          ->dataset ('Commands/Min', $structure->get('data'));
  }
}
