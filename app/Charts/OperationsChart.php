<?php

namespace SmartRooms\Charts;

use Chartisan\PHP\Chartisan;

use Illuminate\Http\Request;

use SmartRooms\Contracts\Repositories\ComfortOS\OpQueueThroughputRepository;

class OperationsChart extends HourTimelineChart
{
    public $repositories = [
      'global' => OpQueueThroughputRepository::class,
    ];

    public function handler (Request $request): Chartisan
    {
        $structure = $this->structurize ($request);

        return Chartisan::build()
            ->labels ($structure->get('labels'))
            ->dataset ('Operations/Min', $structure->get('data'));
    }
}
