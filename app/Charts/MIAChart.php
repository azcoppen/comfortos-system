<?php

namespace SmartRooms\Charts;

use Chartisan\PHP\Chartisan;

use Illuminate\Http\Request;

use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertThroughputRepository;

class MIAChart extends HourTimelineChart
{
    public $repositories = [
      'global' => MIAAlertThroughputRepository::class,
    ];

    public function handler (Request $request): Chartisan
    {
        $structure = $this->structurize ($request);

        return Chartisan::build()
            ->labels ($structure->get('labels'))
            ->dataset ('Alerts/Min', $structure->get('data'));
    }
}
