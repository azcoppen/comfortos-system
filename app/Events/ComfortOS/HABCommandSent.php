<?php

namespace SmartRooms\Events\ComfortOS;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

use SmartRooms\Models\ComfortOS\HABCommand;
use Illuminate\Support\Facades\Broadcast;

class HABCommandSent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $command;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(HABCommand $command)
    {
        $this->command = $command;

        $this->broker = $this->command->room->building->property->ws_brokers->first();

        Broadcast::setDefaultDriver ('dynamic-centrifuge');

        config ([
          'broadcasting.connections.dynamic-centrifuge.url'       => $this->broker->http_url,
          'broadcasting.connections.dynamic-centrifuge.secret'    => $this->broker->hmac_secret,
          'broadcasting.connections.dynamic-centrifuge.api_key'   => $this->broker->api_key,
        ]);

        Broadcast::server(config ('broadcasting.connections.dynamic-centrifuge'));
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        if (! $this->command || ! is_object($this->command)) {
            return [];
        }

        return new Channel('rooms.'.$this->command->room_id);
    }

    public function broadcastAs()
    {
        return 'command';
    }

    public function broadcastWith()
    {
        if (! $this->command || ! is_object($this->command)) {
            return [];
        }

        return [
            'domain'     => 'hab',
            'component'  => $this->command->component_id ?? null,
            'attribute'  => $this->command->attribute ?? null,
            'value'      => $this->command->value ?? null,
            'sent_at'    => $this->command->sent_at->format('c'),
            'response'   => $this->command->response ?? 0,
            'created_at' => $this->command->created_at->format('c'),
            'updated_at' => $this->command->updated_at->format('c'),
        ];
    }
}
