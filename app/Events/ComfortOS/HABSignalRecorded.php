<?php

namespace SmartRooms\Events\ComfortOS;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

use SmartRooms\Models\ComfortOS\HABSignal;

class HABSignalRecorded implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $signal;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(HABSignal $signal)
    {
        $this->signal = $signal;

        $this->broker = $this->signal->room->building->property->ws_brokers->first();

        Broadcast::setDefaultDriver ('dynamic-centrifuge');

        config ([
          'broadcasting.connections.dynamic-centrifuge.url'       => $this->broker->http_url,
          'broadcasting.connections.dynamic-centrifuge.secret'    => $this->broker->hmac_secret,
          'broadcasting.connections.dynamic-centrifuge.api_key'   => $this->broker->api_key,
        ]);

        Broadcast::server(config ('broadcasting.connections.dynamic-centrifuge'));

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        if (! $this->signal || ! is_object($this->signal)) {
            return [];
        }

        return new Channel('rooms.'.$this->signal->room_id);
    }

    public function broadcastAs()
    {
        return 'signal';
    }

    public function broadcastWith()
    {
        if (! $this->signal || ! is_object($this->signal)) {
            return [];
        }

        return [
            'domain'     => 'hab',
            'component'  => $this->signal->component_id ?? null,
            'attribute'  => $this->signal->attribute ?? null,
            'value'      => $this->signal->value ?? null,
            'received_at' => $this->signal->created_at->format('c'),
            'created_at' => $this->signal->created_at->format('c'),
            'updated_at' => $this->signal->updated_at->format('c'),
        ];
    }
}
