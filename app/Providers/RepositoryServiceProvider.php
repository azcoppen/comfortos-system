<?php

namespace SmartRooms\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

// COMFORT OS

use SmartRooms\Contracts\Repositories\ComfortOS\ChecklistRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABCommandThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABCommandThroughputRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABCommandThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABCommandThroughputHourlyRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABSignalThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABSignalThroughputRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABSignalThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABSignalThroughputHourlyRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentMIAAlertThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentMIAAlertThroughputHourlyRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomMIAAlertThroughputRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomMIAAlertThroughputHourlyRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\OpQueueRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\OpQueueThroughputRepository;

use SmartRooms\Repositories\ComfortOS\ChecklistRepositoryEloquent;

use SmartRooms\Repositories\ComfortOS\HABCommandRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\HABCommandThroughputRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\ComponentHABCommandThroughputRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\RoomHABCommandThroughputRepositoryEloquent;

use SmartRooms\Repositories\ComfortOS\HABCommandThroughputHourlyRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\ComponentHABCommandThroughputHourlyRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\RoomHABCommandThroughputHourlyRepositoryEloquent;

use SmartRooms\Repositories\ComfortOS\HABSignalRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\HABSignalThroughputRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\ComponentHABSignalThroughputRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\RoomHABSignalThroughputRepositoryEloquent;

use SmartRooms\Repositories\ComfortOS\HABSignalThroughputHourlyRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\ComponentHABSignalThroughputHourlyRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\RoomHABSignalThroughputHourlyRepositoryEloquent;

use SmartRooms\Repositories\ComfortOS\MIAAlertRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\MIAAlertThroughputRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\MIAAlertThroughputHourlyRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\ComponentMIAAlertThroughputRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\ComponentMIAAlertThroughputHourlyRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\RoomMIAAlertThroughputRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\RoomMIAAlertThroughputHourlyRepositoryEloquent;

use SmartRooms\Repositories\ComfortOS\OpQueueRepositoryEloquent;
use SmartRooms\Repositories\ComfortOS\OpQueueThroughputRepositoryEloquent;

// COMPONENTS

use SmartRooms\Contracts\Repositories\Components\Devices\BulbRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\CameraRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\ChromecastRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\CodeRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\DimmerRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\LEDRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\LightRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\LockRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\MotorRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\PhysicalSwitchRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\PlugRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\SensorRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\SpeakerRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\STBRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\ThermostatRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\TVRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\TVAppRepository;


use SmartRooms\Repositories\Components\Devices\BulbRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\CameraRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\ChromecastRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\CodeRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\DimmerRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\LEDRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\LightRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\LockRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\MotorRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\PhysicalSwitchRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\PlugRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\SensorRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\SpeakerRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\STBRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\ThermostatRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\TVAppRepositoryEloquent;
use SmartRooms\Repositories\Components\Devices\TVRepositoryEloquent;


// MIRRORS

use SmartRooms\Contracts\Repositories\Components\Mirror\DisplayRepository;
use SmartRooms\Contracts\Repositories\Components\Mirror\ModuleRepository;

use SmartRooms\Repositories\Components\Mirror\DisplayRepositoryEloquent;
use SmartRooms\Repositories\Components\Mirror\ModuleRepositoryEloquent;

// OS

use SmartRooms\Contracts\Repositories\Components\OS\HABRepository;
use SmartRooms\Contracts\Repositories\Components\OS\MQTTBrokerRepository;
use SmartRooms\Contracts\Repositories\Components\OS\HubRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\Components\OS\WSBrokerRepository;

use SmartRooms\Repositories\Components\OS\HABRepositoryEloquent;
use SmartRooms\Repositories\Components\OS\MQTTBrokerRepositoryEloquent;
use SmartRooms\Repositories\Components\OS\VPNClientRepositoryEloquent;
use SmartRooms\Repositories\Components\OS\VPNServerRepositoryEloquent;
use SmartRooms\Repositories\Components\OS\WSBrokerRepositoryEloquent;

// TELECOMS

use SmartRooms\Contracts\Repositories\Components\Telecoms\DECTRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\ExtensionRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\HandsetRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\PBXRepository;

use SmartRooms\Repositories\Components\Telecoms\DECTRepositoryEloquent;
use SmartRooms\Repositories\Components\Telecoms\ExtensionRepositoryEloquent;
use SmartRooms\Repositories\Components\Telecoms\HandsetRepositoryEloquent;
use SmartRooms\Repositories\Components\Telecoms\PBXRepositoryEloquent;

// WIFI

use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;

use SmartRooms\Repositories\Components\Wifi\NetworkRepositoryEloquent;
use SmartRooms\Repositories\Components\Wifi\RouterRepositoryEloquent;

// IDAM
use SmartRooms\Contracts\Repositories\IDAM\AccessRepository;
use SmartRooms\Contracts\Repositories\IDAM\PermissionRepository;
use SmartRooms\Contracts\Repositories\IDAM\RoleRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Repositories\IDAM\AccessRepositoryEloquent;
use SmartRooms\Repositories\IDAM\PermissionRepositoryEloquent;
use SmartRooms\Repositories\IDAM\RoleRepositoryEloquent;
use SmartRooms\Repositories\IDAM\UserRepositoryEloquent;

// OPERATORS/PROPERTIES
use SmartRooms\Contracts\Repositories\Operator\BrandRepository;
use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;
use SmartRooms\Contracts\Repositories\Property\BuildingRepository;
use SmartRooms\Contracts\Repositories\Property\PropertyRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

use SmartRooms\Repositories\Operator\BrandRepositoryEloquent;
use SmartRooms\Repositories\Operator\OperatorRepositoryEloquent;
use SmartRooms\Repositories\Property\BuildingRepositoryEloquent;
use SmartRooms\Repositories\Property\PropertyRepositoryEloquent;
use SmartRooms\Repositories\Property\RoomRepositoryEloquent;

// MISC
use SmartRooms\Contracts\Repositories\Shared\AuditRepository;
use SmartRooms\Contracts\Repositories\Shared\NoteRepository;

use SmartRooms\Contracts\Repositories\Workflow\BookingRepository;
use SmartRooms\Contracts\Repositories\Workflow\CheckinRepository;
use SmartRooms\Contracts\Repositories\Workflow\CheckoutRepository;
use SmartRooms\Contracts\Repositories\Workflow\OccupancyRepository;
use SmartRooms\Contracts\Repositories\Workflow\RegistrationRepository;
use SmartRooms\Contracts\Repositories\Workflow\RentalRepository;


use SmartRooms\Repositories\Shared\AuditRepositoryEloquent;
use SmartRooms\Repositories\Shared\NoteRepositoryEloquent;

use SmartRooms\Repositories\Workflow\BookingRepositoryEloquent;
use SmartRooms\Repositories\Workflow\CheckinRepositoryEloquent;
use SmartRooms\Repositories\Workflow\CheckoutRepositoryEloquent;
use SmartRooms\Repositories\Workflow\OccupancyRepositoryEloquent;
use SmartRooms\Repositories\Workflow\RegistrationRepositoryEloquent;
use SmartRooms\Repositories\Workflow\RentalRepositoryEloquent;

class RepositoryServiceProvider extends ServiceProvider
{
    public $route_bindings = [
        'access'        => AccessRepository::class,
        //'audit'         => AuditRepository::class,
        'dect'          => DECTRepository::class,
        'building'      => BuildingRepository::class,
        'bulb'          => BulbRepository::class,
        'camera'        => CameraRepository::class,
        'checklist'     => ChecklistRepository::class,
        'chromecast'    => ChromecastRepository::class,
        'dimmer'        => DimmerRepository::class,
        'display'       => DisplayRepository::class,
        'extension'     => ExtensionRepository::class,
        'hab'           => HABRepository::class,
        'handset'       => HandsetRepository::class,
        'led'           => LEDRepository::class,
        'lock'          => LockRepository::class,
        'mia_alert'     => MIAAlertRepository::class,
        'mirror'        => DisplayRepository::class,
        'module'        => ModuleRepository::class,
        'motor'         => MotorRepository::class,
        'mqtt_broker'   => MQTTBrokerRepository::class,
        'network'       => VPNRepository::class,
        //'note'          => NoteRepository::class,
        'permission'    => PermissionRepository::class,
        'plug'          => PlugRepository::class,
        'property'      => PropertyRepository::class,
        'pbx'           => PBXRepository::class,
        'role'          => RoleRepository::class,
        'room'          => RoomRepository::class,
        'router'        => RouterRepository::class,
        'sensor'        => SensorRepository::class,
        'speaker'       => SpeakerRepository::class,
        'ssid'          => NetworkRepository::class,
        'stb'           => STBRepository::class,
        'switch'        => PhysicalSwitchRepository::class,
        'thermostat'    => ThermostatRepository::class,
        'tv'            => TVRepository::class,
        'user'          => UserRepository::class,
        'vpn_client'    => VPNClientRepository::class,
        'vpn_server'    => VPNServerRepository::class,
        'wifi'          => NetworkRepository::class,
        'wifi_network'  => NetworkRepository::class,
        'ws_broker'     => WSBrokerRepository::class,
    ];

    public $bindings = [

      /*************************************************************************
      CORE REPOSITORIES
      ************************************************************************/
      AccessRepository::class           => AccessRepositoryEloquent::class,
      //AuditRepository::class            => AuditRepositoryEloquent::class,
      //NoteRepository::class             => NoteRepositoryEloquent::class,
      PermissionRepository::class       => PermissionRepositoryEloquent::class,
      RoleRepository::class             => RoleRepositoryEloquent::class,
      UserRepository::class             => UserRepositoryEloquent::class,

      /*************************************************************************
      COMFORTOS REPOSITORIES
      ************************************************************************/
      ChecklistRepository::class                          => ChecklistRepositoryEloquent::class,
      OpQueueRepository::class                            => OpQueueRepositoryEloquent::class,
      OpQueueThroughputRepository::class                  => OpQueueThroughputRepositoryEloquent::class,

      HABCommandRepository::class                         => HABCommandRepositoryEloquent::class,
      HABCommandThroughputRepository::class               => HABCommandThroughputRepositoryEloquent::class,
      ComponentHABCommandThroughputRepository::class      => ComponentHABCommandThroughputRepositoryEloquent::class,
      RoomHABCommandThroughputRepository::class           => RoomHABCommandThroughputRepositoryEloquent::class,

      HABCommandThroughputHourlyRepository::class          => HABCommandThroughputHourlyRepositoryEloquent::class,
      ComponentHABCommandThroughputHourlyRepository::class => ComponentHABCommandThroughputHourlyRepositoryEloquent::class,
      RoomHABCommandThroughputHourlyRepository::class      => RoomHABCommandThroughputHourlyRepositoryEloquent::class,

      HABSignalRepository::class                          => HABSignalRepositoryEloquent::class,
      HABSignalThroughputRepository::class                => HABSignalThroughputRepositoryEloquent::class,
      ComponentHABSignalThroughputRepository::class       => ComponentHABSignalThroughputRepositoryEloquent::class,
      RoomHABSignalThroughputRepository::class            => RoomHABSignalThroughputRepositoryEloquent::class,

      HABSignalThroughputHourlyRepository::class           => HABSignalThroughputHourlyRepositoryEloquent::class,
      ComponentHABSignalThroughputHourlyRepository::class  => ComponentHABSignalThroughputHourlyRepositoryEloquent::class,
      RoomHABSignalThroughputHourlyRepository::class       => RoomHABSignalThroughputHourlyRepositoryEloquent::class,

      MIAAlertRepository::class                           => MIAAlertRepositoryEloquent::class,
      MIAAlertThroughputRepository::class                 => MIAAlertThroughputRepositoryEloquent::class,
      MIAAlertThroughputHourlyRepository::class           => MIAAlertThroughputHourlyRepositoryEloquent::class,
      ComponentMIAAlertThroughputRepository::class        => ComponentMIAAlertThroughputRepositoryEloquent::class,
      ComponentMIAAlertThroughputHourlyRepository::class  => ComponentMIAAlertThroughputHourlyRepositoryEloquent::class,
      RoomMIAAlertThroughputRepository::class             => RoomMIAAlertThroughputRepositoryEloquent::class,
      RoomMIAAlertThroughputHourlyRepository::class       => RoomMIAAlertThroughputHourlyRepositoryEloquent::class,


      /*************************************************************************
      COMPONENT REPOSITORIES
      ************************************************************************/

      // DEVICES

      BulbRepository::class       => BulbRepositoryEloquent::class,
      CameraRepository::class     => CameraRepositoryEloquent::class,
      ChromecastRepository::class => ChromecastRepositoryEloquent::class,
      CodeRepository::class       => CodeRepositoryEloquent::class,
      DimmerRepository::class     => DimmerRepositoryEloquent::class,
      LEDRepository::class        => LEDRepositoryEloquent::class,
      LockRepository::class       => LockRepositoryEloquent::class,
      MotorRepository::class      => MotorRepositoryEloquent::class,
      PlugRepository::class       => PlugRepositoryEloquent::class,
      SensorRepository::class     => SensorRepositoryEloquent::class,
      SpeakerRepository::class    => SpeakerRepositoryEloquent::class,
      PhysicalSwitchRepository::class => PhysicalSwitchRepositoryEloquent::class,
      STBRepository::class        => STBRepositoryEloquent::class,
      ThermostatRepository::class => ThermostatRepositoryEloquent::class,
      TVRepository::class         => TVRepositoryEloquent::class,
      TVAppRepository::class      => TVAppRepositoryEloquent::class,

      // MIRRORS
      DisplayRepository::class      => DisplayRepositoryEloquent::class,
      ModuleRepository::class       => ModuleRepositoryEloquent::class,

      // OS
      HABRepository::class          => HABRepositoryEloquent::class,
      HubRepository::class          => HubRepositoryEloquent::class,
      MQTTBrokerRepository::class   => MQTTBrokerRepositoryEloquent::class,
      VPNClientRepository::class    => VPNClientRepositoryEloquent::class,
      VPNServerRepository::class    => VPNServerRepositoryEloquent::class,
      WSBrokerRepository::class     => WSBrokerRepositoryEloquent::class,

      // TELECOMS
      DECTRepository::class         => DECTRepositoryEloquent::class,
      ExtensionRepository::class    => ExtensionRepositoryEloquent::class,
      HandsetRepository::class      => HandsetRepositoryEloquent::class,
      PBXRepository::class          => PBXRepositoryEloquent::class,

      // WIFI

      NetworkRepository::class    => NetworkRepositoryEloquent::class,
      RouterRepository::class     => RouterRepositoryEloquent::class,


      /*************************************************************************
      RESERVED/USE LATER
      ************************************************************************/

      OperatorRepository::class => OperatorRepositoryEloquent::class,
      BrandRepository::class    => BrandRepositoryEloquent::class,

      PropertyRepository::class => PropertyRepositoryEloquent::class,
      BuildingRepository::class => BuildingRepositoryEloquent::class,
      RoomRepository::class     => RoomRepositoryEloquent::class,

    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->bindings as $contract => $repo) {
            $this->app->bind($contract, $repo);
        }

        foreach ($this->route_bindings as $param => $repo) {
            //Route::model ($param, app ($repo)->model());
            Route::bind($param, function ($value) use ($param, $repo) {
                switch ($param) {
                  case 'operator':
                  case 'brand':
                  case 'property':
                  case 'building':

                      $slug_model = app($repo)->scopeQuery(function ($query) use ($value) {
                          return $query->where('slug', $value);
                      })->first();

                      if ($slug_model) {
                          return $slug_model;
                      }

                      $model = app($repo)->find($value);

                      if ($model) {
                          return $model;
                      }

                      abort(404);

                  break;

                  default:
                    return app($repo)->find($value) ?? abort(404);
                  break;
              }
            });
        }
    }
}
