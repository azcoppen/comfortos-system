<?php

namespace SmartRooms\Providers;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

use SmartRooms\Contracts\System;

use SmartRooms\Services\System\Entities;
use SmartRooms\Services\System\IDAM;

class ManagementServicesServiceProvider extends ServiceProvider implements DeferrableProvider
{
    private $manager_bindings = [
      System\BrandManagementContract::class                  => Entities\BrandManager::class,
      System\BuildingManagementContract::class               => Entities\BuildingManager::class,
      System\ComponentManagementContract::class              => Entities\ComponentManager::class,
      System\DECTManagementContract::class                   => Entities\DECTManager::class,
      System\ExtensionManagementContract::class              => Entities\ExtensionManager::class,
      System\HABManagementContract::class                    => Entities\HABManager::class,
      System\HandsetManagementContract::class                => Entities\HandsetManager::class,
      System\MirrorDisplayManagementContract::class          => Entities\MirrorManager::class,
      System\MirrorModuleManagementContract::class           => Entities\MirrorModuleManager::class,
      System\MQTTBrokerManagementContract::class             => Entities\MQTTBrokerManager::class,
      System\OperatorAssociationManagementContract::class    => IDAM\OperatorAssociationManager::class,
      System\OperatorManagementContract::class               => Entities\OperatorManager::class,
      System\PBXManagementContract::class                    => Entities\PBXManager::class,
      System\PermissionAssociationManagementContract::class  => IDAM\PermissionAssociationManager::class,
      System\PropertyManagementContract::class               => Entities\PropertyManager::class,
      System\RoleAssociationManagementContract::class        => IDAM\RoleAssociationManager::class,
      System\RoomManagementContract::class                   => Entities\RoomManager::class,
      System\RoomUserManagementContract::class               => IDAM\RoomUserManager::class,
      System\RouterManagementContract::class                 => Entities\RouterManager::class,
      System\STBAppManagementContract::class                 => Entities\STBAppManager::class,
      System\STBManagementContract::class                    => Entities\STBManager::class,
      System\UserManagementContract::class                   => IDAM\UserManager::class,
      System\VPNClientManagementContract::class              => Entities\VPNClientManager::class,
      System\VPNServerManagementContract::class              => Entities\VPNServerManager::class,
      System\VirtualWifiNetworkManagementContract::class     => Entities\VirtualWifiNetworkManager::class,
      System\WifiNetworkManagementContract::class            => Entities\WifiNetworkManager::class,
      System\WSBrokerManagementContract::class               => Entities\WSBrokerManager::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      foreach ( $this->manager_bindings AS $contract => $manager )
      {
        $this->app->bind ($contract, $manager);
      }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides() : array
    {
        return array_keys ($this->manager_bindings);
    }
}
