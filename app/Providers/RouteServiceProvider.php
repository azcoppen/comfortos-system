<?php

namespace SmartRooms\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

use SmartRooms\Contracts\Operator\BrandRepository;
use SmartRooms\Contracts\Operator\OperatorRepository;
use SmartRooms\Contracts\Property\BuildingRepository;
use SmartRooms\Contracts\Property\PropertyRepository;
use SmartRooms\Contracts\Property\RoomRepository;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/dashboards';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();

        Route::pattern('domain', '[a-z0-9.\-]+');
        Route::pattern('role_tag', '[a-z0-9.\-]+');

        $this->configureRateLimiting();

        $this->routes(function () {
            Route::domain(env('DASH_DOMAIN', 'dash').'.'.env('APP_DOMAIN', 'smartrooms.dev'))
                ->middleware('web')
                ->group(base_path('routes/web.php'));

            Route::domain(env('DESK_DOMAIN', 'desk').'.'.env('APP_DOMAIN', 'smartrooms.dev'))
              ->middleware('web')
              ->name('desk.')
              ->group(base_path('routes/desk.php'));

            Route::domain(env('STORE_DOMAIN', 'store').'.'.env('APP_DOMAIN', 'smartrooms.dev'))
              ->middleware('web')
              ->name('store.')
              ->group(base_path('routes/store.php'));

            Route::domain(env('GUEST_DOMAIN', 'guest').'.'.env('APP_DOMAIN', 'smartrooms.dev'))
              ->middleware('web')
              ->name('guest.')
              ->group(base_path('routes/guest.php'));

            Route::domain(env('API_DOMAIN', 'api').'.'.env('APP_DOMAIN', 'smartrooms.dev'))
                ->name ('api.')
                ->prefix('v1')
                ->middleware('api')
                ->group(base_path('routes/api.php'));
        });

    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60);
        });
    }
}
