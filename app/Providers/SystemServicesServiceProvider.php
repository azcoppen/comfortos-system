<?php

namespace SmartRooms\Providers;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use SimpleSoftwareIO\QrCode\Generator as QRGenerator;

use SmartRooms\Contracts\System\FlowOutputContract;
use SmartRooms\Contracts\System\MikroTikCLIProcessContract;
use SmartRooms\Contracts\System\MQTTPublishingContract;
use SmartRooms\Contracts\System\OTPGenerationContract;
use SmartRooms\Contracts\System\OTPVerificationContract;
use SmartRooms\Contracts\System\QRCodeGenerationContract;
use SmartRooms\Contracts\System\WSBrokerAccessContract;

use SmartRooms\Services\System\Flow\Logger;
use SmartRooms\Services\MikroTik\RouterOS\CLIExecutor;
use SmartRooms\Services\System\Flow\MQTTPublisher;
use SmartRooms\Services\System\Flow\MultiCentrifugoAccess;
use SmartRooms\Services\System\OTP\Generator as OTPGenerator;
use SmartRooms\Services\System\OTP\Verifier;

class SystemServicesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FlowOutputContract::class, Logger::class);
        $this->app->bind(MikroTikCLIProcessContract::class, CLIExecutor::class);

        $this->app->bind(MQTTPublishingContract::class, MQTTPublisher::class);

        $this->app->bind(OTPGenerationContract::class, OTPGenerator::class);
        $this->app->bind(OTPVerificationContract::class, Verifier::class);
        $this->app->bind(QRCodeGenerationContract::class, QRGenerator::class);
        $this->app->bind(WSBrokerAccessContract::class, MultiCentrifugoAccess::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides() : array
    {
        return [
            FlowOutputContract::class,
            MQTTProcessingContract::class,
            OTPGenerationContract::class,
            OTPVerificationContract::class,
            QRCodeGenerationContract::class,
            WSBrokerAccessContract::class,
        ];
    }
}
