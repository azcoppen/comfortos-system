<?php

namespace SmartRooms\Providers\OpenHAB;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

use SmartRooms\Contracts\OpenHAB\API\CommandExecutionContract;
use SmartRooms\Services\OpenHAB\API\HTTPExecutor;
use SmartRooms\Services\OpenHAB\API\MQTTExecutor;

use \Exception;

class BridgeServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      if ( strtolower ( env ('COMMAND_VECTOR') ) == 'mqtt' )
      {
        $this->app->bind(CommandExecutionContract::class, MQTTExecutor::class);
      }

      if ( strtolower ( env ('COMMAND_VECTOR') ) == 'http' )
      {
        $this->app->bind(CommandExecutionContract::class, HTTPExecutor::class);
      }

      if (! env ('COMMAND_VECTOR') )
      {
        throw new Exception ("Environmental var COMMAND_VECTOR [mqtt/http] needs to be set.");
      }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return [
          CommandExecutionContract::class,
        ];
    }
}
