<?php

namespace SmartRooms\Providers;

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\ServiceProvider;

use Illuminate\Broadcasting\BroadcastManager;

use SmartRooms\Services\System\Flow\DynamicCentrifugeBroadcaster;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(BroadcastManager $broadcastManager)
    {
        $broadcastManager->extend('dynamic-centrifuge', function ($app) {
    			return new DynamicCentrifugeBroadcaster;
    		});

        Broadcast::routes();

        require base_path('routes/channels.php');
    }
}
