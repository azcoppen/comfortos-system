<?php

namespace SmartRooms\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use SmartRooms\Models\Components\OS;
use SmartRooms\Models\Components\Devices;
use SmartRooms\Models\Components\Mirror;
use SmartRooms\Models\Components\Telecoms;
use SmartRooms\Models\Components\Wifi;
use SmartRooms\Models\IDAM\User;
use SmartRooms\Models\Operator;
use SmartRooms\Models\Property;
use SmartRooms\Policies\ComfortOS;
use SmartRooms\Policies\Operator AS OperatorPolicies;
use SmartRooms\Policies\Property AS PropertyPolicies;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Operator\Brand::class       => OperatorPolicies\BrandPolicy::class,
        Property\Building::class    => PropertyPolicies\BuildingPolicy::class,
        Devices\Bulb::class         => ComfortOS\BulbPolicy::class,
        Devices\Camera::class       => ComfortOS\CameraPolicy::class,
        Devices\Chromecast::class   => ComfortOS\ChromecastPolicy::class,
        Telecoms\DECT::class        => ComfortOS\DECTPolicy::class,
        Devices\Dimmer::class       => ComfortOS\DimmerPolicy::class,
        Telecoms\Extension::class   => ComfortOS\ExtensionPolicy::class,
        OS\HAB::class               => ComfortOS\HABPolicy::class,
        Telecoms\Handset::class     => ComfortOS\HandsetPolicy::class,
        Devices\LED::class          => ComfortOS\LEDPolicy::class,
        Devices\Lock::class         => ComfortOS\LockPolicy::class,
        Devices\Motor::class        => ComfortOS\MotorPolicy::class,
        Mirror\Display::class       => ComfortOS\MirrorPolicy::class,
        Mirror\Module::class        => ComfortOS\MirrorModulePolicy::class,
        OS\MQTTBroker::class        => ComfortOS\MQTTBrokerPolicy::class,
        Telecoms\PBX::class         => ComfortOS\PBXPolicy::class,
        Devices\Plug::class         => ComfortOS\PlugPolicy::class,
        Operator\Operator::class    => OperatorPolicies\OperatorPolicy::class,
        Property\Property::class    => PropertyPolicies\PropertyPolicy::class,
        Property\Room::class        => PropertyPolicies\RoomPolicy::class,
        Wifi\Router::class          => ComfortOS\RouterPolicy::class,
        Devices\Sensor::class       => ComfortOS\SensorPolicy::class,
        Devices\Speaker::class      => ComfortOS\SpeakerPolicy::class,
        Devices\STB::class          => ComfortOS\STBPolicy::class,
        Devices\PhysicalSwitch::class => ComfortOS\PhysicalSwitchPolicy::class,
        Devices\Thermostat::class   => ComfortOS\ThermostatPolicy::class,
        Devices\TV::class           => ComfortOS\TVPolicy::class,
        User::class                 => ComfortOS\UserPolicy::class,
        OS\VPNClient::class         => ComfortOS\VPNClientPolicy::class,
        OS\VPNServer::class         => ComfortOS\VPNServerPolicy::class,
        Wifi\Network::class         => ComfortOS\WifiNetworkPolicy::class,
        OS\WSBroker::class          => ComfortOS\WSBrokerPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
