<?php

namespace SmartRooms\Providers\ComfortOS;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

use SmartRooms\Contracts\ComfortOS\RoomAccessManagementContract;
use SmartRooms\Services\ComfortOS\Flow\RoomAccessManager;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;
use SmartRooms\Services\System\IDAM\UserConstraintManager;

class FlowServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RoomAccessManagementContract::class, RoomAccessManager::class);
        $this->app->bind(UserConstraintManagementContract::class, UserConstraintManager::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides() : array
    {
        return [
            RoomAccessManagementContract::class,
            UserConstraintManagementContract::class,
        ];
    }
}
