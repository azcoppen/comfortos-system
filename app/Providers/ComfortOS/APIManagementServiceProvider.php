<?php

namespace SmartRooms\Providers\ComfortOS;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use SmartRooms\Contracts\ComfortOS\API as APIContracts;
use SmartRooms\Services\ComfortOS\API as APIServices;

class APIManagementServiceProvider extends ServiceProvider implements DeferrableProvider
{
    private $controller_service_bindings = [
        APIContracts\BulbManagementContract::class          => APIServices\BulbManager::class,
        APIContracts\CameraManagementContract::class        => APIServices\CameraManager::class,
        APIContracts\ChromecastManagementContract::class    => APIServices\ChromecastManager::class,
        APIContracts\DimmerManagementContract::class        => APIServices\DimmerManager::class,
        APIContracts\LEDManagementContract::class           => APIServices\LEDManager::class,
        APIContracts\LockManagementContract::class          => APIServices\LockManager::class,
        APIContracts\MirrorManagementContract::class        => APIServices\MirrorManager::class,
        APIContracts\MotorManagementContract::class         => APIServices\MotorManager::class,
        APIContracts\PlugManagementContract::class          => APIServices\PlugManager::class,
        APIContracts\SpeakerManagementContract::class       => APIServices\SpeakerManager::class,
        APIContracts\STBManagementContract::class           => APIServices\STBManager::class,
        APIContracts\PhysicalSwitchManagementContract::class => APIServices\PhysicalSwitchManager::class,
        APIContracts\ThermostatManagementContract::class    => APIServices\ThermostatManager::class,
        APIContracts\TVManagementContract::class            => APIServices\TVManager::class,
        APIContracts\WifiManagementContract::class          => APIServices\WifiManager::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->controller_service_bindings as $contract => $service) {
            $this->app->bind($contract, $service);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides() : array
    {
        return array_keys($this->controller_service_bindings);
    }
}
