<?php

namespace SmartRooms\Providers\ComfortOS;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use SmartRooms\Contracts\Metrics\AggregationContract;
use SmartRooms\Services\Metrics\Aggregator;

class MetricsServiceProvider extends ServiceProvider
{
    private $service_bindings = [
        AggregationContract::class => Aggregator::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->service_bindings as $contract => $service) {
            $this->app->bind($contract, $service);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides() : array
    {
        return array_keys($this->service_bindings);
    }
}
