<?php

namespace SmartRooms\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

use ConsoleTVs\Charts\Registrar as ChartRegistrar;
use \Illuminate\Pagination\Paginator;
use SmartRooms\Charts;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot (ChartRegistrar $charts)
    {
        Blade::directive('cdn', function ($expression) {
            return "<?php echo config ('app.cdn_url') . $expression; ?>";
        });

        Paginator::useBootstrap();

        $charts->register ([
            Charts\CommandsChart::class,
            Charts\MIAChart::class,
            Charts\OperationsChart::class,
            Charts\SignalsChart::class,
            Charts\DailyCommandsChart::class,
            Charts\DailyMIAChart::class,
            Charts\DailySignalsChart::class,
        ]);
    }
}
