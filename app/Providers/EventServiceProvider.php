<?php

namespace SmartRooms\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use SmartRooms\Events\ComfortOS\WifiSignalReceived;
use SmartRooms\Listeners\ComfortOS\ProcessWifiSignal;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

      WifiSignalReceived::class =>
      [
        ProcessWifiSignal::class,
      ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {

        //
    }
}
