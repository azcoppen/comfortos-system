<?php

namespace SmartRooms\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use SmartRooms\Contracts\OpenHAB\API\CommandExecutionContract;

use SmartRooms\Events\ComfortOS\OperationExecuted;
use SmartRooms\Events\ComfortOS\OperationFailed;

use SmartRooms\Contracts\System\FlowOutputContract;

abstract class OpenHABCommand implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  protected $payload;

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
      app (FlowOutputContract::class)->signal (get_class($this), $this->payload);

      if (! $this->payload->has('op_queue')) {
          event ( new OperationFailed ($this->payload->get('op_queue'), $this) );
          app (FlowOutputContract::class)->signal ('No op_queue. Aborting.');
          $this->delete();
      }

      if (! $this->payload->has('entity')) {
          $this->payload->get('op_queue')->update([
              'processed_at'  => now(),
              'result'        => false,
              'reason'        => 'No entity',
          ]);

          app (FlowOutputContract::class)->signal ('No entity. Aborting.');

          event ( new OperationFailed ($this->payload->get('op_queue'), $this) );

          $this->delete();
      }

      if (! $this->payload->has('commands') || ! count($this->payload->get('commands'))) {
          $this->payload->get('op_queue')->update([
              'processed_at'  => now(),
              'result'        => false,
              'reason'        => 'No commands',
          ]);

          app (FlowOutputContract::class)->signal ('No commands. Aborting.');

          event ( new OperationFailed ($this->payload->get('op_queue'), $this) );

          $this->delete();
      }

      foreach ($this->payload->get('commands') as $cmd)
      {
        app (FlowOutputContract::class)->signal ('Firing command: ', $cmd);

        $result = app (CommandExecutionContract::class) // HTTP or MQTT Executor
          ->queue ($this->payload->get('op_queue'))
          ->entity ($this->payload->get('entity'))
          ->fire ($cmd);
      }

      $this->payload->get('op_queue')->update([
          'processed_at'  => now(),
          'result'        => $result,
      ]);

      app (FlowOutputContract::class)->signal ('Command completed.');

      event ( new OperationExecuted ($this->payload->get('op_queue'), $this) );

  }


}
