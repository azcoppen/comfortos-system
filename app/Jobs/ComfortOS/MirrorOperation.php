<?php

namespace SmartRooms\Jobs\ComfortOS;

use SmartRooms\Jobs\MirrorCommand;

class MirrorOperation extends MirrorCommand
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $payload)
    {
        $this->payload = collect($payload);
    }
}
