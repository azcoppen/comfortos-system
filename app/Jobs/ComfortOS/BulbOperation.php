<?php

namespace SmartRooms\Jobs\ComfortOS;

use SmartRooms\Jobs\OpenHABCommand;

class BulbOperation extends OpenHABCommand
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $payload)
    {
        $this->payload = collect($payload);
    }
}
