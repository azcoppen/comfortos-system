<?php

namespace SmartRooms\Jobs\MQTT;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use \Exception;
use \Carbon\Carbon;
use \Illuminate\Support\Str;
use \Mosquitto\Message;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;

use SmartRooms\Events\ComfortOS\HABSignalRecorded;
use SmartRooms\Events\ComfortOS\WifiSignalReceived;

use SmartRooms\Contracts\System\FlowOutputContract;

class RecordIncomingSignal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $hierarchy;
    protected $payload;
    protected $received;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct (array $hierarchy, $payload, Carbon $received)
    {
        $this->hierarchy = $hierarchy;
        $this->payload = $payload;
        $this->received = $received;
    }

    public function format_color_values ()
    {
      $parts = explode (',', $this->payload);

      if ( is_array ($parts) && count ($parts) == 3 )
      {
        foreach ($parts AS $index => $part)
        {
          $parts[$index] = floatval ($part);
        }

        $this->payload = $parts;
      }
    }

    public function format_router_mongo_insert ()
    {
      /****************************************
      Mongo won't store values whith .id in them, which RouterOS uses.
      *****************************************/

      if ( is_object ($this->payload) )
      {
        if ( isset ($this->payload->{'.id'}) )
        {
          $this->payload->id = $val;
          unset ($this->payload->{'.id'});
        }
      }

      if ( is_array ($this->payload) )
      {
        foreach ($this->payload AS $key => $val)
        {

          if ( is_object ($val) )
          {
            if ( isset ($val->{'.id'}) )
            {
              $this->payload[$key]->id = $val->{'.id'};
              unset ($this->payload[$key]->{'.id'});
            }
          }

          if ( is_array ($val) )
          {
            if ( isset ($val['.id']) )
            {
              $this->payload[$key]['id'] = $val['.id'];
              unset ($this->payload[$key]['.id']);
            }
          }

        }
      } // end is_array
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      try
      {
        /*
          Topics look like:

          property_id [0]/building_id[1]/room_id[2]/group[3]/component_id[4]/attribute[5]/state[6]

          5fb225099ba3874e9767b64e/5fb2250a9ba3874e9767b652/5fb2250a9ba3874e9767b656/speakers/5fb230d808d7ee320c1a18d2/volume
          5fb225099ba3874e9767b64e/+/+/+/+/+/state
        */

        if ( !is_array ($this->hierarchy) || count ($this->hierarchy) != 7 )
        {
          return;
        }

        if ( $this->hierarchy[6] != 'state' )
        {
          return;
        }

        if ( in_array($this->hierarchy[3], ['routers', 'wifi', 'wlans', 'wifi-launches', 'wifi-renames', 'wifi-deletes', 'wifi-pwds']) )
        {
          // could be an ON/OFF
          if ( Str::startsWith ($this->payload, '{') || Str::startsWith ($this->payload, '[') )
          {
            if ( json_decode ($this->payload) && json_last_error() == JSON_ERROR_NONE )
            {
              $this->payload = json_decode ($this->payload);
              $this->format_router_mongo_insert();
            }
          }

        }

        if ( in_array ($this->hierarchy[5], ['rgb', 'hsb']) && Str::contains ($this->payload, ',') )
        {
          $this->format_color_values ();
        }

        if ( is_numeric ($this->payload) )
        {
          $this->payload = floatval ($this->payload);
        }

        $signal = app (HABSignalRepository::class)->create ([
          'room_id'       => $this->hierarchy[2] ?? null,
          'group'         => $this->hierarchy[3] ?? 'unknown',
          'component_id'  => $this->hierarchy[4] ?? null,
          'attribute'     => $this->hierarchy[5] ?? null,
          'value'         => $this->payload ?? NULL,
          'received_at'   => $this->received,
        ]);

        if ( in_array ($this->hierarchy[5], ['wifi-launches', 'wifi-renames', 'wifi-pwds', 'wifi-deletes']) )
        {
          event ( new WifiSignalReceived ($signal) );
        }

        event ( new HABSignalRecorded ($signal) );

      }
      catch (Exception $e)
      {
        app (FlowOutputContract::class)->error ($e->getMessage(), $e);
        return false;
      }


    }
}
