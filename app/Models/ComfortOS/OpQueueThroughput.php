<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class OpQueueThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class OpQueueThroughput extends Base implements Transformable
{
    use TransformableTrait;

    public $collection = 'op_queue_global_tput';

    protected $dates = ['dt'];
}
