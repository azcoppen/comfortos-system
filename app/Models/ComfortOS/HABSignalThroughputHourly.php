<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class HABSignalThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class HABSignalThroughputHourly extends Base implements Transformable
{
  use TransformableTrait;

  public $collection = 'hab_signals_global_tput_hourly';

  protected $dates = ['dt'];

}
