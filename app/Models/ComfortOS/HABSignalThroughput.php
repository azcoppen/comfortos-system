<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class HABSignalThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class HABSignalThroughput extends Base implements Transformable
{
  use TransformableTrait;

  public $collection = 'hab_signals_global_tput';

  protected $dates = ['dt'];

}
