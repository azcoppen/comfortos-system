<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use SmartRooms\Traits\Models\BelongsToRoom;

/**
 * Class RoomHABSignalThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class RoomHABSignalThroughput extends Base implements Transformable
{
  use TransformableTrait, BelongsToRoom;

  public $collection = 'hab_signals_room_tput';

  protected $dates = ['dt'];

}
