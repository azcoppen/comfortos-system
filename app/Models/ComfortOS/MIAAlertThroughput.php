<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class MIAAlertThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class MIAAlertThroughput extends Base implements Transformable
{
    use TransformableTrait;

    public $collection = 'mia_alerts_global_tput';

    protected $dates = ['dt'];

}
