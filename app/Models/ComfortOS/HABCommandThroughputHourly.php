<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class HABCommandThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class HABCommandThroughputHourly extends Base implements Transformable
{
  use TransformableTrait;

  public $collection = 'hab_commands_global_tput_hourly';

  protected $dates = ['dt'];

}
