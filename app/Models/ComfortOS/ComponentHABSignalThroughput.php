<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ComponentHABSignalThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class ComponentHABSignalThroughput extends Base implements Transformable
{
  use TransformableTrait;

  public $collection = 'hab_signals_component_tput';

  protected $dates = ['dt'];

}
