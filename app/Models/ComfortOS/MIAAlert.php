<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use SmartRooms\Traits\Models\BelongsToRoom;

/**
 * Class MIAAlert.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class MIAAlert extends Base implements Transformable
{
    use TransformableTrait, BelongsToRoom;

    public $collection = 'mia_alerts';

    protected $dates = ['last_seen_at', 'ignored_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

}
