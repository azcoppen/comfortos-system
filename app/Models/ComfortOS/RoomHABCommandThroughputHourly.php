<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use SmartRooms\Traits\Models\BelongsToRoom;

/**
 * Class RoomHABCommandThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class RoomHABCommandThroughputHourly extends Base implements Transformable
{
  use TransformableTrait, BelongsToRoom;

  public $collection = 'hab_commands_room_tput_hourly';

  protected $dates = ['dt'];

}
