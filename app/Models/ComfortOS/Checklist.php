<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Checklist.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class Checklist extends Base implements Transformable
{
    use TransformableTrait;

    public $collection = 'checklists';

    protected $guarded = [];

}
