<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class MIAAlertThroughputHourly.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class MIAAlertThroughputHourly extends Base implements Transformable
{
    use TransformableTrait;

    public $collection = 'mia_alerts_global_tput_hourly';

    protected $dates = ['dt'];

}
