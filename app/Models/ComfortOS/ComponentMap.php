<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Traits\Models\BelongsToRoom;

/**
 * Class ComponentMap.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class ComponentMap extends Base implements Transformable
{
    use TransformableTrait, BelongsToRoom;

    public $collection = 'room_cpts_map';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
