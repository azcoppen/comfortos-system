<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ComponentMIAAlertThroughputHourly.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class ComponentMIAAlertThroughputHourly extends Base implements Transformable
{
    use TransformableTrait;

    public $collection = 'mia_alerts_component_tput_hourly';

    protected $dates = ['dt'];

}
