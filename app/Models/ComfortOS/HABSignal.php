<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Traits\Models\BelongsToRoom;

use SmartRooms\Search\Elasticsearch\Serializers\HABSignalSearch;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Models\Components\Telecoms;
use SmartRooms\Models\Components\Wifi;

/**
 * Class HABSignal.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class HABSignal extends Base implements Transformable
{
  use TransformableTrait, BelongsToRoom;

  public $collection = 'hab_signals';

  public $mappings = [
    'bulbs'       => Devices\Bulb::class,
    'cameras'     => Devices\Camera::class,
    'chromecasts' => Devices\Chromecast::class,
    'dects'       => Telecoms\DECT::class,
    'dimmers'     => Devices\Dimmer::class,
    'extensions'  => Telecoms\Extension::class,
    'handsets'    => Telecoms\Handset::class,
    'leds'        => Devices\LED::class,
    'mirrors'     => Mirror\Display::class,
    'motors'      => Devices\Motor::class,
    'plugs'       => Devices\Plug::class,
    'routers'     => Wifi\Router::class,
    'sensors'     => Devices\Sensor::class,
    'speakers'    => Devices\Speaker::class,
    'stbs'        => Devices\STB::class,
    'switches'    => Devices\PhysicalSwitch::class,
    'thermostats' => Devices\Thermostat::class,
    'tvs'         => Devices\TV::class,
    'wifi'        => Wifi\Network::class,
    'wlans'       => Wifi\Router::class,
  ];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = [];

  protected $dates = ['execute_at', 'processed_at', 'expire_at', 'received_at'];

  public function component ()
  {
    return $this->belongsTo ($this->mappings[$this->group] ?? Devices\Motor::class, 'component_id', '_id');
  }

  public function user()
  {
      return $this->belongsTo(User::class);
  }

}
