<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RoomMIAAlertThroughputHourly.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class RoomMIAAlertThroughputHourly extends Base implements Transformable
{
    use TransformableTrait;

    public $collection = 'mia_alerts_room_tput_hourly';

    protected $dates = ['dt'];

}
