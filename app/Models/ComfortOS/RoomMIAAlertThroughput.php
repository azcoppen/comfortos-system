<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class RoomMIAAlertThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class RoomMIAAlertThroughput extends Base implements Transformable
{
    use TransformableTrait;

    public $collection = 'mia_alerts_room_tput';

    protected $dates = ['dt'];

}
