<?php

namespace SmartRooms\Models\ComfortOS;

use SmartRooms\Models\Base;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ComponentHABCommandThroughput.
 *
 * @package namespace SmartRooms\Models\ComfortOS;
 */
class ComponentHABCommandThroughput extends Base implements Transformable
{
  use TransformableTrait;

  public $collection = 'hab_commands_component_tput';

  protected $dates = ['dt'];

}
