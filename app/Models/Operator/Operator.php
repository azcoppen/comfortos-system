<?php

namespace SmartRooms\Models\Operator;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Models\Base;
use SmartRooms\Models\IDAM\User;

use SmartRooms\Search\Elasticsearch\Serializers\OperatorSearch;

/**
 * Class Operator.
 */
class Operator extends Base implements Transformable
{
    use TransformableTrait, OperatorSearch;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function brands()
    {
        return $this->hasMany(Brand::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, null, 'operator_ids', 'user_ids');
    }
}
