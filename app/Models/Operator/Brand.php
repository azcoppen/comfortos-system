<?php

namespace SmartRooms\Models\Operator;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Models\Base;
use SmartRooms\Models\Property\Property;

use SmartRooms\Search\Elasticsearch\Serializers\BrandSearch;

/**
 * Class Brand.
 */
class Brand extends Base implements Transformable
{
    use TransformableTrait, BrandSearch;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
