<?php

namespace SmartRooms\Models\Components;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Models\Base;
use SmartRooms\Traits\Models\BelongsToRoom;
use SmartRooms\Traits\Models\HasChangesAndReadings;
use SmartRooms\Traits\Models\HasCreator;
use SmartRooms\Traits\Models\HasProvisioning;

use ScoutElastic\Searchable;

/**
 * Class Component.
 */
abstract class Component extends Base implements Transformable
{
    use TransformableTrait, BelongsToRoom, HasChangesAndReadings, HasCreator, HasProvisioning, Searchable;

    protected $guarded = [];

    protected $appends = ['active'];

    protected $dates = ['created_at', 'updated_at', 'ordered_at', 'installed_at', 'provisioned_at', 'last_signal_at', 'launch_at', 'expire_at', 'last_command_at', 'start_at', 'end_at', 'confirmed_at', 'active_at', 'revoked_at'];

    protected $hidden = [
      'driver', 'items',
      'ordered_at', 'installed_at', 'provisioned_at', 'order_id',
      'recovery', 'base_station_ids', 'extension_ids', 'vpn_ids', 'handset_ids', 'dect_ids', 'int_ip', 'vpn_ip',
      'created_by', 'installed_by', 'provisioned_by', 'ordered_by',
      'last_signal_at', 'last_command_at', 'options', 'image',
    ];

    public $mia_alert_mins = 90;

    public function getActiveAttribute ()
    {
      return !empty ($this->room_id) &&$this->installed_at != FALSE && $this->provisioned_at != FALSE ? 1 : 0;
    }

    public function getHabAttribute ()
    {
      return $this->room->habs->first();
    }
}
