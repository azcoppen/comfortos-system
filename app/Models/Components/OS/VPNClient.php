<?php

namespace SmartRooms\Models\Components\OS;

use SmartRooms\Models\Components\Component;

use SmartRooms\Models\Components\Telecoms\DECT;
use SmartRooms\Models\Components\OS\HAB;
use SmartRooms\Models\Components\OS\MQTTBroker;
use SmartRooms\Models\Components\OS\WSBroker;
use SmartRooms\Models\Components\Telecoms\PBX;
use SmartRooms\Models\Components\Wifi\Router;
use SmartRooms\Models\Components\Devices\STB;
use SmartRooms\Models\Components\Mirror\Display;

use SmartRooms\Search\Elasticsearch\Serializers\VPNClientSearch;

/**
 * Class VPNClient.
 *
 * @package namespace SmartRooms\Models\Components\OS;
 */
class VPNClient extends Component
{
  use VPNClientSearch;

  public $collection = 'vpn_clients';

  public function getCertificateAttribute () : string
  {
    if ( $this->attributes['certificate'] )
    {
      return \Crypt::decrypt ($this->attributes['certificate']);
    }
    return '';
  }

  public function dects ()
  {
    return $this->belongsToMany (DECT::class, null, 'vpn_client_ids', 'dect_ids');
  }

  public function habs ()
  {
    return $this->belongsToMany (HAB::class, null, 'vpn_client_ids', 'hab_ids');
  }

  public function mirrors ()
  {
    return $this->belongsToMany (Display::class, null, 'vpn_client_ids', 'mirror_ids');
  }

  public function mqtt_brokers ()
  {
    return $this->belongsToMany (MQTTBroker::class, null, 'vpn_client_ids', 'mqtt_broker_ids');
  }

  public function pbxs ()
  {
    return $this->belongsToMany (PBX::class, null, 'vpn_client_ids', 'pbx_ids');
  }

  public function routers ()
  {
    return $this->belongsToMany (Router::class, null, 'vpn_client_ids', 'router_ids');
  }

  public function stbs ()
  {
    return $this->belongsToMany (STB::class, null, 'vpn_client_ids', 'stb_ids');
  }

  public function vpn_server ()
  {
    return $this->belongsTo (VPNServer::class, 'vpn_server_id');
  }

  public function ws_brokers ()
  {
    return $this->belongsToMany (WSBroker::class, null, 'vpn_client_ids', 'ws_broker_ids');
  }

}
