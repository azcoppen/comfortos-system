<?php

namespace SmartRooms\Models\Components\OS;

use SmartRooms\Models\Components\Component;

use SmartRooms\Traits\Models\BelongsToRouter;
use SmartRooms\Traits\Models\BelongsToHAB;

/**
 * Class SmartHub.
 */
class Hub extends Component
{
    use BelongsToRouter, BelongsToHAB;

    public $collection = 'cpt_hubs';
}
