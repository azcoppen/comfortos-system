<?php

namespace SmartRooms\Models\Components\OS;

use SmartRooms\Models\Components\Component;
use SmartRooms\Traits\Models\BelongsToVPN;
use SmartRooms\Models\Property\Property;

use SmartRooms\Search\Elasticsearch\Serializers\MQTTBrokerSearch;

/**
 * Class MQTTBroker.
 *
 * @package namespace SmartRooms\Models\OS;
 */
class MQTTBroker extends Component
{
    use BelongsToVPN, MQTTBrokerSearch;

    public $collection = 'mqtt_brokers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function properties()
    {
        return $this->belongsToMany (Property::class, null, 'mqtt_broker_ids', 'property_ids');
    }

    public function getAdminPassAttribute () : string
    {
      if ( $this->attributes['admin_pass'] )
      {
        return \Crypt::decrypt ($this->attributes['admin_pass']);
      }
      return '';
    }

    public function getAppPassAttribute () : string
    {
      if ( $this->attributes['app_pass'] )
      {
        return \Crypt::decrypt ($this->attributes['app_pass']);
      }
      return '';
    }

}
