<?php

namespace SmartRooms\Models\Components\OS;

use SmartRooms\Models\Components\Component;

use SmartRooms\Traits\Models\BelongsToVPN;
use SmartRooms\Traits\Models\BelongsToRouter;

use SmartRooms\Models\ComfortOS\HABCommand;
use SmartRooms\Models\ComfortOS\HABSignal;

use SmartRooms\Search\Elasticsearch\Serializers\HABSearch;

/**
 * Class Server.
 */
class HAB extends Component
{
    use BelongsToRouter, BelongsToVPN, HABSearch;

    public $collection = 'cpt_habs';

    public function commands ()
    {
      return $this->hasMany (HABCommand::class)
        ->where ('room_id', $this->room_id)
        ->orderBy ('created_at', 'DESC')
        ->take (50);
    }

    public function signals ()
    {
      return $this->hasMany (HABSignal::class)
        ->where ('room_id', $this->room_id)
        ->orderBy ('created_at', 'DESC')
        ->take (50);
    }
}
