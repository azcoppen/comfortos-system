<?php

namespace SmartRooms\Models\Components\OS;

use SmartRooms\Models\Components\Component;
use SmartRooms\Traits\Models\BelongsToVPN;
use SmartRooms\Models\Property\Property;

use SmartRooms\Search\Elasticsearch\Serializers\WSBrokerSearch;

/**
 * Class WSBroker.
 *
 * @package namespace SmartRooms\Models\OS;
 */
class WSBroker extends Component
{
    use BelongsToVPN, WSBrokerSearch;

    public $collection = 'ws_brokers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function properties()
    {
        return $this->belongsToMany (Property::class, null, 'ws_broker_ids', 'property_ids');
    }

    public function getAdminPassAttribute () : string
    {
      if ( $this->attributes['admin_pass'] )
      {
        return \Crypt::decrypt ($this->attributes['admin_pass']);
      }
      return '';
    }

    public function getAdminSecretAttribute () : string
    {
      if ( $this->attributes['admin_secret'] )
      {
        return \Crypt::decrypt ($this->attributes['admin_secret']);
      }
      return '';
    }

    public function getHmacSecretAttribute () : string
    {
      if ( $this->attributes['hmac_secret'] )
      {
        return \Crypt::decrypt ($this->attributes['hmac_secret']);
      }
      return '';
    }

    public function getHttpUrlAttribute () : string
    {
      switch ( $this->attributes['ws_port'] )
      {
        case 80:
          return 'http://' . $this->attributes['host'];
        break;

        case 443:
          return 'https://' . $this->attributes['host'];
        break;

        default:
          return 'https://' . $this->attributes['host'].':'.$this->attributes['ws_port'];
        break;
      }

      return '';
    }

    public function getWssUrlAttribute () : string
    {
      switch ( $this->attributes['ws_port'] )
      {
        case 80:
          return 'ws://' . $this->attributes['host'];
        break;

        case 443:
          return 'wss://' . $this->attributes['host'];
        break;

        default:
          return 'wss://' . $this->attributes['host'].':'.$this->attributes['ws_port'];
        break;
      }

      return '';
    }

    public function getApiKeyAttribute () : string
    {
      if ( $this->attributes['api_key'] )
      {
        return \Crypt::decrypt ($this->attributes['api_key']);
      }
      return '';
    }
}
