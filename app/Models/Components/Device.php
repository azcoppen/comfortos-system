<?php

namespace SmartRooms\Models\Components;

use SmartRooms\Models\Components\Component;

use \ReflectionClass;
use Illuminate\Support\Str;
use \Crypt;
use \Exception;

use SmartRooms\Traits\Models\BelongsToHAB;

/**
 * Class Device.
 */
abstract class Device extends Component
{
    use BelongsToHAB;

    protected $guarded = [];

    public function getPluralAttribute () : string
    {
      $plural = Str::plural ( (new ReflectionClass ($this))->getShortName() );
      return strtolower ($plural);
    }

    public function getMqttTopicAttribute () : string
    {
      return
        $this->room->building->property->_id . '/' .
        $this->room->building->_id . '/' .
        $this->room->_id . '/' .
        $this->plural . '/' .
        $this->_id;
    }

    public function getHabHttpSecurityAttribute () : bool
    {
      if ( isset ($this->room->habs) && is_object ($this->room->habs) && count ($this->room->habs) )
      {
        $hab = $this->room->habs->first();
        return isset ($hab->http_user) && !empty ($hab->http_user) ? true : false;
      }

      return false;
    }

    public function getHabHttpUserAttribute () : string
    {
      if ( isset ($this->room->habs) && is_object ($this->room->habs) && count ($this->room->habs) )
      {
        $hab = $this->room->habs->first();
        return $hab->http_user;
      }

      return '';
    }

    public function getHabHttpPassAttribute () : string
    {
      if ( isset ($this->room->habs) && is_object ($this->room->habs) && count ($this->room->habs) )
      {
        $hab = $this->room->habs->first();

        try
        {
          return Crypt::decrypt($hab->http_pass);
        }
        catch (Exception $e)
        {

        }

      }

      return '';
    }

    public function getHABItemEndpointAttribute () : string
    {
      if ( isset ($this->room->habs) && is_object ($this->room->habs) && count ($this->room->habs) )
      {
        $hab = $this->room->habs->first();

        $endpoint = 'https://' . ($hab->vpn_ip ?? '127.0.0.1');

        if ( $hab->https_port && !empty ($hab->https_port) )
        {
          if ( intval($hab->https_port) != 443 )
          {
            $endpoint .= ':' . ($hab->https_port ?? 8443);
          }
        }

        return $endpoint . '/rest/items/';
      }

      return '';
    }
}
