<?php

namespace SmartRooms\Models\Components\Wifi;

use SmartRooms\Models\Components\Component;

use SmartRooms\Traits\Models\BelongsToVPN;

use SmartRooms\Search\Elasticsearch\Serializers\RouterSearch;

/**
 * Class Router.
 */
class Router extends Component
{
    use BelongsToVPN, RouterSearch;

    public $collection = 'cpt_routers';

    protected $hidden = ['router_id'];

    protected $dates = ['ordered_at', 'installed_at', 'provisioned_at', 'last_signal_at', 'last_command_at', 'launch_at', 'expire_at'];

    public function getMqttTopicAttribute () : string
    {
      return
        $this->room->building->property->_id . '/' .
        $this->room->building->_id . '/' .
        $this->room->_id . '/' .
        'routers/' .
        $this->_id;
    }

    public function getApiPwdAttribute () : string
    {
      if ( $this->attributes['api_pwd'] )
      {
        try
        {
          return \Crypt::decrypt ($this->attributes['api_pwd']);
        }
        catch ( \Exception $e )
        {

        }
      }
      return '';
    }

    public function getWebPwdAttribute () : string
    {
      if ( $this->attributes['web_pwd'] )
      {
        try
        {
          return \Crypt::decrypt ($this->attributes['web_pwd']);
        }
        catch ( \Exception $e )
        {

        }
      }
      return '';
    }

    public function networks()
    {
        return $this->hasMany(Network::class, 'router_id');
    }

}
