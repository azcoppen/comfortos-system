<?php

namespace SmartRooms\Models\Components\Wifi;

use SmartRooms\Models\Components\Component;

use SmartRooms\Search\Elasticsearch\Serializers\WifiSearch;

/**
 * Class Network.
 */
class Network extends Component
{
    use WifiSearch;

    public $collection = 'cpt_wifi';

    protected $hidden = ['profile_index', 'network_index', 'interface', 'security_profile', 'router_id', 'provisioned_at', 'provisioned_by'];

    public function getPasswordAttribute () : string
    {
      if ( $this->attributes['password'] )
      {
        try
        {
          return \Crypt::decrypt ($this->attributes['password']);
        }
        catch ( \Exception $e )
        {

        }
      }
      return '';
    }

    public function router()
    {
        return $this->belongsTo(Router::class);
    }
}
