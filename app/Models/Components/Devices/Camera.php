<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\CameraSearch;

/**
 * Class Camera.
 */
class Camera extends Device
{
    use CameraSearch;

    public $collection = 'cpt_cameras';
}
