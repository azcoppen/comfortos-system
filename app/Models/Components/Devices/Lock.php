<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\LockSearch;

/**
 * Class Lock.
 */
class Lock extends Device
{
    use LockSearch;

    public $collection = 'cpt_locks';

    public function codes()
    {
        return $this->hasMany(Code::class);
    }
}
