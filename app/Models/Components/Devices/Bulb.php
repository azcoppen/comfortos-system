<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\BulbSearch;

/**
 * Class Bulb.
 */
class Bulb extends Device
{
    use BulbSearch;

    public $collection = 'cpt_bulbs';

    public $mia_alert_mins = 600;
}
