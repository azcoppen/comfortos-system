<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\MotorSearch;

/**
 * Class Camera.
 */
class Motor extends Device
{
    use MotorSearch;

    public $collection = 'cpt_motors';
}
