<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\SpeakerSearch;

/**
 * Class Speaker.
 */
class Speaker extends Device
{
    use SpeakerSearch;

    public $collection = 'cpt_speakers';
}
