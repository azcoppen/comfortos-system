<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\ThermostatSearch;

/**
 * Class Thermostat.
 */
class Thermostat extends Device
{
    use ThermostatSearch;

    public $collection = 'cpt_thermostats';
}
