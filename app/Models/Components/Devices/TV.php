<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\TVSearch;

/**
 * Class TV.
 */
class TV extends Device
{
    use TVSearch;

    public $collection = 'cpt_tvs';
}
