<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\ChromecastSearch;

/**
 * Class Camera.
 */
class Chromecast extends Device
{
    use ChromecastSearch;

    public $collection = 'cpt_chromecasts';
}
