<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\SensorSearch;

/**
 * Class Sensor.
 */
class Sensor extends Device
{
    use SensorSearch;

    public $collection = 'cpt_sensors';
}
