<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\SwitchSearch;

/**
 * Class Switch.
 */
class PhysicalSwitch extends Device
{
    use SwitchSearch;

    public $collection = 'cpt_switches';
}
