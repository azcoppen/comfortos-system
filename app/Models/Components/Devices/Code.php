<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Component;
use SmartRooms\Traits\Models\BelongsToRoom;
use SmartRooms\Traits\Models\HasCreator;

use SmartRooms\Search\Elasticsearch\Serializers\LockCodeSearch;

/**
 * Class Code.
 */
class Code extends Component
{
    use BelongsToRoom, LockCodeSearch;

    public $collection = 'cpt_codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function lock()
    {
        return $this->belongsTo(Lock::class);
    }

    public function shouldBeSearchable()
    {
        return false;
    }
}
