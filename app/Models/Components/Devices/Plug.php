<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\PlugSearch;

/**
 * Class Plug.
 */
class Plug extends Device
{
    use PlugSearch;

    public $collection = 'cpt_plugs';
}
