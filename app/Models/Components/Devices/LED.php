<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\LEDSearch;

/**
 * Class Bulb.
 */
class LED extends Device
{
    use LEDSearch;

    public $collection = 'cpt_leds';
    
    public $mia_alert_mins = 600;
}
