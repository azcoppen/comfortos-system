<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\DimmerSearch;

/**
 * Class Dimmer.
 */
class Dimmer extends Device
{
    use DimmerSearch;

    public $collection = 'cpt_dimmers';
}
