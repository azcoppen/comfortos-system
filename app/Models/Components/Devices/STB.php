<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Traits\Models\BelongsToRouter;
use SmartRooms\Traits\Models\BelongsToVPN;

use SmartRooms\Search\Elasticsearch\Serializers\STBSearch;

/**
 * Class STB.
 */
class STB extends Device
{
    use BelongsToVPN, BelongsToRouter, STBSearch;

    public $collection = 'cpt_stbs';

    protected $hidden = [
      'tv_app_ids', 'router_id', 'host', 'serial', 'int_ip', 'items', 'apps',
      'created_by', 'installed_by', 'provisioned_by', 'ordered_by', 'order_id',
      'ordered_at', 'installed_at', 'provisioned_at', 'vpn_ids', 'vpn_ip', 'ip',
    ];

    public function tv_apps ()
    {
        return $this->belongsToMany (TVApp::class, null, 'stb_ids', 'tv_app_ids');
    }

    public function tv ()
    {
        return $this->belongsTo(TV::class);
    }
}
