<?php

namespace SmartRooms\Models\Components\Devices;

use SmartRooms\Models\Components\Device;

use SmartRooms\Search\Elasticsearch\Serializers\TVAppSearch;

/**
 * Class App.
 */
class TVApp extends Device
{
    use TVAppSearch;

    public $collection = 'cpt_tv_apps';

    protected $hidden = ['install', 'stb_ids', 'created_at', 'updated_at'];

    public function stbs ()
    {
        return $this->belongsToMany (TVApp::class, null, 'tv_app_ids', 'stb_ids');
    }
}
