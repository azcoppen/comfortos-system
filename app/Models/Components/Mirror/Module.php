<?php

namespace SmartRooms\Models\Components\Mirror;

use SmartRooms\Models\Components\Component;

use SmartRooms\Search\Elasticsearch\Serializers\MirrorModuleSearch;

/**
 * Class Server.
 */
class Module extends Component
{
    use MirrorModuleSearch;

    public $collection = 'cpt_mmms';

    protected $hidden = ['repository', 'config', 'display_ids', 'created_at', 'updated_at'];

    public function displays()
    {
        return $this->belongsToMany(Display::class, null, 'module_ids', 'display_ids');
    }
}
