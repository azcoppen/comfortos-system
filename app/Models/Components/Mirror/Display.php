<?php

namespace SmartRooms\Models\Components\Mirror;

use SmartRooms\Models\Components\Component;

use SmartRooms\Traits\Models\BelongsToRouter;
use SmartRooms\Traits\Models\BelongsToVPN;

use SmartRooms\Search\Elasticsearch\Serializers\MirrorDisplaySearch;

/**
 * Class Server.
 */
class Display extends Component
{
    use BelongsToVPN, BelongsToRouter, MirrorDisplaySearch;

    public $collection = 'cpt_mirrors';

    protected $hidden = [
      'host', 'hardware', 'mac', 'router_id', 'module_ids', 'vpn_ids', 'int_ip', 'vpn_ip',
      'created_by', 'installed_by', 'provisioned_by', 'ordered_by',
      'ordered_at', 'installed_at', 'provisioned_at', 'order_id',
      'created_by',
    ];

    public function modules()
    {
        return $this->belongsToMany(Module::class, null, 'display_ids', 'module_ids');
    }
}
