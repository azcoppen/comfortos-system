<?php

namespace SmartRooms\Models\Components\Telecoms;

use SmartRooms\Models\Components\Component;
use SmartRooms\Traits\Models\BelongsToProperty;
use SmartRooms\Traits\Models\BelongsToVPN;

use SmartRooms\Search\Elasticsearch\Serializers\PBXSearch;

/**
 * Class Handset.
 */
class PBX extends Component
{
    use BelongsToVPN, BelongsToProperty, PBXSearch;

    public $collection = 'pbxs';

    protected $hidden = [
      'license', 'trunk_vendor', 'trunk_number', 'admin_url', 'admin_port', 'admin_user', 'admin_pwd',
      'created_by', 'installed_by', 'provisioned_by', 'ordered_by', 'order_id',
      'ordered_at', 'installed_at', 'provisioned_at', 'vpn_ids', 'vpn_ip', 'ip',
    ];

    public function getAdminPwdAttribute()
    {
        if (! isset($this->attributes['admin_pwd'])) {
            return null;
        }

        try
        {
          return \Crypt::decrypt($this->attributes['admin_pwd']);
        }
        catch (\Exception $e)
        {

        }

        return null;
    }

    public function dects()
    {
        return $this->hasMany(DECT::class, 'pbx_id');
    }

    public function extensions()
    {
        return $this->hasMany(Extension::class, 'pbx_id');
    }

    public function handsets()
    {
        return $this->hasMany(Handset::class, 'pbx_id');
    }
}
