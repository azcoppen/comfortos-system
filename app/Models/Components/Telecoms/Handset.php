<?php

namespace SmartRooms\Models\Components\Telecoms;

use SmartRooms\Models\Components\Component;

use SmartRooms\Search\Elasticsearch\Serializers\HandsetSearch;

/**
 * Class Handset.
 */
class Handset extends Component
{
    use HandsetSearch;

    public $collection = 'cpt_handsets';

    protected $hidden = [
      'dect_ids', 'extension_ids', 'IPEI', 'serial', 'ip', 'mac'.
      'created_by', 'installed_by', 'provisioned_by', 'ordered_by',
      'ordered_at', 'installed_at', 'provisioned_at', 'order_id',
      'created_by',
    ];

    public function dects ()
    {
        return $this->belongsToMany(DECT::class, null, 'handset_ids', 'dect_ids');
    }

    public function extensions()
    {
        return $this->belongsToMany(Extension::class, null, 'handset_ids', 'extension_ids');
    }

    public function pbx()
    {
        return $this->belongsTo(PBX::class);
    }
}
