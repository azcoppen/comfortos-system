<?php

namespace SmartRooms\Models\Components\Telecoms;

use SmartRooms\Models\Components\Component;

use SmartRooms\Search\Elasticsearch\Serializers\ExtensionSearch;

/**
 * Class Extension.
 */
class Extension extends Component
{
    use ExtensionSearch;

    public $collection = 'cpt_extensions';

    protected $hidden = [
      'remote_id', 'pbx_id',
      'ordered_at', 'installed_at', 'provisioned_at', 'order_id',
      'recovery', 'base_station_ids', 'extension_ids', 'vpn_ids', 'handset_ids', 'dect_ids', 'int_ip', 'vpn_ip',
      'created_by', 'installed_by', 'provisioned_by', 'ordered_by',
      'last_signal_at', 'last_command_at',
    ];

    public function getAuthPwdAttribute()
    {
        if (! isset($this->attributes['auth_pwd'])) {
            return null;
        }

        try
        {
          return \Crypt::decrypt($this->attributes['auth_pwd']);
        }
        catch (\Exception $e)
        {

        }

        return '';
    }

    public function dects()
    {
        return $this->belongsToMany(DECT::class, null, 'extension_ids', 'dect_ids');
    }

    public function handsets()
    {
        return $this->belongsToMany(Handset::class, null, 'extension_ids', 'handset_ids');
    }

    public function pbx()
    {
        return $this->belongsTo(PBX::class);
    }
}
