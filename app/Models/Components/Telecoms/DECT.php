<?php

namespace SmartRooms\Models\Components\Telecoms;

use SmartRooms\Models\Components\Component;

use SmartRooms\Traits\Models\BelongsToVPN;
use SmartRooms\Traits\Models\BelongsToRouter;

use SmartRooms\Search\Elasticsearch\Serializers\DECTSearch;

/**
 * Class Handset.
 */
class DECT extends Component
{
    use BelongsToRouter, BelongsToVPN, DECTSearch;

    public $collection = 'cpt_dect';

    public function getAuthPwdAttribute () : string
    {
      if ( $this->attributes['auth_pwd'] )
      {
        return \Crypt::decrypt ($this->attributes['auth_pwd']);
      }
      return '';
    }

    public function extensions()
    {
        return $this->belongsToMany(Extension::class, null, 'dect_ids', 'extension_ids');
    }

    public function handsets()
    {
        return $this->belongsToMany(Handset::class, null, 'dect_ids', 'handset_ids');
    }

    public function pbx()
    {
        return $this->belongsTo(PBX::class);
    }
}
