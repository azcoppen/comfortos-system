<?php

namespace SmartRooms\Models\IDAM;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Models\Base;

class Access extends Base implements Transformable
{
    use TransformableTrait;

    public $collection = 'accesses';

    protected $guarded = [];

    protected $dates = ['start_at', 'end_at'];

    public function grantee()
    {
        return $this->morphTo();
    }

    public function accessible()
    {
        return $this->morphTo();
    }
}
