<?php

namespace SmartRooms\Models\IDAM;

use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Maklad\Permission\Traits\HasRoles;
use OwenIt\Auditing\Auditable as AuditTrait;
use OwenIt\Auditing\Contracts\Auditable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Models\Operator\Operator;
use SmartRooms\Models\Property\Room;
use SmartRooms\Traits\Models\HasAccessGrants;
use SmartRooms\Traits\Models\HasCreator;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Support\Str;

use SmartRooms\Search\Elasticsearch\Serializers\UserSearch;

/**
 * Class User.
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract, Auditable, Transformable, JWTSubject
{
    use Authenticatable, Authorizable, AuditTrait, Notifiable, SoftDeletes, TransformableTrait,
    HasRoles, HasManyNotes, HasCreator, HasAccessGrants, UserSearch;

    protected $connection = 'mongodb';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $appends = ['full_name', 'images'];

    protected $hidden = ['password', 'otp_secret', 'deleted_at', 'room_ids', 'operator_ids', 'role_ids', 'permission_ids', 'image'];

    protected $dates = [
        'dob',
        'last_login_at',
        'last_activity_at',
        'email_verified_at',
        'pwd_changed_at',
        'pwd_expires_at',
        'created_at',
        'updated_at',
    ];

    protected $auditInclude = [

    ];

    public function getFullNameAttribute ()
    {
      return Str::title ($this->first . ' ' . $this->last);
    }

    public function getImagesAttribute () : array
    {
      if ( $this->image )
      {
        return [
          'lg' => 'https://res.cloudinary.com/smartrooms/users/' . $this->image .'.png',
          'md' => 'https://res.cloudinary.com/smartrooms/c_scale,w_200/users/' . $this->image .'.png',
          'sm' => 'https://res.cloudinary.com/smartrooms/c_scale,w_25/users/' . $this->image .'.png',
        ];
      }

      return [
        'lg' => null,
        'md' => null,
        'sm' => null,
      ];
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return env('SLACK_WEBHOOK', 'https://hooks.slack.com/services/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');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function operators()
    {
        return $this->belongsToMany(Operator::class, null, 'user_ids', 'operator_ids');
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class, null, 'user_ids', 'room_ids');
    }
}
