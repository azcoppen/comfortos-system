<?php

namespace SmartRooms\Models;

use Arcanedev\LaravelNotes\Traits\HasManyNotes;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as AuditTrait;
use OwenIt\Auditing\Contracts\Auditable;

class Base extends Model implements Auditable
{
    use SoftDeletes, AuditTrait, HasManyNotes;

    protected $connection = 'mongodb';

    protected $hidden = ['deleted_at', 'password', 'otp_secret'];

    public $integer = [
        'type' => 'integer',
    ];

    public $ip_format = [
        'type' => 'ip',
    ];

    public $date = [
        'type' => 'date',
    ];

    public $object = [
        'type' => 'object',
    ];

    public $double = [
        'type' => 'double',
    ];

    public $partial_text = [
        'type' => 'text',
        'fields' => [
          'analyzed' => [
            'type' => 'text',
            'analyzer' => 'partial_match_analyzer',
          ],
        ],
    ];

    public $partial_keyword = [
        'type' => 'keyword',
        'fields' => [
          'analyzed' => [
            'type' => 'keyword',
            'analyzer' => 'partial_match_analyzer',
          ],
        ],
    ];

    public $geo_shape = [
        'type' => 'geo_shape',
    ];

    public function __create_mappings ()
    {
      /*
      foreach ($this->setup AS $field => $layout)
      {
        $this->mapping['properties'][$field] = $this->{$layout};
      }

      return $this->mapping;
      */
    }
}
