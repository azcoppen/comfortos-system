<?php

namespace SmartRooms\Models\Property;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Models\Base;

use SmartRooms\Search\Elasticsearch\Serializers\BuildingSearch;

/**
 * Class Building.
 */
class Building extends Base implements Transformable
{
    use TransformableTrait, BuildingSearch;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $hidden = ['created_by'];

    protected $appends = ['images'];

    public function getImagesAttribute () : array
    {
      if ( $this->image )
      {
        return [
          'lg' => 'https://res.cloudinary.com/smartrooms/buildings/' . $this->image .'.png',
          'md' => 'https://res.cloudinary.com/smartrooms/c_scale,w_200/buildings/' . $this->image .'.png',
          'sm' => 'https://res.cloudinary.com/smartrooms/c_scale,w_25/buildings/' . $this->image .'.png',
        ];
      }

      return [
        'lg' => null,
        'md' => null,
        'sm' => null,
      ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }
}
