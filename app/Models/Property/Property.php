<?php

namespace SmartRooms\Models\Property;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Models\Base;
use SmartRooms\Models\Operator\Brand;

use SmartRooms\Models\Components\OS\MQTTBroker;
use SmartRooms\Models\Components\OS\WSBroker;

use SmartRooms\Search\Elasticsearch\Serializers\PropertySearch;

/**
 * Class Property.
 */
class Property extends Base implements Transformable
{
    use TransformableTrait, PropertySearch;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $hidden = ['brand_id', 'telephones', 'emails', 'created_by'];

    protected $appends = ['images'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getImagesAttribute () : array
    {
      if ( $this->image )
      {
        return [
          'lg' => 'https://res.cloudinary.com/smartrooms/properties/' . $this->image .'.png',
          'md' => 'https://res.cloudinary.com/smartrooms/c_scale,w_200/properties/' . $this->image .'.png',
          'sm' => 'https://res.cloudinary.com/smartrooms/c_scale,w_25/properties/' . $this->image .'.png',
        ];
      }

      return [
        'lg' => null,
        'md' => null,
        'sm' => null,
      ];
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function buildings()
    {
        return $this->hasMany(Building::class);
    }

    public function rooms()
    {
        return $this->hasManyThrough(Room::class, Building::class);
    }

    public function mqtt_brokers()
    {
        return $this->belongsToMany (MQTTBroker::class, null, 'property_ids', 'mqtt_broker_ids');
    }

    public function ws_brokers()
    {
        return $this->belongsToMany (WSBroker::class, null, 'property_ids', 'ws_broker_ids');
    }
}
