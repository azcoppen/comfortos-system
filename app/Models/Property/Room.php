<?php

namespace SmartRooms\Models\Property;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SmartRooms\Models\Base;

use SmartRooms\Models\Components\Devices\Bulb;
use SmartRooms\Models\Components\Devices\Camera;
use SmartRooms\Models\Components\Devices\Chromecast;
use SmartRooms\Models\Components\Devices\Dimmer;
use SmartRooms\Models\Components\Devices\LED;
use SmartRooms\Models\Components\Devices\Lock;
use SmartRooms\Models\Components\Devices\Motor;
use SmartRooms\Models\Components\Devices\PhysicalSwitch;
use SmartRooms\Models\Components\Devices\Plug;
use SmartRooms\Models\Components\Devices\Sensor;
use SmartRooms\Models\Components\Devices\Speaker;
use SmartRooms\Models\Components\Devices\STB;
use SmartRooms\Models\Components\Devices\Thermostat;
use SmartRooms\Models\Components\Devices\TVApp;
use SmartRooms\Models\Components\Devices\TV;

use SmartRooms\Models\Components\OS\HAB;
use SmartRooms\Models\Components\OS\Hub;

use SmartRooms\Models\Components\Mirror\Display;

use SmartRooms\Models\Components\Telecoms\DECT;
use SmartRooms\Models\Components\Telecoms\Extension;
use SmartRooms\Models\Components\Telecoms\Handset;
use SmartRooms\Models\Components\Telecoms\PBX;

use SmartRooms\Models\Components\Wifi\Network;
use SmartRooms\Models\Components\Wifi\Router;

use SmartRooms\Models\IDAM\User;
use SmartRooms\Traits\Models\HasAccessGrants;
use SmartRooms\Traits\Models\HasChangesAndReadings;

use SmartRooms\Models\ComfortOS\ComponentMap;
use SmartRooms\Models\ComfortOS\HABCommand;
use SmartRooms\Models\ComfortOS\HABSignal;

use SmartRooms\Models\ComfortOS\RoomHABCommandThroughput;
use SmartRooms\Models\ComfortOS\RoomHABSignalThroughput;

use SmartRooms\Search\Elasticsearch\Serializers\RoomSearch;

/**
 * Class Room.
 */
class Room extends Base implements Transformable
{
    use TransformableTrait, HasAccessGrants, RoomSearch;

    protected $guarded = [];

    protected $hidden = ['created_by', 'user_ids'];

    protected $appends = ['images'];

    public function getSummaryAttribute ()
    {
      return $this->signals->groupBy ('attribute')->transform(function ($item, $key) {
        if ( is_object ($item) && is_object ($item->first()) )
        {
          return $item->first()->value;
        }

        return null;
      })->sortKeys();
    }

    public function getImagesAttribute () : array
    {
      if ( $this->image )
      {
        return [
          'lg' => 'https://res.cloudinary.com/smartrooms/rooms/' . $this->image .'.png',
          'md' => 'https://res.cloudinary.com/smartrooms/c_scale,w_200/rooms/' . $this->image .'.png',
          'sm' => 'https://res.cloudinary.com/smartrooms/c_scale,w_25/rooms/' . $this->image .'.png',
        ];
      }

      return [
        'lg' => null,
        'md' => null,
        'sm' => null,
      ];
    }

    public function component_map ()
    {
      return $this->hasOne (ComponentMap::class);
    }

    public function command_throughput ()
    {
      return $this->hasMany (RoomHABCommandThroughput::class);
    }

    public function signal_throughput ()
    {
      return $this->hasMany (RoomHABSignalThroughput::class);
    }

    public function commands ()
    {
        return $this->hasMany (HABCommand::class)
          ->orderBy('created_at', 'DESC')
          ->take(50);
    }

    public function signals ()
    {
        return $this->hasMany (HABSignal::class)
          ->orderBy('created_at', 'DESC')
          ->take(50);
    }

    public function last_command ()
    {
        return $this->hasOne (HABCommand::class)
          ->orderBy('created_at', 'DESC');
    }

    public function last_signal ()
    {
        return $this->hasOne (HABSignal::class)
          ->orderBy('created_at', 'DESC');
    }

    public function building()
    {
        return $this->belongsTo(Building::class);
    }

    public function bulbs()
    {
        return $this->hasMany(Bulb::class);
    }

    public function cameras()
    {
        return $this->hasMany(Camera::class);
    }

    public function chromecasts()
    {
        return $this->hasMany(Chromecast::class);
    }

    public function dects()
    {
        return $this->hasMany(DECT::class);
    }

    public function dimmers()
    {
        return $this->hasMany(Dimmer::class);
    }

    public function extensions()
    {
        return $this->hasMany(Extension::class);
    }

    public function habs()
    {
        return $this->hasMany(HAB::class);
    }

    public function handsets()
    {
        return $this->hasMany(Handset::class);
    }

    public function hubs()
    {
        return $this->hasMany(HUB::class);
    }

    public function leds()
    {
        return $this->hasMany(LED::class);
    }

    public function locks()
    {
        return $this->hasMany(Lock::class);
    }

    public function mirrors()
    {
        return $this->hasMany(Display::class);
    }

    public function motors()
    {
        return $this->hasMany(Motor::class);
    }

    public function pbx()
    {
        return $this->hasOne(PBX::class);
    }

    public function plugs()
    {
        return $this->hasMany(Plug::class);
    }

    public function routers()
    {
        return $this->hasMany(Router::class);
    }

    public function sensors()
    {
        return $this->hasMany(Sensor::class);
    }

    public function speakers()
    {
        return $this->hasMany(Speaker::class);
    }

    public function stbs()
    {
        return $this->hasMany(STB::class);
    }

    public function tv_apps()
    {
        return $this->hasMany(TVApp::class);
    }

    public function switches()
    {
        return $this->hasMany(PhysicalSwitch::class);
    }

    public function thermostats()
    {
        return $this->hasMany(Thermostat::class);
    }

    public function tvs()
    {
        return $this->hasMany(TV::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, null, 'room_ids', 'user_ids');
    }

    // for API naming
    public function wifi()
    {
        return $this->hasMany (Network::class);
    }

    public function wifi_networks()
    {
        return $this->hasMany (Network::class);
    }
}
