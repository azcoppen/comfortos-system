<?php

namespace SmartRooms\Traits\Models;

use SmartRooms\Models\IDAM\User;

trait HasProvisioning
{
    public function installer()
    {
        return $this->belongsTo(User::class, 'installed_by');
    }

    public function provisioner()
    {
        return $this->belongsTo(User::class, 'provisioned_by');
    }

    public function purchaser()
    {
        return $this->belongsTo(User::class, 'ordered_by');
    }
}
