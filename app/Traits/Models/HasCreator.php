<?php

namespace SmartRooms\Traits\Models;

use SmartRooms\Models\IDAM\User;

trait HasCreator
{
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
