<?php

namespace SmartRooms\Traits\Models;

use SmartRooms\Models\Components\OS\VPNClient;
use SmartRooms\Models\Components\OS\VPNServer;
use Illuminate\Support\Str;
use \ReflectionClass;

trait BelongsToVPN
{

  public function vpn_clients ()
  {
      return $this->belongsToMany (VPNClient::class, null, strtolower ((new ReflectionClass($this))->getShortName()).'_ids', 'vpn_client_ids');
  }

  public function vpn_servers ()
  {
      return $this->belongsToMany (VPNServer::class, null, strtolower ((new ReflectionClass($this))->getShortName()).'_ids', 'vpn_server_ids');
  }
  
}
