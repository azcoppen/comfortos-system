<?php

namespace SmartRooms\Traits\Models;

use SmartRooms\Models\Property\Property;

trait BelongsToProperty
{
    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
