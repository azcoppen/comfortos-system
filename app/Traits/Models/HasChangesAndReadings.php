<?php

namespace SmartRooms\Traits\Models;

use SmartRooms\Models\Property\Room;
use SmartRooms\Models\ComfortOS\HABCommand;
use SmartRooms\Models\ComfortOS\HABSignal;

trait HasChangesAndReadings
{
    public function getSummaryAttribute ()
    {
      return $this->signals->groupBy ('attribute')->transform(function ($item, $key) {
        if ( is_object ($item) && is_object ($item->first()) )
        {
          return $item->first()->value;
        }

        return null;
      })->sortKeys();
    }

    public function commands ()
    {
        return $this->hasMany (HABCommand::class, 'component_id')
          ->orderBy('created_at', 'DESC')
          ->take(50);
    }

    public function signals ()
    {
        return $this->hasMany (HABSignal::class, 'component_id')
          ->orderBy('created_at', 'DESC')
          ->take(50);
    }

    public function last_commands ()
    {
        return $this->hasMany (HABCommand::class, 'component_id', '_id')
          ->orderBy('created_at', 'DESC')
          ->take(10);
    }

    public function last_command ()
    {
        return $this->hasOne (HABCommand::class, 'component_id', '_id')
          ->orderBy('created_at', 'DESC');
    }

    public function last_signal ()
    {
        return $this->hasOne (HABSignal::class, 'component_id', '_id')
          ->orderBy('created_at', 'DESC');
    }

    public function last_hsb_signal ()
    {
        return $this->hasOne (HABSignal::class, 'component_id', '_id')
          ->where('attribute', 'hsb')
          ->orderBy('created_at', 'DESC');
    }

}
