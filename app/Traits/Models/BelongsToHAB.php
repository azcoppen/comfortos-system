<?php

namespace SmartRooms\Traits\Models;

use SmartRooms\Models\Components\OS\HAB;

trait BelongsToHAB
{
    public function hab ()
    {
        return $this->belongsTo (HAB::class);
    }
}
