<?php

namespace SmartRooms\Traits\Models;

use SmartRooms\Models\Property\Room;

trait BelongsToRoom
{
    public function room()
    {
        return $this->belongsTo(Room::class);
    }
}
