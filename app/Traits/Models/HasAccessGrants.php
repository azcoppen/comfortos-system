<?php

namespace SmartRooms\Traits\Models;

use SmartRooms\Models\IDAM\Access;

trait HasAccessGrants
{
    public function accesses()
    {
        return $this->morphMany(Access::class, 'grantee');
    }

    public function accessibles()
    {
        return $this->morphMany(Access::class, 'accessible');
    }

    public function grantees()
    {
        return $this->morphMany(Access::class, 'grantee');
    }
}
