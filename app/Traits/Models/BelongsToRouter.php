<?php

namespace SmartRooms\Traits\Models;

use SmartRooms\Models\Components\Wifi\Router;
use Illuminate\Support\Str;

trait BelongsToRouter
{
    public function router ()
    {
        return $this->belongsTo (Router::class);
    }
}
