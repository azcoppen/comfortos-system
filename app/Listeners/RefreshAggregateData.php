<?php

namespace SmartRooms\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use SmartRooms\Contracts\Metrics\AggregationContract;

class RefreshAggregateData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if (isset($event->reload) && $event->reload !== false) {
            app(AggregationContract::class)->execute();
        }
    }
}
