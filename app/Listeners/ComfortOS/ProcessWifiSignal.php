<?php

namespace SmartRooms\Listeners\ComfortOS;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use SmartRooms\Events\ComfortOS\WifiSignalReceived;

use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;
use Illumindate\Support\Str;

class ProcessWifiSignal
{
    public $repository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->repository = app (NetworkRepository::class);
    }

    /**
     * Handle the event.
     *
     * @param  WifiSignalReceived  $event
     * @return void
     */
    public function handle(WifiSignalReceived $event)
    {
        if ( in_array ($event->signal->attribute, ['wifi-launches', 'wifi-renames', 'wifi-pwds', 'wifi-deletes']) )
        {
          $parts = explode (',', $event->signal->value);

          // could be an exception
          if ( is_array ($parts) && count ($parts) )
          {
            switch ($event->signal->attribute)
            {
              case 'wifi-launches':
                // network_id, ssid, password, profile_id, profile_name
                $network = $this->repository->findWhere ([
                  'room_id' => $event->signal->room_id,
                  'ssid'    => $parts[1],
                ])->first();

                if ( $network )
                {
                  $this->repository->update ([
                    'profile_index'     => $parts[3] ?? null,
                    'network_index'     => $parts[0] ?? null,
                    'security_profile'  => $parts[4] ?? null,
                    'confirmed_at'      => now(),
                    'active_at'         => now(),
                  ], $network->_id);
                }

              break;

              case 'wifi-renames':
                // network_id, old_name, new_name
                $network = $this->repository->findWhere ([
                  'room_id'       => $event->signal->room_id,
                  'network_index' => $parts[0] ?? rand (0,1000),
                  'ssid'          => $parts[1] ?? Str::random(25),
                ])->first();

                if ( $network )
                {
                  $this->repository->update ([
                    'ssid'     => $parts[2],
                  ], $network->_id);
                }

              break;

              case 'wifi-pwds':
                // network_id, ssid, new_password, profile_id, profile_name
                $network = $this->repository->findWhere ([
                  'room_id'       => $event->signal->room_id,
                  'network_index' => $parts[0] ?? rand (0,1000),
                  'ssid'          => $parts[1] ?? Str::random(25),
                ])->first();

                if ( $network )
                {
                  $this->repository->update ([
                    'password'     => \Crypt::encrypt ($parts[2]),
                  ], $network->_id);
                }

              break;

              case 'wifi-deletes':
                $network = $this->repository->findWhere ([
                  'room_id'       => $event->signal->room_id,
                  'ssid'          => $parts[0] ?? Str::random(25),
                ])->scopeQuery(function($query) {
                    return $query->withTrashed();
                })->first();

                if (! $network->deleted_at )
                {
                  $this->repository->delete ($network->_id);
                }
              break;
            }
          }


        }
    }
}
