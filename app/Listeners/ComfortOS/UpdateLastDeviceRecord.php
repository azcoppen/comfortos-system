<?php

namespace SmartRooms\Listeners\ComfortOS;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateLastDeviceRecord
{
    private $room;
    private $update;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //

        /*

        temperature
        uv
        humidity
        motion
        tamper
        illuminance
        smoke
        lock:
        lights: num on
        tv
        voicemail
        messages

        bulbs: []
        dimmers: []
        led_strips: []
        locks: []
        plugs: []
        sensors: []
        speakers: []
        switches: []
        thermostats: []
        tvs: []
        wifi: []

        */
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
    }
}
