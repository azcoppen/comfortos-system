<?php

namespace SmartRooms\Contracts\Repositories\ComfortOS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HABCommandRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\ComfortOS;
 */
interface HABCommandRepository extends RepositoryInterface
{
    //
}
