<?php

namespace SmartRooms\Contracts\Repositories\ComfortOS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MIAAlertThroughputHourlyRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\ComfortOS;
 */
interface MIAAlertThroughputHourlyRepository extends RepositoryInterface
{
    //
}
