<?php

namespace SmartRooms\Contracts\Repositories\ComfortOS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomHABCommandThroughputRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\ComfortOS;
 */
interface RoomHABCommandThroughputHourlyRepository extends RepositoryInterface
{
    //
}
