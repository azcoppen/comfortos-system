<?php

namespace SmartRooms\Contracts\Repositories\ComfortOS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MIAAlertThroughputRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\ComfortOS;
 */
interface MIAAlertThroughputRepository extends RepositoryInterface
{
    //
}
