<?php

namespace SmartRooms\Contracts\Repositories\ComfortOS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ComponentMIAAlertThroughputHourlyRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\ComfortOS;
 */
interface ComponentMIAAlertThroughputHourlyRepository extends RepositoryInterface
{
    //
}
