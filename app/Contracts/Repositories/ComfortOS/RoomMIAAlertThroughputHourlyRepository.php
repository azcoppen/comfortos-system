<?php

namespace SmartRooms\Contracts\Repositories\ComfortOS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomMIAAlertThroughputHourlyRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\ComfortOS;
 */
interface RoomMIAAlertThroughputHourlyRepository extends RepositoryInterface
{
    //
}
