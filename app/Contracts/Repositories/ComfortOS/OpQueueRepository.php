<?php

namespace SmartRooms\Contracts\Repositories\ComfortOS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository.
 */
interface OpQueueRepository extends RepositoryInterface
{
    //
}
