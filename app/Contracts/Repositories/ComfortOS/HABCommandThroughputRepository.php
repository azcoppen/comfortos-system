<?php

namespace SmartRooms\Contracts\Repositories\ComfortOS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HABCommandThroughputRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\ComfortOS;
 */
interface HABCommandThroughputRepository extends RepositoryInterface
{
    //
}
