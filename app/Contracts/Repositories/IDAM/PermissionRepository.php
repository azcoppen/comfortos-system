<?php

namespace SmartRooms\Contracts\Repositories\IDAM;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository.
 */
interface PermissionRepository extends RepositoryInterface
{
    //
}
