<?php

namespace SmartRooms\Contracts\Repositories\IDAM;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository.
 */
interface RoleRepository extends RepositoryInterface
{
    //
}
