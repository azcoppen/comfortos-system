<?php

namespace SmartRooms\Contracts\Repositories\Workflow;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegistrationRepository.
 */
interface RegistrationRepository extends RepositoryInterface
{
    //
}
