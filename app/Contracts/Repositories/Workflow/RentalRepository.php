<?php

namespace SmartRooms\Contracts\Repositories\Workflow;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RentalRepository.
 */
interface RentalRepository extends RepositoryInterface
{
    //
}
