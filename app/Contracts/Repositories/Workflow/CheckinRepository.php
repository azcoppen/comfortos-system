<?php

namespace SmartRooms\Contracts\Repositories\Workflow;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CheckinRepository.
 */
interface CheckinRepository extends RepositoryInterface
{
    //
}
