<?php

namespace SmartRooms\Contracts\Repositories\Workflow;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OccupancyRepository.
 */
interface OccupancyRepository extends RepositoryInterface
{
    //
}
