<?php

namespace SmartRooms\Contracts\Repositories\Workflow;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CheckoutRepository.
 */
interface CheckoutRepository extends RepositoryInterface
{
    //
}
