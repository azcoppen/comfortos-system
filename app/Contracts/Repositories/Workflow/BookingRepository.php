<?php

namespace SmartRooms\Contracts\Repositories\Workflow;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BookingRepository.
 */
interface BookingRepository extends RepositoryInterface
{
    //
}
