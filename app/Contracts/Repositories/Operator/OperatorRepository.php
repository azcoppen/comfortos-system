<?php

namespace SmartRooms\Contracts\Repositories\Operator;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OperatorRepository.
 */
interface OperatorRepository extends RepositoryInterface
{
    //
}
