<?php

namespace SmartRooms\Contracts\Repositories\Operator;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BrandRepository.
 */
interface BrandRepository extends RepositoryInterface
{
    //
}
