<?php

namespace SmartRooms\Contracts\Repositories\Property;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BuildingRepository.
 */
interface BuildingRepository extends RepositoryInterface
{
    //
}
