<?php

namespace SmartRooms\Contracts\Repositories\Property;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConfigurationRepository.
 */
interface ConfigurationRepository extends RepositoryInterface
{
    //
}
