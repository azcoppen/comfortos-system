<?php

namespace SmartRooms\Contracts\Repositories\Property;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomRepository.
 */
interface RoomRepository extends RepositoryInterface
{
    //
}
