<?php

namespace SmartRooms\Contracts\Repositories\Property;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PropertyRepository.
 */
interface PropertyRepository extends RepositoryInterface
{
    //
}
