<?php

namespace SmartRooms\Contracts\Repositories\Components\Mirror;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServerRepository.
 */
interface DisplayRepository extends RepositoryInterface
{
    //
}
