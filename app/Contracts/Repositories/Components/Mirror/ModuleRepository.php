<?php

namespace SmartRooms\Contracts\Repositories\Components\Mirror;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServerRepository.
 */
interface ModuleRepository extends RepositoryInterface
{
    //
}
