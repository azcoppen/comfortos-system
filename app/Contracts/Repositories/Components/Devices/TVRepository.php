<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppRepository.
 */
interface TVRepository extends RepositoryInterface
{
    //
}
