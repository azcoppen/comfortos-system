<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SpeakerRepository.
 */
interface SpeakerRepository extends RepositoryInterface
{
    //
}
