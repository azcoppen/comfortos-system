<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SwitchRepository.
 */
interface PhysicalSwitchRepository extends RepositoryInterface
{
    //
}
