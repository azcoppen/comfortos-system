<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CodeRepository.
 */
interface CodeRepository extends RepositoryInterface
{
    //
}
