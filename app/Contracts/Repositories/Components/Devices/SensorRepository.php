<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SensorRepository.
 */
interface SensorRepository extends RepositoryInterface
{
    //
}
