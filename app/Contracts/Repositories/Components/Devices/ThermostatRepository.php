<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ThermostatRepository.
 */
interface ThermostatRepository extends RepositoryInterface
{
    //
}
