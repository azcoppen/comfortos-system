<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CameraRepository.
 */
interface CameraRepository extends RepositoryInterface
{
    //
}
