<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LockRepository.
 */
interface LockRepository extends RepositoryInterface
{
    //
}
