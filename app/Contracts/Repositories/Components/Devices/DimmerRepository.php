<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DimmerRepository.
 */
interface DimmerRepository extends RepositoryInterface
{
    //
}
