<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BulbRepository.
 */
interface LEDRepository extends RepositoryInterface
{
    //
}
