<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppRepository.
 */
interface TVAppRepository extends RepositoryInterface
{
    //
}
