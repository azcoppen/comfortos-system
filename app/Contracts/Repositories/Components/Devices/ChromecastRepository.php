<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChromecastRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\Components\Devices;
 */
interface ChromecastRepository extends RepositoryInterface
{
    //
}
