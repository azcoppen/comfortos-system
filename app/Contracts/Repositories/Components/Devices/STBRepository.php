<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface STBRepository.
 */
interface STBRepository extends RepositoryInterface
{
    //
}
