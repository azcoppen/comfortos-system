<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BulbRepository.
 */
interface BulbRepository extends RepositoryInterface
{
    //
}
