<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MotorRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\Components\Devices;
 */
interface MotorRepository extends RepositoryInterface
{
    //
}
