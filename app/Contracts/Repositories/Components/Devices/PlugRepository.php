<?php

namespace SmartRooms\Contracts\Repositories\Components\Devices;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlugRepository.
 */
interface PlugRepository extends RepositoryInterface
{
    //
}
