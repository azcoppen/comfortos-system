<?php

namespace SmartRooms\Contracts\Repositories\Components\Telecoms;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ExtensionRepository.
 */
interface ExtensionRepository extends RepositoryInterface
{
    //
}
