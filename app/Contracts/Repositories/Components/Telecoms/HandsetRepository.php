<?php

namespace SmartRooms\Contracts\Repositories\Components\Telecoms;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HandsetRepository.
 */
interface HandsetRepository extends RepositoryInterface
{
    //
}
