<?php

namespace SmartRooms\Contracts\Repositories\Components\OS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MQTTBrokerRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\OS;
 */
interface MQTTBrokerRepository extends RepositoryInterface
{
    //
}
