<?php

namespace SmartRooms\Contracts\Repositories\Components\OS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServerRepository.
 */
interface HABRepository extends RepositoryInterface
{
    //
}
