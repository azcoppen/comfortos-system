<?php

namespace SmartRooms\Contracts\Repositories\Components\OS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VPNClientRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\Components\OS;
 */
interface VPNClientRepository extends RepositoryInterface
{
    //
}
