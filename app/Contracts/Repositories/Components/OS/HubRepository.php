<?php

namespace SmartRooms\Contracts\Repositories\Components\OS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SmartHubRepository.
 */
interface HubRepository extends RepositoryInterface
{
    //
}
