<?php

namespace SmartRooms\Contracts\Repositories\Components\OS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface WSBrokerRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\OS;
 */
interface WSBrokerRepository extends RepositoryInterface
{
    //
}
