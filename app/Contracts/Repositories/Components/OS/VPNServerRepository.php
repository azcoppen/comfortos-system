<?php

namespace SmartRooms\Contracts\Repositories\Components\OS;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VPNRepository.
 *
 * @package namespace SmartRooms\Contracts\Repositories\OS;
 */
interface VPNServerRepository extends RepositoryInterface
{
    //
}
