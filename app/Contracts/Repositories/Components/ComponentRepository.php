<?php

namespace SmartRooms\Contracts\Repositories\Components;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ComponentRepository.
 */
interface ComponentRepository extends RepositoryInterface
{
    //
}
