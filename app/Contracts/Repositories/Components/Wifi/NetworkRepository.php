<?php

namespace SmartRooms\Contracts\Repositories\Components\Wifi;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PasswordRepository.
 */
interface NetworkRepository extends RepositoryInterface
{
    //
}
