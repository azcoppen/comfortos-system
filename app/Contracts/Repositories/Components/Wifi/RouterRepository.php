<?php

namespace SmartRooms\Contracts\Repositories\Components\Wifi;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RouterRepository.
 */
interface RouterRepository extends RepositoryInterface
{
    //
}
