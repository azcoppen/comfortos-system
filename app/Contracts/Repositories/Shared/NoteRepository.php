<?php

namespace SmartRooms\Contracts\Repositories\Shared;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository.
 */
interface NoteRepository extends RepositoryInterface
{
    //
}
