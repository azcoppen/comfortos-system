<?php

namespace SmartRooms\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class HSB implements Rule
{
    public $message = 'HSB values must be 3 values separated by commas e.g. ([0-360],[0-100],[0-100]). ';

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $parts = explode (',', $value);

        if (! is_array ($parts) )
        {
          $this->message .= "Could not parse string into 3 parts.";
          return false;
        }

        if ( count ($parts) != 3 )
        {
          $this->message .= "String does not have 3 values.";
          return false;
        }

        foreach ($parts AS $index => $pc)
        {
          if (! is_numeric ($pc) )
          {
            $this->message .= "Value ".($index+1)." is not numeric.";
            return false;
          }

          if ( is_numeric ($pc) && $pc < 0 )
          {
            $this->message .= "Value ".($index+1)." cannot be less than zero.";
            return false;
          }

          if ( $index == 0 )
          {
            if ( is_numeric ($pc) && $pc > 360 )
            {
              $this->message .= "Value ".($index+1)." cannot be more than 360.";
              return false;
            }
          }

          if ( $index > 0 )
          {
            if ( is_numeric ($pc) && $pc > 100 )
            {
              $this->message .= "Value ".($index+1)." cannot be more than 100.";
              return false;
            }
          }

        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
