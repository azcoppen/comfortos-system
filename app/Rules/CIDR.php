<?php

namespace SmartRooms\Rules;

use Illuminate\Contracts\Validation\Rule;

class CIDR implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      $parts = explode ('/', $value);

      if (! is_array ($parts) || count($parts) != 2 )
      {
          return false;
      }

      return
           preg_match ("/^\d+$/", $parts[1])
        && intval ($parts[1]) > 0
        && intval ($parts[1]) <= 32
        && filter_var ($parts[0], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be a properly-formatted CIDR IP range.';
    }
}
