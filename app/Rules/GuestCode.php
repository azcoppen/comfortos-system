<?php

namespace SmartRooms\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class GuestCode implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ( in_array ($value, config ('comfortos.overrides')) )
        {
            return true;
        }

        return
        Str::contains ($value, 'M') &&
        is_numeric (Str::before ($value, 'M')) &&
        intval (Str::before ($value, 'M')) < 1000 &&
        Str::contains ($value, 'S') &&
        is_numeric (Str::after ($value, 'S')) &&
        intval (Str::after ($value, 'S')) < 1000;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __ ('guest/auth.login.form.otp_format');
    }
}
