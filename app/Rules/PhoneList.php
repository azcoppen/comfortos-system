<?php

namespace SmartRooms\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class PhoneList implements Rule
{
    public $country;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct( string $country = 'US' )
    {
        $this->country = $country;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      $entries = explode ("\n", trim ($value));

      if (is_array ($entries) && count ($entries))
      {
        foreach ($entries AS $entry)
        {
          if (! empty ($entry) )
          {

            if ( Validator::make (compact ('entry'), ['entry' => 'phone:AUTO,LENIENT,'.$this->country])->fails() )
            {
              //return false;
            }

          }
        }
      }

      return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'One or more phone numbers in the list is invalid.';
    }
}
