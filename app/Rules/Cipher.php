<?php

namespace SmartRooms\Rules;

use Illuminate\Contracts\Validation\Rule;

class Cipher implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      return in_array ($value, openssl_get_cipher_methods());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be a properly-formatted cipher codename.';
    }
}
