<?php

namespace SmartRooms\Rules;

use Illuminate\Contracts\Validation\Rule;

use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class RoomRange implements Rule
{
    private $building_id;
    private $floor;
    private $num;
    private $conflicts;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct ( ?string $building_id, ?int $floor, ?int $num )
    {
        $this->building_id = $building_id;
        $this->floor = $floor;
        $this->num = $num;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      $existing = app (RoomRepository::class)
        ->scopeQuery(function($query) use ($value) {
            return $query
              ->where ('building_id', $this->building_id)
              ->where('floor', intval ($this->floor))
              ->whereIn ('number', range ($value, ($value + $this->num), 1));
        })->all();

      $this->conflicts = $existing->pluck ('number')->implode (', ');

      return $existing->count() < 1;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Rooms '.$this->conflicts. ' are allocated on this floor already. Your request conflicts with at least one room number.';
    }
}
