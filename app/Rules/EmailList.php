<?php

namespace SmartRooms\Rules;

use Illuminate\Contracts\Validation\Rule;

class EmailList implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      $entries = explode ("\n", trim ($value));

      if (is_array ($entries) && count ($entries))
      {
        foreach ($entries AS $entry)
        {
          if (! empty ($entry) )
          {
            if (! filter_var ($entry, FILTER_VALIDATE_EMAIL) )
            {
              return false;
            }
          }
        }
      }

      return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'One or more emails in the list is invalid.';
    }
}
