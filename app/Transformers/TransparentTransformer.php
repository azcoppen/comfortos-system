<?php

namespace SmartRooms\Transformers;

use League\Fractal\TransformerAbstract;

class TransparentTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($model)
    {
        if (is_array($model)) {
            return $model;
        }

        return $model->toArray();
    }
}
