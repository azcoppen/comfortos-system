<?php

namespace SmartRooms\Transformers\API\ComfortOS;

use League\Fractal\TransformerAbstract;
use SmartRooms\Models\Components\Telecoms\Extension;

class ExtensionTransformer extends TransformerAbstract
{
    public $method;

    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    public function __construct ( string $method = 'index' )
    {
      $this->method = $method;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform (Extension $model)
    {
        if ($this->method == 'show' )
        {
          $model->handsets->transform(function ($item, $key) {
              return $item->_id;
          });

          if ( isset ($model->pbx) && is_object ($model->pbx) )
          {

          }
        }


        return $model->toArray();
    }
}
