<?php

namespace SmartRooms\Transformers\API\ComfortOS;

use League\Fractal\TransformerAbstract;
use SmartRooms\Models\Components\Devices\Bulb;

class DeviceTransformer extends TransformerAbstract
{
    public $method;

    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
     protected $availableIncludes = [

     ];

     public function __construct ( string $method = 'index' )
     {
       $this->method = $method;
     }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform ($model)
    {
        if ($this->method == 'show' )
        {
          if ( isset ($model->commands) && is_object ($model->commands) && count ($model->commands) )
          {
            $model->commands->transform(function ($item, $key) {
                unset ($item->room_id, $item->group, $item->component_id, $item->updated_at);
                return $item;
            });
          }

          if ( isset ($model->signals) && is_object ($model->signals) && count ($model->signals) )
          {
            $model->signals->transform(function ($item, $key) {
                unset ($item->room_id, $item->group, $item->component_id, $item->updated_at);
                return $item;
            });
          }
        }

        return $model->toArray();
    }
}
