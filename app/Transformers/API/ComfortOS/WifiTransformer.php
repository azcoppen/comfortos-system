<?php

namespace SmartRooms\Transformers\API\ComfortOS;

use League\Fractal\TransformerAbstract;
use SmartRooms\Models\Components\Wifi\Network;

class WifiTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform (Network $model)
    {
        return $model->toArray();
    }
}
