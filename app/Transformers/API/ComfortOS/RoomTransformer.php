<?php

namespace SmartRooms\Transformers\API\ComfortOS;

use League\Fractal\TransformerAbstract;
use SmartRooms\Models\Property\Room;

class RoomTransformer extends TransformerAbstract
{
    public $relations;

    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    public function __construct (array $relations = [])
    {
      $this->relations = $relations;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform (Room $model)
    {
        if ( $this->relations && count ($this->relations) )
        {
          foreach ($this->relations AS $relation)
          {
            $model->{$relation}->transform(function ($item, $key) {
                return $item->_id;
            });
          }

        }

        return $model->setAppends(['summary'])->toArray();
    }
}
