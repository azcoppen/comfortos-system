<?php

namespace SmartRooms\Transformers\API\ComfortOS;

use League\Fractal\TransformerAbstract;
use SmartRooms\Models\Property\Building;

class BuildingTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform (Building $model)
    {
        $model->rooms->transform(function ($item, $key) {
            return $item->_id;
        });

        return $model->toArray();
    }
}
