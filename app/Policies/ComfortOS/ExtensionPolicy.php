<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Telecoms\Extension;
use SmartRooms\Models\IDAM\User;

class ExtensionPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
