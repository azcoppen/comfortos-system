<?php

namespace SmartRooms\Policies\ComfortOS;

use SmartRooms\Models\IDAM\User;

abstract class BaseReadWriteDeletePolicy extends BaseUserRoomAccessPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @return mixed
     */
    public function index(?User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can create any models.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @return mixed
     */
    public function create(?User $user)
    {
      if ($this->restricted($user)) {
          return false;
      }

      return $this->superuser($user);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function show(?User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model) || $this->within_available_room($model);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function commands(?User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model) || $this->within_available_room($model);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function signals(?User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model) || $this->within_available_room($model);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @return mixed
     */
    public function store(User $user)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->within_available_room($model);
    }

    /**
     * Determine whether the user can provision (link/attach) models.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @return mixed
     */
    public function provision(User $user)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->within_available_room($model);
    }

    /**
     * Determine whether the user can deprovision (unlink/detach) models.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @return mixed
     */
    public function deprovision(User $user)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->within_available_room($model);
    }

    /**
     * Determine whether the user can edit the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function edit(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function update(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model) || $this->within_available_room($model);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function destroy(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->within_available_room($model);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function restore(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function forceDelete(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user);
    }
}
