<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\TV;
use SmartRooms\Models\IDAM\User;

class TVPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
