<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Motor;
use SmartRooms\Models\IDAM\User;

class MotorPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
