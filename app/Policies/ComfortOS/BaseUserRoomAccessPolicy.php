<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Telecoms\Extension;
use SmartRooms\Models\Components\Wifi\Network;
use SmartRooms\Models\IDAM\User;
use SmartRooms\Models\Property\Building;
use SmartRooms\Models\Property\Property;
use SmartRooms\Models\Property\Room;

abstract class BaseUserRoomAccessPolicy
{
    use HandlesAuthorization;

    public function rooms(User $user) : array
    {
        return $user->accesses()
            ->where('end_at', '>', now())
            ->get()
            ->pluck('accessible_id')
            ->all();
    }

    public function restricted(User $user) : bool
    {
        return false;
    }

    public function self_operation(User $user, $model) : bool
    {
        return get_class($model) == User::class && $user->_id == $model->_id;
    }

    public function superuser(User $user)
    {
        return $user->hasAnyRole(['developer', 'superuser', 'administrator']);
    }

    public function within_available_room($model) : bool
    {
        switch (get_class($model)) {
            case Building::class:
            case Property::class:
                return $model->rooms()->whereIn('_id', $this->rooms())->count() > 0;
            break;

            case Room::class:
                return in_array($model->_id, $this->rooms());
            break;

            case Extension::class:
                $dects = $model->base_stations()->whereIn('room_id', $this->rooms())->count();

                if ($dects > 0) {
                    return true;
                }

                $handsets = $model->handsets()->whereIn('room_id', $this->rooms())->count();
                if ($handsets > 0) {
                    return true;
                }
            break;

            case Network::class:
                return in_array($model->router->room_id, $this->rooms());
            break;

            default:

                if (! isset($model->room_id)) {
                    return false;
                }

                return in_array($model->room_id, $this->rooms());

            break;
        }

        return false;
    }
}
