<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Telecoms\DECT;
use SmartRooms\Models\IDAM\User;

class DECTPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
