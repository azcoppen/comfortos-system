<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Sensor;
use SmartRooms\Models\IDAM\User;

class SensorPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
