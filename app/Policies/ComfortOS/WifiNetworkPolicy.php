<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Wifi\Network;
use SmartRooms\Models\IDAM\User;

class WifiNetworkPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
