<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Plug;
use SmartRooms\Models\IDAM\User;

class PlugPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
