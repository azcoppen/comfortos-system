<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Wifi\Router;
use SmartRooms\Models\IDAM\User;

class RouterPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
