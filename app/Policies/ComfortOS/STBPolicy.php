<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\STB;
use SmartRooms\Models\IDAM\User;

class STBPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
