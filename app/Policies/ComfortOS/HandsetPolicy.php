<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Telecoms\Handset;
use SmartRooms\Models\IDAM\User;

class HandsetPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
