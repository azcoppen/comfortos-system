<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Bulb;
use SmartRooms\Models\IDAM\User;

class BulbPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
