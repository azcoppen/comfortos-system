<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Thermostat;
use SmartRooms\Models\IDAM\User;

class ThermostatPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
