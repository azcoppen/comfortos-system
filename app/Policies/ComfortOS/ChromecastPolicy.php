<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Camera;
use SmartRooms\Models\IDAM\User;

class ChromecastPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
