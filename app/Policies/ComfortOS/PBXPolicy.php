<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\OS\HAB;
use SmartRooms\Models\IDAM\User;

class PBXPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
