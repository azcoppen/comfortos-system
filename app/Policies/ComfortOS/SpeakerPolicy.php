<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Speaker;
use SmartRooms\Models\IDAM\User;

class SpeakerPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
