<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Lock;
use SmartRooms\Models\IDAM\User;

class LockPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
