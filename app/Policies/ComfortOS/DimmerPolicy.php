<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Dimmer;
use SmartRooms\Models\IDAM\User;

class DimmerPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
