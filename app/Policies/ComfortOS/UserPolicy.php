<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\IDAM\User;

class UserPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
