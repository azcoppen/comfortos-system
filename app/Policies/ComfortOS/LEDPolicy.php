<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Bulb;
use SmartRooms\Models\IDAM\User;

class LEDPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
