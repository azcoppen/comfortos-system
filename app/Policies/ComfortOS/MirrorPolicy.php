<?php

namespace SmartRooms\Policies\ComfortOS;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\Components\Devices\Mirror\Display;
use SmartRooms\Models\IDAM\User;

class MirrorPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
