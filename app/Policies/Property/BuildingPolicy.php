<?php

namespace SmartRooms\Policies\Property;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\IDAM\User;
use SmartRooms\Models\Property\Building;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class BuildingPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function show(?User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model) || in_array ($model->_id, app(UserConstraintManagementContract::class)->target($user)->buildings());
    }


    /**
     * Determine whether the user can edit the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function edit(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model) || in_array ($model->_id, app(UserConstraintManagementContract::class)->target($user)->buildings());
    }

    /**
     * Determine whether the user can edit the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function stickers(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model) || in_array ($model->_id, app(UserConstraintManagementContract::class)->target($user)->buildings());
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function update(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model) || in_array ($model->_id, app(UserConstraintManagementContract::class)->target($user)->buildings());
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function destroy(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || in_array ($model->_id, app(UserConstraintManagementContract::class)->target($user)->buildings());
    }

}
