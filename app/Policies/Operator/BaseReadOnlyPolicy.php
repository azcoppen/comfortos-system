<?php

namespace SmartRooms\Policies\Operator;

use Illuminate\Auth\Access\Response;
use SmartRooms\Models\IDAM\User;

abstract class BaseReadOnlyPolicy extends BaseOperatorAccessPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @return mixed
     */
    public function index(?User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function show(?User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user) || $this->self_operation($user, $model);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @return mixed
     */
    public function store(User $user)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function update(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function destroy(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function restore(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \SmartRooms\Models\IDAM\User  $user
     * @param  $model
     * @return mixed
     */
    public function forceDelete(User $user, $model)
    {
        if ($this->restricted($user)) {
            return false;
        }

        return $this->superuser($user);
    }
}
