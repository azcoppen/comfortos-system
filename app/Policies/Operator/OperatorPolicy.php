<?php

namespace SmartRooms\Policies\Operator;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\IDAM\User;

class OperatorPolicy extends BaseReadWriteDeletePolicy
{
    use HandlesAuthorization;
}
