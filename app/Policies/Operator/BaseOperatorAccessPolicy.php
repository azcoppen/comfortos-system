<?php

namespace SmartRooms\Policies\Operator;

use Illuminate\Auth\Access\HandlesAuthorization;
use SmartRooms\Models\IDAM\User;

abstract class BaseOperatorAccessPolicy
{
    use HandlesAuthorization;

    public function restricted(User $user) : bool
    {
        return false;
    }

    public function self_operation(User $user, $model) : bool
    {
        return get_class($model) == User::class && $user->_id == $model->_id;
    }

    public function superuser(User $user)
    {
        return $user->hasAnyRole(['developer', 'superuser', 'administrator']);
    }
}
