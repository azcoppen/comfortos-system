<?php

namespace SmartRooms\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Log;
use SmartRooms\Contracts\ComfortOS\API\LockManagementContract;
use SmartRooms\Contracts\Repositories\Components\Devices\CodeRepository;

use SmartRooms\Contracts\System\FlowOutputContract;

class DestroyExpiredLockCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartrooms:prune-lockcodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes smart lock codes set to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app (FlowOutputContract::class)->signal ($this->signature . ' ('.$this->description.')');

        $codes = app(CodeRepository::class)->with(['lock'])->all();

        foreach ($codes as $code)
        {
            try
            {

                if ( isset($code->expire_at) && $code->expire_at->timestamp > time())
                {
                    app (LockManagementContract::class)
                    ->entity($code->lock)
                    ->wipe($code);
                }

                if (isset($code->expire_at) && ($code->expire_at->timestamp - time() < 300))
                {
                    //event ( new LockCodeAboutToExpire ($code) );
                }

            }
            catch (Exception $e) 
            {
                Log::error($e);
            }
        }
    }
}
