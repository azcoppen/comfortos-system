<?php

namespace SmartRooms\Console\Commands;

use Illuminate\Console\Command;

use SmartRooms\Search\Elasticsearch\Indices;

class UpdateElasticIndices extends Command
{
    private $indices = [
      Indices\BrandIndexConfigurator::class,
      Indices\BuildingIndexConfigurator::class,
      Indices\BulbIndexConfigurator::class,
      Indices\CameraIndexConfigurator::class,
      Indices\ChromecastIndexConfigurator::class,
      Indices\DECTIndexConfigurator::class,
      Indices\DimmerIndexConfigurator::class,
      Indices\ExtensionIndexConfigurator::class,
      //Indices\HABCommandIndexConfigurator::class,
      Indices\HABIndexConfigurator::class,
      //Indices\HABSignalIndexConfigurator::class,
      Indices\HandsetIndexConfigurator::class,
      Indices\LEDIndexConfigurator::class,
      Indices\LockIndexConfigurator::class,
      Indices\LockCodeIndexConfigurator::class,
      Indices\MirrorDisplayIndexConfigurator::class,
      Indices\MirrorModuleIndexConfigurator::class,
      Indices\MotorIndexConfigurator::class,
      Indices\MQTTBrokerIndexConfigurator::class,
      //Indices\NoteIndexConfigurator::class,
      Indices\OperatorIndexConfigurator::class,
      //Indices\OpQueueIndexConfigurator::class,
      Indices\PBXIndexConfigurator::class,
      Indices\PlugIndexConfigurator::class,
      Indices\PropertyIndexConfigurator::class,
      //Indices\RoleIndexConfigurator::class,
      Indices\RoomIndexConfigurator::class,
      Indices\RouterIndexConfigurator::class,
      Indices\SensorIndexConfigurator::class,
      Indices\SpeakerIndexConfigurator::class,
      Indices\STBIndexConfigurator::class,
      Indices\SwitchIndexConfigurator::class,
      Indices\ThermostatIndexConfigurator::class,
      Indices\TVAppIndexConfigurator::class,
      Indices\TVIndexConfigurator::class,
      Indices\UserIndexConfigurator::class,
      Indices\VPNClientIndexConfigurator::class,
      Indices\VPNServerIndexConfigurator::class,
      Indices\WifiIndexConfigurator::class,
      Indices\WSBrokerIndexConfigurator::class,
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartrooms:update-indices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all Elasticsearch indices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach ($this->indices as $index) {
            $this->line('Updating '.$index);
            $this->call('elastic:update-index', ['index-configurator' => $index]);
        }
        return 0;
    }
}
