<?php

namespace SmartRooms\Console\Commands;

use Illuminate\Console\Command;

use SmartRooms\Contracts\Repositories\Components\Devices;
use SmartRooms\Contracts\Repositories\Components\Mirror;
use SmartRooms\Contracts\Repositories\Components\OS;
use SmartRooms\Contracts\Repositories\Components\Telecoms;
use SmartRooms\Contracts\Repositories\Components\Wifi;

use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertRepository;

use SmartRooms\Contracts\System\FlowOutputContract;

use Illuminate\Support\Str;
use \Exception;
use \ReflectionClass;

class CheckMIA extends Command
{
    public $repository;

    public $repositories = [

      Wifi\RouterRepository::class,
      Mirror\DisplayRepository::class,

      Telecoms\DECTRepository::class,
      Telecoms\HandsetRepository::class,

      Devices\BulbRepository::class,
      Devices\CameraRepository::class,
      Devices\ChromecastRepository::class,
      Devices\CodeRepository::class,
      Devices\DimmerRepository::class,
      Devices\LEDRepository::class,

      Devices\LockRepository::class,
      Devices\MotorRepository::class,
      Devices\PhysicalSwitchRepository::class,
      Devices\PlugRepository::class,
      Devices\SensorRepository::class,
      Devices\SpeakerRepository::class,
      Devices\STBRepository::class,
      Devices\ThermostatRepository::class,
      Devices\TVRepository::class,
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartrooms:mia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks for devices which may be missing or offline';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->repository = app (MIAAlertRepository::class);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      app (FlowOutputContract::class)->signal ($this->signature . ' ('.$this->description.')');

        foreach ($this->repositories AS $repo)
        {
          app (FlowOutputContract::class)->signal ('MIA Check[repo]: '.(new ReflectionClass ($repo))->getShortName());

          foreach (app($repo)->all() AS $obj)
          {
            app (FlowOutputContract::class)->signal ('MIA Check[object]: '.$obj->mia_alert_mins.'mins ['.strtolower (Str::plural ( (new ReflectionClass ($obj))->getShortName() )).']: '.$obj->_id);

            try
            {
              if ($obj->active)
              {

                // Only generate a new record every 15mins as it's redundant
                $last_alert = $this->repository->findWhere([
                  'room_id'         => $obj->room_id ?? null,
                  'component_id'    => $obj->_id ?? null,
                ])->first();

                if ( method_exists ($obj, 'last_signal') )
                {
                  if (! is_object ($obj->last_signal) )
                  {
                    app (FlowOutputContract::class)->signal ('MIA Check [object]: '.$obj->_id.' does not have a last_signal.');

                    if ( !$last_alert || ($last_alert && $last_alert->created_at->diffInMinutes (now()) > 15))
                    {
                      $this->repository->create ([
                        'room_id'         => $obj->room_id ?? null,
                        'component_id'    => $obj->_id,
                        'group'           => strtolower (Str::plural ( (new ReflectionClass ($obj))->getShortName() )),
                        'threshold'       => $obj->mia_alert_mins,
                        'last_signal_id'  => null,
                        'last_seen_at'    => null,
                        'last_seen_mins'  => null,
                        'ignored_at'      => null,
                      ]);
                    }

                  }

                  if ( is_object ($obj->last_signal) && $obj->last_signal->created_at->diffInMinutes (now()) > $obj->mia_alert_mins )
                  {
                    app (FlowOutputContract::class)->signal ($obj->_id .' IS MIA. LAST SIGNAL WAS '.strtoupper ($obj->last_signal->created_at->diffForHumans()));

                    if ( !$last_alert || ($last_alert && $last_alert->created_at->diffInMinutes (now()) > 15) )
                    {
                      $this->repository->create ([
                        'room_id'         => $obj->room_id ?? null,
                        'component_id'    => $obj->_id,
                        'group'           => strtolower (Str::plural ( (new ReflectionClass ($obj))->getShortName() )),
                        'threshold'       => $obj->mia_alert_mins,
                        'last_signal_id'  => $obj->last_signal->id,
                        'last_seen_at'    => $obj->last_signal->created_at,
                        'last_seen_mins'  => $obj->last_signal->created_at->diffInMinutes (now()),
                        'ignored_at'      => null,
                      ]);
                    }

                  }

                } // method_exists
              } // end active
            }
            catch (Exception $e)
            {
              app (FlowOutputContract::class)->error ($e->getMessage(), $e);
            }

          } // end foreach objs
        } // end foreach repos

        return 0;
    }
}
