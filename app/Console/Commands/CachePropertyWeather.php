<?php

namespace SmartRooms\Console\Commands;

use Illuminate\Console\Command;

use \Cache;
use \Exception;

use SmartRooms\Contracts\System\FlowOutputContract;
use \OpenWeather;

use SmartRooms\Contracts\Repositories\Property\PropertyRepository;

class CachePropertyWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartrooms:cache-weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grabs weather from OpenWeatherMap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        app (FlowOutputContract::class)->signal ($this->signature . ' ('.$this->description.')');

        $properties = app (PropertyRepository::class)->all();

        foreach ($properties AS $property)
        {
            if ( isset ($property->geo['coordinates']) && count ($property->geo['coordinates']) )
            {
              try
              {
                $weather_data = (new OpenWeather)->getCurrentWeatherByCoords ($property->geo['coordinates'][1], $property->geo['coordinates'][0]);
                app (FlowOutputContract::class)->signal ('Getting weather for '.$property->label.' ['.$property->_id.'] --> ['.$property->geo['coordinates'][0].', '.$property->geo['coordinates'][1].']', $weather_data);
                Cache::put ($property->_id.'-weather', $weather_data, now()->addMinutes(30));
              }
              catch (Exception $e)
              {
                app (FlowOutputContract::class)->error ($e->getMessage(), $e);
              }

            }
        }

        return 0;
    }
}
