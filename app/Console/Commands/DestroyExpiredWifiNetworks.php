<?php

namespace SmartRooms\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Log;
use SmartRooms\Contracts\ComfortOS\API\WifiManagementContract;
use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;
use SmartRooms\Events\MikroTik\RouterOS\WifiNetworkAboutToExpire;

use SmartRooms\Contracts\System\FlowOutputContract;

class DestroyExpiredWifiNetworks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartrooms:prune-ssids';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes wifi networks set to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app (FlowOutputContract::class)->signal ($this->signature . ' ('.$this->description.')');

        $networks = app (NetworkRepository::class)->with(['router', 'creator'])->all();

        foreach ($networks as $network) {
            try {

                if ( $network->type == 'virtual' )
                {
                  if ( isset ($network->expire_at) && $network->expire_at->timestamp > time() ) {

                      app (WifiManagementContract::class)
                      ->src ('cron')
                      ->user ($network->creator)
                      ->entity ($network)
                      ->params ([])
                      ->destroy ();
                  }

                  if ( isset ($network->expire_at) && ($network->expire_at->timestamp - time() < 300) ) {
                      event (new WifiNetworkAboutToExpire($network));
                  }
                }

            } catch (Exception $e) {
                app (FlowOutputContract::class)->error ($e->getMessage(), $e);
            }
        }
    }
}
