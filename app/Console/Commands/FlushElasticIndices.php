<?php

namespace SmartRooms\Console\Commands;

use Illuminate\Console\Command;

use SmartRooms\Contracts\Repositories\ComfortOS;
use SmartRooms\Contracts\Repositories\Components\Devices;
use SmartRooms\Contracts\Repositories\Components\Mirror;
use SmartRooms\Contracts\Repositories\Components\OS;
use SmartRooms\Contracts\Repositories\Components\Telecoms;
use SmartRooms\Contracts\Repositories\Components\Wifi;
use SmartRooms\Contracts\Repositories\Operator;
use SmartRooms\Contracts\Repositories\Property;
use SmartRooms\Contracts\Repositories\IDAM;

class FlushElasticIndices extends Command
{
    public $repositories = [

      Mirror\ModuleRepository::class,
      Devices\TVAppRepository::class,

      IDAM\UserRepository::class,

      Operator\OperatorRepository::class,
      Operator\BrandRepository::class,
      Property\PropertyRepository::class,
      Property\BuildingRepository::class,
      Property\RoomRepository::class,

      OS\VPNClientRepository::class,
      OS\VPNServerRepository::class,
      OS\MQTTBrokerRepository::class,
      OS\WSBrokerRepository::class,
      Telecoms\PBXRepository::class,
      Wifi\RouterRepository::class,
      OS\HABRepository::class,
      Mirror\DisplayRepository::class,

      Telecoms\DECTRepository::class,
      Telecoms\ExtensionRepository::class,
      Telecoms\HandsetRepository::class,

      Devices\BulbRepository::class,
      Devices\CameraRepository::class,
      Devices\ChromecastRepository::class,
      Devices\CodeRepository::class,
      Devices\DimmerRepository::class,
      Devices\LEDRepository::class,

      Devices\LockRepository::class,
      Devices\MotorRepository::class,
      Devices\PhysicalSwitchRepository::class,
      Devices\PlugRepository::class,
      Devices\SensorRepository::class,
      Devices\SpeakerRepository::class,
      Devices\STBRepository::class,
      Devices\ThermostatRepository::class,
      Devices\TVRepository::class,
      Wifi\NetworkRepository::class,

      //ComfortOS\OpQueueRepository::class,
      //ComfortOS\HABCommandRepository::class,
      //ComfortOS\HABSignalRepository::class,
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartrooms:flush-indices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Erases data from all Elasticsearch indices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach ($this->repositories as $repository) {
          $this->line('Flushing '.$repository);
          $this->call('scout:flush', ['model' => app($repository)->model()]);
        }

        return 0;
    }
}
