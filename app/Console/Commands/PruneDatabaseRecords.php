<?php

namespace SmartRooms\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use SmartRooms\Contracts\System\FlowOutputContract;

use SmartRooms\Contracts\Repositories\Property\RoomRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\OpQueueRepository;

use Illuminate\Support\Str;

class PruneDatabaseRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartrooms:prune-database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes old records from the database';

    public $repos_to_prune = [
        HABSignalRepository::class,
        HABCommandRepository::class,
        MIAAlertRepository::class,
        OpQueueRepository::class,
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app (FlowOutputContract::class)->signal ($this->signature . ' ('.$this->description.')');

        $all_rooms = app (RoomRepository::class)->all();

        foreach ($all_rooms AS $room)
        {
          foreach ($this->repos_to_prune as $repo)
          {
              $before = app ($repo)->scopeQuery (function($query) use ($room) {
                  return $query->where('room_id', $room->_id);
              })->count();

              if ( $before > 0 )
              {
                app (FlowOutputContract::class)->signal ('Room '.$room->_id.' [retain '.$room->retention.' days] '. Str::afterLast ($repo, '\\') . ': starting count --> '.$before.'. Remove records <= '.now()->subDays($room->retention ?? 3)->format('D dS M Y H:i'));

                app ($repo)->deleteWhere (
                  [
                    'room_id' => $room->_id,
                    ['created_at', '<=', now()->subDays($room->retention ?? 3)],
                  ]
                );

                $after = app ($repo)->scopeQuery (function($query) use ($room) {
                    return $query->where('room_id', $room->_id);
                })->count();

                if (abs ($before - $after) > 0)
                {
                  app (FlowOutputContract::class)->signal ('Room '.$room->id. ' ['.Str::afterLast ($repo, '\\') . '] after pruning --> '.$after.'. Removed '.abs ($before - $after).' records.');
                }
              }

          }
        }

    }
}
