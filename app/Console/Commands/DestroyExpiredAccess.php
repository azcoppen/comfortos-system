<?php

namespace SmartRooms\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Log;

use SmartRooms\Contracts\Repositories\IDAM\AccessRepository;

use SmartRooms\Contracts\System\FlowOutputContract;

class DestroyExpiredAccess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartrooms:prune-access';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes access grants set to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app (FlowOutputContract::class)->signal ($this->signature . ' ('.$this->description.')');

        $accesses = app (AccessRepository::class)->all();

        foreach ($accesses as $access)
        {
            try {

                if ( isset ($access->end_at) && time() > $access->end_at->timestamp )
                {
                    app (AccessRepository::class)->delete ($access->_id);
                }

            }
            catch (Exception $e)
            {
                app (FlowOutputContract::class)->error ($e->getMessage(), $e);
            }
        }
    }
}
