<?php

namespace SmartRooms\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Log;
use MikroTik\RouterOS\Events\DeviceStatusChecked;
use SmartRooms\Contracts\MikroTik\RouterOS\StatusContract;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\StatusRepository;

use SmartRooms\Contracts\System\FlowOutputContract;

class CheckRouterStatuses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartrooms:router-statuses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Queries MikroTik routers for latest info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app (FlowOutputContract::class)->signal ($this->signature . ' ('.$this->description.')');

        $routers = app(RouterRepository::class)->all();
        foreach ($routers as $router) {
            try {
                $check = app(StatusRepository::class)->create([
                  'router_id'  => $router->_id,
                  'data'       => app(StatusContract::class)->check([
                      'host'      => $router->api_url,
                      'port'      => $router->api_port,
                      'username'  => $router->api_user,
                      'password'  => \Crypt::decrypt($router->api_pwd),
                  ]),
                ]);

                event(new DeviceStatusChecked($check));

                return; // only do first record
            } catch (Exception $e) {
                Log::error($e);
            }
        }
    }
}
