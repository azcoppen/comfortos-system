<?php

namespace SmartRooms\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // * * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
        // watch -n 1 /some/command --> run every 1 second

        $schedule->command('smartrooms:prune-access')->everyMinute();
        $schedule->command('smartrooms:prune-ssids')->everyMinute();
        $schedule->command('smartrooms:mia')->everyFiveMinutes();
        $schedule->command('smartrooms:cache-weather')->everyFifteenMinutes();
        $schedule->command('smartrooms:prune-database')->hourly();

        /*
        ->environments(['staging', 'production']);
        ->pingBefore($url)
        ->thenPing($url)
        ->between('7:00', '22:00');
        ->unlessBetween('23:00', '4:00');
        ->when(function () {
            return true;
        })
        ->skip(function () {
            return true;
        })
        ->before(function () {
            // Task is about to start...
        })
        ->after(function () {
            // Task is complete...
        })
        ->onSuccess(function () {
            // The task succeeded...
        })
        ->onFailure(function () {
            // The task failed...
        })
        ->pingOnSuccess($successUrl)
        ->pingOnFailure($failureUrl)
        ->sendOutputTo($filePath)
        ->emailOutputTo('foo@example.com')
        ->emailOutputOnFailure('foo@example.com');
        */
    }

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    /*
    protected function scheduleTimezone()
    {
       return 'America/Chicago';
    }
    */

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
