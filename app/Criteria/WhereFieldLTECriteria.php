<?php

namespace SmartRooms\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StatusCriteria.
 */
class WhereFieldLTECriteria implements CriteriaInterface
{
    private $field;
    private $value;

    public function __construct (string $field = 'created_at', $value )
    {
        $this->field  = $field;
        $this->value  = $value;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->field, '<=', $this->value);
    }
}
