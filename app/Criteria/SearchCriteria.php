<?php

namespace SmartRooms\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StatusCriteria.
 */
class SearchCriteria implements CriteriaInterface
{
    private $query;

    public function __construct(string $query = '')
    {
        $this->query = $query;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model::search('*'.trim(preg_replace('/[^A-Za-z0-9 ]/', ' ', $this->query)).'*');
    }
}
