<?php

namespace SmartRooms\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StatusCriteria.
 */
class WhereFieldBetweenCriteria implements CriteriaInterface
{
    private $field;
    private $values;

    public function __construct (string $field = 'created_at', array $values )
    {
        $this->field  = $field;
        $this->values = $values;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereBetween($this->field, $this->values);
    }
}
