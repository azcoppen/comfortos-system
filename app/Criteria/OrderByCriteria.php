<?php

namespace SmartRooms\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StatusCriteria.
 */
class OrderByCriteria implements CriteriaInterface
{
    private $field;
    private $dir;

    public function __construct(string $field = 'created_at', string $dir = 'DESC')
    {
        $this->field = $field;
        $this->dir = $dir;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy($this->field, $this->dir);
    }
}
