<?php

namespace SmartRooms\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StatusCriteria.
 */
class NearCriteria implements CriteriaInterface
{
    private $field;
    private $lat;
    private $lon;
    private $meters;

    public function __construct(string $field = 'location', float $lat = 0, float $lon = 0, int $meters = 10000)
    {
        $this->field = $field;
        $this->lat = $lat;
        $this->lon = $lon;
        $this->meters = $meters;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where ($this->field, 'near', [
            '$geometry' => [
                'type' => 'Point',
                'coordinates' => [
                    $this->lon, // longitude
                    $this->lat, // latitude
                ],
            ],
            //'spherical' => true,
            //'distanceField' => "distance",
            '$maxDistance' => $this->meters,
        ]);
    }
}
