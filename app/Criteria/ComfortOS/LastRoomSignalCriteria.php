<?php

namespace SmartRooms\Criteria\ComfortOS;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StatusCriteria.
 */
class LastRoomSignalCriteria implements CriteriaInterface
{
    private $room;
    private $attribute;

    public function __construct ($room, string $attribute)
    {
        $this->room = $room;
        $this->attribute = $attribute;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply ($model, RepositoryInterface $repository)
    {
        return $model
          ->orderBy ('received_at', 'DESC')
          ->where ('room_id', $this->room->_id)
          ->where ('attribute', $this->attribute)
          ->take (1);
    }
}
