<?php

namespace SmartRooms\Criteria\ComfortOS;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StatusCriteria.
 */
class LastSignalCriteria implements CriteriaInterface
{
    private $component;
    private $attribute;

    public function __construct ($component, string $attribute)
    {
        $this->component = $component;
        $this->attribute = $attribute;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply ($model, RepositoryInterface $repository)
    {
        return $model
          ->orderBy ('received_at', 'DESC')
          ->where ('component_id', $this->component->_id)
          ->where ('attribute', $this->attribute)
          ->take (1);
    }
}
