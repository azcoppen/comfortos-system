<?php

namespace SmartRooms\Criteria\ComfortOS;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use SmartRooms\Models\IDAM\User;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

/**
 * Class PropertiesAvailableToUserCriteria.
 */
class OperatorsAvailableToUser implements CriteriaInterface
{
    private $user;

    public function __construct (User $user)
    {
        $this->user = $user;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( $this->user->hasRole ('developer') || $this->user->hasRole ('superuser') )
        {
          return $model;
        }

        return $model->whereIn ('user_ids', [$this->user->_id]);
    }
}
