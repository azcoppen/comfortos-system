<?php

namespace SmartRooms\Criteria\ComfortOS;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use SmartRooms\Models\IDAM\User;

/**
 * Class RoomsAvailableToUserCriteria.
 */
class NotProvisionedToAnyRoom implements CriteriaInterface
{
    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('room_id', 'exists', false)
        ->orWhere ('room_id', null)
        ->orderBy('created_at', 'DESC');
    }
}
