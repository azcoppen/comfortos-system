<?php

namespace SmartRooms\Criteria\ComfortOS;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;
use SmartRooms\Models\IDAM\User;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

/**
 * Class RoomsAvailableToUserCriteria.
 */
class WifiNetworksAvailableToUserCriteria implements CriteriaInterface
{
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('room_id', app (UserConstraintManagementContract::class)->target ($this->user)->rooms())
          ->where ('type', 'virtual')
          ->where('expire_at', '>', now());
    }
}
