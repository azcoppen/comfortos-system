<?php

namespace SmartRooms\Criteria\ComfortOS;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use SmartRooms\Models\IDAM\User;

use \Carbon\Carbon;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

/**
 * Class RoomsAvailableToUserCriteria.
 */
class DevicesAvailableToUserCriteria implements CriteriaInterface
{
    private $user;
    private $model;

    public function __construct($user, string $model)
    {
        $this->user = $user;
        $this->model = $model;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('room_id', app (UserConstraintManagementContract::class)->target ($this->user)->rooms());
    }
}
