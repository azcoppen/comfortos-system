<?php

namespace SmartRooms\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

use SmartRooms\Contracts\Repositories\IDAM\RoleRepository;

/**
 * Class StatusCriteria.
 */
class HasRolesCriteria implements CriteriaInterface
{
    private $roles;

    public function __construct(array $roles = [])
    {
        $this->roles = app (RoleRepository::class)
          ->scopeQuery (function($query) use ($roles) {
              return $query->whereIn ('name', $roles);
          })->all();
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn ('role_ids', $this->roles->pluck ('_id'));
    }
}
