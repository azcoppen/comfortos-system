<?php

namespace SmartRooms\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StatusCriteria.
 */
class StatusCriteria implements CriteriaInterface
{
    private $whitelist;
    private $status;

    public function __construct(string $status, array $whitelist = [])
    {
        $this->whitelist = $whitelist;
        $this->status = $status;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (! in_array($this->status, $this->whitelist)) {
            return $model;
        }

        return $model->currentStatus([$this->status]);
    }
}
