<?php

namespace SmartRooms\Services\MikroTik\RouterOS;

use Illuminate\Support\Str;
use MikroTik\RouterOS\Commands;
use ReflectionClass;
use SmartRooms\Contracts\MikroTik\RouterOS\StatusContract;

class Status implements StatusContract
{
    private $config;
    private $result = [];

    private $static_commands = [
        'ip' => [
            Commands\IP\Address::class,
            Commands\IP\DNS::class,
            Commands\IP\Proxy::class,
            Commands\IP\Route::class,
            Commands\IP\Service::class,
            Commands\IP\DHCP\Lease::class,
        ],
        'network' => [
            Commands\Network\Interfaces::class,
        ],
        'system' => [
            Commands\System\Clock::class,
            Commands\System\History::class,
            Commands\System\Identity::class,
            Commands\System\License::class,
            Commands\System\Package::class,
            Commands\System\Resource::class,
            Commands\System\Routerboard::class,
        ],
    ];

    private $collection_commands = [
        'wireless' => [
            Commands\Wireless\Network::class,
            Commands\Wireless\SecurityProfile::class,
        ],
    ];

    // MongoDB does not allow column names which contain a dot
    private function rename_id_column(array $dataset)
    {
        $rebuilt = [];
        foreach ($dataset as $key => $value) {
            if ($key === '.id') {
                $key = 'id';
            }

            if (is_array($value)) {
                $value = $this->rename_id_column($value);
            }

            $rebuilt[$key] = $value;
        }

        return $rebuilt;
    }

    public function check(array $config)
    {
        $this->config = $config;

        foreach ($this->static_commands as $group => $cmd_list) {
            $this->result[$group] = [];
            foreach ($cmd_list as $query_obj) {
                $section = strtolower((new ReflectionClass($query_obj))->getShortName());
                $this->result[$group][$section] = $this->rename_id_column(
                    app($query_obj)->config($this->config)->read()->response()
                );
            }
        }

        foreach ($this->collection_commands as $group => $cmd_list) {
            $this->result[$group] = [];
            foreach ($cmd_list as $query_obj) {
                $section = Str::plural(strtolower((new ReflectionClass($query_obj))->getShortName()));
                $this->result[$group][$section] = $this->rename_id_column(
                    app($query_obj)->config($this->config)->all()->response()
                );
            }
        }

        return $this->result;
    }
}
