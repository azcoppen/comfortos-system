<?php

namespace SmartRooms\Services\MikroTik\RouterOS;

use SmartRooms\Contracts\System\MikroTikCLIProcessContract;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use \Exception;

class CLIExecutor implements MikroTikCLIProcessContract
{
  public $bin;
  public $command;
  public $process;
  public $flags = [];
  public $args = [];
  public $input;
  public $output;

  public function __construct ()
  {
    if (! file_exists (base_path ('bin/mikrotik-cli')) )
    {
      throw new Exception (base_path ('bin/mikrotik-cli') . " doesn't exist.");
    }

    if (! is_executable (base_path ('bin/mikrotik-cli')) )
    {
      throw new Exception (base_path ('bin/mikrotik-cli') . " is not executable.");
    }

    $this->bin = base_path ('bin/mikrotik-cli');
  }

  public function command (string $command) : self
  {
    $this->command = $command;

    return $this;
  }

  public function args (array $args = []) : self
  {
    $this->args = $args;

    return $this;
  }

  public function mqtt (bool $mqtt = true) : self
  {
    array_push ($this->flags, '--mqtt='.($mqtt !== FALSE ? 'true' : 'false'));

    return $this;
  }

  public function input () : string
  {
    if (! $this->input )
    {
      $this->input = $this->bin . ' ' .$this->command. ' '. implode (' ', $this->args). ' '. implode (' ', $this->flags);
    }

    return $this->input;
  }

  public function output () : string
  {
    return $this->output;
  }

  public function connection (string $host, string $user, string $pass) : self
  {
    if (! filter_var($host, FILTER_VALIDATE_IP) )
    {
      throw new Exception ($host." is not a valid IP address.");
    }

    array_push ($this->flags, '--h='.$host);
    array_push ($this->flags, '--u='.$user);
    array_push ($this->flags, '--p='.$pass);

    return $this;
  }

  public function execute () : self
  {
    $this->input = $this->bin . ' ' .$this->command. ' '. implode (' ', $this->args). ' '. implode (' ', $this->flags);
    $this->process = new Process (array_merge([$this->bin, $this->command], $this->args, $this->flags));
    $this->process->run();

    if (! $this->process->isSuccessful() ) {
        throw new ProcessFailedException ($this->process);
    }

    $this->output = $this->process->getOutput();

    return $this;
  }

}
