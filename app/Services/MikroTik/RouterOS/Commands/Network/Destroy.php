<?php

namespace SmartRooms\Services\MikroTik\RouterOS\Commands\Network;

use SmartRooms\Contracts\MikroTik\RouterOS\CommandContract;

class Destroy extends Command implements CommandContract
{
  public $cli = 'remove';
  public $item = 'wifi-deletes';
  public $arguments   = [];

  public function __construct (string $ssid)
  {
      $this->arguments = [$ssid];
  }

}
