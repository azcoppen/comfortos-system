<?php

namespace SmartRooms\Services\MikroTik\RouterOS\Commands\Network;

use SmartRooms\Contracts\MikroTik\RouterOS\CommandContract;

class Launch extends Command implements CommandContract
{
    public $cli = 'create';
    public $item = 'wifi-launches';
    public $arguments   = [];

    public function __construct (string $ssid, string $password)
    {
        $this->arguments = [$ssid . ' '. $password];
    }
}
