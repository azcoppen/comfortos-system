<?php

namespace SmartRooms\Services\MikroTik\RouterOS\Commands\Network;

use SmartRooms\Contracts\MikroTik\RouterOS\CommandContract;

class ChangePassword extends Command implements CommandContract
{
  public $cli = 'password';
  public $item = 'wifi-pwds';
  public $arguments   = [];

  public function __construct (string $ssid, string $password)
  {
      $this->arguments = [$ssid . ' '. $password];
  }
}
