<?php

namespace SmartRooms\Services\MikroTik\RouterOS\Commands\Network;

use SmartRooms\Contracts\MikroTik\RouterOS\CommandContract;

class ChangeSSID extends Command implements CommandContract
{
    public $cli = 'rename';
    public $item = 'wifi-renames';
    public $arguments   = [];

    public function __construct (string $old_ssid, string $new_ssid)
    {
        $this->arguments = [$old_ssid . ' '. $new_ssid];
    }
}
