<?php

namespace SmartRooms\Services\Metrics;

use DB;
use SmartRooms\Contracts\Metrics\AggregationContract;

class Aggregator implements AggregationContract
{
    protected $slice_size = 10;

    protected $changes_collection = 'st_changes_agg';

    protected $readings_collection = 'st_readings_agg';

    protected $combined_collection = 'st_combined_agg';

    protected $latest_collection = 'st_last_agg';

    public function changes() : array
    {
        return [
            [
                '$match' => [
                    'capability' => [
                        '$not' => [
                            '$eq' => 'healthCheck',
                        ],
                    ],
                ],
            ],
            [
                '$sort' => [
                    'updated_at' => -1,
                ],
            ],
            [
                '$group' => [
                    '_id' => [
                        'device_id' => '$device_id',
                        'capability' => '$capability',
                        'attribute' => '$attribute',
                    ],
                    'count'         => ['$sum' => 1],
                    'value'         => ['$first' => '$value'],
                    'updated_at'    => ['$first' => '$updated_at'],
                    'latest'        => ['$first' => '$$ROOT'],
                    'entries'       => ['$push' => '$$ROOT'],
                ],
            ],
            [
                '$project' => [
                    '_id'           => 0,
                    'device_id'     => '$_id.device_id',
                    'capability'    => '$_id.capability',
                    'attribute'     => '$_id.attribute',
                    'total'         => '$count',
                    'value'         => '$value',
                    'updated_at'    => '$updated_at',
                    'latest'        => '$latest',
                    'entries'       => [
                        '$slice' => ['$entries', $this->slice_size],
                    ],
                ],
            ],
            [
                '$sort' => [
                    'updated_at' => -1,
                ],
            ],
            [
                '$out' => $this->changes_collection,
            ],
        ];
    }

    public function readings() : array
    {
        return [
            [
                '$match' => [
                    'capability' => [
                        '$not' => [
                            '$eq' => 'healthCheck',
                        ],
                    ],
                ],
            ],
            [
                '$sort' => [
                    'updated_at' => -1,
                ],
            ],
            [
                '$group' => [
                    '_id' => [
                        'device_id' => '$device_id',
                        'capability' => '$capability',
                        'attribute' => '$attribute',
                    ],
                    'count'         => ['$sum' => 1],
                    'value'         => ['$first' => '$value'],
                    'updated_at'    => ['$first' => '$updated_at'],
                    'latest'        => ['$first' => '$$ROOT'],
                    'entries'       => ['$push' => '$$ROOT'],
                ],
            ],
            [
                '$project' => [
                    '_id'           => 0,
                    'device_id'     => '$_id.device_id',
                    'capability'    => '$_id.capability',
                    'attribute'     => '$_id.attribute',
                    'total'         => '$count',
                    'value'         => '$value',
                    'updated_at'    => '$updated_at',
                    'latest'        => '$latest',
                    'entries'       => [
                        '$slice' => ['$entries', $this->slice_size],
                    ],
                ],
            ],
            [
                '$sort' => [
                    'updated_at' => -1,
                ],
            ],
            [
                '$out' => $this->readings_collection,
            ],
        ];
    }

    public function combined() : array
    {
        return [
            [
                '$project' => [
                    '_id' => [
                        '$toString' => '$_id',
                    ],
                ],
            ],
            [
                '$lookup' => [
                    'from'          => $this->changes_collection,
                    'localField'    => '_id',
                    'foreignField'  => 'device_id',
                    'as'            => 'changes',
                ],
            ],
            [
                '$lookup' => [
                    'from'          => $this->readings_collection,
                    'localField'    => '_id',
                    'foreignField'  => 'device_id',
                    'as'            => 'readings',
                ],
            ],
            [
                '$project' => [
                    '_id' => '$_id',
                    'measurements' => [
                        '$concatArrays' => ['$changes', '$readings'],
                    ],
                ],
            ],
            [
                '$unwind' => '$measurements',
            ],
            [
                '$sort' => [
                    '_id' => 1,
                    'measurements.updated_at' => -1,
                ],
            ],
            [
                '$group' => [
                    '_id' => '$_id',
                    'measurements' => [
                        '$push' => '$measurements',
                    ],
                ],
            ],
            [
                '$sort' => [
                    '_id' => 1,
                ],
            ],
            [
                '$unwind' => '$measurements',
            ],
            [
                '$group' => [
                    '_id' => [
                        'device_id'     => '$_id',
                        'capability'    => '$measurements.capability',
                        'attribute'     => '$measurements.attribute',
                    ],
                    'count'         => ['$sum' => 1],
                    'value'         => ['$first' => '$measurements.value'],
                    'updated_at'    => ['$first' => '$measurements.updated_at'],
                    'latest'        => ['$first' => '$measurements'],
                    'entries'       => ['$push' => '$measurements'],
                ],
            ],
            [
                '$project' => [
                    '_id'           => 0,
                    'device_id'     => '$_id.device_id',
                    'capability'    => '$_id.capability',
                    'attribute'     => '$_id.attribute',
                    'value'         => '$value',
                    'updated_at'    => '$updated_at',
                    'latest'        => '$latest',
                ],
            ],
            [
                '$sort' => [
                    'updated_at' => -1,
                ],
            ],
            [
                '$out' => $this->combined_collection,
            ],
        ];
    }

    public function latest() : array
    {
        return [
            [
                '$group' => [
                    '_id'       => '$device_id',
                    'latest'    => ['$first' => '$latest'],
                ],
            ],
            [
                '$project' => [
                    '_id'           => 0,
                    'device_id'     => '$latest.device_id',
                    'capability'    => '$latest.capability',
                    'attribute'     => '$latest.attribute',
                    'value'         => '$latest.value',
                    'updated_at'    => '$latest.updated_at',
                ],
            ],
            [
                '$sort' => [
                    'device_id' => 1,
                ],
            ],
            [
                '$out' => $this->latest_collection,
            ],
        ];
    }

    public function execute() : array
    {
        return [
            DB::collection('st_changes')->raw()->aggregate($this->changes()),
            DB::collection('st_readings')->raw()->aggregate($this->readings()),
            DB::collection('st_devices')->raw()->aggregate($this->combined()),
            DB::collection($this->combined_collection)->raw()->aggregate($this->latest()),
        ];
    }
}
