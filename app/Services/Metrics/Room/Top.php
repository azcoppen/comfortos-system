<?php

namespace SmartRooms\Services\Metrics\Room;

use Illuminate\Support\Collection;
use SmartRooms\Contracts\Metrics\Device\DeviceTopContract;
use SmartRooms\Contracts\Metrics\Room\RoomTopContract;
use SmartRooms\Models\Property\Room;

class Top implements RoomTopContract
{
    public $data = [];

    public function room(Room $room) : self
    {
        $this->room = $room;

        return $this;
    }

    public function data() : Collection
    {
        return $this->data;
    }

    public function scan() : self
    {
        $this->data = collect($this->data);

        foreach ($this->room->component_relations as $relation) {
            if (is_object($this->room->{$relation})) {
                if (! $this->data->has($relation)) {
                    $this->data->put($relation, collect([]));
                }

                foreach ($this->room->{$relation} as $device) {
                    $this->data->get($relation)->put($device->_id,
                        app(DeviceTopContract::class)->entity($device)->slice()->data()
                    );
                }
            }
        }

        return $this;
    }
}
