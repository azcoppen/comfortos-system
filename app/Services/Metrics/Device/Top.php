<?php

namespace SmartRooms\Services\Metrics\Device;

use Illuminate\Support\Collection;
use SmartRooms\Contracts\Metrics\Device\DeviceTopContract;

class Top implements DeviceTopContract
{
    public $entity;
    public $grouped;
    public $data;

    public function raw() : Collection
    {
        return $this->grouped;
    }

    public function data() : Collection
    {
        return $this->data;
    }

    public function entity($entity) : self
    {
        $this->entity = $entity;

        if (! is_object($this->entity->changes ?? null) && ! is_object($this->entity->readings ?? null)) {
            $this->entity->load($this->entity->full_component_eager_load);
        }

        return $this;
    }

    public function build() : self
    {
        $this->grouped = $this->entity->readings->merge($this->entity->changes ?? collect([]))
            ->groupBy('capability')
            ->map(function ($i, $k) {
                return $i->groupBy('attribute')->map(function ($item, $key) {
                    return $item->sortBy(function ($datum) {
                        return $datum->created_at->timestamp;
                    }, SORT_REGULAR, true);
                });
            });

        return $this;
    }

    public function slice() : self
    {
        if (! $this->grouped) {
            $this->build();
        }

        $this->data = $this->grouped->each(function ($item, $key) {
            return $item->transform(function ($i, $k) {
                $original = $i->first();

                return [
                    'unit'          => $original->unit,
                    'value'         => $original->value,
                    'data'          => $original->data,
                    'created_at'    => $original->created_at,
                ];
            });
        })
        ->except(config('smartrooms-demo.v1.ignore.capabilities'));

        return $this;
    }
}
