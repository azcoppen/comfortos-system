<?php

namespace SmartRooms\Services\ComfortOS\API;

use SmartRooms\Contracts\ComfortOS\API\PlugManagementContract;
use SmartRooms\Jobs\ComfortOS\PlugOperation;

use SmartRooms\Services\OpenHAB\API\Commands\Power\Off;
use SmartRooms\Services\OpenHAB\API\Commands\Power\On;
use SmartRooms\Services\OpenHAB\API\Commands\Level\Level;

class PlugManager extends APIManager implements PlugManagementContract
{
    public $job = PlugOperation::class;
    public $domain = 'openhab';

    public function update()
    {
        $this->payload['commands'] = collect([]);

        if ($this->params->has('power')) {
            switch ($this->params->get('power')) {
                case 'on':
                    $this->payload['commands']->push(
                        new On
                    );
                break;

                case 'off':
                    $this->payload['commands']->push(
                        new Off
                    );
                break;
            }
        }

        if ($this->params->has('level')) {
            $this->payload['commands']->push(
                new Level($this->params->get('level'))
            );
        }

        $this->dispatch();
    }
}
