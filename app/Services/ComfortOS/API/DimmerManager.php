<?php

namespace SmartRooms\Services\ComfortOS\API;

use SmartRooms\Contracts\ComfortOS\API\DimmerManagementContract;
use SmartRooms\Jobs\ComfortOS\DimmerOperation;

use SmartRooms\Services\OpenHAB\API\Commands\Level\Level;
use SmartRooms\Services\OpenHAB\API\Commands\Level\State;

use SmartRooms\Services\OpenHAB\API\Commands\Power\Off;
use SmartRooms\Services\OpenHAB\API\Commands\Power\On;

class DimmerManager extends APIManager implements DimmerManagementContract
{
    public $job = DimmerOperation::class;
    public $domain = 'openhab';

    public function update()
    {
        $this->payload['commands'] = collect([]);

        if ($this->params->has('power')) {
            switch ($this->params->get('power')) {
                case 'on':
                    $this->payload['commands']->push(
                        new On
                    );
                break;

                case 'off':
                    $this->payload['commands']->push(
                        new Off
                    );
                break;
            }
        }

        if ($this->params->has('level')) {
            $this->payload['commands']->push(
                new Level($this->params->get('level'))
            );
        }

        if ($this->params->has('state')) {
            $this->payload['commands']->push(
                new State($this->params->get('state'))
            );
        }

        $this->dispatch();
    }
}
