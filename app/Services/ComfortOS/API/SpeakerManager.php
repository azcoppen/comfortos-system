<?php

namespace SmartRooms\Services\ComfortOS\API;

use SmartRooms\Contracts\ComfortOS\API\SpeakerManagementContract;
use SmartRooms\Jobs\ComfortOS\SpeakerOperation;

use SmartRooms\Services\OpenHAB\API\Commands\Power\Off;
use SmartRooms\Services\OpenHAB\API\Commands\Power\On;

use SmartRooms\Services\OpenHAB\API\Commands\Audio\Mode;
use SmartRooms\Services\OpenHAB\API\Commands\Audio\Mute;
use SmartRooms\Services\OpenHAB\API\Commands\Audio\Preset;
use SmartRooms\Services\OpenHAB\API\Commands\Audio\Unmute;
use SmartRooms\Services\OpenHAB\API\Commands\Audio\Volume;

use SmartRooms\Services\OpenHAB\API\Commands\Playback\Next;
use SmartRooms\Services\OpenHAB\API\Commands\Playback\Pause;
use SmartRooms\Services\OpenHAB\API\Commands\Playback\Play;
use SmartRooms\Services\OpenHAB\API\Commands\Playback\Previous;
use SmartRooms\Services\OpenHAB\API\Commands\Playback\Stop;

class SpeakerManager extends APIManager implements SpeakerManagementContract
{
    public $job = SpeakerOperation::class;
    public $domain = 'openhab';

    public function update()
    {
        $this->payload['commands'] = collect([]);

        if ($this->params->has('power')) {
            switch ($this->params->get('power')) {
                case 'on':
                    $this->payload['commands']->push(
                        new On
                    );
                break;

                case 'off':
                    $this->payload['commands']->push(
                        new Off
                    );
                break;
            }
        }

        if ($this->params->has('mute')) {
            switch ($this->params->get('mute')) {
                case 'on':
                    $this->payload['commands']->push(
                        new Mute
                    );
                break;

                case 'off':
                    $this->payload['commands']->push(
                        new Unmute
                    );
                break;
            }
        }

        if ($this->params->has('mode')) {
            $this->payload['commands']->push(
                new Mode($this->params->get('mode'))
            );
        }

        if ($this->params->has('volume')) {
            $this->payload['commands']->push(
                new Volume($this->params->get('volume'))
            );
        }

        if ($this->params->has('preset')) {
            $this->payload['commands']->push(
                new Preset($this->params->get('preset'))
            );
        }

        if ($this->params->has('controls')) {
            switch ($this->params->get('controls')) {
                case 'next':
                    $this->payload['commands']->push(
                        new Next
                    );
                break;

                case 'pause':
                    $this->payload['commands']->push(
                        new Pause
                    );
                break;

                case 'play':
                    $this->payload['commands']->push(
                        new Play
                    );
                break;

                case 'previous':
                    $this->payload['commands']->push(
                        new Previous
                    );
                break;

                case 'stop':
                    $this->payload['commands']->push(
                        new Stop
                    );
                break;
            }
        }

        $this->dispatch();
    }
}
