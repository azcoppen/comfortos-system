<?php

namespace SmartRooms\Services\ComfortOS\API;

use Carbon\Carbon;
use Illuminate\Support\Str;

use SmartRooms\Contracts\ComfortOS\API\WifiManagementContract;
use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

use SmartRooms\Jobs\ComfortOS\WifiOperation;
use SmartRooms\Models\Components\Wifi\Network;

use SmartRooms\Services\MikroTik\RouterOS\Commands\Network\ChangePassword;
use SmartRooms\Services\MikroTik\RouterOS\Commands\Network\ChangeSSID;
use SmartRooms\Services\MikroTik\RouterOS\Commands\Network\Destroy;
use SmartRooms\Services\MikroTik\RouterOS\Commands\Network\Launch;

class WifiManager extends APIManager implements WifiManagementContract
{
    public $job = WifiOperation::class;
    public $master_interface = 'wlan1';
    public $profile_postfix = '-sec-profile';
    public $domain = 'mikrotik';

    public function store() : Network
    {
        $this->operation = __FUNCTION__;

        $room = app (RoomRepository::class)->find($this->params->get('room'));

        if (! $room )
        {
          throw new \Exception ("Unable to detect room.");
        }

        // we need to there are no other SSIOs with the same name
        $existing = app (NetworkRepository::class)->findWhere ([
          'room_id'   => $room->_id,
          'ssid'      => $this->params->get ('ssid'),
          ['expire_at', '>', now()],
        ]);

        if ( $existing->count () > 0 )
        {
          throw new \Exception ("A network with the SSID ".$this->params->get ('ssid').' already exists.');
        }

        $router = $room->routers()->first();

        if (! $router )
        {
          throw new \Exception ("Unable to detect router.");
        }

        $this->entity = app (NetworkRepository::class)->create ([
            'room_id'           => $room->_id,
            'router_id'         => $router->_id,
            'label'             => $this->params->get('label', 'Virtual Wifi Network'),
            'type'              => 'virtual',
            'interface'         => $this->master_interface,
            'security_profile'  => null,
            'profile_index'     => null,
            'network_index'     => null,
            'ssid'              => $this->params->get('ssid'),
            'password'          => \Crypt::encrypt($this->params->get('password')),
            'launch_at'         => $this->params->has('launch_at') ? Carbon::parse($this->params->get('launch_at'), $this->user->timezone)->timezone('UTC') : now()->addDays(1),
            'expire_at'         => $this->params->has('expire_at') ? Carbon::parse($this->params->get('expire_at'), $this->user->timezone)->timezone('UTC'): now()->addDays(1),
            'confirmed_at'      => null,
            'active_at'         => null,
            'provisioned_at'    => now(),
            'provisioned_by'    => $this->user->_id ?? null,
            'created_by'        => $this->user->_id ?? null,
        ]);

        $this->payload['entity'] = $router;

        $this->payload['commands'] = collect([]);

        $this->payload['commands']->push(
            new Launch ($this->params->get('ssid'), $this->params->get('password'))
        );

        $this->dispatch();

        return $this->entity;
    }

    public function update()
    {
        $this->payload['commands'] = collect([]);

        if ($this->params->has('ssid')) {
            $this->payload['commands']->push(
                new ChangeSSID ($this->entity->ssid, $this->params->get('ssid'))
            );
        }

        if ($this->params->has('password')) {

            $this->payload['commands']->push(
                new ChangePassword ($this->entity->ssid, $this->params->get('password'))
            );
        }

        if ($this->params->has('expire_at')) {

            if (Carbon::parse($this->params->get('expire_at'))->timestamp < time()) {
                $this->payload['commands']->push(
                    new Destroy ($this->entity->ssid)
                );
            }
        }

        if ( $this->payload['commands']->count() > 0 )
        {
          $this->dispatch();
        }

    }

    public function destroy()
    {
        $this->operation = __FUNCTION__;
        $this->payload['commands'] = collect([]);

        $this->payload['commands']->push(
            new Destroy ($this->entity->ssid)
        );

        $this->dispatch();

        app (NetworkRepository::class)->delete ($this->entity->_id);
    }
}
