<?php

namespace SmartRooms\Services\ComfortOS\API;

use SmartRooms\Contracts\ComfortOS\API\BulbManagementContract;
use SmartRooms\Jobs\ComfortOS\BulbOperation;

use SmartRooms\Services\OpenHAB\API\Commands\Color\Brightness;
use SmartRooms\Services\OpenHAB\API\Commands\Color\HSB;
use SmartRooms\Services\OpenHAB\API\Commands\Color\Temperature;

use SmartRooms\Services\OpenHAB\API\Commands\Power\Off;
use SmartRooms\Services\OpenHAB\API\Commands\Power\On;

class BulbManager extends APIManager implements BulbManagementContract
{
    public $job = BulbOperation::class;
    public $domain = 'openhab';

    public function update()
    {
        $this->payload['commands'] = collect([]);

        if ($this->params->has('power')) {
            switch ($this->params->get('power')) {
                case 'on':

                    $this->payload['commands']->push(
                        new On
                    );

                    $this->payload['commands']->push(
                      new HSB ([rand (1, 360), 100, 100])
                    );

                break;

                case 'off':

                    $this->payload['commands']->push(
                        new Off
                    );

                    $this->payload['commands']->push(
                      new HSB ([0, 0, 0]),
                    );

                    $this->payload['commands']->push(
                      new Temperature (0),
                    );

                break;
            }
        }

        if ($this->params->has('brightness')) {
            $this->payload['commands']->push(
                new Brightness ($this->params->get('brightness'))
            );
        }

        if ($this->params->has('hsb')) {
            $this->payload['commands']->push(
                new HSB (
                  is_string ($this->params->get('hsb')) ? explode (',', $this->params->get('hsb')) : $this->params->get('hsb')
                )
            );
        }

        if ($this->params->has('temperature')) {
            $this->payload['commands']->push(
                new Temperature($this->params->get('temperature'))
            );
        }

        $this->dispatch();
    }
}
