<?php

namespace SmartRooms\Services\ComfortOS\API;

use Illuminate\Support\Arr;
use SmartRooms\Contracts\ComfortOS\API\MotorManagementContract;
use SmartRooms\Jobs\ComfortOS\MotorOperation;

use SmartRooms\Services\OpenHAB\API\Commands\Power\Off;
use SmartRooms\Services\OpenHAB\API\Commands\Power\On;

use SmartRooms\Services\OpenHAB\API\Commands\Motor\Down;
use SmartRooms\Services\OpenHAB\API\Commands\Motor\Left;
use SmartRooms\Services\OpenHAB\API\Commands\Motor\Mode;
use SmartRooms\Services\OpenHAB\API\Commands\Motor\Position;
use SmartRooms\Services\OpenHAB\API\Commands\Motor\Right;
use SmartRooms\Services\OpenHAB\API\Commands\Motor\Speed;
use SmartRooms\Services\OpenHAB\API\Commands\Motor\Up;

class MotorManager extends APIManager implements MotorManagementContract
{
    public $job = MotorOperation::class;
    public $domain = 'openhab';

    public function update()
    {
      $this->payload['commands'] = collect([]);

      if ($this->params->has('power')) {
          switch ($this->params->get('power')) {
              case 'on':
                  $this->payload['commands']->push(
                      new On
                  );
              break;

              case 'off':
                  $this->payload['commands']->push(
                      new Off
                  );
              break;
          }
      }

      if ($this->params->has('dir')) {
          switch ($this->params->get('dir')) {

              case 'up':
                  $this->payload['commands']->push(
                      new Up
                  );
              break;

              case 'down':
                  $this->payload['commands']->push(
                      new Down
                  );
              break;

              case 'left':
                  $this->payload['commands']->push(
                      new Left
                  );
              break;

              case 'right':
                  $this->payload['commands']->push(
                      new Right
                  );
              break;

              case 'stop':
                  $this->payload['commands']->push(
                      new Speed(0)
                  );
              break;

          }
      }

      if ($this->params->has('position')) {
          $this->payload['commands']->push(
              new Position($this->params->get('position'))
          );
      }

      if ($this->params->has('speed')) {
          $this->payload['commands']->push(
              new Speed($this->params->get('speed'))
          );
      }

      $this->dispatch();
    }

}
