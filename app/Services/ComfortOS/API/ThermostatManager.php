<?php

namespace SmartRooms\Services\ComfortOS\API;

use SmartRooms\Contracts\ComfortOS\API\ThermostatManagementContract;
use SmartRooms\Jobs\ComfortOS\ThermostatOperation;

use SmartRooms\Services\OpenHAB\API\Commands\Power\Off;
use SmartRooms\Services\OpenHAB\API\Commands\Power\On;

use SmartRooms\Services\OpenHAB\API\Commands\Thermostat\CoolingPoint;
use SmartRooms\Services\OpenHAB\API\Commands\Thermostat\HeatingPoint;
use SmartRooms\Services\OpenHAB\API\Commands\Thermostat\FanMode;
use SmartRooms\Services\OpenHAB\API\Commands\Thermostat\FanState;
use SmartRooms\Services\OpenHAB\API\Commands\Thermostat\Mode;
use SmartRooms\Services\OpenHAB\API\Commands\Thermostat\OpState;

class ThermostatManager extends APIManager implements ThermostatManagementContract
{
    public $job = ThermostatOperation::class;
    public $domain = 'openhab';

    public function update()
    {
        $this->payload['commands'] = collect([]);

        if ($this->params->has('power')) {
            switch ($this->params->get('power')) {
                case 'on':
                    $this->payload['commands']->push(
                        new On
                    );
                break;

                case 'off':
                    $this->payload['commands']->push(
                        new Off
                    );
                break;
            }
        }

        if ($this->params->has('opstate')) {
            $this->payload['commands']->push(
                new OpState($this->params->get('opstate'))
            );
        }

        if ($this->params->has('cooling_point')) {
            $this->payload['commands']->push(
                new CoolingPoint($this->params->get('cooling_point'))
            );
        }

        if ($this->params->has('heating_point')) {
            $this->payload['commands']->push(
                new HeatingPoint($this->params->get('heating_point'))
            );
        }

        if ($this->params->has('mode')) {
            $this->payload['commands']->push(
                new Mode($this->params->get('mode'))
            );
        }

        if ($this->params->has('fan_mode')) {
            $this->payload['commands']->push(
                new FanMode($this->params->get('fan_mode'))
            );
        }

        if ($this->params->has('fan_state')) {
            $this->payload['commands']->push(
                new FanState($this->params->get('fan_state'))
            );
        }
        $this->dispatch();
    }
}
