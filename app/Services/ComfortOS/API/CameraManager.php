<?php

namespace SmartRooms\Services\ComfortOS\API;

use SmartRooms\Contracts\ComfortOS\API\CameraManagementContract;
use SmartRooms\Jobs\ComfortOS\CameraOperation;

use SmartRooms\Services\OpenHAB\API\Commands\Power\Off;
use SmartRooms\Services\OpenHAB\API\Commands\Power\On;

class CameraManager extends APIManager implements CameraManagementContract
{
    public $job = CameraOperation::class;
    public $domain = 'openhab';

    public function update()
    {
        if ($this->params->has('power')) {
            switch ($this->params->get('power')) {
                case 'on':
                    $this->payload['commands']->push(
                        new On
                    );
                break;

                case 'off':
                    $this->payload['commands']->push(
                        new Off
                    );
                break;
            }
        }

        $this->dispatch();
    }
}
