<?php

namespace SmartRooms\Services\ComfortOS\API;

use Carbon\Carbon;
use Exception;
use SmartRooms\Contracts\Repositories\ComfortOS\OpQueueRepository;
use SmartRooms\Events\ComfortOS\OperationRequested;

use SmartRooms\Models\IDAM\User;
use SmartRooms\Models\Components\Wifi\Network;

use SmartRooms\Contracts\System\FlowOutputContract;

abstract class APIManager
{
    public $src = 'api';
    public $user;
    public $entity;
    public $request;
    public $params;
    public $job;
    public $payload = ['entity' => null, 'commands' => []];
    public $operation = 'update';

    public function src (string $src) : self
    {
      $this->src = $src;

      return $this;
    }

    public function user (User $user) : self
    {
      $this->user = $user;

      return $this;
    }

    public function params (array $params) : self
    {
        $this->params = collect ($params);

        return $this;
    }

    public function entity ($entity) : self
    {
        $this->entity = $entity;
        $this->payload['entity'] = $entity;

        return $this;
    }

    public function request ($request) : self
    {
        $this->request  = $request;
        $this->params   = collect ($request->all());
        $this->user     = $request->user();

        return $this;
    }

    public function dispatch() : self
    {
        app (FlowOutputContract::class)->signal (get_class($this).'@'.__FUNCTION__, [$this->params, $this->payload]);

        if (! $this->entity) {

            app (FlowOutputContract::class)->signal ('APIManager Error: No entity');
            throw new Exception('No device entity specified.');

            return $this;
        }

        if (! count($this->payload['commands'])) {

            app (FlowOutputContract::class)->signal ('APIManager Error: No commands');
            throw new Exception('No commands given.');

            return $this;
        }

        $op_queue_data = [
            'src'           => $this->src,
            'domain'        => $this->domain,
            'user_id'       => $this->user->_id ?? null,
            'room_id'       => $this->entity->room_id ?? null,
            'component_id'  => $this->entity->_id ?? null,
            'operation'     => $this->operation,
            'request'       => $this->params->all(),
            'dispatcher'    => get_class($this),
            'executor'      => $this->job,
            'payload'       => ['entity' => $this->entity->toArray(), 'commands' => $this->payload['commands']],
            'execute_at'    => now(),
            'delay'         => false,
            'processed_at'  => null,
            'expire_at'     => now()->addDays(1),
            'result'        => null,
        ];

        if ($this->params->has('execute_at')) {
            if (count($this->params->except(['_method', '_token'])) > 1) { // do we any other fields than this?
                if ($parsed = Carbon::parse($this->params->get('execute_at'))) { // can we make out a datetime?
                    if ($parsed->timestamp > now()->timestamp) { // is it later than now?

                        $op_queue_data['execute_at'] = Carbon::parse($this->params->get('execute_at'));
                        $op_queue_data['delay'] = true;

                        $op_queue = app(OpQueueRepository::class)->create($op_queue_data);
                        $this->payload['op_queue'] = $op_queue;

                        app ($this->job)::dispatch($this->payload)
                            ->onQueue('commands')
                            ->delay(Carbon::parse($this->params->get('execute_at')));

                        event (new OperationRequested($op_queue));

                        return $this;
                    }
                }
            }
        }

        app (FlowOutputContract::class)->signal ('Creating new op queue item.', $op_queue_data);

        $op_queue = app (OpQueueRepository::class)->create($op_queue_data);
        $this->payload['op_queue'] = $op_queue;

        app (FlowOutputContract::class)->signal ('Dispatching new job: '.$this->job);

        if ( $this->domain == 'mikrotik' && strtolower ( env ('COMMAND_VECTOR') ) == 'http' )
        {
          $this->job::dispatch($this->payload)
            ->onQueue('commands');
        }
        else
        {
          $this->job::dispatch($this->payload)
            ->onQueue('commands')
            ->onConnection(env('COMMAND_QUEUE_CONNECTION', 'sync'));
        }

        event(new OperationRequested($op_queue));

        return $this;
    }
}
