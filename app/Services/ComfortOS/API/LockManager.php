<?php

namespace SmartRooms\Services\ComfortOS\API;

use Carbon\Carbon;
use SmartRooms\Contracts\ComfortOS\API\LockManagementContract;
use SmartRooms\Jobs\ComfortOS\LockOperation;
use SmartRooms\Models\Components\Devices\Code;

use SmartRooms\Services\OpenHAB\API\Commands\Lock\Close;
use SmartRooms\Services\OpenHAB\API\Commands\Lock\Insert;
use SmartRooms\Services\OpenHAB\API\Commands\Lock\Open;
use SmartRooms\Services\OpenHAB\API\Commands\Lock\Wipe;

use SmartRooms\Contracts\Repositories\Components\Devices\CodeRepository;

class LockManager extends APIManager implements LockManagementContract
{
    public $job = LockOperation::class;
    public $starting_index = 10;
    public $default_expiry = 86400;
    public $domain = 'openhab';

    public function update()
    {
        $this->payload['commands'] = collect([]);

        if ($this->params->has('command'))
        {
            switch ($this->params->get('command'))
            {
                case 'lock':
                    $this->payload['commands']->push(
                        new Close
                    );
                break;

                case 'unlock':
                    $this->payload['commands']->push(
                        new Open
                    );
                break;
            }
        }

        // WHAT ARE WE DOING HERE?
        // EXPIRY?
        if ($this->params->has('insert'))
        {
            $last_code = $this->entity->codes()->latest()->first();

            if (! $last_code)
            {
                $new_index = $this->starting_index;
            }
            else
            {
                $last_index = intval ($last_code->index);
                $new_index = $last_index + 1;
            }

            $code_data = [
                'lock_id'       => $this->entity->_id,
                'index'         => $new_index,
                'code'          => hash('sha256', $this->params->get('insert')), // we don't use encrypt here because we need to search
                'recovery'      => \Crypt::encrypt($this->params->get('insert')),
                'hint'          => substr($this->params->get('insert'), 0, 2).str_repeat('*', strlen($this->params->get('insert')) - 2),
                'label'         => $this->params->get('label'),
                'expire_at'     => Carbon::parse($this->params->get('expire_at', Carbon::now()->timestamp + $this->default_expiry)),
                'used'          => 0,
                'created_by'    => $this->user->_id,
            ];

            // Operators setting codes from the dashboard shouldn't be changed by guests
            if ( isset ($this->src) && $this->src == 'dash' )
            {
              $code_data['readonly'] = 1;
            }

            $next_code = app (CodeRepository::class)->create($code_data);

            $this->payload['commands']->push (
                new Insert ($next_code->index, $this->params->get('insert'), $next_code->label ?? $next_code->_id)
            );
        }

        if ($this->params->has('wipe'))
        {
            // do we have this code or index?
            $unexpired = $this->entity->codes()->where('expire_at', '>=', Carbon::now())->get();

            $wipe = $this->params->get('wipe');
            $deletable = $unexpired->filter(function ($value, $key) use ($wipe) {
                return $value->_id == $wipe || $value->index == $wipe || $value->code == hash('sha256', $wipe);
            });

            if ($deletable->count() > 0) {
                foreach ($deletable as $code) {
                    $this->wipe($code);
                }
            }
        }

        $this->dispatch();
    }

    public function wipe (Code $code, bool $dispatch = false)
    {
        $this->payload['commands'] = collect([]);

        $this->payload['commands']->push (
            new Wipe ($code->index)
        );

        app (CodeRepository::class)->delete ($code->_id);

        if ($dispatch !== false) {
            $this->dispatch();
        }

        return $this;
    }

    public function expiry (Code $code, string $expire_at)
    {
        app (CodeRepository::class)->update ([
          'expire_at' => Carbon::parse ($expire_at),
        ], $code->_id);

        if ( Carbon::parse ($expire_at)->timestamp < time () )
        {
          $this->wipe ($code);
        }

        return $this;
    }
}
