<?php

namespace SmartRooms\Services\ComfortOS\API;

use Illuminate\Support\Arr;
use SmartRooms\Contracts\ComfortOS\API\MirrorManagementContract;
use SmartRooms\Jobs\ComfortOS\MirrorOperation;

class MirrorManager extends APIManager implements MirrorManagementContract
{
    public $job = MirrorOperation::class;
    public $domain = 'mirrors';
}
