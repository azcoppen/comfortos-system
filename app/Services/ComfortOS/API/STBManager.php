<?php

namespace SmartRooms\Services\ComfortOS\API;

use SmartRooms\Contracts\ComfortOS\API\STBManagementContract;
use SmartRooms\Jobs\ComfortOS\STBOperation;

class STBManager extends APIManager implements STBManagementContract
{
    public $job = STBOperation::class;
    public $domain = 'androidtv';

    public function update()
    {
        $this->dispatch();
    }
}
