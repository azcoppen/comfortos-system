<?php

namespace SmartRooms\Services\ComfortOS\API;

use SmartRooms\Contracts\ComfortOS\API\TVManagementContract;
use SmartRooms\Jobs\ComfortOS\TVOperation;

use SmartRooms\Services\OpenHAB\API\Commands\Power\Off;
use SmartRooms\Services\OpenHAB\API\Commands\Power\On;

use SmartRooms\Services\OpenHAB\API\Commands\Color\Brightness;

use SmartRooms\Services\OpenHAB\API\Commands\TV\Channel;
use SmartRooms\Services\OpenHAB\API\Commands\TV\Contrast;
use SmartRooms\Services\OpenHAB\API\Commands\TV\Keycode;
use SmartRooms\Services\OpenHAB\API\Commands\TV\Sharpness;

class TVManager extends APIManager implements TVManagementContract
{
    public $job = TVOperation::class;
    public $domain = 'openhab';

    public function update()
    {
        $this->payload['commands'] = collect([]);

        if ($this->params->has('power')) {
            switch ($this->params->get('power')) {
                case 'on':
                    $this->payload['commands']->push(
                        new On
                    );
                break;

                case 'off':
                    $this->payload['commands']->push(
                        new Off
                    );
                break;
            }
        }

        if ($this->params->has('brightness')) {
            $this->payload['commands']->push(
                new Brightness($this->params->get('brightness'))
            );
        }

        if ($this->params->has('channel')) {
            $this->payload['commands']->push(
                new Channel($this->params->get('channel'))
            );
        }

        if ($this->params->has('contrast')) {
            $this->payload['commands']->push(
                new Contrast($this->params->get('contrast'))
            );
        }

        if ($this->params->has('keycode')) {
            $this->payload['commands']->push(
                new Keycode($this->params->get('keycode'))
            );
        }

        if ($this->params->has('sharpness')) {
            $this->payload['commands']->push(
                new Sharpness($this->params->get('sharpness'))
            );
        }

        $this->dispatch();
    }
}
