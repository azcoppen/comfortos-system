<?php

namespace SmartRooms\Services\ComfortOS\Flow;

use Carbon\Carbon;
use Exception;
use SmartRooms\Contracts\ComfortOS\RoomAccessManagementContract;
use SmartRooms\Contracts\Repositories\IDAM\AccessRepository;
use SmartRooms\Events\ComfortOS\RoomAccessGranted;
use SmartRooms\Events\ComfortOS\RoomAccessModified;
use SmartRooms\Events\ComfortOS\RoomAccessRevoked;
use SmartRooms\Models\IDAM\Access;
use SmartRooms\Models\IDAM\User;
use SmartRooms\Models\Property\Room;

class RoomAccessManager implements RoomAccessManagementContract
{
    public $user;
    public $room;
    public $access;

    public $start_timezoned;
    public $end_timezoned;

    public $start_utc;
    public $end_utc;

    public $creator;

    public function creator(User $creator) : self
    {
        $this->creator = $creator;

        return $this;
    }

    public function user(User $user) : self
    {
        $this->user = $user;

        return $this;
    }

    public function room(Room $room) : self
    {
        $this->room = $room->load(['building.property']);

        return $this;
    }

    public function existing(Access $access) : self
    {
        $this->access = $access;

        return $this;
    }

    public function start(string $start_date, string $start_time) : self
    {
        $this->start_timezoned = Carbon::parse($start_date.' '.$start_time, $this->room->building->property->timezone);
        $this->start_utc = $this->start_timezoned->copy()->timezone('UTC');

        return $this;
    }

    public function end(string $end_date, string $end_time) : self
    {
        $this->end_timezoned = Carbon::parse($end_date.' '.$end_time, $this->room->building->property->timezone);
        $this->end_utc = $this->end_timezoned->copy()->timezone('UTC');

        return $this;
    }

    public function store() : Access
    {
        if (! $this->user) {
            throw new Exception('No user record specified.');
        }

        if (! $this->room) {
            throw new Exception('No room record specified.');
        }

        $previous = $this->user->accesses()->where('accessible_id', $this->room->_id)->where('end_at', '>', now())->orderBy('created_at', 'DESC');

        if ($previous->count() > 0) {
            event(new RoomAccessRevoked($previous->first()));
            $previous->delete();
        }

        $this->access = app(AccessRepository::class)->create([
            'grantee_type'      => get_class($this->user),
            'grantee_id'        => $this->user->_id,
            'accessible_type'   => get_class($this->room),
            'accessible_id'     => $this->room->_id,
            'type'              => 'full',
            'start_at'          => $this->start_utc,
            'end_at'            => $this->end_utc,
            'created_by'        => $this->creator->_id,
        ]);

        event(new RoomAccessGranted($this->access));

        return $this->access;
    }

    public function modify() : Access
    {
        if (! $this->room) {
            throw new Exception('No room record specified.');
        }

        if (! $this->access) {
            throw new Exception('Access record does not exist.');
        }

        app(AccessRepository::class)->update([
            'accessible_type'   => get_class($this->room),
            'accessible_id'     => $this->room->_id,
            'start_at'          => $this->start_utc,
            'end_at'            => $this->end_utc,
        ], $this->access->_id);

        event(new RoomAccessModified($this->access));

        return $this->access;
    }

    public function halt() : Access
    {
        app(AccessRepository::class)->update([
            'end_at'            => now()->copy()->subSeconds(1)->timezone('UTC'),
        ], $this->access->_id);

        event(new RoomAccessRevoked($this->access));

        return $this->access;
    }
}
