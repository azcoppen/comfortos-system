<?php

namespace SmartRooms\Services\System\IDAM;

use Prettus\Repository\Events\RepositoryEntityUpdated;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\System\PermissionAssociationManagementContract;

class PermissionAssociationManager implements PermissionAssociationManagementContract
{
    public $target;

    public function target($target) : self
    {
        $this->target = $target;

        return $this;
    }

    public function sync(array $params)
    {
        $this->target->permissions()->detach();

        if (! count($params)) {
            $this->target->createNote('All permissions removed.');
            event(new RepositoryEntityUpdated(app(UserRepository::class), $this->target));

            return $this->target;
        }

        $this->target->permissions()->attach($params);
        $this->target->createNote('Permissions synchronised: '.implode(', ', $params));
        event(new RepositoryEntityUpdated(app(UserRepository::class), $this->target));

        return $this->target;
    }
}
