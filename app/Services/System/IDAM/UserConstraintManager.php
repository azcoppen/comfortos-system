<?php

namespace SmartRooms\Services\System\IDAM;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;
use SmartRooms\Models\IDAM\User;

use \Exception;

class UserConstraintManager implements UserConstraintManagementContract
{
  private $target;

  public function target (User $user) : self
  {
    $this->target = $user;

    return $this;
  }

  public function operators () : array
  {
    if (! $this->target || ! is_object ($this->target) )
    {
      throw new Exception ("Unable to identify target user to examine permissions.");
    }

    return $this->target->operator_ids;
  }

  public function brands () : array
  {
    if (! $this->target || ! is_object ($this->target) )
    {
      throw new Exception ("Unable to identify target user to examine permissions.");
    }
  }

  public function properties () : array
  {
    if (! $this->target || ! is_object ($this->target) )
    {
      throw new Exception ("Unable to identify target user to examine permissions.");
    }

    return $this->target->accesses()
    //->where('start_at', '<', now()) BEHAVING STUPIDLY
    //->where('end_at', '>', now()) BEHAVING STUPIDLY
    ->with(['accessible.building.property'])
    ->get()
    ->pluck('accessible.building.property._id')
    ->all();
  }

  public function buildings () : array
  {
    if (! $this->target || ! is_object ($this->target) )
    {
      throw new Exception ("Unable to identify target user to examine permissions.");
    }

    return $this->target->accesses()
    //->where('start_at', '<', now()) BEHAVING STUPIDLY
    //->where('end_at', '>', now()) BEHAVING STUPIDLY
    ->with(['accessible.building'])
    ->get()
    ->pluck('accessible.building._id')
    ->all();
  }

  public function rooms () : array
  {
    if (! $this->target || ! is_object ($this->target) )
    {
      throw new Exception ("Unable to identify target user to examine permissions.");
    }

    return $this->target->accesses()
    //->where('start_at', '<', now()) BEHAVING STUPIDLY
    //->where('end_at', '>', now()) BEHAVING STUPIDLY
    ->get()
    ->pluck('accessible_id')
    ->all();
  }

  public function users () : array
  {

    if (! $this->target || ! is_object ($this->target) )
    {
      throw new Exception ("Unable to identify target user to examine permissions.");
    }

    $available = [$this->target->_id];

    $operators = app (OperatorRepository::class)->with(['brands.properties.buildings.rooms.users'])->findWhereIn('_id', $this->target->operator_ids ?? []);

    if ( $operators && $operators->count () > 0 )
    {
      $available = array_merge ($available, $operators->pluck('user_ids')->flatten()->unique()->all());

      foreach ($operators AS $operator)
      {
        foreach ($operator->brands AS $brand)
        {
          foreach ($brand->properties AS $property)
          {
            foreach ($property->buildings AS $building)
            {
              foreach ($building->rooms AS $room)
              {
                $available = array_merge ($available, $room->user_ids ?? []);
              }
            }
          }
        }
      }

    }

    return collect ($available)->unique()->values()->all();
  }
}
