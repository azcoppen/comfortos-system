<?php

namespace SmartRooms\Services\System\IDAM;

use Prettus\Repository\Events\RepositoryEntityUpdated;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\System\OperatorAssociationManagementContract;

class OperatorAssociationManager implements OperatorAssociationManagementContract
{
    public $target;

    public function target($target) : self
    {
        $this->target = $target;

        return $this;
    }

    public function sync(array $params)
    {
        $this->target->operators()->detach();

        if (! count($params)) {
            $this->target->createNote('All operator associations removed.');
            event(new RepositoryEntityUpdated(app(UserRepository::class), $this->target));

            return $this->target;
        }

        $this->target->operators()->attach($params);
        $this->target->createNote('Operator associations synchronised: '.implode(', ', $params));
        event(new RepositoryEntityUpdated(app(UserRepository::class), $this->target));

        return $this->target;
    }
}
