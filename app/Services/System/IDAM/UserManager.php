<?php

namespace SmartRooms\Services\System\IDAM;

use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use libphonenumber\PhoneNumberFormat;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\System\UserManagementContract;
use SmartRooms\Models\Idam\User;

class UserManager implements UserManagementContract
{
    public $source_interface;
    public $user;
    public $initiator;

    public function api () : self
    {
        $this->source_interface = 'api';

        return $this;
    }

    public function dashboard () : self
    {
        $this->source_interface = 'dashboard';

        return $this;
    }

    public function user (User $user) : self
    {
        $this->user = $user;

        return $this;
    }

    public function entity ($entity = null)
    {
      if ( $entity )
      {
        $this->user = $entity;
        return $this;
      }

      return $this->entity;
    }

    public function initiator (?User $initiator = null)
    {
      if ( $initiator )
      {
        $this->initiator = $initiator;
        return $this;
      }

      return $this->initiator;
    }

    public function store (array $params) : User
    {
      switch ($this->source_interface)
      {
          case 'dashboard':
          case 'api':

            $params['password'] = Hash::make($params['password']);

            if (! isset($params['display']) )
            {
              $params['display'] = Str::title ($params['first'].' '.$params['last']);
            }

            if (! isset($params['slug']) )
            {
                $params['slug'] = Str::slug ($params['first'].'-'.$params['last'].'-'.uniqid());
            }

            if ( isset($params['dob']) )
            {
                $params['dob'] = Carbon::parse (date('Y-m-d', strtotime($params['dob'])))->format('Y-m-d');
            }

            if (isset($params['telephones']) && ! empty($params['telephones']))
            {
                $params['telephones'] = explode("\n", $params['telephones']);
                if (! is_array($params['telephones']) || ! count($params['telephones'])) {
                    unset($params['telephones']);
                } else {
                    foreach ($params['telephones'] as $index => $telephone) {
                        $params['telephones'][$index] = phone ($telephone, $params['country'] ?? 'US', PhoneNumberFormat::E164);
                    }
                }
            }

            if (isset($params['tags']) && ! empty($params['tags']))
            {
                $params['tags'] = explode("\n", $params['tags']);

                if (! is_array($params['tags']) || ! count($params['tags'])) {
                    unset($params['tags']);
                } else {
                    foreach ($params['tags'] as $index => $tag) {
                        $params['tags'][$index] = Str::slug($tag);
                    }
                }
            }

            $params['location'] = [
                'street'  => Str::title ($params['street'] ?? null),
                'city'    => Str::title ($params['city'] ?? null),
                'region'  => $params['region'],
                'postal'  => strtoupper($params['postal'] ?? null),
                'country' => strtoupper($params['country'] ?? 'US'),
            ];

            unset ($params['street'], $params['city'], $params['region'], $params['postal'], $params['country']);

            $params['geo'] = [
              'type' => 'Point',
              'coordinates' => [
                floatval($params['longitude']) ?? 0,
                floatval($params['latitude']) ?? 0,
              ],
            ];

            unset ($params['longitude'], $params['latitude']);

            $params['otp_secret'] = Crypt::encrypt(Str::random(24));
            $params['created_by'] = $this->initiator->_id;
            $params['last_login_at'] = null;
            $params['last_activity_at'] = null;
            $params['email_verified_at'] = null;
            $params['pwd_changed_at'] = now();
            $params['pwd_expires_at'] = now()->addYears(1);

          break;
      }

      $this->user = app (UserRepository::class)->update($params, $this->user->_id);
      $this->user->createNote ('Created manually via '.$this->source_interface.' '.Carbon::now()->format('c'));

      return $this->user;

    }

    public function update (array $params) : User
    {
        if (! $this->user )
        {
            throw new \Exception ('Uwer record not specified.');
        }

        switch ($this->source_interface)
        {
            case 'dashboard':
            case 'api':

            $image = $this->user->image;

            if ($image && !isset($params['image']) )
            {
              $image = null;
            }

            if ( isset($params['image']) )
            {
              try
              {
                $result = $params['image']->storeOnCloudinaryAs('users', uniqId());

                if ( $result )
                {
                  $image = Str::afterLast($result->getPublicId(), '/');
                }
              }
              catch (\Exception $e) {}
            }

            $params['image'] = $image;

            if (!isset($params['password']) || empty($params['password']))
            {
                unset($params['password']);
            }

            if (isset($params['password']) && empty($params['password']))
            {
                unset($params['password']);
            }

            if (isset($params['password']) && ! empty($params['password']))
            {
                $params['password'] = Hash::make($params['password']);
                $params['pwd_changed_at'] = now();
                $params['pwd_expires_at'] = now()->addYears(1);
            }

            if (! isset($params['display']) || (isset($params['display']) && empty($params['display']))) {
                $params['display'] = Str::title($params['first'].' '.$params['last']);
            }

            if (! isset($params['slug']) || (isset($params['slug']) && empty($params['slug']))) {
                $params['slug'] = Str::slug($params['first'].'-'.$params['last'].'-'.uniqid());
            }

            if (isset($params['dob']) && ! empty($params['dob'])) {
                $params['dob'] = Carbon::parse(date('Y-m-d', strtotime($params['dob'])))->format('Y-m-d');
            }

            if (isset($params['telephones']) && ! empty($params['telephones'])) {
                $params['telephones'] = explode("\n", $params['telephones']);
                if (! is_array($params['telephones']) || ! count($params['telephones'])) {
                    unset($params['telephones']);
                } else {
                    foreach ($params['telephones'] as $index => $telephone) {
                        $params['telephones'][$index] = phone($telephone, $params['country'] ?? 'US', PhoneNumberFormat::E164);
                    }
                }
            }

            if (isset($params['tags']) && ! empty($params['tags'])) {
                $params['tags'] = explode("\n", $params['tags']);

                if (! is_array($params['tags']) || ! count($params['tags'])) {
                    unset($params['tags']);
                } else {
                    foreach ($params['tags'] as $index => $tag) {
                        $params['tags'][$index] = Str::slug($tag);
                    }
                }
            }

            $params['location'] = [
                'street'  => Str::title($params['street'] ?? null),
                'city'    => Str::title($params['city'] ?? null),
                'region'  => $params['region'],
                'postal'  => strtoupper($params['postal'] ?? null),
                'country' => strtoupper($params['country'] ?? 'US'),
            ];

            unset($params['street'], $params['city'], $params['region'], $params['postal'], $params['country']);

            $params['geo'] = [
              'type' => 'Point',
              'coordinates' => [
                floatval($params['longitude']) ?? 0,
                floatval($params['latitude']) ?? 0,
              ],
            ];

            unset($params['longitude'], $params['latitude']);

            if (! $this->user->otp_secret) {
                $params['otp_secret'] = Crypt::encrypt(Str::random(24));
            }

            break;
        }

        if ( isset($params['password']) && !$params['password'] )
        {
          unset($params['password']);
        }

        if ( isset($params['password']) && empty($params['password']) )
        {
          unset($params['password']);
        }

        $this->user = app(UserRepository::class)->update($params, $this->user->_id);
        $this->user->createNote('Updated manually via '.$this->source_interface.' '.Carbon::now()->format('c'));

        return $this->user;
    }
}
