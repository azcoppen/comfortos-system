<?php

namespace SmartRooms\Services\System\IDAM;

use SmartRooms\Services\System\Entities\EntityManager;

use SmartRooms\Contracts\Repositories\IDAM\AccessRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Contracts\System\RoomUserManagementContract;
use SmartRooms\Models\Property\Room;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RoomUserManager extends EntityManager implements RoomUserManagementContract
{
  public $repository = UserRepository::class;
  public $has_parent = true;
  public $room;

  public function room (Room $room)
  {
    if ( $room )
    {
      $this->room = $room;
      return $this;
    }

    return $this->room;
  }

  public function store () : self
  {
    $this->__check (__FUNCTION__, false);

    $this->__persist ( __FUNCTION__, null, [
      'prefix'                => '',
      'first'                 => 'Guest',
      'last'                  => 'User',
      'suffix'                => '',
      'display'               => 'Floor '.$this->room->floor.' Room '.$this->room->number.' Guest User',
      'slug'                  => $this->parent->property->slug.'-room-'.$this->room->number.'-guest-user',
      'email'                 => $this->parent->property->slug.'-floor-'.$this->room->floor.'-room-'.$this->room->number.'-guest-user@rooms.smartrooms.dev',
      'password'              => Hash::make ('room-'.$this->room->number),
      'dob'                   => null,
      'sex'                   => 'X',
      'job_title'             => 'Anonymous',
      'location'              => $this->parent->property->location,
      'geo'                   => $this->parent->property->geo,
      'telephones'            => $this->parent->property->telephones,
      'lang'                  => $this->parent->property->lang,
      'locale'                => $this->parent->property->locale,
      'timezone'              => $this->parent->property->timezone,
      'currency'              => $this->parent->property->currency,
      'otp_secret'            => Crypt::encrypt(Str::random(24)),
      'image'                 => '',
      'tags'                  => array_merge(['anon', 'guest',], $this->room->tags),
      'last_login_at'         => now(),
      'last_activity_at'      => now(),
      'email_verified_at'     => now(),
      'pwd_changed_at'        => now(),
      'pwd_expires_at'        => now()->addYears(1),
      'created_by'            => $this->initiator->_id,
    ]);

    $this->entity->assignRole(['anon', 'guest']);

    $this->room->users()->attach ($this->entity);

      // Create room access
    $access = app (AccessRepository::class)->create (
    [
        'grantee_type'      => get_class ($this->entity),
        'grantee_id'        => $this->entity->_id,
        'accessible_type'   => get_class ($this->room),
        'accessible_id'     => $this->room->_id,
        'type'              => 'full',
        'start_at'          => now(),
        'end_at'            => now()->addYears(10),
        'created_by'        => $this->initiator->_id,
    ]);

    return $this;
  }
}
