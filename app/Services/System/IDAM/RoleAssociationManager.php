<?php

namespace SmartRooms\Services\System\IDAM;

use Prettus\Repository\Events\RepositoryEntityUpdated;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\System\RoleAssociationManagementContract;

class RoleAssociationManager implements RoleAssociationManagementContract
{
    public $target;

    public function target($target) : self
    {
        $this->target = $target;

        return $this;
    }

    public function sync(array $params)
    {
        $this->target->roles()->detach();

        if (! count($params)) {
            $this->target->createNote('All roles removed.');
            event(new RepositoryEntityUpdated(app(UserRepository::class), $this->target));

            return $this->target;
        }

        $this->target->roles()->attach($params);
        $this->target->createNote('Roles synchronised: '.implode(', ', $params));
        event(new RepositoryEntityUpdated(app(UserRepository::class), $this->target));

        return $this->target;
    }
}
