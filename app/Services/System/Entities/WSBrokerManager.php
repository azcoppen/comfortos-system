<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\WSBrokerManagementContract;

use SmartRooms\Contracts\Repositories\Components\OS\WSBrokerRepository;

use \Carbon\Carbon;
use Illuminate\Support\Str;

class WSBrokerManager extends EntityManager implements WSBrokerManagementContract
{
  public $repository = WSBrokerRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->put ('created_by', $this->initiator->_id ?? null)
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all());

    if ( $this->params->get ('vpn_server_id') )
    {
      $this->entity->vpn_servers()->attach ($this->params->get ('vpn_server_id'));
    }

    if ( $this->params->get ('vpn_client_id') )
    {
      $this->entity->vpn_clients()->attach ($this->params->get ('vpn_client_id'));
    }

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all(), $this->entity->_id);

    if ( $this->params->get ('vpn_server_id') )
    {
      $this->entity->vpn_servers()->sync ([$this->params->get ('vpn_server_id')]);
    }

    if ( $this->params->get ('vpn_client_id') )
    {
      $this->entity->vpn_clients()->sync ([$this->params->get ('vpn_client_id')]);
    }

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    $this->entity->properties()->detach();
    $this->entity->vpn_servers()->detach();
    $this->entity->vpn_clients()->detach();

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }

}
