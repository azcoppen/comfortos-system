<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\VirtualWifiNetworkManagementContract;

use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;

use \Carbon\Carbon;
use \Crypt;
use Illuminate\Support\Str;

use SmartRooms\Services\ComfortOS\API\WifiManager;

class VirtualWifiNetworkManager extends EntityManager implements VirtualWifiNetworkManagementContract
{
  public $repository = NetworkRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $launch_at = $expire_at = null;

    try {
      $launch_at = Carbon::parse ($this->params->get ('launch_at_date') .' '.$this->params->get ('launch_at_time'), $this->initiator->timezone);
    } catch ( \Exception $e) {}

    try {
      $expire_at = Carbon::parse ($this->params->get ('expire_at_date') .' '.$this->params->get ('expire_at_time'), $this->initiator->timezone);
    } catch ( \Exception $e) {}


    $result = app (WifiManager::class)
      ->src ('dash')
      ->user ($this->initiator)
      ->params ([
        'room'      => $this->parent->_id,
        'label'     => $this->params->get ('label', 'Untitled Network'),
        'ssid'      => $this->params->get ('ssid', 'SmartRooms'),
        'password'  => $this->params->get ('password', 'smartrooms'),
        'launch_at' => $launch_dt ?? now($this->initiator->timezone),
        'expire_at' => $expire_dt ?? now($this->initiator->timezone)->addYears(10),
      ])
      ->store ();

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $launch_at = $expire_at = null;

    if ( $this->params->has ('expire_at_date') )
    {
      try {
        $expire_at = Carbon::parse ($this->params->get ('expire_at_date') .' '.$this->params->get ('expire_at_time'), $this->initiator->timezone);
      } catch ( \Exception $e) {}

      $this->params->put ('expire_at', $expire_at);
      $this->params->pull ('expire_at_date');
      $this->params->pull ('expire_at_time');
    }

    $this->params
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->only(['label', 'expire_at'])->all(), $this->entity->_id);

    if ( $this->params->has ('ssid') || $this->params->has ('password') || $this->params->has ('expire_at') )
    {
      $result = app (WifiManager::class)
        ->src ('dash')
        ->user ($this->initiator)
        ->entity ($this->entity)
        ->params ($this->params->all())
        ->update ();
    }

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    $result = app (WifiManager::class)
      ->src ('dash')
      ->user ($this->initiator)
      ->entity ($this->entity)
      ->params ([])
      ->destroy ();

    return $this;
  }
}
