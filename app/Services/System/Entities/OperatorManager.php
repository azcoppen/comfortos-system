<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\OperatorManagementContract;

use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Contracts\Repositories\Property\BuildingRepository;
use SmartRooms\Contracts\Repositories\Property\PropertyRepository;
use SmartRooms\Contracts\Repositories\Operator\BrandRepository;
use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;

use Illuminate\Support\Str;

class OperatorManager extends EntityManager implements OperatorManagementContract
{
  public $repository = OperatorRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $image = null;

    if ( $this->params->has ('image') )
    {
      try
      {
        $result = $this->params->get ('image')->storeOnCloudinaryAs('operators', uniqId());

        if ( $result )
        {
          $image = Str::afterLast($result->getPublicId(), '/');
        }
      }
      catch (\Exception $e) {}
    }

    $this->__persist ( __FUNCTION__, null, [
      'label'       => $this->params->get ('label', 'Untitled Item'),
      'slug'        => $this->unique_slug ($this->params->get ('slug'), $this->params->get ('label')),
      'location'    => $this->location_block (),
      'geo'         => $this->geo_block (),
      'telephones'  => $this->params->has ('telephones') ? $this->format_telephone_list ($this->params->get ('telephones'), $this->params->get ('country', 'US')) : [],
      'emails'      => $this->params->has ('emails') ? $this->format_email_list ($this->params->get ('emails')) : [],
      'image'       => $image,
      'lang'        => $this->params->get ('lang', 'en'),
      'locale'      => $this->params->get ('locale', 'en_US'),
      'timezone'    => $this->params->get ('timezone', 'PST'),
      'currency'    => $this->params->get ('currency', 'USD'),
      'created_by'  => $this->initiator->_id ?? null,
    ]);

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $image = $this->entity->image;

    if ( $this->params->has ('image') )
    {
      try
      {
        $result = $this->params->get ('image')->storeOnCloudinaryAs('operators', uniqId());

        if ( $result )
        {
          $image = Str::afterLast($result->getPublicId(), '/');
        }
      }
      catch (\Exception $e) {}
    }

    if ($image && !$this->params->has ('image') )
    {
      $image = null;
    }

    $this->__persist ( __FUNCTION__, null, [
      'label'       => $this->params->get ('label', 'Untitled Item'),
      'slug'        => $this->entity->slug != $this->params->get ('slug') ? $this->unique_slug ($this->params->get ('slug'), $this->params->get ('label')) : $this->entity->slug,
      'location'    => $this->location_block (),
      'geo'         => $this->geo_block (),
      'telephones'  => $this->params->has ('telephones') ? $this->format_telephone_list ($this->params->get ('telephones'), $this->params->get ('country', 'US')) : [],
      'emails'      => $this->params->has ('emails') ? $this->format_email_list ($this->params->get ('emails')) : [],
      'image'       => $image,
      'lang'        => $this->params->get ('lang', 'en'),
      'locale'      => $this->params->get ('locale', 'en_US'),
      'timezone'    => $this->params->get ('timezone', 'PST'),
      'currency'    => $this->params->get ('currency', 'USD'),
    ], $this->entity->_id);

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    foreach ( $this->entity->brands()->get() AS $brand )
    {
      foreach ( $brands->properties()->get() AS $property )
      {
        foreach ( $property->buildings()->get() AS $building )
        {
          foreach ( $building->rooms()->get() AS $room )
          {
            $room->users()->delete();
            app (RoomRepository::class)->delete ($room->_id);
          } // end rooms
          app (BuildingRepository::class)->delete ($building->_id);
        } // end buildings
        app (PropertyRepository::class)->delete ($property->_id);
      } // end properties
      app (BrandRepository::class)->delete ($brand->_id);
    } // end brands

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }

}
