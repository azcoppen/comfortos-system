<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\WifiNetworkManagementContract;

use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;

use \Carbon\Carbon;
use \Crypt;
use Illuminate\Support\Str;

class WifiNetworkManager extends EntityManager implements WifiNetworkManagementContract
{
  public $repository = NetworkRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $launch_at = $expire_at = null;

    try {
      $launch_at = Carbon::parse ($this->params->get ('launch_at_date') .' '.$this->params->get ('launch_at_time'), $this->initiator->timezone)
        ->timezone('UTC');
    } catch ( \Exception $e) {}

    try {
      $expire_at = Carbon::parse ($this->params->get ('expire_at_date') .' '.$this->params->get ('expire_at_time'), $this->initiator->timezone)
        ->timezone('UTC');
    } catch ( \Exception $e) {}

    $this->__persist ( __FUNCTION__, null, [
      'router_id'         => $this->params->get ('router_id'),
      'label'             => $this->params->get ('label', 'Untitled Item'),
      'type'              => $this->params->get ('type', 'hardware'),
      'interface'         => $this->params->get ('interface', 'wlan1'),
      'security_profile'  => $this->params->get ('security_profile', 'default'),
      'profile_index'     => $this->params->get ('profile_index', '*A'),
      'network_index'     => $this->params->get ('network_index', '*A'),
      'ssid'              => $this->params->get ('ssid', 'SmartRooms'),
      'password'          => Crypt::encrypt ($this->params->get ('password', 'smartrooms')),
      'launch_at'         => $launch_dt ?? now(),
      'expire_at'         => $expire_dt ?? now()->addYears(10),
      'installed_at'      => $this->params->get ('installed_at'),
      'installed_by'      => $this->params->get ('installed_by', $this->initiator->_id ?? null),
      'provisioned_at'    => $this->params->get ('provisioned_at'),
      'provisioned_by'    => $this->params->get ('provisioned_by', $this->initiator->_id ?? null),
      'created_by'        => $this->initiator->_id ?? null,
    ]);

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $launch_at = $expire_at = null;

    try {
      $launch_at = Carbon::parse ($this->params->get ('launch_at_date') .' '.$this->params->get ('launch_at_time'), $this->initiator->timezone)
        ->timezone('UTC');
    } catch ( \Exception $e) {}

    try {
      $expire_at = Carbon::parse ($this->params->get ('expire_at_date') .' '.$this->params->get ('expire_at_time'), $this->initiator->timezone)
        ->timezone('UTC');
    } catch ( \Exception $e) {}

    $this->__persist ( __FUNCTION__, null, [
      'router_id'         => $this->params->get ('router_id'),
      'label'             => $this->params->get ('label', 'Untitled Item'),
      'type'              => $this->params->get ('type', 'hardware'),
      'interface'         => $this->params->get ('interface', 'wlan1'),
      'security_profile'  => $this->params->get ('security_profile', 'default'),
      'profile_index'     => $this->params->get ('profile_index', '*A'),
      'network_index'     => $this->params->get ('network_index', '*A'),
      'ssid'              => $this->params->get ('ssid', 'SmartRooms'),
      'password'          => Crypt::encrypt ($this->params->get ('password', 'smartrooms')),
      'launch_at'         => $launch_dt ?? now(),
      'expire_at'         => $expire_dt ?? now()->addYears(10),
      'installed_at'      => $this->params->get ('installed_at'),
      'installed_by'      => $this->params->get ('installed_by', $this->initiator->_id ?? null),
      'provisioned_at'    => $this->params->get ('provisioned_at'),
      'provisioned_by'    => $this->params->get ('provisioned_by', $this->initiator->_id ?? null),
    ], $this->entity->_id);

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }
}
