<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\BuildingManagementContract;

use SmartRooms\Contracts\Repositories\Property\BuildingRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

use Illuminate\Support\Str;

class BuildingManager extends EntityManager implements BuildingManagementContract
{
  public $repository = BuildingRepository::class;
  public $has_parent = true;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $image = null;

    if ( $this->params->has ('image') )
    {
      try
      {
        $result = $this->params->get ('image')->storeOnCloudinaryAs('buildings', uniqId());

        if ( $result )
        {
          $image = Str::afterLast($result->getPublicId(), '/');
        }
      }
      catch (\Exception $e) {}
    }

    $this->__persist ( __FUNCTION__, null, [
      'property_id' => $this->parent->_id ?? null,
      'label'       => $this->params->get ('label', 'Untitled Item'),
      'slug'        => $this->unique_slug ($this->params->get ('slug'), $this->params->get ('label')),
      'identifier'  => Str::slug ($this->params->get ('identifier')),
      'geo'         => $this->geo_block (),
      'image'       => $image,
      'tags'        => $this->params->has ('tags') ? $this->format_tags_list ($this->params->get ('tags')) : [],
      'created_by'  => $this->initiator->_id ?? null,
    ]);

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $image = $this->entity->image;

    if ( $this->params->has ('image') )
    {
      try
      {
        $result = $this->params->get ('image')->storeOnCloudinaryAs('buildings', uniqId());

        if ( $result )
        {
          $image = Str::afterLast($result->getPublicId(), '/');
        }
      }
      catch (\Exception $e) {}
    }

    if ($image && !$this->params->has ('image') )
    {
      $image = null;
    }

    $this->__persist ( __FUNCTION__, null, [
      'label'       => $this->params->get ('label', 'Untitled Item'),
      'slug'        => $this->entity->slug != $this->params->get ('slug') ? $this->unique_slug ($this->params->get ('slug'), $this->params->get ('label')) : $this->entity->slug,
      'identifier'  => Str::slug ($this->params->get ('identifier')),
      'geo'         => $this->geo_block (),
      'image'       => $image,
      'tags'        => $this->params->has ('tags') ? $this->format_tags_list ($this->params->get ('tags')) : [],
    ], $this->entity->_id);

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    foreach ( $building->rooms()->get() AS $room )
    {
      $room->users()->delete();
      app (RoomRepository::class)->delete ($room->_id);
    } // end rooms

    app ($this->repository)->delete ($building->_id);

    return $this;
  }
}
