<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\VPNServerManagementContract;

use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;

use \Carbon\Carbon;
use Illuminate\Support\Str;

class VPNServerManager extends EntityManager implements VPNServerManagementContract
{
  public $repository = VPNServerRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->put ('created_by', $this->initiator->_id ?? null)
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all());

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all(), $this->entity->_id);

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    $this->entity->dects()->detach();
    $this->entity->habs()->detach();
    $this->entity->mirrors()->detach();
    $this->entity->pbxs()->detach();
    $this->entity->routers()->detach();
    $this->entity->stbs()->detach();

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }

}
