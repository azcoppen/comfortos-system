<?php

namespace SmartRooms\Services\System\Entities;

use Illuminate\Http\Request;
use SmartRooms\Models\IDAM\User;

use \Carbon\Carbon;
use \Crypt;
use Illuminate\Support\Str;
use Propaganistas\LaravelPhone\PhoneNumber;

use SmartRooms\Contracts\System\FlowOutputContract;

use \Exception;

abstract class EntityManager
{
  public $repository;
  public $initiator;
  public $has_parent = false;
  public $parent;
  public $entity;
  public $params;
  private $debug = false;

  public function debug () : self
  {
    $this->debug = true;
    return $this;
  }

  public function __check ( string $method = 'store', bool $has_params = true ) : self
  {
    if (! $this->initiator || ! is_object ($this->initiator) )
    {
      throw new Exception ("Initiator not set or not an object.");
    }

    if ( isset ($this->has_parent) && $this->has_parent !== FALSE )
    {
      if (! $this->parent || ! is_object ($this->parent) )
      {
        throw new Exception ("Parent entity not set or not an object.");
      }
    }

    switch ( $method )
    {
      case 'store':
        if ( $has_params )
        {
          if (! $this->params || !is_object ($this->params) )
          {
            throw new Exception ("No parameters specified.");
          }

          if (! count ($this->params) )
          {
            throw new Exception ("Empty parameters.");
          }
        }
      break;

      case 'update':

        if (! $this->entity || ! is_object ($this->entity) )
        {
          throw new Exception ("Entity not set or not an object.");
        }

        if ( $has_params )
        {
          if (! $this->params || !is_object ($this->params) )
          {
            throw new Exception ("No parameters specified.");
          }

          if (! count ($this->params) )
          {
            throw new Exception ("Empty parameters.");
          }
        }
      break;

      case 'destroy':

        if (! $this->entity || ! is_object ($this->entity) )
        {
          throw new Exception ("Entity not set or not an object.");
        }

      break;
    }

    return $this;
  }

  public function mutations (string $key, $item)
  {
    try
    {
      if ( Str::contains ($key, '_at') && !empty ($item) )
      {
        if (is_object ($item))
        {
          return $item->timezone('UTC');
        }

        return Carbon::parse ($item, $this->initiator->timezone)->timezone('UTC');
      }

      if ( Str::contains ($key, 'config') && !empty ($item) )
      {
        return json_decode ($item);
      }

      if ( Str::contains ($key, 'did') )
      {
        return phone ($item, $this->params->get('country', 'US'))->lenient()->formatE164();
      }

      if ( Str::contains ($key, 'certificate') && !empty ($item) )
      {
        return Crypt::encrypt ($item);
      }

      if ( Str::contains ($key, '_pass') && !empty ($item) )
      {
        return Crypt::encrypt ($item);
      }

      if ( Str::contains ($key, '_key') && !empty ($item) )
      {
        return Crypt::encrypt ($item);
      }

      if ( Str::contains ($key, '_secret') && !empty ($item) )
      {
        return Crypt::encrypt ($item);
      }

      if ( Str::contains ($key, '_port') && !empty ($item) && !Str::contains ($key, 'range') )
      {
        return intval ($item);
      }

      if ( Str::contains ($key, '_pwd') && !empty ($item) )
      {
        return Crypt::encrypt ($item);
      }

      if ( Str::contains ($key, 'tags') )
      {
        return $this->format_tags_list ($item);
      }

      if ( Str::contains ($key, 'trunk_number') )
      {
        return phone ($item, $this->params->get('country'))->lenient()->formatE164();
      }

    }
    catch ( Exception $e )
    {
      throw $e;
    }

    if ( is_numeric ($item) )
    {
      return intval ($item);
    }

    return $item;
  }

  public function initiator (?User $initiator = null)
  {
    if ( $initiator )
    {
      $this->initiator = $initiator;
      return $this;
    }

    return $this->initiator;
  }

  public function parent ($parent = null)
  {
    if ( $parent )
    {
      $this->parent = $parent;
      return $this;
    }

    return $this->parent;
  }

  public function entity ($entity = null)
  {
    if ( $entity )
    {
      $this->entity = $entity;
      return $this;
    }

    return $this->entity;
  }

  public function params ($params = null) : self
  {
    if ( $params )
    {
      $this->params = is_array ($params) ? collect ($params)->except (['_method', '_token']) : $params;
      $this->params->pull ('vpn_server_id');
      $this->params->pull ('vpn_client_id');
      return $this;
    }

    return $this->params;
  }

  public function __persist (string $method, ?string $relation = null, array $data, ?string $_id = null) : self
  {
    if ( $this->debug )
    {
      app (FlowOutputContract::class)->signal (get_class ($this).'::'.$method, ['params' => $this->params, 'args' => func_get_args()]);
      return $this;
    }

    if (! $method || empty ($method) )
    {
      throw new Exception ("No method specified.");
    }

    if ( $method == 'store' || $method == 'batch' )
    {
      $this->entity = app ($this->repository)->create ($data);

      if ( is_object ($this->params) && $this->params->has ('notes') )
      {
        $this->entity->createNote ($this->params->get ('notes'));
      }
    }

    if ( $method == 'update' && $_id )
    {
      $this->entity = app ($this->repository)->update ($data, $_id);

      if ( is_object ($this->params) && $this->params->has ('notes') )
      {
        $this->entity->createNote ($this->params->get ('notes'));
      }
    }

    return $this;
  }

  public function _clone (int $num, $relations = null)
  {
    for ($i = 0; $i < intval($num); $i++ )
    {
      $clone = $this->entity->replicate ();
      $clone->label .= ' Copy';
      $clone->save();

      if (! empty($this->params->get ('vpn_server_id')) )
      {
        $clone->vpn_servers()->attach ($this->params->get ('vpn_server_id'));
      }

      if (! empty($this->params->get ('vpn_client_id')) )
      {
        $clone->vpn_clients()->attach ($this->params->get ('vpn_client_id'));
      }

      if ( $relations )
      {
        foreach ($relations AS $relation => $field)
        {
          $clone->{$relation}()->attach ($this->params->get ($field));
        }
      }

    }
  }

  public function unique_slug ( ?string $slug = null, string $label ) : string
  {
    // No slug specified. try the label.
    $existing = app ($this->repository)->findWhere (['slug' => Str::slug ($slug ?? $label)])->count();

    return $existing > 1 ? Str::slug ($slug ?? $label).'-'.uniqId () : Str::slug ($slug ?? $label);

  }

  public function location_block () : array
  {
    return [
      'street'  => $this->params->get ('street'),
      'city'    => $this->params->get ('city'),
      'region'  => $this->params->get ('region'),
      'postal'  => $this->params->get ('postal'),
      'country' => $this->params->get ('country', 'US'),
    ];
  }

  public function geo_block () : array
  {
    return [
      'type' => 'Point',
      'coordinates' => [
        floatval ($this->params->get ('longitude', 0)),
        floatval ($this->params->get ('latitude', 0)),
      ],
    ];
  }

  public function format_telephone_list ( string $telephones, $country = 'US' ) : array
  {
    if ( $telephones && !empty ($telephones) )
    {
      $entries = explode ("\n", $telephones);

      if (is_array ($entries) && count ($entries))
      {
        return collect ($entries)->transform (function ($item, $key) use ($country) {
          return PhoneNumber::make ($item, $country)->formatE164();
        })->all();
      }
    }

    return [];
  }

  public function format_email_list ( string $emails ) : array
  {
    if ( $emails && !empty ($emails) )
    {
      $entries = explode ("\n", $emails);

      if (is_array ($entries) && count ($entries))
      {
        return collect ($entries)->transform (function ($item, $key) {
          return filter_var ($item, FILTER_VALIDATE_EMAIL) ? $item : null;
        })->filter()->all();
      }

    }
    return [];
  }

  public function format_tags_list ( string $tags ) : array
  {
    if ( $tags && !empty ($tags) )
    {
      $entries = explode ("\n", $tags);

      if (is_array ($entries) && count ($entries))
      {
        return collect ($entries)->transform (function ($item, $key) {
          return Str::slug ($item);
        })->filter()->all();
      }

    }
    return [];
  }
}
