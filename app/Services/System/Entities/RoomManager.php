<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\RoomManagementContract;
use SmartRooms\Contracts\System\RoomUserManagementContract;

use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Contracts\Repositories\Components\OS\HABRepository;

use Illuminate\Support\Str;

class RoomManager extends EntityManager implements RoomManagementContract
{
  public $repository = RoomRepository::class;
  public $has_parent = true;

  public function batch () : self
  {
    $this->__check (__FUNCTION__);

    $image = null;

    if ( $this->params->has ('image') )
    {
      try
      {
        $result = $this->params->get ('image')->storeOnCloudinaryAs('rooms', uniqId());

        if ( $result )
        {
          $image = Str::afterLast($result->getPublicId(), '/');
        }
      }
      catch (\Exception $e) {}
    }

    $this->parent->load (['property.brand.operator']);

    for ($i = intval ($this->params->get('start')); $i < intval ($this->params->get('start')) + intval ($this->params->get('num')); $i++ )
    {
      $this->__persist ( __FUNCTION__, null, [
        'building_id' => $this->parent->_id,
        'floor'       => intval ($this->params->get('floor')),
        'number'      => $i,
        'label'       => 'Room '.$i.', '.$this->parent->label.', '.$this->parent->property->label,
        'type'        => $this->params->get ('type'),
        'grade'       => $this->params->get ('grade'),
        'description' => $this->params->get ('description'),
        'footage'     => intval ($this->params->get ('footage')),
        'beds'        => intval ($this->params->get ('beds')),
        'occupancy'   => intval ($this->params->get ('occupancy')),
        'smoking'     => intval ($this->params->get ('smoking')),
        'AC'          => intval ($this->params->get ('AC')),
        'eth'         => intval ($this->params->get ('eth')),
        'ext'         => intval ($this->params->get ('ext')),
        'public'      => intval ($this->params->get ('public')),
        'geo'         => $this->geo_block (),
        'image'       => $image,
        'tags'        => collect(array_merge ($this->parent->property->brand->operator->tags ?? [], $this->parent->property->brand->tags ?? [], $this->parent->property->tags ?? [], $this->parent->tags ?? [],))->unique()->all(),
        'retention'   => 7,
        'created_by'  => $this->initiator->_id ?? null,
      ]);

      app (RoomUserManagementContract::class)
        ->initiator ($this->initiator)
        ->parent ($this->parent)
        ->room ($this->entity)
        ->store ();

    } // end for

    return $this;
  }

  public function provision_hab () : self
  {
    $this->__check (__FUNCTION__);

    if ( app (HABRepository::class)->find (head ($this->params->get('habs'))) )
    {
      app (HABRepository::class)->update ([
        'room_id'         => $this->entity->_id,
        'provisioned_at'  => now (),
        'provisioned_by'  => $this->initiator->_id,
      ], head ($this->params->get('habs')));
    }

    return $this;
  }

  public function deprovision_hab () : self
  {
    $this->__check (__FUNCTION__);

    if ( app (HABRepository::class)->find (head ($this->params->get('habs'))) )
    {
      app (HABRepository::class)->update ([
        'room_id'         => null,
        'provisioned_at'  => null,
        'provisioned_by'  => null,
      ], head ($this->params->get('habs')));
    }

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $image = $this->entity->image;

    if ( $this->params->has ('image') )
    {
      try
      {
        $result = $this->params->get ('image')->storeOnCloudinaryAs('rooms', uniqId());

        if ( $result )
        {
          $image = Str::afterLast($result->getPublicId(), '/');
        }
      }
      catch (\Exception $e) {}
    }

    if ($image && !$this->params->has ('image') )
    {
      $image = null;
    }

    $this->__persist ( __FUNCTION__, null, [
      'label'       => $this->params->get ('label'),
      'type'        => $this->params->get ('type'),
      'grade'       => $this->params->get ('grade'),
      'description' => $this->params->get ('description'),
      'footage'     => intval ($this->params->get ('footage')),
      'beds'        => intval ($this->params->get ('beds')),
      'occupancy'   => intval ($this->params->get ('occupancy')),
      'smoking'     => intval ($this->params->get ('smoking')),
      'AC'          => intval ($this->params->get ('AC')),
      'eth'         => intval ($this->params->get ('eth')),
      'ext'         => intval ($this->params->get ('ext')),
      'public'      => intval ($this->params->get ('public')),
      'geo'         => $this->geo_block (),
      'image'       => $image,
      'tags'        => $this->params->has ('tags') ? $this->format_tags_list ($this->params->get ('tags')) : [],
      'retention'   => 7,
    ], $this->entity->_id);

    if ( is_array ($this->params->get ('users')) && count ($this->params->get ('users')) )
    {
      $this->entity->users()->sync ($this->params->get ('users'));
    }

    return $this;
  }

  public function destroy () : self
  {
    $this->entity->users()->delete();
    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }

}
