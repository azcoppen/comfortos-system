<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\HandsetManagementContract;

use SmartRooms\Contracts\Repositories\Components\Telecoms\HandsetRepository;

use \Carbon\Carbon;
use \Crypt;
use Illuminate\Support\Str;

class HandsetManager extends EntityManager implements HandsetManagementContract
{
  public $repository = HandsetRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->put ('created_by', $this->initiator->_id ?? null)
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id', 'dect_id'])->all());

    if ( $this->params->get ('dect_id') )
    {
      $this->entity->dects()->attach ($this->params->get ('dect_id'));
    }

    if ( $this->params->get('clone') && is_numeric ($this->params->get('clone')) && intval($this->params->get('clone')) > 0 )
    {
      $this->_clone (intval ($this->params->get('clone')), ['dects' => 'dect_id']);
    }

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id', 'dect_id'])->all(), $this->entity->_id);

    if ( $this->params->get ('dect_id') )
    {
      $this->entity->dects()->sync ([$this->params->get ('dect_id')]);
    }

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    $this->entity->extensions()->detach();
    $this->entity->dects()->detach();

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }
}
