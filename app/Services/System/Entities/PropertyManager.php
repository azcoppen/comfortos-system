<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\PropertyManagementContract;

use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Contracts\Repositories\Property\BuildingRepository;
use SmartRooms\Contracts\Repositories\Property\PropertyRepository;

use Illuminate\Support\Str;

class PropertyManager extends EntityManager implements PropertyManagementContract
{
  public $repository = PropertyRepository::class;
  public $has_parent = true;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $image = null;

    if ( $this->params->has ('image') )
    {
      try
      {
        $result = $this->params->get ('image')->storeOnCloudinaryAs('properties', uniqId());

        if ( $result )
        {
          $image = Str::afterLast($result->getPublicId(), '/');
        }
      }
      catch (\Exception $e) {}
    }

    $this->__persist ( __FUNCTION__, null, [
      'brand_id'    => $this->parent->_id,
      'label'       => $this->params->get ('label', 'Untitled Item'),
      'slug'        => $this->unique_slug ($this->params->get ('slug'), $this->params->get ('label')),
      'location'    => $this->location_block (),
      'geo'         => $this->geo_block (),
      'telephones'  => $this->params->has ('telephones') ? $this->format_telephone_list ($this->params->get ('telephones'), $this->params->get ('country', 'US')) : [],
      'emails'      => $this->params->has ('emails') ? $this->format_email_list ($this->params->get ('emails')) : [],
      'image'       => $image,
      'lang'        => $this->params->get ('lang', 'en'),
      'locale'      => $this->params->get ('locale', 'en_US'),
      'timezone'    => $this->params->get ('timezone', 'PST'),
      'currency'    => $this->params->get ('currency', 'USD'),
      'tags'        => $this->params->has ('tags') ? $this->format_tags_list ($this->params->get ('tags')) : [],
      'created_by'  => $this->initiator->_id ?? null,
    ]);

    if ( $this->params->get ('mqtt_broker_id') )
    {
      $this->entity->mqtt_brokers()->attach ($this->params->get ('mqtt_broker_id'));
    }

    if ( $this->params->get ('ws_broker_id') )
    {
      $this->entity->ws_brokers()->attach ($this->params->get ('ws_broker_id'));
    }

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $image = $this->entity->image;

    if ( $this->params->has ('image') )
    {
      try
      {
        $result = $this->params->get ('image')->storeOnCloudinaryAs('properties', uniqId());

        if ( $result )
        {
          $image = Str::afterLast($result->getPublicId(), '/');
        }
      }
      catch (\Exception $e) {}
    }

    if ($image && !$this->params->has ('image') )
    {
      $image = null;
    }

    $this->__persist ( __FUNCTION__, null, [
      'label'       => $this->params->get ('label', 'Untitled Item'),
      'slug'        => $this->entity->slug != $this->params->get ('slug') ? $this->unique_slug ($this->params->get ('slug'), $this->params->get ('label')) : $this->entity->slug,
      'location'    => $this->location_block (),
      'geo'         => $this->geo_block (),
      'telephones'  => $this->params->has ('telephones') ? $this->format_telephone_list ($this->params->get ('telephones'), $this->params->get ('country', 'US')) : [],
      'emails'      => $this->params->has ('emails') ? $this->format_email_list ($this->params->get ('emails')) : [],
      'image'       => $image,
      'lang'        => $this->params->get ('lang', 'en'),
      'locale'      => $this->params->get ('locale', 'en_US'),
      'timezone'    => $this->params->get ('timezone', 'PST'),
      'currency'    => $this->params->get ('currency', 'USD'),
      'tags'        => $this->params->has ('tags') ? $this->format_tags_list ($this->params->get ('tags')) : [],
    ], $this->entity->_id);

    if ( $this->params->get ('mqtt_broker_id') )
    {
      $this->entity->mqtt_brokers()->sync ([$this->params->get ('mqtt_broker_id')]);
    }

    if ( $this->params->get ('ws_broker_id') )
    {
      $this->entity->ws_brokers()->sync ([$this->params->get ('ws_broker_id')]);
    }

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    foreach ( $property->buildings()->get() AS $building )
    {
      foreach ( $building->rooms()->get() AS $room )
      {
        $room->users()->delete();
        app (RoomRepository::class)->delete ($room->_id);
      } // end rooms
      app (BuildingRepository::class)->delete ($building->_id);
    } // end buildings

    $this->entity->mqtt_brokers()->detach();
    $this->entity->ws_brokers()->detach();

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }
}
