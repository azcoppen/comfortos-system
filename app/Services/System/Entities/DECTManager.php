<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\DECTManagementContract;

use SmartRooms\Contracts\Repositories\Components\Telecoms\DECTRepository;

use \Carbon\Carbon;
use \Crypt;
use Illuminate\Support\Str;

class DECTManager extends EntityManager implements DECTManagementContract
{
  public $repository = DECTRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->put ('created_by', $this->initiator->_id ?? null)
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all());

    if ( $this->params->get ('vpn_server_id') )
    {
      $this->entity->vpn_servers()->attach ($this->params->get ('vpn_server_id'));
    }

    if ( $this->params->get ('vpn_client_id') )
    {
      $this->entity->vpn_clients()->attach ($this->params->get ('vpn_client_id'));
    }

    if ( $this->params->get('clone') && is_numeric ($this->params->get('clone')) && intval($this->params->get('clone')) > 0 )
    {
      $this->_clone (intval ($this->params->get('clone')));
    }

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all(), $this->entity->_id);

    if (! empty ($this->params->get ('vpn_server_id')) )
    {
      $this->entity->vpn_servers()->sync ([$this->params->get ('vpn_server_id')]);
    }

    if ( $this->params->get ('vpn_client_id') )
    {
      $this->entity->vpn_clients()->sync ([$this->params->get ('vpn_client_id')]);
    }

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    $this->entity->extensions()->detach();
    $this->entity->handsets()->detach();
    $this->entity->vpn_servers()->detach();
    $this->entity->vpn_clients()->detach();

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }
}
