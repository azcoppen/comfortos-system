<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\ComponentManagementContract;

use \Exception;
use SmartRooms\Models\Property\Room;

use SmartRooms\Contracts\Repositories\Components\Devices;
use SmartRooms\Contracts\Repositories\Components\Mirror;
use SmartRooms\Contracts\Repositories\Components\Telecoms;
use SmartRooms\Contracts\Repositories\Components\Wifi;

class ComponentManager extends EntityManager implements ComponentManagementContract
{
  public $provisionables = [
    'dects'       => Telecoms\DECTRepository::class,
    'extensions'  => Telecoms\ExtensionRepository::class,
    'handsets'    => Telecoms\HandsetRepository::class,
    'mirrors'     => Mirror\DisplayRepository::class,
    'routers'     => Wifi\RouterRepository::class,
    'stbs'        => Devices\STBRepository::class,
    'wiif-networks' => Wifi\NetworkRepository::class,
  ];

  public $habables = [
    'bulbs'       => Devices\BulbRepository::class,
    'cameras'     => Devices\CameraRepository::class,
    'chromecasts' => Devices\ChromecastRepository::class,
    'dimmers'     => Devices\DimmerRepository::class,
    'leds'        => Devices\LEDRepository::class,
    'locks'       => Devices\LockRepository::class,
    'motors'      => Devices\MotorRepository::class,
    'plugs'       => Devices\PlugRepository::class,
    'sensors'     => Devices\SensorRepository::class,
    'speakers'    => Devices\SpeakerRepository::class,
    'stbs'        => Devices\STBRepository::class,
    'switches'    => Devices\PhysicalSwitchRepository::class,
    'thermostats' => Devices\ThermostatRepository::class,
    'tvs'         => Devices\TVRepository::class,
  ];

  public $has_parent = true;

  public function provision () : self
  {
    $this->__check (__FUNCTION__);

    $group = head (array_keys($this->params->get('provisionables')));

    if (! $group || empty ($group) || !array_key_exists ($group, $this->provisionables) )
    {
      throw new Exception ("Group must be specified.");
    }

    foreach ( $this->params->get('provisionables') AS $group_key => $items )
    {
      foreach ( $items AS $cpt_id )
      {
        if ( app ($this->provisionables[$group_key])->find ($cpt_id) )
        {
          app ($this->provisionables[$group_key])->update ([
            'room_id'         => $this->parent->_id,
            'provisioned_at'  => now (),
            'provisioned_by'  => $this->initiator->_id,
          ], $cpt_id);
        }
      }
    }

    return $this;
  }

  public function deprovision () : self
  {
    $this->__check (__FUNCTION__);

    $group = head (array_keys($this->params->get('deprovisionables')));

    if (! $group || empty ($group) || !array_key_exists ($group, $this->provisionables) )
    {
      throw new Exception ("Group must be specified.");
    }

    foreach ( $this->params->get('deprovisionables') AS $group_key => $items )
    {
      foreach ( $items AS $cpt_id )
      {
        if ( app ($this->provisionables[$group_key])->find ($cpt_id) )
        {
          app ($this->provisionables[$group_key])->update ([
            'room_id'         => null,
            'provisioned_at'  => null,
            'provisioned_by'  => null,
          ], $cpt_id);
        }
      }
    }

    return $this;
  }

  public function store ( string $group ) : self
  {
    $this->__check (__FUNCTION__);

    if (! $group || empty ($group) || !array_key_exists ($group, $this->habables) )
    {
      throw new Exception ("Group must be specified.");
    }

    $this->repository = $this->habables[$group];

    $this->params
      ->put ('room_id', $this->parent->_id ?? null)
      ->put ('created_by', $this->initiator->_id ?? null)
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone'])->all());

    if ( $this->params->get('clone') && is_numeric ($this->params->get('clone')) && intval($this->params->get('clone')) > 0 )
    {
      $this->_clone (intval ($this->params->get('clone')));
    }

    return $this;
  }

  public function update ( string $group ) : self
  {
    $this->__check (__FUNCTION__);

    $this->repository = $this->habables[$group];

    $this->params
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes'])->all(), $this->entity->_id);

    return $this;
  }

  public function destroy ( string $group ) : self
  {
    $this->__check (__FUNCTION__);

    $this->repository = $this->habables[$group];

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }

}
