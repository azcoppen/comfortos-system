<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\RouterManagementContract;

use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;

use \Carbon\Carbon;
use \Crypt;
use Illuminate\Support\Str;

class RouterManager extends EntityManager implements RouterManagementContract
{
  public $repository = RouterRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->put ('created_by', $this->initiator->_id ?? null)
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all());

    if (! empty($this->params->get ('vpn_server_id')) )
    {
      $this->entity->vpn_servers()->attach ($this->params->get ('vpn_server_id'));
    }

    if ( $this->params->get ('vpn_client_id') )
    {
      $this->entity->vpn_clients()->attach ($this->params->get ('vpn_client_id'));
    }

    if ( $this->params->get('clone') && is_numeric ($this->params->get('clone')) && intval($this->params->get('clone')) > 0 )
    {
      $this->_clone (intval ($this->params->get('clone')));
    }

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all(), $this->entity->_id);

    if (! empty ($this->params->get ('vpn_server_id')) )
    {
      $this->entity->vpn_servers()->sync ([$this->params->get ('vpn_server_id')]);
    }

    if ( $this->params->get ('vpn_client_id') )
    {
      $this->entity->vpn_clients()->sync ([$this->params->get ('vpn_client_id')]);
    }

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    $this->entity->vpn_servers()->detach();
    $this->entity->vpn_clients()->detach();

    foreach ($this->entity->networks()->get() AS $network)
    {
      app (NetworkRepository::class)->delete ($network->_id);
    }

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }
}
