<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\ExtensionManagementContract;

use SmartRooms\Contracts\Repositories\Components\Telecoms\ExtensionRepository;

use \Carbon\Carbon;
use \Crypt;
use Illuminate\Support\Str;

class ExtensionManager extends EntityManager implements ExtensionManagementContract
{
  public $repository = ExtensionRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->put ('created_by', $this->initiator->_id ?? null)
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all());

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes', 'clone', 'vpn_client_id', 'vpn_server_id'])->all(), $this->entity->_id);

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }
}
