<?php

namespace SmartRooms\Services\System\Entities;

use SmartRooms\Contracts\System\MirrorModuleManagementContract;

use SmartRooms\Contracts\Repositories\Components\Mirror\ModuleRepository;

use \Carbon\Carbon;
use \Crypt;
use Illuminate\Support\Str;

class MirrorModuleManager extends EntityManager implements MirrorModuleManagementContract
{
  public $repository = ModuleRepository::class;

  public function store () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->put ('created_by', $this->initiator->_id ?? null)
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes'])->all());

    return $this;
  }

  public function update () : self
  {
    $this->__check (__FUNCTION__);

    $this->params
      ->transform (function ($item, $key) {
        return $this->mutations ($key, $item);
      });

    $this->__persist ( __FUNCTION__, null, $this->params->except(['_method', '_token', 'notes'])->all(), $this->entity->_id);

    return $this;
  }

  public function destroy () : self
  {
    $this->__check (__FUNCTION__);

    app ($this->repository)->delete ($this->entity->_id);

    return $this;
  }
}
