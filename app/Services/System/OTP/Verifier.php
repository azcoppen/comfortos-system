<?php

namespace SmartRooms\Services\System\OTP;

use OTPHP\TOTP;
use ParagonIE\ConstantTime\Base32;
use SmartRooms\Contracts\System\OTPVerificationContract;

class Verifier implements OTPVerificationContract
{
    public function verify(?string $secret, string $code, ?int $digits = 8, ?int $seconds = 1800) : bool
    {
        return TOTP::create(
            Base32::encode($secret ?? env('APP_NAME')), // Let the secret be defined by the class
            $seconds,    // The period is now 30mins
            'sha256', // The digest algorithm
            $digits      // The output will generate 8 digits
        )->verify($code);
    }
}
