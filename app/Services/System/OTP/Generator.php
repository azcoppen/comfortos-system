<?php

namespace SmartRooms\Services\System\OTP;

use OTPHP\TOTP;
use OTPHP\TOTPInterface;
use ParagonIE\ConstantTime\Base32;
use SmartRooms\Contracts\System\OTPGenerationContract;

class Generator implements OTPGenerationContract
{
    public function create(?string $secret = null, ?int $digits = 8, ?int $seconds = 1800) : string
    {
        return TOTP::create(
            Base32::encode($secret ?? env('APP_NAME')), // We can use something better
            $seconds,    // The period is now 30mins
            'sha256', // The digest algorithm
            $digits      // The output will generate 8 digits
        )->now();
    }

    public function batch(?int $total = 10) : array
    {
        $created = [];

        for ($i = 0; $i <= $total; $i++) {
            $created[] = $this->create();
        }

        return $created;
    }
}
