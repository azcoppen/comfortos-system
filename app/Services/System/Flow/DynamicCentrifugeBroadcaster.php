<?php

namespace SmartRooms\Services\System\Flow;

use LaraComponents\Centrifuge\CentrifugeBroadcaster;

class DynamicCentrifugeBroadcaster extends CentrifugeBroadcaster
{
  public function server ( array $config ) : self
  {
      $this->centrifuge->server ($config);

      return $this;
  }

  public function __construct ()
	{
    parent::__construct (new DynamicCentrifuge);
	}
}
