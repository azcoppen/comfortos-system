<?php

namespace SmartRooms\Services\System\Flow;

use SmartRooms\Contracts\System\MQTTPublishingContract;
use SmartRooms\Contracts\System\FlowOutputContract;

use SmartRooms\Models\Components\OS\MQTTBroker;

use \PhpMqtt\Client\MqttClient;
use \PhpMqtt\Client\ConnectionSettings;
use PhpMqtt\Client\Exceptions\MqttClientException;

use \Log;
use \Exception;

class MQTTPublisher implements MQTTPublishingContract
{
  public $mqtt;
  public $broker;

  public function broker ( MQTTBroker $broker ) : self
  {
    $this->broker = $broker;

    return $this;
  }

  public function send ( string $topic, $payload ) : bool
  {
    if (! $this->broker )
    {
      throw new Exception ("Broker not specified.");
    }

    app (FlowOutputContract::class)->signal ('MQTTPublisher::send() __BROKER__ --> '.$this->broker->host.':'.($this->broker->context == 'remote' ? $this->broker->secure_port : $this->broker->insecure_port));
    app (FlowOutputContract::class)->signal ('MQTTPublisher::send() __AUTH__ --> '.$this->broker->app_user.' / '.$this->broker->app_pass);
    app (FlowOutputContract::class)->signal ('MQTTPublisher::send() __MESSAGE__ --> '.$topic.' ==> '.$payload);

    try
    {

      $this->mqtt = new MqttClient (
        $this->broker->host,
        ($this->broker->context == 'remote' ? $this->broker->secure_port : $this->broker->insecure_port),
        env('APP_NAME')."_".env('APP_ENV').'_'.uniqId()
      );

      if ( $this->broker->context == 'remote' )
      {
        $settings = (new ConnectionSettings)
            ->setConnectTimeout (3)
            ->setUseTls (true)
            ->setTlsVerifyPeer (false)
            ->setTlsVerifyPeerName (false)
            ->setTlsSelfSignedAllowed (true)
            ->setUsername ($this->broker->app_user)
            ->setPassword ($this->broker->app_pass);
      }

      if ( $this->broker->context == 'local' )
      {
        $settings = (new ConnectionSettings)
            ->setConnectTimeout (3)
            ->setUsername ($this->broker->app_user)
            ->setPassword ($this->broker->app_pass);
      }

      $this->mqtt->connect ($settings, true);
      $this->mqtt->publish ($topic, $payload, 0);
      $this->mqtt->disconnect ();

      return true;

    }
    catch (MqttClientException $e)
    {
      Log::error ("MQTT connection error: ".$e->getMessage());
      Log::error ($e);

      return false;
    }
    catch (Exception $e)
    {
      Log::error ("Unable to send MQTT message: ".$e->getMessage());
      Log::error ($e);

      return false;
    }

  }

}
