<?php

namespace SmartRooms\Services\System\Flow;

use SmartRooms\Models\IDAM\User;
use SmartRooms\Models\Property\Room;

use SmartRooms\Contracts\System\WSBrokerAccessContract;
use \Crypt;

use SmartRooms\Services\System\Flow\DynamicCentrifuge;

class MultiCentrifugoAccess implements WSBrokerAccessContract
{
  private $engine;
  private $broker;
  private $user;
  private $room;

  public function __construct ()
  {
    $this->engine = new DynamicCentrifuge;
  }

  public function room ( Room $room ) : self
  {
    $this->room = $room;
    $this->room->load (['building.property.ws_brokers', 'users']);
    $this->broker = $this->room->building->property->ws_brokers->first();

    return $this;
  }

  public function user ( User $user ) : self
  {
    $this->user = $user;

    return $this;
  }

  public function token () : string
  {

    if ( $this->room )
    {
      config ([
        'broadcasting.connections.centrifuge.url'       => $this->broker->http_url,
        'broadcasting.connections.centrifuge.secret'    => $this->broker->hmac_secret,
        'broadcasting.connections.centrifuge.api_key'   => $this->broker->api_key,
      ]);

      return $this->engine
        ->server (config ('broadcasting.connections.centrifuge'))
        ->generateToken ($this->room->users->first()->_id);
    }

    if ( $this->user )
    {
      return $this->engine
        ->server (config ('broadcasting.connections.centrifuge'))
        ->generateToken ($this->user->_id);
    }

    return $this->engine
      ->server (config ('broadcasting.connections.centrifuge'))
      ->generateToken(auth()->user()->_id);

  }

}
