<?php

namespace SmartRooms\Services\System\Flow;

use LaraComponents\Centrifuge\Centrifuge;
use GuzzleHttp\Client;

class DynamicCentrifuge extends Centrifuge
{
  public function server ( array $config ) : self
  {
      $this->config = $config;

      return $this;
  }

  public function __construct ()
	{
    parent::__construct([], new Client);
	}
}
