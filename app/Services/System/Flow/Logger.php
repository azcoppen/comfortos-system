<?php

namespace SmartRooms\Services\System\Flow;

use SmartRooms\Contracts\System\FlowOutputContract;

class Logger implements FlowOutputContract
{
  public function __construct ()
  {

  }

  public function signal ( ?string $msg = null, $mixed = null, ?bool $is_request = false ) : self
  {
    if (! env ('FLOW_CHANNEL') )
    {
      return false;
    }

    $msg = "[".env('APP_ENV')."] " . $msg;

    try
    {

      if ( $is_request !== FALSE )
      {
        \Log::channel (env ('FLOW_CHANNEL'))->info (
          $msg . $mixed->method() . ' ' . $mixed->path(),
          [
            'headers' => $mixed->headers(),
            'body'    => $mixed->all()
          ]
        );
        return true;
      }

      \Log::channel (env ('FLOW_CHANNEL'))->debug ($msg, !is_array ($mixed) ? [$mixed] : $mixed);

      return $this;

    }
    catch ( \Exception $e )
    {
      \Log::channel ('daily')->error ($e);
    }

    return $this;
  }

  public function error ( ?string $msg = null, $mixed = null ) : self
  {
    if (! env ('FLOW_CHANNEL') )
    {
      return false;
    }

    $msg = "[".env('APP_ENV')."] " . $msg;

    try
    {

      \Log::channel (env ('FLOW_CHANNEL'))->error ($msg, !is_array ($mixed) ? [$mixed] : $mixed);

      return $this;

    }
    catch ( \Exception $e )
    {
      \Log::channel ('daily')->error ($e);
    }

    return $this;
  }
}
