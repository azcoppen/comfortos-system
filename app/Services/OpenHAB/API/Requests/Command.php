<?php

namespace SmartRooms\Services\OpenHAB\API\Requests;

use SmartRooms\Contracts\OpenHAB\API\CommandContract;

abstract class Command implements CommandContract {

    public $command     = '';
    public $arguments   = [];

}
