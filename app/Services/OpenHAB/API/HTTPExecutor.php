<?php

namespace SmartRooms\Services\OpenHAB\API;

use SmartRooms\Contracts\OpenHAB\API\CommandExecutionContract;

use \Exception;
use Illuminate\Support\Facades\Http;
use \Log;

use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;

use SmartRooms\Contracts\System\FlowOutputContract;
use SmartRooms\Contracts\System\MQTTPublishingContract;
use SmartRooms\Contracts\System\MikroTikCLIProcessContract;

use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;

use SmartRooms\Events\ComfortOS\HABCommandSent;

class HTTPExecutor implements CommandExecutionContract
{
  private $queue;
  private $entity;
  private $body;
  private $response;

  public function queue ( $queue ) : self
  {
    $this->queue = $queue;

    return $this;
  }

  public function entity ( $entity ) : self
  {
    $this->entity = $entity;

    return $this;
  }

  private function build ($command)
  {
    if ( isset ($command->command) && ! count ($command->arguments) )
    {
      return $command->command;
    }

    if ( isset ($command->arguments) && count ($command->arguments) )
    {
      if ( count ($command->arguments) == 1 )
      {
        return head ($command->arguments);
      }

      if ( count ($command->arguments) > 1 )
      {
        return implode (',', $command->arguments);
      }
    }

    return '';
  }

  public function fire ( $command ) : bool
  {

      if (! $command )
      {
        app (FlowOutputContract::class)->error ('fire(): NO COMMAND', $command);
        return false;
      }

      if (! $command->item )
      {
        app (FlowOutputContract::class)->error ('fire(): Command has no item', $command);
        return false;
      }

      if (! $this->entity )
      {
        app (FlowOutputContract::class)->error ('fire(): Entity is NULL', $this->entity);
        return false;
      }

      /***************************************
      MIKROTIK CLI
      ***************************************/
      if ( isset ($this->queue) && $this->queue->domain == 'mkrotik' )
      {
        if ( strtolower ( env ('COMMAND_VECTOR') ) == 'http' )
        {
          $this->entity = $this->entity->router;

          // What do we do here about the MQTT topic?
          $this->entity->load (['room.building.property']);

          if (! isset ($this->entity->room) || ! is_object ($this->entity->room) )
          {
            app (FlowOutputContract::class)->error ('fire(): Cannot get room ID', $this->entity);
            return false;
          }

          if (! isset ($this->entity->room->building) || ! is_object ($this->entity->room->building) )
          {
            app (FlowOutputContract::class)->error ('fire(): Cannot get building ID', $this->entity);
            return false;
          }

          if (! isset ($this->entity->room->building->property) || ! is_object ($this->entity->room->building->property) )
          {
            app (FlowOutputContract::class)->error ('fire(): Cannot get property ID', $this->entity);
            return false;
          }

          $result = app (MikroTikCLIProcessContract::class)
            ->connection ($this->entity->vpn_ip, $this->entity->api_user, $this->entity->api_pass)
            ->mqtt (false) // if we're already set to HTTP, MQTT isn't available
            ->command ($command->cli)
            ->args ($command->args);

          app (FlowOutputContract::class)->signal ('fire(): triggering CLI '.$result->input());

          try
          {

            $result->execute ()->output();

          }
          catch (Exception $e)
          {
            app (FlowOutputContract::class)->error ($e->getMessage(), $e);
          }

          $command = app (HABCommandRepository::class)->create ([
            'room_id'       => $this->entity->room_id,
            'group'         => $this->entity->plural,
            'component_id'  => $this->entity->_id,
            'attribute'     => $command->item,
            'value'         => $command->args,
            'sent_at'       => now(),
            'response'      => 200
          ]);

          event ( new HABCommandSent ($command) );

          return true;
        }

      }



      /***************************************
      OPENHAB REST API
      ***************************************/

      if (! $this->entity->items || ! isset ($this->entity->items) || ! is_array ($this->entity->items) || ! count ($this->entity->items) )
      {
        app (FlowOutputContract::class)->error ('fire(): Entity HAB items are not present', $this->entity);
        return false;
      }

      if (! isset ($this->entity->items[$command->item]) )
      {
        // the color widget sends its value as [hsb] but the HAB item is called [color] because it can do different colour classes.
        if ( $command->item != 'hsb' )
        {
          app (FlowOutputContract::class)->error ('fire(): Cannot find HAB item to trigger', [$command, $this->entity->items]);
          return false;
        }
      }

      app (FlowOutputContract::class)->signal ('fire(): triggering '.$this->entity->hab_item_endpoint . $this->entity->items[($command->item == 'hsb' ? 'color' : $command->item)], $this->build ($command));

      $this->response = Http::timeout(5)->withOptions([
            'debug' => false,
            'verify' => false, // OpenHAB's SSL is self-signed
        ])->withHeaders ([
          'User-Agent'    => 'smartrooms/1.0',
          'Accept'        => 'application/json',
        ]);

      if ( $this->entity->hab_http_security )
      {
        $this->response = $this->response->withBasicAuth ($this->entity->hab_http_user, $this->entity->hab_http_pass);
      }

      try
      {

        $this->response = $this->response->withBody ((string)$this->build ($command), 'text/plain')
        ->post (
          $this->entity->hab_item_endpoint . $this->entity->items[($command->item == 'hsb' ? 'color' : $command->item)]
        );

        app (FlowOutputContract::class)->signal ('fire(): response is '.$this->response->status (), [
          'headers'    => $this->response->headers(),
          'successful' => $this->response->successful (),
          'failed'     => $this->response->failed (),
          'client'     => $this->response->clientError(),
          'server'     => $this->response->serverError(),
        ]);

        $command = app (HABCommandRepository::class)->create ([
          'room_id'       => $this->entity->room_id,
          'group'         => $this->entity->plural,
          'component_id'  => $this->entity->_id,
          'attribute'     => $command->item,
          'value'         => $this->build ($command),
          'sent_at'       => now(),
          'response'      => is_object ($this->response) && method_exists ($this->response, 'status') ? $this->response->status () : 0,
        ]);

        event ( new HABCommandSent ($command) );


        if ( is_object ($this->response) && method_exists ($this->response, 'successful') && $this->response->successful() )
        {
          return true;
        }

      }
      catch (RequestException $e)
      {
        app (FlowOutputContract::class)->error ($e->getMessage(), $e);

        $command = app (HABCommandRepository::class)->create ([
          'room_id'       => $this->entity->room_id,
          'group'         => $this->entity->plural,
          'component_id'  => $this->entity->_id,
          'attribute'     => $command->item,
          'value'         => $this->build ($command),
          'sent_at'       => now(),
          'response'      => is_object ($this->response) && method_exists ($this->response, 'status') ? $this->response->status () : 0,
        ]);

        event ( new HABCommandSent ($command) );

        if ( isset ($this->response) && is_object ($this->response) && method_exists ($this->response, 'status') )
        {
          app (FlowOutputContract::class)->signal ('fire(): response is '.$this->response->status (), [
            'headers'    => $this->response->headers(),
            'successful' => $this->response->successful (),
            'failed'     => $this->response->failed (),
            'client'     => $this->response->clientError(),
            'server'     => $this->response->serverError(),
          ]);
        }
      }
      catch (ConnectionException $e)
      {
        app (FlowOutputContract::class)->error ($e->getMessage(), $e);
      }
      catch (\Exception $e)
      {
        Log::error($e);
      }

      return false;
  }

}
