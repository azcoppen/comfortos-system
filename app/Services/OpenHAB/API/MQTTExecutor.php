<?php

namespace SmartRooms\Services\OpenHAB\API;

use SmartRooms\Contracts\OpenHAB\API\CommandExecutionContract;
use SmartRooms\Contracts\System\MQTTPublishingContract;

use \Exception;
use \Log;

use SmartRooms\Contracts\System\FlowOutputContract;
use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;

use SmartRooms\Events\ComfortOS\HABCommandSent;
use SmartRooms\Models\Components\Wifi\Network;

class MQTTExecutor implements CommandExecutionContract
{
  private $queue;
  private $client;
  private $entity;
  private $body;
  private $response;

  public function queue ( $queue ) : self
  {
    $this->queue = $queue;

    return $this;
  }

  public function entity ( $entity ) : self
  {
    $this->entity = $entity;

    return $this;
  }

  private function build ($command)
  {
    if ( isset ($command->command) && ! count ($command->arguments) )
    {
      return $command->command;
    }

    if ( isset ($command->arguments) && count ($command->arguments) )
    {
      if ( count ($command->arguments) == 1 )
      {
        return head ($command->arguments);
      }

      if ( count ($command->arguments) > 1 )
      {
        return implode (',', $command->arguments);
      }
    }

    return '';
  }

  public function fire ( $command ) : bool
  {

      if (! $command )
      {
        app (FlowOutputContract::class)->error ('fire(): NO COMMAND', $command);
        return false;
      }

      if (! $command->item )
      {
        app (FlowOutputContract::class)->error ('fire(): Command has no item', $command);
        return false;
      }

      if (! $this->entity )
      {
        app (FlowOutputContract::class)->error ('fire(): Entity is NULL', $this->entity);
        return false;
      }

      // Have to do a switcheroo here, as we're acting on the parent router, not the DB entity
      if ( get_class($this->entity) == Network::class )
      {
        $this->entity = $this->entity->router;
      }

      $this->entity->load (['room.building.property.mqtt_brokers']);

      if (! isset ($this->entity->room) || ! is_object ($this->entity->room) )
      {
        app (FlowOutputContract::class)->error ('fire(): Cannot get room ID', $this->entity);
        return false;
      }

      if (! isset ($this->entity->room->building) || ! is_object ($this->entity->room->building) )
      {
        app (FlowOutputContract::class)->error ('fire(): Cannot get building ID', $this->entity);
        return false;
      }

      if (! isset ($this->entity->room->building->property) || ! is_object ($this->entity->room->building->property) )
      {
        app (FlowOutputContract::class)->error ('fire(): Cannot get property ID', $this->entity);
        return false;
      }

      app (FlowOutputContract::class)->signal ('fire(): publishing to MQTT topic '.$this->entity->mqtt_topic . '/' . $command->item . '/command', $this->build ($command));

      try
      {
        $mqtt_broker = $this->entity->room->building->property->mqtt_brokers->first() ?? null;

        if (! $mqtt_broker )
        {
          app (FlowOutputContract::class)->error ('fire(): Property has no MQTT broker.', $this->entity->room->building->property->mqtt_brokers);
          return false;
        }

        app (MQTTPublishingContract::class)
          ->broker ($mqtt_broker)
          ->send ($this->entity->mqtt_topic . '/' . $command->item . '/command', (string) $this->build ($command));

      }
      catch (Exception $e)
      {
        app (FlowOutputContract::class)->error ($e->getMessage(), $e);
      }

      $command = app (HABCommandRepository::class)->create ([
        'room_id'       => $this->entity->room_id,
        'group'         => $this->entity->plural,
        'component_id'  => $this->entity->_id,
        'attribute'     => $command->item,
        'value'         => $this->build ($command),
        'sent_at'       => now(),
        'response'      => 200
      ]);

      event ( new HABCommandSent ($command) );


      return true;
  }

}
