<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Power;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class On extends Command
{
  public $item        = 'power';
  public $command     = 'ON';
}
