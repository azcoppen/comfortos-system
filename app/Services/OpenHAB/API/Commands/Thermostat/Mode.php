<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Thermostat;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Mode extends Command
{
  public $item        = 'mode';
  public $command     = 'OFF';

  public function __construct ( $mode )
  {
      $this->arguments = [$mode];
  }
}
