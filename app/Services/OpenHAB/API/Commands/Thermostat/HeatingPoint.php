<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Thermostat;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class HeatingPoint extends Command
{
    public $item        = 'heating_point';
    public $arguments   = [];

    public function __construct ( int $degrees )
    {
        $this->arguments = [$degrees];
    }
}
