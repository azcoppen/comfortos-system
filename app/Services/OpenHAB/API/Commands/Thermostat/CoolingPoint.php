<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Thermostat;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class CoolingPoint extends Command
{
    public $item        = 'cooling_point';
    public $arguments   = [];

    public function __construct ( int $degrees )
    {
        $this->arguments = [$degrees];
    }
}
