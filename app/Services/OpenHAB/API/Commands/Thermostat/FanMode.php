<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Thermostat;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class FanMode extends Command
{
  public $item        = 'mode';
  public $command     = 'OFF';

  public function __construct ( int $mode )
  {
      $this->arguments = [$this->modes];
  }
}
