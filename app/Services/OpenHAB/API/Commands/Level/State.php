<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Level;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class State extends Command
{
    public $item        = 'state';
    public $arguments   = [];

    public function __construct ( int $level )
    {
        $this->arguments = [$level];
    }
}
