<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Level;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Level extends Command
{
    public $item        = 'level';
    public $arguments   = [];

    public function __construct ( int $level )
    {
        $this->arguments = [$level];
    }
}
