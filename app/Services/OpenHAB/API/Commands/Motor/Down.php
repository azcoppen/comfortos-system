<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Motor;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Down extends Command
{
  public $item        = 'dir';
  public $command     = 'Reverse';
}
