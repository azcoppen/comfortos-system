<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Motor;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Position extends Command
{
    public $item        = 'position';
    public $arguments   = [];

    public function __construct ( int $position )
    {
        $this->arguments = [$position];
    }
}
