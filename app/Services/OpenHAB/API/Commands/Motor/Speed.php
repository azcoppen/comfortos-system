<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Motor;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Speed extends Command
{
    public $item        = 'speed';
    public $arguments   = [];

    public function __construct ( int $rpms )
    {
        $this->arguments = [$rpms];
    }
}
