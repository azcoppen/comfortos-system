<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Motor;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Up extends Command
{
  public $item        = 'dir';
  public $command     = 'Forward';
}
