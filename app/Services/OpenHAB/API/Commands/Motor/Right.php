<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Motor;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Right extends Command
{
  public $item        = 'dir';
  public $command     = 'Right';
}
