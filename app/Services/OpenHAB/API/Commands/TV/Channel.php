<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\TV;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Channel extends Command
{
  public $item        = 'channel';
  public $arguments   = [];

  public function __construct ( string $channel )
  {
      $this->arguments = [$channel];
  }
}
