<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\TV;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Keycode extends Command
{
  public $item        = 'keycode';
  public $arguments   = [];

  public function __construct ( string $key )
  {
      $this->arguments = [$key];
  }
}
