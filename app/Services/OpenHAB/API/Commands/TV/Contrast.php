<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\TV;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Contrast extends Command
{
    public $item        = 'brightness';
    public $arguments   = [];

    public function __construct ( int $percent )
    {
        $this->arguments = [$percent];
    }
}
