<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\TV;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Sharpness extends Command
{
    public $item        = 'sharpness';
    public $arguments   = [];

    public function __construct ( int $percent )
    {
        $this->arguments = [$percent];
    }
}
