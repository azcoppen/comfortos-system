<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Lock;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Wipe extends Command
{
    public $item        = 'code-removals';
    public $arguments   = [];

    public function __construct ( int $slot )
    {
        $this->arguments = [$slot];
    }
}
