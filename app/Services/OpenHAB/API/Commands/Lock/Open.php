<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Lock;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Open extends Command
{
  public $item        = 'lock';
  public $command     = 'OFF';
}
