<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Lock;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Insert extends Command
{
    public $item        = 'code-inserts';
    public $arguments   = [];

    public function __construct ( int $slot, string $code, ?string $name = null)
    {
        $this->arguments = [$slot, $code, $name];
    }
}
