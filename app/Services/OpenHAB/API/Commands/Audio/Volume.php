<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Audio;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Volume extends Command
{
    public $item        = 'volume';
    public $arguments   = [];

    public function __construct ( int $percent )
    {
        $this->arguments = [$percent];
    }
}
