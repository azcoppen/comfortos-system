<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Audio;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Preset extends Command
{
    public $item        = 'preset';
    public $arguments   = [];

    public function __construct ( int $preset )
    {
        $this->arguments = [$preset];
    }
}
