<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Audio;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Unmute extends Command
{
  public $item        = 'mute';
  public $command     = 'OFF';
}
