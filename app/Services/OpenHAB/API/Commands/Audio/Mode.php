<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Audio;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Mode extends Command
{
    public $item        = 'mode';
    public $arguments   = [];

    public function __construct ( string $mode )
    {
        $this->arguments = [$mode];
    }
}
