<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Color;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Temperature extends Command
{
    public $item        = 'temperature';
    public $arguments   = [];

    public function __construct ( int $percent )
    {
        $this->arguments = [$percent];
    }
}
