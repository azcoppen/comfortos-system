<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Color;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class HSB extends Command
{
    public $item        = 'hsb';
    public $arguments   = [];

    public function __construct ( array $hsb )
    {
        $this->arguments = $hsb;
    }
}
