<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Color;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Brightness extends Command
{
    public $item        = 'brightness';
    public $arguments   = [];

    public function __construct ( int $percent )
    {
        $this->arguments = [$percent];
    }
}
