<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Playback;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Pause extends Command
{
  public $item        = 'player';
  public $command     = 'PAUSE';
}
