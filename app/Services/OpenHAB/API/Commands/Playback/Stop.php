<?php

namespace SmartRooms\Services\OpenHAB\API\Commands\Playback;

use SmartRooms\Services\OpenHAB\API\Requests\Command;

class Stop extends Command
{
  public $item        = 'stop';
  public $command     = 'ON';
}
