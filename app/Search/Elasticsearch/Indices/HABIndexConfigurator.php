<?php

namespace SmartRooms\Search\Elasticsearch\Indices;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class HABIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    public function __construct ()
    {
      $this->name = env('APP_ENV').'_habs';
    }

    /**
     * @var array
     */
    protected $settings = [
      'analysis' => [
        'analyzer' => [
          'partial_match_analyzer' => [
            'tokenizer' => 'standard',
            'filter' => ['ngram_filter', 'lowercase'],
          ],
        ],
        'filter' => [
          'ngram_filter' => [
            'type' => 'nGram',
            'min_gram' => 3,
            'max_gram' => 3,
          ],
        ],
      ],
    ];
}
