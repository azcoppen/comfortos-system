<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait HABSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\HABIndexConfigurator::class;

  public $setup = [
    'property_id'=> 'partial_keyword',
    'building_id'=> 'partial_keyword',
    'room_id'    => 'partial_keyword',
    'router_id'  => 'partial_keyword',
    'label'      => 'partial_text',
    'os'         => 'partial_text',
    'release'    => 'partial_text',
    'codename'   => 'partial_text',
    'kernel'     => 'partial_text',
    'model'      => 'partial_text',
    'serial'     => 'partial_text',
    'engine'     => 'partial_text',
    'dist'       => 'partial_text',
    'http_port'  => 'partial_keyword',
    'https_port' => 'partial_keyword',
    'http_user'  => 'partial_keyword',
    'hardware'   => 'partial_text',
    'mac'        => 'partial_text',
    'host'       => 'partial_text',
    'vpn_ip'     => 'ip',

    'canonical'  => 'partial_text',
    'description'=> 'partial_text',
    'property'   => 'partial_text',
    'building'   => 'partial_text',
    'location'   => 'partial_text',
    'geo'        => 'geo_shape',
    'order_id'   => 'partial_text',
    'tags'       => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'property_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'building_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'room_id'    => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'router_id'  => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'os'         => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'release'    => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'codename'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'kernel'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'model'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'serial'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'engine'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'dist'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'http_port'  => [
            'type' => 'integer',
        ],
        'https_port' => [
            'type' => 'integer',
        ],
        'http_user'  => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'hardware'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'mac'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'host'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'vpn_ip'     => [
            'type' => 'ip',
            'ignore_malformed' => true,
        ],

        'canonical'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'description'=> [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'property'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'building'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'location'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'geo'        => [
            'type' => 'geo_shape',
        ],
        'order_id'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'tags'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    $this->load (['room.building.property',]);

    return [
      'property_id' => $this->room->building->property->_id ?? null,
      'building_id' => $this->room->building->_id ?? null,
      'room_id'     => $this->room->_id ?? null,
      'router_id'   => $this->router_id,
      'label'       => $this->label,
      'os'          => $this->os,
      'release'     => $this->release,
      'codename'    => $this->codename,
      'kernel'      => $this->kernel,
      'model'       => $this->model,
      'serial'      => $this->serial,
      'engine'      => $this->engine,
      'dist'        => $this->dist,
      'http_port'   => $this->http_port,
      'https_port'  => $this->https_port,
      'http_user'   => $this->http_user,
      'hardware'    => $this->hardware,
      'mac'         => $this->mac,
      'host'        => $this->host,
      'vpn_ip'      => $this->vpn_ip,

      'canonical'   => collect ([$this->label, 'Room '.($this->room->number ?? null), 'Floor '.($this->room->floor ?? null), ($this->room->building->label ?? null), ($this->room->building->property->label ?? null), collect ($this->room->building->property->location ?? [])->implode (', ')])->implode (', '),
      'description' => $this->description,
      'property'    => $this->room->building->property->label ?? null,
      'building'    => $this->room->building->label ?? null,
      'location'    => collect ($this->room->building->property->location ?? [])->implode (', '),
      'geo'         => $this->room->geo ?? null,
      'order_id'    => $this->order_id,
      'tags'        => collect ($this->room->tags ?? [])->implode (', '),
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
