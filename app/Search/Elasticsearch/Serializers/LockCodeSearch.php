<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait LockCodeSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\LockCodeIndexConfigurator::class;

  public $setup = [
    'property_id'=> 'partial_keyword',
    'building_id'=> 'partial_keyword',
    'room_id'    => 'partial_keyword',
    'lock_id'    => 'partial_keyword',
    'index'      => 'partial_keyword',
    'label'      => 'partial_text',
    'canonical'  => 'partial_text',
    'description'=> 'partial_text',
    'property'   => 'partial_text',
    'building'   => 'partial_text',
    'location'   => 'partial_text',
    'geo'        => 'geo_shape',
    'tags'       => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'property_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'building_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'room_id'    => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'lock_id'    => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'index'      => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'canonical'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'description'=> [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'property'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'building'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'location'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'geo'        => [
            'type' => 'geo_shape',
        ],
        'order_id'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'tags'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    $this->load (['lock.room.building.property',]);

    return [
      'property_id' => $this->lock->room->building->property->_id ?? null,
      'building_id' => $this->lock->room->building->_id ?? null,
      'room_id'     => $this->lock->room->_id ?? null,
      'lock_id'     => $this->lock->_id ?? null,
      'index'       => $this->index,
      'label'       => $this->label,
      'canonical'   => collect ([$this->label ?? null, ($this->lock->label ?? null), 'Room '.($this->lock->room->number ?? null), 'Floor '.($this->lock->room->floor ?? null), ($this->lock->room->building->label ?? null), ($this->lock->room->building->property->label ?? null), collect ($this->lock->room->building->property->location ?? [])->implode (', ')])->implode (', '),
      'description' => $this->description,
      'property'    => $this->lock->room->building->property->label ?? null,
      'building'    => $this->lock->room->building->label ?? null,
      'location'    => collect ($this->lock->room->building->property->location ?? [])->implode (', '),
      'geo'         => $this->lock->room->geo ?? null,
      'tags'        => collect ($this->lock->room->tags ?? [])->implode (', '),
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
