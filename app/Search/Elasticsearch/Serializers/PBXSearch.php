<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait PBXSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\PBXIndexConfigurator::class;

  public $setup = [
    'type'            => 'partial_keyword',
    'label'           => 'partial_text',
    'v'               => 'partial_keyword',
    'country'         => 'partial_keyword',
    'license'         => 'partial_text',
    'host'            => 'partial_text',
    'ip'              => 'ip',
    'trunk_vendor'    => 'partial_text',
    'trunk_number'    => 'partial_text',
    'admin_url'       => 'partial_text',
    'admin_port'      => 'partial_keyword',
    'admin_user'      => 'partial_text',
    'sip_port'        => 'partial_keyword',
    'secure_sip_port' => 'partial_keyword',
    'vpn_ip'          => 'ip',
    'order_id'        => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'type'            => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'           => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'v'               => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'country'         => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'license'         => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'host'            => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'server_ip'      => [
          'type' => 'ip',
          'ignore_malformed' => true,
        ],
        'trunk_vendor'    => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'trunk_number'    => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'admin_url'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'admin_port'      => [
          'type' => 'integer',
        ],
        'admin_user'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'sip_port'        => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'secure_sip_port' => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'vpn_ip'          => [
          'type' => 'ip',
          'ignore_malformed' => true,
        ],
        'order_id'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    return [
      'type'            => $this->type,
      'label'           => $this->label,
      'v'               => $this->v,
      'country'         => $this->country,
      'license'         => $this->license,
      'host'            => $this->host,
      'server_ip'       => $this->ip ?? null,
      'trunk_vendor'    => $this->trunk_vendor,
      'trunk_number'    => $this->trunk_number,
      'admin_url'       => $this->admin_url,
      'admin_port'      => $this->admin_port,
      'admin_user'      => $this->admin_user,
      'sip_port'        => $this->sip_port,
      'secure_sip_port' => $this->secure_sip_port,
      'vpn_ip'          => $this->vpn_ip,
      'order_id'        => $this->order_id,
      'created_at'      => $this->created_at->format('c'),
      'updated_at'      => $this->updated_at->format('c'),
    ];
  }

}
