<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait WSBrokerSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\WSBrokerIndexConfigurator::class;

  public $setup = [
    'type'          => 'partial_keyword',
    'v'             => 'partial_keyword',
    'region'        => 'partial_keyword',
    'label'         => 'partial_text',
    'context'       => 'partial_keyword',
    'host'          => 'partial_keyword',
    'http_port'     => 'integer',
    'admin_port'    => 'integer',
    'conn_endpoint' => 'partial_keyword',
    'api_endpoint'  => 'partial_keyword',
    'auth_type'     => 'partial_keyword',
    'vpn_ip'        => 'ip',
    'created_at'    => 'date',
    'updated_at'    => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'type'          => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'v'             => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'region'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'         => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'context'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'host'          => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'ws_port'     => [
            'type' => 'integer',
        ],
        'admin_port'    => [
            'type' => 'integer',
        ],
        'conn_endpoint' => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'api_endpoint'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'auth_type'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'vpn_ip' => [
            'type' => 'ip',
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    return [
      'type'          => $this->type,
      'v'             => $this->v,
      'region'        => $this->region,
      'label'         => $this->label,
      'context'       => $this->context,
      'host'          => $this->host,
      'ws_port'       => floatval ($this->ws_port),
      'admin_port'    => floatval ($this->admin_port),
      'conn_endpoint' => $this->conn_endpoint,
      'api_endpoint'  => $this->api_endpoint,
      'auth_type'     => $this->auth_type,
      'vpn_ip'        => $this->vpn_ip,
      'created_at'    => $this->created_at->format('c'),
      'updated_at'    => $this->updated_at->format('c'),
    ];
  }

}
