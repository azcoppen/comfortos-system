<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;

trait BrandSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\BrandIndexConfigurator::class;

  public $setup = [
    'operator_id'=> 'partial_keyword',
    'label'      => 'partial_text',
    'slug'       => 'partial_keyword',
    'operator'   => 'partial_text',
    'properties' => 'partial_text',
    'tags'       => 'partial_keyword',
    'users'      => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'operator_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'brand_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'slug'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'operator'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'properties' => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'tags'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'users'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    $this->load (['properties', 'operator.users']);

    return [
      'operator_id' => $this->operator->_id ?? null,
      'brand_id'    => $this->_id,
      'label'       => $this->label,
      'slug'        => $this->slug,
      'operator'    => $this->operator->label ?? null,
      'properties'  => $this->properties->pluck('label')->implode (', '),
      'tags'        => collect ($this->tags ?? [])->implode (', '),
      'users'       => isset ($this->operator->users) ? $this->operator->users->pluck('full_name')->implode (', ') : null,
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
