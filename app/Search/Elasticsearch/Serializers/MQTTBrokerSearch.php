<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait MQTTBrokerSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\MQTTBrokerIndexConfigurator::class;

  public $setup = [
    'type'          => 'partial_keyword',
    'v'             => 'partial_keyword',
    'region'        => 'partial_keyword',
    'label'         => 'partial_text',
    'context'       => 'partial_keyword',
    'host'          => 'partial_text',
    'insecure_port' => 'integer',
    'secure_port'   => 'integer',
    'admin_user'    => 'partial_keyword',
    'admin_port'    => 'integer',
    'app_user'      => 'partial_keyword',
    'vpn_ip'        => 'ip',
    'created_at'    => 'date',
    'updated_at'    => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'type'        => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'v'             => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'region'        => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'         => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'context'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'host'          => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'insecure_port' => [
          'type' => 'integer',
        ],
        'secure_port'   => [
          'type' => 'integer',
        ],
        'admin_user'    => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'admin_port'    => [
          'type' => 'integer',
        ],
        'app_user'      => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'vpn_ip' => [
            'type' => 'ip',
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    return [
      'type'          => $this->type,
      'v'             => $this->v,
      'region'        => $this->region,
      'label'         => $this->label,
      'host'          => $this->host,
      'insecure_port' => floatval ($this->insecure_port),
      'secure_port'   => floatval ($this->secure_port),
      'admin_user'    => $this->admin_user,
      'admin_port'    => floatval ($this->admin_port),
      'app_user'      => $this->app_user,
      'vpn_ip'        => $this->vpn_ip,
      'created_at'    => $this->created_at->format('c'),
      'updated_at'    => $this->updated_at->format('c'),
    ];
  }

}
