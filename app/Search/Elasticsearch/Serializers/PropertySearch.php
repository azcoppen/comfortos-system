<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;

trait PropertySearch
{
  use Searchable;

  protected $indexConfigurator = Indices\PropertyIndexConfigurator::class;

  public $setup = [
    'operator_id'=> 'partial_keyword',
    'brand_id'   => 'partial_keyword',
    'label'      => 'partial_text',
    'slug'       => 'partial_keyword',
    'brand'      => 'partial_text',
    'operator'   => 'partial_text',
    'buildings'  => 'partial_text',
    'location'   => 'partial_text',
    'geo'        => 'geo_shape',
    'telephones' => 'partial_text',
    'emails'     => 'partial_text',
    'tags'       => 'partial_keyword',
    'users'      => 'partial_keyword',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'operator_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'brand_id'   => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'property_id'   => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'slug'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'brand'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'operator'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'buildings'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'location'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'geo'        => [
          'type' => 'geo_shape',
        ],
        'telephones' => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'emails'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'tags'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'users'      => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    $this->load (['brand.operator.users', 'buildings']);

    return [
      'operator_id' => $this->brand->operator->_id ?? null,
      'brand_id'    => $this->brand->_id ?? null,
      'property_id' => $this->_id,
      'label'       => $this->label,
      'slug'        => $this->slug,
      'brand'       => $this->brand->label ?? null,
      'operator'    => $this->brand->operator->label ?? null,
      'buildings'   => isset ($this->buildings) ? $this->buildings->pluck('label')->implode (', ') : null,
      'location'    => collect ($this->location)->implode (', '),
      'geo'         => $this->geo,
      'telephones'  => collect ($this->telephones)->implode (', '),
      'emails'      => collect ($this->emails)->implode (', '),
      'tags'        => collect ($this->tags)->implode (', '),
      'users'       => isset ($this->brand->operator->users) ? $this->brand->operator->users->pluck('full_name')->implode (', ') : null,
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
