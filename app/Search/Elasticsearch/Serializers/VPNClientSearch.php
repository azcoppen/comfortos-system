<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait VPNClientSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\VPNClientIndexConfigurator::class;

  public $setup = [
    'type'          => 'partial_keyword',
    'server'        => 'partial_text',
    'host'          => 'partial_text',
    'label'         => 'partial_text',
    'client_id'     => 'partial_keyword',
    'cipher'        => 'partial_keyword',
    'revoked_at'    => 'date',
    'created_at'    => 'date',
    'updated_at'    => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'type'        => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'server'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'host'          => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'         => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'client_id'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'cipher'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'revoked_at' => [
            'type' => 'date',
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    return [
      'type'          => $this->type ?? null,
      'server'        => $this->vpn_server->label ?? null,
      'host'          => $this->vpn_server->host ?? null,
      'label'         => $this->label ?? null,
      'client_id'     => $this->client_id ?? null,
      'cipher'        => $this->cipher ?? null,
      'created_at'    => is_object ($this->revoked_at) ? $this->revoked_at->format('c') : null,
      'created_at'    => $this->created_at->format('c'),
      'updated_at'    => $this->updated_at->format('c'),
    ];
  }

}
