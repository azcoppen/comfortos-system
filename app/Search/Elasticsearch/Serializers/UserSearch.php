<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait UserSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\UserIndexConfigurator::class;

  public $setup = [
    'user_id'     => 'partial_text',
    'prefix'      => 'partial_keyword',
    'first'       => 'partial_text',
    'last'        => 'partial_text',
    'suffix'      => 'partial_keyword',
    'display'     => 'partial_text',
    'full'        => 'partial_text',
    'slug'        => 'partial_text',
    'email'       => 'partial_text',
    'dob'         => 'date',
    'sex'         => 'partial_keyword',
    'job_title'   => 'partial_text',
    'location'    => 'partial_text',
    'geo'         => 'geo_shape',
    'telephones'  => 'partial_text',
    'tags'        => 'partial_text',
    'operators'   => 'partial_text',
    'roles'       => 'partial_text',
    'rooms'       => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [
        'user_id'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'prefix'      => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'first'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'last'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'suffix'      => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'display'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'full'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'slug'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'email'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'dob'         => [
          'type' => 'date',
        ],
        'sex'         => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'job_title'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'location'    => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'geo'         => [
          'type' => 'geo_shape',
        ],
        'telephones'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'tags'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'operators'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'roles'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'rooms'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {

    return [
      'user_id'     => $this->_id,
      'prefix'      => $this->prefix,
      'first'       => $this->first,
      'last'        => $this->last,
      'suffix'      => $this->suffix,
      'display'     => $this->display,
      'full'        => $this->prefix .' '.$this->first.' '.$this->last,
      'slug'        => $this->slug,
      'email'       => $this->email,
      'dob'         => is_object ($this->dob) ? $this->dob->format ('Y-m-d') : null,
      'sex'         => $this->sex == 'M' ? 'Male' : 'Female',
      'job_title'   => $this->job_title,
      'location'    => collect ($this->location)->implode (', '),
      'geo'         => $this->geo,
      'telephones'  => collect ($this->telephones)->implode (', '),
      'tags'        => collect ($this->tags)->implode (', '),
      'operators'   => isset ($this->operators) ? $this->operators->pluck('label')->implode (', ') : null,
      'roles'       => isset ($this->roles) ? $this->roles->pluck('name')->implode (', ') : null,
      'rooms'       => isset ($this->rooms) ? $this->rooms->pluck('label')->implode (', ') : null,
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
