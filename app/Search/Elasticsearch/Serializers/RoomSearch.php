<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;

trait RoomSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\RoomIndexConfigurator::class;

  public $setup = [
    'property_id'=> 'partial_keyword',
    'building_id'=> 'partial_keyword',
    'floor'      => 'integer',
    'number'     => 'integer',
    'type'       => 'partial_keyword',
    'grade'      => 'partial_keyword',
    'label'      => 'partial_text',
    'canonical'  => 'partial_text',
    'description'=> 'partial_text',
    'operator'   => 'partial_text',
    'brand'      => 'partial_text',
    'property'   => 'partial_text',
    'building'   => 'partial_text',
    'location'   => 'partial_text',
    'geo'        => 'geo_shape',
    'telephones' => 'partial_text',
    'emails'     => 'partial_text',
    'tags'       => 'partial_keyword',
    'op_users'   => 'partial_text',
    'users'      => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'property_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'building_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'room_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'floor'      => [
          'type' => 'integer',
        ],
        'number'     => [
          'type' => 'integer',
        ],
        'type'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'grade'      => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'canonical'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'description'=> [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'operator'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'brand'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'property'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'building'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'location'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'geo'        => [
          'type' => 'geo_shape',
        ],
        'telephones' => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'emails'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'tags'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'op_users'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'users'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    $this->load (['building.property.brand.operator.users', 'users']);

    return [
      'property_id' => $this->building->property->_id ?? null,
      'building_id' => $this->building->_id ?? null,
      'room_id'     => $this->_id,
      'floor'       => $this->floor,
      'number'      => $this->number,
      'label'       => $this->label,
      'canonical'   => collect (['Room '.$this->number, 'Floor '.$this->floor, ($this->building->label ?? null), ($this->building->property->label ?? null), collect ($this->building->property->location ?? [])->implode (', ')])->implode (', '),
      'type'        => $this->type,
      'grade'       => $this->grade,
      'description' => $this->description,
      'operator'    => $this->building->property->brand->operator->label ?? null,
      'brand'       => $this->building->property->brand->label ?? null,
      'property'    => $this->building->property->label ?? null,
      'building'    => $this->building->label ?? null,
      'location'    => collect ($this->building->property->location ?? [])->implode (', '),
      'geo'         => $this->geo,
      'telephones'  => collect ($this->building->property->telephones ?? [])->implode (', '),
      'emails'      => collect ($this->building->property->emails ?? [])->implode (', '),
      'tags'        => collect ($this->tags)->implode (', '),
      'op_users'    => isset ($this->building->property->brand->operator->users) ? $this->building->property->brand->operator->users->pluck('full_name')->implode (', ') : null,
      'users'       => isset ($this->users) ? $this->users->pluck('full_name')->implode (', ') : null,
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
