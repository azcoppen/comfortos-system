<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait VPNServerSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\VPNServerIndexConfigurator::class;

  public $setup = [
    'type'        => 'partial_keyword',
    'region'      => 'partial_keyword',
    'timezone'    => 'partial_keyword',
    'label'       => 'partial_text',
    'host'        => 'partial_text',
    'ip_range'    => 'partial_text',
    'subnet'      => 'partial_text',
    'cipher'      => 'partial_text',
    'order_id'    => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'type'        => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'region'      => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'timezone'    => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'host'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'ip_range'    => [
            'type' => 'ip',
            'ignore_malformed' => true,
        ],
        'subnet'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'cipher'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'order_id'    => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    return [
      'type'      => $this->type,
      'region'    => $this->region,
      'timezone'  => $this->timezone,
      'label'     => $this->label,
      'host'      => $this->host,
      'ip_range'  => $this->ip_range,
      'subnet'    => $this->subnet,
      'cipher'    => $this->cipher,
      'order_id'  => $this->order_id,
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
