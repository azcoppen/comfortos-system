<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;

trait BuildingSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\BuildingIndexConfigurator::class;

  public $setup = [
    'operator_id'=> 'partial_keyword',
    'brand_id'   => 'partial_keyword',
    'property_id'=> 'partial_keyword',
    'building_id'=> 'partial_keyword',
    'label'      => 'partial_text',
    'slug'       => 'partial_keyword',
    'canonical'  => 'partial_text',
    'operator'   => 'partial_text',
    'brand'      => 'partial_text',
    'property'   => 'partial_text',
    'location'   => 'partial_text',
    'geo'        => 'geo_shape',
    'telephones' => 'partial_text',
    'emails'     => 'partial_text',
    'tags'       => 'partial_keyword',
    'users'      => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'operator_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'brand_id'   => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'property_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'building_id'=> [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'slug'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'canonical'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'operator'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'brand'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'property'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'location'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'geo'        => [
            'type' => 'geo_shape',
        ],
        'telephones' => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'emails'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'tags'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'users'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    $this->load (['property.brand.operator.users',]);

    return [
      'operator_id' => $this->property->brand->operator->_id ?? null,
      'brand_id'    => $this->property->brand->_id ?? null,
      'property_id' => $this->property->_id ?? null,
      'building_id' => $this->_id,
      'label'       => $this->label,
      'slug'        => $this->slug,
      'canonical'   => collect ([$this->label, $this->property->label ?? null, collect ($this->property->location ?? [])->implode (', ')])->implode (', '),
      'operator'    => $this->property->brand->operator->label ?? null,
      'brand'       => $this->property->brand->label ?? null,
      'property'    => $this->property->label ?? null,
      'location'    => collect ($this->property->location ?? [])->implode (', '),
      'geo'         => $this->geo,
      'telephones'  => collect ($this->property->telephones ?? [])->implode (', '),
      'emails'      => collect ($this->property->emails ?? [])->implode (', '),
      'tags'        => collect ($this->tags ?? [])->implode (', '),
      'users'       => isset ($this->property->brand->operator->users) ? $this->property->brand->operator->users->pluck('full_name')->implode (', ') : null,
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
