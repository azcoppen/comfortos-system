<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait OpQueueSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\OpQueueIndexConfigurator::class;

  public $setup = [
    'src'           => 'partial_keyword',
    'domain'        => 'partial_keyword',
    'user_id'       => 'partial_text',
    'room_id'       => 'partial_text',
    'component_id'  => 'partial_text',
    'operation'     => 'partial_keyword',
    'type'          => 'partial_keyword',
    'label'         => 'partial_text',
    'canonical'     => 'partial_text',
    'description'   => 'partial_text',
    'property'      => 'partial_text',
    'building'      => 'partial_text',
    'location'      => 'partial_text',
    'geo'           => 'geo_shape',
    'order_id'      => 'partial_text',
    'tags'          => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'src'           => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'domain'        => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'user_id'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'room_id'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'component_id'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'operation'     => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'type'          => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'         => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'canonical'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'description'=> [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'property'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'building'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'location'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'geo'        => [
            'type' => 'geo_shape',
        ],
        'tags'       => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    $this->load (['room.building.property', 'component']);

    return [
      'src'           => $this->src,
      'domain'        => $this->domain,
      'user_id'       => $this->user_id,
      'room_id'       => $this->room_id,
      'component_id'  => $this->component_id,
      'operation'     => $this->operation,
      'type'          => $this->component->type ?? null,
      'label'         => $this->component->label ?? null,
      'canonical'     => collect ([$this->component->label ?? null, 'Room '.($this->room->number ?? null), 'Floor '.($this->room->floor ?? null), ($this->room->building->label ?? null), ($this->room->building->property->label ?? null), collect ($this->room->building->property->location ?? [])->implode (', ')])->implode (', '),
      'description'   => $this->description,
      'property'      => $this->room->building->property->label ?? null,
      'building'      => $this->room->building->label ?? null,
      'location'      => collect ($this->room->building->property->location ?? [])->implode (', '),
      'geo'           => $this->room->geo ?? null,
      'tags'          => collect ($this->room->tags ?? [])->implode (', '),
      'created_at'    => $this->created_at->format('c'),
      'updated_at'    => $this->updated_at->format('c'),
    ];
  }

}
