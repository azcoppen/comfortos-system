<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;
use Illuminate\Support\Str;

trait MirrorModuleSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\MirrorModuleIndexConfigurator::class;

  public $setup = [
    'module'      => 'partial_keyword',
    'repository'  => 'partial_text',
    'description' => 'partial_text',
    'position'    => 'partial_keyword',
    'tags'        => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'module'      => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'repository'  => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'description' => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'position'    => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'tags'        => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    return [
      'module'      => $this->module,
      'repository'  => $this->repository,
      'description' => $this->description,
      'position'    => $this->position,
      'tags'        => collect ($this->tags)->implode (', '),
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
