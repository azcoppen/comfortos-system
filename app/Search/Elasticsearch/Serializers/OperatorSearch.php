<?php

namespace SmartRooms\Search\Elasticsearch\Serializers;

use SmartRooms\Search\Elasticsearch\Serializer;
use ScoutElastic\Searchable;
use SmartRooms\Search\Elasticsearch\Indices;

trait OperatorSearch
{
  use Searchable;

  protected $indexConfigurator = Indices\OperatorIndexConfigurator::class;

  public $setup = [
    'operator_id'=> 'partial_text',
    'label'      => 'partial_text',
    'slug'       => 'partial_keyword',
    'brands'     => 'partial_text',
    'properties' => 'partial_text',
    'location'   => 'partial_text',
    'geo'        => 'geo_shape',
    'telephones' => 'partial_text',
    'users'      => 'partial_text',
    'emails'     => 'partial_text',
    'created_at' => 'date',
    'updated_at' => 'date',
  ];

  protected $mapping = [
      'properties' => [

        'operator_id'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'label'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'slug'       => [
            'type' => 'keyword',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'brands'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'properties' => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'location'   => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'geo'        => [
          'type' => 'geo_shape',
        ],
        'telephones' => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'users'      => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'emails'     => [
            'type' => 'text',
            'fields' => [
              'analyzed' => [
                'type' => 'text',
                'analyzer' => 'partial_match_analyzer',
              ],
            ],
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'updated_at' => [
            'type' => 'date',
        ],

      ], // end properties
  ];

  public function toSearchableArray ()
  {
    $this->load (['brands.properties', 'users']);

    $properties = collect([]);
    foreach ($this->brands AS $brand)
    {
      $properties->push ($brand->properties->pluck('label'));
    }

    return [
      'operator_id' => $this->_id,
      'label'       => $this->label,
      'slug'        => $this->slug,
      'brands'      => $this->brands->pluck('label')->implode (', '),
      'properties'  => $properties->flatten()->implode (', '),
      'location'    => collect ($this->location)->implode (', '),
      'geo'         => $this->geo,
      'telephones'  => collect ($this->telephones)->implode (', '),
      'emails'      => collect ($this->emails)->implode (', '),
      'users'       => isset ($this->users) ? $this->users->pluck('full_name')->implode (', ') : null,
      'created_at'  => $this->created_at->format('c'),
      'updated_at'  => $this->updated_at->format('c'),
    ];
  }

}
