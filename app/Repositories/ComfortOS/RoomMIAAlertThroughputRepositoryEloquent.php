<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomMIAAlertThroughputRepository;
use SmartRooms\Models\ComfortOS\RoomMIAAlertThroughput;
use SmartRooms\Validators\ComfortOS\RoomMIAAlertThroughputValidator;

/**
 * Class RoomMIAAlertThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class RoomMIAAlertThroughputRepositoryEloquent extends BaseRepository implements RoomMIAAlertThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoomMIAAlertThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
