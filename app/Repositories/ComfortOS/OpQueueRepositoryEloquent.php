<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\OpQueueRepository;
use SmartRooms\Models\ComfortOS\OpQueue;
use SmartRooms\Validators\UserValidator;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class UserRepositoryEloquent.
 */
class OpQueueRepositoryEloquent extends BaseRepository implements OpQueueRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return OpQueue::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
