<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABSignalThroughputHourlyRepository;
use SmartRooms\Models\ComfortOS\ComponentHABSignalThroughputHourly;
use SmartRooms\Validators\ComfortOS\ComponentHABSignalThroughputHourlyValidator;

/**
 * Class ComponentHABSignalThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class ComponentHABSignalThroughputHourlyRepositoryEloquent extends BaseRepository implements ComponentHABSignalThroughputHourlyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ComponentHABSignalThroughputHourly::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
