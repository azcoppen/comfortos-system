<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandThroughputHourlyRepository;
use SmartRooms\Models\ComfortOS\HABCommandThroughputHourly;
use SmartRooms\Validators\ComfortOS\HABCommandThroughputHourlyValidator;

/**
 * Class HABCommandThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class HABCommandThroughputHourlyRepositoryEloquent extends BaseRepository implements HABCommandThroughputHourlyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HABCommandThroughputHourly::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
