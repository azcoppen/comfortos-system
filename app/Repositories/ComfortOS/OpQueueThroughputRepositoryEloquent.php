<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\OpQueueThroughputRepository;
use SmartRooms\Models\ComfortOS\OpQueueThroughput;
use SmartRooms\Validators\ComfortOS\OpQueueThroughputValidator;

/**
 * Class OpQueueThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class OpQueueThroughputRepositoryEloquent extends BaseRepository implements OpQueueThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OpQueueThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
