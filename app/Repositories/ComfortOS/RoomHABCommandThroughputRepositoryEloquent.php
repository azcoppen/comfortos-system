<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABCommandThroughputRepository;
use SmartRooms\Models\ComfortOS\RoomHABCommandThroughput;
use SmartRooms\Validators\ComfortOS\RoomHABCommandThroughputValidator;

/**
 * Class RoomHABCommandThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class RoomHABCommandThroughputRepositoryEloquent extends BaseRepository implements RoomHABCommandThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoomHABCommandThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
