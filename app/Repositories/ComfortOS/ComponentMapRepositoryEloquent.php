<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentMapRepository;
use SmartRooms\Models\ComfortOS\ComponentMap;
use SmartRooms\Validators\ComfortOS\ComponentMapValidator;

/**
 * Class ComponentMapRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class ComponentMapRepositoryEloquent extends BaseRepository implements ComponentMapRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ComponentMap::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
