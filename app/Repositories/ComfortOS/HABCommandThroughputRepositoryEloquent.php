<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandThroughputRepository;
use SmartRooms\Models\ComfortOS\HABCommandThroughput;
use SmartRooms\Validators\ComfortOS\HABCommandThroughputValidator;

/**
 * Class HABCommandThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class HABCommandThroughputRepositoryEloquent extends BaseRepository implements HABCommandThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HABCommandThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
