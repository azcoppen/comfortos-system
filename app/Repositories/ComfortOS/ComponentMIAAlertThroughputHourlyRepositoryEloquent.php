<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentMIAAlertThroughputHourlyRepository;
use SmartRooms\Models\ComfortOS\ComponentMIAAlertThroughputHourly;
use SmartRooms\Validators\ComfortOS\ComponentMIAAlertThroughputHourlyValidator;

/**
 * Class ComponentMIAAlertThroughputHourlyRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class ComponentMIAAlertThroughputHourlyRepositoryEloquent extends BaseRepository implements ComponentMIAAlertThroughputHourlyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ComponentMIAAlertThroughputHourly::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
