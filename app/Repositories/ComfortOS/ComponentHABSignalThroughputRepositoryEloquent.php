<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABSignalThroughputRepository;
use SmartRooms\Models\ComfortOS\ComponentHABSignalThroughput;
use SmartRooms\Validators\ComfortOS\ComponentHABSignalThroughputValidator;

/**
 * Class ComponentHABSignalThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class ComponentHABSignalThroughputRepositoryEloquent extends BaseRepository implements ComponentHABSignalThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ComponentHABSignalThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
