<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomMIAAlertThroughputHourlyRepository;
use SmartRooms\Models\ComfortOS\RoomMIAAlertThroughputHourly;
use SmartRooms\Validators\ComfortOS\RoomMIAAlertThroughputHourlyValidator;

/**
 * Class RoomMIAAlertThroughputHourlyRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class RoomMIAAlertThroughputHourlyRepositoryEloquent extends BaseRepository implements RoomMIAAlertThroughputHourlyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoomMIAAlertThroughputHourly::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
