<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABCommandThroughputHourlyRepository;
use SmartRooms\Models\ComfortOS\ComponentHABCommandThroughputHourly;
use SmartRooms\Validators\ComfortOS\ComponentHABCommandThroughputHourlyValidator;

/**
 * Class ComponentHABCommandThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class ComponentHABCommandThroughputHourlyRepositoryEloquent extends BaseRepository implements ComponentHABCommandThroughputHourlyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ComponentHABCommandThroughputHourly::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
