<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABSignalThroughputRepository;
use SmartRooms\Models\ComfortOS\RoomHABSignalThroughput;
use SmartRooms\Validators\ComfortOS\RoomHABSignalThroughputValidator;

/**
 * Class RoomHABSignalThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class RoomHABSignalThroughputRepositoryEloquent extends BaseRepository implements RoomHABSignalThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoomHABSignalThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
