<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertThroughputRepository;
use SmartRooms\Models\ComfortOS\MIAAlertThroughput;
use SmartRooms\Validators\ComfortOS\MIAAlertThroughputValidator;

/**
 * Class MIAAlertThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class MIAAlertThroughputRepositoryEloquent extends BaseRepository implements MIAAlertThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MIAAlertThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
