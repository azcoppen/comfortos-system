<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertRepository;
use SmartRooms\Models\ComfortOS\MIAAlert;
use SmartRooms\Validators\ComfortOS\MIAAlertValidator;

/**
 * Class MIAAlertRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class MIAAlertRepositoryEloquent extends BaseRepository implements MIAAlertRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MIAAlert::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
