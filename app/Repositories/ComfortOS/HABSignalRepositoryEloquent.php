<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Models\ComfortOS\HABSignal;
use SmartRooms\Validators\ComfortOS\HABSignalValidator;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class HABSignalRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class HABSignalRepositoryEloquent extends BaseRepository implements HABSignalRepository, CacheableInterface
{
    use CacheableRepository;
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HABSignal::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
