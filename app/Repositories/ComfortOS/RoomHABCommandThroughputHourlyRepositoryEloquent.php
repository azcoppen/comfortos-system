<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABCommandThroughputHourlyRepository;
use SmartRooms\Models\ComfortOS\RoomHABCommandThroughputHourly;
use SmartRooms\Validators\ComfortOS\RoomHABCommandThroughputHourlyValidator;

/**
 * Class RoomHABCommandThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class RoomHABCommandThroughputHourlyRepositoryEloquent extends BaseRepository implements RoomHABCommandThroughputHourlyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoomHABCommandThroughputHourly::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
