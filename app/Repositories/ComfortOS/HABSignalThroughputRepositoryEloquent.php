<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalThroughputRepository;
use SmartRooms\Models\ComfortOS\HABSignalThroughput;
use SmartRooms\Validators\ComfortOS\HABSignalThroughputValidator;

/**
 * Class HABSignalThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class HABSignalThroughputRepositoryEloquent extends BaseRepository implements HABSignalThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HABSignalThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
