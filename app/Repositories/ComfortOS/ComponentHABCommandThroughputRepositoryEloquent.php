<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentHABCommandThroughputRepository;
use SmartRooms\Models\ComfortOS\ComponentHABCommandThroughput;
use SmartRooms\Validators\ComfortOS\ComponentHABCommandThroughputValidator;

/**
 * Class ComponentHABCommandThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class ComponentHABCommandThroughputRepositoryEloquent extends BaseRepository implements ComponentHABCommandThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ComponentHABCommandThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
