<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\ComponentMIAAlertThroughputRepository;
use SmartRooms\Models\ComfortOS\ComponentMIAAlertThroughput;
use SmartRooms\Validators\ComfortOS\ComponentMIAAlertThroughputValidator;

/**
 * Class ComponentMIAAlertThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class ComponentMIAAlertThroughputRepositoryEloquent extends BaseRepository implements ComponentMIAAlertThroughputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ComponentMIAAlertThroughput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
