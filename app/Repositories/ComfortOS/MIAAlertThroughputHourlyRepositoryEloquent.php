<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertThroughputHourlyRepository;
use SmartRooms\Models\ComfortOS\MIAAlertThroughputHourly;
use SmartRooms\Validators\ComfortOS\MIAAlertThroughputHourlyValidator;

/**
 * Class MIAAlertThroughputHourlyRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class MIAAlertThroughputHourlyRepositoryEloquent extends BaseRepository implements MIAAlertThroughputHourlyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MIAAlertThroughputHourly::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
