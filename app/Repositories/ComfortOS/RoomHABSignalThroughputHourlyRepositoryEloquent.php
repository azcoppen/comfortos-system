<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\RoomHABSignalThroughputHourlyRepository;
use SmartRooms\Models\ComfortOS\RoomHABSignalThroughputHourly;
use SmartRooms\Validators\ComfortOS\RoomHABSignalThroughputHourlyValidator;

/**
 * Class RoomHABSignalThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class RoomHABSignalThroughputHourlyRepositoryEloquent extends BaseRepository implements RoomHABSignalThroughputHourlyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoomHABSignalThroughputHourly::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
