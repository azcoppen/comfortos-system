<?php

namespace SmartRooms\Repositories\ComfortOS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalThroughputHourlyRepository;
use SmartRooms\Models\ComfortOS\HABSignalThroughputHourly;
use SmartRooms\Validators\ComfortOS\HABSignalThroughputHourlyValidator;

/**
 * Class HABSignalThroughputRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\ComfortOS;
 */
class HABSignalThroughputHourlyRepositoryEloquent extends BaseRepository implements HABSignalThroughputHourlyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HABSignalThroughputHourly::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
