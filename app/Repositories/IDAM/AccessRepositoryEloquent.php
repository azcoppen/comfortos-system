<?php

namespace SmartRooms\Repositories\IDAM;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\IDAM\AccessRepository;
use SmartRooms\Models\IDAM\Access;
use SmartRooms\Validators\AccessValidator;

/**
 * Class UserRepositoryEloquent.
 */
class AccessRepositoryEloquent extends BaseRepository implements AccessRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Access::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
