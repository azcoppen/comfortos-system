<?php

namespace SmartRooms\Repositories\IDAM;

use Maklad\Permission\Models\Role;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\IDAM\RoleRepository;
use SmartRooms\Validators\RoleValidator;

/**
 * Class UserRepositoryEloquent.
 */
class RoleRepositoryEloquent extends BaseRepository implements RoleRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
