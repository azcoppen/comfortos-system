<?php

namespace SmartRooms\Repositories\Property;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Models\Property\Room;
use SmartRooms\Validators\Property\RoomValidator;

/**
 * Class RoomRepositoryEloquent.
 */
class RoomRepositoryEloquent extends BaseRepository implements RoomRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Room::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
