<?php

namespace SmartRooms\Repositories\Property;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Property\BuildingRepository;
use SmartRooms\Models\Property\Building;
use SmartRooms\Validators\Property\BuildingValidator;

/**
 * Class BuildingRepositoryEloquent.
 */
class BuildingRepositoryEloquent extends BaseRepository implements BuildingRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Building::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
