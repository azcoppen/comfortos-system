<?php

namespace SmartRooms\Repositories\Property;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Property\ConfigurationRepository;
use SmartRooms\Models\Property\Configuration;
use SmartRooms\Validators\Property\ConfigurationValidator;

/**
 * Class ConfigurationRepositoryEloquent.
 */
class ConfigurationRepositoryEloquent extends BaseRepository implements ConfigurationRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Configuration::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
