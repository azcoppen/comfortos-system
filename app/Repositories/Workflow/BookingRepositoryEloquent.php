<?php

namespace SmartRooms\Repositories\Workflow;

use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use SmartRooms\Contracts\Workflow\BookingRepository;
use SmartRooms\Models\Workflow\Booking;
use SmartRooms\Validators\Workflow\BookingValidator;

/**
 * Class BookingRepositoryEloquent.
 */
class BookingRepositoryEloquent extends BaseRepository implements BookingRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Booking::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
