<?php

namespace SmartRooms\Repositories\Workflow;

use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use SmartRooms\Contracts\Workflow\OccupancyRepository;
use SmartRooms\Models\Workflow\Occupancy;
use SmartRooms\Validators\Workflow\OccupancyValidator;

/**
 * Class OccupancyRepositoryEloquent.
 */
class OccupancyRepositoryEloquent extends BaseRepository implements OccupancyRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Occupancy::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
