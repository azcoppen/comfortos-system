<?php

namespace SmartRooms\Repositories\Workflow;

use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use SmartRooms\Contracts\Workflow\CheckinRepository;
use SmartRooms\Models\Workflow\Checkin;
use SmartRooms\Validators\Workflow\CheckinValidator;

/**
 * Class CheckinRepositoryEloquent.
 */
class CheckinRepositoryEloquent extends BaseRepository implements CheckinRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Checkin::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
