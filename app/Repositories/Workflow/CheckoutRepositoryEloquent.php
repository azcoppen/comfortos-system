<?php

namespace SmartRooms\Repositories\Workflow;

use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use SmartRooms\Contracts\Workflow\CheckoutRepository;
use SmartRooms\Models\Workflow\Checkout;
use SmartRooms\Validators\Workflow\CheckoutValidator;

/**
 * Class CheckoutRepositoryEloquent.
 */
class CheckoutRepositoryEloquent extends BaseRepository implements CheckoutRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Checkout::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
