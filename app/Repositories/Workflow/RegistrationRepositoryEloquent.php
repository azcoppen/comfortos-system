<?php

namespace SmartRooms\Repositories\Workflow;

use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use SmartRooms\Contracts\Workflow\RegistrationRepository;
use SmartRooms\Models\Workflow\Registration;
use SmartRooms\Validators\Workflow\RegistrationValidator;

/**
 * Class RegistrationRepositoryEloquent.
 */
class RegistrationRepositoryEloquent extends BaseRepository implements RegistrationRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Registration::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
