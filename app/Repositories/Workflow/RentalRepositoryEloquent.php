<?php

namespace SmartRooms\Repositories\Workflow;

use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use SmartRooms\Contracts\Workflow\RentalRepository;
use SmartRooms\Models\Workflow\Rental;
use SmartRooms\Validators\Workflow\RentalValidator;

/**
 * Class RentalRepositoryEloquent.
 */
class RentalRepositoryEloquent extends BaseRepository implements RentalRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Rental::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
