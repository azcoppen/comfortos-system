<?php

namespace SmartRooms\Repositories\Shared;

use Maklad\Permission\Models\Shared\Audit;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Shared\AuditRepository;
use SmartRooms\Validators\AuditValidator;

/**
 * Class UserRepositoryEloquent.
 */
class AuditRepositoryEloquent extends BaseRepository implements AuditRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Audit::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
