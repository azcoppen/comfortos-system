<?php

namespace SmartRooms\Repositories\Shared;

use Maklad\Permission\Models\Shared\Note;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Shared\NoteRepository;
use SmartRooms\Validators\NoteValidator;

/**
 * Class UserRepositoryEloquent.
 */
class NoteRepositoryEloquent extends BaseRepository implements NoteRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Note::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
