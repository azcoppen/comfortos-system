<?php

namespace SmartRooms\Repositories\Components\OS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\Components\OS\MQTTBrokerRepository;
use SmartRooms\Models\Components\OS\MQTTBroker;
use SmartRooms\Validators\Components\OS\MQTTBrokerValidator;

/**
 * Class MQTTBrokerRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\OS;
 */
class MQTTBrokerRepositoryEloquent extends BaseRepository implements MQTTBrokerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MQTTBroker::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
