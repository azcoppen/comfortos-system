<?php

namespace SmartRooms\Repositories\Components\OS;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\OS\HubRepository;
use SmartRooms\Models\Components\OS\Hub;
use SmartRooms\Validators\Components\OS\HubValidator;

/**
 * Class SmartHubRepositoryEloquent.
 */
class HubRepositoryEloquent extends BaseRepository implements HubRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Hub::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
