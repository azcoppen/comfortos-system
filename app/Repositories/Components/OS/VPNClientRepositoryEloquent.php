<?php

namespace SmartRooms\Repositories\Components\OS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Models\Components\OS\VPNClient;
use SmartRooms\Validators\Components\OS\VPNClientValidator;

/**
 * Class VPNClientRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\Components\OS;
 */
class VPNClientRepositoryEloquent extends BaseRepository implements VPNClientRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return VPNClient::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
