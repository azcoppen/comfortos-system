<?php

namespace SmartRooms\Repositories\Components\OS;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\OS\HABRepository;
use SmartRooms\Models\Components\OS\HAB;
use SmartRooms\Validators\Components\OS\HABValidator;

/**
 * Class ServerRepositoryEloquent.
 */
class HABRepositoryEloquent extends BaseRepository implements HABRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return HAB::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
