<?php

namespace SmartRooms\Repositories\Components\OS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\Components\OS\WSBrokerRepository;
use SmartRooms\Models\Components\OS\WSBroker;
use SmartRooms\Validators\Components\OS\WSBrokerValidator;

/**
 * Class WSBrokerRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\OS;
 */
class WSBrokerRepositoryEloquent extends BaseRepository implements WSBrokerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return WSBroker::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
