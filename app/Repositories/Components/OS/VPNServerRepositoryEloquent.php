<?php

namespace SmartRooms\Repositories\Components\OS;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Models\Components\OS\VPNServer;
use SmartRooms\Validators\Components\OS\VPNServerValidator;

/**
 * Class VPNRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\OS;
 */
class VPNServerRepositoryEloquent extends BaseRepository implements VPNServerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return VPNServer::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
