<?php

namespace SmartRooms\Repositories\Components\Telecoms;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\HandsetRepository;
use SmartRooms\Models\Components\Telecoms\Handset;
use SmartRooms\Validators\Components\Telecoms\HandsetValidator;

/**
 * Class HandsetRepositoryEloquent.
 */
class HandsetRepositoryEloquent extends BaseRepository implements HandsetRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Handset::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
