<?php

namespace SmartRooms\Repositories\Components\Telecoms;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\PBXRepository;
use SmartRooms\Models\Components\Telecoms\PBX;
use SmartRooms\Validators\Components\Telecoms\PBXValidator;

/**
 * Class HandsetRepositoryEloquent.
 */
class PBXRepositoryEloquent extends BaseRepository implements PBXRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return PBX::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
