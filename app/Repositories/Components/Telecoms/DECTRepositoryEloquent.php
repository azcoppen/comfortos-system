<?php

namespace SmartRooms\Repositories\Components\Telecoms;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\DECTRepository;
use SmartRooms\Models\Components\Telecoms\DECT;
use SmartRooms\Validators\Components\Telecoms\DECTValidator;

/**
 * Class HandsetRepositoryEloquent.
 */
class DECTRepositoryEloquent extends BaseRepository implements DECTRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return DECT::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
