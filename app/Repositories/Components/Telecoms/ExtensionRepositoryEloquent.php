<?php

namespace SmartRooms\Repositories\Components\Telecoms;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\ExtensionRepository;
use SmartRooms\Models\Components\Telecoms\Extension;
use SmartRooms\Validators\Components\Telecoms\ExtensionValidator;

/**
 * Class ExtensionRepositoryEloquent.
 */
class ExtensionRepositoryEloquent extends BaseRepository implements ExtensionRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Extension::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
