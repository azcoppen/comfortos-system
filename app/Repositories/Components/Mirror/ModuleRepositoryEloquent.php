<?php

namespace SmartRooms\Repositories\Components\Mirror;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Mirror\ModuleRepository;
use SmartRooms\Models\Components\Mirror\Module;
use SmartRooms\Validators\Components\Mirror\ModuleValidator;

/**
 * Class ServerRepositoryEloquent.
 */
class ModuleRepositoryEloquent extends BaseRepository implements ModuleRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Module::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
