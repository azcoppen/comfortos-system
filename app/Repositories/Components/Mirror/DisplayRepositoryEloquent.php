<?php

namespace SmartRooms\Repositories\Components\Mirror;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Mirror\DisplayRepository;
use SmartRooms\Models\Components\Mirror\Display;
use SmartRooms\Validators\Components\Mirror\DisplayValidator;

/**
 * Class ServerRepositoryEloquent.
 */
class DisplayRepositoryEloquent extends BaseRepository implements DisplayRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Display::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
