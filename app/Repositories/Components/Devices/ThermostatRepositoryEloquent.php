<?php

namespace SmartRooms\Repositories\Components\Devices;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\ThermostatRepository;
use SmartRooms\Models\Components\Devices\Thermostat;
use SmartRooms\Validators\Components\Devices\ThermostatValidator;

/**
 * Class ThermostatRepositoryEloquent.
 */
class ThermostatRepositoryEloquent extends BaseRepository implements ThermostatRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Thermostat::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
