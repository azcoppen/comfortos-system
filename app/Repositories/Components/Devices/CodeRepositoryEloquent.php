<?php

namespace SmartRooms\Repositories\Components\Devices;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\CodeRepository;
use SmartRooms\Models\Components\Devices\Code;
use SmartRooms\Validators\Components\Devices\CodeValidator;

/**
 * Class CodeRepositoryEloquent.
 */
class CodeRepositoryEloquent extends BaseRepository implements CodeRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Code::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
