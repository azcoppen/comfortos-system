<?php

namespace SmartRooms\Repositories\Components\Devices;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\PhysicalSwitchRepository;
use SmartRooms\Models\Components\Devices\PhysicalSwitch;
use SmartRooms\Validators\Components\Devices\PhysicalSwitchValidator;

/**
 * Class SwitchRepositoryEloquent.
 */
class PhysicalSwitchRepositoryEloquent extends BaseRepository implements PhysicalSwitchRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return PhysicalSwitch::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
