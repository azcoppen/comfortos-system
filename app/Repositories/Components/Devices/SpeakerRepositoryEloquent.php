<?php

namespace SmartRooms\Repositories\Components\Devices;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\SpeakerRepository;
use SmartRooms\Models\Components\Devices\Speaker;
use SmartRooms\Validators\Components\Devices\SpeakerValidator;

/**
 * Class SpeakerRepositoryEloquent.
 */
class SpeakerRepositoryEloquent extends BaseRepository implements SpeakerRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Speaker::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
