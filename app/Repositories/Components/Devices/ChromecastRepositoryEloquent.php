<?php

namespace SmartRooms\Repositories\Components\Devices;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\Components\Devices\ChromecastRepository;
use SmartRooms\Models\Components\Devices\Chromecast;
use SmartRooms\Validators\Components\Devices\ChromecastValidator;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class ChromecastRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\Components\Devices;
 */
class ChromecastRepositoryEloquent extends BaseRepository implements ChromecastRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Chromecast::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
