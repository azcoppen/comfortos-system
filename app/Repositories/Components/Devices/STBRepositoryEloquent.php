<?php

namespace SmartRooms\Repositories\Components\Devices;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\STBRepository;
use SmartRooms\Models\Components\Devices\STB;
use SmartRooms\Validators\Components\Devices\STBValidator;

/**
 * Class STBRepositoryEloquent.
 */
class STBRepositoryEloquent extends BaseRepository implements STBRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return STB::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
