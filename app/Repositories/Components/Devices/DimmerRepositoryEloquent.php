<?php

namespace SmartRooms\Repositories\Components\Devices;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\DimmerRepository;
use SmartRooms\Models\Components\Devices\Dimmer;
use SmartRooms\Validators\Components\Devices\DimmerValidator;

/**
 * Class DimmerRepositoryEloquent.
 */
class DimmerRepositoryEloquent extends BaseRepository implements DimmerRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Dimmer::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
