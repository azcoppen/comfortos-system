<?php

namespace SmartRooms\Repositories\Components\Devices;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use SmartRooms\Contracts\Repositories\Components\Devices\MotorRepository;
use SmartRooms\Models\Components\Devices\Motor;
use SmartRooms\Validators\Components\Devices\MotorValidator;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class MotorRepositoryEloquent.
 *
 * @package namespace SmartRooms\Repositories\Components\Devices;
 */
class MotorRepositoryEloquent extends BaseRepository implements MotorRepository, CacheableInterface
{
    use CacheableRepository;
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Motor::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
