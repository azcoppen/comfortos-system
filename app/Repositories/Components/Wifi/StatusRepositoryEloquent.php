<?php

namespace SmartRooms\Repositories\Components\Wifi;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\StatusRepository;
use SmartRooms\Models\Components\Wifi\Status;
use SmartRooms\Validators\Components\Wifi\StatusValidator;

/**
 * Class RouterRepositoryEloquent.
 */
class StatusRepositoryEloquent extends BaseRepository implements StatusRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Status::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
