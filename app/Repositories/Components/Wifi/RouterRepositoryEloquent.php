<?php

namespace SmartRooms\Repositories\Components\Wifi;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;
use SmartRooms\Models\Components\Wifi\Router;
use SmartRooms\Validators\Components\Wifi\RouterValidator;

/**
 * Class RouterRepositoryEloquent.
 */
class RouterRepositoryEloquent extends BaseRepository implements RouterRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Router::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
