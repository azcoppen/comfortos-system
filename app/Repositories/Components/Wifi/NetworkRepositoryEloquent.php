<?php

namespace SmartRooms\Repositories\Components\Wifi;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;
use SmartRooms\Models\Components\Wifi\Network;
use SmartRooms\Validators\Components\Wifi\NetworkValidator;

/**
 * Class PasswordRepositoryEloquent.
 */
class NetworkRepositoryEloquent extends BaseRepository implements NetworkRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Network::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
