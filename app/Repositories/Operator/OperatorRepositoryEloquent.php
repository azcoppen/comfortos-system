<?php

namespace SmartRooms\Repositories\Operator;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;
use SmartRooms\Models\Operator\Operator;
use SmartRooms\Validators\OperatorValidator;

/**
 * Class OperatorRepositoryEloquent.
 */
class OperatorRepositoryEloquent extends BaseRepository implements OperatorRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Operator::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
