<?php

namespace SmartRooms\Repositories\Operator;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;
use SmartRooms\Contracts\Repositories\Operator\BrandRepository;
use SmartRooms\Models\Operator\Brand;
use SmartRooms\Validators\BrandValidator;

/**
 * Class BrandRepositoryEloquent.
 */
class BrandRepositoryEloquent extends BaseRepository implements BrandRepository, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Brand::class;
    }

    /**
     * Boot up the repository, pushing criteria.
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
