<?php

use OzdemirBurak\Iris\Color\Hsv;
use OzdemirBurak\Iris\Color\Rgb;

if (! function_exists ('hsb') )
{
  function hsb ( array $value ) : Hsv
  {
    if (! isset ($value[0]) || !is_numeric ($value[0]) )
    {
      $value[0] = 0;
    }
    if (! isset ($value[1]) || !is_numeric ($value[1]) )
    {
      $value[1] = 0;
    }
    if (! isset ($value[2]) || !is_numeric ($value[2]) )
    {
      $value[2] = 0;
    }
    return new Hsv ('hsv('.($value[0] ?? 0).','.($value[1] ?? 0).'%,'.($value[2] ?? 0).'%)');
  }
}

if (! function_exists ('rgb') )
{
  function rgb ( string $value ) : Rgb
  {
    return new Rgb ($value);
  }
}

if (! function_exists ('format_search_query') )
{
  function format_search_query ( string $q ) : string
  {
    return '*'.trim(preg_replace('/[^A-Za-z0-9 ]/', ' ', $q)).'*';
  }
}
