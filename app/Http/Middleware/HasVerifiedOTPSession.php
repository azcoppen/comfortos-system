<?php

namespace SmartRooms\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class HasVerifiedOTPSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      try {

        if (! auth()->check () )
        {
          if ( Str::contains (($request->segments()[0] ?? ''), 'room') )
          {
            return redirect ()->route ('guest.room.login', $request->segments()[1] ?? null)->with ('error', __('guest/auth.login.errors.session'));
          }

          if ( Str::contains (($request->segments()[0] ?? ''), 'component') )
          {
            return redirect ()->route ('guest.component.'.Str::singular ($request->segments()[1]).'.login', $request->segments()[2] ?? null)->with ('error', __('guest/auth.login.errors.session'));
          }
        }

      }
      catch (\Exception $e)
      {
        //throw $e;
      }


        return $next($request);
    }
}
