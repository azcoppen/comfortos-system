<?php

namespace SmartRooms\Http\Requests\API\ComfortOS;

use Illuminate\Foundation\Http\FormRequest;

class UpdateWifiNetwork extends BaseUpdateRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label'         => 'nullable|filled|string|min:3',
            'ssid'          => 'nullable|filled|string|min:5|alpha_dash',
            'password'      => 'nullable|filled|string|min:8',
            'launch_at'     => 'nullable|filled|string|date',
            'expire_at'     => 'nullable|filled|string|date',
        ];
    }
}
