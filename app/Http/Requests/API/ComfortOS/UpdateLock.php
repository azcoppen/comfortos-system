<?php

namespace SmartRooms\Http\Requests\API\ComfortOS;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLock extends BaseUpdateRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'command'         => 'nullable|filled|string|max:3|in:open,close',
            'insert'          => 'nullable|filled|numeric|min:1000|max:1000000',
            'label'           => 'nullable|filled|string|max:100',
            'wipe'            => 'nullable|filled|string|max:100',
            'expire_at'       => 'nullable|filled|string|date',
            'execute_at'      => 'nullable|filled|string|date',
        ];
    }
}
