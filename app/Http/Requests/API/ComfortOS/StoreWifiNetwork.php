<?php

namespace SmartRooms\Http\Requests\API\ComfortOS;

use SmartRooms\Models\Property\Room;

class StoreWifiNetwork extends BaseStoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label'         => 'required|filled|string|min:3',
            'room'          => 'required|filled|string|exists:mongodb.'.Room::class.',_id',
            'ssid'          => 'required|filled|string|min:5|alpha_dash',
            'password'      => 'required|filled|string|min:8',
            'launch_at'     => 'nullable|filled|string|date',
            'expire_at'     => 'nullable|filled|string|date|after:launch_at',
        ];
    }
}
