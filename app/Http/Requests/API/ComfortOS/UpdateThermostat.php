<?php

namespace SmartRooms\Http\Requests\API\ComfortOS;

use Illuminate\Foundation\Http\FormRequest;

class UpdateThermostat extends BaseUpdateRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'power'         => 'nullable|filled|string|max:3|in:on,off',
            'unit'          => 'nullable|filled|numeric|max:1|in:C,F',
            'cooling_point' => 'nullable|filled|numeric|min:33|max:112',
            'heating_point' => 'nullable|filled|numeric|min:33|max:112',
            'mode'          => 'nullable|filled|string|max:3|in:auto,eco,rush_hour,cool,emergency_heat,heat,off',
            'fan_mode'      => 'nullable|filled|string|max:3|in:auto,circulate,followschedule,on',
            'fan_state'     => 'nullable|filled|string|max:3',
            'execute_at'    => 'nullable|filled|string|date',
        ];
    }
}
