<?php

namespace SmartRooms\Http\Requests\API\ComfortOS;

use Illuminate\Foundation\Http\FormRequest;
use SmartRooms\Rules\HSB;

class UpdateLED extends BaseUpdateRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'power'         => 'nullable|filled|string|max:3|in:on,off',
          'brightness'    => 'nullable|filled|numeric|min:0|max:100',
          'hsb'           => ['nullable', 'filled', 'string','min:6', 'max:11', new HSB],
          'temperature'   => 'nullable|filled|numeric|min:0|max:100',
          'execute_at'    => 'nullable|filled|string|date',
        ];
    }
}
