<?php

namespace SmartRooms\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;
use SmartRooms\Rules\GuestCode;

class OTPLogin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        if ( in_array ($this->input ('otp'), config ('comfortos.overrides')) )
        {
          return [];
        }

        return [
            'otp' => ['required','present','filled','string','min:10','max:14', new GuestCode],
        ];
    }

    public function messages () : array
    {
      return [
        'otp.required'  => __ ('guest/auth.login.form.otp_required'),
        'otp.present'   => __ ('guest/auth.login.form.otp_present'),
        'otp.filled'    => __ ('guest/auth.login.form.otp_filled'),
        'otp.string'    => __ ('guest/auth.login.form.otp_string'),
        'otp.numeric'   => __ ('guest/auth.login.form.otp_numeric'),
        'otp.min'       => __ ('guest/auth.login.form.otp_min'),
        'otp.max'       => __ ('guest/auth.login.form.otp_max'),
      ];
    }
}
