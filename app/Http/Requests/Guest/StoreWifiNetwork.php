<?php

namespace SmartRooms\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

class StoreWifiNetwork extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'label'             => 'required|filled|string|max:255',
          'expire_at_date'    => 'required|filled|string|max:255|date',
          'expire_at_time'    => 'required|filled|string|max:255',
          'ssid'              => 'required|filled|string|max:20|alpha_dash',
          'password'          => 'required|filled|string|min:8|max:20',
        ];
    }
}
