<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Components\Telecoms\PBX;
use SmartRooms\Models\IDAM\User;


class UpdateExtension extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pbx_id'          => 'nullable|present|string|max:255|alpha_dash|exists:'.PBX::class.',_id',
            'label'           => 'required|filled|string|max:255',
            'caller_id'       => 'required|filled|string|max:255',
            'did'             => 'nullable|present|string|max:255|phone:AUTO,LENIENT,US',
            'vpin'            => 'required|filled|numeric|max:999999',
            'remote_id'       => 'required|filled|numeric|max:999999',
            'sip_id'          => 'required|filled|string|max:255',
            'auth_id'         => 'required|filled|string|max:255|alpha_dash',
            'auth_pwd'        => 'required|filled|string|max:255|alpha_dash',

            'ordered_at'      => 'nullable|present|string|date',
            'ordered_by'      => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'order_id'        => 'nullable|present|string|max:255',
            'installed_at'    => 'nullable|present|string|date',
            'installed_by'    => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'provisioned_at'  => 'nullable|present|string|date',
            'provisioned_by'  => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
        ];
    }
}
