<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Components\Telecoms\DECT;
use SmartRooms\Models\Components\Telecoms\PBX;
use SmartRooms\Models\Components\Wifi\Router;
use SmartRooms\Models\Components\OS\VPNServer;
use SmartRooms\Models\Components\OS\VPNClient;
use SmartRooms\Models\IDAM\User;

use SmartRooms\Rules\Mac;

class StoreNewDECT extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pbx_id'          => 'nullable|present|string|max:255|alpha_dash|exists:'.PBX::class.',_id',
            'label'           => 'required|filled|string|max:255',
            'type'            => 'required|filled|string|max:255|alpha_dash',
            'model'           => 'required|filled|string|max:255|alpha_dash',
            'serial'          => ['required','filled','string','max:255','alpha_dash','unique:'.DECT::class.',serial'],
            'v'               => 'required|filled|string|max:255',
            'rfpi'            => ['nullable','filled','string','max:255','alpha_dash','unique:'.DECT::class.',rfpi'],
            'mac'             => ['nullable','filled','string','max:255', new Mac,'unique:'.DECT::class.',mac'],
            'router_id'       => 'nullable|present|string|max:255|alpha_dash|exists:'.Router::class.',_id',
            'host'            => 'required|present|string|max:255|alpha_dash',
            'int_ip'          => 'nullable|present|string|max:255|ip',
            'admin_user'      => 'required|filled|string|max:255|alpha_dash',
            'admin_pwd'       => 'required|filled|string|max:255|alpha_dash',

            'vpn_server_id'   => 'nullable|present|string|max:255|alpha_dash|exists:'.VPNServer::class.',_id',
            'vpn_client_id'   => 'nullable|present|string|max:255|alpha_dash|exists:'.VPNClient::class.',_id',
            'vpn_ip'          => 'nullable|present|string|max:255|ip',

            'ordered_at'      => 'nullable|present|string|date',
            'ordered_by'      => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'order_id'        => 'nullable|present|string|max:255',
            'installed_at'    => 'nullable|present|string|date',
            'installed_by'    => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'provisioned_at'  => 'nullable|present|string|date',
            'provisioned_by'  => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'clone'           => 'nullable|present|numeric|max:100',
        ];
    }
}
