<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Components\OS\VPNServer;
use SmartRooms\Models\Components\OS\VPNClient;
use SmartRooms\Models\IDAM\User;

use SmartRooms\Rules\Mac;

class UpdateRouter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label'           => 'required|filled|string|max:255',
            'type'            => 'required|filled|string|max:255|alpha_dash',
            'model'           => 'required|filled|string|max:255|alpha_dash',
            'serial'          => 'required|filled|string|max:255|alpha_dash',
            'v'               => 'required|filled|string|max:255',
            'mac'             => ['required','filled','string','max:255', new Mac],
            'host'            => 'required|filled|string|max:255|alpha_dash',
            'int_ip'          => 'required|filled|string|max:255|ip',
            'web_user'        => 'required|filled|string|max:255|alpha_dash',
            'web_pwd'         => 'required|filled|string|max:255|alpha_dash',
            'api_user'        => 'required|filled|string|max:255|alpha_dash',
            'api_pwd'         => 'required|filled|string|max:255|alpha_dash',

            'vpn_server_id'   => 'nullable|present|string|max:255|alpha_dash|exists:'.VPNServer::class.',_id',
            'vpn_client_id'   => 'nullable|present|string|max:255|alpha_dash|exists:'.VPNClient::class.',_id',
            'vpn_ip'          => 'nullable|present|string|max:255|ip',

            'ordered_at'      => 'nullable|present|string|date',
            'ordered_by'      => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'order_id'        => 'nullable|present|string|max:255',
            'installed_at'    => 'nullable|present|string|date',
            'installed_by'    => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'provisioned_at'  => 'nullable|present|string|date',
            'provisioned_by'  => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
        ];
    }
}
