<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Property\Building;

use SmartRooms\Rules\Latitude;
use SmartRooms\Rules\Longitude;

class UpdateBuilding extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'label'       => 'required|filled|string|max:255',
          'slug'        => 'nullable|present|string|alpha_dash',
          'identifier'  => 'nullable|present|string|alpha_dash',
          'latitude'    => ['nullable','present','numeric', new Latitude],
          'longitude'   => ['nullable','present','numeric', new Longitude],
          'tags'        => 'nullable|present|string|max:1000',
          'image'       => 'nullable|file|image|mimes:jpg,png|mimetypes:image/jpeg,image/png',
          'notes'       => 'nullable|present|string|max:1000',
        ];
    }
}
