<?php

namespace SmartRooms\Http\Requests\Dashboard\IDAM;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Rules\Country;
use SmartRooms\Rules\Currency;
use SmartRooms\Rules\Language;
use SmartRooms\Rules\Latitude;
use SmartRooms\Rules\Locale;
use SmartRooms\Rules\Longitude;
use SmartRooms\Rules\PhoneList;
use SmartRooms\Rules\EmailList;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prefix'        => 'nullable|string|max:10|in:Mr,Mrs,Miss,Dr,Ms,Prof,Rev',
            'first'         => 'required|filled|string|max:255',
            'last'          => 'required|filled|string|max:255',
            'suffix'        => 'nullable|string|max:255',
            'display'       => 'nullable|string|max:255',
            'slug'          => 'nullable|string|max:255|alpha_dash',
            'email'         => 'required|filled|string|email|max:255',
            'telephones'    => 'required|filled|string|max:255',
            'sex'           => 'nullable|filled|string|max:1|in:M,F,X',
            'dob'           => 'required|filled|string|date|max:255',
            'job_title'     => 'nullable|string|max:255',
            'street'        => 'required|filled|string|max:255',
            'city'          => 'required|filled|string|max:255',
            'region'        => 'required|filled|string|max:10',
            'postal'        => 'required|filled|string|max:20',
            'country'       => ['required','filled','string','size:2','alpha_dash', new Country],
            'latitude'      => ['required','filled','numeric', new Latitude],
            'longitude'     => ['required','filled','numeric', new Longitude],
            'lang'          => ['required','filled','string','size:2', new Language],
            'locale'        => ['required','filled','string','max:5','alpha_dash', new Locale],
            'currency'      => ['required','filled','string','max:5','alpha_dash', new Currency],
            'timezone'      => 'required|filled|string|max:255|timezone',
            'password'      => 'required|filled|string|min:8|max:255',
            'image'         => 'nullable|file|image|mimes:jpg,png|mimetypes:image/jpeg,image/png',
            'tags'          => 'nullable|string|max:255',
        ];
    }
}
