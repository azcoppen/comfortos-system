<?php

namespace SmartRooms\Http\Requests\Dashboard\IDAM;

use Illuminate\Foundation\Http\FormRequest;

class StoreRoomAccess extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room_id'       => 'required|string|max:255|exists:rooms,_id',
            'start_at_date' => 'required|string|date|max:255|before:end_at_date',
            'start_at_time' => 'required|string|max:255',
            'end_at_date'   => 'required|string|date|max:255|after:start_at_date',
            'end_at_time'   => 'required|string|max:255',
        ];
    }
}
