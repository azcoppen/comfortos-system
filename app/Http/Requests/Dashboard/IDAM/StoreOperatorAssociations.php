<?php

namespace SmartRooms\Http\Requests\Dashboard\IDAM;

use Illuminate\Foundation\Http\FormRequest;

class StoreOperatorAssociations extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'operators' => 'nullable|array|exists:operators,_id',
        ];
    }
}
