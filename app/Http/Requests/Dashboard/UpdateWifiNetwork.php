<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Components\Wifi\Router;
use SmartRooms\Models\IDAM\User;

class UpdateWifiNetwork extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'label'             => 'required|filled|string|max:255',
          'type'              => 'required|filled|string|max:255|alpha_dash|in:hardware,virtual',
          'interface'         => 'required|filled|string|max:255|alpha_dash',
          'router_id'         => 'required|present|string|max:255|alpha_dash|exists:'.Router::class.',_id',
          'launch_at_date'    => 'required|filled|string|max:255|date|before:expire_at_date',
          'launch_at_time'    => 'required|filled|string|max:255|date_format:G:i',
          'expire_at_date'    => 'required|filled|string|max:255|date|after:launch_at_date',
          'expire_at_time'    => 'required|filled|string|max:255|date_format:G:i',
          'security_profile'  => 'required|filled|string|max:255|alpha_dash',
          'profile_index'     => 'nullable|present|string|max:3',
          'network_index'     => 'nullable|present|string|max:3',
          'ssid'              => 'required|filled|string|max:20|alpha_dash',
          'password'          => 'required|filled|string|min:8|max:20',

          'installed_at'      => 'nullable|present|string|date',
          'installed_by'      => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'provisioned_at'    => 'nullable|present|string|date',
          'provisioned_by'    => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
        ];
    }
}
