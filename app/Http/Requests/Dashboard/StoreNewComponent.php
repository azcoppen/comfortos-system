<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\IDAM\User;

class StoreNewComponent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'label'           => 'required|filled|string|max:255',
          'type'            => 'required_without:ssid|filled|string|max:255|alpha_dash',
          'variant'         => 'nullable|string|max:255|alpha_dash',
          'driver'          => 'required_without:ssid|filled|string|max:255|alpha_dash',

          'items.*'         => 'nullable|filled|string|max:255|alpha_dash',
          'options.*'       => 'nullable|array',
          'options.*.*'     => 'nullable|filled|max:255|alpha_dash',

          'ordered_at'      => 'nullable|string|date',
          'ordered_by'      => 'nullable|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'order_id'        => 'nullable|string|max:255',
          'installed_at'    => 'nullable|string|date',
          'installed_by'    => 'nullable|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'provisioned_at'  => 'nullable|string|date',
          'provisioned_by'  => 'nullable|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'clone'           => 'nullable|numeric|max:100',

          'ssid'            => ['nullable','string','max:255','alpha_dash'],
          'password'        => 'required_with:ssid|string|min:8|max:20',
          'launch_at_date'  => 'required_with:ssid|string|date|before:expire_at_date',
          'launch_at_time'  => 'required_with:ssid|string|date_format:G:i',
          'expire_at_date'  => 'required_with:ssid|string|date|after:launch_at_date',
          'expire_at_time'  => 'required_with:ssid|string|date_format:G:i',
        ];
    }
}
