<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Components\Telecoms\DECT;
use SmartRooms\Models\Components\Telecoms\Handset;
use SmartRooms\Models\IDAM\User;

use SmartRooms\Rules\Mac;

class StoreNewHandset extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dect_id'         => 'nullable|present|string|max:255|alpha_dash|exists:'.DECT::class.',_id',
            'label'           => 'required|filled|string|max:255',
            'type'            => 'required|filled|string|max:255|alpha_dash',
            'model'           => 'required|filled|string|max:255|alpha_dash',
            'serial'          => ['required','filled','string','max:255','alpha_dash','unique:'.Handset::class.',serial'],
            'v'               => 'required|filled|string|max:255',
            'ipei'            => ['required','filled','string','max:255','alpha_dash','unique:'.Handset::class.',ipei'],
            'mac'             => ['nullable','filled','string','max:255', new Mac, 'unique:'.Handset::class.',mac'],
            'name'            => 'required|filled|string|max:255|alpha_dash',
            'int_ip'          => 'nullable|present|string|max:255|ip',

            'ordered_at'      => 'nullable|present|string|date',
            'ordered_by'      => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'order_id'        => 'nullable|present|string|max:255',
            'installed_at'    => 'nullable|present|string|date',
            'installed_by'    => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'provisioned_at'  => 'nullable|present|string|date',
            'provisioned_by'  => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'clone'           => 'nullable|present|numeric|max:100',
        ];
    }
}
