<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\IDAM\User;
use SmartRooms\Models\Components\OS\VPNServer;

use SmartRooms\Rules\CIDR;
use SmartRooms\Rules\Cipher;
use SmartRooms\Rules\Host;
use SmartRooms\Rules\Netmask;

class StoreNewVPNClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label'           => 'required|filled|string|max:255',
            'type'            => 'required|filled|string|max:255|alpha_dash|in:openvpn,wireguard',
            'vpn_server_id'   => 'required|present|filled|string|max:255|alpha_dash|exists:'.VPNServer::class.',_id',
            'client_id'       => 'required|filled|string|max:255|alpha_dash',
            'cipher'          => ['required','filled','string','max:255', 'alpha_dash', new Cipher],
            'certificate'     => 'required|filled|string|max:2000',
            'ordered_at'      => 'nullable|present|string|date',
            'ordered_by'      => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'order_id'        => 'nullable|present|string|max:255',
            'installed_at'    => 'nullable|present|string|date',
            'installed_by'    => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'provisioned_at'  => 'nullable|present|string|date',
            'provisioned_by'  => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
        ];
    }
}
