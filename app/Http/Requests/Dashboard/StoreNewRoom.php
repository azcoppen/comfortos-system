<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Rules\Latitude;
use SmartRooms\Rules\Longitude;
use SmartRooms\Rules\RoomGrade;
use SmartRooms\Rules\RoomType;

class StoreNewRoom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label'       => 'required|filled|string|max:255',
            'floor'       => 'required|filled|numeric|min:-10|max:100',
            'number'      => 'required|filled|numeric|min:1|max:10000',
            'type'        => ['required','filled','string','max:255', new RoomType],
            'grade'       => ['required','filled','string','max:255', new RoomGrade],
            'description' => 'required|filled|string|max:255',
            'latitude'    => ['nullable','present','numeric', new Latitude],
            'longitude'   => ['nullable','present','numeric', new Longitude],
            'size'        => 'required|filled|numeric|min:1|max:10000',
            'beds'        => 'required|filled|numeric|min:0|max:20',
            'occupancy'   => 'required|filled|numeric|min:1|max:10000',
            'ext'         => 'required|filled|boolean',
            'public'      => 'required|filled|boolean',
            'smoking'     => 'required|filled|boolean',
            'AC'          => 'required|filled|boolean',
            'eth'         => 'required|filled|boolean',
            'tags'        => 'nullable|present|string|max:1000',
            'notes'       => 'nullable|present|string|max:1000',
            'image'       => 'nullable|file|image|mimes:jpg,png|mimetypes:image/jpeg,image/png',
            'users.*'     => ['nullable','present','exists:'.User::class.',_id'],
        ];
    }
}
