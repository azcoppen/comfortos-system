<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Components\OS\VPNServer;
use SmartRooms\Models\Components\OS\VPNClient;
use SmartRooms\Models\IDAM\User;

use SmartRooms\Rules\Country;
use SmartRooms\Rules\Host;

class StoreNewPBX extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label'           => 'required|filled|string|max:255',
            'type'            => 'required|filled|string|max:255|alpha_dash',
            'v'               => 'required|filled|numeric',
            'country'         => ['required','filled','string','size:2','alpha_dash', new Country],
            'license'         => 'required|filled|string|max:255|alpha_dash',
            'host'            => ['required','filled','string','max:255', new Host],
            'ip'              => 'required|filled|string|max:255|ip',
            'trunk_vendor'    => 'nullable|present|string|max:255|alpha_dash',
            'trunk_number'    => 'nullable|present|string|max:255|phone:AUTO,LENIENT,'.strtoupper($this->input('country')),
            'admin_url'       => 'required|filled|string|max:255|url',
            'admin_port'      => 'required|filled|numeric|min:10|max:65000',
            'admin_user'      => 'required|filled|string|max:255|alpha_dash',
            'admin_pwd'       => 'required|filled|string|max:255|alpha_dash',
            'sip_port'        => 'required|filled|numeric|min:10|max:65000',
            'secure_sip_port' => 'required|filled|numeric|min:10|max:65000',
            'rtp_port_range'  => 'required|filled|array|size:2',

            'vpn_server_id'   => 'nullable|present|string|max:255|alpha_dash|exists:'.VPNServer::class.',_id',
            'vpn_client_id'   => 'nullable|present|string|max:255|alpha_dash|exists:'.VPNClient::class.',_id',
            'vpn_ip'          => 'nullable|present|string|max:255|ip',

            'ordered_at'      => 'nullable|present|string|date',
            'ordered_by'      => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'order_id'        => 'nullable|present|string|max:255',
            'installed_at'    => 'nullable|present|string|date',
            'installed_by'    => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'provisioned_at'  => 'nullable|present|string|date',
            'provisioned_by'  => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
        ];
    }
}
