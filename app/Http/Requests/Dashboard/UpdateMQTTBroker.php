<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\IDAM\User;
use SmartRooms\Models\Components\OS\VPNServer;
use SmartRooms\Models\Components\OS\VPNClient;

use SmartRooms\Rules\CIDR;
use SmartRooms\Rules\Cipher;
use SmartRooms\Rules\Host;
use SmartRooms\Rules\Netmask;

class UpdateMQTTBroker extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'label'           => 'nullable|string|max:255',
          'type'            => 'nullable|string|max:255|alpha_dash|in:hivemq,mosquitto',
          'v'               => 'nullable|string|max:255',
          'region'          => 'nullable|string|max:255|alpha_dash',
          'context'         => 'nullable|string|max:255|alpha_dash|in:local,remote',
          'host'            => ['nullable','filled','string','max:255', new Host],
          'insecure_port'   => 'nullable|numeric|min:10|max:65000',
          'secure_port'     => 'nullable|numeric|min:10|max:65000',
          'admin_user'      => 'nullable|string|max:255|alpha_dash',
          'admin_pass'      => 'nullable|string|max:255',
          'admin_port'      => 'nullable|numeric|min:10|max:65000',
          'app_user'        => 'nullable|string|max:255|alpha_dash',
          'app_pass'        => 'nullable|string|max:255',
          'vpn_server_id'   => 'nullable|string|max:255|alpha_dash|exists:'.VPNServer::class.',_id',
          'vpn_ip'          => 'nullable|string|max:255|ip',

          'ordered_at'      => 'nullable|string|date',
          'ordered_by'      => 'nullable|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'order_id'        => 'nullable|string|max:255',
          'installed_at'    => 'nullable|string|date',
          'installed_by'    => 'nullable|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'provisioned_at'  => 'nullable|string|date',
          'provisioned_by'  => 'nullable|string|max:255|alpha_dash|exists:'.User::class.',_id',
        ];
    }
}
