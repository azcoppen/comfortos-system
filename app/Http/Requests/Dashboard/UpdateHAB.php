<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Components\Wifi\Router;
use SmartRooms\Models\Components\OS\HAB;
use SmartRooms\Models\Components\OS\VPNServer;
use SmartRooms\Models\Components\OS\VPNClient;
use SmartRooms\Models\IDAM\User;

use SmartRooms\Rules\Mac;

class UpdateHAB extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'label'           => 'required|filled|string|max:255',
          'os'              => 'required|filled|string|max:255',
          'release'         => 'required|filled|string|max:255',
          'codename'        => 'required|filled|string|max:255',
          'kernel'          => 'required|filled|string|max:255',
          'model'           => 'required|filled|string|max:255',
          'serial'          => 'required|filled|string|max:255|alpha_dash',
          'hardware'        => 'required|filled|string|max:255|alpha_dash',
          'mac'             => ['nullable','filled','string','max:255'],
          'engine'          => 'required|filled|string|max:255|alpha_dash',
          'dist'            => 'required|filled|string|max:255',
          'http_port'       => 'required|present|numeric|min:10|max:65000',
          'https_port'      => 'required|present|numeric|min:10|max:65000',
          'http_user'       => 'nullable|present|string|max:255|alpha_dash',
          'http_pass'       => 'nullable|present|string|max:255|alpha_dash',
          'router_id'       => 'nullable|present|string|max:255|alpha_dash|exists:'.Router::class.',_id',
          'host'            => 'required|present|string|max:255|alpha_dash',
          'int_ip'          => 'nullable|present|string|max:255|ip',
          'ssh_user'        => 'required|filled|string|max:255|alpha_dash',
          'ssh_pass'        => 'required|filled|string|max:255|alpha_dash',
          'vpn_server_id'   => 'nullable|present|string|max:255|alpha_dash|exists:'.VPNServer::class.',_id',
          'vpn_client_id'   => 'nullable|present|string|max:255|alpha_dash|exists:'.VPNClient::class.',_id',
          'vpn_ip'          => 'nullable|present|string|max:255|ip',

          'ordered_at'      => 'nullable|present|string|date',
          'ordered_by'      => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'order_id'        => 'nullable|present|string|max:255',
          'installed_at'    => 'nullable|present|string|date',
          'installed_by'    => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'provisioned_at'  => 'nullable|present|string|date',
          'provisioned_by'  => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
        ];
    }
}
