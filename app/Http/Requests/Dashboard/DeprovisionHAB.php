<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\IDAM\User;

class DeprovisionHAB extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'habs' => 'required|array|min:1',
        ];
    }

    public function messages ()
    {
      return [
        'habs.required' => 'You must select at least one HAB to deprovision.',
      ];
    }
}
