<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Rules\RoomGrade;
use SmartRooms\Rules\RoomRange;
use SmartRooms\Rules\RoomType;

use SmartRooms\Models\Property\Building;

class StoreNewMultipleRoomSetup extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

          // do these rooms already exist?
          'building_id' => 'required|filled|string|exists:'.Building::class.',_id',
          'floor'       => 'required|filled|numeric|min:-10|max:100',
          'num'         => 'required|filled|numeric|min:1|max:100',
          'start'       => ['required','filled','numeric','min:1', new RoomRange ($this->get('building_id'), $this->get('floor'), $this->get('num'))],

          'type'        => ['required','filled','string','max:255', new RoomType],
          'grade'       => ['required','filled','string','max:255', new RoomGrade],
          'description' => 'required|filled|string|max:255',
          'footage'     => 'required|filled|numeric|min:1|max:10000',
          'beds'        => 'required|filled|numeric|min:0|max:20',
          'occupancy'   => 'required|filled|numeric|min:1|max:10000',
          'ext'         => 'required|filled|boolean',
          'public'      => 'required|filled|boolean',
          'smoking'     => 'required|filled|boolean',
          'AC'          => 'required|filled|boolean',
          'eth'         => 'required|filled|boolean',
        ];
    }
}
