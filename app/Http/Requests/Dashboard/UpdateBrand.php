<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBrand extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'label'       => 'required|filled|string|max:255',
          'slug'        => 'required|present|string|alpha_dash',
          'tags'        => 'nullable|present|string|max:1000',
          'image'       => 'nullable|file|image|mimes:jpg,png|mimetypes:image/jpeg,image/png',
          'notes'       => 'nullable|present|string|max:1000',
        ];
    }
}
