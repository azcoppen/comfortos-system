<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\IDAM\User;

class UpdateComponent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'label'           => 'nullable|filled|string|max:255',
          'type'            => 'nullable|filled|string|max:255|alpha_dash',
          'variant'         => 'nullable|string|max:255|alpha_dash',
          'driver'          => 'nullable|filled|string|max:255|alpha_dash',

          'items.*'         => 'nullable|filled|string|max:255|alpha_dash',
          'options.*'       => 'nullable|array',
          'options.*.*'     => 'nullable|filled|max:255|alpha_dash',

          'ordered_at'      => 'nullable|string|date',
          'ordered_by'      => 'nullable|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'order_id'        => 'nullable|string|max:255',
          'installed_at'    => 'nullable|string|date',
          'installed_by'    => 'nullable|string|max:255|alpha_dash|exists:'.User::class.',_id',
          'provisioned_at'  => 'nullable|string|date',
          'provisioned_by'  => 'nullable|string|max:255|alpha_dash|exists:'.User::class.',_id',

          'ssid'            => 'nullable|string|max:255|alpha_dash',
          'password'        => 'nullable|string|min:8|max:20',
          'launch_at_date'  => 'nullable|string|date',
          'launch_at_time'  => 'nullable|string|date_format:G:i',
          'expire_at_date'  => 'nullable|string|date',
          'expire_at_time'  => 'nullable|string|date_format:G:i',

        ];
    }
}
