<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\IDAM\User;

use SmartRooms\Rules\CIDR;
use SmartRooms\Rules\Cipher;
use SmartRooms\Rules\Host;
use SmartRooms\Rules\Netmask;

class StoreNewVPNServer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label'           => 'required|filled|string|max:255',
            'type'            => 'required|filled|string|max:255|alpha_dash',
            'region'          => 'required|filled|string|max:255|alpha_dash',
            'timezone'        => 'required|filled|string|max:255|timezone',
            'host'            => ['required','filled','string','max:255', new Host],
            'ports'           => 'required|filled|array|size:2',
            'subnet'          => ['required','filled','string','max:255', new CIDR],
            'ip_range'        => ['required','filled','string','max:255', new CIDR],
            'netmask'         => ['required','filled','string','max:255', new Netmask],
            'cipher'          => ['required','filled','string','max:255', 'alpha_dash', new Cipher],

            'ordered_at'      => 'nullable|present|string|date',
            'ordered_by'      => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'order_id'        => 'nullable|present|string|max:255',
            'installed_at'    => 'nullable|present|string|date',
            'installed_by'    => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'provisioned_at'  => 'nullable|present|string|date',
            'provisioned_by'  => 'nullable|present|string|max:255|alpha_dash|exists:'.User::class.',_id',
            'clone'           => 'nullable|present|numeric|max:100',

        ];
    }
}
