<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Components\OS\MQTTBroker;
use SmartRooms\Models\Components\OS\WSBroker;

use SmartRooms\Rules\Country;
use SmartRooms\Rules\Currency;
use SmartRooms\Rules\Language;
use SmartRooms\Rules\Latitude;
use SmartRooms\Rules\Locale;
use SmartRooms\Rules\Longitude;
use SmartRooms\Rules\PhoneList;
use SmartRooms\Rules\EmailList;

class UpdateProperty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'label'       => 'required|filled|string|max:255',
          'slug'        => 'required|present|string|alpha_dash',
          'lang'        => ['required','filled','string','size:2', new Language],
          'locale'      => ['required','filled','string','max:5','alpha_dash', new Locale],
          'timezone'    => 'required|filled|string|max:20|timezone',
          'currency'    => ['required','filled','string','max:5','alpha_dash', new Currency],
          'street'      => 'required|filled|string|max:255',
          'city'        => 'required|filled|string|max:255',
          'region'      => 'required|filled|string|max:255',
          'postal'      => 'required|filled|string|max:255|alpha_dash',
          'country'     => ['required','filled','string','size:2','alpha_dash', new Country],
          'latitude'    => ['required','filled','numeric', new Latitude],
          'longitude'   => ['required','filled','numeric', new Longitude],
          'telephones'  => ['required','filled','string','max:255', new PhoneList ($this->get('country', 'US'))],
          'emails'      => ['required','filled','string','max:255', new EmailList],
          'image'       => 'nullable|file|image|mimes:jpg,png|mimetypes:image/jpeg,image/png',
          'tags'        => 'nullable|present|string|max:1000',
          'notes'       => 'nullable|present|string|max:1000',
          'mqtt_broker_id' => 'nullable|present|string|alpha_dash|exists:'.MQTTBroker::class.',_id',
          'ws_broker_id' => 'nullable|present|string|alpha_dash|exists:'.WSBroker::class.',_id',
        ];
    }
}
