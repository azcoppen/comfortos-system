<?php

namespace SmartRooms\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

use SmartRooms\Models\Components\Mirror\Module;

class StoreNewModule extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'module'      => 'required|filled|string|max:255|alpha_dash|unique:'.Module::class.',module',
          'description' => 'nullable|present|string|max:1000',
          'repository'  => 'nullable|present|string|max:1000|url',
          'position'    => 'nullable|present|string|max:1000|in:'.implode (',', config ('comfortos.module_positions')),
          'tags'        => 'nullable|present|string|max:1000',
          'config'      => 'nullable|present|string|json',
        ];
    }
}
