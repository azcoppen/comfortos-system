<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;
use Illuminate\Support\Str;

class ComponentCommandAttributeGraph extends Component
{
    public $component;
    public $group;
    public $attribute;

    public function mount ($component, $group, $attribute)
    {
        $this->component = $component;
        $this->group     = $group;
        $this->attribute = $attribute;
    }

    public function render()
    {
        $this->emit('commands.chart.'.$this->attribute.'.rendering', ['commands-chart-'.$this->attribute]);
        return view('partials.components.charts.commands_component_attribute', ['component' => $this->component, 'attribute' => $this->attribute]);
    }
}
