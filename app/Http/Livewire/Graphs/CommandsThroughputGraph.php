<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;

class CommandsThroughputGraph extends Component
{
    public function render()
    {
        $this->emit('commands.chart.rendering', ['commands-chart']);
        return view('partials.components.charts.commands_global_tput');
    }
}
