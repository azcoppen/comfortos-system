<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;

class RoomSignalsThroughputGraph extends Component
{
    public $room;

    public function mount ($room)
    {
        $this->room = $room;
    }

    public function render()
    {
        $this->emit('signals.chart.rendering', ['signals-chart']);
        return view('partials.components.charts.signals_room_tput', ['room' => $this->room]);
    }
}
