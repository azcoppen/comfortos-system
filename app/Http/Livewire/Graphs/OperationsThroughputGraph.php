<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;

class OperationsThroughputGraph extends Component
{
    public function render()
    {
        $this->emit('operations.chart.rendering', ['operations-chart']);
        return view('partials.components.charts.operations_global_tput');
    }
}
