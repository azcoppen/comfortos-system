<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;

class RoomCommandsThroughputGraph extends Component
{
    public $room;

    public function mount ($room)
    {
        $this->room = $room;
    }

    public function render()
    {
        $this->emit('signals.chart.rendering', ['commands-chart']);
        return view('partials.components.charts.commands_room_tput', ['room' => $this->room]);
    }
}
