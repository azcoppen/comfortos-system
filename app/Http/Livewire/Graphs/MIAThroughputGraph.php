<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;

class MIAThroughputGraph extends Component
{
    public function render()
    {
        $this->emit('mia.chart.rendering', ['mia-chart']);
        return view('partials.components.charts.mia_global_tput');
    }
}
