<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;

class MIAThroughputHourlyGraph extends Component
{
    public function render()
    {
        $this->emit('mia.hourly.chart.rendering', ['mia-hourly-chart']);
        return view('partials.components.charts.mia_global_tput_hourly');
    }
}
