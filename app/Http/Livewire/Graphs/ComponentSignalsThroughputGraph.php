<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;
use Illuminate\Support\Str;

class ComponentSignalsThroughputGraph extends Component
{
    public $component;
    public $group;

    public function mount ($component)
    {
        $this->component = $component;
        $this->group = strtolower (Str::plural ( (new \ReflectionClass ($component))->getShortName() ));

        switch ( $this->group )
        {
          case 'displays':
            $this->group = 'mirrors';
          break;

          case 'networks':
            $this->group = 'wifi-networks';
          break;

          case 'physicalswitches':
            $this->group = 'switches';
          break;
        }
    }

    public function render()
    {
        $this->emit('signals.chart.rendering', ['signals-chart']);
        return view('partials.components.charts.signals_component_tput', ['group' => $this->group, 'component' => $this->component]);
    }
}
