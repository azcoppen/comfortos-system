<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;

class SignalsThroughputHourlyGraph extends Component
{
    public function render()
    {
        $this->emit('signals.hourly.chart.rendering', ['signals-chart']);
        return view('partials.components.charts.signals_global_tput_hourly');
    }
}
