<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;

class CommandsThroughputHourlyGraph extends Component
{
    public function render()
    {
        $this->emit('commands.hourly.chart.rendering', ['commands-chart']);
        return view('partials.components.charts.commands_global_tput_hourly');
    }
}
