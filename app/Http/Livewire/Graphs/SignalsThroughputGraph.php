<?php

namespace SmartRooms\Http\Livewire\Graphs;

use Livewire\Component;

class SignalsThroughputGraph extends Component
{
    public function render()
    {
        $this->emit('signals.chart.rendering', ['signals-chart']);
        return view('partials.components.charts.signals_global_tput');
    }
}
