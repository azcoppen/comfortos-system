<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Services\ComfortOS\API;

class ColorTempSlider extends Component
{
  public $component;

  protected $managers   = [
    Devices\Bulb::class     => API\BulbManager::class,
    Devices\LED::class      => API\LEDManager::class,
    Devices\TV::class       => API\TVManager::class,
  ];

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'temperature'))->first();

      return view('guest.livewire.widgets.color_temp_slider', ['component' => $this->component, 'last_value' => $last_signal->value ?? 50]);
  }

  public function command ( int $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'temperature', 'value' => $value]);

    $result = app ($this->managers [get_class ($this->component)])
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['temperature' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('temperature.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
