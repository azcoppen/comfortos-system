<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Services\ComfortOS\API;

class SpeakerMode extends Component
{
  public $component;

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'mode'))->first();

      $this->dispatchBrowserEvent ('mode.rendering', ['component' => $this->component->_id, 'value' => $last_signal->value ?? null]);

      return view('guest.livewire.widgets.speaker_modes', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
  }

  public function command ( string $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'mode', 'value' => $value]);

    $result = app (API\SpeakerManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['mode' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('mode.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
