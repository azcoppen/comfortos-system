<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;
use \Carbon\Carbon;
use \Exception;

use SmartRooms\Contracts\Repositories\Components\Devices\CodeRepository;

use SmartRooms\Services\ComfortOS\API;

class ManageLockCode extends Component
{
  public $component;
  public $index;
  public $code;

  protected $listeners =[
    'code.inserted' => '$refresh',
    'code.updated' => '$refresh',
    'code.removed' => '$refresh',
  ];

  public function mount ($component, $index, $code)
  {
      $this->component = $component;
      $this->index = $index;
      $this->code = $code;
  }

  public function render()
  {
      return view('guest.livewire.widgets.lock_code', ['component' => $this->component, 'index' => $this->index ?? -1, 'code' => $this->code]);
  }

  public function insert (int $row_index, string $code_date_local_tz, string $code_time, ?string $digits = null)
  {
    if (! $digits )
    {
      return false;
    }

    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'insert', 'value' => func_get_args()]);

    try
    {
      $local_expiry = Carbon::parse ($code_date_local_tz.' '.$code_time, $this->component->room->building->property->timezone);
    }
    catch ( Exception $e)
    {
      $local_expiry = now ($this->component->room->building->property->timezone)->addHours(24);
    }

    $result = app (API\LockManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['insert' => intval ($digits), 'expire_at' => $local_expiry->timezone('UTC')->format('c')])
      ->update ();

    $this->emitSelf ('code.inserted');
    $this->emitUp ($this->component->_id.'.code.inserted');
    $this->dispatchBrowserEvent ($this->component->_id.'.code.inserted', ['component' => $this->component->_id, 'index' => $row_index]);
  }

  public function update (int $row_index, string $code_id, string $code_date_local_tz, string $code_time)
  {
    $code = app (CodeRepository::class)->find ($code_id);

    if (! $code )
    {
      return false;
    }

    if ( $code->readonly )
    {
      return false;
    }

    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'update', 'value' => $code_id]);

    try
    {
      $local_expiry = Carbon::parse ($code_date_local_tz.' '.$code_time, $this->component->room->building->property->timezone);
    }
    catch ( Exception $e)
    {
      $local_expiry = now ($this->component->room->building->property->timezone)->addHours(24);
    }

    $result = app (API\LockManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['code_id' => $code_id])
      ->expiry ($code, $local_expiry->timezone('UTC')->format('c'));

    $this->emitSelf ('code.updated');
    $this->emitUp ($this->component->_id.'.code.updated');
    $this->dispatchBrowserEvent ($this->component->_id.'.code.updated', ['component' => $this->component->_id, 'code_id' => $code->_id, 'index' => $row_index]);
  }

  public function remove (int $row_index, string $code_id)
  {
    $code = app (CodeRepository::class)->find ($code_id);

    if (! $code )
    {
      return false;
    }

    if ( $code->readonly )
    {
      return false;
    }

    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'remove', 'value' => $code_id]);

    $result = app (API\LockManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['code_id' => $code_id])
      ->wipe ($code, true);

    $this->emitSelf ('code.removed');
    $this->emitUp ($this->component->_id.'.code.removed');
    $this->dispatchBrowserEvent ($this->component->_id.'.code.removed', ['component' => $this->component->_id, 'index' => $row_index]);
  }
}
