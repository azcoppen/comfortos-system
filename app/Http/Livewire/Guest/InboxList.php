<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Models\Property\Room;

class InboxList extends Component
{
  public $room;

  public function mount (Room $room)
  {
      $this->room = $room;
  }

  public function render()
  {
      return view('guest.livewire.widgets.inbox_list');
  }
}
