<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Models\Property\Room;
use SmartRooms\Models\Components\Wifi\Network AS WifiNetwork;

use SmartRooms\Services\ComfortOS\API;

class Network extends Component
{
  public $network;

  public function mount (WifiNetwork $network)
  {
      $this->network = $network;
  }

  public function render()
  {
      return view('guest.livewire.widgets.network', ['network' => $this->network]);
  }

}
