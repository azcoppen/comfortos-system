<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

class Start extends Component
{
    public function render()
    {
        return view('livewire.guest.start');
    }
}
