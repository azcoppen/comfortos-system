<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

abstract class WidgetComponent extends Component
{
    private $component_id;

    public function mount (string $component_id)
    {
      $this->component_id = $component_id;
    }
}
