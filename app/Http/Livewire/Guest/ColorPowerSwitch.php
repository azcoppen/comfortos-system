<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Services\ComfortOS\API;

class ColorPowerSwitch extends Component
{
    public $component;

    protected $managers   = [
      Devices\Bulb::class     => API\BulbManager::class,
      Devices\LED::class      => API\LEDManager::class,
    ];

    public function mount ($component)
    {
        $this->component = $component;
    }

    public function render()
    {
        $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'hsb'))->first();

        return view('guest.livewire.widgets.color_power_switch', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
    }

    public function command ( string $value )
    {
      $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'power', 'value' => $value]);
      $this->dispatchBrowserEvent ($this->component->_id.'_command.sent', ['component' => $this->component->_id, 'attribute' => 'power', 'value' => $value]);

      $result = app ($this->managers [get_class ($this->component)])
        ->src ('guest')
        ->user (auth()->user())
        ->entity ($this->component)
        ->params (['power' => $value])
        ->update ();

      $this->dispatchBrowserEvent ('power.updated', ['component' => $this->component->_id, 'value' => $value]);
    }
}
