<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Services\ComfortOS\API;

class MotorButtons extends Component
{
  public $component;

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'dir'))->first();

      return view('guest.livewire.widgets.motor_buttons', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
  }

  public function command ( string $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'dir', 'value' => $value]);

    $result = app (API\MotorManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['dir' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('dir.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
