<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Services\ComfortOS\API;

class ColorWheel extends Component
{
  public $component;

  protected $managers   = [
    Devices\Bulb::class     => API\BulbManager::class,
    Devices\LED::class      => API\LEDManager::class,
  ];

  public $listeners  = [];

  public function mount ($component)
  {
      $this->component = $component;
      $this->listeners[$this->component->_id.'_command'] = 'command';
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'hsb'))->first();

      $this->dispatchBrowserEvent ('hsb.rendering', ['component' => $this->component->_id, 'value' => $last_signal->value ?? null]);

      return view('guest.livewire.widgets.color_wheel', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
  }

  public function command ( $value )
  {
    $this->dispatchBrowserEvent ($this->component->_id.'_command.sent', ['component' => $this->component->_id, 'attribute' => 'hsb', 'value' => $value]);

    $result = app ($this->managers [get_class ($this->component)])
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['hsb' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('hsb.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
