<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Services\ComfortOS\API;

class TVDirButtons extends Component
{
  public $component;

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      return view('guest.livewire.widgets.tv_dir_buttons', ['component' => $this->component,]);
  }

  public function command ( string $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'keycode', 'value' => $value]);

    $result = app (API\TVManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['keycode' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('keycode.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
