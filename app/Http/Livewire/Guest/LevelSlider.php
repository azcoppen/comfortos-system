<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Services\ComfortOS\API;

class LevelSlider extends Component
{
  public $component;

  protected $managers   = [
    Devices\Plug::class           => API\PlugManager::class,
  ];

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'level'))->first();

      return view('guest.livewire.widgets.level_slider', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
  }

  public function command ( int $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'level', 'value' => $value]);

    $result = app ($this->managers [get_class ($this->component)])
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['level' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('level.updated', ['component' => $this->component->_id, 'value' => $value]);
  }

}
