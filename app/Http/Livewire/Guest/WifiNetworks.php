<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Models\Property\Room;

use SmartRooms\Services\ComfortOS\API;

class WifiNetworks extends Component
{
  public $room;

  public function mount (Room $room)
  {
      $this->room = $room;
  }

  public function render()
  {
      $networks = $this->room->wifi_networks()->orderBy('ssid', 'ASC')->get();

      return view('guest.livewire.widgets.wifi_networks', ['wifi_networks' => $networks]);
  }

}
