<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastRoomSignalCriteria;

use SmartRooms\Models\Property\Room;

class SensorReading extends Component
{
  public $room;
  public $type;

  public function mount (Room $room, string $type)
  {
      $this->room = $room;
      $this->type = $type;
  }

  public function value ($val)
  {
    if ( in_array ($this->type, ['motion', ]) )
    {
      if ( is_string ($val) && $val == 'ON' )
      {
        return __('guest/sensors.yes');
      }
      else
      {
        return __('guest/sensors.no');
      }
    }

    if ( in_array ($this->type, ['contact', ]) )
    {
      if ( is_string ($val) && $val == 'ON' )
      {
        return __('guest/sensors.open');
      }
      else
      {
        return __('guest/sensors.closed');
      }
    }

    if ( in_array ($this->type, ['smoke', 'co', 'flood']) )
    {
      return __('guest/sensors.clear');
    }

    if ( $val && !empty ($val) )
    {
      return $val;
    }

    return '<i data-feather="help-circle" class="text-warning"></i>';
  }

  public function suffix ()
  {
    switch ($this->type)
    {
      case 'temperature':
        return "&deg; F";
      break;

      case 'humidity':
      case 'luminance':
      case 'uv':
        return "%";
      break;

      case 'smoke':
      case 'co':
      case 'flood':
        return '<i data-feather="check-circle" class="text-success"></i>';
      break;
    }

    return '';
  }

  public function poll ()
  {
    switch ($this->type)
    {
      case 'motion':
      case 'contact':
        return 2;
      default:
        return 30;
      break;
    }
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastRoomSignalCriteria ($this->room, $this->type))->first();

      $data = [
        'type'       => $this->type,
        'last_value' => $this->value ($last_signal->value ?? null),
        'suffix'     => $this->suffix (),
        'poll'       => $this->poll (),
      ];

      $this->dispatchBrowserEvent ('sensor.rendering', $data);

      return view ('guest.livewire.widgets.sensor_reading', $data);
  }
}
