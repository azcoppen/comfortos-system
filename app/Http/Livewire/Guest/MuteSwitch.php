<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Services\ComfortOS\API;

class MuteSwitch extends Component
{
  public $component;

  protected $managers   = [
    Devices\Chromecast::class     => API\ChromecastManager::class,
    Devices\Speaker::class        => API\SpeakerManager::class,
    Devices\TV::class             => API\TVManager::class,
  ];

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'mute'))->first();

      return view('guest.livewire.widgets.mute_switch', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
  }

  public function command ( string $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'mute', 'value' => $value]);

    $result = app ($this->managers [get_class ($this->component)])
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['mute' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('mute.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
