<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Services\ComfortOS\API;

class PlayerButtons extends Component
{
  public $component;

  protected $managers   = [
    Devices\Chromecast::class     => API\ChromecastManager::class,
    Devices\Speaker::class        => API\SpeakerManager::class,
    Devices\TV::class             => API\TVManager::class,
  ];

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      return view('guest.livewire.widgets.player_buttons', ['component' => $this->component]);
  }

  public function command ( string $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'controls', 'value' => $value]);

    $result = app ($this->managers [get_class ($this->component)])
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['controls' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('controls.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
