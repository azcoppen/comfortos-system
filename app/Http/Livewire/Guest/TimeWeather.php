<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use \Cache;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastRoomSignalCriteria;

use SmartRooms\Models\Property\Room;

class TimeWeather extends Component
{
  public $room;

  public function mount (Room $room)
  {
      $this->room = $room;
  }

  public function render()
  {
      return view('guest.livewire.widgets.time_weather', ['weather' => Cache::get ($this->room->building->property->_id.'-weather')]);
  }
}
