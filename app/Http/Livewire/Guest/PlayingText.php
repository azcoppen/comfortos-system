<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

class PlayingText extends Component
{
    public $component;

    public function mount ($component)
    {
        $this->component = $component;
    }

    public function render()
    {
        $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'playing'))->first();

        return view('guest.livewire.widgets.playing_text', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
    }
}
