<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Services\ComfortOS\API;

class TVButton extends Component
{
  public $component;
  public $keycode;

  public function mount ($component, string $keycode)
  {
      $this->component = $component;
      $this->keycode = $keycode;
  }

  public function render()
  {
      return view('guest.livewire.widgets.tv_keycode_button', ['component' => $this->component, 'keycode' => $this->keycode ?? null]);
  }

  public function command ( string $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'keycode', 'value' => $value]);

    $result = app (API\TVManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['keycode' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('keycode.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
