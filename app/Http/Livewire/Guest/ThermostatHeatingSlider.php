<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Services\ComfortOS\API;

class ThermostatHeatingSlider extends Component
{
  public $component;

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'heating_point'))->first();

      return view('guest.livewire.widgets.thermostat_heating_slider', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
  }

  public function command ( int $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'heating_point', 'value' => $value]);

    $result = app (API\ThermostatManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['heating_point' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('heating_point.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
