<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Services\ComfortOS\API;

class TVChannelEntry extends Component
{
  public $component;

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'channel'))->first();

      $this->dispatchBrowserEvent ('channel.rendering', ['component' => $this->component->_id, 'value' => $last_signal->value ?? null]);

      return view('guest.livewire.widgets.tv_channel_entry', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
  }

  public function command ( string $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'channel', 'value' => $value]);

    $result = app (API\TVManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['channel' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('channel.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
