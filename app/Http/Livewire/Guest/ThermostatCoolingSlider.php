<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Services\ComfortOS\API;

class ThermostatCoolingSlider extends Component
{
  public $component;

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'cooling_point'))->first();

      return view('guest.livewire.widgets.thermostat_cooling_slider', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
  }

  public function command ( int $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'cooling_point', 'value' => $value]);

    $result = app (API\ThermostatManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['cooling_point' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('cooling_point.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
