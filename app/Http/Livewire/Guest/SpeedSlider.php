<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastSignalCriteria;

use SmartRooms\Services\ComfortOS\API;

class SpeedSlider extends Component
{
  public $component;

  public function mount ($component)
  {
      $this->component = $component;
  }

  public function render()
  {
      $last_signal = app (HABSignalRepository::class)->pushCriteria (new LastSignalCriteria ($this->component, 'speed'))->first();

      return view('guest.livewire.widgets.speed_slider', ['component' => $this->component, 'last_value' => $last_signal->value ?? null]);
  }

  public function command ( int $value )
  {
    $this->dispatchBrowserEvent ('command.sent', ['component' => $this->component->_id, 'attribute' => 'speed', 'value' => $value]);

    $result = app (API\MotorManager::class)
      ->src ('guest')
      ->user (auth()->user())
      ->entity ($this->component)
      ->params (['speed' => $value])
      ->update ();

    $this->dispatchBrowserEvent ('speed.updated', ['component' => $this->component->_id, 'value' => $value]);
  }
}
