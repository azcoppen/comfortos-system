<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

use \Cache;
use Illuminate\Support\Str;

use SmartRooms\Models\Property\Room;

class SessionRemaining extends Component
{
    public $model;
    public $inline;

    public function mount ($model, bool $inline = false)
    {
        $this->model = $model;
        $this->inline = $inline;
    }

    public function render()
    {
        if ( Cache::get ($this->model->_id.'-session-end') )
        {
          if ( time () > Cache::get ($this->model->_id.'-session-end')->timestamp )
          {

            switch (get_class ($this->model))
            {
              case Room::class:
                $this->redirect(route ('guest.room.logout', $this->model->_id));
              break;

              default:
                $this->redirect(route ('guest.component.'.Str::lower(Str::singular($this->model->plural)).'.logout', $this->model->_id));
              break;
            }


          }
        }

        return view('guest.livewire.widgets.session_lifetime', ['model' => $this->model, 'inline' => $this->inline]);
    }
}
