<?php

namespace SmartRooms\Http\Livewire\Guest;

use Livewire\Component;

class LockCodeGrid extends Component
{
    public $component;

    public $listeners = [];

    public function mount ($component)
    {
        $this->component = $component;
        $this->listeners[$this->component->_id.'.code.inserted'] = '$refresh';
        $this->listeners[$this->component->_id.'.code.updated'] = '$refresh';
        $this->listeners[$this->component->_id.'.code.removed'] = '$refresh';
    }

    public function render ()
    {
        $codes = $this->component->codes ()->orderBy('index', 'ASC')->get();

        return view ('guest.livewire.widgets.lock_code_grid', ['lock' => $this->component, 'codes' => $codes]);
    }
}
