<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;
use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertRepository;

class MIATable extends Component
{
    private $alerts;

    public function render()
    {
        $this->alerts = app (MIAAlertRepository::class)
        ->scopeQuery(function($query) {
            return $query->orderBy('created_at','DESC')->take (100);
        })->paginate(100);

        return view('partials.components.panels.mia', ['alerts' => $this->alerts]);
    }
}
