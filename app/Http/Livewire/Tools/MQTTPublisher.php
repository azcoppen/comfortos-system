<?php

namespace SmartRooms\Http\Livewire\Tools;

use Livewire\Component;

use SmartRooms\Contracts\System\MQTTPublishingContract;
use SmartRooms\Contracts\Repositories\Components\OS\MQTTBrokerRepository;
use SmartRooms\Models\Components\OS\MQTTBroker;

class MQTTPublisher extends Component
{
    private $mqtt_broker;
    public $broker;
    public $topic;
    public $payload;

    public function render()
    {
        return view ('dashboards.tools.livewire.mqtt_form', [
          'mqtt_brokers' => app (MQTTBrokerRepository::class)->all(),
        ]);
    }

    public function send ()
    {
      $this->validate ([
          'broker'    => 'required|present|filled|string|exists:'.MQTTBroker::class.',_id',
          'topic'     => 'required|present|filled|string',
          'payload'   => 'required|present|filled',
      ]);

      $mqtt_broker = app (MQTTBrokerRepository::class)->find ($this->broker);

      app (MQTTPublishingContract::class)->broker ($mqtt_broker)->send ($this->topic, (string) $this->payload);
    }
}
