<?php

namespace SmartRooms\Http\Livewire\Tools;

use Livewire\Component;
use Illuminate\Support\Facades\Http;
use \Exception;

class HTTPPoster extends Component
{
    public $url;
    public $auth_user;
    public $auth_pass;
    public $payload;
    private $request;

    public $listeners = ['finished' => 'render'];

    public $response = [
      'display'     => false,
      'status'      => null,
      'successful'  => null,
      'failed'      => null,
      'serverError' => null,
      'clientError' => null,
      'headers'     => null,
      'body'        => null,
    ];

    public function mount ()
    {
      $this->response = [
        'display'     => is_object ($this->request) ? true: false,
        'status'      => is_object ($this->request) ? $this->request->status() : null,
        'successful'  => is_object ($this->request) ? $this->request->successful() : null,
        'failed'      => is_object ($this->request) ? $this->request->failed() : null,
        'serverError' => is_object ($this->request) ? $this->request->serverError() : null,
        'clientError' => is_object ($this->request) ? $this->request->clientError() : null,
        'headers'     => is_object ($this->request) ? $this->request->headers() : null,
        'body'        => is_object ($this->request) ? $this->request->body() : null,
      ];
    }

    public function render()
    {
      $this->response = [
        'display'     => is_object ($this->request) ? true: false,
        'status'      => is_object ($this->request) ? $this->request->status() : null,
        'successful'  => is_object ($this->request) ? $this->request->successful() : null,
        'failed'      => is_object ($this->request) ? $this->request->failed() : null,
        'serverError' => is_object ($this->request) ? $this->request->serverError() : null,
        'clientError' => is_object ($this->request) ? $this->request->clientError() : null,
        'headers'     => is_object ($this->request) ? $this->request->headers() : null,
        'body'        => is_object ($this->request) ? $this->request->body() : null,
      ];
      return view ('dashboards.tools.livewire.http_post_form', ['response' => $this->response]);
    }

    public function send ()
    {
      $this->validate ([
          'url'       => 'required|present|filled|url',
          'auth_user' => 'nullable|present|string',
          'auth_pass' => 'nullable|present|string',
          'payload'   => 'required|present|filled',
      ]);

      $this->request = Http::timeout(5)->withOptions([
          'debug'   => false,
          'verify'  => false, // OpenHAB's SSL is self-signed
      ])->withHeaders ([
        'User-Agent'    => 'smartrooms/1.0',
        'Accept'        => 'application/json',
      ]);

      if ( !empty ($this->auth_user) && !empty ($this->auth_pass) )
      {
        $this->request = $this->request->withBasicAuth ($this->auth_user, $this->auth_pass);
      }

      try
      {
        $this->request = $this->request
          ->withBody ((string)$this->payload, 'text/plain')
          ->post ($this->url);

        $this->response = [
          'display'     => is_object ($this->request) ? true: false,
          'status'      => is_object ($this->request) ? $this->request->status() : null,
          'successful'  => is_object ($this->request) ? $this->request->successful() : null,
          'failed'      => is_object ($this->request) ? $this->request->failed() : null,
          'serverError' => is_object ($this->request) ? $this->request->serverError() : null,
          'clientError' => is_object ($this->request) ? $this->request->clientError() : null,
          'headers'     => is_object ($this->request) ? $this->request->headers() : null,
          'body'        => is_object ($this->request) ? $this->request->body() : null,
        ];

      }
      catch (Exception $e)
      {

      }

      $this->emit('finished');
    }
}
