<?php

namespace SmartRooms\Http\Livewire\OTP;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;
use SmartRooms\Criteria\ComfortOS\OperatorsAvailableToUser;

use SmartRooms\Models\Property\Room;
use SmartRooms\Contracts\System\OTPGenerationContract;

class RoomCode extends Component
{
  public $room;
  public $activation = 60;
  public $hours = 24;
  public $code;

  protected $listeners  = ['room' => 'room'];

  protected $rules = [
      'activation' => 'required|numeric|min:1|max:200',
      'hours'      => 'required|numeric|min:1|max:200',
  ];

  public function room ( string $room_id )
  {
    $this->room = Room::find ($room_id);
    $this->code = null;
  }

  public function render()
  {
      return view ('partials.components.widgets.otp.room_code');
  }

  public function generate ()
  {
    $this->code = null;

    $this->validate();

    $fh = $this->hours < 10 ? '0'. $this->hours : $this->hours;

    $this->code = $this->activation . 'M' . app (OTPGenerationContract::class)->create ($this->room->_id, 6, intval ($this->activation) * 60) . 'S'.$fh;
  }
}
