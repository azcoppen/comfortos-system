<?php

namespace SmartRooms\Http\Livewire\OTP;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;
use SmartRooms\Criteria\ComfortOS\OperatorsAvailableToUser;

use SmartRooms\Models\Property\Room;
use SmartRooms\Contracts\System\OTPGenerationContract;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Models\Components\Mirror;
use SmartRooms\Models\Components\Telecoms;
use SmartRooms\Models\Components\Wifi;

class ComponentCode extends Component
{
  public $group = 'bulbs';
  public $component;
  public $activation = 60;
  public $hours = 24;
  public $code;

  private $cpt_model_map = [
    'bulbs'       => Devices\Bulb::class,
    'cameras'     => Devices\Camera::class,
    'chromecasts' => Devices\Chromecast::class,
    'dects'       => Telecoms\DECT::class,
    'dimmers'     => Devices\Dimmer::class,
    'extensions'  => Telecoms\Extension::class,
    'handsets'    => Telecoms\Handset::class,
    'leds'        => Devices\LED::class,
    'locks'       => Devices\Lock::class,
    'mirrors'     => Mirror\Display::class,
    'motors'      => Devices\Motor::class,
    'plugs'       => Devices\Plug::class,
    'routers'     => Wifi\Router::class,
    'sensors'     => Devices\Sensor::class,
    'speakers'    => Devices\Speaker::class,
    'stbs'        => Devices\STB::class,
    'switches'    => Devices\PhysicalSwitch::class,
    'thermostats' => Devices\Thermostat::class,
    'tvs'         => Devices\TV::class,
    'wifi'        => Wifi\Network::class,
  ];

  protected $listeners  = [
    'group' => 'group',
    'cpt'   => 'cpt',
  ];

  protected $rules = [
      'activation' => 'required|numeric|min:1|max:200',
      'hours'      => 'required|numeric|min:1|max:200',
  ];

  public function group ( string $group )
  {
    $this->group = $group;
    $this->component = null;
    $this->code = null;
  }

  public function cpt ( string $cpt_id )
  {
    if (! array_key_exists ($this->group, $this->cpt_model_map) )
    {
      return false;
    }

    $this->component = app($this->cpt_model_map[$this->group])::find ($cpt_id);
    $this->code = null;
  }

  public function render()
  {
      return view ('partials.components.widgets.otp.cpt_code');
  }

  public function generate ()
  {
    $this->code = null;

    $this->validate();

    $fh = $this->hours < 10 ? '0'. $this->hours : $this->hours;

    $this->code = $this->activation . 'M' . app (OTPGenerationContract::class)->create ($this->component->_id, 6, intval ($this->activation) * 60) . 'S'.$fh;
  }
}
