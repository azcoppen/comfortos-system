<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;

class RoomSignalsTable extends Component
{
    public $room;

    public function mount ($room)
    {
        $this->room = $room;
    }

    public function render()
    {
        $this->signals = app (HABSignalRepository::class)
        ->scopeQuery(function($query) {
            return $query->orderBy('received_at','DESC')->take (50);
        })->findWhere (['room_id' => $this->room]);

        return view('partials.components.panels.signals', ['signals' => $this->signals]);
    }
}
