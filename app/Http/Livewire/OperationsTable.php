<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;
use SmartRooms\Contracts\Repositories\ComfortOS\OpQueueRepository;

class OperationsTable extends Component
{

    public function render()
    {
        $this->operations = app (OpQueueRepository::class)
        ->scopeQuery(function($query) {
            return $query->orderBy('created_at','DESC')->take (50);
        })->all();

        return view('partials.components.panels.operations', ['operations' => $this->operations]);
    }
}
