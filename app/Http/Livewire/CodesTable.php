<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;

class CodesTable extends Component
{
    public $component;

    public function mount ($component)
    {
        $this->component = $component->load (['codes']);
    }

    public function render()
    {
        return view('partials.components.panels.codes', ['codes' => $this->component->codes]);
    }
}
