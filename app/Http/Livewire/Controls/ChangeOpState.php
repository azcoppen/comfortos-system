<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeOpState extends HABComponent
{
  public $attribute     = 'opstate';
  public $view_suffix   = 'op_state';
}
