<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeChannel extends HABComponent
{
    public $attribute     = 'channel';
    public $view_suffix   = 'channel';
}
