<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeFanMode extends HABComponent
{
  public $attribute     = 'fan_mode';
  public $view_suffix   = 'fan_mode';
}
