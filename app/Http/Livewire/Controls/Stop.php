<?php

namespace SmartRooms\Http\Livewire\Controls;

class Stop extends HABComponent
{
  public $attribute     = 'stop';
  public $view_suffix   = 'stop';
}
