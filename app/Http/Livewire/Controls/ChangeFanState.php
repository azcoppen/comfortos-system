<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeFanState extends HABComponent
{
  public $attribute     = 'fan_state';
  public $view_suffix   = 'fan_state';
}
