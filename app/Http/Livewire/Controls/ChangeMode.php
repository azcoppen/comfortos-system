<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeMode extends HABComponent
{
  public $attribute     = 'mode';
  public $view_suffix   = 'mode';
}
