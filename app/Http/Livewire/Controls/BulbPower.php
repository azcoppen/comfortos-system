<?php

namespace SmartRooms\Http\Livewire\Controls;

class BulbPower extends HABComponent
{
    /*
    In OpenHAB, color objects can be sent a switch ON/OFF command, but they set
    the brightness to 0 or 100, so the component is never truly "off". When we
    send commands, we need to write to the switch. When we want to know the statys,
    we have read the HSB signal to determine the brightness.
    */
    
    public $attribute     = 'hsb';
    public $write_attribute = 'power';
    public $view_suffix   = 'hsb_power';
}
