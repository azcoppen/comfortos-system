<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeLevel extends HABComponent
{
    public $attribute     = 'level';
    public $view_suffix   = 'level';
}
