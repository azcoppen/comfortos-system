<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeDimmerState extends HABComponent
{
  public $attribute     = 'state';
  public $view_suffix   = 'dimmer';
}
