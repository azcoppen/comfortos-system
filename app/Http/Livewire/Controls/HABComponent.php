<?php

namespace SmartRooms\Http\Livewire\Controls;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\ComfortOS\LastAttributeValueCriteria;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Models\Components\Mirror;
use SmartRooms\Models\Components\Wifi;
use SmartRooms\Services\ComfortOS\API;

use SmartRooms\Contracts\System\FlowOutputContract;

abstract class HABComponent extends Component
{
    public $component;
    public $additional;
    public $attribute;
    public $view_suffix;
    public $last_signal;
    public $last_value;

    protected $managers   = [
      Devices\Bulb::class           => API\BulbManager::class,
      Devices\Camera::class         => API\CameraManager::class,
      Devices\Chromecast::class     => API\ChromecastManager::class,
      Devices\Dimmer::class         => API\DimmerManager::class,
      Devices\LED::class            => API\LEDManager::class,
      Devices\Lock::class           => API\LockManager::class,
      Mirror\Display::class         => API\MirrorManager::class,
      Devices\Motor::class          => API\MotorManager::class,
      Devices\Plug::class           => API\PlugManager::class,
      Devices\Speaker::class        => API\SpeakerManager::class,
      Devices\STB::class            => API\STBManager::class,
      Devices\PhysicalSwitch::class => API\SwitchManager::class,
      Devices\Thermostat::class     => API\ThermostatManager::class,
      Devices\TV::class             => API\TVManager::class,
      Wifi\Network::class           => API\WifiManager::class,
    ];

    public $speaker_modes = [
      'STANDBY'         => 'standby',
      'INTERNET_RADIO'  => 'radio',
      'BLUETOOTH'       => 'bluetooth',
      'STORED_MUSIC'    => 'stored',
      'AUX'             => 'aux',
      'SPOTIFY'         => 'spotify',
      'PANDORA'         => 'pandora',
      'DEEZER'          => 'deezer',
      'SIRIUSXM'        => 'sirius',
      'AMAZON'          => 'amazon',
    ];

    public function mount ($component, $additional = null)
    {
        $this->component = $component;
        $this->additional = $additional;
        $this->last_signal = app (HABSignalRepository::class)
          ->pushCriteria (new LastAttributeValueCriteria ($this->component, $this->attribute))
          ->first();
        $this->last_value = is_object ($this->last_signal) ? $this->last_signal->value : null;
    }

    public function render()
    {
        $this->last_signal = app (HABSignalRepository::class)
          ->pushCriteria (new LastAttributeValueCriteria ($this->component, $this->attribute))
          ->first();

        $this->last_value = is_object ($this->last_signal) ? $this->last_signal->value : null;

        $this->dispatchBrowserEvent($this->attribute.'.rendering', ['last_value' =>  $this->last_value]);

        if ( get_class ($this) == BulbPower::class )
        {
          $this->dispatchBrowserEvent('bulb.power', ['value' => $this->last_value]);
        }

        return view('components.controls.'.$this->view_suffix, ['component' => $this->component, 'additional' => $this->additional, 'last_signal' => $this->last_signal, 'last_value' => $this->last_value]);
    }

    public function command ($value)
    {
      app (FlowOutputContract::class)->signal ('[Livewire] Command: '.$this->component->plural.' / '.$this->component->_id.' ['.(isset ($this->write_attribute) ? $this->write_attribute: $this->attribute).' => '.(is_string ($value) ? $value : json_encode ($value)) .']');

      $this->dispatchBrowserEvent('command.sent', ['component' => $this->component->_id, 'attribute' => (isset ($this->write_attribute) ? $this->write_attribute: $this->attribute), 'value' => $value]);

      $this->last_value = $value;

      $result = app ($this->managers [get_class ($this->component)])
        ->src ('dash')
        ->user (auth()->user())
        ->entity ($this->component)
        ->params ([(isset ($this->write_attribute) ? $this->write_attribute: $this->attribute) => $value])
        ->update ();

      $this->dispatchBrowserEvent((isset ($this->write_attribute) ? $this->write_attribute: $this->attribute).'.updated', ['last_value' => $this->last_value]);
    }
}
