<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangePreset extends HABComponent
{
  public $attribute     = 'preset';
  public $view_suffix   = 'preset';
}
