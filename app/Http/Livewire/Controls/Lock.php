<?php

namespace SmartRooms\Http\Livewire\Controls;

class Lock extends HABComponent
{
  public $attribute     = 'lock';
  public $view_suffix   = 'lock';
}
