<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeSpeed extends HABComponent
{
    public $attribute     = 'speed';
    public $view_suffix   = 'speed';
}
