<?php

namespace SmartRooms\Http\Livewire\Controls;

class InsertCode extends HABComponent
{
    public $attribute     = 'codes';
    public $view_suffix   = 'insert_code';
}
