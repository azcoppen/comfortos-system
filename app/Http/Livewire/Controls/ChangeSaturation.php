<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeSaturation extends HABComponent
{
  public $attribute     = 'saturation';
  public $view_suffix   = 'saturation';
}
