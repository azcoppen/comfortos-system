<?php

namespace SmartRooms\Http\Livewire\Controls;

class Mute extends HABComponent
{
  public $attribute     = 'mute';
  public $view_suffix   = 'mute';
}
