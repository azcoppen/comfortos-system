<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeHeatingPoint extends HABComponent
{
  public $attribute     = 'heating_point';
  public $view_suffix   = 'heating_point';
}
