<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeSharpness extends HABComponent
{
  public $attribute     = 'sharpness';
  public $view_suffix   = 'sharpness';
}
