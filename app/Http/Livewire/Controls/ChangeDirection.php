<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeDirection extends HABComponent
{
  public $attribute   = 'dir';
  public $view_suffix = 'dir';
}
