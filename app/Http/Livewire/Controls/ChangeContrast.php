<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeContrast extends HABComponent
{
  public $attribute     = 'contrast';
  public $view_suffix   = 'contrast';
}
