<?php

namespace SmartRooms\Http\Livewire\Controls;

class Player extends HABComponent
{
  public $attribute     = 'controls';
  public $view_suffix   = 'player';
}
