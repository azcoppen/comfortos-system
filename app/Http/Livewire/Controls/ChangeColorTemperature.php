<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeColorTemperature extends HABComponent
{
  public $attribute     = 'temperature';
  public $view_suffix   = 'color_temperature';
}
