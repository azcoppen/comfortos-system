<?php

namespace SmartRooms\Http\Livewire\Controls;

class Power extends HABComponent
{
  public $attribute     = 'power';
  public $view_suffix   = 'power';
}
