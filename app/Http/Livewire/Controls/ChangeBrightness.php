<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeBrightness extends HABComponent
{
    public $attribute     = 'brightness';
    public $view_suffix   = 'brightness';
}
