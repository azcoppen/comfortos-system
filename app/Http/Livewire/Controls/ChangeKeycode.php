<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeKeycode extends HABComponent
{
    public $attribute     = 'keycode';
    public $view_suffix   = 'keycode';
}
