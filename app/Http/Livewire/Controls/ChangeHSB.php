<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeHSB extends HABComponent
{
    public $attribute     = 'hsb';
    public $view_suffix   = 'hsb';

    protected $listeners  = ['command' => 'command'];
}
