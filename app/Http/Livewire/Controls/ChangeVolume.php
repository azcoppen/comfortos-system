<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeVolume extends HABComponent
{
    public $attribute     = 'volume';
    public $view_suffix   = 'volume';
}
