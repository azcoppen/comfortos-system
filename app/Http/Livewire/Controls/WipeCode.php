<?php

namespace SmartRooms\Http\Livewire\Controls;

class WipeCode extends HABComponent
{
    public $attribute   = 'codes';
    public $view_suffix = 'wipe_code';
}
