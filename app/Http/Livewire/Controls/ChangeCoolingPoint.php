<?php

namespace SmartRooms\Http\Livewire\Controls;

class ChangeCoolingPoint extends HABComponent
{
  public $attribute     = 'cooling_point';
  public $view_suffix   = 'cooling_point';
}
