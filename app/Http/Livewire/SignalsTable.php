<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;

class SignalsTable extends Component
{
    public $component;

    public function mount ($component)
    {
        $this->component = $component;
    }

    public function render()
    {
        $this->signals = app (HABSignalRepository::class)
        ->scopeQuery(function($query) {
            return $query->orderBy('received_at','DESC')->take (50);
        });

        if ( $this->component && $this->component != 'all' )
        {
          $this->signals = $this->signals->findWhere (['component_id' => $this->component]);
        }
        else
        {
          $this->signals = $this->signals->all();
        }

        return view('partials.components.panels.signals', ['signals' => $this->signals]);
    }
}
