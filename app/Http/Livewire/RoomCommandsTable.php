<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;
use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;

class RoomCommandsTable extends Component
{
    public $room;

    public function mount ($room)
    {
        $this->room = $room;
    }

    public function render()
    {
        $this->commands = app (HABCommandRepository::class)
        ->scopeQuery(function($query) {
            return $query->orderBy('created_at','DESC')->take (50);
        })->findWhere (['room_id' => $this->room]);

        return view('partials.components.panels.commands', ['commands' => $this->commands]);
    }
}
