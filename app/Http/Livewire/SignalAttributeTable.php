<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Criteria\WhereFieldEqualsCriteria;

class SignalAttributeTable extends Component
{
    public $component;
    public $attribute;

    public function mount ($component, string $attribute)
    {
      $this->component = $component;
      $this->attribute = $attribute;
    }

    public function render()
    {
        $this->signals = app (HABSignalRepository::class)
        ->pushCriteria (new WhereFieldEqualsCriteria ('component_id', $this->component->_id) )
        ->pushCriteria (new WhereFieldEqualsCriteria ('attribute', $this->attribute == 'bulb-power' ? 'hsb' : $this->attribute) )
        ->scopeQuery(function($query) {
            return $query->orderBy('received_at','DESC')->take (100);
        })->all();

        return view('partials.components.panels.signals_attribute', ['component' => $this->component, 'attribute' => $this->attribute, 'signals' => $this->signals]);
    }
}
