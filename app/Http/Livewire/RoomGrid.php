<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class RoomGrid extends Component
{
    public $building;
    public $menu_floor;

    public $q = '';
    public $page = 1;

    protected $queryString = [
        'q'    => ['except' => ''],
        'page' => ['except' => 1],
    ];

    public function mount ($building, $menu_floor = 0)
    {
        $this->building = $building;
        $this->menu_floor = $menu_floor;

        $this->fill(request()->only('q', 'page'));
    }

    public function render()
    {
      $this->unique_floors = $this->building->rooms()->get()->pluck('floor')->unique()->sort();

      if (! $this->q )
      {
        $this->building->load(['rooms' => function ($q) {
          return $q->where ('floor', intval($this->menu_floor));
        }]);

        $this->building->rooms->each->load (['building.property.brand.operator', 'commands', 'signals']);
        $this->building->rooms->each->setAppends(['summary']);

        $rooms = $this->building->rooms;
      }

      if ( $this->q )
      {
        $rooms = app (app(RoomRepository::class)->model())::search (format_search_query($this->q))
          ->with (['building.property.brand.operator', 'commands', 'signals'])
          ->where ('building_id', $this->building->_id)
          ->get ()
          ->each->setAppends(['summary']);
      }

      return view('explore.operators.brands.properties.buildings.room_grid', ['building' => $this->building, 'unique_floors' => $this->unique_floors, 'rooms' => $rooms, 'menu_floor' => $this->menu_floor]);
    }
}
