<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;

use SmartRooms\Contracts\Repositories\Components\Devices;
use SmartRooms\Contracts\Repositories\Components\Mirror;
use SmartRooms\Contracts\Repositories\Components\OS;
use SmartRooms\Contracts\Repositories\Components\Telecoms;
use SmartRooms\Contracts\Repositories\Components\Wifi;

use SmartRooms\Contracts\System\FlowOutputContract;

class RoomComponentPanel extends Component
{
    private $repositories = [
      'bulbs'         => Devices\BulbRepository::class,
      'cameras'       => Devices\CameraRepository::class,
      'chromecasts'   => Devices\ChromecastRepository::class,
      'dects'         => Telecoms\DECTRepository::class,
      'dimmers'       => Devices\DimmerRepository::class,
      'extensions'    => Telecoms\ExtensionRepository::class,
      'handsets'      => Telecoms\HandsetRepository::class,
      'leds'          => Devices\LEDRepository::class,
      'locks'         => Devices\LockRepository::class,
      'mirrors'       => Mirror\DisplayRepository::class,
      'motors'        => Devices\MotorRepository::class,
      'switches'      => Devices\PhysicalSwitchRepository::class,
      'plugs'         => Devices\PlugRepository::class,
      'routers'       => Wifi\RouterRepository::class,
      'sensors'       => Devices\SensorRepository::class,
      'speakers'      => Devices\SpeakerRepository::class,
      'stbs'          => Devices\STBRepository::class,
      'thermostats'   => Devices\ThermostatRepository::class,
      'tvs'           => Devices\TVRepository::class,
      'wifi'          => Wifi\NetworkRepository::class,
    ];

    public $room;
    public $group;
    public $object_id;
    public $component;
    public $ready = false;

    public function mount ($room, $group, $object_id)
    {
      $this->room       = $room;
      $this->group      = $group;
      $this->object_id  = $object_id;
    }

    public function load ()
    {
      $this->ready = true;
    }

    public function render()
    {

      $this->component = $this->ready && isset ($this->repositories[$this->group]) && $this->object_id ?
        app ($this->repositories[$this->group])
          ->with (['last_command', 'last_signal',])
          ->findWhere (['_id' => $this->object_id])
          ->first()
        : null;

      return view ('explore.operators.brands.properties.buildings.rooms.component_panel', [
        'room'       => $this->room,
        'group'      => $this->group,
        'component'  => $this->component,
      ]);

    }
}
