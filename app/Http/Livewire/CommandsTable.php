<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;
use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;

class CommandsTable extends Component
{
    public $component;

    public function mount ($component)
    {
        $this->component = $component;
    }

    public function render()
    {
        $this->commands = app (HABCommandRepository::class)
          ->scopeQuery(function($query) {
              return $query->orderBy('created_at','DESC')->take (50);
          });

        if ( $this->component && $this->component != 'all' )
        {
          $this->commands = $this->commands->findWhere (['component_id' => $this->component]);
        }
        else
        {
          $this->commands = $this->commands->all();
        }

        return view('partials.components.panels.commands', ['commands' => $this->commands]);
    }
}
