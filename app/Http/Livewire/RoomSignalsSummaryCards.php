<?php

namespace SmartRooms\Http\Livewire;

use Livewire\Component;
use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;

class RoomSignalsSummaryCards extends Component
{
    public $room;
    public $summary;

    public function mount ($room)
    {
        $this->room = $room;
    }

    public function render()
    {
        $this->summary = $this->room->signals()->get()->groupBy ('attribute')->transform(function ($item, $key) {
          if ( is_object ($item) && is_object ($item->first()) )
          {
            return $item->first();
          }
          return null;
        })->sortKeys();

        return view('partials.components.panels.room_signal_summary', ['summary' => $this->summary]);
    }
}
