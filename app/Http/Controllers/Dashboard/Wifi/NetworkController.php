<?php

namespace SmartRooms\Http\Controllers\Dashboard\Wifi;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Wifi\Network;

use SmartRooms\Http\Requests\Dashboard\StoreNewWifiNetwork;
use SmartRooms\Http\Requests\Dashboard\UpdateWifiNetwork;

use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\WifiNetworkManagementContract;

final class NetworkController extends BaseController
{
    public $repository = NetworkRepository::class;
    public $app_section = 'wifi';
    public $nav_section = 'networks';
    public $view_subdir = 'wifi.ssids.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Network::class);

        try {

          $records = $this->manager ($request)
            ->with ([
                'room.building.property',
            ]);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate(25),
          ]);
          
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Network::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'routers' => app (RouterRepository::class)->all (),
              'users'   => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewWifiNetwork $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, Network::class);

        try {

          app (WifiNetworkManagementContract::class)
            ->initiator ($request->user())
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('wifi.ssids.index')
            ->with ('success', 'Network created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, Network $ssid) : View
    {
        $this->authorize (__FUNCTION__, $ssid);

        try {

            $ssid->load (['router.room.building.property']);

            return $this->view ($this->view_subdir.__FUNCTION__, [
              'network' => $ssid,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Network $ssid) : View
    {
        $this->authorize (__FUNCTION__, $ssid);

        try {

          return $this->view ($this->view_subdir.'create_edit', [
            'network' => $ssid,
            'routers' => app (RouterRepository::class)->all (),
            'users'   => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateWifiNetwork $request, Network $ssid) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $ssid);

        try {

          app (WifiNetworkManagementContract::class)
            ->initiator ($request->user())
            ->entity ($ssid)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('wifi.ssids.index')
            ->with ('success', 'Network updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Network $ssid) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $ssid);

        try {

          app (WifiNetworkManagementContract::class)
            ->initiator ($request->user())
            ->entity ($ssid)
            ->destroy ();

          return redirect ()->route ('wifi.ssids.index')
            ->with ('success', 'Network deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
