<?php

namespace SmartRooms\Http\Controllers\Dashboard\Wifi;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Wifi\Router;

use SmartRooms\Http\Requests\Dashboard\StoreNewRouter;
use SmartRooms\Http\Requests\Dashboard\UpdateRouter;

use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\RouterManagementContract;

final class RouterController extends BaseController
{
    public $repository = RouterRepository::class;
    public $app_section = 'wifi';
    public $nav_section = 'routers';
    public $view_subdir = 'wifi.routers.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Router::class);

        try {

          $records = $this->manager ($request)
            ->with ([
                'room.building.property',
            ]);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate(25),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Router::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'vpn_clients'    => app (VPNClientRepository::class)->all(),
              'vpn_servers'    => app (VPNServerRepository::class)->all(),
              'users'          => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewRouter $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, Router::class);

        try {

          app (RouterManagementContract::class)
            ->initiator ($request->user())
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('wifi.routers.index')
            ->with ('success', 'Router created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, Router $router) : View
    {
        $this->authorize (__FUNCTION__, $router);

        try {

            $router->load (['networks', 'room.building.property', 'vpn_clients', 'vpn_servers', 'purchaser', 'installer', 'provisioner',]);

            return $this->view ($this->view_subdir.__FUNCTION__, compact('router'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Router $router) : View
    {
        $this->authorize (__FUNCTION__, $router);

        try {

          return $this->view ($this->view_subdir.'create_edit', [
            'router'         => $router,
            'vpn_clients'    => app (VPNClientRepository::class)->all(),
            'vpn_servers'    => app (VPNServerRepository::class)->all(),
            'users'          => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateRouter $request, Router $router) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $router);

        try {

          app (RouterManagementContract::class)
            ->initiator ($request->user())
            ->entity ($router)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('wifi.routers.index')
            ->with ('success', 'Router updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Router $router) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $router);

        try {

          app (RouterManagementContract::class)
            ->initiator ($request->user())
            ->entity ($router)
            ->destroy ();

          return redirect ()->route ('wifi.routers.index')
            ->with ('success', 'Router deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
