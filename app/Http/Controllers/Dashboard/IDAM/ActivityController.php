<?php

namespace SmartRooms\Http\Controllers\Dashboard\IDAM;

use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\IDAM\ActivityRepository;
use SmartRooms\Http\Controllers\Dashboard\BaseController;

final class ActivityController extends BaseController
{
    public $app_section = 'idam';
    public $nav_section = 'activity';
    public $repository = ActivityRepository::class;
    public $view_subdir = 'idam.activity.';

    public function __construct()
    {
        //parent::__construct();
    }
}
