<?php

namespace SmartRooms\Http\Controllers\Dashboard\IDAM;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use SmartRooms\Contracts\Repositories\IDAM\PermissionRepository;
use SmartRooms\Criteria\OrderByCriteria;
use SmartRooms\Http\Controllers\Dashboard\BaseController;

final class PermissionController extends BaseController
{
    public $app_section = 'idam';
    public $nav_section = 'permissions';
    public $repository = PermissionRepository::class;
    public $view_subdir = 'idam.permissions.';

    public function index(Request $request) : View
    {
        try {
            $prepare = app($this->repository)
        ->pushCriteria(new OrderByCriteria('name', 'ASC'));

            return $this->view($this->view_subdir.__FUNCTION__, [
        'records' => $prepare->paginate(25),
      ]);
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
