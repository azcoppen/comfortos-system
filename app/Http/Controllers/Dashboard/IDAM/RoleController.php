<?php

namespace SmartRooms\Http\Controllers\Dashboard\IDAM;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use SmartRooms\Contracts\Repositories\IDAM\RoleRepository;
use SmartRooms\Criteria\OrderByCriteria;
use SmartRooms\Http\Controllers\Dashboard\BaseController;

final class RoleController extends BaseController
{
    public $app_section = 'idam';
    public $nav_section = 'roles';
    public $repository = RoleRepository::class;
    public $view_subdir = 'idam.roles.';

    public function index(Request $request) : View
    {
        try {
            $prepare = app($this->repository)
          ->pushCriteria(new OrderByCriteria('name', 'ASC'));

            return $this->view($this->view_subdir.__FUNCTION__, [
          'records' => $prepare->paginate(25),
        ]);
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
