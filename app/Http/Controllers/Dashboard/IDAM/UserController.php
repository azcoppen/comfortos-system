<?php

namespace SmartRooms\Http\Controllers\Dashboard\IDAM;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Http\Requests\Dashboard\IDAM\StoreUser;

use SmartRooms\Http\Requests\Dashboard\IDAM\UpdateUser;

use SmartRooms\Contracts\Repositories\IDAM\RoleRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\System\UserManagementContract;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\ComfortOS\OtherUsersAvailableToUser;
use SmartRooms\Criteria\OrderByCriteria;
use SmartRooms\Criteria\WhereFieldEqualsCriteria;

use SmartRooms\Models\IDAM\User;

final class UserController extends BaseController
{
    public $app_section = 'idam';
    public $nav_section = 'users';
    public $sub_section = 'basics';
    public $repository = UserRepository::class;
    public $view_subdir = 'idam.users.';

    public function index (Request $request, ?string $tag = null) : View
    {
        $this->authorize (__FUNCTION__, User::class);

        try {

          $records = $this->manager ($request)
            ->with ([
                'roles', 'permissions', 'rooms',
            ]);

          if (! $request->has ('q') )
          {
            $records
              ->pushCriteria (new OrderByCriteria('created_at', 'DESC'))
              ->pushCriteria (new OtherUsersAvailableToUser ($request->user()));

              if ( $tag )
              {
                $records->pushCriteria ( new HasRolesCriteria ([Str::singular($tag)]) );
                $this->nav_section = $tag;
              }
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate(25),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, User::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [

            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (Request $request) : View
    {
        $this->authorize (__FUNCTION__, User::class);

        try {

          app (UserManagementContract::class)
              ->dashboard ()
              ->initiator ($request->user())
              ->store ($request->except(['_method', '_token']));

          return redirect()->back()->withInput()->with ('success', 'User created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, User $user) : View
    {
        $this->authorize (__FUNCTION__, $user);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'record' => $user,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateUser $request, User $user)
    {
        $this->authorize (__FUNCTION__, $user);

        try {

            app (UserManagementContract::class)
                ->dashboard ()
                ->initiator ($request->user())
                ->entity ($user)
                ->update ($request->except(['_method', '_token']));

            return redirect()->back()->withInput()->with ('success', 'User updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, User $user) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $user);

        try {

          app (UserManagementContract::class)
            ->dashboard ()
            ->entity ($user)
            ->destroy ();

          return redirect ()->route ('idam.users.index')
            ->with ('success', 'User deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
