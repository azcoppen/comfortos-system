<?php

namespace SmartRooms\Http\Controllers\Dashboard\Explore;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Contracts\Repositories\Property\PropertyRepository;
use SmartRooms\Contracts\Repositories\Components\OS\MQTTBrokerRepository;
use SmartRooms\Contracts\Repositories\Components\OS\WSBrokerRepository;

use SmartRooms\Http\Requests\Dashboard\StoreNewProperty;
use SmartRooms\Http\Requests\Dashboard\UpdateProperty;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Operator\Brand;
use SmartRooms\Models\Operator\Operator;
use SmartRooms\Models\Property\Property;

use SmartRooms\Contracts\System\PropertyManagementContract;

final class PropertyController extends BaseController
{
    public $repository  = PropertyRepository::class;
    public $app_section = 'explorer';
    public $view_subdir = 'explore.operators.brands.properties.';

    public function index (Request $request, Operator $operator, Brand $brand) : View
    {
        $this->authorize (__FUNCTION__, [Property::class, $brand]);

        try {

          if (! $request->has ('q') )
          {
            $brand->load (['properties.buildings']);
            $records = $brand->properties;
          }

          if ( $request->has ('q') )
          {
            $records = $this->manager ($request, ['brand_id' => $brand->_id])
              ->with (['buildings'])
              ->paginate (25);
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'menu_operator'   => $operator,
            'menu_brand'      => $brand,
            'menu_properties' => $records,
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request, Operator $operator, Brand $brand) : View
    {
        $this->authorize (__FUNCTION__, [Property::class, $brand]);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'menu_operator'   => $operator,
              'menu_brand'      => $brand,
              'mqtt_brokers'    => app (MQTTBrokerRepository::class)->all(),
              'ws_brokers'      => app (WSBrokerRepository::class)->all(),
              'autofill'        => $request->has ('autofill') ? app (OperatorRepository::class)->find ($request->input ('autofill')) : false
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewProperty $request, Operator $operator, Brand $brand) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, [Property::class, $brand]);

        try {

          app (PropertyManagementContract::class)
            ->initiator ($request->user())
            ->parent ($brand)
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('explorer.operators.brands.properties.index', [$operator->slug, $brand->slug])
            ->with ('success', 'Property created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Operator $operator, Brand $brand, Property $property) : View
    {
        $this->authorize (__FUNCTION__, $property);

        try {

            $property->load (['mqtt_brokers', 'ws_brokers']);

            return $this->view ($this->view_subdir.'create_edit', [
                'menu_operator'   => $operator,
                'menu_brand'      => $brand,
                'menu_property'   => $property,
                'property'        => $property,
                'mqtt_brokers'    => app (MQTTBrokerRepository::class)->all(),
                'ws_brokers'      => app (WSBrokerRepository::class)->all(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateProperty $request, Operator $operator, Brand $brand, Property $property) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $property);

        try {

          app (PropertyManagementContract::class)
            ->initiator ($request->user())
            ->parent ($brand)
            ->entity ($property)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('explorer.operators.brands.properties.index', [$operator->slug, $brand->slug])
            ->with ('success', 'Property updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Operator $operator, Brand $brand, Property $property) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $property);

        try {

          app (PropertyManagementContract::class)
            ->initiator ($request->user())
            ->parent ($brand)
            ->entity ($property)
            ->destroy ();

          return redirect ()->route ('explorer.operators.brands.properties.index', [$operator->slug, $brand->slug])
            ->with ('success', 'Property deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
