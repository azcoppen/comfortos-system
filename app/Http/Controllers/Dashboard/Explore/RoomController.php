<?php

namespace SmartRooms\Http\Controllers\Dashboard\Explore;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Requests\Dashboard\StoreNewMultipleRoomSetup;
use SmartRooms\Http\Requests\Dashboard\UpdateRoom;
use SmartRooms\Http\Requests\Dashboard\ProvisionHAB;
use SmartRooms\Http\Requests\Dashboard\DeprovisionHAB;

use SmartRooms\Contracts\System\OTPGenerationContract;
use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Operator\Brand;
use SmartRooms\Models\Operator\Operator;
use SmartRooms\Models\Property\Building;
use SmartRooms\Models\Property\Property;
use SmartRooms\Models\Property\Room;

use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Contracts\Repositories\Components\OS\HABRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;

use SmartRooms\Contracts\System\RoomManagementContract;
use SmartRooms\Criteria\ComfortOS\NotProvisionedToAnyRoom;
use SmartRooms\Contracts\System\WSBrokerAccessContract;

final class RoomController extends BaseController
{
    public $repository  = RoomRepository::class;
    public $app_section = 'explorer';
    public $view_subdir = 'explore.operators.brands.properties.buildings.rooms.';

    public function index (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, $floor = 0) : View
    {
        $this->authorize (__FUNCTION__, [Room::class, $building]);

        try {

            return $this->view ($this->view_subdir.__FUNCTION__, [
                'menu_operator'   => $operator,
                'menu_brand'      => $brand,
                'menu_property'   => $property,
                'menu_building'   => $building,
                'menu_floor'      => $floor,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request, Operator $operator, Brand $brand, Property $property, Building $building) : View
    {
        $this->authorize (__FUNCTION__, [Room::class, $building]);

        try {

            return $this->view ($this->view_subdir. __FUNCTION__, [
              'menu_operator'   => $operator,
              'menu_brand'      => $brand,
              'menu_property'   => $property,
              'menu_building'   => $building,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewMultipleRoomSetup $request, Operator $operator, Brand $brand, Property $property, Building $building) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, [Room::class, $building]);

        try {

          app (RoomManagementContract::class)
            ->initiator ($request->user())
            ->parent ($building)
            ->params ($request->all())
            ->batch ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.index', [$operator->slug, $brand->slug, $property->slug, $building->slug])
            ->with ('success', 'Room created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : View
    {
        $this->authorize (__FUNCTION__, $room);

        try {

            $component_counts = [];

            return $this->view ($this->view_subdir.__FUNCTION__, array_merge ($component_counts, [
                'menu_operator'   => $operator,
                'menu_brand'      => $brand,
                'menu_property'   => $property,
                'menu_building'   => $building,
                'menu_room'       => $room,
                'room'            => $room->load (['component_map', 'building.property.brand.operator', 'building.property.mqtt_brokers', 'building.property.ws_brokers']),
                'otp'                     => '30M'.app (OTPGenerationContract::class)->create($room->_id, 6, 1800).'S04',
                'ws_broker'               => $room->building->property->ws_brokers->first() ?? null,
                'centrifugo_jwt_token'    => app (WSBrokerAccessContract::class)->user ($request->user())->token(),
                'centrifugo_room_token'   => app (WSBrokerAccessContract::class)->room ($room)->token(),
            ]));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function feed (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : View
    {
        $this->authorize (__FUNCTION__, $room);

        try {

          if ( $request->has ('q') )
          {
            $q_commands = app (app(HABCommandRepository::class)->model())::search (format_search_query($request->get('q')))
              ->where ('room_id', $room->_id)
              ->orderBy ('created_at', 'DESC')
              ->get ();

            $q_signals = app (app(HABSignalRepository::class)->model())::search (format_search_query($request->get('q')))
              ->where ('room_id', $room->_id)
              ->orderBy ('created_at', 'DESC')
              ->get ();
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
              'menu_operator'   => $operator,
              'menu_brand'      => $brand,
              'menu_property'   => $property,
              'menu_building'   => $building,
              'menu_room'       => $room,
              'room'            => $room->load (['building.property.mqtt_brokers', 'building.property.ws_brokers']),
              'q_commands'      => $q_commands ?? null,
              'q_signals'       => $q_signals ?? null,
              'ws_broker'               => $room->building->property->ws_brokers->first() ?? null,
              'centrifugo_jwt_token'    => app (WSBrokerAccessContract::class)->user ($request->user())->token(),
              'centrifugo_room_token'   => app (WSBrokerAccessContract::class)->room ($room)->token(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function hab_availability (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : View
    {
        $this->authorize (__FUNCTION__, $room);

        try {

          $room->load (['habs']);

          return $this->view ($this->view_subdir.'hab', [
              'menu_operator'   => $operator,
              'menu_brand'      => $brand,
              'menu_property'   => $property,
              'menu_building'   => $building,
              'menu_room'       => $room,
              'room'            => $room,
              'available'       => app (HABRepository::class)
                ->pushCriteria ( new NotProvisionedToAnyRoom )->all(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function hab_provisioning (ProvisionHAB $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $room);

        try {

          app (RoomManagementContract::class)
            ->initiator ($request->user())
            ->parent ($building)
            ->entity ($room)
            ->params ($request->all())
            ->provision_hab ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.show', [$operator->slug, $brand->slug, $property->slug, $building->slug, $room->_id])
            ->with ('success', 'HAB provisioned to room successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function hab_deprovisioning (ProvisionHAB $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $room);

        try {

          app (RoomManagementContract::class)
            ->initiator ($request->user())
            ->parent ($building)
            ->entity ($room)
            ->params ($request->all())
            ->deprovision_hab ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.show', [$operator->slug, $brand->slug, $property->slug, $building->slug, $room->_id])
            ->with ('success', 'HAB removed from room successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function openhab (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : View
    {
        $this->authorize ('destroy', $room);

        try {


          return $this->view ($this->view_subdir.'openhab_config', [
              'menu_operator'   => $operator,
              'menu_brand'      => $brand,
              'menu_property'   => $property,
              'menu_building'   => $building,
              'menu_room'       => $room,
              'room'            => $room->load (['component_map', 'building.property.brand.operator', 'building.property.mqtt_brokers', 'building.property.ws_brokers']),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : View
    {
        $this->authorize (__FUNCTION__, $room);

        try {

            $room->load (['notes', 'users.roles']);

            return $this->view ($this->view_subdir.__FUNCTION__, [
                'menu_operator' => $operator,
                'menu_brand'    => $brand,
                'menu_property' => $property,
                'menu_building' => $building,
                'menu_room'     => $room,
                'room'          => $room,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateRoom $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $room);

        try {

          app (RoomManagementContract::class)
            ->initiator ($request->user())
            ->parent ($building)
            ->entity ($room)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.index', [$operator->slug, $brand->slug, $property->slug, $building->slug])
            ->with ('success', 'Room updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $room);

        try {

          app (RoomManagementContract::class)
            ->initiator ($request->user())
            ->parent ($building)
            ->entity ($room)
            ->destroy ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.index', [$operator->slug, $brand->slug, $property->slug, $building->slug])
            ->with ('success', 'Room deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
