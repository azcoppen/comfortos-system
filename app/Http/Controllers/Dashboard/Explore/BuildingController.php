<?php

namespace SmartRooms\Http\Controllers\Dashboard\Explore;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Contracts\Repositories\Property\BuildingRepository;

use SmartRooms\Http\Requests\Dashboard\StoreNewBuilding;
use SmartRooms\Http\Requests\Dashboard\UpdateBuilding;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Operator\Brand;
use SmartRooms\Models\Operator\Operator;
use SmartRooms\Models\Property\Building;
use SmartRooms\Models\Property\Property;

use SmartRooms\Contracts\System\BuildingManagementContract;

final class BuildingController extends BaseController
{
    public $repository  = BuildingRepository::class;
    public $app_section = 'explorer';
    public $view_subdir = 'explore.operators.brands.properties.buildings.';

    public function index (Request $request, Operator $operator, Brand $brand, Property $property) : View
    {
        $this->authorize (__FUNCTION__, [Building::class, $property]);

        try {

          if (! $request->has ('q') )
          {
            $property->load (['buildings.rooms']);
            $records = $property->buildings;
          }

          if ( $request->has ('q') )
          {
            $records = $this->manager ($request, ['property_id' => $property->_id])
              ->with (['rooms'])
              ->paginate (25);
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'menu_operator'   => $operator,
            'menu_brand'      => $brand,
            'menu_property'   => $property,
            'menu_buildings'  => $records,
          ]);


        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request, Operator $operator, Brand $brand, Property $property) : View
    {
        $this->authorize (__FUNCTION__, [Building::class, $property]);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'menu_operator'   => $operator,
              'menu_brand'      => $brand,
              'menu_property'   => $property,
              'autofill'        => $request->has ('autofill') ? app (PropertyRepository::class)->find ($request->input ('autofill')) : false
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewBuilding $request, Operator $operator, Brand $brand, Property $property) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, [Building::class, $property]);

        try {

          app (BuildingManagementContract::class)
            ->initiator ($request->user())
            ->parent ($property)
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.index', [$operator->slug, $brand->slug, $property->slug])
            ->with ('success', 'Building created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Operator $operator, Brand $brand, Property $property, Building $building) : View
    {
        $this->authorize (__FUNCTION__, $building);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
                'menu_operator' => $operator,
                'menu_brand'    => $brand,
                'menu_property' => $property,
                'menu_building' => $building,
                'building'      => $building,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function stickers (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, int $floor = 0) : View
    {
        $this->authorize (__FUNCTION__, $building);

        try {

            return $this->view ($this->view_subdir.'stickers', [
                'property'  => $property,
                'building'  => $building->load(['rooms']),
                'floor'     => $floor,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateBuilding $request, Operator $operator, Brand $brand, Property $property, Building $building) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $building);

        try {

          app (BuildingManagementContract::class)
            ->initiator ($request->user())
            ->parent ($property)
            ->entity ($building)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.index', [$operator->slug, $brand->slug, $property->slug])
            ->with ('success', 'Building updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Operator $operator, Brand $brand, Property $property, Building $building) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $building);

        try {

          app (BuildingManagementContract::class)
            ->initiator ($request->user())
            ->parent ($property)
            ->entity ($building)
            ->destroy ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.index', [$operator->slug, $brand->slug, $property->slug])
            ->with ('success', 'Building deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
