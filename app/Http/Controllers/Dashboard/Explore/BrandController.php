<?php

namespace SmartRooms\Http\Controllers\Dashboard\Explore;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Requests\Dashboard\StoreNewBrand;
use SmartRooms\Http\Requests\Dashboard\UpdateBrand;

use SmartRooms\Contracts\Repositories\Operator\BrandRepository;
use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Operator\Brand;
use SmartRooms\Models\Operator\Operator;

use SmartRooms\Contracts\System\BrandManagementContract;
use SmartRooms\Criteria\OrderByCriteria;

final class BrandController extends BaseController
{
    public $repository = BrandRepository::class;
    public $app_section = 'explorer';
    public $view_subdir = 'explore.operators.brands.';

    public function index (Request $request, Operator $operator) : View
    {
        $this->authorize (__FUNCTION__, [Brand::class, $operator]);

        try {

          if (! $request->has ('q') )
          {
            $operator->load (['brands.properties']);
            $records = $operator->brands;
          }

          if ( $request->has ('q') )
          {
            $records = $this->manager ($request, ['operator_id' => $operator->_id])
              ->paginate (25);
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'menu_operator' => $operator,
            'menu_brands'   => $records,
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request, Operator $operator) : View
    {
        $this->authorize (__FUNCTION__, [Brand::class, $operator]);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'menu_operator'   => $operator,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewBrand $request, Operator $operator) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, [Brand::class, $operator]);

        try {

          app (BrandManagementContract::class)
            ->initiator ($request->user())
            ->parent ($operator)
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('explorer.operators.brands.index', [$operator->slug])
            ->with ('success', 'Brand created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Operator $operator, Brand $brand) : View
    {
        $this->authorize (__FUNCTION__, $brand);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
                'menu_operator' => $operator,
                'menu_brand'    => $brand,
                'brand'         => $brand,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateBrand $request, Operator $operator, Brand $brand) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $brand);

        try {

          app (BrandManagementContract::class)
            ->initiator ($request->user())
            ->parent ($operator)
            ->entity ($brand)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('explorer.operators.brands.index', [$operator->slug])
            ->with ('success', 'Brand updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Operator $operator, Brand $brand) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $brand);

        try {

          app (BrandManagementContract::class)
            ->initiator ($request->user())
            ->parent ($operator)
            ->entity ($brand)
            ->destroy ();

          return redirect ()->route ('explorer.operators.brands.index', [$operator->slug])
            ->with ('success', 'Brand deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
