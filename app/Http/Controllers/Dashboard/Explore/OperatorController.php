<?php

namespace SmartRooms\Http\Controllers\Dashboard\Explore;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Requests\Dashboard\StoreNewOperator;
use SmartRooms\Http\Requests\Dashboard\UpdateOperator;

use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;
use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Operator\Operator;

use SmartRooms\Contracts\System\OperatorManagementContract;

use SmartRooms\Criteria\ComfortOS\OperatorsAvailableToUser;
use SmartRooms\Criteria\OrderByCriteria;

final class OperatorController extends BaseController
{
    public $repository = OperatorRepository::class;
    public $app_section = 'explorer';
    public $view_subdir = 'explore.operators.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Operator::class);

        try {

          $records = $this->manager ($request)
            ->with (['brands',]);

          if (! $request->has ('q') )
          {
            $records
              ->pushCriteria (new OrderByCriteria('label', 'ASC'))
              ->pushCriteria (new OperatorsAvailableToUser ($request->user()) );
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'menu_operators' => $records->paginate (25)
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Operator::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'menu_operators' => $this->operators_for_user ($request)->all()
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewOperator $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, Operator::class);

        try {

          app (OperatorManagementContract::class)
            ->initiator ($request->user())
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('explorer.index')
            ->with ('success', 'Operator created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Operator $operator) : View
    {
        $this->authorize (__FUNCTION__, $operator);

        try {

            $operator->load (['brands']);

            return $this->view ($this->view_subdir.'create_edit', [
                'menu_operator' => $operator,
                'menu_brands'   => $operator->brands,
                'operator'      => $operator,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateOperator $request, Operator $operator) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $operator);

        try {

          app (OperatorManagementContract::class)
            ->initiator ($request->user())
            ->entity ($operator)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('explorer.index')
            ->with ('success', 'Operator updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Operator $operator) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $operator);

        try {

          app (OperatorManagementContract::class)
            ->initiator ($request->user())
            ->entity ($operator)
            ->destroy ();

          return redirect ()->route ('explorer.index')
            ->with ('success', 'Operator deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
