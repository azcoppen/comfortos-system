<?php

namespace SmartRooms\Http\Controllers\Dashboard\Explore;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\View\View;

use SmartRooms\Contracts\System\OTPGenerationContract;

use SmartRooms\Http\Requests\Dashboard\DeprovisionComponent;
use SmartRooms\Http\Requests\Dashboard\ProvisionComponent;
use SmartRooms\Http\Requests\Dashboard\StoreNewComponent;
use SmartRooms\Http\Requests\Dashboard\UpdateComponent;

use SmartRooms\Http\Controllers\Dashboard\BaseController;

use SmartRooms\Models\Operator\Brand;
use SmartRooms\Models\Operator\Operator;
use SmartRooms\Models\Property\Building;
use SmartRooms\Models\Property\Property;
use SmartRooms\Models\Property\Room;

use SmartRooms\Models\Components\Telecoms\Extension;
use SmartRooms\Models\Components\Devices\Bulb;
use SmartRooms\Models\Components\Devices\LED;
use SmartRooms\Models\Components\Devices\Lock;
use SmartRooms\Models\Components\Wifi\Network;

use SmartRooms\Contracts\Repositories\Components\Telecoms\DECTRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\ExtensionRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\HandsetRepository;
use SmartRooms\Contracts\Repositories\Components\Mirror\DisplayRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\STBRepository;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\ComfortOS\NotProvisionedToAnyRoom;
use SmartRooms\Contracts\System\WSBrokerAccessContract;

use SmartRooms\Contracts\System\ComponentManagementContract;
use SmartRooms\Contracts\System\VirtualWifiNetworkManagementContract;

final class ComponentController extends BaseController
{
    public $app_section = 'explorer';
    public $view_subdir = 'explore.operators.brands.properties.buildings.rooms.';

    public $repositories = [
      'dects'       => DECTRepository::class,
      'extensions'  => ExtensionRepository::class,
      'handsets'    => HandsetRepository::class,
      'mirrors'     => DisplayRepository::class,
      'routers'     => RouterRepository::class,
      'stbs'        => STBRepository::class,
    ];

    private function unprovisioned (string $group) : Collection
    {
      if ( in_array ($group, array_keys ($this->repositories)) )
      {
        return app ($this->repositories[$group])
          ->pushCriteria ( new NotProvisionedToAnyRoom )->all();
      }

      return collect ([]);
    }

    public function create (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : View
    {
        $this->authorize ('component', $room);

        try {

            $group = $request->segments()[count ($request->segments())-2];

            if ( in_array ($group, array_keys ($this->repositories)) )
            {
              $room->load ([$group.($group == 'extensions' ? '.pbx' : '')]);
            }

            return $this->view ($this->view_subdir.$group.($group == 'wifi-networks' ? '.create' : '.create_edit'), [
              'menu_operator'   => $operator,
              'menu_brand'      => $brand,
              'menu_property'   => $property,
              'menu_building'   => $building,
              'menu_room'       => $room,
              'group'           => $group,
              'commands'        => config ('comfortos.commands.'.$group),
              'options'         => config ('comfortos.options.'.$group),
              'users'           => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
              'available'       => $this->unprovisioned ($group),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewComponent $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : RedirectResponse
    {
        $this->authorize ('component', $room);

        try {

          app ($request->segments()[count ($request->segments())-1] == 'wifi-networks' ? VirtualWifiNetworkManagementContract::class : ComponentManagementContract::class)
            ->initiator ($request->user())
            ->parent ($room)
            ->params ($request->all())
            ->store ($request->segments()[count ($request->segments())-1]);

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.show', [$operator->slug, $brand->slug, $property->slug, $building->slug, $room->_id])
            ->with ('success', 'Component created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function provision (ProvisionComponent $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $room);

        try {

          app (ComponentManagementContract::class)
            ->initiator ($request->user())
            ->parent ($room)
            ->params ($request->all())
            ->provision ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.show', [$operator->slug, $brand->slug, $property->slug, $building->slug, $room->_id])
            ->with ('success', 'Component provisioned successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function deprovision (DeprovisionComponent $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $room);

        try
        {

          app (ComponentManagementContract::class)
            ->initiator ($request->user())
            ->parent ($room)
            ->params ($request->all())
            ->deprovision ();

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.show', [$operator->slug, $brand->slug, $property->slug, $building->slug, $room->_id])
            ->with ('success', 'Component deprovisioned successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room, $model) : View
    {
        $this->authorize (__FUNCTION__, $model);

        try {

            if (method_exists($model, 'last_signal')) {
                $model->load([
                  'room.building.property.mqtt_brokers',
                  'room.building.property.ws_brokers',
                  'room.habs',
                  'last_command',
                  'last_signal',
                  'purchaser',
                  'installer',
                  'provisioner',
                ]);
            }

            if ( is_object ($model) )
            {
              switch ( get_class ($model) )
              {
                case Bulb::class;
                  $model->load (['last_hsb_signal']);
                break;

                case Extension::class:
                  $model->load (['pbx', 'dects', 'handsets']);
                break;

                case LED::class;
                  $model->load (['last_hsb_signal']);
                break;

                case Lock::class:
                  $model->load(['codes' => function ($q) { return $q->orderBy('created_at', 'DESC')->take (50); }]);
                break;

                case Network::class;
                  $model->load (['router']);
                break;

              }
            }

            $group = $path = $this->_get_model_plural ($model, true) ?? abort (404);

            // Unique condition
            switch ( $group )
            {
              case 'displays':
                $group = $path = 'mirrors';
              break;

              case 'networks':
                $group = $path = 'wifi-networks';
              break;

              case 'physicalswitches':
                $group = $path = 'switches';
              break;
            }

            return $this->view($this->view_subdir.$path.'.'.__FUNCTION__, [
                'menu_operator'   => $operator,
                'menu_brand'      => $brand,
                'menu_property'   => $property,
                'menu_building'   => $building,
                'menu_room'       => $room,
                'room'            => $room,
                'component'       => $model,
                'group'           => $group,
                'otp'                     => '30M' . app(OTPGenerationContract::class)->create($model->_id, 6, 1800) . 'S04',
                'ws_broker'               => $room->building->property->ws_brokers->first() ?? null,
                'centrifugo_jwt_token'    => app (WSBrokerAccessContract::class)->user ($request->user())->token(),
                'centrifugo_room_token'   => app (WSBrokerAccessContract::class)->room ($room)->token(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function commands (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room, $model) : View
    {
        $this->authorize (__FUNCTION__, $model);

        try {

            if ( $request->has ('q') )
            {
              $q_commands = app (app(HABCommandRepository::class)->model())::search (format_search_query($request->get('q')))
                ->where ('component_id', $model->_id)
                ->orderBy ('created_at', 'DESC')
                ->get ();
            }

            $group = $path = $this->_get_model_plural ($model, true) ?? abort (404);

            // Unique condition
            switch ( $group )
            {
              case 'displays':
                $group = $path = 'mirrors';
              break;

              case 'networks':
                $group = $path = 'wifi-networks';
              break;

              case 'physicalswitches':
                $group = $path = 'switches';
              break;
            }


            return $this->view ($this->view_subdir.$path.'.'.__FUNCTION__, [
                'menu_operator'   => $operator,
                'menu_brand'      => $brand,
                'menu_property'   => $property,
                'menu_building'   => $building,
                'menu_room'       => $room,
                'room'            => $room,
                'component'       => $model->load (['room.building.property.mqtt_brokers','room.building.property.ws_brokers',]),
                'group'           => $group,
                'q_commands'      => $q_commands ?? null,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function signals (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room, $model) : View
    {
        $this->authorize (__FUNCTION__, $model);

        try {

          if ( $request->has ('q') )
          {
            $q_signals = app (app(HABSignalRepository::class)->model())::search (format_search_query($request->get('q')))
              ->where ('component_id', $model->_id)
              ->orderBy ('created_at', 'DESC')
              ->get ();
          }

            $group = $path = $this->_get_model_plural ($model, true) ?? abort (404);

            // Unique condition
            switch ( $group )
            {
              case 'displays':
                $group = $path = 'mirrors';
              break;

              case 'networks':
                $group = $path = 'wifi-networks';
              break;

              case 'physicalswitches':
                $group = $path = 'switches';
              break;
            }


            return $this->view ($this->view_subdir.$path.'.'.__FUNCTION__, [
                'menu_operator'   => $operator,
                'menu_brand'      => $brand,
                'menu_property'   => $property,
                'menu_building'   => $building,
                'menu_room'       => $room,
                'room'            => $room,
                'component'       => $model->load (['room.building.property.mqtt_brokers','room.building.property.ws_brokers',]),
                'group'           => $group,
                'q_signals'       => $q_signals ?? null,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room, $model) : View
    {
        $this->authorize (__FUNCTION__, $model);

        try {

          $group = $this->_get_model_plural ($model, true);

          // Unique condition
          switch ( $group )
          {
            case 'displays':
              $group = $path = 'mirrors';
            break;

            case 'networks':
              $group = $path = 'wifi-networks';
            break;

            case 'physicalswitches':
              $group = $path = 'switches';
            break;
          }

          if ( in_array ($group, array_keys ($this->repositories)) )
          {
            $room->load ([$group.($group == 'extensions' ? '.pbx' : '')]);
          }

          return $this->view ($this->view_subdir. $group.($group == 'wifi-networks' ? '.edit' : '.create_edit'), [
              'menu_operator' => $operator,
              'menu_brand'    => $brand,
              'menu_property' => $property,
              'menu_building' => $building,
              'menu_room'     => $room,
              'room'          => $room,
              'component'     => $model,
              'group'         => $group,
              'commands'      => config ('comfortos.commands.'.$group),
              'options'       => config ('comfortos.options.'.$group),
              'users'         => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }


    public function update (UpdateComponent $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room, $model) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $model);

        try {

          app ($this->_get_model_plural ($model, true) == 'networks' ? VirtualWifiNetworkManagementContract::class : ComponentManagementContract::class)
            ->initiator ($request->user())
            ->parent ($room)
            ->entity ($model)
            ->params ($request->all())
            ->update ($this->_get_model_plural ($model, true));

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.show', [$operator->slug, $brand->slug, $property->slug, $building->slug, $room->_id])
            ->with ('success', 'Component updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Operator $operator, Brand $brand, Property $property, Building $building, Room $room, $model) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $model);

        try {

          app ($this->_get_model_plural ($model, true) == 'networks' ? VirtualWifiNetworkManagementContract::class : ComponentManagementContract::class)
            ->initiator ($request->user())
            ->parent ($room)
            ->entity ($model)
            ->destroy ($this->_get_model_plural ($model, true));

          return redirect ()->route ('explorer.operators.brands.properties.buildings.rooms.show', [$operator->slug, $brand->slug, $property->slug, $building->slug, $room->_id])
            ->with ('success', 'Component deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
