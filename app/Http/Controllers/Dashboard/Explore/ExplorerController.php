<?php

namespace SmartRooms\Http\Controllers\Dashboard\Explore;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;

use SmartRooms\Models\Operator\Operator;
use SmartRooms\Criteria\ComfortOS\OperatorsAvailableToUser;

final class ExplorerController extends BaseController
{
    public $repository = OperatorRepository::class;
    public $app_section = 'explorer';
    public $view_subdir = 'explore.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Operator::class);

        try {

            return $this->view ($this->view_subdir.__FUNCTION__, [
                'menu_operators' => app ($this->repository)->pushCriteria (new OperatorsAvailableToUser ($request->user()) )->paginate (25),
                'map'            => app ($this->repository)->pushCriteria (new OperatorsAvailableToUser ($request->user()) )->with (['brands.properties.buildings.rooms'])->paginate (5),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
