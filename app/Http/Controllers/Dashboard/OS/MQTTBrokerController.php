<?php

namespace SmartRooms\Http\Controllers\Dashboard\OS;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\OS\MQTTBroker;

use SmartRooms\Http\Requests\Dashboard\StoreNewMQTTBroker;
use SmartRooms\Http\Requests\Dashboard\UpdateMQTTBroker;

use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\Components\OS\MQTTBrokerRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\MQTTBrokerManagementContract;

final class MQTTBrokerController extends BaseController
{
    public $repository = MQTTBrokerRepository::class;
    public $app_section = 'os';
    public $nav_section = 'mqtt-brokers';
    public $view_subdir = 'os.mqtt.brokers.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, MQTTBroker::class);

        try {

          $records = $this->manager ($request);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate (25)
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, MQTTBroker::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'vpn_clients' => app (VPNClientRepository::class)->all(),
              'vpn_servers' => app (VPNServerRepository::class)->all(),
              'users'       => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewMQTTBroker $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, MQTTBroker::class);

        try {

          app (MQTTBrokerManagementContract::class)
          ->initiator ($request->user())
          ->params ($request->all())
          ->store ();

          return redirect ()->route ('os.mqtt-brokers.index')
            ->with ('success', 'MQTT Broker created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, MQTTBroker $mqtt_broker) : View
    {
        $this->authorize (__FUNCTION__, $mqtt_broker);

        try {

          $mqtt_broker->load (['properties.brand.operator', 'vpn_servers', 'vpn_clients', 'purchaser', 'installer', 'provisioner']);

          return $this->view ($this->view_subdir.__FUNCTION__, compact('mqtt_broker'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function mosquitto_template (Request $request, MQTTBroker $mqtt_broker) : Response
    {
        $this->authorize ('destroy', $mqtt_broker);

        try {

          $mqtt_broker->load (['properties']);

          return response()
            ->view ('conf.mosquitto-conf', [
              'property' => $mqtt_broker->properties->first() ?? NULL,
            ])
            ->header ("Content-type","application/octet-stream; charset=utf-8")
            ->header ("Content-disposition","attachment; filename=".$mqtt_broker->properties->first()->_id."-mosquitto.conf");

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function env_template (Request $request, MQTTBroker $mqtt_broker) : Response
    {
        $this->authorize ('destroy', $mqtt_broker);

        try {

          $mqtt_broker->load (['properties']);

          return response()
            ->view ('conf.smartrooms-cli-env', [
              'property' => $mqtt_broker->properties->first() ?? NULL,
            ])
            ->header ("Content-type","application/octet-stream; charset=utf-8")
            ->header ("Content-disposition","attachment; filename=".$mqtt_broker->properties->first()->_id."-smartrooms-cli.env");

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function systemd_template (Request $request, MQTTBroker $mqtt_broker) : Response
    {
        $this->authorize ('destroy', $mqtt_broker);

        try {

          $mqtt_broker->load (['properties']);

          return response()
            ->view ('conf.smartrooms-mqtt-listener-service', [
              'property' => $mqtt_broker->properties->first() ?? NULL,
            ])
            ->header ("Content-type","application/octet-stream; charset=utf-8")
            ->header ("Content-disposition","attachment; filename=".$mqtt_broker->properties->first()->_id."-mosquitto-mqtt-listener.service");

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, MQTTBroker $mqtt_broker) : View
    {
        $this->authorize (__FUNCTION__, $mqtt_broker);

        try {

          $mqtt_broker->load (['vpn_servers', 'vpn_clients', ]);

          return $this->view ($this->view_subdir.'create_edit', [
            'mqtt_broker'   => $mqtt_broker,
            'vpn_clients'   => app (VPNClientRepository::class)->all(),
            'vpn_servers'   => app (VPNServerRepository::class)->all(),
            'users'         => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateMQTTBroker $request, MQTTBroker $mqtt_broker) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $mqtt_broker);

        try {

          app (MQTTBrokerManagementContract::class)
            ->initiator ($request->user())
            ->entity ($mqtt_broker)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('os.mqtt-brokers.index')
            ->with ('success', 'MQTT broker updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, MQTTBroker $mqtt_broker) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $mqtt_broker);

        try {

          app (MQTTBrokerManagementContract::class)
            ->initiator ($request->user())
            ->entity ($mqtt_broker)
            ->destroy ();

          return redirect ()->route ('os.mqtt-brokers.index')
            ->with ('success', 'MQTT broker deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
