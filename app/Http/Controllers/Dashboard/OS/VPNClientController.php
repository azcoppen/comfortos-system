<?php

namespace SmartRooms\Http\Controllers\Dashboard\OS;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\OS\VPNClient;

use SmartRooms\Http\Requests\Dashboard\StoreNewVPNClient;
use SmartRooms\Http\Requests\Dashboard\UpdateVPNClient;

use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\VPNClientManagementContract;

final class VPNClientController extends BaseController
{
    public $repository = VPNClientRepository::class;
    public $app_section = 'os';
    public $nav_section = 'vpn-clients';
    public $view_subdir = 'os.vpns.clients.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, VPNClient::class);

        try {

          $records = $this->manager ($request)->with (['vpn_server']);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate (25)
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, VPNClient::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'vpn_servers' => app (VPNServerRepository::class)->all(),
              'users'       => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewVPNClient $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, VPNClient::class);

        try {

          app (VPNClientManagementContract::class)
          ->initiator ($request->user())
          ->params ($request->all())
          ->store ();

          return redirect ()->route ('os.vpn-clients.index')
            ->with ('success', 'VPN client created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, VPNClient $vpn_client) : View
    {
        $this->authorize (__FUNCTION__, $vpn_client);

        try {

          $vpn_client->load ([
            'vpn_server', 'purchaser', 'installer', 'provisioner',
            'routers.room', 'habs.room', 'pbxs', 'dects.room', 'mirrors.room', 'stbs.room',
          ]);

          return $this->view ($this->view_subdir.__FUNCTION__, compact('vpn_client'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, VPNClient $vpn_client) : View
    {
        $this->authorize (__FUNCTION__, $vpn_client);

        try {

          return $this->view($this->view_subdir.'create_edit', [
            'vpn_client'  => $vpn_client,
            'vpn_servers' => app (VPNServerRepository::class)->all(),
            'users'       => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateVPNClient $request, VPNClient $vpn_client) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $vpn_client);

        try {

          app (VPNClientManagementContract::class)
            ->initiator ($request->user())
            ->entity ($vpn_client)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('os.vpn-clients.index')
            ->with ('success', 'VPN client updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function revoke (Request $request, VPNClient $vpn_client) : RedirectResponse
    {
        $this->authorize ('update', $vpn_client);

        try {

          app (VPNClientManagementContract::class)
            ->initiator ($request->user())
            ->entity ($vpn_client)
            ->params (['revoked_at' => now()])
            ->update ();

          return redirect ()->route ('os.vpn-clients.index')
            ->with ('success', 'VPN client revoked successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function unrevoke (Request $request, VPNClient $vpn_client) : RedirectResponse
    {
        $this->authorize ('update', $vpn_client);

        try {

          app (VPNClientManagementContract::class)
            ->initiator ($request->user())
            ->entity ($vpn_client)
            ->params (['revoked_at' => null])
            ->update ();

          return redirect ()->route ('os.vpn-clients.index')
            ->with ('success', 'VPN reactivated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, VPNClient $vpn_client) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $vpn_client);

        try {

          app (VPNClientManagementContract::class)
            ->initiator ($request->user())
            ->entity ($vpn_client)
            ->destroy ();

          return redirect ()->route ('os.vpn-clients.index')
            ->with ('success', 'VPN client deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
