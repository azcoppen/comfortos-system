<?php

namespace SmartRooms\Http\Controllers\Dashboard\OS;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\OS\HAB;

use SmartRooms\Contracts\Repositories\Components\OS\HABRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;

use SmartRooms\Http\Requests\Dashboard\StoreNewHAB;
use SmartRooms\Http\Requests\Dashboard\UpdateHAB;

use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;
use SmartRooms\Criteria\SearchCriteria;

use SmartRooms\Contracts\System\HABManagementContract;

final class HABController extends BaseController
{
    public $repository = HABRepository::class;
    public $app_section = 'os';
    public $nav_section = 'habs';
    public $view_subdir = 'os.habs.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, HAB::class);

        try {

          $records = $this->manager ($request)
            ->with (['room.building.property',]);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate (25)
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, HAB::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'routers'     => app (RouterRepository::class)->all(),
              'vpn_clients' => app (VPNClientRepository::class)->all(),
              'vpn_servers' => app (VPNServerRepository::class)->all(),
              'users'       => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewHAB $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, HAB::class);

        try {

          app (HABManagementContract::class)
            ->initiator ($request->user())
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('os.habs.index')
            ->with ('success', 'HAB created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, HAB $hab) : View
    {
        $this->authorize (__FUNCTION__, $hab);

        try {

            $hab->load (['room.building.property', 'room.signals', 'room.commands', 'vpn_clients', 'vpn_servers', 'purchaser', 'installer', 'provisioner']);

            return $this->view ($this->view_subdir.__FUNCTION__, compact('hab'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, HAB $hab) : View
    {
        $this->authorize (__FUNCTION__, $hab);

        try {

            $hab->load (['room.building.property', 'router', 'vpn_clients', 'vpn_servers', ]);

            return $this->view ($this->view_subdir.'create_edit', [
              'hab'         => $hab,
              'routers'     => app (RouterRepository::class)->all(),
              'vpn_clients' => app (VPNClientRepository::class)->all(),
              'vpn_servers' => app (VPNServerRepository::class)->all(),
              'users'       => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateHAB $request, HAB $hab) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $hab);

        try {

          app (HABManagementContract::class)
            ->initiator ($request->user())
            ->entity ($hab)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('os.habs.index')
            ->with ('success', 'HAB updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, HAB $hab) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $hab);

        try {

          app (HABManagementContract::class)
            ->initiator ($request->user())
            ->entity ($hab)
            ->destroy ();

          return redirect ()->route ('os.habs.index')
            ->with ('success', 'HAB deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
