<?php

namespace SmartRooms\Http\Controllers\Dashboard\OS;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\OS\WSBroker;

use SmartRooms\Http\Requests\Dashboard\StoreNewWSBroker;
use SmartRooms\Http\Requests\Dashboard\UpdateWSBroker;

use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\Components\OS\WSBrokerRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\WSBrokerManagementContract;

final class WSBrokerController extends BaseController
{
    public $repository = WSBrokerRepository::class;
    public $app_section = 'os';
    public $nav_section = 'ws-brokers';
    public $view_subdir = 'os.ws.brokers.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, WSBroker::class);

        try {

          $records = $this->manager ($request);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate (25)
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, WSBroker::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'vpn_clients' => app (VPNClientRepository::class)->all(),
              'vpn_servers' => app (VPNServerRepository::class)->all(),
              'users'       => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewWSBroker $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, WSBroker::class);

        try {

          app (WSBrokerManagementContract::class)
          ->initiator ($request->user())
          ->params ($request->all())
          ->store ();

          return redirect ()->route ('os.ws-brokers.index')
            ->with ('success', 'Websocket Broker created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, WSBroker $ws_broker) : View
    {
        $this->authorize (__FUNCTION__, $ws_broker);

        try {

          $ws_broker->load (['properties.brand.operator', 'vpn_servers', 'vpn_clients', 'purchaser', 'installer', 'provisioner']);

          return $this->view ($this->view_subdir.__FUNCTION__, compact('ws_broker'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function config_template (Request $request, WSBroker $ws_broker) : Response
    {
        $this->authorize ('destroy', $ws_broker);

        try {

          $ws_broker->load (['properties']);

          return response()
            ->view ('conf.centrifugo-json', compact('ws_broker'))
            ->header ("Content-type","application/octet-stream; charset=utf-8")
            ->header ("Content-disposition","attachment; filename=".$ws_broker->properties->first()->_id."-centrifugo.json");

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function nginx_template (Request $request, WSBroker $ws_broker) : Response
    {
        $this->authorize ('destroy', $ws_broker);

        try {

          $ws_broker->load (['properties']);

          return response()
            ->view ('conf.broker-nginx', [
              'property' => $ws_broker->properties->first() ?? NULL,
            ])
            ->header ("Content-type","application/octet-stream; charset=utf-8")
            ->header ("Content-disposition","attachment; filename=".$ws_broker->properties->first()->_id."-nginx.conf");

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, WSBroker $ws_broker) : View
    {
        $this->authorize (__FUNCTION__, $ws_broker);

        try {

          $ws_broker->load (['vpn_servers', 'vpn_clients', ]);

          return $this->view ($this->view_subdir.'create_edit', [
            'vpn_clients'  => app (VPNClientRepository::class)->all(),
            'vpn_servers'  => app (VPNServerRepository::class)->all(),
            'ws_broker'    => $ws_broker,
            'users'        => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateWSBroker $request, WSBroker $ws_broker) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $ws_broker);

        try {

          app (WSBrokerManagementContract::class)
            ->initiator ($request->user())
            ->entity ($ws_broker)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('os.ws-brokers.index')
            ->with ('success', 'Websocket broker updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, MQTTBroker $ws_broker) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $ws_broker);

        try {

          app (WSBrokerManagementContract::class)
            ->initiator ($request->user())
            ->entity ($ws_broker)
            ->destroy ();

          return redirect ()->route ('os.ws-brokers.index')
            ->with ('success', 'Websocket broker deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
