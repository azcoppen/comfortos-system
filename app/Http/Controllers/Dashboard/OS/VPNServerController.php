<?php

namespace SmartRooms\Http\Controllers\Dashboard\OS;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\OS\VPNServer;

use SmartRooms\Http\Requests\Dashboard\StoreNewVPNServer;
use SmartRooms\Http\Requests\Dashboard\UpdateVPNServer;

use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\VPNServerManagementContract;

final class VPNServerController extends BaseController
{
    public $repository = VPNServerRepository::class;
    public $app_section = 'os';
    public $nav_section = 'vpn-servers';
    public $view_subdir = 'os.vpns.servers.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, VPNServer::class);

        try {

          $records = $this->manager ($request)->with (['vpn_clients']);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate (25)
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, VPNServer::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'users'   => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewVPNServer $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, VPNServer::class);

        try {

          app (VPNServerManagementContract::class)
          ->initiator ($request->user())
          ->params ($request->all())
          ->store ();

          return redirect ()->route ('os.vpn-servers.index')
            ->with ('success', 'VPN server created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, VPNServer $vpn_server) : View
    {
        $this->authorize (__FUNCTION__, $vpn_server);

        try {

          $vpn_server->load ([
            'routers.room', 'habs.room', 'pbxs', 'dects.room', 'mirrors.room', 'stbs.room',
            'purchaser', 'installer', 'provisioner', 'vpn_clients',
          ]);

          return $this->view ($this->view_subdir.__FUNCTION__, compact('vpn_server'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, VPNServer $vpn_server) : View
    {
        $this->authorize (__FUNCTION__, $vpn_server);

        try {

          $vpn_server->load ([
            'purchaser', 'installer', 'provisioner',
          ]);

          return $this->view ($this->view_subdir.'create_edit', [
            'vpn_server'  => $vpn_server,
            'users'       => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateVPNServer $request, VPNServer $vpn_server) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $vpn_server);

        try {

          app (VPNServerManagementContract::class)
            ->initiator ($request->user())
            ->entity ($vpn_server)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('os.vpn-servers.index')
            ->with ('success', 'VPN server updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, VPNServer $vpn_server) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $vpn_server);

        try {

          app (VPNServerManagementContract::class)
            ->initiator ($request->user())
            ->entity ($vpn_server)
            ->destroy ();

          return redirect ()->route ('os.vpn-servers.index')
            ->with ('success', 'VPN server deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
