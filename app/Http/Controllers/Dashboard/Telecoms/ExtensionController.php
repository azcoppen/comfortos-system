<?php

namespace SmartRooms\Http\Controllers\Dashboard\Telecoms;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Telecoms\Extension;

use SmartRooms\Http\Requests\Dashboard\StoreNewExtension;
use SmartRooms\Http\Requests\Dashboard\UpdateExtension;

use SmartRooms\Contracts\Repositories\Components\Telecoms\ExtensionRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\DECTRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\PBXRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\ExtensionManagementContract;

final class ExtensionController extends BaseController
{
    public $repository = ExtensionRepository::class;
    public $app_section = 'telecoms';
    public $nav_section = 'extensions';
    public $view_subdir = 'telecoms.extensions.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Extension::class);

        try {

          $records = $this->manager ($request)
            ->with ([
                'room.building.property',
            ]);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate(25),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Extension::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'pbxs'    => app (PBXRepository::class)->all(),
              'users'   => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewExtension $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, Extension::class);

        try {

          app (ExtensionManagementContract::class)
            ->initiator ($request->user())
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('telecoms.extensions.index')
            ->with ('success', 'Extension created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, Extension $extension) : View
    {
        $this->authorize (__FUNCTION__, $extension);

        try {

            $extension->load ([
                'room.building.property',
                'pbx',
                'dects.handsets',
                'dects.room.building.property',
                'handsets.room.building.property',
                'purchaser', 'installer', 'provisioner',
            ]);

            return $this->view ($this->view_subdir.__FUNCTION__, compact('extension'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Extension $extension) : View
    {
        $this->authorize (__FUNCTION__, $extension);

        try {

          return $this->view ($this->view_subdir.'create_edit', [
            'extension' => $extension,
            'pbxs'      => app (PBXRepository::class)->all(),
            'users'     => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateExtension $request, Extension $extension) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $extension);

        try {

          app (ExtensionManagementContract::class)
            ->initiator ($request->user())
            ->entity ($extension)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('telecoms.extensions.index')
            ->with ('success', 'Extension updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Extension $extension) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $extension);

        try {

          app (ExtensionManagementContract::class)
            ->initiator ($request->user())
            ->entity ($extension)
            ->destroy ();

          return redirect ()->route ('telecoms.extensions.index')
            ->with ('success', 'Extension deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
