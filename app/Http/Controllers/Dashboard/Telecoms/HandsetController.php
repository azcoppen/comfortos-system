<?php

namespace SmartRooms\Http\Controllers\Dashboard\Telecoms;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Telecoms\Handset;

use SmartRooms\Http\Requests\Dashboard\StoreNewHandset;
use SmartRooms\Http\Requests\Dashboard\UpdateHandset;

use SmartRooms\Contracts\Repositories\Components\Telecoms\DECTRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\HandsetRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\HandsetManagementContract;

final class HandsetController extends BaseController
{
    public $repository = HandsetRepository::class;
    public $app_section = 'telecoms';
    public $nav_section = 'handsets';
    public $view_subdir = 'telecoms.handsets.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Handset::class);

        try {

          $records = $this->manager ($request)
            ->with ([
                'dects.room.building.property',
            ]);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate(25),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Handset::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'dects'   => app (DECTRepository::class)->all(),
              'users'   => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewHandset $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, Handset::class);

        try {

          app (HandsetManagementContract::class)
            ->initiator ($request->user())
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('telecoms.handsets.index')
            ->with ('success', 'Handset created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, Handset $handset) : View
    {
        $this->authorize (__FUNCTION__, $handset);

        try {

            $handset->load ([
                'extensions.room.building.property',
                'dects.room.building.property',
                'dects.handsets',
                'dects.pbx',
                'purchaser', 'installer', 'provisioner',
            ]);

            return $this->view($this->view_subdir.__FUNCTION__, compact('handset'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Handset $handset) : View
    {
        $this->authorize (__FUNCTION__, $handset);

        try {

          return $this->view ($this->view_subdir.'create_edit', [
            'handset' => $handset,
            'dects'   => app (DECTRepository::class)->all(),
            'users'   => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateHandset $request, Handset $handset) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $handset);

        try {

          app (HandsetManagementContract::class)
            ->initiator ($request->user())
            ->entity ($handset)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('telecoms.handsets.index')
            ->with ('success', 'Handset updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Handset $handset) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $handset);

        try {

          app (HandsetManagementContract::class)
            ->initiator ($request->user())
            ->entity ($handset)
            ->destroy ();

          return redirect ()->route ('telecoms.handsets.index')
            ->with ('success', 'Handset deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
