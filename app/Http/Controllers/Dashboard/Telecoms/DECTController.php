<?php

namespace SmartRooms\Http\Controllers\Dashboard\Telecoms;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Telecoms\DECT;

use SmartRooms\Http\Requests\Dashboard\StoreNewDECT;
use SmartRooms\Http\Requests\Dashboard\UpdateDECT;

use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\DECTRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\PBXRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\DECTManagementContract;

final class DECTController extends BaseController
{
    public $repository = DECTRepository::class;
    public $app_section = 'telecoms';
    public $nav_section = 'dect';
    public $view_subdir = 'telecoms.dect.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, DECT::class);

        try {

          $records = $this->manager ($request)
            ->with ([
                'room.building.property',
                'extensions.room.building.property',
                'handsets.room.building.property',
            ]);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate(25),
          ]);


        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, DECT::class);

        try {

            return $this->view($this->view_subdir.'create_edit', [
              'pbxs'           => app (PBXRepository::class)->all(),
              'routers'        => app (RouterRepository::class)->all(),
              'vpn_clients'    => app (VPNClientRepository::class)->all(),
              'vpn_servers'    => app (VPNServerRepository::class)->all(),
              'users'          => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewDECT $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, DECT::class);

        try {

          app (DECTManagementContract::class)
            ->initiator ($request->user())
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('telecoms.dect.index')
            ->with ('success', 'DECT created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, DECT $dect) : View
    {
        $this->authorize (__FUNCTION__, $dect);

        try {

            $dect->load ([
                'room.building.property',
                'pbx',
                'handsets.room.building.property',
                'vpn_clients', 'vpn_servers', 'purchaser', 'installer', 'provisioner',
            ]);

            return $this->view($this->view_subdir.__FUNCTION__, compact('dect'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, DECT $dect) : View
    {
        $this->authorize (__FUNCTION__, $dect);

        try {

            return $this->view($this->view_subdir.'create_edit', [
              'dect'           => $dect,
              'pbxs'           => app (PBXRepository::class)->all(),
              'routers'        => app (RouterRepository::class)->all(),
              'vpn_clients'    => app (VPNClientRepository::class)->all(),
              'vpn_servers'    => app (VPNServerRepository::class)->all(),
              'users'          => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateDECT $request, DECT $dect) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $dect);

        try {

          app (DECTManagementContract::class)
            ->initiator ($request->user())
            ->entity ($dect)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('telecoms.dect.index')
            ->with ('success', 'DECT updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, DECT $dect) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $dect);

        try {

          app (DECTManagementContract::class)
            ->initiator ($request->user())
            ->entity ($dect)
            ->destroy ();

          return redirect ()->route ('telecoms.dect.index')
            ->with ('success', 'DECT deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
