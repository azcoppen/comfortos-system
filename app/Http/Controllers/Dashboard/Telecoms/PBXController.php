<?php

namespace SmartRooms\Http\Controllers\Dashboard\Telecoms;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Telecoms\PBX;

use SmartRooms\Http\Requests\Dashboard\StoreNewPBX;
use SmartRooms\Http\Requests\Dashboard\UpdatePBX;

use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\PBXRepository;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\PBXManagementContract;

final class PBXController extends BaseController
{
    public $repository = PBXRepository::class;
    public $app_section = 'telecoms';
    public $nav_section = 'pbxs';
    public $view_subdir = 'telecoms.pbxs.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, PBX::class);

        try {

          $records = $this->manager ($request);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate(25),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, PBX::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'vpn_clients'    => app (VPNClientRepository::class)->all(),
              'vpn_servers'    => app (VPNServerRepository::class)->all(),
              'users'          => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewPBX $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, PBX::class);

        try {

          app (PBXManagementContract::class)
          ->initiator ($request->user())
          ->params ($request->all())
          ->store ();

          return redirect ()->route ('telecoms.pbxs.index')
            ->with ('success', 'PBX created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, PBX $pbx) : View
    {
        $this->authorize (__FUNCTION__, $pbx);

        try {

            $pbx->load ([
                'dects.room.building.property',
                'dects.handsets',
                'extensions.room.building.property',
                'handsets.room.building.property',
                'purchaser', 'installer', 'provisioner',
            ]);

            return $this->view ($this->view_subdir.__FUNCTION__, compact('pbx'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, PBX $pbx) : View
    {
        $this->authorize (__FUNCTION__, $pbx);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'pbx'            => $pbx,
              'vpn_clients'    => app (VPNClientRepository::class)->all(),
              'vpn_servers'    => app (VPNServerRepository::class)->all(),
              'users'          => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdatePBX $request, PBX $pbx) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $pbx);

        try {

          app (PBXManagementContract::class)
            ->initiator ($request->user())
            ->entity ($pbx)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('telecoms.pbxs.index')
            ->with ('success', 'PBX updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, PBX $pbx) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $pbx);

        try {

          app (PBXManagementContract::class)
            ->initiator ($request->user())
            ->entity ($pbx)
            ->destroy ();

          return redirect ()->route ('telecoms.pbxs.index')
            ->with ('success', 'PBX deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
