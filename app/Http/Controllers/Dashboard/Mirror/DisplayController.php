<?php

namespace SmartRooms\Http\Controllers\Dashboard\Mirror;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Mirror\Display;

use SmartRooms\Http\Requests\Dashboard\StoreNewDisplay;
use SmartRooms\Http\Requests\Dashboard\UpdateDisplay;

use SmartRooms\Contracts\Repositories\Components\Mirror\DisplayRepository;
use SmartRooms\Contracts\Repositories\Components\Mirror\ModuleRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;
use SmartRooms\Contracts\Repositories\Components\Wifi\RouterRepository;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Criteria\HasRolesCriteria;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\MirrorDisplayManagementContract;

final class DisplayController extends BaseController
{
    public $repository = DisplayRepository::class;
    public $app_section = 'mirrors';
    public $nav_section = 'displays';
    public $view_subdir = 'mirrors.displays.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Display::class);

        try {

          $records = $this->manager ($request)
            ->with ([
                'room.building.property',
            ]);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('created_at', 'DESC'));
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate(25),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request) : View
    {
        $this->authorize (__FUNCTION__, Display::class);

        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'modules'     => app (ModuleRepository::class)->all(),
              'routers'     => app (RouterRepository::class)->all(),
              'vpn_clients' => app (VPNClientRepository::class)->all(),
              'vpn_servers' => app (VPNServerRepository::class)->all(),
              'users'       => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreNewDisplay $request) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, Display::class);

        try {

          app (MirrorDisplayManagementContract::class)
            ->initiator ($request->user())
            ->params ($request->all())
            ->store ();

          return redirect ()->route ('mirrors.displays.index')
            ->with ('success', 'Display created successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, Display $display) : View
    {
        $this->authorize (__FUNCTION__, $display);

        try {

            $display->load (['router.room.building.property', 'modules', 'purchaser', 'installer', 'provisioner',]);

            return $this->view($this->view_subdir.__FUNCTION__, compact('display'));

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, Display $display) : View
    {
        $this->authorize (__FUNCTION__, $display);

        try {

          $display->load (['modules']);

          return $this->view ($this->view_subdir.'create_edit', [
            'display'     => $display,
            'modules'     => app (ModuleRepository::class)->all(),
            'routers'     => app (RouterRepository::class)->all(),
            'vpn_clients' => app (VPNClientRepository::class)->all(),
            'vpn_servers' => app (VPNServerRepository::class)->all(),
            'users'       => app (UserRepository::class)->pushCriteria (new HasRolesCriteria (['developer']))->get(),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update (UpdateDisplay $request, Display $display) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $display);

        try {

          app (MirrorDisplayManagementContract::class)
            ->initiator ($request->user())
            ->entity ($display)
            ->params ($request->all())
            ->update ();

          return redirect ()->route ('mirrors.displays.index')
            ->with ('success', 'Display updated successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function destroy (Request $request, Display $display) : RedirectResponse
    {
        $this->authorize (__FUNCTION__, $display);

        try {

          app (MirrorDisplayManagementContract::class)
            ->initiator ($request->user())
            ->entity ($display)
            ->destroy ();

          return redirect ()->route ('mirrors.displays.index')
            ->with ('success', 'Display deleted successfully.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
