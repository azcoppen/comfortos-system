<?php

namespace SmartRooms\Http\Controllers\Dashboard\Mirror;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Mirror\Module;

use SmartRooms\Http\Requests\Dashboard\StoreNewModule;
use SmartRooms\Http\Requests\Dashboard\UpdateModule;

use SmartRooms\Contracts\Repositories\Components\Mirror\ModuleRepository;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\MirrorModuleManagementContract;

final class ModuleController extends BaseController
{
  public $repository = ModuleRepository::class;
  public $app_section = 'mirrors';
  public $nav_section = 'modules';
  public $view_subdir = 'mirrors.modules.';

  public function index (Request $request, ?string $tag = null) : View
  {
      $this->authorize (__FUNCTION__, Module::class);

      try {

          $records = $this->manager ($request);

          if (! $request->has ('q') )
          {
            $records->pushCriteria (new OrderByCriteria('module', 'ASC'));
            
            $records->scopeQuery(function ($query) use ($tag) {
                return $query->where('tags', 'all', [$tag]);
            });

            $this->nav_section = $tag;
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'records' => $records->paginate(25),
            'tag'     => $tag,
          ]);

      } catch (Exception $e) {
          return $this->page_exception_response($e);
      }
  }

  public function create (Request $request, ?string $tag = null) : View
  {
      $this->authorize (__FUNCTION__, Module::class);

      try {

        return $this->view ($this->view_subdir.'create_edit', [
          'tag'   => $tag,
        ]);

      } catch (Exception $e) {
          return $this->page_exception_response($e);
      }
  }

  public function store (StoreNewModule $request) : RedirectResponse
  {
      $this->authorize (__FUNCTION__, Module::class);

      try {

        app (MirrorModuleManagementContract::class)
          ->initiator ($request->user())
          ->params ($request->all())
          ->store ();

        return redirect ()->route ('mirrors.modules.index')
          ->with ('success', 'Module created successfully.');

      } catch (Exception $e) {
          return $this->page_exception_response($e);
      }
  }

  public function show (Request $request, Module $module) : View
  {
      $this->authorize (__FUNCTION__, $module);

      try {

        $this->nav_section = head ($module->tags);

        return $this->view ($this->view_subdir.__FUNCTION__, compact('module'));

      } catch (Exception $e) {
          return $this->page_exception_response($e);
      }
  }

  public function edit (Request $request, Module $module) : View
  {
      $this->authorize (__FUNCTION__, $module);

      try {

        $this->nav_section = head ($module->tags);

        return $this->view ($this->view_subdir.'create_edit', compact('module'));

      } catch (Exception $e) {
          return $this->page_exception_response($e);
      }
  }

  public function update (UpdateModule $request, Module $module) : RedirectResponse
  {
      $this->authorize (__FUNCTION__, $module);

      try {

        app (MirrorModuleManagementContract::class)
          ->initiator ($request->user())
          ->entity ($module)
          ->params ($request->all())
          ->update ();

        return redirect ()->route ('mirrors.modules.index')
          ->with ('success', 'Module updated successfully.');

      } catch (Exception $e) {
          return $this->page_exception_response($e);
      }
  }

  public function destroy (Request $request, Module $module) : RedirectResponse
  {
      $this->authorize (__FUNCTION__, $display);

      try {

        app (MirrorModuleManagementContract::class)
          ->initiator ($request->user())
          ->entity ($module)
          ->destroy ();

        return redirect ()->route ('mirrors.modules.index')
          ->with ('success', 'Module deleted successfully.');

      } catch (Exception $e) {
          return $this->page_exception_response($e);
      }
  }

}
