<?php

namespace SmartRooms\Http\Controllers\Dashboard\Houston;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Contracts\Repositories\ComfortOS\MIAAlertRepository;

use SmartRooms\Criteria\OrderByCriteria;

final class MIAController extends BaseController
{
    public $repository  = MIAAlertRepository::class;
    public $app_section = 'dashboards';
    public $nav_section = 'mia';
    public $view_subdir = 'dashboards.mia.';

    public function index (Request $request) : View
    {
        try {

          if ( $request->has ('q') )
          {
            $q_alerts = app (app($this->repository)->model())::search (format_search_query($request->get('q')))
              ->orderBy ('created_at', 'DESC')
              ->get ();
          }

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'q_alerts' => $q_alerts ?? null
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
