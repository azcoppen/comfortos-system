<?php

namespace SmartRooms\Http\Controllers\Dashboard\Houston;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;

use SmartRooms\Contracts\Repositories\ComfortOS\ChecklistRepository;

final class ChecklistController extends BaseController
{
    public $app_section = 'dashboards';
    public $nav_section = 'checklists';
    public $view_subdir = 'dashboards.checklists.';

    public function index (Request $request, string $type) : View
    {
        try {

          $this->nav_section = $type;

          return $this->view ($this->view_subdir.__FUNCTION__, [
            'type' => $type,
            'records' => app (ChecklistRepository::class)->findWhere (['type' => $type]),
          ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

}
