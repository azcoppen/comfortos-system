<?php

namespace SmartRooms\Http\Controllers\Dashboard\Houston;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Collection;

use SmartRooms\Http\Controllers\Dashboard\BaseController;

use SmartRooms\Contracts\Repositories\ComfortOS\HABCommandRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\HABSignalRepository;
use SmartRooms\Contracts\Repositories\ComfortOS\OpQueueRepository;

use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;
use SmartRooms\Criteria\ComfortOS\OperatorsAvailableToUser;

use SmartRooms\Models\Property\Room;
use SmartRooms\Models\Components\Devices;
use SmartRooms\Models\Components\Mirror;
use SmartRooms\Models\Components\Telecoms;
use SmartRooms\Models\Components\Wifi;

final class DashboardController extends BaseController
{
    public $app_section = 'dashboards';
    public $nav_section = 'dashboards';
    public $view_subdir = 'dashboards.';

    private $cpt_model_map = [
      'bulbs'       => Devices\Bulb::class,
      'cameras'     => Devices\Csmera::class,
      'chromecasts' => Devices\Chromecast::class,
      'dects'       => Telecoms\DECT::class,
      'dimmers'     => Devices\Dimmer::class,
      'extensions'  => Telecoms\Extension::class,
      'handsets'    => Telecoms\Handset::class,
      'leds'        => Devices\LED::class,
      'locks'       => Devices\Lock::class,
      'mirrors'     => Mirror\Display::class,
      'motors'      => Devices\Motor::class,
      'plugs'       => Devices\Plug::class,
      'routers'     => Wifi\Router::class,
      'sensors'     => Devices\Sensor::class,
      'speakers'    => Devices\Speaker::class,
      'stbs'        => Devices\STB::class,
      'switches'    => Devices\PhysicalSwitch::class,
      'thermostats' => Devices\Thermostat::class,
      'tvs'         => Devices\TV::class,
      'wifi'        => Wifi\Network::class,
    ];

    private function user_properties (Request $request) : Collection
    {
      $operators = app (OperatorRepository::class)->pushCriteria (new OperatorsAvailableToUser ($request->user()) )
        ->with (['brands.properties'])->all();

      $brands  = $operators->pluck('brands')->flatten();
      return $brands->pluck('properties')->flatten();
    }

    public function index (Request $request) : View
    {
        try {

          $this->nav_section = 'signals';
          return $this->view ($this->view_subdir.__FUNCTION__);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function room_code_generator (Request $request) : View
    {
        try {

          $this->nav_section = 'room-generator';
          return $this->view ($this->view_subdir.__FUNCTION__);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function cpt_code_generator (Request $request) : View
    {
        try {

          $this->nav_section = 'cpt-generator';
          return $this->view ($this->view_subdir.__FUNCTION__);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function room_search (Request $request) : JsonResponse
    {
        try {

          if (! $request->has('q') )
          {
            return response()->json([]);
          }

          $results = Room::search ($request->get('q', ''))
            ->whereIn('property_id', $this->user_properties($request)->pluck('_id')->unique()->all())
            ->get()
            ->load (['building.property']);

          return response()->json ($results->map(function ($item, $key) {
              return [
                'value'   => $item->_id,
                'text'    => '# '.$item->number . ' ('.$item->label.'), Floor '.$item->floor.', '.$item->building->label.', '.$item->building->property->label,
              ];
          }));


        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function component_search (Request $request, ?string $group = null) : JsonResponse
    {
        try {

          if (! $group )
          {
            return response()->json([]);
          }

          if (! $request->has('q') )
          {
            return response()->json([]);
          }

          if (! array_key_exists ($group, $this->cpt_model_map) )
          {
            return response()->json([]);
          }

          $results = app ($this->cpt_model_map[$group])::search ($request->get('q', ''))
            ->whereIn('property_id', $this->user_properties($request)->pluck('_id')->unique()->all())
            ->get()
            ->load (['room.building.property']);

          return response()->json ($results->map(function ($item, $key) {
              return [
                'value'   => $item->_id,
                'text'    => $item->label.' '.($item->type ? '('.$item->type.')' : '') .', Room #'.$item->room->number . ' ('.$item->room->label.'), Floor '.$item->room->floor.', '.$item->room->building->label.', '.$item->room->building->property->label,
              ];
          }));


        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function operations (Request $request) : View
    {
        try {

          if ( $request->has ('q') )
          {
            $q_ops = app (app(OpQueueRepository::class)->model())::search (format_search_query($request->get('q')))
              ->orderBy ('created_at', 'DESC')
              ->get ();
          }

          $this->nav_section = 'operations';
          return $this->view ($this->view_subdir.__FUNCTION__, ['q_ops' => $q_ops ?? null]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function signals (Request $request) : View
    {
        try {

          if ( $request->has ('q') )
          {
            $q_signals = app (app(HABSignalRepository::class)->model())::search (format_search_query($request->get('q')))
              ->orderBy ('created_at', 'DESC')
              ->get ();
          }

          $this->nav_section = 'signals';
          return $this->view ($this->view_subdir.__FUNCTION__, ['q_signals' => $q_signals ?? null]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function commands (Request $request) : View
    {
        try {

          if ( $request->has ('q') )
          {
            $q_commands = app (app(HABCommandRepository::class)->model())::search (format_search_query($request->get('q')))
              ->orderBy ('created_at', 'DESC')
              ->get ();
          }

          $this->nav_section = 'commands';
          return $this->view ($this->view_subdir.__FUNCTION__, ['q_commands' => $q_commands ?? null]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
