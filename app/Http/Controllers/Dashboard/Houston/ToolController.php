<?php

namespace SmartRooms\Http\Controllers\Dashboard\Houston;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;



final class ToolController extends BaseController
{
    public $app_section = 'dashboards';
    public $nav_section = 'tools';
    public $view_subdir = 'dashboards.tools.';

    public function mqtt_publisher (Request $request) : View
    {
        try {

          $this->nav_section = 'mqtt';
          return $this->view ($this->view_subdir.__FUNCTION__);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function http_poster (Request $request) : View
    {
        try {

          $this->nav_section = 'http';
          return $this->view ($this->view_subdir.__FUNCTION__);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

}
