<?php

namespace SmartRooms\Http\Controllers\Dashboard\TV;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Devices\STB;

use SmartRooms\Http\Requests\TV\StoreNewSTV;
use SmartRooms\Http\Requests\TV\UpdateSTB;

use SmartRooms\Contracts\Repositories\Components\Devices\TVAppRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\STBRepository;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\STBManagementContract;

final class STBController extends BaseController
{
    public $repository = STBRepository::class;
    public $app_section = 'tvs';
    public $nav_section = 'stbs';
    public $view_subdir = 'tv.stbs.';

    public function index (Request $request) : View
    {
        $this->authorize (__FUNCTION__, STB::class);

        try {

            return $this->view ($this->view_subdir.__FUNCTION__, [
              'records' => app ($this->repository)
                ->with(['tv', 'installed', 'room.building.property'])
                ->pushCriteria(new OrderByCriteria('created_at', 'DESC'))
                ->paginate(25),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function show (Request $request, STB $stb) : View
    {
        try {

            $stb->load (['tv', 'room.building.property']);

            return $this->view ($this->view_subdir.__FUNCTION__, [
                'stb' => $stb,
                'apps' => app (TVAppRepository::class)->findWhereIn('_id', $stb->apps ?? [])->all(),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
