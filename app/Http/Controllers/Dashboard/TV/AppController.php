<?php

namespace SmartRooms\Http\Controllers\Dashboard\TV;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Models\Components\Devices\TVApp;

use SmartRooms\Http\Requests\TV\StoreNewTVApp;
use SmartRooms\Http\Requests\TV\UpdateTVApp;

use SmartRooms\Contracts\Repositories\Components\Devices\TVAppRepository;
use SmartRooms\Criteria\OrderByCriteria;

use SmartRooms\Contracts\System\STBAppNetworkManagementContract;

final class AppController extends BaseController
{
    public $repository = TVAppRepository::class;
    public $app_section = 'tvs';
    public $nav_section = 'apps';
    public $view_subdir = 'tv.apps.';

    public function index (Request $request, ?string $tag = null) : View
    {
        $this->authorize (__FUNCTION__, TVApp::class);

        try {

            $prepare = app ($this->repository)
              ->pushCriteria (new OrderByCriteria('label', 'ASC'));

            if ( $tag )
            {

                $prepare = $prepare->scopeQuery (function ($query) use ($tag) {
                    return $query->where ('tags', 'all', [$tag]);
                });

                $this->nav_section = $tag;
            }

            return $this->view ($this->view_subdir.__FUNCTION__, [
              'records' => $prepare->paginate(25),
              'tag' => $tag,
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
