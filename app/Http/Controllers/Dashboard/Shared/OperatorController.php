<?php

namespace SmartRooms\Http\Controllers\Dashboard\Shared;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use SmartRooms\Contracts\Controllers\SharedSubresourceController;
use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;
use SmartRooms\Contracts\System\OperatorAssociationManagementContract;
use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Http\Requests\Dashboard\IDAM\StoreOperatorAssociations;
use SmartRooms\Http\Requests\Dashboard\StoreSharedObjectRoles;

final class OperatorController extends BaseController implements SharedSubresourceController
{
    public $app_section = 'idam';
    public $nav_section = 'users';
    public $sub_section = 'operators';
    public $repository = OperatorRepository::class;
    public $view_subdir = 'idam.users.operators.';

    public function index(Request $request, ...$subresources) : View
    {
        try {
            return $this->view($this->view_subdir.__FUNCTION__, [
            'record'    => head($subresources)->load(['operators.brands']),
            'operators' => app($this->repository)->paginate(25),
          ]);
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store(StoreOperatorAssociations $request, ...$subresources) : RedirectResponse
    {
        try {
            app(OperatorAssociationManagementContract::class)
            ->target(head($subresources))
            ->sync($request->input('operators') ?? []);

            return redirect()->back()->with('success', 'Operator associations updated successfully.');
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
