<?php

namespace SmartRooms\Http\Controllers\Dashboard\Shared;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Models\Property\Room;
use SmartRooms\Http\Controllers\Dashboard\BaseController;

use SmartRooms\Charts\CommandsChart;
use SmartRooms\Charts\SignalsChart;
use SmartRooms\Charts\DailyCommandsChart;
use SmartRooms\Charts\DailySignalsChart;
use SmartRooms\Charts\ComponentCommandAttributeChart;
use SmartRooms\Charts\ComponentSignalAttributeChart;
use SmartRooms\Charts\DailyComponentCommandAttributeChart;
use SmartRooms\Charts\DailyComponentSignalAttributeChart;

final class GraphDataController extends BaseController
{
  public function room_commands_tput (Request $request, Room $room) : JsonResponse
  {
      return new JsonResponse ((new CommandsChart)
      ->room ($room)
      ->handler($request)->toObject());
  }

  public function room_commands_tput_hourly (Request $request, Room $room) : JsonResponse
  {
      return new JsonResponse ((new DailyCommandsChart)
      ->room ($room)
      ->handler($request)->toObject());
  }

  public function room_signals_tput (Request $request, Room $room) : JsonResponse
  {
      return new JsonResponse ((new SignalsChart)
      ->room ($room)
      ->handler($request)->toObject());
  }

  public function room_signals_tput_hourly (Request $request, Room $room) : JsonResponse
  {
      return new JsonResponse ((new DailySignalsChart)
      ->room ($room)
      ->handler($request)->toObject());
  }

  public function component_commands_tput (Request $request, $model) : JsonResponse
  {
      return new JsonResponse ((new CommandsChart)
      ->component ($model)
      ->handler($request)->toObject());
  }

  public function component_commands_tput_hourly (Request $request, $model) : JsonResponse
  {
      return new JsonResponse ((new DailyCommandsChart)
      ->component ($model)
      ->handler($request)->toObject());
  }

  public function component_signals_tput (Request $request, $model) : JsonResponse
  {
      return new JsonResponse ((new SignalsChart)
      ->component ($model)
      ->handler($request)->toObject());
  }

  public function component_signals_tput_hourly (Request $request, $model) : JsonResponse
  {
      return new JsonResponse ((new DailySignalsChart)
      ->component ($model)
      ->handler($request)->toObject());
  }

  public function component_commands_attribute (Request $request, $model, string $attribute) : JsonResponse
  {
      return new JsonResponse ((new ComponentCommandAttributeChart)
      ->component ($model)
      ->attribute ($attribute)
      ->handler($request)->toObject());
  }

  public function component_commands_attribute_hourly (Request $request, $model, string $attribute) : JsonResponse
  {
      return new JsonResponse ((new DailyComponentCommandAttributeChart)
      ->component ($model)
      ->attribute ($attribute)
      ->handler($request)->toObject());
  }

  public function component_signals_attribute (Request $request, $model, string $attribute) : JsonResponse
  {
      return new JsonResponse ((new ComponentSignalAttributeChart)
      ->component ($model)
      ->attribute ($attribute)
      ->handler($request)->toObject());
  }

  public function component_signals_attribute_hourly (Request $request, $model, string $attribute) : JsonResponse
  {
      return new JsonResponse ((new DailyComponentSignalAttributeChart)
      ->component ($model)
      ->attribute ($attribute)
      ->handler($request)->toObject());
  }
}
