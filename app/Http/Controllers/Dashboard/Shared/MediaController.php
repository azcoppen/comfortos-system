<?php

namespace SmartRooms\Http\Controllers\Dashboard\Shared;

use Illuminate\Http\Request;
use SmartRooms\Contracts\Controllers\SharedSubresourceController;
use SmartRooms\Http\Controllers\Dashboard\BaseController;

final class MediaController extends BaseController implements SharedSubresourceController
{
    public $app_section = '{section}';
    public $nav_section = '{object}';
    public $sub_section = 'media';
    public $relation = 'media';
    public $view_subdir = '{section}.{object}.media.';

    public function __construct()
    {
        //parent::__construct();
    }
}
