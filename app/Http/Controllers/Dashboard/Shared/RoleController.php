<?php

namespace SmartRooms\Http\Controllers\Dashboard\Shared;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use SmartRooms\Contracts\Controllers\SharedSubresourceController;
use SmartRooms\Contracts\Repositories\IDAM\RoleRepository;
use SmartRooms\Contracts\System\RoleAssociationManagementContract;
use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Http\Requests\Dashboard\IDAM\StoreRoleAssociations;
use SmartRooms\Http\Requests\Dashboard\StoreSharedObjectRoles;

final class RoleController extends BaseController implements SharedSubresourceController
{
    public $app_section = 'idam';
    public $nav_section = 'users';
    public $sub_section = 'roles';
    public $repository = RoleRepository::class;
    public $view_subdir = 'idam.users.roles.';

    public function index(Request $request, ...$subresources) : View
    {
        try {
            return $this->view($this->view_subdir.__FUNCTION__, [
            'record'  => head($subresources)->load(['roles']),
            'roles'   => app($this->repository)->all(),
          ]);
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store(StoreRoleAssociations $request, ...$subresources) : RedirectResponse
    {
        try {
            app(RoleAssociationManagementContract::class)
            ->target(head($subresources))
            ->sync($request->input('roles') ?? []);

            return redirect()->back()->with('success', 'Role associations updated successfully.');
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
