<?php

namespace SmartRooms\Http\Controllers\Dashboard\Shared;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use SmartRooms\Contracts\Controllers\SharedSubresourceController;
use SmartRooms\Contracts\Repositories\Shared\NoteRepository;
use SmartRooms\Http\Controllers\Dashboard\BaseController;

final class NoteController extends BaseController implements SharedSubresourceController
{
    public $app_section = 'idam';
    public $nav_section = 'users';
    public $sub_section = 'notes';
    public $repository = NoteRepository::class;
    public $view_subdir = 'idam.users.notes.';

    public function index(Request $request, ...$subresources) : View
    {
        try {
            return $this->view($this->view_subdir.__FUNCTION__, [
              'record'  => head($subresources),
              'records' => head($subresources)->notes()->with(['author'])->orderBy('created_at', 'DESC')->paginate(25),
            ]);
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
