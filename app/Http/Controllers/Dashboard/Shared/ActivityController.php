<?php

namespace SmartRooms\Http\Controllers\Dashboard\Shared;

use Illuminate\Http\Request;
use SmartRooms\Contracts\Controllers\SharedSubresourceController;
use SmartRooms\Http\Controllers\Dashboard\BaseController;

final class ActivityController extends BaseController implements SharedSubresourceController
{
    public $app_section = '{section}';
    public $nav_section = '{object}';
    public $sub_section = 'activity';
    public $relation = 'activities';
    public $view_subdir = '{section}.{object}.activity.';

    public function __construct()
    {
        //parent::__construct();
    }
}
