<?php

namespace SmartRooms\Http\Controllers\Dashboard\Shared;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use SmartRooms\Contracts\Controllers\SharedSubresourceController;
use SmartRooms\Contracts\Repositories\IDAM\PermissionRepository;
use SmartRooms\Contracts\System\PermissionAssociationManagementContract;
use SmartRooms\Criteria\OrderByCriteria;
use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Http\Requests\Dashboard\IDAM\StorePermissionAssociations;
use SmartRooms\Http\Requests\Dashboard\StoreSharedObjectPermissions;

final class PermissionController extends BaseController implements SharedSubresourceController
{
    public $app_section = 'idam';
    public $nav_section = 'users';
    public $sub_section = 'permissions';
    public $repository = PermissionRepository::class;
    public $view_subdir = 'idam.users.permissions.';

    public function index(Request $request, ...$subresources) : View
    {
        try {
            return $this->view($this->view_subdir.__FUNCTION__, [
              'record'        => head($subresources)->load(['permissions']),
              'permissions'   => app($this->repository)->pushCriteria(new OrderByCriteria('name', 'ASC'))->paginate(1000),
            ]);
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store(StorePermissionAssociations $request, ...$subresources) : RedirectResponse
    {
        try {
            app(PermissionAssociationManagementContract::class)
              ->target(head($subresources))
              ->sync($request->input('permissions') ?? []);

            return redirect()->back()->with('success', 'Permission associations updated successfully.');
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
