<?php

namespace SmartRooms\Http\Controllers\Dashboard\Shared;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use SmartRooms\Contracts\ComfortOS\RoomAccessManagementContract;
use SmartRooms\Contracts\Controllers\SharedSubresourceController;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Http\Controllers\Dashboard\BaseController;
use SmartRooms\Http\Requests\Dashboard\IDAM\ModifyRoomAccess;
use SmartRooms\Http\Requests\Dashboard\IDAM\StoreRoomAccess;

final class AccessController extends BaseController implements SharedSubresourceController
{
    public $app_section = 'idam';
    public $nav_section = 'users';
    public $sub_section = 'rooms';
    public $repository = RoomRepository::class;
    public $view_subdir = 'idam.users.rooms.';

    public function index (Request $request, ...$subresources) : View
    {
        try {
            return $this->view($this->view_subdir.__FUNCTION__, [
              'record'  => head($subresources),
              'records' => head($subresources)
                ->accesses()
                ->withTrashed()
                ->orderBy('end_at', 'DESC')
                ->with(['accessible.building.property.brand.operator'])
                ->paginate(25),
            ]);
        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function create (Request $request, ...$subresources) : View
    {
        try {

            return $this->view($this->view_subdir.'create_edit', [
              'record'  => head($subresources),
              'properties' =>  app(RoomRepository::class)->with(['building.property'])->all()->groupBy('building.property.label')->sortKeys()->transform(function ($item, $key) {
                  return $item->pluck('number', '_id')->sort()->transform(function ($i, $k) use ($key) {
                      return '('.$key.') Room #'.$i;
                  });
              }),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function store (StoreRoomAccess $request, ...$subresources) : RedirectResponse
    {
        try {

            app (RoomAccessManagementContract::class)
              ->creator ($request->user())
              ->user (head($subresources))
              ->room (app(RoomRepository::class)->find($request->input('room_id')))
              ->start ($request->input('start_at_date'), $request->input('start_at_time'))
              ->end ($request->input('end_at_date'), $request->input('end_at_time'))
              ->store ();

            return redirect()->route ('idam.users.accesses.index', [$request->user()->_id])
                ->with('success', 'Room access added.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function edit (Request $request, ...$subresources) : View
    {
        try {

            return $this->view ($this->view_subdir.'create_edit', [
              'record'  => head($subresources),
              'access'  => isset($subresources[1]) && is_object($subresources[1]) ? $subresources[1]->load(['accessible.building.property']) : null,
              'properties' =>  app(RoomRepository::class)->with(['building.property'])->all()->groupBy('building.property.label')->sortKeys()->transform(function ($item, $key) {
                  return $item->pluck('number', '_id')->sort()->transform(function ($i, $k) use ($key) {
                      return '('.$key.') Room #'.$i;
                  });
              }),
            ]);

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function update( ModifyRoomAccess $request, ...$subresources) : RedirectResponse
    {
        try {

            app (RoomAccessManagementContract::class)
              ->creator ($request->user())
              ->existing ($subresources[1] ?? null)
              ->room (app(RoomRepository::class)->find($request->input('room_id')))
              ->start ($request->input('start_at_date'), $request->input('start_at_time'))
              ->end ($request->input('end_at_date'), $request->input('end_at_time'))
              ->modify ();

            return redirect()->route ('idam.users.accesses.index', [$request->user()->_id])
                ->with('success', 'Room access modified.');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }

    public function halt (Request $request, ...$subresources) : RedirectResponse
    {
        try {

            app (RoomAccessManagementContract::class)
              ->existing($subresources[1] ?? null)
              ->halt();

            return redirect()->route ('idam.users.accesses.index', [$request->user()->_id])
                ->with('success', 'Room access removed (immediate)');

        } catch (Exception $e) {
            return $this->page_exception_response($e);
        }
    }
}
