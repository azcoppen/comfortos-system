<?php

namespace SmartRooms\Http\Controllers\Dashboard;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View as ViewFacade;
use Illuminate\View\View;
use Log;
use \ReflectionClass;
use Illuminate\Support\Str;

use SmartRooms\Contracts\Controllers\SharedSubresourceController;
use SmartRooms\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;
use SmartRooms\Criteria\ComfortOS\OperatorsAvailableToUser;

abstract class BaseController extends Controller
{
    /**
     * Shared Parent function for packaging up views. Chedks their existence, but can also
     * load composers etc, if necessary.
     *
     * @param  string    $blade    The template to load
     * @param  array  $data  The payload
     *
     * @return Illuminate\View\View
     */
    public function view (string $blade, array $data = null) : View
    {
        if (ViewFacade::exists($blade)) {
            if ($data && count($data)) {
                return view ($blade, array_merge(
                  $data,
                  [
                      'app_section' => $this->app_section ?? null,
                      'nav_section' => $this->nav_section ?? null,
                      'sub_section' => $this->sub_section ?? null,
                      'user_roles'  => auth()->user() ? auth()->user()->roles()->get() : null,
                  ]
                ));
            }

            return view ($blade, [
              'app_section' => $this->app_section ?? null,
              'nav_section' => $this->nav_section ?? null,
              'sub_section' => $this->sub_section ?? null,
              'user_roles'  => auth()->user() ? auth()->user()->roles()->get() : null,
            ]);
        }

        return view ('errors.404');
    }

    /**
     * Shared parent function handle view exceptions.
     *
     * @param  Exception    $e    The exception object
     *
     * @return Illuminate\View\View
     */
    public function page_exception_response (Exception $e) : View
    {
        if (env('APP_ENV') == 'local') {
            throw $e;
        }
        Log::error($e);

        return view('errors.500', compact('e'));
    }

    /**
     * Shared parent function handle exceptions requiring a redirection.
     *
     * @param  Exception    $e    The exception object
     *
     * @return Illuminate\View\View
     */
    public function redirect_exception_response (Exception $e) : RedirectResponse
    {
        Log::error($e);

        return redirect()->back()
        ->withInput()
        ->with('error', $e->getMessage());
    }

    public function manager ( Request $request, ?array $where = null )
    {
      if ( $request->has ('q') && !empty ($request->get('q')) )
      {
        if ( $where && count ($where) )
        {
          return app(app ($this->repository)->model())::search (format_search_query($request->get('q')))
            ->where(array_key_first($where), $where[array_key_first($where)]);
        }

        return app(app ($this->repository)->model())::search (format_search_query($request->get('q')));
      }

      return app ($this->repository);
    }

    public function _get_model_plural ( $model, bool $lower = false ) : string
    {
        $plural = Str::plural ( (new ReflectionClass ($model))->getShortName() );
        return $lower ? strtolower ($plural) : $plural;
    }

    public function operators_for_user ( Request $request )
    {
      return app (OperatorRepository::class)
        ->pushCriteria (new OperatorsAvailableToUser ($request->user()));
    }

}
