<?php

namespace SmartRooms\Http\Controllers\Guest;

use SmartRooms\Http\Controllers\Dashboard\BaseController AS DashBaseController;

use \Cache;
use \Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use Illuminate\Support\Str;

use SmartRooms\Http\Requests\Guest\OTPLogin;

use SmartRooms\Contracts\System\OTPVerificationContract;
use SmartRooms\Models\Property\Room;

use SmartRooms\Exceptions\Guest AS GuestExceptions;

abstract class BaseController extends DashBaseController
{
    public function manage_session ( Request $request, $model )
    {
      if ( session ('lang') )
      {
        \App::setLocale ( session ('lang') );
      }

      if ( Cache::get ($model->_id.'-session-end') )
      {
        if ( time () > Cache::get ($model->_id.'-session-end')->timestamp )
        {
          if (! session ('override') )
          {
            throw new GuestExceptions\SessionExpiredException (__('guest/auth.login.errors.session'));
          }
        }
      }

      if ( session ('model_lock') != $model->_id )
      {
        if (! session ('override') )
        {
          throw new GuestExceptions\AccessViolationException (__('guest/auth.violation'));
        }
      }

      return $model;
    }

    public function login ( Request $request, $model, ?string $prefill = null ) : View
    {
      switch (get_class ($model))
      {
        case Room::class:
          return $this->view ('guest.room.login', ['room' => $model, 'prefill' => $prefill]);
        break;

        default:

          if ( in_array (get_class ($model), $this->allowed) )
          {
            return $this->view ('guest.component.login', ['component' => $model, 'prefill' => $prefill]);
          }

          return $this->view ('guest.component.forbidden', ['component' => $model, 'prefill' => $prefill]);

        break;
      }
    }

    public function verify ( OTPLogin $request, $model ) : RedirectResponse
    {
      try
      {

        if (! is_object ($model) )
        {
          return redirect()->back()->withInput()->with ('error', __('guest/auth.login.errors.object'));
        }

        switch (get_class ($model))
        {
          case Room::class:
            $model->load (['users']);
            $users = $model->users;
          break;

          default:
            $model->load (['room.users']);
            $users = $model->room->users;
          break;
        }

        // Don't load anything if the room has no users to log in as
        if (! $users->first() )
        {
          return redirect()->back()->withInput()->with ('error', __('guest/auth.login.errors.no_users'));
        }

        // completely override anything entered for superadmins - debugging
        // but don't interfere with cache session of someone else
        if ( in_array ($request->input ('otp'), config ('comfortos.overrides')) )
        {
          auth()->login ($users->first());

          session (['override' => $model->_id]);

          switch (get_class ($model))
          {
            case Room::class:
              return redirect()->route ('guest.room.display', $model->_id);
            break;

            default:
              return redirect()->route ('guest.component.'.Str::lower(Str::singular($model->plural)).'.display', $model->_id);
            break;
          }

        }

        $secs = intval ( Str::before ( $request->input ('otp'), 'M') ) * 60;

        $verifiable = Str::between ($request->input ('otp'), 'M', 'S');

        // Make sure the code is being used in the correct time period
        if (! $result = app (OTPVerificationContract::class)->verify ($model->_id, $verifiable, 6, $secs) )
        {
          return redirect()->back()->withInput()->with ('error', __('guest/auth.login.errors.invalid_code'));
        }

        // Find when to end the session
        $hours = intval ( Str::after ( $request->input ('otp'), 'S') );

        // If no time period, limit it to an hour
        if (! $hours || $hours < 1)
        {
          $hours = 1;
        }

        // Check if someone else is already logged in.
        if ( Cache::get ($model->_id.'-session-end') )
        {
          if ( Cache::get ($model->_id.'-session-end')->timestamp > time () )
          {
            return redirect()->back()->withInput()->with ('warning', __('guest/auth.login.errors.conflict'));
          }
        }

        auth()->login ($users->first(), true);

        // don't allow logged in users to view anything else
        session (['model_lock' => $model->_id]);

        Cache::put ($model->_id.'-session-start', now(), now()->addHours(730)); // keep for a month
        Cache::put ($model->_id.'-session-end', now()->addHours($hours), now()->addHours(730)); // keep for a month

        switch (get_class ($model))
        {
          case Room::class:
            return redirect()->route ('guest.room.display', $model->_id);
          break;

          default:
            return redirect()->route ('guest.component.'.Str::lower(Str::singular($model->plural)).'.display', $model->_id);
          break;
        }

      }
      catch (Exception $e)
      {
          return redirect()->back()->withInput()->with ('error', $e->getMessage ());
      }
    }

    public function logout ( Request $request, $model) : RedirectResponse
    {
      try
      {

        Cache::forget ($model->_id.'-session-start');
        Cache::forget ($model->_id.'-session-end');

        auth()->logout();

        session()->flush();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        switch (get_class ($model))
        {
          case Room::class:
            return redirect ()->route ('guest.room.login', $model->_id);
          break;

          default:
            return redirect ()->route ('guest.component.'.Str::lower(Str::singular($model->plural)).'.login', $model->_id);
          break;
        }

        return redirect ()->route ('guest.room.login', $model->_id);

      }
      catch (Exception $e)
      {
        switch (get_class ($model))
        {
          case Room::class:
            return redirect ()
              ->route ('guest.room.login', $model->_id)
              ->with ('error', $e->getMessage ());
          break;

          default:
            return redirect ()
              ->route ('guest.component.'.Str::lower(Str::singular($model->plural)).'.login', $model->_id)
              ->with ('error', $e->getMessage ());
          break;
        }
      }
    }
}
