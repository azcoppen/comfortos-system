<?php

namespace SmartRooms\Http\Controllers\Guest;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Models\Property\Room;
use SmartRooms\Models\Components\Wifi\Network;

use SmartRooms\Http\Requests\Guest\StoreWifiNetwork;

use SmartRooms\Contracts\System\VirtualWifiNetworkManagementContract;

final class WifiController extends BaseController
{
  public $app_section = 'wifi';

  public function index (Request $request, Room $room)
  {
      try
      {

        $room->load (['users', 'component_map', 'wifi_networks', 'building.property.brand.operator', 'building.property.mqtt_brokers', 'building.property.ws_brokers']);

        $this->manage_session ($request, $room);

        return $this->view('guest.room.screens.wifi', compact('room'));

      }
      catch (Exception $e)
      {
        return redirect ()->route('guest.room.login', $room->_id)
          ->with ('error', $e->getMessage ());
      }
  }

  public function store (StoreWifiNetwork $request, Room $room) : RedirectResponse
  {
      try
      {

        app (VirtualWifiNetworkManagementContract::class)
          ->initiator ($request->user())
          ->parent ($room)
          ->params ($request->only (['label', 'ssid', 'password', 'expire_at_date', 'expire_at_time']))
          ->store ('wifi-networks');

        return redirect ()->route ('guest.room.wifi.index', $room->_id);

      }
      catch (Exception $e)
      {
        throw $e;
        return redirect ()->back()
          ->with ('error', $e->getMessage ());
      }
  }

  public function destroy (Request $request, Room $room, Network $network) : RedirectResponse
  {
      try
      {
        app (VirtualWifiNetworkManagementContract::class)
          ->initiator ($request->user())
          ->parent ($room)
          ->entity ($network)
          ->destroy ();

        return redirect ()->route ('guest.room.wifi.index', $room->_id);

      }
      catch (Exception $e)
      {
        return redirect ()->back()
          ->with ('error', $e->getMessage ());
      }
  }
}
