<?php

namespace SmartRooms\Http\Controllers\Guest;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use Illuminate\Support\Str;

use SmartRooms\Models\Property\Room;

use SmartRooms\Models\Components\Devices;
use SmartRooms\Models\Components\Telecoms;
use SmartRooms\Models\Components\Wifi;

final class ComponentController extends BaseController
{
    public $app_section = 'component';

    public $allowed = [
      Devices\Bulb::class,
      Devices\Chromecast::class,
      Devices\LED::class,
      Devices\Lock::class,
      Devices\Motor::class,
      Devices\Plug::class,
      Devices\Speaker::class,
      Devices\Thermostat::class,
      Devices\TV::class,
    ];

    public function index ( Request $request, $model )
    {
      try
      {

        $this->manage_session ($request, $model);

        $model->load (['room',]);

        return $this->view ('guest.component.screens.'.Str::lower(Str::singular($model->plural)), ['component' => $model]);

      }
      catch (Exception $e)
      {
        return redirect ()->route('guest.component.'.Str::lower(Str::singular($model->plural)).'.login')
          ->with ('error', $e->getMessage ());
      }
    }
}
