<?php

namespace SmartRooms\Http\Controllers\Guest;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use SmartRooms\Models\Property\Room;

final class PowerController extends BaseController
{
  public $app_section = 'power';

  public function index (Request $request, Room $room)
  {
      try
      {

        $room->load (['users', 'component_map', 'plugs', 'building.property.brand.operator', 'building.property.mqtt_brokers', 'building.property.ws_brokers']);

        $this->manage_session ($request, $room);

        return $this->view('guest.room.screens.power', compact('room'));

      }
      catch (Exception $e)
      {
        return redirect ()->route('guest.room.login', $room->_id)
          ->with ('error', $e->getMessage ());
      }
  }
}
