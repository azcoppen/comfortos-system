<?php

namespace SmartRooms\Http\Controllers\Guest;

use \Cache;
use \Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

use SmartRooms\Rules\Latitude;
use SmartRooms\Rules\Longitude;

use SmartRooms\Models\Property\Room;

use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Criteria\NearCriteria;
use SmartRooms\Criteria\OrderByCriteria;

final class LocatorController extends BaseController
{
    public $app_section = 'locator';

    public function options (Request $request, string $coords) : View
    {
        try
        {

          if ( Str::contains ($coords, ',') )
          {
            $parts = explode (',', $coords);

            $validator = Validator::make([
              'lat' => $parts[0] ?? null,
              'lon' => $parts[1] ?? null,
            ], [
                'lat' => ['required', 'numeric', new Latitude],
                'lat' => ['required', 'numeric', new Longitude],
            ]);

            if ( $validator->passes () )
            {
              $rooms = app (RoomRepository::class)
                ->pushCriteria (new NearCriteria ('geo', floatval($parts[0] ?? 0), floatval ($parts[1] ?? 0), 50))
                ->pushCriteria (new OrderByCriteria ('distance', 'ASC'))
                ->with(['building.property'])
                ->all();

              return $this->view('guest.locator', [
                'rooms'     => $rooms,
                'latitude'  => floatval($parts[0] ?? 0),
                'longitude' => floatval($parts[1] ?? 0),
              ]);
            }
          }

          return $this->view ('guest.broken_geolocation', ['error_msg' => "We can't locate you from the coordinates you've supplied. You need to provide a valid latitude and longitude."]);

        }
        catch (Exception $e)
        {
          return $this->view ('guest.broken_geolocation', ['error_msg' => $e->getMessage()]);
        }
    }

}
