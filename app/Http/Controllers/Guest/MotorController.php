<?php

namespace SmartRooms\Http\Controllers\Guest;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use SmartRooms\Models\Property\Room;

final class MotorController extends BaseController
{
  public $app_section = 'motors';

  public function index (Request $request, Room $room)
  {
      try
      {

        $room->load (['users', 'component_map', 'motors', 'building.property.brand.operator', 'building.property.mqtt_brokers', 'building.property.ws_brokers']);

        $this->manage_session ($request, $room);

        return $this->view('guest.room.screens.motors', compact('room'));

      }
      catch (Exception $e)
      {
        return redirect ()->route('guest.room.login', $room->_id)
          ->with ('error', $e->getMessage ());
      }
  }
}
