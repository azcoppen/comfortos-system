<?php

namespace SmartRooms\Http\Controllers\Guest;

use \Cache;
use \Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

use SmartRooms\Models\Property\Room;

final class RoomController extends BaseController
{
    public $app_section = 'start';

    public function index (Request $request, Room $room)
    {
        try
        {

          $this->manage_session ($request, $room);

          $room->load (['users', 'component_map', 'building.property.brand.operator', 'building.property.mqtt_brokers', 'building.property.ws_brokers']);

          return $this->view('guest.room.screens.start', compact('room'));

        }
        catch (Exception $e)
        {
          return redirect ()->route('guest.room.login', $room->_id)
            ->with ('error', $e->getMessage ());
        }
    }

}
