<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\SpeakerRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateSpeaker;
use SmartRooms\Models\Components\Devices\Speaker;

use SmartRooms\Transformers\API\ComfortOS\SpeakerTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class SpeakerController extends BaseController
{
    public $repository = SpeakerRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Speaker::class;
    public $transformer = SpeakerTransformer::class;

    public $allowed = ['power', 'mode', 'preset', 'mute', 'volume', 'controls', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (UpdateSpeaker $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
