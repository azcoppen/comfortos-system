<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\LEDRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateLED;
use SmartRooms\Models\Components\Devices\LED;

use SmartRooms\Transformers\API\ComfortOS\LEDTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class LEDController extends BaseController
{
    public $repository = LEDRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = LED::class;
    public $transformer = LEDTransformer::class;

    public $allowed = ['power', 'brightness', 'hsb', 'temperature', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function store (Request $request) : JsonResponse
    {
        return $this->__store($request);
    }

    public function update (UpdateLED $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
