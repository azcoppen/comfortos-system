<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\LockRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateLock;
use SmartRooms\Models\Components\Devices\Lock;

use SmartRooms\Transformers\API\ComfortOS\LockTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class LockController extends BaseController
{
    public $repository = LockRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals', 'codes'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Lock::class;
    public $transformer = LockTransformer::class;

    public $allowed = ['command', 'insert', 'wipe', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (UpdateLock $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
