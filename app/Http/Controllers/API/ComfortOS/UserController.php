<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Criteria\ComfortOS\UsersAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateUser;
use SmartRooms\Models\IDAM\User;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class UserController extends BaseController
{
    public $repository = UserRepository::class;
    public $eager_load = [];
    public $model = User::class;

    public function __construct()
    {
        $this->filter = new UsersAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public $constraint_field = 'user_id'; // how ES searches should be limited
    public $constraint = 'users';

    public function update (UpdateUser $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
