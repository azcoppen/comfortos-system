<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\DimmerRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateDimmer;
use SmartRooms\Models\Components\Devices\Dimmer;

use SmartRooms\Transformers\API\ComfortOS\DimmerTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class DimmerController extends BaseController
{
    public $repository = DimmerRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Dimmer::class;
    public $transformer = DimmerTransformer::class;

    public $allowed = ['power', 'level', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (UpdateDimmer $request, $model) : JsonResponse
    {
        return $this->__update ($request, $model);
    }
}
