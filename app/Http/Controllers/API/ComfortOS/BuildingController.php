<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Property\BuildingRepository;
use SmartRooms\Criteria\ComfortOS\BuildingsAvailableToUserCriteria;
use SmartRooms\Models\Property\Building;

use SmartRooms\Transformers\API\ComfortOS\BuildingTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class BuildingController extends BaseController
{
    public $repository = BuildingRepository::class;
    public $collection_eager_load = ['rooms'];
    public $single_eager_load = ['rooms'];

    public $constraint_field = 'building_id'; // how ES searches should be limited
    public $constraint = 'buildings';

    public $model = Building::class;
    public $transformer = BuildingTransformer::class;

    public function __construct()
    {
        $this->filter = new BuildingsAvailableToUserCriteria(auth('api')->user());
    }
}
