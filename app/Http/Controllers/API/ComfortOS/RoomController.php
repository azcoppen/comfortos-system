<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Criteria\ComfortOS\RoomsAvailableToUserCriteria;
use SmartRooms\Models\Property\Room;

use SmartRooms\Transformers\API\ComfortOS\RoomTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class RoomController extends BaseController
{
    public $repository = RoomRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = [
      'signals',
      'bulbs', 'cameras', 'chromecasts', 'dimmers', 'extensions', 'handsets',
      'leds', 'locks', 'mirrors', 'motors', 'plugs', 'sensors', 'speakers',
      'stbs', 'switches', 'thermostats', 'tvs', 'wifi',
    ];

    public $show_transformer_args = [
      'relations' => [
        'bulbs', 'cameras', 'chromecasts', 'dimmers', 'extensions', 'handsets',
        'leds', 'locks', 'mirrors', 'motors', 'plugs', 'sensors', 'speakers',
        'stbs', 'switches', 'thermostats', 'tvs', 'wifi',
      ]
    ];

    public $model = Room::class;
    public $transformer = RoomTransformer::class;

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new RoomsAvailableToUserCriteria(auth('api')->user());
    }
}
