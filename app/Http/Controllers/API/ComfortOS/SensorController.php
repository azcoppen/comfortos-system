<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\SensorRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Models\Components\Devices\Sensor;

use SmartRooms\Transformers\API\ComfortOS\SensorTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class SensorController extends BaseController
{
    public $repository = SensorRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Sensor::class;
    public $transformer = SensorTransformer::class;

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }
}
