<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\TVRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateTV;
use SmartRooms\Models\Components\Devices\TV;

use SmartRooms\Transformers\API\ComfortOS\TVTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class TVController extends BaseController
{
    public $repository = TVRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = TV::class;
    public $transformer = TVTransformer::class;

    public $allowed = ['power', 'brightness', 'contrast', 'sharpness', 'channel', 'keycode', 'volume', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (UpdateTV $request, $model) : JsonResponse
    {
        return $this->__update ($request, $model);
    }
}
