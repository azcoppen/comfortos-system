<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\ChromecastRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateChromecast;
use SmartRooms\Models\Components\Devices\Chromecast;

use SmartRooms\Transformers\API\ComfortOS\ChromecastTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class ChromecastController extends BaseController
{
    public $repository = ChromecastRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Chromecast::class;
    public $transformer = ChromecastTransformer::class;

    public $allowed = ['power', 'mute', 'volume', 'controls', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (UpdateChromecast $request, $model) : JsonResponse
    {
        return $this->__update ($request, $model);
    }
}
