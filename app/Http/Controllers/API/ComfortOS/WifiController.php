<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Wifi\NetworkRepository;
use SmartRooms\Criteria\ComfortOS\WifiNetworksAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\StoreWifiNetwork;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateWifiNetwork;
use SmartRooms\Models\Components\Wifi\Network;

use SmartRooms\Transformers\API\ComfortOS\WifiTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class WifiController extends BaseController
{
    public $repository = NetworkRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = [];

    public $show_transformer_args = ['method' => 'show'];
    public $model = Network::class;

    public $transformer = WifiTransformer::class;

    public $allowed = ['name', 'password', 'room', 'launch_at', 'expire_at', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->middleware ('throttle:5,1')->only (['store', 'update', 'destroy']);
        $this->filter = new WifiNetworksAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function store (StoreWifiNetwork $request) : JsonResponse
    {
        return $this->__store($request);
    }

    public function update (UpdateWifiNetwork $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
