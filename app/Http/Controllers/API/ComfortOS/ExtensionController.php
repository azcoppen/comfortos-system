<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Telecoms\ExtensionRepository;
use SmartRooms\Criteria\ComfortOS\ExtensionsAvailableToUserCriteria;
use SmartRooms\Models\Components\Telecoms\Extension;

use SmartRooms\Transformers\API\ComfortOS\ExtensionTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class ExtensionController extends BaseController
{
    public $repository = ExtensionRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['handsets', 'pbx'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Extension::class;
    public $transformer = ExtensionTransformer::class;

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new ExtensionsAvailableToUserCriteria(auth('api')->user(), $this->model);
    }
}
