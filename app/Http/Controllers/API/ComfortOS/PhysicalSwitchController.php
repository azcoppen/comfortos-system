<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\PhysicalSwitchRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdatePhysicalSwitch;
use SmartRooms\Models\Components\Devices\PhysicalSwitch;

use SmartRooms\Transformers\API\ComfortOS\SwitchTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class PhysicalSwitchController extends BaseController
{
    public $repository = PhysicalSwitchRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = PhysicalSwitch::class;
    public $transformer = SwitchTransformer::class;

    public $allowed = ['power', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (UpdatePhysicalSwitch $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
