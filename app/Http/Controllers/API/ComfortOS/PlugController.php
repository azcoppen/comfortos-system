<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\PlugRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdatePlug;
use SmartRooms\Models\Components\Devices\Plug;

use SmartRooms\Transformers\API\ComfortOS\PlugTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class PlugController extends BaseController
{
    public $repository = PlugRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Plug::class;
    public $transformer = PlugTransformer::class;

    public $allowed = ['power', 'level', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (UpdatePlug $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
