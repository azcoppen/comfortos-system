<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Mirror\DisplayRepository;
use SmartRooms\Criteria\ComfortOS\MirrorsAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateMirror;
use SmartRooms\Models\Components\Mirror\Display;

use SmartRooms\Transformers\API\ComfortOS\MirrorTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class MirrorController extends BaseController
{
    public $repository = DisplayRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals', 'modules'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Display::class;
    public $transformer = MirrorTransformer::class;

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new MirrorsAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function store (Request $request) : JsonResponse
    {
        return $this->__store($request);
    }

    public function update (UpdateMirror $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
