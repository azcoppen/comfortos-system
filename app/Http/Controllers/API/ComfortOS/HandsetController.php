<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Telecoms\HandsetRepository;
use SmartRooms\Criteria\ComfortOS\HandsetsAvailableToUserCriteria;
use SmartRooms\Models\Components\Telecoms\Handset;

use SmartRooms\Transformers\API\ComfortOS\HandsetTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class HandsetController extends BaseController
{
    public $repository = HandsetRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['signals', 'extensions'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Handset::class;
    public $transformer = HandsetTransformer::class;

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new HandsetsAvailableToUserCriteria(auth('api')->user(), $this->model);
    }
}
