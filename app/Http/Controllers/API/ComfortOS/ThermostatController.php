<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\ThermostatRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateThermostat;
use SmartRooms\Models\Components\Devices\Thermostat;

use SmartRooms\Transformers\API\ComfortOS\ThermostatTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class ThermostatController extends BaseController
{
    public $repository = ThermostatRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Thermostat::class;
    public $transformer = ThermostatTransformer::class;

    public $allowed = ['power', 'cooling_point', 'heating_point', 'mode', 'op_state', 'fan_mode', 'fan_state', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (UpdateThermostat $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
