<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\MotorRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateMotor;
use SmartRooms\Models\Components\Devices\Motor;

use SmartRooms\Transformers\API\ComfortOS\MotorTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class MotorController extends BaseController
{
    public $repository = MotorRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Motor::class;
    public $transformer = MotorTransformer::class;

    public $allowed = ['power', 'dir', 'position', 'speed', 'mode', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (UpdateMotor $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
