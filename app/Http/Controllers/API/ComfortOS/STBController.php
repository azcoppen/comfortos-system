<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\STBRepository;
use SmartRooms\Criteria\ComfortOS\STBsAvailableToUserCriteria;
use SmartRooms\Models\Components\Devices\STB;

use SmartRooms\Transformers\API\ComfortOS\STBTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class STBController extends BaseController
{
    public $repository = STBRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals', 'tv_apps'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = STB::class;
    public $transformer = STBTransformer::class;

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new STBsAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update (Request $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
