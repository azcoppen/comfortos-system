<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\BulbRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateBulb;
use SmartRooms\Models\Components\Devices\Bulb;

use SmartRooms\Transformers\API\ComfortOS\BulbTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class BulbController extends BaseController
{
    public $repository = BulbRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Bulb::class;
    public $transformer = BulbTransformer::class;

    public $allowed = ['power', 'brightness', 'hsb', 'temperature', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function store (Request $request) : JsonResponse
    {
        return $this->__store($request);
    }

    public function update (UpdateBulb $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
