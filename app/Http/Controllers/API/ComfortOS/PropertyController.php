<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Property\PropertyRepository;
use SmartRooms\Criteria\ComfortOS\PropertiesAvailableToUserCriteria;
use SmartRooms\Models\Property\Property;

use SmartRooms\Transformers\API\ComfortOS\PropertyTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class PropertyController extends BaseController
{
    public $repository = PropertyRepository::class;
    public $collection_eager_load = ['buildings'];
    public $single_eager_load = ['buildings'];

    public $model = Property::class;
    public $transformer = PropertyTransformer::class;

    public $constraint_field = 'property_id'; // how ES searches should be limited
    public $constraint = 'properties';

    public function __construct()
    {
        $this->filter = new PropertiesAvailableToUserCriteria(auth('api')->user());
    }
}
