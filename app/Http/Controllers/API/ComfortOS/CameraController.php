<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Contracts\Repositories\Components\Devices\CameraRepository;
use SmartRooms\Criteria\ComfortOS\DevicesAvailableToUserCriteria;
use SmartRooms\Http\Requests\API\ComfortOS\UpdateCamera;
use SmartRooms\Models\Components\Devices\Camera;

use SmartRooms\Transformers\API\ComfortOS\CameraTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class CameraController extends BaseController
{
    public $repository = CameraRepository::class;
    public $collection_eager_load = [];
    public $single_eager_load = ['commands', 'signals'];

    public $show_transformer_args = ['method' => 'show'];

    public $model = Camera::class;
    public $transformer = CameraTransformer::class;

    public $allowed = ['power', 'execute_at'];

    public $constraint_field = 'room_id'; // how ES searches should be limited
    public $constraint = 'rooms';

    public function __construct()
    {
        $this->filter = new DevicesAvailableToUserCriteria(auth('api')->user(), $this->model);
    }

    public function update(UpdateCamera $request, $model) : JsonResponse
    {
        return $this->__update($request, $model);
    }
}
