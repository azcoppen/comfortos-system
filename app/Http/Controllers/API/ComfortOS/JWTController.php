<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SmartRooms\Http\Requests\API\JWTLogin;
use SmartRooms\Transformers\TransparentTransformer;

use SmartRooms\Contracts\System\WSBrokerAccessContract;

final class JWTController extends BaseController
{
    private function room_ws_tokens () : array
    {
      $tokens = [];

      foreach (auth('api')->user()->accesses()->with(['accessible'])->with(['accessible.building.property.ws_brokers'])->get() AS $access)
      {
        $tokens[$access->accessible->_id] = app (WSBrokerAccessContract::class)->room ($access->accessible)->token();
      }

      return $tokens;
    }

    public function authenticate(JWTLogin $request) : JsonResponse
    {
        try {

            if (! $token = auth('api')->attempt($request->only(['email', 'password']))) {
                return $this->json_error_response('Unauthorized: username or password incorrect.', 401);
            }

            auth('api')->user()->update([
              'last_login_at'     => new Carbon,
              'last_activity_at'  => new Carbon,
            ]);

            return response()->json([
              'access_token'    => $token,
              'ws_token'        => app (WSBrokerAccessContract::class)->user (auth('api')->user())->token(),
              'token_type'      => 'bearer',
              'expires_in'      => auth('api')->factory()->getTTL() * 60,
              'room_ws_tokens'  => $this->room_ws_tokens(),
            ]);

        } catch (Exception $e) {
            return $this->json_exception_response($e);
        }
    }

    public function me (Request $request) : JsonResponse
    {
        try {

            return $this->json_item_response(auth('api')->user(), TransparentTransformer::class);

        } catch (Exception $e) {
            return $this->json_exception_response($e);
        }
    }

    public function logout (Request $request) : JsonResponse
    {
        try {

            auth('api')->logout();

            return $this->json_success_response('Successfully logged out');

        } catch (Exception $e) {
            return $this->json_exception_response($e);
        }
    }

    public function refresh (Request $request) : JsonResponse
    {
        try {

            return response()->json([
              'access_token'    => auth('api')->refresh(),
              'ws_token'        => app('centrifuge')->generateToken($request->user()->_id),
              'token_type'      => 'bearer',
              'expires_in'      => auth('api')->factory()->getTTL() * 60,
              'room_ws_tokens'  => $this->room_ws_tokens(),
            ]);

        } catch (Exception $e) {
            return $this->json_exception_response($e);
        }
    }
}
