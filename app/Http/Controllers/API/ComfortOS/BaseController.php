<?php

namespace SmartRooms\Http\Controllers\API\ComfortOS;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Log;
use SmartRooms\Criteria\EagerLoadCriteria;
use SmartRooms\Http\Controllers\Controller;
use SmartRooms\Transformers\ExceptionTransformer;
use SmartRooms\Transformers\TransparentTransformer;

use SmartRooms\Contracts\ComfortOS\UserConstraintManagementContract;

class BaseController extends Controller
{
    public $repository;
    public $dataset;
    public $filter;
    public $model;
    public $eager_load;
    public $collection_eager_load;
    public $single_eager_load;
    public $transformer;
    public $exception_transformer = ExceptionTransformer::class;

    public $eager_load_on = ['show'];

    private function api_headers () : array
    {
        return [
            'X-Env'   => env('APP_ENV'),
            'X-User'  => request()->user()->_id,
            'X-Roles' => request()->user()->roles->pluck('name')->unique()->implode(', '),
            'X-Rooms' => collect(app(UserConstraintManagementContract::class)->target(auth('api')->user())->rooms())->implode(', '),
        ];
    }

    /**
     * Shared Parent function for packaging up views. Chedks their existence, but can also
     * load composers etc, if necessary.
     *
     * @param  string    $blade    The template to load
     * @param  array  $data  The payload
     *
     * @return Illuminate\View\View
     */
    public function build (Request $request)
    {
      if ( $request->has ('q') && !empty ($request->get('q')) )
      {
        $this->dataset = app(app ($this->repository)->model())::search (format_search_query ($request->get('q')));

        if ($this->constraint_field && $this->constraint)
        {
          $this->dataset->whereIn ($this->constraint_field, app(UserConstraintManagementContract::class)->target(auth('api')->user())->{$this->constraint}());
        }

        //dd($this->dataset);

        if ( isset ($this->collection_eager_load) && is_array ($this->collection_eager_load) && count ($this->collection_eager_load))
        {
            $this->dataset->with ($this->collection_eager_load);
        }

      }

      if (! $request->has ('q') )
      {
        $this->dataset = app ($this->repository);

        if (! $this->dataset)
        {
            throw new Exception ('Could not build repository.');
        }

        if ( $this->filter )
        {
            $this->dataset->pushCriteria ($this->filter);
        }

        if ( isset ($this->collection_eager_load) && is_array ($this->collection_eager_load) && count ($this->collection_eager_load))
        {
            $this->dataset->pushCriteria (new EagerLoadCriteria ($this->collection_eager_load));
        }
      }

      return $this->dataset;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index (Request $request) : JsonResponse
    {
        $this->authorize (__FUNCTION__, $this->model);

        try {

            return $this->json_collection_response
            (
                $this->build ($request)->paginate(25),
                $this->transformer ?? TransparentTransformer::class
            );

        } catch (Exception $e) {
          throw $e;
            return $this->json_exception_response($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __store ($request) : JsonResponse
    {
        $this->authorize('store', $this->model);

        try {

            if (! count ($request->all()) )
            {
              return $this->json_error_response ("Request cannot be blank.", 400);
            }

            if ( isset ($this->allowed) && count ($this->allowed) )
            {
              foreach ($request->all() AS $name => $val)
              {
                if (! in_array ($name, $this->allowed) )
                {
                  return $this->json_error_response ($name." is not an acceptable parameter.", 400);
                }
              }
            }

            if (! config('comfortos.api.managers.'.get_class($this)) )
            {
              throw new Exception ("No manager class defined for ".get_class($this));
            }

            $new_entity = app(config('comfortos.api.managers.'.get_class($this)))
                ->request($request)
                ->store();

            return $this->json_item_response(
                $new_entity,
                $this->transformer ?? TransparentTransformer::class,
                201
            );

        } catch (Exception $e) {
            return $this->json_exception_response($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function show (Request $request, $model) : JsonResponse
    {
        $this->authorize(__FUNCTION__, $model);

        try {

            if (isset($this->single_eager_load) && is_array($this->single_eager_load) && count($this->single_eager_load)) {
                $model->load($this->single_eager_load);
            }

            if (isset($this->show_transformer_args) && is_array($this->show_transformer_args) && count($this->show_transformer_args)) {
                $this->transformer = app()->makeWith($this->transformer, $this->show_transformer_args);
            }

            return $this->json_item_response(
                  $model,
                  $this->transformer ?? TransparentTransformer::class
              );

        } catch (Exception $e) {
            return $this->json_exception_response($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $request
     * @param  $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function __update ($request, $model) : JsonResponse
    {
        $this->authorize('update', $model);

        try {

            if (! count ($request->all()) )
            {
              return $this->json_error_response ("Request cannot be blank.", 400);
            }

            if ( isset ($this->allowed) && count ($this->allowed) )
            {
              foreach ($request->all() AS $name => $val)
              {
                if (! in_array ($name, $this->allowed) )
                {
                  return $this->json_error_response ($name." is not an acceptable parameter.", 400);
                }
              }
            }

            if (! config('comfortos.api.managers.'.get_class($this)) )
            {
              throw new Exception ("No manager class defined for ".get_class($this));
            }

            $result = app(config('comfortos.api.managers.'.get_class($this)))
                ->entity($model)
                ->request($request)
                ->update();

            if (isset($this->single_eager_load) && is_array($this->single_eager_load) && count($this->single_eager_load)) {
                $model->load($this->single_eager_load);
            }

            return $this->json_item_response(
                $model,
                $this->transformer ?? TransparentTransformer::class
            );

        } catch (Exception $e) {
            return $this->json_exception_response($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy (Request $request, $model) : JsonResponse
    {
        $this->authorize(__FUNCTION__, $model);

        try {

            if (! config('comfortos.api.managers.'.get_class($this)) )
            {
              throw new Exception ("No manager class defined for ".get_class($this));
            }

            $result = app(config('comfortos.api.managers.'.get_class($this)))
                ->entity($model)
                ->request($request)
                ->destroy();

            return $this->json_item_response(
                ['deleted' => true],
                TransparentTransformer::class
            );

        } catch (Exception $e) {
            return $this->json_exception_response($e);
        }
    }

    /**
     * Shared parent function handle JSON data transmission.
     *
     * @param  array|object    $collection    The collection to transform
     * @param  string|object $transformer  The transformer to use
     * @param int $code The HTTP code to send
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function json_collection_response ($collection, $transformer, int $code = 200) : JsonResponse
    {
        return fractal()->collection($collection, $transformer)
            ->respond($code, $this->api_headers(), JSON_PRETTY_PRINT | JSON_HEX_APOS | JSON_HEX_QUOT);
    }

    /**
     * Shared parent function handle JSON data transmission.
     *
     * @param  array|object    $item    The thing to transform
     * @param  string|object $transformer  The transformer to use
     * @param int $code The HTTP code to send
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function json_item_response ($item, $transformer, int $code = 200) : JsonResponse
    {
        return fractal()->item($item, $transformer)
            ->respond($code, $this->api_headers(), JSON_PRETTY_PRINT | JSON_HEX_APOS | JSON_HEX_QUOT);
    }

    /**
     * Shared parent function handle JSON exceptions.
     *
     * @param  Exception    $e    The exception object
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function json_exception_response (Exception $e, $code = 500) : JsonResponse
    {
        Log::error($e);

        return fractal()->item($e, $this->exception_transformer)
            ->respond($code, [], JSON_PRETTY_PRINT | JSON_HEX_APOS | JSON_HEX_QUOT);
    }

    public function json_success_response ($msg, $data = [], $code = 200) : JsonResponse
    {
        return fractal()->item(array_merge($data, ['msg' => $msg]), new TransparentTransformer)
        ->respond($code, ['X-ROOMS' => collect($this->rooms())->implode(',')], JSON_PRETTY_PRINT);
    }

    public function json_error_response ($error_msg, $error_code = 500) : JsonResponse
    {
        return fractal()->item(['error' => $error_msg], TransparentTransformer::class)
        ->respond($error_code, ['X-ROOMS' => collect($this->rooms())->implode(',')], JSON_PRETTY_PRINT);
    }
}
