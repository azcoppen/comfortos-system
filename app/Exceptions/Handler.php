<?php

namespace SmartRooms\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
      try
      {
        if ( env ('FLOW_CHANNEL') )
        {
          \Log::channel (env ('FLOW_CHANNEL'))->error ($exception);
        }
      }
      catch (Exception $e)
      {

      }

        if ($request->route() && ! empty($request->route()->getName())) {
            if ($exception instanceof AuthenticationException && str_contains($request->route()->getName(), 'api.')) {
                return response()->json(['message' => $exception->getMessage()], 401);
            }
            if ($exception instanceof AuthorizationException && str_contains($request->route()->getName(), 'api.')) {
                return response()->json(['message' => $exception->getMessage()], 401);
            }
        }

        return parent::render($request, $exception);
    }
}
