<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Broadcaster
    |--------------------------------------------------------------------------
    |
    | This option controls the default broadcaster that will be used by the
    | framework when an event needs to be broadcast. You may set this to
    | any of the connections defined in the "connections" array below.
    |
    | Supported: "pusher", "redis", "log", "null"
    |
    */

    'default' => env('BROADCAST_DRIVER', 'null'),

    /*
    |--------------------------------------------------------------------------
    | Broadcast Connections
    |--------------------------------------------------------------------------
    |
    | Here you may define all of the broadcast connections that will be used
    | to broadcast events to other systems or over websockets. Samples of
    | each available type of connection are provided inside this array.
    |
    */

    'connections' => [

        'crossbar' => [
            'driver' => 'crossbar',
            'options' => [
                'sockets' => [
                    'default' => [
                        'ws_realm'  => env('CROSSBAR_WS_DEFAULT_REALM', 'smartrooms'), // WAMP v2 realm for UI to join (in .crossvar/config.json)
                        'ws_host'   => env('CROSSBAR_WS_DEFAULT_HOST', 'wss://127.0.0.1'), // URL of the Websocket (in .crossvar/config.json)
                        'ws_port'   => env('CROSSBAR_WS_DEFAULT_PORT', 7000), // Port for the Websocket connection (in .crossvar/config.json, set to 80/443 if proxying/running clear)
                    ],
                ],
                'publish' => [
                    'http_host'     => env('CROSSBAR_PUBLISH_HTTP_HOST', 'http://127.0.0.1'), // Address of the Crossbar HTTP service (in .crossvar/config.json)
                    'http_port'     => env('CROSSBAR_PUBLISH_HTTP_PORT', 8000), // Port of the Crossbar HTTP service (in .crossvar/config.json, set for 80/443 if proxying/running clear)
                    'path'          => env('CROSSBAR_PUBLISH_HTTP_PATH', '/publish'), //URI Crossbar HTTP service (in .crossvar/config.json)
                    'log_request'   => env('CROSSBAR_PUBLISH_LOG_REQUEST', true),
                    'log_response'  => env('CROSSBAR_PUBLISH_LOG_RESPONSE', true),
                ],
            ],
        ],

        'centrifuge' => [
            'driver'       => 'centrifuge',
            'url'          => env('CENTRIFUGE_URL', 'http://127.0.0.1:8000'),
            'token_ttl'    => env('CENTRIFUGE_TOKEN_TTL', 3600),
            'token_issuer' => env('APP_URL', 'default'),
            'secret'       => env('CENTRIFUGE_SECRET', null),
            'api_key'      => env('CENTRIFUGE_API_KEY', null),
            'ssl_key'      => env('CENTRIFUGE_SSL_KEY', null),
            'verify'       => env('CENTRIFUGE_VERIFY', false),
        ],

        'dynamic-centrifuge' => [
            'driver'       => 'dynamic-centrifuge',
            'url'          => env('CENTRIFUGE_URL', 'http://127.0.0.1:8000'),
            'token_ttl'    => env('CENTRIFUGE_TOKEN_TTL', 3600),
            'token_issuer' => env('APP_URL', 'default'),
            'secret'       => env('CENTRIFUGE_SECRET', null),
            'api_key'      => env('CENTRIFUGE_API_KEY', null),
            'ssl_key'      => env('CENTRIFUGE_SSL_KEY', null),
            'verify'       => env('CENTRIFUGE_VERIFY', false),
        ],

        'pusher' => [
            'driver' => 'pusher',
            'key' => env('PUSHER_APP_KEY'),
            'secret' => env('PUSHER_APP_SECRET'),
            'app_id' => env('PUSHER_APP_ID'),
            'options' => [
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'useTLS' => true,
            ],
        ],

        'redis' => [
            'driver' => 'redis',
            'connection' => 'default',
        ],

        'log' => [
            'driver' => 'log',
        ],

        'null' => [
            'driver' => 'null',
        ],

    ],

];
