<?php

return [

    /* -----------------------------------------------------------------
     |  Database
     | -----------------------------------------------------------------
     */

    'database' => [
        'connection' => env('DB_CONNECTION', 'mysql'),

        'prefix'     => null,
    ],

    /* -----------------------------------------------------------------
     |  Models
     | -----------------------------------------------------------------
     */

    'authors' => [
        'table' => 'users',
        'model' => Smartrooms\Models\IDAM\User::class,
    ],

    'notes' => [
        'table' => 'notes',
        'model' => SmartRooms\Models\Shared\Note::class,
    ],

];
