'use strict'

var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var inject = require('gulp-inject');
var debug = require('gulp-debug');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var sass = require('gulp-sass');
var merge = require('gulp-merge');
var injectPartials = require('gulp-inject-partials');
var replace = require('gulp-replace');
var cleanCSS = require('gulp-clean-css');

/* Static Server + watching scss/html files */
gulp.task('sass', function () {
    return gulp.src('./scss/**/*-template.scss')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(cleanCSS())
        .pipe(browserSync.stream());
});

/* Start server & execute the code */
gulp.task('serve', gulp.series('sass', function() {
    browserSync.init({
        port: 5001,
        server: "./",
        ghostMode: false,
        notify: false
    });
    gulp.watch('./scss/**/*.scss', gulp.series('sass'));
    gulp.watch('./demo/**/*.html').on('change', browserSync.reload);
    gulp.watch('./dist/js/**/*.js').on('change', browserSync.reload);
}));

/* Inject Flite Demo CSS */
gulp.task('injectFliteDemoStyles', function () {
    var target = gulp.src('./demo/flite/pages/**/*.html');
    var sources = gulp.src([
        './dist/css/vendor.styles.css',
        './dist/css/demo/flite-template.css'
    ], {read: false});
    return target.pipe(inject(sources, { relative: true }))
        .pipe(gulp.dest('./demo/'));
});

/* Inject Legacy Demo CSS */
gulp.task('injectLegacyDemoStyles', function () {
    var target = gulp.src('./demo/legacy/pages/**/*.html');
    var sources = gulp.src([
        './dist/css/vendor.styles.css',
        './dist/css/demo/legacy-template.css'
    ], {read: false});
    return target.pipe(inject(sources, { relative: true }))
        .pipe(gulp.dest('./demo/'));
});

/* Inject Dark Demo CSS */
gulp.task('injectDarkDemoStyles', function () {
    var target = gulp.src('./demo/dark-double/pages/**/*.html');
    var sources = gulp.src([
        './dist/css/vendor.styles.css',
        './dist/css/demo/dark-template.css'
    ], {read: false});
    return target.pipe(inject(sources, { relative: true }))
        .pipe(gulp.dest('./demo/'));
});

/* Inject Horizontal Demo CSS */
gulp.task('injectHorizontalDemoStyles', function () {
    var target = gulp.src('./demo/light-horizontal/pages/**/*.html');
    var sources = gulp.src([
        './dist/css/vendor.styles.css',
        './dist/css/demo/light-horizontal-template.css'
    ], {read: false});
    return target.pipe(inject(sources, { relative: true }))
        .pipe(gulp.dest('./demo/'));
});

/* Inject Floater Demo CSS */
gulp.task('injectFloaterDemoStyles', function () {
    var target = gulp.src('./demo/floater/pages/**/*.html');
    var sources = gulp.src([
        './dist/css/vendor.styles.css',
        './dist/css/demo/floater-template.css'
    ], {read: false});
    return target.pipe(inject(sources, { relative: true }))
        .pipe(gulp.dest('./demo/'));
});

/* Inject Light Demo CSS */
gulp.task('injectLightDemoStyles', function () {
    var target = gulp.src('./demo/light/pages/**/*.html');
    var sources = gulp.src([
        './dist/css/vendor.styles.css',
        './dist/css/demo/light-template.css'
    ], {read: false});
    return target.pipe(inject(sources, { relative: true }))
        .pipe(gulp.dest('./demo/'));
});

/* Inject Js & CSS assets into HTML */
gulp.task('injectVendorAssets', function () {

    var target = gulp.src('./demo/**/pages/**/*.html');
    var vendorSource = gulp.src([
        './dist/js/vendor.base.js',
        './dist/js/vendor.bundle.js'
    ] , {read: false});
    return target.pipe(inject(vendorSource, { relative: true }))
        .pipe(gulp.dest('./demo/'));

});

/* Inject HTML5 Partials in HTML */
gulp.task('injectPartialsFiles', function () {
    //return gulp.src('./demo/**/partials/*.html')
    return gulp.src('./demo/**/*.html')
        .pipe(injectPartials())
        .pipe(gulp.dest('./demo'));
});

/*sequence for injecting partials and replacing paths*/
//gulp.task('inject', gulp.series('injectLegacyDemoStyles', 'injectFloaterDemoStyles', 'injectHorizontalDemoStyles', 'injectDarkDemoStyles', 'injectLightDemoStyles', 'injectVendorAssets', 'injectPartialsFiles'));
gulp.task('inject', gulp.series('injectFloaterDemoStyles', 'injectFliteDemoStyles', 'injectLightDemoStyles', 'injectHorizontalDemoStyles', 'injectLegacyDemoStyles',  'injectDarkDemoStyles', 'injectVendorAssets', 'injectPartialsFiles'));

gulp.task('clean:vendors', function () {
    return del([
        './js/base/',
        './js/vendor/',
        './css/vendor/'
    ]);
});

/* Build Base Scripts*/
gulp.task('buildBaseScripts', function () {
    return gulp.src(['./node_modules/jquery/dist/jquery.min.js',
        './node_modules/popper.js/dist/umd/popper.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js'])
        .pipe(concat('vendor.base.js'))
        .pipe(gulp.dest('./dist/js/'));
});

/* Build Vendors CSS*/
gulp.task('buildVendorStyles', function () {
    return gulp.src([
        './node_modules/dropify/dist/css/dropify.min.css',
        './node_modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css',
        './node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        './node_modules/bootstrap-daterangepicker/daterangepicker.css',
        './node_modules/bootstrap-select/dist/css/bootstrap-select.min.css',
        './node_modules/smartwizard/dist/css/smart_wizard.min.css',
        './node_modules/smartwizard/dist/css/smart_wizard_theme_arrows.min.css',
        './node_modules/smartwizard/dist/css/smart_wizard_theme_circles.min.css',
        './node_modules/smartwizard/dist/css/smart_wizard_theme_dots.min.css',
        './node_modules/quill/dist/quill.snow.css',
        './node_modules/quill/dist/quill.bubble.css',
        './node_modules/summernote/dist/summernote-bs4.css',
        './node_modules/chartist/dist/chartist.min.css',
        './node_modules/morris.js/morris.css',
        './node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css',
        './node_modules/jsgrid/dist/jsgrid.css',
        './node_modules/jsgrid/dist/jsgrid-theme.css',
        './node_modules/perfect-scrollbar/css/perfect-scrollbar.css',
        './node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
        './node_modules/magnific-popup/dist/magnific-popup.css',
        './node_modules/sweetalert2/dist/sweetalert2.min.css',
        './node_modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
        './node_modules/jquery-steps/demo/css/jquery.steps.css',
        './node_modules/@fortawesome/fontawesome-free/css/all.css',
        './node_modules/flag-icon-css/css/flag-icon.min.css',
        './node_modules/jqvmap/dist/jqvmap.min.css',
        './node_modules/@fullcalendar/core/main.min.css',
        './node_modules/@fullcalendar/daygrid/main.min.css',
        './node_modules/@fullcalendar/list/main.min.css',
        './node_modules/@fullcalendar/timegrid/main.min.css',
        './node_modules/@fullcalendar/timeline/main.min.css',
        './node_modules/@fullcalendar/resource-timeline/main.min.css',
    ]).pipe(concat('vendor.styles.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./dist/css/'));
});

/* Build Vendors Scripts*/
gulp.task('buildVendorScripts', function () {
    return gulp.src([
        './node_modules/feather-icons/dist/feather.min.js',
        './node_modules/raphael/raphael.min.js',
        './node_modules/chart.js/dist/Chart.bundle.min.js',
        './node_modules/chartist/dist/chartist.min.js',
        './node_modules/apexcharts/dist/apexcharts.min.js',
        './node_modules/morris.js/morris.js',
        './node_modules/jquery-sparkline/jquery.sparkline.min.js',
        './node_modules/justgage/justgage.js',
        './node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
        './node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js',
        './node_modules/dropify/dist/js/dropify.min.js',
        './node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        './node_modules/bootstrap-daterangepicker/moment.min.js',
        './node_modules/bootstrap-daterangepicker/daterangepicker.js',
        './node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
        './node_modules/smartwizard/dist/js/jquery.smartWizard.min.js',
        './node_modules/quill/dist/quill.min.js',
        './node_modules/summernote/dist/summernote-bs4.min.js',
        './node_modules/datatables.net/js/jquery.dataTables.js',
        './node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js',
        './node_modules/jsgrid/dist/jsgrid.min.js',
        './node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js',
        './node_modules/owl.carousel/dist/owl.carousel.min.js',
        './node_modules/bootstrap-notify/bootstrap-notify.min.js',
        './node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
        './node_modules/sweetalert2/dist/sweetalert2.all.min.js',
        './node_modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        './node_modules/jquery-steps/build/jquery.steps.js',
        './node_modules/jqvmap/dist/jquery.vmap.min.js',
        './node_modules/jqvmap/dist/maps/jquery.vmap.world.js',
        './node_modules/jqvmap/dist/maps/jquery.vmap.europe.js',
        './node_modules/jqvmap/dist/maps/jquery.vmap.usa.js',
        './node_modules/jqvmap/dist/maps/continents/jquery.vmap.asia.js',
        './node_modules/jqvmap/dist/maps/continents/jquery.vmap.australia.js',
        './node_modules/jquery-mapael/js/jquery.mapael.min.js',
        './node_modules/jquery-mapael/js/maps/france_departments.js',
        './node_modules/jquery-mapael/js/maps/usa_states.js',
        './node_modules/jquery-mapael/js/maps/world_countries.js',
        './node_modules/jquery-mapael/js/maps/world_countries_mercator.js',
        './node_modules/jquery-mapael/js/maps/world_countries_miller.js',
        './node_modules/moment/min/moment.min.js',
        './node_modules/@fullcalendar/core/main.min.js',
        './node_modules/@fullcalendar/daygrid/main.min.js',
        './node_modules/@fullcalendar/interaction/main.min.js',
        './node_modules/@fullcalendar/list/main.min.js',
        './node_modules/@fullcalendar/timegrid/main.min.js',
        './node_modules/@fullcalendar/timeline/main.min.js',
        './node_modules/@fullcalendar/resource-common/main.min.js',
        './node_modules/@fullcalendar/resource-timeline/main.min.js',
        './node_modules/clipboard/dist/clipboard.min.js',
    ])
        .pipe(concat('vendor.bundle.js'))
        .pipe(gulp.dest('./dist/js/'));

});
gulp.task('dropifyFonts', function () {
    return gulp.src(['./node_modules/dropify/dist/fonts/*'])
        .pipe(gulp.dest('./dist/fonts/'));
});
gulp.task('summernoteFonts', function () {
    return gulp.src(['./node_modules/summernote/dist/font/*'])
        .pipe(gulp.dest('./dist/css/font/'));
});
gulp.task('FontAwesome', function () {
    return gulp.src(['./node_modules/@fortawesome/fontawesome-free/webfonts/*'])
        .pipe(gulp.dest('./dist/webfonts/'));
});
gulp.task('flagIconSvgs', function () {
    return gulp.src(['./node_modules/flag-icon-css/flags/**/*', './node_modules/flag-icon-css/flags/**/*'])
        .pipe(gulp.dest('./dist/flags/'));
});

gulp.task('buildVendorFonts', gulp.series('dropifyFonts', 'summernoteFonts', 'FontAwesome', 'flagIconSvgs'));

/* Build vendor's Assets */
gulp.task('buildVendors', gulp.series('clean:vendors','buildBaseScripts', 'buildVendorStyles','buildVendorScripts', 'buildVendorFonts'));

/* Whole Actions in one */
gulp.task('buildDemo', gulp.series('buildVendors','inject'));