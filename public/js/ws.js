if ( document.querySelector('meta[name="ws_host"]').content && document.querySelector('meta[name="ws_token"]').content)
{

    const centrifuge = new Centrifuge(document.querySelector('meta[name="ws_host"]').content, { debug: true });
    centrifuge.setToken(document.querySelector('meta[name="ws_token"]').content);

    centrifuge.on('error', function(err) {
      console.log(err);
    });

    centrifuge.on('connect', function(ctx) {
      console.info(ctx);
      if (document.getElementById('live-feed'))
      {
        document.getElementById('live-feed').innerHTML = "Websocket connected: " + document.querySelector('meta[name="ws_host"]').content;
      }
      if (document.getElementById('component-feed'))
      {
        document.getElementById('component-feed').innerHTML = "Websocket connected: " + document.querySelector('meta[name="ws_host"]').content;
      }
    });

    centrifuge.on('disconnect', function(ctx) {
      console.log(ctx);
      if (document.getElementById('live-feed'))
      {
        document.getElementById('live-feed').innerHTML = "Websocket disconnected.";
      }
      if (document.getElementById('component-feed'))
      {
        document.getElementById('component-feed').innerHTML = "Websocket disconnected.";
      }
    });

    centrifuge.subscribe(document.querySelector('meta[name="ws_channel"]').content, function(ctx) {
      console.info (ctx.data);

      if ( document.getElementById(ctx.data.component+'-attribute') )
      {
        document.getElementById(ctx.data.component+'-attribute').innerHTML = ctx.data.attribute;
        document.getElementById(ctx.data.component+'-value').innerHTML = ctx.data.value;
        document.getElementById(ctx.data.component+'-received').innerHTML = moment(ctx.data.received_at).format("MMM D h:mm:ss A");
        if ( document.getElementById(ctx.data.component+'-created') )
        {
          document.getElementById(ctx.data.component+'-created').innerHTML = moment(ctx.data.created_at).format("MMM D h:mm:ss A");
        }
      }
      if (document.getElementById('live-feed'))
      {
        if (ctx.data.group != 'routers')
        {
          document.getElementById('live-feed').innerHTML = '<span class="">[' + ctx.data.domain +"/"+ ctx.data.event+"]</span> " + ctx.data.component + ' <span class="">'+ctx.data.attribute + '</span>: <strong class="">' + ctx.data.value + "</strong> ("+ moment(ctx.data.created_at).fromNow() +")";
        }

      }
      if (document.getElementById('component-feed'))
      {
        if (document.getElementById('component-feed').getAttribute("data-component") == ctx.data.component )
        {
          if (ctx.data.group != 'routers')
          {
            document.getElementById('component-feed').innerHTML = '<span class="">[' + ctx.data.domain +"/"+ ctx.data.event+"]</span> " + ctx.data.component + ' <span class="">'+ctx.data.attribute + '</span>: <strong class="">' + ctx.data.value + "</strong> ("+ moment(ctx.data.created_at).fromNow() +")";
          }
        }
      }
    });

    try
    {
      centrifuge.connect();
    }
    catch (e)
    {
      console.log(e);
    }

}
