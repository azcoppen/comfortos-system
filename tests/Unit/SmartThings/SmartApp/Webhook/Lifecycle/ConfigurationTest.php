<?php

namespace Tests\Unit\SmartThings\SmartApp\Webhook\Lifecycle;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Str;
use Tests\TestCase;

class ConfigurationTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testConfiguration()
    {
        $json_to_decode_initialize = '
          {
            "lifecycle": "CONFIGURATION",
            "executionId": "85f0047b-bb24-8eeb-da11-cb6e2f767322",
            "locale": "en",
            "version": "0.1.0",
            "configurationData": {
              "installedAppId": "8a0dcdc9-1ab4-4c60-9de7-cb78f59a1121",
              "phase": "INITIALIZE",
              "pageId": "",
              "previousPageId": "",
              "config": {

              }
            },
            "settings": {

            }
          }
        ';

        $response = $this->json('POST', '/webhooks/unit-test', (array) json_decode($json_to_decode_initialize));

        $response->assertStatus(200)
        ->assertExactJson([
            'configurationData' => [
              'initialize' => config('smartrooms-demo.v1.configuration.initialize'),
            ],
        ]);

        $json_to_decode_page1 = '
          {
            "lifecycle": "CONFIGURATION",
            "executionId": "85f0047b-bb24-8eeb-da11-cb6e2f767322",
            "locale": "en",
            "version": "0.1.0",
            "configurationData": {
              "installedAppId": "8a0dcdc9-1ab4-4c60-9de7-cb78f59a1121",
              "phase": "PAGE",
              "pageId": "1",
              "previousPageId": "",
              "config": {
                "app": [
                  {

                  }
                ]
              }
            },
            "settings": {

            }
          }
        ';

        $response = $this->json('POST', '/webhooks/unit-test', (array) json_decode($json_to_decode_page1));

        $response->assertStatus(200)
        ->assertExactJson([
            'configurationData' => [
              'page' => config('smartrooms-demo.v1.configuration.page'),
            ],
        ]);
    }
}
