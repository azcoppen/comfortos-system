<?php

namespace Tests\Unit\SmartThings\SmartApp\Webhook\Lifecycle;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Str;
use Tests\TestCase;

class PingTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testPing()
    {
        $challenge = Str::uuid()->toString();

        $response = $this->json('POST', '/webhooks/unit-test', [
          'lifecycle'     => 'PING',
          'executionId'   => Str::uuid()->toString(),
          'locale'        => 'en',
          'version'       => '1.0.0',
          'pingData'      => ['challenge' => $challenge],
        ]);

        $response->assertStatus(200)
        ->assertExactJson([
            'pingData' => ['challenge' => $challenge],
        ]);
    }
}
