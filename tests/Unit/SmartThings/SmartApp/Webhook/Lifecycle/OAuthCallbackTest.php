<?php

namespace Tests\Unit\SmartThings\SmartApp\Webhook\Lifecycle;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Str;
use Tests\TestCase;

class OAuthCallbackTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testOAuthCallback()
    {
        $response = $this->json('POST', '/webhooks/unit-test', [
          'lifecycle'     => 'OAUTH_CALLBACK',
          'executionId'   => Str::uuid()->toString(),
          'locale'        => 'en',
          'version'       => '1.0.0',
          'oAuthCallbackData'      => [
            'installedAppId' => 'string',
            'urlPath' => 'string',
          ],
        ]);

        $response->assertStatus(200)
        ->assertExactJson([
            'oAuthCallbackData' => new \stdClass,
        ]);
    }
}
