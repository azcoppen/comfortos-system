<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Components\OS\WSBrokerRepository;

class WSBrokerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = app (UserRepository::class)->first();

        $vpn = app (WSBrokerRepository::class)->create ([
          'type'          => 'centrifugo',
          'v'             => '2.8.0',
          'region'        => 'ca-east',
          'label'         => 'OVH Centrifugo (local)',
          'context'       => 'local',
          'host'          => 'localhost',
          'ws_port'       => 8000,
          'admin_pass'    => \Crypt::encrypt('cstr8media'),
          'admin_secret'  => \Crypt::encrypt('cstr8media'),
          'hmac_secret'   => \Crypt::encrypt('59b4df6a-53f8-4ccc-bdbd-08cb7107dafe'),
          'api_key'       => \Crypt::encrypt ('4546b992-f9a8-4270-917f-efa7edebea07'),
          'admin_port'    => 8000,
          'conn_endpoint' => 'connection/websocket',
          'api_endpoint'  => 'api',
          'auth_type'     => 'jwt',
          'vpn_ip'        => null,
          'ordered_at'    => now()->subYears (1),
          'ordered_by'    => $user->_id,
          'order_id'      => 'EMPTY',
          'installed_at'  => now(),
          'installed_by'  => $user->_id,
          'provisioned_at'=> now(),
          'provisioned_by'=> $user->_id,
          'created_by'    => $user->_id,
        ]);

        $vpn = app (WSBrokerRepository::class)->create ([
          'type'          => 'centrifugo',
          'v'             => '2.8.0',
          'region'        => 'ca-east',
          'label'         => 'OVH Centrifugo (remote)',
          'context'       => 'remote',
          'host'          => 'realtime.smartrooms.dev',
          'ws_port'       => 443,
          'admin_pass'    => \Crypt::encrypt('cstr8media'),
          'admin_secret'  => \Crypt::encrypt('cstr8media'),
          'hmac_secret'   => \Crypt::encrypt('59b4df6a-53f8-4ccc-bdbd-08cb7107dafe'),
          'api_key'       => \Crypt::encrypt ('4546b992-f9a8-4270-917f-efa7edebea07'),
          'admin_port'    => 443,
          'conn_endpoint' => 'connection/websocket',
          'api_endpoint'  => 'api',
          'auth_type'     => 'jwt',
          'vpn_ip'        => null,
          'ordered_at'    => now()->subYears (1),
          'ordered_by'    => $user->_id,
          'order_id'      => 'EMPTY',
          'installed_at'  => now(),
          'installed_by'  => $user->_id,
          'provisioned_at'=> now(),
          'provisioned_by'=> $user->_id,
          'created_by'    => $user->_id,
        ]);
    }
}
