<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use SmartRooms\Contracts\Repositories\Components\Devices\TVAppRepository;

class AndroidTVAppSeeder extends Seeder
{
    private $data = [
        'vod' => [
            'bbc.iplayer.android' => 'BBC iPlayer',
            'com.britbox.us' => 'Britbox',
            'com.gotv.crackle.handset' => 'Crackle',
            'com.netflix.mediaclient' => 'Netflix',
            'com.hulu.plus' => 'Hulu',
            'com.peacocktv.peacockandroid' => 'Peacock TV',
            'com.amazon.avod.thirdpartyclient' => 'Prime Video',
            'com.rollingstorm.iamflix' => 'Pureflix',
            'com.tubitv' => 'Tubi',
            'air.com.vudu.air.DownloaderTablet' => 'Vudu',
        ],
        'live' => [
            'com.att.tv' => 'AT&T',
            'com.directv.dvrscheduler' => 'DirecTV',
            'com.filmon.android.app.yetanotherlivetv' => 'FilmOn',
            'com.verizon.fios.tv' => 'Fios',
            'tv.fubo.mobile' => 'Fubo TV',
            'com.philo.philo.google' => 'Philo',
            'tv.pluto.android' => 'Pluto',
            'com.sling' => 'Sling TV',
            'com.xfinity.cloudtvr' => 'Xfinity',
            'com.xumo.xumo' => 'Xumo',
            'com.google.android.youtube.tvunplugged' => 'YouTube TV',
        ],
        'music' => [
            'com.amazon.mp3' => 'Amazon Music',
            'com.apple.android.music' => 'Apple Music',
            'com.google.android.apps.youtube.music' => 'YouTube Music',
            'com.pandora.android' => 'Pandora',
            'com.spotify.music' => 'Spotify',
        ],
        'brands' => [
            'com.disney.datg.videoplatforms.android.abc' => 'ABC',
            'com.aetn.aetv.watch' => 'A&E',
            'com.channel4.ondemand' => 'All 4',
            'com.amctve.amcfullepisodes' => 'AMC',
            'com.discovery.aplgo' => 'Animal Planet',
            'com.bbca.androidtv' => 'BBC America',
            'com.bet.shows' => 'BET Now',
            'com.bloomberg.btva' => 'Bloomberg',
            'com.nbcu.tve.bravo' => 'Bravo',
            'com.turner.cnvideoapp' => 'Cartoon Network',
            'com.cbs.app' => 'CBS',
            'com.cnbc.client' => 'CNBC',
            'com.cnn.mobile.android.tv' => 'CNN Go',
            'com.vmn.android.comedycentral' => 'Comedy Central',
            'com.cw.fullepisodes.android' => 'CW',
            'com.discovery.discoverygo' => 'Discovery Go',
            'com.disney.disneyplus' => 'Disney Plus',
            'com.nbcu.tve.e' => 'E! Entertainment',
            'com.epix.epix.now' => 'Epix',
            'com.foodnetwork.watcher' => 'Food Network Go',
            'com.fox.now' => 'Fox Now',
            'com.disney.datg.videoplatforms.android.abc' => 'Freeform',
            'com.foxnews.android' => 'Fox News',
            'com.fxnetworks.fxnow' => 'FX Now',
            'com.hallmarkchannel.awe' => 'Hallmark',
            'com.hgtv.watcher' => 'HGTV Go',
            'com.aetn.history.watch' => 'History',
            'com.hbo.hbonow' => 'HBO Max',
            'com.hbo.go' => 'HBO Go',
            'air.ITVMobilePlayer' => 'ITV Hub',
            'com.aetn.lifetime.watch.androidtv' => 'Lifetime',
            'com.mtvn.mtvPrimeAndroid' => 'MTV',
            'com.music.choice' => 'Music Choice',
            'com.mobileiq.demand5' => 'My 5',
            'com.natgeo.tv' => 'Nat Geo TV',
            'com.nbcuni.nbc' => 'NBC',
            'com.zumobi.msnbc' => 'NBC News',
            'com.nickonline.android.nickapp' => 'Nick',
            'com.nbcu.tve.oxygen' => 'Oxygen',
            'com.vmn.android.spike' => 'Paramount',
            'com.discovery.scigo' => 'Sci Go',
            'com.showtime.standalone' => 'Showtime',
            'com.bskyb.skygo' => 'Sky Go',
            'com.bydeluxe.d3.android.program.starz' => 'Starz',
            'com.nbcu.tve.syfy' => 'SYFY',
            'tbn_mobile.android' => 'TBN',
            'com.tcm.tcm' => 'TCM',
            'com.discovery.tlcgo' => 'TLC Go',
            'com.rhythmnewmedia.tmz' => 'TMZ',
            'com.turner.tnt.android.networkapp' => 'TNT',
            'com.travelchannel.watcher' => 'Travel Channel Go',
            'com.turner.trutv' => 'TruTV',
            'uk.co.uktv.dave' => 'UKTV Play',
            'com.usanetwork.watcher' => 'USA',
            'com.mtvn.vh1android' => 'VH1',
            'com.weather.Weather' => 'Weather Channel',
        ],
        'news' => [
            'bbc.mobile.news.ww' => 'BBC News',
            'com.dailymail.online' => 'Daily Mail',
            'com.economist.darwin' => 'Economist',
            'com.ft.news' => 'FT',
            'com.apptivateme.next.la' => 'LA Times',
            'com.nytimes.android' => 'New York Times',
            'org.npr.android.news' => 'NPR',
            'me.oann.news' => 'OANN',
            'com.thomsonreuters.reuters' => 'Reuters',
            'uk.co.thetimes' => 'The Times',
            'com.usatoday.android.news' => 'USA Today',
            'uk.co.telegraph.android' => 'The Telegraph',
            'com.washingtonpost.android' => 'Washington Post',
            'wsj.reader_sp' => 'WSJ',
        ],
        'sports' => [
            'com.espn.score_center' => 'ESPN',
            'com.foxsports.videogo' => 'Fox Sports Go',
            'com.nbadigital.gametimelite' => 'NBA',
            'air.com.nbcuni.com.nbcsports.liveextra' => 'NBC Sports',
            'com.gotv.nflgamecenter.us.lite' => 'NFL',
            'com.nhl.gc1112.free' => 'NHL',
            'com.wwe.universe' => 'WWE',
        ],
        'utility' => [
            'com.brave.browser' => 'Brave',
            'com.calm.android' => 'Calm',
            'com.google.android.apps.mapslite' => 'Google Maps',
            'com.google.android.street' => 'Google Street View',
            'org.zwanoo.android.speedtest' => 'Speedtest',
            'com.waze' => 'Waze',
        ],
        'travel' => [
            'com.alaskaairlines.android' => 'Alaska Airlines',
            'com.aa.android' => 'American Airlines',
            'com.amtrak.rider' => 'Amtrak',
            'com.ba.mobile' => 'British Airways',
            'com.delta.mobile.android' => 'Fly Delta',
            'com.flyfrontier.android' => 'Frontier Airlines',
            'com.greyhound.mobile.consumer' => 'Greyhound',
            'com.jetblue.JetBlueAndroid' => 'jetBlue',
            'com.southwestairlines.mobile' => 'Southwest Airlines',
            'com.spirit.customerapp' => 'Spirit Airlines',
            'com.united.mobile.android' => 'United Airlines',
            'com.vaa.runway' => 'Virgin Atlantic',
        ],

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $group => $items) {
            foreach ($items as $key => $title) {
                app(TVAppRepository::class)->create([
                    'label' => $title,
                    'install' => $key,
                    'slug' => Str::slug($title),
                    'tags' => [$group],
                ]);
            }
        }
    }
}
