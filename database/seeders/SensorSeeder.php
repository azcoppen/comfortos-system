<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class SensorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $room = app (RoomRepository::class)->first();
      $user = app (UserRepository::class)->first();

      $room->sensors()->create ([
        'type'    => 'aeotec-multi-sensor-6',
        'variant' => 'multi',
        'driver'  => 'openhab',
        'label'   => 'Multi-Sensor 01',
        'items' => [
        ],
        'ordered_at'    => now()->subYears (1),
        'ordered_by'    => $user->_id,
        'order_id'      => 'EMPTY',
        'installed_at'  => now(),
        'installed_by'  => $user->_id,
        'provisioned_at'=> now(),
        'provisioned_by'=> $user->_id,
        'created_by'    => $user->_id,
        'last_signal_at'=> NULL,
        'last_command_at'=> NULL,
      ]);

      $room->sensors()->create ([
        'type'    => 'aeotec-contact',
        'variant' => 'contact',
        'driver'  => 'openhab',
        'label'   => 'Contact Sensor 01',
        'items' => [
        ],
        'ordered_at'    => now()->subYears (1),
        'ordered_by'    => $user->_id,
        'order_id'      => 'EMPTY',
        'installed_at'  => now(),
        'installed_by'  => $user->_id,
        'provisioned_at'=> now(),
        'provisioned_by'=> $user->_id,
        'created_by'    => $user->_id,
        'last_signal_at'=> NULL,
        'last_command_at'=> NULL,
      ]);

      $room->sensors()->create ([
        'type'    => 'first-alert-zcombo',
        'variant' => 'smoke',
        'driver'  => 'openhab',
        'label'   => 'Smoke/CO Detector 01',
        'items' => [
        ],
        'ordered_at'    => now()->subYears (1),
        'ordered_by'    => $user->_id,
        'order_id'      => 'EMPTY',
        'installed_at'  => now(),
        'installed_by'  => $user->_id,
        'provisioned_at'=> now(),
        'provisioned_by'=> $user->_id,
        'created_by'    => $user->_id,
        'last_signal_at'=> NULL,
        'last_command_at'=> NULL,
      ]);

    }
}
