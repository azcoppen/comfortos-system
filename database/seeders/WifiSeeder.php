<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNRepository;

class WifiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vpn = app (VPNRepository::class)->first();

        $rooms = app(RoomRepository::class)->all();

        $user = app (UserRepository::class)->first();

        $end_ip = 20;

        foreach ($rooms AS $index => $room)
        {
          $router = $room->routers()->create([
              'type'        => 'router-os-6',
              'label'       => 'MikroTik hEX lite Router',
              'hostname'    => 'SmartRooms',
              'model'       => 'RB931-2nD',
              'serial'      => 'AD280A84FAE0',
              'v'           => '6.46.4',
              'int_ip'      => '192.168.86.1',
              'vpn_ip'      => '100.96.1.'.$end_ip,
              'mac'         => '74:4D:28:BF:3B:4D',
              'web_user'    => 'admin',
              'web_pwd'     => Crypt::encrypt('cstr8media'),
              'api_user'    => 'admin',
              'api_pwd'     => Crypt::encrypt('cstr8media'),
              'ordered_at'    => now()->subYears (1),
              'ordered_by'    => $user->_id,
              'order_id'      => 'EMPTY',
              'installed_at'  => now(),
              'installed_by'  => $user->_id,
              'provisioned_at'=> now(),
              'provisioned_by'=> $user->_id,
              'created_by'    => $user->_id,
          ]);

          $router->vpns()->attach ($vpn);

          $router->createNote('Router created by system seeding '.now()->format('c'));

          $default_network = $router->networks()->create([
              'type' => 'hardware',
              'label'       => 'Default SmartRooms Wifi Network',
              'interface' => 'wlan1',
              'profile_index' => '*A',
              'network_index' => '*B',
              'security_profile' => 'room-'.$room->number,
              'ssid' => 'Room-'.$room->number,
              'password' => Crypt::encrypt('smwifi'.$room->number),
              'launch_at' => now(),
              'expire_at' => now()->addYears(20),
              'provisioned_at'=> now(),
              'provisioned_by'=> $user->_id,
              'created_by'    => $user->_id,
              'room_id' => $room->_id,
          ]);

          $default_network->createNote('Wifi network created by system seeding '.now()->format('c'));


          $end_ip++;
        }// end foreach


    }
}
