<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Maklad\Permission\Models\Role;
use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

class UserSeeder extends Seeder
{
    public $data;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->data = [
            [
                'prefix'    => 'Mr',
                'first'     => 'Alex',
                'last'      => 'Cameron',
                'suffix'    => '',
                'display'   => 'Alex',
                'slug'      => 'alex-cameron',
                'email'     => 'ac@smartrooms.dev',
                'password'  => Hash::make('12345'),
                'dob'       => '1978-12-17',

                'sex'       => 'M',
                'job_title' => 'El Presidente',
                'location'=> [
                  'street' => 'Suite 310, 1897 Preston White Drive',
                  'city'   => 'Reston',
                  'region' => 'VA',
                  'postal' => '20191',
                  'country' => 'US',
                ],
                'geo' => [
                  'type' => 'Point',
                  'coordinates' => [
                    -77.326770,
                    38.942720,
                  ],
                ],
                'telephones' => [
                  '+1708962276',
                ],
                'lang'      => 'en',
                'locale'    => 'en_US',
                'timezone'  => 'America/Los_Angeles',
                'currency'  => 'USD',
                'otp_secret' => Crypt::encrypt(Str::random(24)),
                'created_by' => null,
                'tags' => ['developer'],
                'last_login_at' => now(),
                'last_activity_at' => now(),
                'email_verified_at'     => now(),
                'pwd_changed_at' => now(),
                'pwd_expires_at' => now()->addYears(1),
            ],
        ];

        foreach ($this->data as $datum) {
            $user = app(UserRepository::class)->create($datum);
            $user->assignRole('developer');
            $user->createNote('User created by system seeding '.now()->format('c'));
        }
    }
}
