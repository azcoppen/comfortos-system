<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;

use SmartRooms\Contracts\Repositories\Components\OS\VPNClientRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNServerRepository;

class VPNSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = app (UserRepository::class)->first();

        $vpn_server = app (VPNServerRepository::class)->create ([
          'type'      => 'openvpn',
          'region'    => 'us-west',
          'timezone'  => 'America/Los_Angeles',
          'label'     => 'OpenVPN Cloud Test Account',
          'host'      => 'us-west-2.gw.openvpn.cloud',
          'ports'     => [443, 1194],
          'ip_range'  => '100.96.1.32/28',
          'subnet'    => '10.0.0.0/24',
          'netmask'   => '255.255.255.240',
          'cipher'    => 'AES-256-CBC',
          'ordered_at'    => now()->subYears (1),
          'ordered_by'    => $user->_id,
          'order_id'      => 'EMPTY',
          'installed_at'  => now(),
          'installed_by'  => $user->_id,
          'provisioned_at'=> now(),
          'provisioned_by'=> $user->_id,
          'created_by'    => $user->_id,
        ]);

        $vpn_server->vpn_clients()->create ([
          'type'          => 'openvpn',
          'label'         => 'Example VPN client',
          'client_id'     => 'example',
          'certificate'   => \Crypt::encrypt("
          -----BEGIN CERTIFICATE-----
          MIIDRDCCAiygAwIBAgIJAPRJm0V+RAQkMA0GCSqGSIb3DQEBCwUAMBsxGTAXBgNV
          BAMMEENsb3VkVlBOIFByb2QgQ0EwHhcNMTkxMDE2MTgwNDQ3WhcNNDkxMDA4MTgw
          NDQ3WjAbMRkwFwYDVQQDDBBDbG91ZFZQTiBQcm9kIENBMIIBIjANBgkqhkiG9w0B
          AQEFAAOCAQ8AMIIBCgKCAQEAvbodWcvyngHYGLVvLHTy9hLFIqrkxWQqi7gnC4RO
          Nz0VLzr9WckNN+kE7IrI9qbWL+F/4g5FKNOAayZf1Xc6YU2kV2JkXiScfGexsbFN
          rG+CFnphEdk2gJmDEFNxtDmxFVaTv7QCgtcijYEGMADW07sFfvDX0fjeKCTatSe6
          rVrQ0jCKsKoOtTiE2IvIlg41cvM7N7Pj/WVFs3Rwjbctd/i+bJ9A4j+G5gCR13KE
          Y48fCmkuvptaXZ8JyKoisoX46XTqDEQqBSG3exT6txlgsdqd+obp+CtuhPQQAVH4
          f04bM/wNDbFDWVPl0AbdKj9EmdBPIroLWi4VYtJeu4vjMQIDAQABo4GKMIGHMB0G
          A1UdDgQWBBTZQ8jn1xwiL3mg+FhSrS5WLh7bKDBLBgNVHSMERDBCgBTZQ8jn1xwi
          L3mg+FhSrS5WLh7bKKEfpB0wGzEZMBcGA1UEAwwQQ2xvdWRWUE4gUHJvZCBDQYIJ
          APRJm0V+RAQkMAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEB
          CwUAA4IBAQBCk7zsTsiC0qEtwAfN1yo6VgbTBJf/rBDtoIjofVNDtpjaDZBA3/PT
          v+aJJEHSYaP7xNX91cP32Un09aG9BDT4OuLhYO3mOx+ghmFCCt5NhTvbT4gqmWfi
          MNaB8Z89gSaeuduD46btNjk+SeIWKBDrZlq0tJ9zexi6OEkre/XA7lssDTtE4GaC
          5xKtug1TxrzRziWv2OA14RLGq8n0Hhk40xdhrvE+bhWAUsd2fbfNxTzwhsfQrBH7
          K4FWB1sRavCTokSSJ7OX4R3w8QIn7Ziyqzu9JLJ5sy0EhVQxkQpuoW3BwmDVdXLS
          d4rhWO4aNE5qqE6lq5AOJ1riv7BrZ4jj
          -----END CERTIFICATE-----
          "),
          'cipher'        => 'AES-256-CBC',
          'revoked_at'    => null,
          'ordered_at'    => now()->subYears (1),
          'ordered_by'    => $user->_id,
          'order_id'      => 'EMPTY',
          'installed_at'  => now(),
          'installed_by'  => $user->_id,
          'provisioned_at'=> now(),
          'provisioned_by'=> $user->_id,
          'created_by'    => $user->_id,
        ]);
    }
}
