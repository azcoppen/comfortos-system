<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class BulbSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $rooms = app (RoomRepository::class)->all();
      $user = app (UserRepository::class)->first();

      foreach ($rooms AS $index => $room)
      {
        if ( $index < 2 )
        {
          for ( $i = 1; $i < 3; $i++ )
          {
            $room->bulbs()->create ([
              'type'    => 'hue_extended_color',
              'driver'  => 'openhab',
              'label'   => 'Color Bulb 0'.$i,
              'items' => [
                'power'       => 'bulb_0'.$i.'_color',
                'brightness'  => 'bulb_0'.$i.'_brightness',
                'color'       => 'bulb_0'.$i.'_color',
                'temperature' => 'bulb_0'.$i.'_color_temp',
              ],
              'ordered_at'    => now()->subYears (1),
              'ordered_by'    => $user->_id,
              'order_id'      => 'EMPTY',
              'installed_at'  => now(),
              'installed_by'  => $user->_id,
              'provisioned_at'=> now(),
              'provisioned_by'=> $user->_id,
              'created_by'    => $user->_id,
              'last_signal_at'=> NULL,
              'last_command_at'=> NULL,
            ]);
          }

        }
      }

    }
}
