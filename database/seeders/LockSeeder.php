<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class LockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $room = app (RoomRepository::class)->first();
      $user = app (UserRepository::class)->first();

      $lock = $room->locks()->create ([
        'type'    => 'yale-YRD110-ZW-605',
        'driver'  => 'openhab',
        'label'   => 'Yale Keypad Deadbolt 01',
        'items' => [
          'insert'       => 'lock_01_config',
          'close'        => 'lock_01_close',
          'open'         => 'lock_01_open',
          'wipe'         => 'lock_01_config',
        ],
        'ordered_at'    => now()->subYears (1),
        'ordered_by'    => $user->_id,
        'order_id'      => 'EMPTY',
        'installed_at'  => now(),
        'installed_by'  => $user->_id,
        'provisioned_at'=> now(),
        'provisioned_by'=> $user->_id,
        'created_by'    => $user->_id,
        'last_signal_at'=> NULL,
        'last_command_at'=> NULL,
      ]);

      $lock->codes()->create ([
        'index'         => 10,
        'code'          => hash('sha256', '12345'), // we don't use encrypt here because we need to search
        'recovery'      => \Crypt::encrypt('12345'),
        'hint'          => '12***',
        'label'         => 'Simple example code for lock',
        'expire_at'     => now()->addYears(5),
        'used'          => 0,
        'created_by'    => $user->_id,
      ]);
    }
}
