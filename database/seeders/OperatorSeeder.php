<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\IDAM\AccessRepository;
use SmartRooms\Contracts\Repositories\Operator\OperatorRepository;

use SmartRooms\Contracts\Repositories\Components\OS\MQTTBrokerRepository;
use SmartRooms\Contracts\Repositories\Components\OS\WSBrokerRepository;

class OperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = app (UserRepository::class)->first();

        $operator = app(OperatorRepository::class)->create([
          'label'   => 'Example Hotels Group',
          'slug'    => 'example-hotels-group',
          'location'=> [
            'street' => '223 South Hamilton Drive',
            'city'   => 'Beverly Hills',
            'region' => 'CA',
            'postal' => '90211',
            'country' => 'US',
          ],
          'geo' => [
            'type' => 'Point',
            'coordinates' => [
              -118.375397,
              34.063251,
            ],
          ],
          'telephones' => [
            '+13234715284',
          ],
          'emails' => [
            'example-group@smartrooms.dev',
          ],
          'lang'      => 'en',
          'locale'    => 'en_US',
          'timezone'  => 'America/Los_Angeles',
          'currency'  => 'USD',
          'image'     => '',
          'created_by' => $user->_id,
        ]);

        $operator->createNote('Operator created by system seeding '.now()->format('c'));

        $brand = $operator->brands()->create([
          'label' => 'Example Hotels',
          'slug'  => 'example-hotels',
          'image' => '',
          'tags' => ['example-hotels-group'],
          'created_by' =>  $user->_id,
        ]);

        $brand->createNote('Brand created by system seeding '.now()->format('c'));

        $property = $brand->properties()->create([
          'label'   => '223 South Hamilton',
          'slug'    => '223-south-hamilton',
          'location'=> [
            'street' => '223 South Hamilton Drive',
            'city'   => 'Beverly Hills',
            'region' => 'CA',
            'postal' => '90211',
            'country' => 'US',
          ],
          'geo' => [
            'type' => 'Point',
            'coordinates' => [
              -118.375397,
              34.063251,
            ],
          ],
          'telephones' => [
            '+13234715284',
          ],
          'emails' => [
            '223-south-hamilton@smartrooms.dev',
          ],
          'lang'      => 'en',
          'locale'    => 'en_US',
          'timezone'  => 'America/Los_Angeles',
          'currency'  => 'USD',
          'image'     => '',
          'tags' => ['example-hotels-group', 'example-hotels'],
          'created_by' => $user->_id,
        ]);

        $property->createNote('Property created by system seeding '.now()->format('c'));

        $property->ws_brokers()->attach (app(WSBrokerRepository::class)->first());
        $property->mqtt_brokers()->attach (app(MQTTBrokerRepository::class)->first());

        $building = $property->buildings()->create([
          'label'       => 'Main Building',
          'slug'        => 'main-building',
          'identifier'  => 'main',
          'geo' => [
            'type' => 'Point',
            'coordinates' => [
              -118.375397,
              34.063251,
            ],
          ],
          'tags' => ['example-hotels-group', 'example-hotels', '223-south-hamilton'],
          'created_by' => $user->_id,
        ]);

        $building->createNote('Building created by system seeding '.now()->format('c'));

        for ($i = 101; $i <= 104; $i++) {
            $room = $building->rooms()->create([
            'floor'       => 1,
            'number'      => $i,
            'label'       => 'Room '.$i.', '.$building->label.', '.$property->label,
            'type'        => 'double',
            'grade'       => 'queen',
            'description' => 'Guest room, 1 Double, Courtyard view',
            'footage'     => 375,
            'beds'        => 1,
            'occupancy'   => 2,
            'smoking'     => 0,
            'AC'          => 1,
            'eth'         => 0,
            'ext'         => 1,
            'public'      => 1,
            'geo' => [
              'type' => 'Point',
              'coordinates' => [
                -118.375397,
                34.063251,
              ],
            ],
            'image' => '',
            'tags'  => ['example-hotels-group', 'example-hotels', '223-south-hamilton', 'main-building'],
            'retention' => 7,
            'created_by' => $user->_id,
          ]);

            $room->createNote('Room created by system seeding '.now()->format('c'));

            $room_user = app(UserRepository::class)->create([
              'prefix'    => '',
              'first'     => 'Guest',
              'last'      => 'User',
              'suffix'    => '',
              'display'   => 'Room '.$i.' Guest User',
              'slug'      => $property->slug.'-room-'.$i.'-guest-user',
              'email'     => $property->slug.'-room-'.$i.'-guest-user@rooms.smartrooms.dev',
              'password'  => Hash::make($i),
              'dob'       => null,

              'sex'       => 'X',
              'job_title' => 'Anonymous',
              'location'  => $property->location,
              'geo'       => $property->geo,
              'telephones' => $property->telephones,
              'lang'      => $property->lang,
              'locale'    => $property->locale,
              'timezone'  => $property->timezone,
              'currency'  => $property->currency,
              'otp_secret' => Crypt::encrypt(Str::random(24)),
              'created_by' => null,
              'tags' => ['anon', 'guest', 'example-hotels-group', 'example-hotels', '223-south-hamilton', 'main-building'],
              'last_login_at' => now(),
              'last_activity_at' => now(),
              'email_verified_at'     => now(),
              'pwd_changed_at' => now(),
              'pwd_expires_at' => now()->addYears(1),
              'created_by'     => $user->_id,
          ]);

            $room_user->assignRole(['anon', 'guest']);
            $room_user->createNote('Room user created by system seeding '.now()->format('c'));

            $room->users()->attach($room_user);

            // Create room access
            $access = app(AccessRepository::class)->create([
              'grantee_type'      => get_class($room_user),
              'grantee_id'        => $room_user->_id,
              'accessible_type'   => get_class($room),
              'accessible_id'     => $room->_id,
              'type'              => 'full',
              'start_at'          => now(),
              'end_at'            => now()->addYears(10),
              'created_by'        => $user->_id,
          ]);

            $room_user->createNote('Room user access assigned 10 year access to room '.$room->_id.': '.now()->format('c'));
        }


        /* INDEXING */


        /*

        Schema::create('bars', function ($collection) {
            $collection->geospatial('location', '2dsphere');
        });

        db.operators.createIndex( { location : "2dsphere" } )
        db.brands.createIndex( { location : "2dsphere" } )
        db.properties.createIndex( { location : "2dsphere" } )
        db.buildings.createIndex( { location : "2dsphere" } )
        db.rooms.createIndex( { location : "2dsphere" } )
        */



    }
}
