<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\ComfortOS\ChecklistRepository;

class RouterChecklistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
          ["Router 110v power cable replaced with USB-C charger cable?" , "In order to be portable, the router should be plug into any charger, not just the wall.", 3,],
          ['Hostname set to "SmartRooms-router_F01_R01" (floor 01, room 01) in System > Identity?' , "The default router name should not be MikroTik or just SmartRooms. The upstream DHCP server must be able to identify 1000 different routers.", 3,],
          ["Default user admin changed to an unguessable username?" , "A stranger must not be able to gain access with the default username (admin).", 2,],
          ["Default user password changed to an unguessable string?" , "The main admin interface has no password set.", 1,],
          ["Default API user created with unguessable username and password?" , "Needed for registering the device in COS.", 1,],
          ["Router plugged into an Ethernet socket or Powerline adaptor?" , "If no Ethernet is available, try Powerline or GPRS.", 2,],
          ["Router successfully has an IP address from its upstream DHCP server?" , "Not all DHCP servers act nicely.", 3,],
          ["Internal gateway IP address noted down?" , "Needed for registering the device in COS.", 3,],
          ["Ethernet MAC address noted down?", "This can be used to block signals from the router if necessary.", 3],
          ["System clock is successfully connected to the correct time for the client property?" , "The timezone may not be the same as where the router is being set up.", 3,],
          ["License noted down from System > License?" , "Needed for registering the device in COS.", 3,],
          ["Details noted down from System > Resources?" , "Needed for registering the device in COS.", 3,],
          ["Details noted down from System > Routerboard?" , "Needed for registering the device in COS.", 3,],
          ["UPnP enabled in IP > UPnP?" , "This is needed for media devices attached to the router.", 3,],
        ];

        foreach ($items AS $index => $data)
        {
            app (ChecklistRepository::class)->create ([
              'type' => 'router',
              'index' => $index +1,
              'item' => $data[0],
              'help' => $data[1],
              'priority' => $data[2],
            ]);
        }
    }
}
