<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Components\Mirror\ModuleRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNRepository;

class MirrorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vpn = app (VPNRepository::class)->first();

        $room = app(RoomRepository::class)->first();

        $user = app (UserRepository::class)->first();

        $mirror = $room->mirrors()->create([
          'os'         => 'raspbian',
          'release'    => 'Raspbian GNU/Linux 10 (buster)',
          'codename'   => 'Buster',
          'hardware'   => 'BCM2711',
          'kernel'     => 'Linux 5.4.72-v7l+',
          'label'      => 'Raspbian MagicMirror-2 server',
          'host'       => 'mirror_01',
          'serial'     => '100000005c935dbf',
          'model'      => 'Raspberry Pi 4 Model B Rev 1.2',
          'engine'     => 'MagicMirror-2',
          'dist'       => '2.12.0',
          'int_ip'     => '192.168.86.55',
          'vpn_ip'     => '100.96.1.24',
          'mac'        => 'dc:a6:32:96:f7:26',
          'router_id'  => $room->routers()->first()->_id,
          'ordered_at'    => now()->subYears (1),
          'ordered_by'    => $user->_id,
          'order_id'      => 'EMPTY',
          'installed_at'  => now(),
          'installed_by'  => $user->_id,
          'provisioned_at'=> now(),
          'provisioned_by'=> $user->_id,
          'created_by'    => $user->_id,
        ]);

        $mirror->vpns()->attach ($vpn);
        $mirror->modules()->attach (app(ModuleRepository::class)->all()->pluck('_id')->all());

        $mirror->createNote('Mirror created by system seeding '.now()->format('c'));

    }
}
