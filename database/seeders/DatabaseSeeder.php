<?php

namespace Database\Seeders;

use Database\Seeders;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      // Reference data
      $this->call ([
        Seeders\MirrorModuleSeeder::class,
        Seeders\AndroidTVAppSeeder::class,
      ]);

      // Add VPN
      $this->call ([
        Seeders\VPNSeeder::class,
        Seeders\MQTTBrokerSeeder::class,
        Seeders\WSBrokerSeeder::class,
      ]);

      // IDAM and properties
      $this->call ([
        Seeders\IDAMSeeder::class,
        Seeders\UserSeeder::class,
        Seeders\OperatorSeeder::class,
      ]);

      // Add hardware
      $this->call ([
        Seeders\WifiSeeder::class, // Pingable MikroTik with VPN + HTTP
        Seeders\HABSeeder::class, // Pingable Pi with VPN + HTTP
        Seeders\TelecomsSeeder::class, // PBX + VPN, pingable DECT with VPN + HTTP + handsets
        Seeders\MirrorSeeder::class, // Pingable Pi with VPN
        Seeders\STBSeeder::class, // Pingable Android TV with VPN
      ]);

      // Add example rooms
      $this->call ([
        Seeders\BulbSeeder::class, // devices for 2 rooms
        Seeders\CameraSeeder::class, // devices for 1 rooms
        Seeders\ChromecastSeeder::class, // devices for 1 rooms
        Seeders\DimmerSeeder::class, // devices for 1 rooms
        Seeders\LEDSeeder::class, // devices for 2 rooms
        Seeders\LockSeeder::class, // devices for 1 rooms --> NOT DONE YET
        Seeders\MotorSeeder::class, // devices for 1 rooms --> NOT DONE YET
        Seeders\PlugSeeder::class, // devices for 2 rooms
        Seeders\SensorSeeder::class, // devices for 1 rooms
        Seeders\SpeakerSeeder::class, // devices for 1 rooms
        Seeders\SwitchSeeder::class, // devices for 1 rooms
        Seeders\ThermostatSeeder::class, // devices for 1 rooms
        Seeders\TVSeeder::class, // devices for 1 rooms
      ]);

    }
}
