<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Components\Telecoms\PBXRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNRepository;

class TelecomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $room = app(RoomRepository::class)->first();

        $vpn = app (VPNRepository::class)->first();

        $user = app (UserRepository::class)->first();

        $pbx = app(PBXRepository::class)->create([
            'country' => 'US',
            'host' => 'smartrooms.3cx.us',
            'label' => '3CX v16 PBX Server',
            'ip' => '35.199.19.91',
            'vpn_ip'  => '100.96.1.26',
            'license' => 'NW4T-C4IK-DAIU-7EPF',
            'type' => '3cx',
            'v' => 16,
            'trunk_vendor' => 'example',
            'trunk_number' => '55555501234',
            'sip_port' => 5060,
            'secure_sip_port' => 5061,
            'rtp_port_range' => [
                9000,
                10999,
            ],
            'admin_url' => 'https://smartrooms.3cx.us',
            'admin_port' => 443,
            'admin_user' => 'admin',
            'admin_pwd' => Crypt::encrypt('cstr8media'),
            'ordered_at'    => now()->subYears (1),
            'ordered_by'    => $user->_id,
            'order_id'      => 'EMPTY',
            'installed_at'  => now(),
            'installed_by'  => $user->_id,
            'provisioned_at'=> now(),
            'provisioned_by'=> $user->_id,
            'created_by'    => $user->_id,
        ]);

        $pbx->vpns()->attach ($vpn);

        $extension = $pbx->extensions()->create([
            'remote_id' => 2,
            'caller_id' => 'Room '.$room->number,
            'sip_id' => (string) $room->number,
            'label' => 'Room '.$room->number.' Extension',
            'auth_id' => 'xf5CIfMjk4',
            'auth_pwd' => Crypt::encrypt('cstr8media'),
            'did' => '+17035550100',
            'vpin' => '4443',
            'installed_at'  => now(),
            'installed_by'  => $user->_id,
            'provisioned_at'=> now(),
            'provisioned_by'=> $user->_id,
            'created_by'    => $user->_id,
            'room_id'       => $room->_id,
        ]);

        $dect = $room->dects()->create([
            'pbx_id' => $pbx->_id,
            'type' => 'grandstream-dp750',
            'name' => 'DECT-'.$room->number,
            'label' => 'DP750 DECT Base Station',
            'model' => 'DP750',
            'serial' => '9610003816A',
            'rfpi' => '02DC257F90',
            'v' => '1.0.9.9',
            'int_ip' => '192.168.86.250',
            'vpn_ip'  => '100.96.1.28',
            'mac' => 'C0:74:AD:05:E0:59',
            'admin_user' => 'admin',
            'admin_pwd' => Crypt::encrypt('cstr8media'),
            'router_id'  => $room->routers()->first()->_id,
            'ordered_at'    => now()->subYears (1),
            'ordered_by'    => $user->_id,
            'order_id'      => 'EMPTY',
            'installed_at'  => now(),
            'installed_by'  => $user->_id,
            'provisioned_at'=> now(),
            'provisioned_by'=> $user->_id,
            'created_by'    => $user->_id,
        ]);

        $dect->extensions()->attach($extension);
        $dect->vpns()->attach ($vpn);

        $handset = $room->handsets()->create([
            'type' => 'grandstream-dp720',
            'name' => 'SRL1',
            'label' => 'DP720 VoIP Handset',
            'model' => 'DP720',
            'ipei' => '02DC26C35A',
            'v' => '1.0.9.9',
            'serial' => 'oBFsGkZi39EH8',
            'int_ip' => '192.168.86.160',
            'mac' => 'b1:2a:b0:35:f5:b2',
            'ordered_at'    => now()->subYears (1),
            'ordered_by'    => $user->_id,
            'order_id'      => 'EMPTY',
            'installed_at'  => now(),
            'installed_by'  => $user->_id,
            'provisioned_at'=> now(),
            'provisioned_by'=> $user->_id,
            'created_by'    => $user->_id,
        ]);

        $handset->dects()->attach($dect);
        $handset->extensions()->attach($extension);
    }
}
