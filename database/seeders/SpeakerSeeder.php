<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class SpeakerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $room = app (RoomRepository::class)->first();
      $user = app (UserRepository::class)->first();

      $room->speakers()->create ([
        'type'    => 'soundtouch-10',
        'driver'  => 'openhab',
        'label'   => 'Soundtouch Speaker 01',
        'items' => [
          'mode'      => 'soundtouch_01_mode',
          'mute'      => 'soundtouch_01_mute',
          'power'     => 'soundtouch_01_power',
          'player'    => 'soundtouch_01_player',
          'preset'    => 'soundtouch_01_preset',
          'volume'    => 'soundtouch_01_volume',
        ],
        'options' => [
          'modes' => [
            'standby'   => 'STANDBY',
            'radio'     => 'INTERNET_RADIO',
            'bluetooth' => 'BLUETOOTH',
            'stored'    => 'STORED_MUSIC',
            'aux'       => 'AUX',
            'spotify'   => 'SPOTIFY',
            'pandora'   => 'PANDORA',
            'deezer'    => 'DEEZER',
            'sirius'    => 'SIRIUSXM',
            'amazon'    => 'AMAZON',
          ],
        ],
        'ordered_at'    => now()->subYears (1),
        'ordered_by'    => $user->_id,
        'order_id'      => 'EMPTY',
        'installed_at'  => now(),
        'installed_by'  => $user->_id,
        'provisioned_at'=> now(),
        'provisioned_by'=> $user->_id,
        'created_by'    => $user->_id,
        'last_signal_at'=> NULL,
        'last_command_at'=> NULL,
      ]);
    }
}
