<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class ThermostatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $room = app (RoomRepository::class)->first();
      $user = app (UserRepository::class)->first();

      $room->thermostats()->create ([
        'type'    => 'gocontrol-GC-TBZ48',
        'driver'  => 'openhab',
        'label'   => 'GoControl Thermostat 01',
        'items' => [
          'cooling_point' => 'thermostat_01_cooling_point',
          'fan_mode'      => 'thermostat_01_fan_mode',
          'fan_state'     => 'thermostat_01_fan_state',
          'heating_point' => 'thermostat_01_heating_point',
          'mode'          => 'thermostat_01_mode',
          'op_state'      => 'thermostat_01_opstate',
          'power'         => 'thermostat_01_power',
        ],
        'options' => [
          'modes' => [
            'off'   => 0,
            'heat'  => 1,
            'cool'  => 2,
            'auto'  => 3,
            'aux'   => 4,
          ],
          'fan_modes' => [
            'auto' => 0,
            'on'   => 1,
          ],
          'op_states' => [
            'heat'  => 0,
            'cool'  => 1,
            '2nd'   => 2,
            '3rd'   => 3,
            'fan'   => 4,
            'last'  => 5,
            'mot'   => 6,
            'mrt'   => 7,
          ],
          'fan_states' => [
            'stopped' => 0,
            'running' => 1,
          ],
        ]
        'ordered_at'    => now()->subYears (1),
        'ordered_by'    => $user->_id,
        'order_id'      => 'EMPTY',
        'installed_at'  => now(),
        'installed_by'  => $user->_id,
        'provisioned_at'=> now(),
        'provisioned_by'=> $user->_id,
        'created_by'    => $user->_id,
        'last_signal_at'=> NULL,
        'last_command_at'=> NULL,
      ]);
    }
}
