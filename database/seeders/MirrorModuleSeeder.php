<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use SmartRooms\Contracts\Repositories\Components\Mirror\ModuleRepository;

class MirrorModuleSeeder extends Seeder
{
    private $data = [
        'default' => [
            'alert' => [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'Displays notifications from other modules',
                'position' => null,
                'config' => [
                    'effect' => 'slide',
                    'alert_effect' => 'jelly',
                    'display_time' => 3500,
                    'position' => 'center',
                    'welcome_message' => false,
                ],
            ],
            'calendar'=> [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'Displays events from a public .ical calendar',
                'position' => 'top_left',
                'config' => [
                    'calendars' => [
                        [
                            'name' => 'US Holidays',
                            'symbol' => 'calendar-check',
                            'url' => 'webcal://www.calendarlabs.com/ical-calendar/ics/76/US_Holidays.ics',
                            'auth' => [
                                'user' => '',
                                'pass' => '',
                                'method' => 'basic',
                            ],
                            'color' => '#efefef',
                            'repeatingCountTitle' => 'Holiday',
                            'maximumEntries' => 100,
                            'maximumNumberOfDays' => 365,
                            'symbolClass' => '',
                            'titleClass' => '',
                            'timeClass' => '',
                            'broadcastPastEvents' => false,
                        ],
                    ],
                    'maximumEntries' => 10,
                    'maximumEntries' => 365,
                    'displaySymbol' => true,
                    'defaultSymbol' => 'calendar',
                    'showLocation' => false,
                    'maxTitleLength' => 25,
                    'maxLocationTitleLength' => 25,
                    'wrapEvents' => false,
                    'wrapLocationEvents' => false,
                    'maxTitleLines' => 3,
                    'maxEventTitleLines' => 3,
                    'fetchInterval' => 300000,
                    'animationSpeed' => 2000,
                    'fade' => true,
                    'fadePoint' => 0.25,
                    'tableClass' => 'small',
                    'titleReplace' => ['De verjaardag van ' => '', "'s birthday" => ''],
                    'displayRepeatingCountTitle' => false,
                    'dateFormat' => 'MMM Do',
                    'dateEndFormat' => 'HH:mm',
                    'showEnd' => true,
                    'fullDayEventDateFormat' => 'MMM Do',
                    'timeFormat' => 'relative',
                    'showEnd' => true,
                    'getRelative' => 6,
                    'urgency' => 7,
                    'broadcastEvents' => true,
                    'hidePrivate' => false,
                    'hideOngoing' => false,
                    'excludedEvents' => [],
                    'broadcastPastEvents' => false,
                    'sliceMultiDayEvents' => true,
                    'nextDaysRelative' => false,
                ],
            ],
            'clock'=> [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'Displays the current date and time',
                'position' => 'top_left',
                'config' => [
                    'timeFormat' => 24,
                    'displaySeconds' => true,
                    'showPeriod' => true,
                    'showPeriodUpper' => false,
                    'clockBold' => false,
                    'showDate' => true,
                    'showWeek' => false,
                    'showSunTimes' => false,
                    'showMoonTimes' => false,
                    'lat' => 47.630539,
                    'lon' => -122.344147,
                    'dateFormat' => 'dddd, LL',
                    'displayType' => 'digital',
                    'analogSize' => 200,
                    'analogFace' => 'simple',
                    'secondsColor' => '#888888',
                    'analogPlacement' => 'bottom',
                    'analogShowDate' => 'top',
                    'timezone' => 'none',
                ],
            ],
            'compliments'=> [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'Displays a random compliment',
                'position' => 'lower_third',
                'config' => [
                    'compliments' => [
                        'anytime' => [
                            'Hey there sexy!',
                        ],
                        'morning' => [
                            'Good morning, handsome!',
                            'Enjoy your day!',
                            'How was your sleep?',
                        ],
                        'afternoon' => [
                            'Hello, beauty!',
                            'You look sexy!',
                            'Looking good today!',
                        ],
                        'evening' => [
                            'Wow, you look hot!',
                            'You look nice!',
                            'Hi, sexy!',
                        ],
                    ],
                    'updateInterval' => 30000,
                    'fadeSpeed' => 4000,
                    'remoteFile' => null,
                    'classes' => 'thin xlarge bright',
                    'morningStartTime' => 3,
                    'morningEndTime' => 12,
                    'afternoonStartTime' => 12,
                    'afternoonEndTime' => 17,
                ],
            ],
            'currentweather'=> [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'Displays the current weather, including the windspeed, the sunset or sunrise time, the temperature and an icon to display the current conditions',
                'position' => 'top_right',
                'config' => [
                    'location' => 'Los Angeles',
                    'locationID' => '', //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
                    'appid' => 'YOUR_OPENWEATHER_API_KEY',
                    'units' => 'imperial',
                    'roundTemp' => false,
                    'degreeLabel' => false,
                    'updateInterval' => 600000,
                    'animationSpeed' => 1000,
                    'timeFormat' => 24,
                    'showPeriod' => true,
                    'showPeriodUpper' => false,
                    'showWindDirection' => true,
                    'showWindDirectionAsArrow' => false,
                    'showHumidity' => false,
                    'showSun' => true,
                    'showIndoorTemperature' => false,
                    'onlyTemp' => false,
                    'showFeelsLike' => true,
                    'useKMPHwind' => false,
                    'useBeaufort' => true,
                    'lang' => 'en',
                    'decimalSymbol' => '.',
                    'initialLoadDelay' => 0,
                    'retryDelay' => 2500,
                    'apiVersion' => 2.5,
                    'apiBase' => 'http://api.openweathermap.org/data/',
                    'weatherEndpoint' => 'weather',
                    'appendLocationNameToHeader' => true,
                    'useLocationAsHeader' => false,
                    'calendarClass' => 'calendar',
                    'iconTable' => [
                        '01d'=> 'wi-day-sunny',
                        '02d'=> 'wi-day-cloudy',
                        '03d'=> 'wi-cloudy',
                        '04d'=> 'wi-cloudy-windy',
                        '09d'=> 'wi-showers',
                        '10d'=> 'wi-rain',
                        '11d'=> 'wi-thunderstorm',
                        '13d'=> 'wi-snow',
                        '50d'=> 'wi-fog',
                        '01n'=> 'wi-night-clear',
                        '02n'=> 'wi-night-cloudy',
                        '03n'=> 'wi-night-cloudy',
                        '04n'=> 'wi-night-cloudy',
                        '09n'=> 'wi-night-showers',
                        '10n'=> 'wi-night-rain',
                        '11n'=> 'wi-night-thunderstorm',
                        '13n'=> 'wi-night-snow',
                        '50n'=> 'wi-night-alt-cloudy-windy',
                    ],
                ],
            ],
            'helloworld'=> [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'Display a static line of text',
                'position' => 'bottom_bar',
                'config' => [
                    'text' => 'Hello, world!',
                ],
            ],
            'newsfeed'=> [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'Displays news headlines based on an RSS feed',
                'position' => 'bottom_bar',
                'config' => [
                    'feeds' => [
                        [
                            'title' => 'New York Times',
                            'url' => 'https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml',
                            'encoding' => 'UTF-8',
                        ],
                    ],
                    'showSourceTitle' => true,
                    'showPublishDate' => true,
                    'broadcastNewsFeeds' => true,
                    'broadcastNewsUpdates' => true,
                    'showDescription' => false,
                    'wrapTitle' => true,
                    'wrapDescription' => true,
                    'truncDescription' => true,
                    'lengthDescription' => 400,
                    'hideLoading' => false,
                    'reloadInterval' => 300000,
                    'updateInterval' => 10000,
                    'animationSpeed' => 2500,
                    'maxNewsItems' => 0,
                    'ignoreOldItems' => false,
                    'ignoreOlderThan' => 86400000,
                    'removeStartTags' => 'both',
                    'startTags' => ['news'],
                    'removeEndTags' => 'both',
                    'endTags' => [],
                    'prohibitedWords' => ['fuck'],
                    'prohibitedWords' => 500,
                    'logFeedWarnings' => false,
                ],
            ],
            'updatenotification'=> [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'Displays a message whenever a new version of the application is available',
                'position' => 'top_bar',
                'config' => [
                    'updateInterval' => 600000,
                    'ignoreModules' => [],
                ],
            ],
            'weather'=> [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'A replacement for the current currentweather and weatherforcast modules',
                'position' => 'top_right',
                'config' => [
                    'type' => 'current',
                    'location' => 'Los Angeles',
                    'locationID' => '', //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
                    'appid' => 'YOUR_OPENWEATHER_API_KEY',
                    'units' => 'imperial',
                    'roundTemp' => false,
                    'degreeLabel' => false,
                    'updateInterval' => 600000,
                    'animationSpeed' => 1000,
                    'timeFormat' => 24,
                    'showPeriod' => true,
                    'showPeriodUpper' => false,
                    'showWindDirection' => true,
                    'showWindDirectionAsArrow' => false,
                    'showHumidity' => false,
                    'showSun' => true,
                    'showIndoorTemperature' => false,
                    'onlyTemp' => false,
                    'showFeelsLike' => true,
                    'useKMPHwind' => false,
                    'useBeaufort' => true,
                    'lang' => 'en',
                    'decimalSymbol' => '.',
                    'initialLoadDelay' => 0,
                    'retryDelay' => 2500,
                    'apiVersion' => 2.5,
                    'apiBase' => 'http://api.openweathermap.org/data/',
                    'weatherEndpoint' => 'weather',
                    'appendLocationNameToHeader' => true,
                    'useLocationAsHeader' => false,
                    'calendarClass' => 'calendar',
                    'iconTable' => [
                        '01d'=> 'wi-day-sunny',
                        '02d'=> 'wi-day-cloudy',
                        '03d'=> 'wi-cloudy',
                        '04d'=> 'wi-cloudy-windy',
                        '09d'=> 'wi-showers',
                        '10d'=> 'wi-rain',
                        '11d'=> 'wi-thunderstorm',
                        '13d'=> 'wi-snow',
                        '50d'=> 'wi-fog',
                        '01n'=> 'wi-night-clear',
                        '02n'=> 'wi-night-cloudy',
                        '03n'=> 'wi-night-cloudy',
                        '04n'=> 'wi-night-cloudy',
                        '09n'=> 'wi-night-showers',
                        '10n'=> 'wi-night-rain',
                        '11n'=> 'wi-night-thunderstorm',
                        '13n'=> 'wi-night-snow',
                        '50n'=> 'wi-night-alt-cloudy-windy',
                    ],
                ],
            ],
            'weatherforecast'=> [
                'repository' => 'https://github.com/MichMich/MagicMirror',
                'description' => 'Displays the weather forecast for the coming week, including an an icon to display the current conditions, the minimum temperature and the maximum temperature.',
                'position' => 'top_right',
                'config' => [
                    'location' => 'Los Angeles',
                    'locationID' => '', //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
                    'appid' => 'YOUR_OPENWEATHER_API_KEY',
                    'units' => 'imperial',
                    'roundTemp' => false,
                    'degreeLabel' => false,
                    'updateInterval' => 600000,
                    'animationSpeed' => 1000,
                    'timeFormat' => 24,
                    'showPeriod' => true,
                    'showPeriodUpper' => false,
                    'showWindDirection' => true,
                    'showWindDirectionAsArrow' => false,
                    'showHumidity' => false,
                    'showSun' => true,
                    'showIndoorTemperature' => false,
                    'onlyTemp' => false,
                    'showFeelsLike' => true,
                    'useKMPHwind' => false,
                    'useBeaufort' => true,
                    'lang' => 'en',
                    'decimalSymbol' => '.',
                    'initialLoadDelay' => 0,
                    'retryDelay' => 2500,
                    'apiVersion' => 2.5,
                    'apiBase' => 'http://api.openweathermap.org/data/',
                    'weatherEndpoint' => 'weather',
                    'appendLocationNameToHeader' => true,
                    'useLocationAsHeader' => false,
                    'calendarClass' => 'calendar',
                    'iconTable' => [
                        '01d'=> 'wi-day-sunny',
                        '02d'=> 'wi-day-cloudy',
                        '03d'=> 'wi-cloudy',
                        '04d'=> 'wi-cloudy-windy',
                        '09d'=> 'wi-showers',
                        '10d'=> 'wi-rain',
                        '11d'=> 'wi-thunderstorm',
                        '13d'=> 'wi-snow',
                        '50d'=> 'wi-fog',
                        '01n'=> 'wi-night-clear',
                        '02n'=> 'wi-night-cloudy',
                        '03n'=> 'wi-night-cloudy',
                        '04n'=> 'wi-night-cloudy',
                        '09n'=> 'wi-night-showers',
                        '10n'=> 'wi-night-rain',
                        '11n'=> 'wi-night-thunderstorm',
                        '13n'=> 'wi-night-snow',
                        '50n'=> 'wi-night-alt-cloudy-windy',
                    ],
                ],
            ],
        ],
        'entertainment' => [
            'MMM-SpotifyHelper' => [
                'repository' => 'https://github.com/victor-paumier/MMM-SpotifyHelper',
                'description' => 'Add events to MMM-Spotify to manage volume and playlists',
                'position' => 'center',
                'config' => [
                    'showNotif' => true,
                    'volume' => 50,
                    'volumeStep' => 10,
                    'urls' => [],
                ],
            ],
        ],
        'finance' => [
            'MMM-AVStock' => [
                'repository' => 'https://github.com/lavolp3/MMM-AVStock',
                'description' => 'Displays stock prices with Alphavantage API.',
                'position' => 'top_right',
                'config' => [
                    'apiKey' => 'YOUR_ALPHAVANTAGE_KEY', // https://www.alphavantage.co/
                    'timeFormat'=> 'YYYY-MM-DD HH:mm:ss',
                    'symbols' => ['aapl', 'GOOGL', '005930.KS'],
                    'alias' => ['APPLE', '', 'SAMSUNG Electronics'], //Easy name of each symbol. When you use `alias`, the number of symbols and alias should be the same. If value is null or "", symbol string will be used by default.
                    'tickerDuration' => 60, // Ticker will be cycled once per this second.
                    'chartDays' => 90, //For `mode:series`, how much daily data will be taken. (max. 90)
                    'poolInterval' => 1000 * 15, // (Changed in ver 1.1.0) - Only For Premium Account
                    'mode' => 'table', // "table", "ticker", "series"
                    'decimals' => 4, // number o decimals for all values including decimals (prices, price changes, change%...)
                    'candleSticks' => false, //show candle sticks if mode is Series
                    'coloredCandles' => false, //colored bars: red and green for negative and positive candles
                    'premiumAccount' => false, // To change poolInterval, set this to true - Only For Premium Account
                ],
            ],
        ],
        'health' => [
            'MMM-COVID19' => [
                'repository' => 'https://github.com/bibaldo/MMM-COVID19',
                'description' => 'Keeps track of Corona Virus (COVID-19) cases via rapidapi API',
                'position' => 'top_left',
                'config' => [
                    'updateInterval' => 300000,
                    'worldStats' => true,
                    'delta' => true,
                    'lastUpdateInfo' => true,
                    'countries' => ['Argentina', 'China', 'Italy', 'Spain'],
                    'headerRowClass' => 'small',
                    'rapidapiKey' => '01d6665ba777fdb4117cdmshc742d5373fae8f1cp148639jsn1', // this is an example, do not try to use it for real
                ],
            ],
        ],
        'iot' => [
            'MMM-Hue' => [
                'repository' => 'https://github.com/MitchSS/MMM-Hue',
                'description' => 'Displays the status of groups or lights in a Philips Hue setup.',
                'position' => 'top_right',
                'config' => [
                    'bridgeip' => '192.168.1.1',
                    'userid' => 'my user id',
                    'colour' => false,
                    'refreshTime' =>  60 * 10000,
                    'lightsorgroups' => 'groups',
                    'showOnlyOn' => false,
                    'hideSpecificGroups' => false,
                    'hideGroupsWithString' => '',
                    'showLabel' => true,
                ],
            ],
        ],
        'office' => [
            'MMM-GoogleSheets' => [
                'repository' => 'https://github.com/ryan-d-williams/MMM-GoogleSheets',
                'description' => 'Display Google Sheets data.',
                'position' => 'top_right',
                'config' => [
                    'url' => '',
                    'sheet' => 'Sheet1',
                    'range' => 'A1:B6',
                    'updateInterval' => 1, // minutes
                    'requestDelay' => 250, // ms
                    'updateFadeSpeed' => 0, // ms
                    'cellStyle' => 'mimic',
                    'border' => '1px solid #777',
                    'stylesFromSheet' => ['background-color', 'color', 'font-weight'],
                    'customStyles' => ['font-size: 18px', 'padding: 5px'],
                    'headerStyles' => ['font-weight: bold'],
                    'styleFunc' => '(rowNum, colNum, cellProps) => {if(rowNum%2 == 0){return "background-color:#666;"}}', // Colors every other row background
                ],
            ],
        ],
        'religion' => [
            'MMM-DailyBibleVerse' => [
                'repository' => 'https://github.com/arthurgarzajr/MMM-DailyBibleVerse',
                'description' => 'Displays the verse of the day from www.biblegateway.com',
                'position' => 'bottom_bar',
                'config' => [
                    'version' => 'NIV',
                    'size' => 'small',
                ],
            ],
        ],
        'sport' => [
            'MMM-NFL' => [
                'repository' => 'https://github.com/fewieden/MMM-NFL',
                'description' => 'Displays scores from the National Football League',
                'position' => 'top_right',
                'config' => [
                    'colored' => false,
                    'helmets' => false,
                    'focus_on' => false,
                    'format' => 'ddd h:mm',
                    'reloadInterval' => 1800000,
                ],
            ],
        ],
        'travel' => [
            'MMM-GoogleMapsTraffic' => [
                'repository' => 'https://github.com/vicmora/MMM-GoogleMapsTraffic',
                'description' => 'Displays a Google map, centered at provided coordinates, with traffic information.',
                'position' => 'top_left',
                'config' => [
                    'key' =>'YOUR_KEY',
                    'lat' => 37.8262306,
                    'lng' => -122.2920096,
                    'height' => '300px',
                    'width' => '300px',
                    'styledMapType' => 'transparent',
                    'disableDefaultUI' => true,
                    'backgroundColor' => 'hsla(0, 0%, 0%, 0)',
                    'markers' => [
                        [
                            'lat' => 37.8262316,
                            'lng' => -122.2920196,
                            'fillColor' => '#9966ff',
                        ],
                    ],
                ],
            ],
        ],
        'voice' => [
            'MMM-alexa' => [
                'repository' => 'https://github.com/sakirtemel/MMM-alexa',
                'description' => 'Connects Amazon Alexa Voice Synthesis(AVS) service',
                'position' => 'top_right',
                'config' => [
                    'avsDeviceId' => 'my_device',
                    'avsClientId' => 'amzn1.application-oa2-client.abcdefgh',
                    'avsClientSecret' => 'abcdefgh',
                    'avsInitialCode' => 'ANVabcdefgh',
                    'enableRaspberryButton' => true,
                    'hideStatusIndicator' => false,
                    'disableVoiceActivityDetection' => false,
                ],
            ],
        ],
        'weather' => [
            'MMM-Pollen' => [
                'repository' => 'https://github.com/vincep5/MMM-Pollen',
                'description' => 'Displays the pollen.com forecast for a US zipcode',
                'position' => 'top_left',
                'config' => [
                    'updateInterval' => 3 * 60 * 60 * 1000, // every 3 hours
                    'zip_code' => '90210',
                ],
            ],
        ],

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $group => $items) {
            foreach ($items as $module => $data) {
                app(ModuleRepository::class)->create(array_merge(['tags' => [$group], 'module' => $module], $data));
            }
        }
    }
}
