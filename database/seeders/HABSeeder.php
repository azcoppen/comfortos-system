<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \Carbon\Carbon;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNRepository;

class HABSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $vpn = app (VPNRepository::class)->first();

      $room = app(RoomRepository::class)->first();

      $user = app (UserRepository::class)->first();

      $hab = $room->habs()->create([
             'os'         => 'OpenHABian',
             'release'    => 'Raspbian GNU/Linux 10 (buster)',
             'codename'   => 'Buster',
             'hardware'   => 'BCM2711',
             'kernel'     => 'Linux 5.4.72-v7l+',
             'label'      => 'OpenHABian 2.5.1 HAB server',
             'host'       => 'openhab',
             'serial'     => '100000005c935dbf',
             'model'      => 'Raspberry Pi 4 Model B Rev 1.2',
             'engine'     => 'OpenHAB',
             'dist'       => '2.5.10-1',
             'int_ip'     => '192.168.86.248',
             'vpn_ip'     => '100.96.1.2',
             'mac'        => 'dc:a6:32:96:f7:26',
             'ssh_user'   => 'openhabian',
             'ssh_pass'   => \Crypt::encrypt('openhabian'),
             'http_port'  => 8080,
             'https_port' => 8443,
             'http_user'  => 'smartrooms',
             'http_pass'  => \Crypt::encrypt('smartrooms'),
             'router_id'  => $room->routers()->first()->_id,
             'ordered_at'    => now()->subYears (1),
             'ordered_by'    => $user->_id,
             'order_id'      => 'EMPTY',
             'installed_at'  => now(),
             'installed_by'  => $user->_id,
             'provisioned_at'=> now(),
             'provisioned_by'=> $user->_id,
             'created_by'    => $user->_id,
      ]);

      $hab->vpns()->attach ($vpn);

      $hab->createNote('HAB created by system seeding '.now()->format('c'));
    }
}
