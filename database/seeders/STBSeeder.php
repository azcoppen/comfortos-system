<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Components\Devices\TVAppRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;
use SmartRooms\Contracts\Repositories\Components\OS\VPNRepository;

class STBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vpn = app (VPNRepository::class)->first();

        $room = app(RoomRepository::class)->first();

        $user = app (UserRepository::class)->first();

        $stb = $room->stbs()->create([
            'type'       => 'nvidia-shield',
            'v'          => 'Pro',
            'host'       => 'shield',
            'name'       => 'SmartRooms',
            'label'      => 'Nvidia Shield Android TV Pro',
            'model'      => '945-12897-2500-101',
            'serial'     => 'UcqPqXCWsThaaPNxyiC',
            'os'         => 'android-9-pie',
            'int_ip'     => '192.168.86.61',
            'vpn_ip'     => '100.96.1.22',
            'router_id'  => $room->routers()->first()->_id,
            'items'      => [],
            'ordered_at'    => now()->subYears (1),
            'ordered_by'    => $user->_id,
            'order_id'      => 'EMPTY',
            'installed_at'  => now(),
            'installed_by'  => $user->_id,
            'provisioned_at'=> now(),
            'provisioned_by'=> $user->_id,
            'created_by'    => $user->_id,
        ]);

        $stb->vpns()->attach ($vpn);
        $stb->tv_apps()->attach (app(TVAppRepository::class)->all()->pluck('_id')->all())

        $stb->createNote('STB created by system seeding '.now()->format('c'));

    }
}
