<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Maklad\Permission\Models\Permission;
use Maklad\Permission\Models\Role;
use SmartRooms\Models\Components\ComfortOS;
use SmartRooms\Models\Components\Entertainment;
use SmartRooms\Models\Components\Entertainment\STB\App as TVApp;
use SmartRooms\Models\Components\Hub\Devices;
use SmartRooms\Models\Components\Mirror\Server as MMServer;
use SmartRooms\Models\Components\Mirror\Software as MMSoftware;
use SmartRooms\Models\Components\OS\Server as OSServer;
use SmartRooms\Models\Components\Telecoms;
use SmartRooms\Models\Components\Wifi;
use SmartRooms\Models\Components\Wifi\Network as WifiNetwork;
use SmartRooms\Models\IDAM;
use SmartRooms\Models\Operator;
use SmartRooms\Models\Property;

class IDAMSeeder extends Seeder
{
    //'env.domain.timeperiod.lang.object.operation'
    // domain = api, analysis, ui, queue, cli, data, server, smartthings, mikrotik, voip, pi
    // production.db.forever.all.bulbs.update
    // object.id.operation

    private $envs = [
        'production',
        'review',
        'dev',
    ];

    private $domains = [
        'api',
        'analysis',
        'cli',
        'data',
        'db',
        'dect',
        'esb',
        'handset',
        'im',
        'mirror',
        'notification',
        'pbx',
        'queue',
        'router',
        'os',
        'smartthings',
        'stb',
        'ui',
    ];

    private $periods = [
        'forever',
        'year',
        'quarter',
        'month',
        'week',
        'day',
        'hour',
    ];

    private $operations = [
        'index',
        'filter',
        'search',
        'view',
        'store',
        'update',
        'destroy',
        'hide',
        'restore',
        'wipe',
        'review',
        'backup',
        'lock',
        'unlock',
        'activate',
        'deactivate',
        'audit',
        'flag',
        'attach',
        'detach',
        'enable',
        'disable',
        'cancel',
        'block',
        'order',
        'request',
        'provision',
        'deprovision',
        'link',
        'unlink',
        'import',
        'export',
        'subscribe',
        'unsubscribe',
        'install',
        'uninstall',
        'observe',
        'publish',
        'unpublish',
        'broadcast',
        'relay',
        'copy',
        'manage',
    ];

    private $objects = [

        'system',
        'server',
        'queue',
        'ws',
        'cli',
        'smartthings',
        'mikrotik',
        '3cx',
        'hue',
        'androidtv',

        'users',
        'activity',
        'permissions',
        'roles',

        'brands',
        'operators',
        'properties',
        'buildings',
        'rooms',
        'devices',
        'telemetry',

        'bulbs',
        'cameras',
        'chromecasts',
        'codes',
        'dimmers',
        'ledstrips',
        'locks',
        'motors',
        'switches',
        'plugs',
        'sensors',
        'speakers',
        'thermostats',

        'mirrors',
        'mmsoftware',
        'propservers',
        'dect',
        'extensions',
        'handsets',
        'pbxs',
        'tvs',
        'stbs',
        'tvapps',
        'routers',
        'wifi',

    ];

    private $roles = [
        'developer',
        'superuser',
        'administrator',
        'executive',
        'auditor',
        'staff',
        'sales',
        'installer',
        'engineer',
        'provider',
        'partner',
        'analyst',
        'editor',
        'reviewer',
        'agent',
        'support',
        'operator',
        'property',
        'facility',
        'traveler',
        'anon',
        'guest',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('maklad.permission.cache');

        foreach ($this->roles as $role) {
            Role::create(['name' => $role, 'guard_name' => 'web']);
            Role::create(['name' => $role, 'guard_name' => 'api']);
        }

        return true; // DON'T ADD PERMS

        foreach ($this->envs as $env) {
            foreach ($this->objects as $object) {
                foreach ($this->operations as $operation) {
                    Permission::create([
                                    'name' => $env.'.'.$object.'.'.$operation,
                                    'guard_name' => 'web',
                                ]);

                    Permission::create([
                                    'name' => $env.'.'.$object.'.'.$operation,
                                    'guard_name' => 'api',
                                ]);
                }
            } // end operation
        } // end envs
    }
}
