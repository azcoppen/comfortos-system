<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class PlugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $rooms = app (RoomRepository::class)->all();
      $user = app (UserRepository::class)->first();

      foreach ($rooms AS $index => $room)
      {
        if ( $index < 2 )
        {
          $room->plugs()->create ([
            'type'    => 'innr-sp224',
            'driver'  => 'openhab',
            'label'   => 'Plug 01',
            'items' => [
              'power'       => 'plug_01_power',
              'level'       => 'plug_01_level',
            ],
            'ordered_at'    => now()->subYears (1),
            'ordered_by'    => $user->_id,
            'order_id'      => 'EMPTY',
            'installed_at'  => now(),
            'installed_by'  => $user->_id,
            'provisioned_at'=> now(),
            'provisioned_by'=> $user->_id,
            'created_by'    => $user->_id,
            'last_signal_at'=> NULL,
            'last_command_at'=> NULL,
          ]);

        }
      }
    }
}
