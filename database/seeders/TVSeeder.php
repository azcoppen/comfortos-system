<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class TVSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $room = app (RoomRepository::class)->first();
      $user = app (UserRepository::class)->first();

      $room->tvs()->create ([
        'type'    => 'samsung-UE46E5505',
        'driver'  => 'openhab',
        'label'   => 'Samsung TV 01',
        'items' => [
          'brightness'   => 'tv_01_brightness',
          'channel'      => 'tv_01_channel',
          'contrast'     => 'tv_01_contrast',
          'keycode'     => 'tv_01_keycode',
          'mute'        => 'tv_01_mute',
          'power'       => 'tv_01_power',
          'sharpness'   => 'tv_01_sharpness',
          'temperature' => 'tv_01_color_temp',
          'volume'      => 'tv_01_volume',
        ],
        'ordered_at'    => now()->subYears (1),
        'ordered_by'    => $user->_id,
        'order_id'      => 'EMPTY',
        'installed_at'  => now(),
        'installed_by'  => $user->_id,
        'provisioned_at'=> now(),
        'provisioned_by'=> $user->_id,
        'created_by'    => $user->_id,
        'last_signal_at'=> NULL,
        'last_command_at'=> NULL,
      ]);
    }
}
