<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Property\RoomRepository;

class ChromecastSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $room = app (RoomRepository::class)->first();
      $user = app (UserRepository::class)->first();

      $room->chromecasts()->create ([
        'type'    => 'v3',
        'driver'  => 'openhab',
        'label'   => 'Chromecast 01',
        'items' => [
          'mute'      => 'chromecast_01_mute',
          'player'    => 'chromecast_01_player',
          'stop'      => 'chromecast_01_stop',
          'volume'    => 'chromecast_01_volume',
        ],
        'ordered_at'    => now()->subYears (1),
        'ordered_by'    => $user->_id,
        'order_id'      => 'EMPTY',
        'installed_at'  => now(),
        'installed_by'  => $user->_id,
        'provisioned_at'=> now(),
        'provisioned_by'=> $user->_id,
        'created_by'    => $user->_id,
        'last_signal_at'=> NULL,
        'last_command_at'=> NULL,
      ]);
    }
}
