<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use SmartRooms\Contracts\Repositories\IDAM\UserRepository;
use SmartRooms\Contracts\Repositories\Components\OS\MQTTBrokerRepository;

class MQTTBrokerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = app (UserRepository::class)->first();

        $vpn = app (MQTTBrokerRepository::class)->create ([
          'type'          => 'mosquitto',
          'v'             => '2.0.3',
          'region'        => 'ca-east',
          'label'         => 'OVH Mosquitto (local)',
          'context'       => 'local',
          'host'          => 'localhost',
          'insecure_port' => 1883,
          'secure_port'   => 8883,
          'admin_user'    => null,
          'admin_pass'    => null,
          'admin_port'    => null,
          'app_user'      => 'smartrooms',
          'app_pass'      => \Crypt::encrypt ('cstr8media'),
          'vpn_ip'        => null,
          'ordered_at'    => now()->subYears (1),
          'ordered_by'    => $user->_id,
          'order_id'      => 'EMPTY',
          'installed_at'  => now(),
          'installed_by'  => $user->_id,
          'provisioned_at'=> now(),
          'provisioned_by'=> $user->_id,
          'created_by'    => $user->_id,
        ]);

        $vpn = app (MQTTBrokerRepository::class)->create ([
          'type'          => 'hivemq',
          'v'             => '2020.6',
          'region'        => 'ca-east',
          'label'         => 'OVH HiveMQ (remote)',
          'context'       => 'remote',
          'host'          => 'mqtt.smartrooms.dev',
          'insecure_port' => 1883,
          'secure_port'   => 8883,
          'admin_user'    => 'admin',
          'admin_pass'    => \Crypt::encrypt('hivemq'),
          'admin_port'    => 8080,
          'app_user'      => 'smartrooms',
          'app_pass'      => \Crypt::encrypt ('cstr8media'),
          'vpn_ip'        => null,
          'ordered_at'    => now()->subYears (1),
          'ordered_by'    => $user->_id,
          'order_id'      => 'EMPTY',
          'installed_at'  => now(),
          'installed_by'  => $user->_id,
          'provisioned_at'=> now(),
          'provisioned_by'=> $user->_id,
          'created_by'    => $user->_id,
        ]);
    }
}
