<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationIndices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::collection('operators', function ($collection) {
          $collection->geospatial('geo', '2dsphere');
      });
      Schema::collection('brands', function ($collection) {
          $collection->geospatial('geo', '2dsphere');
      });
      Schema::collection('properties', function ($collection) {
          $collection->geospatial('geo', '2dsphere');
      });
      Schema::collection('buildings', function ($collection) {
          $collection->geospatial('geo', '2dsphere');
      });
      Schema::collection('rooms', function ($collection) {
          $collection->geospatial('geo', '2dsphere');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
