{
  created_at: {
    $lte: new Date(),
    $gte: new Date(new Date().setDate(new Date().getDate()-1))
  }
}

{
  _id: {
    y: {
      $year: '$created_at'
    },
    m: {
      $month: '$created_at'
    },
    d: {
      $dayOfMonth: '$created_at'
    },
    h: {
      $hour: '$created_at'
    },
    i: {
      $minute: '$created_at'
    }
  },
  count: {
    $sum: 1
  }
}

{
  _id: 0,
  dt:     {
    $dateFromParts: {
      year: '$_id.y',
      month: '$_id.m',
      day: '$_id.d',
      hour: '$_id.h',
      minute: '$_id.i'
    }
  },
  ts: {
    $subtract: [
      {
      $dateFromParts: {
        year: '$_id.y',
        month: '$_id.m',
        day: '$_id.d',
        hour: '$_id.h',
        minute: '$_id.i'
      }
      },
      new Date("1970-01-01")
    ]
  },
  y: '$_id.y',
  m: '$_id.m',
  d: '$_id.d',
  h: '$_id.h',
  i: '$_id.i',
  total: '$count'
}





{
  _id: {
    room_id: '$room_id',
    y: {
      $year: '$created_at'
    },
    m: {
      $month: '$created_at'
    },
    d: {
      $dayOfMonth: '$created_at'
    },
    h: {
      $hour: '$created_at'
    },
    i: {
      $minute: '$created_at'
    }
  },
  count: {
    $sum: 1
  }
}

{
  _id: 0,
  room_id: '$_id.room_id',
  dt: {
    $dateFromParts: {
      year: '$_id.y',
      month: '$_id.m',
      day: '$_id.d',
      hour: '$_id.h',
      minute: '$_id.i'
    }
  },
  ts: {
    $subtract: [
      {
        $dateFromParts: {
          year: '$_id.y',
          month: '$_id.m',
          day: '$_id.d',
          hour: '$_id.h',
          minute: '$_id.i'
        }
      },
      ISODate('1970-01-01T00:00:00.000Z')
    ]
  },
  y: '$_id.y',
  m: '$_id.m',
  d: '$_id.d',
  h: '$_id.h',
  i: '$_id.i',
  total: '$count'
}


{
  _id: {
    component_id: '$component_id',
    y: {
      $year: '$created_at'
    },
    m: {
      $month: '$created_at'
    },
    d: {
      $dayOfMonth: '$created_at'
    },
    h: {
      $hour: '$created_at'
    },
    i: {
      $minute: '$created_at'
    }
  },
  count: {
    $sum: 1
  }
}
{
  _id: 0,
  component_id: '$_id.component_id',
  dt: {
    $dateFromParts: {
      year: '$_id.y',
      month: '$_id.m',
      day: '$_id.d',
      hour: '$_id.h',
      minute: '$_id.i'
    }
  },
  ts: {
    $subtract: [
      {
        $dateFromParts: {
          year: '$_id.y',
          month: '$_id.m',
          day: '$_id.d',
          hour: '$_id.h',
          minute: '$_id.i'
        }
      },
      ISODate('1970-01-01T00:00:00.000Z')
    ]
  },
  y: '$_id.y',
  m: '$_id.m',
  d: '$_id.d',
  h: '$_id.h',
  i: '$_id.i',
  total: '$count'
}
