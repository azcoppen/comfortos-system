{
  _id: {
    y: {
      $year: '$created_at'
    },
    m: {
      $month: '$created_at'
    },
    d: {
      $dayOfMonth: '$created_at'
    },
    h: {
      $hour: '$created_at'
    }
  },
  count: {
    $sum: 1
  }
}


{
_id: 0,

  dt: {
    $dateFromParts: {
      year: '$_id.y',
      month: '$_id.m',
      day: '$_id.d',
      hour: '$_id.h'
    }
  },
  ts: {
    $subtract: [
      {
        $dateFromParts: {
          year: '$_id.y',
          month: '$_id.m',
          day: '$_id.d',
          hour: '$_id.h'
        }
      },
      ISODate('1970-01-01T00:00:00.000Z')
    ]
  },
  y: '$_id.y',
  m: '$_id.m',
  d: '$_id.d',
  h: '$_id.h',
  total: '$count'
}
