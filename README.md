# SmartRooms Demo

Author: Alex Cameron < alex.cameron@cstraight.com >

## Overview

This app is a backend for the SmartRooms lab demo. It provides the POST webhook functionality required by:

- SmartThings SmartApp(s) - including Philips Hue Bridge
- MikroTik Router OS (Wifi Control)
- Android TV

SmartThings is Samsung's ecosystem for Smart Home Automation. It's a cloud platform for controlling IoT devices.

## Setup

### Database

Make sure you have the latest MongoDB:

```
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo systemctl daemon-reload
sudo systemctl start mongod
sudo systemctl status mongod
sudo systemctl enable mongod
```

Then set up a Mongo superuser to add to `.env` below:

```
mongo --username admin --password --authenticationDatabase admin
# (enter password)

use admin
db.createUser({ user: "smartrooms" , pwd: "redacted", roles: ["userAdminAnyDatabase", "dbAdminAnyDatabase", "readWriteAnyDatabase"]})
```

See:

- https://docs.mongodb.com/manual/mongo/
- https://docs.mongodb.com/manual/tutorial/create-users/

The simplest free UI for Mongo is Robo 3T: https://robomongo.org/ .

![logging](resources/screenshots/mongo.png "Mongo logging")


### PHP Specifics

Make sure you have the necessary PHP extensions:

```
php -v
php -m
sudo apt install php-mongodb php-redis php-mbstring php-curl php-dom php-sqlite3
```

### Application Specifics

Run basic setup tasks:

```
composer -vvv install
php artisan db:seed --class=UserSeeder
```

NB: Mongo doesn't need migrations.

### Running tests

PHPUnit runs in its own environment and uses SQLite by default. Make sure you've set the appropriate variables in `phpunit.xml`:

```
<testsuite name="SmartApp-Lifecycle">
    <directory suffix="Test.php">./tests/Unit/SmartThings/SmartApp/Webhook/Lifecycle</directory>
</testsuite>
```


```
<php>
    <server name="APP_ENV" value="testing"/>
    <server name="DB_CONNECTION" value="mongodb"/>
    <server name="DB_DATABASE" value="SmartRooms"/>
    <server name="QUEUE_CONNECTION" value="sync"/>
</php>
```

To run the Webhook tests:

```
vendor/bin/phpunit --testsuite SmartApp-Lifecycle
```

## Example .env File

In addition to the usual settings, you need these:

```
DB_CONNECTION=mongodb
DB_HOST=127.0.0.1
DB_PORT=27017
DB_DATABASE=SmartRooms
DB_USERNAME=redacted
DB_PASSWORD=redacted


PUSHER_APP_ID=redacted
PUSHER_APP_KEY=redacted
PUSHER_APP_SECRET=redacted
PUSHER_APP_CLUSTER=mt1

SLACK_WEBHOOK=https://hooks.slack.com/services/...

SMARTTHINGS_APP_ID=getfromworkspace
SMARTTHINGS_CLIENT_ID=getfromworkspace
SMARTTHINGS_CLIENT_SECRET=getfromworkspace
SMARTTHINGS_TOKEN=getfromworkspace
```

## Understanding MikroTik RouterOS

MikroTik are a Latvian network hardware company who produce Wifi equipment which operates via an embedded Linux firmware named RouterOS (https://mikrotik.com/software). Because it's Linux, it's programmable via SSH, HTTP, and all the usual ways you'd interact with a server. It even comes with virtual machine images and PHP libraries to use, e.g:

- https://wiki.mikrotik.com/wiki/API_PHP_package
- https://pear2.github.io/Net_RouterOS/
- https://github.com/EvilFreelancer/routeros-api-php.

Yes, the OS supports proxy servers, captive portals, VPNs, firewall config, rate-limiting, and individual user logins. These need to be pre-configured, if possible.

Documentation:

- https://wiki.mikrotik.com/wiki/Manual:Interface/Wireless
- https://i.mt.lv/files/pdf/manual/ros_2_9_wireless.pdf

### Setting the SSID (network name)

The command line sequence to create a 2.4Ghz access point on 2442Mhz with the name "MySmartRoom" is:

```
/interface wireless set wlan1 ssid=MySmartRoom frequency=2442 band=2.4ghz-b/g mode=ap-bridge disabled=no
```

The syntax to change the SSID is similar:

```
/interface wireless set wlan1 ssid=MySmartRoomNewName
```

### Setting the Wifi Password

RouterOS has a concept of multiple _security profiles_ (i.e. presets) which can be stored and reused. There is a `default` security profile which is easiest to work with.

To set up a simple network ("MySmartRoom") and assign it a security profile with a password of "mypasswordhere":

```
/interface wireless security-profiles add name=smartroom-profile mode=dynamic-keys authentication-types=wpa2-psk wpa2-pre-shared key=mypasswordhere
/interface wireless set wlan1 ssid=MySmartRoom security-profile=smartroom-profile frequency=auto disabled=no
```

To set the default security profile's password:

```
/interface wireless security-profiles set [ find default=yes ] wpa2-pre-shared-key=mypasswordhere
/interface wireless security-profiles set wpa2-pre-shared-key=mypasswordhere [find numbers="default"]
```

This is the equivalent of this call in the PHP Pear Library:

```
$util
    ->setMenu( '/interface wireless security-profiles')
    ->set (
        'default',
        [
            'wpa-pre-shared-key' => 'mypasswordhere',
            'wpa2-pre-shared-key' => 'mypasswordhere'
        ]
    );
```

To get the Wifi stats (e.g. signal strength etc), we need the `monitoring` interface:

```
/interface wireless monitor wlan1 once
```

Whereas from the API, we can loop endlessly using async requests with an `interval`:

```
$request = new Request('/interface wireless monitor numbers=wlan1 interval=2', null, 'm');

$client->sendAsync ($request, function ($response) {
    $signal = $response->getArgument('signal');
    if ($signal < 60) {
        //????
    }
});
$client->loop();
```


### Cloud Access

All this is well and good when you're on the same internal LAN (http://192.168.1.10 etc), but how do you expose the router API interface out onto the Interwebs?

First, it's generally a bad idea. So, think VPNs, IP restrictions, authentication, SSL, and other precautions.

The secret here is using **Ngrok** (https://ngrok.com/) to create an HTTPS tunnel. It has a programmatic API.

Install Ngrok on RouterOS, and do some basic setup like linking your Ngrok.com account via SSH:

```
ngrok authtoken <YOUR_AUTHTOKEN>
ngrok http -subdomain=accesspoint -auth="htuser:htpass" -bind-tls=true 80
```

_NB: You can use a config file and start it like this: ngrok http -config=/opt/ngrok/conf/ngrok.yml 8000_

Then, traffic sent to your subdomain (e.g. API calls to the router) is automatically forwarded to your device:

```
Tunnel Status                 online
Version                       2.0/2.0
Web Interface                 http://192.168.1.10:80
Forwarding                    http://accesspoint.ngrok.io -> localhost:80
Forwarding                    https://accesspoint.ngrok.io -> localhost:80
```

Now, you can send HTTPS requests to `https://accesspoint.ngrok.io` as if they were the internal `192.x` endpoint, and they are auto-forwarded.

Example API connection:

```
$client = \RouterOS::getClient([
    'host' => 'https://htuser:htpass@accesspoint.ngrok.io',
    'user' => 'admin', // API creds
    'pass' => 'admin', // API creds
    'port' => 443,
]);
```

Ngrok can channel SSH, Websockets, TLS sockets, and any other traffic you want.


## Understanding SmartThings Apps

**NB: Make sure you have a Samsung account and have joined the Cstraight Media organisation.**

SmartThings isn't exactly easy to understand, and the documentation isn't great.

### SmartThings iOS/Android App

Everything that happens within SmartThings is linked to the main phone app and your Samsung account. Once you create a SmartApp, you need to "add" it to your SmartThings phone app, like a Facebook app, kinda.

- https://play.google.com/store/apps/details?id=com.samsung.android.oneconnect&hl=en_US
- https://apps.apple.com/us/app/smartthings/id1222822904

Once installed, it looks on your Wifi network for a SmartThings Hub. Once you've added that, it auto-connects all the devices (which connect to it through Z-Wave/Zigbee). Then you can add integrations etc.

**Get this first.**

### Quick Overview

The general gist of this is you create a Facebook-style "SmartApp" in the developer workspace which acts as a virtual "bridge" between the phone app and your web backend (a POST webhook). When something happens, a POST goes to the webhook, which needs to respond in a specific way.

SmartApps have a "lifecycle" where a user "installs" them, and they generate "events". The terminology isn't great. See below for example screenshots.

A `location` (e.g. my house) is divided into `rooms` (unhelpfully named 'groups', living room, kitchen etc) which have `devices` (e.g. hub, sensor, alarm etc). Devices have `capabilities` (e.g. temperatureMeasurement) and require `permissions` (e.g. `w:devices:*`). These can be grouped into `scenes` and follow automated if/then sequences specified as `rules`.


It gets more complicated, because you can 'virtualise' devices and their functionality by creating `DeviceHandlers`. Believe it or not, the previous version before the API was so much worse.

In essence, when a user "installs" a SmartApp, they effectively give permission to it to do whatever they like to the devices they have connected into the SmartThings app on their phone.

## Our Setup

In our lab setup:

**Location** - Cstraight Office:
https://graph-na04-useast2.api.smartthings.com/location/show/ccff781f-621e-49af-8707-f932bf185682

_(hasMany)-->_

**Room** - Imaginarium:
https://graph-na04-useast2.api.smartthings.com/group/show/8ace3202-f677-40de-b996-e741486bdef1

_(hasMany)-->_

**SmartThings Hub**:

https://graph-na04-useast2.api.smartthings.com/hub/show/fe65990b-c817-4a6f-81a2-0f99fab56ab8

**Devices**

https://graph-na04-useast2.api.smartthings.com/device/list

**Device**

https://graph-na04-useast2.api.smartthings.com/device/show/fb96d992-6120-4234-9c44-6babfc9cc2db

_(hasMany)-->_

**Events**

https://graph-na04-useast2.api.smartthings.com/device/fb96d992-6120-4234-9c44-6babfc9cc2db/event/eb1ef389-57e2-11ea-9744-286d977df634?all=true&source=&max=25

### Main Documentation

It's nicely designed, but it's not helpful.

![docs](resources/screenshots/apidoc.png "API docs")

- Guides: https://smartthings.developer.samsung.com/
- API Reference: https://smartthings.developer.samsung.com/docs/api-ref/st-api.html
- App Reference: https://smartthings.developer.samsung.com/docs/api-ref/smartapps-v1.html
- List of All Capabilities: https://smartthings.developer.samsung.com/docs/api-ref/capabilities.html

The "capabilities" section is particularly bad. It's almost impossible to work out which devices support what. You have to use the phone app to guess at them.

### App Workspace

Workspaces come in 2 flavours and are a copy of Facebook's developer panel: a) your "private" one, and b) your "corporate" or "organisation" one. The SmartRooms demo app is here:

https://smartthings.developer.samsung.com/workspace/projects/CPT-AUTOMATION/LYsfJvSh7ua7Ftf4

![workspace](resources/screenshots/workspace.png "Workspace")

The workspace is for creating apps, registering them, and using the "live logging" console as the API interacts withthe backend.

### API Explorer:

This is essentially a web UI for everything your OAuth Consumer can see or is available to your Samsung account. It's unhelpfully named the "Groovy IDE". You can use it to view hubs, connected devices, events etc.

https://graph-na04-useast2.api.smartthings.com/

![ide](resources/screenshots/ide.png "IDE")

It's useful for getting a programmatic view. You send REST requests like GET, POST, PUT, PATCH etc to it, including device commands.

### Events & Schedules

To get "live" incoming updates from a device, you have to create a **subscription**. When something happens, like a sensor reading, SmartThings API sends a POST to your webhook.

See: https://smartthings.developer.samsung.com/docs/smartapps/subscriptions.html

A schedule is basically an automated task:

See: https://smartthings.developer.samsung.com/docs/smartapps/scheduling.html

## SmartApp Installation

The flow of "installing" a SmartApp into your SmartThings phone app (see? very confusing) is difficult to understand unless you visualise it. It is essentially a game of ping-pong with your webhook, which sends the instructions to the phone.

The ping-pong goes as follows:

1. PING - does the webhook exist? (happens when creating in the app in the workspace)
2. CONFIGURATION - send me the initialize stuff as a reply
3. CONFIGURATION/INITIALIZE - send me the app name and permissions required as a reply
4. CONFIGURATION/PAGE - send me the stuff you want the user to see on page 1 as a reply
5. CONFIGURATION/PAGE - send me the stuff you want the user to see on page 2 as a reply etc
6. CONFIGURATION - let the user say yes or no to the permissions
7. INSTALL - here are the user's devices
8. EVENT - something happened
9. UPDATE - something changed
10. UNINSTALL - you suck

The text and options are all specified by the webhook response. The info in these screenshots can be found in `config/smartthings.php`.

Example for screen 4 and 6:

```
'configuration' => [
  'initialize' => [
    'name'          => 'SmartRooms Lab Demo',
    'description'   => 'Lab application for recording monitoring data and issuing example commands to devices.',
    'id'            => 'smartrooms-v1',
    'permissions'   => [ // https://smartthings.developer.samsung.com/docs/auth-and-permissions.html
      'r:devices:*',
      'w:devices:*',
      'x:devices:*',
      'i:deviceprofiles',
      'r:locations:*',
      'r:scenes:*',
      'x:scenes:*',
    ],
    'firstPageId'   => '1',
  ],

  'page' => [
    'pageId'          => '1',
    'name'            => 'SmartRooms Lab Demo',
    'nextPageId'      => null,
    'previousPageId'  => null,
    'complete'        => true,
    'sections'        => [
      [
        'name' => 'Add Available Connected Devices To Control: ',
        'settings' => [
          [
            'id'            => 'samjin-sensor',
            'name'          => 'Which Multi-Purpose Sensor(s)?',
            'description'   => 'Tap to choose',
            'type'          => 'DEVICE',
            'required'      => false,
            'multiple'      => true,
            'capabilities'  => [
              'battery',
              'contactSensor',
              'temperatureMeasurement',
              'accelerationSensor',
              'threeAxis',
              //'vibrationSensor'
            ],
            'permissions'   => ['r', 'x'],
          ],

```

![install flow](resources/screenshots/flow.png "Install Flow")

Notice how it crashes at the end!
